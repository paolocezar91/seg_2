const args = process.argv,
	watch = !!~args.indexOf("-w"),
	prod = !!~args.indexOf("-p"),
	skipBuild = watch && !!~args.indexOf("-s"),
	fs = require("fs-extra"),
	// cheerio = require("cheerio"),
	browserify = require("browserify"),
	babelify = require("babelify"),
	watchify = require("watchify"),
	// express = require("express"),
	// http = require("http"),
	// WebSocket = require("ws"),
	// open = require("open"),
	// sass = require("node-sass"),
	// chokidar = require("chokidar"),
	through = require("through"),
	path = require("path"),
	// exorcist = require("exorcist"),
	uglifyify = require("uglifyify"),
	Console = console;

// //dev server setup start
// const PORT = 8080,
// 	app = express(),
// 	server = http.createServer(app),
// 	socket = new WebSocket.Server({server});

// // defining routes
// // injecting Websocket for livereload
// app.get("/", (req, res) => {
// 	fs.readFile("index.html", (err, data) => {
// 		const $ = cheerio.load(data);

// 		$("body").append(`
// 			<script>
// 			var reloadSocket = new WebSocket("ws://localhost:${PORT}");
// 			reloadSocket.onmessage = function() {
// 				location.reload();
// 			};
// 			</script>
// 		`);

// 		res.send($.html());
// 	});
// });

// app.get("/extensibility/*", (req, res) => fs.exists(__dirname+req.url, exists => {
// 	if(exists)
// 		res.sendFile(__dirname+req.url);
// 	else res.sendStatus(404);
// }));

// app.use(express.static(__dirname));
// server.listen(PORT);
// socket.on("connect", () => Console.log("client connected"));

// const reloadClient = () => {
// 	Console.log("Reloading...");
// 	socket.clients.forEach(client => {
// 		if (client.readyState === WebSocket.OPEN)
// 			client.send();
// 	});
// };

// dev server setup end

//extensibility
function resolvePath(file) {
	let buffer = "";

	if (!/\.js/.test(file))
		return through();

	return through(chunk => buffer += chunk.toString(), function() {
		this.queue(buffer.toString().replace(/resolvePath\(.(.+).\)/g, (str, match) => {

			return "`" + path.normalize(path.dirname(file) + "/" + match).replace(/.+\\extensibility\\src(.+)/g, "extensibility$1").split("\\").join("//") +"`";
		}));

		return this.queue(null);
	});
}

function svg(file) {
	let buffer = "";

	if (!/\.svg/.test(file))
		return through();

	return through(chunk => buffer += chunk.toString(), function() {
		const jst = buffer.toString(),
			compiled = `
				const parser = new DOMParser();
				module.exports = parser.parseFromString(${JSON.stringify(jst)}, "image/svg+xml").querySelector("svg");
			`;
		this.queue(compiled);
		return this.queue(null);
	});
}

const extensibilityTransforms = [
	svg,
	resolvePath,
	babelify.configure({
		presets: ["latest"],
		plugins: ["syntax-async-functions","transform-regenerator"]
	})
];

if(prod)
	extensibilityTransforms.push(uglifyify);

const ext = browserify({
	transform: extensibilityTransforms,
	entries: ["src/extensibility/src/operations/index.js"],
	standalone: "extensibility",
	cache: {},
	packageCache: {},
	plugin: watch?[watchify]:[],
	debug: !prod
});

ext.on("update", files => bundleExt(files).then(() => null, e => Console.error(e)));
ext.on("log", msg => Console.log("Finished bundling extensibility: "+ msg));

function bundleExt(files) {
	return new Promise((resolve, reject) => {
		Console.log("Bundling extensibility...");

		if(files) {
			Console.log("Files changed:");
			files.forEach(file => Console.log("\t"+file));
		}
		fs.copySync("src/extensibility/src", "src/extensibility");

		const writeStream = fs.createWriteStream("src/extensibility/extensibility.min.js");
		const stream = ext.bundle()
			.on("error", e => {
				reject(e.stack || e);
				writeStream.end();
			});

		/*if(!prod)
			stream.pipe(exorcist("extensibility/extensibility.min.js.map", "extensibility/extensibility.min.js.map")).pipe(writeStream).on("close", resolve);
		else */
		stream.pipe(writeStream).on("close", resolve);
	});
}
const build = () => bundleExt();

new Promise(resolve => resolve(skipBuild ? {} : build())).then(() => {
	if(watch) {
		// Console.log(`Serving SEG on http://localhost:${PORT}`);
		// open(`http://localhost:${PORT}`);
	} else {
		Console.log("Done");
		process.exit();
	}
}).catch(e => Console.error(e));