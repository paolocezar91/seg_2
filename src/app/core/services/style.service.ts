import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import Icon from "ol/style/Icon";
import { shareReplay } from "rxjs/operators";

const CACHE_SIZE = 1;

@Injectable({
	providedIn: "root"
})
export class StyleService {

	svgCache: any = {};

	constructor(private http: HttpClient) { }


	SVG(options) {
		const container = document.createElement("div"),
			{ svg, height, fill, stroke } = options;

		container.appendChild(svg);

		const svgContents = container.innerHTML,
			cacheKey = JSON.stringify({ svgContents, height, fill, stroke });

		delete options.svg;
		delete options.height;
		delete options.fill;
		delete options.stroke;

		if (!this.svgCache)
			this.svgCache = {};

		if (!this.svgCache[cacheKey]) {
			// clone svg
			const clone = svg.cloneNode(true);

			// set attributes
			clone.setAttribute("fill", fill);

			if (stroke) {
				clone.setAttribute("stroke", stroke.color);
				clone.setAttribute("stroke-width", stroke.width);
			}

			clone.setAttribute("height", height);

			const _container = document.createElement("div");

			_container.appendChild(clone);

			document.body.appendChild(_container);

			const boundingRect = clone.getBoundingClientRect(),
				ratio = boundingRect.width / boundingRect.height;

			clone.setAttribute("width", height * ratio);

			document.body.removeChild(_container);

			this.svgCache[cacheKey] = {
				src: "data:image/svg+xml," + encodeURIComponent(_container.innerHTML),
				imgSize: [height * ratio, height]
			};
		}

		return new Icon(Object.assign(options, this.svgCache[cacheKey]));
	}

	requestSVG(path) {
		return this.http.get(path, {responseType: "text"})
					.pipe(shareReplay(CACHE_SIZE));
	}
}

