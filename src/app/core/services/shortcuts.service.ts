import { MapService } from "@core/services/map/map.service";
import { SelectionService } from "@core/services/interactions/selection.service";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ShortcutsService {

	shortcuts;
	filteredFeatures = [];
	registerTypes = {generic: []};

	constructor(
		private selectionService: SelectionService,
		private mapService: MapService
	) {
		this.selectionService.onSelectionChange((featureToSelect, allSelectedFeatures) => {
			this.shortcuts = null;

			/*
			if (mapService.inMode("SELECT_FILTER"))
		    	mapService.disableMode("SELECT_FILTER");
		    */

			if (allSelectedFeatures.length) {
				const typeOfFirst = allSelectedFeatures[0].get("seg_type");
				if (this.registerTypes[typeOfFirst] && allSelectedFeatures.every(ft => ft.get("seg_type") === typeOfFirst))
					this.shortcuts = this.registerTypes[typeOfFirst];
				else
					this.shortcuts = this.registerTypes["generic"];
			}
		});
	}

	add(layerId, actions) {
		return this.registerTypes[layerId] = actions;
	}

	getShortcuts() {
		return this.shortcuts || [];
	}

	removeShortcutsByLabel(label) {
		for (let i = 0; i < this.shortcuts.length; i++)
			if (this.shortcuts[i].label === label)
				return this.shortcuts.splice(i, 1);
	}

	hasShortcuts() {
		return this.shortcuts && this.shortcuts.length >= 0;
	}

	filterOut() {
		if (!this.mapService.inMode("SELECT_FILTER")) {
			this.filteredFeatures = this.selectionService.getSelectedFeatures();

			this.filteredFeatures.forEach(feature => feature.set("wasSelected", true));

			return this.mapService.enableMode("SELECT_FILTER");
		}

		this.filteredFeatures.forEach(feature => feature.unset("wasSelected"));

		this.filteredFeatures = [];

		return this.mapService.disableMode("SELECT_FILTER");
	}

	filterActive() {
		return !!this.mapService.inMode("SELECT_FILTER");
	}

}
