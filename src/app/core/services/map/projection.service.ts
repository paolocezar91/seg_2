import { Injectable } from "@angular/core";
import { get } from "ol/proj";
import { register } from "ol/proj/proj4.js";
import proj4 from "proj4";
import { RequestService } from "@core/services/request/request.service";

const DOTS_PER_INCH = 25.4 / 0.28,
	INCHES_PER_METER = 39.37,
	// projectionCache: {"EPSG:3395":"+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs"}
	projectionCache = JSON.parse(localStorage.getItem("projectionCache")) || {};

@Injectable({
	providedIn: "root"
})
export class ProjectionService {

	memoizedSfR: any;
	availableProjections: any;

	constructor(private requestService: RequestService) {
		// define CRS:84
		proj4.defs("CRS:84", "+proj=longlat +datum=WGS84 +no_defs");
		register(proj4);
	}

	load(projectionDefinitions) {

		projectionDefinitions.forEach(async projection => {
			// check if openlayers already has this projection defined
			let projectionDefinition = get(projection.code);

			// if it does and it has an extent, we"re done
			if (projectionDefinition && projectionDefinition.getExtent()) {
				this.setExtent(projection);
				return;
			}
			// if proj4 definition is cached, define it and we"re done
			if (projectionCache[projection.code]) {
				proj4.defs(projection.code, projectionCache[projection.code]);
				register(proj4);
				this.setExtent(projection);
				return;
			}

			// if there"s a proj4 definition in the projection, use it
			if (projection.proj4) {
				proj4.defs(projection.code, projection.proj4);
				register(proj4);
				this.setExtent(projection);
				return;
			}

			// fallback to epsg.io lookup
			await this.requestService.get("https://epsg.io", {
				params: {
					format: "json",
					q: projection.code.split(":")[1]
				}
			}).toPromise()
				.then(({results}: any) => {
					if (!results.length)
						return console.log("Could not load projection " + (projection.name || projection.code));

					projectionDefinition = results[0].proj4;

					// if we found a definition, cache it and define it
					projectionCache[projection.code] = projectionDefinition;
					// update projection cache with new definitions
					localStorage.setItem("projectionCache", JSON.stringify(projectionCache));

					proj4.defs(projection.code, projectionDefinition);
					register(proj4);
					this.setExtent(projection);
					return;
				});
		});

		this.availableProjections = projectionDefinitions;
	}

	setExtent(projection) {
		if (projection.extent)
			get(projection.code).setExtent(projection.extent);

		if (projection.worldExtent)
			get(projection.code).setWorldExtent(projection.worldExtent);
	}

	getAvailableProjections() {
		return this.availableProjections;
	}

	isAvailable(projection) {
		return this.getAvailableProjections().find(({code}) => code === projection);
	}

	transformResolution(resolution, sourceProjection, destinationProjection) {
		const srcMPU = get(sourceProjection).getMetersPerUnit();
		const dstMPU = get(destinationProjection).getMetersPerUnit();

		return srcMPU * resolution / dstMPU;
	}

	getResolutionFromScale(scale, projection) {
		const mpu = get(projection).getMetersPerUnit();

		return scale / (DOTS_PER_INCH * mpu * INCHES_PER_METER);
	}

	getScaleFromResolution(resolution, projection) {
		if (this.memoizedSfR && this.memoizedSfR.resolution === resolution && this.memoizedSfR.projection === projection)
			return this.memoizedSfR.scale;

		const mpu = get(projection).getMetersPerUnit(),
			scale = (resolution * DOTS_PER_INCH * mpu * INCHES_PER_METER);

		this.memoizedSfR = {
			resolution,
			projection,
			scale
		};

		return scale;
	}
}
