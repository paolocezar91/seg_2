import { Injector, ElementRef } from "@angular/core";
import Feature from "ol/Feature";
import Geometry from "ol/geom/Geometry";
import GeometryCollection from "ol/geom/GeometryCollection";
import Map from "ol/Map";
import VectorSource from "ol/source/Vector";
import { View } from "ol";
import * as extent from "ol/extent";
import * as interaction_ from "ol/interaction";
import * as proj from "ol/proj";
import { ProjectionService } from "@core/services/map/projection.service";
import { SelectionService } from "@core/services/interactions/selection.service";
import { SourceService } from "@core/services/layer/source.service";
import * as $o from "libs/seg-object";

const MIN_SCALE = 500,
	MAX_SCALE = 70000000;

export class SegMap {
	interactions;
	ol_: Map;
	scaleChangeListeners = new $o.EventListenerQueue();
	projectionChangeListeners = new $o.EventListenerQueue();
	centerChangeListeners = new $o.EventListenerQueue();
	viewportSizeChangeListeners = new $o.EventListenerQueue();
	viewportChangeListeners = new $o.EventListenerQueue();
	viewportChangeEndListeners = new $o.EventListenerQueue();
	preferentialLayers: Array<any> = [];
	layers: Array<any>  = [];

	constructor(
		options,
		private sourceService: SourceService,
		private projectionService: ProjectionService
	) {
		const layers = options.layers;
		delete options.layers;

		/**
		 * OpenLayers Map instance
		 * @member {external:"ol.Map"} seg.map.ol_
		*/
		this.ol_ = new Map({
			controls: [],
			pixelRatio: 1,
			interactions: interaction_.defaults({
				altShiftDragRotate: false,
				doubleClickZoom: false,
				keyboard: false,
				shiftDragZoom: false,
				pinchRotate: false
			}),
			logo: false,
			view: new View({
				projection: options.projection,
				resolution: this.projectionService.getResolutionFromScale(options.scale, options.projection),
				minResolution: this.projectionService.getResolutionFromScale(MIN_SCALE, options.projection),
				maxResolution: this.projectionService.getResolutionFromScale(MAX_SCALE, options.projection),
				center: proj.fromLonLat(options.center, options.projection)
			}),
			target: options.target
		});

		this.ol_.on("change:size", e => {
			this.viewportSizeChangeListeners.trigger(this.ol_.getSize(), e.oldValue);
			this.viewportChangeListeners.trigger();
		});

		this.addLayers(layers);

		this.setupViewportListeners();

		this.forEachLayerInLayerTree(ngolLayer => {
			if (ngolLayer.minScale)
				ngolLayer.ol_.setMaxResolution(this.projectionService.getResolutionFromScale(ngolLayer.minScale, this.getProjection()));

			if (ngolLayer.maxScale)
				ngolLayer.ol_.setMinResolution(this.projectionService.getResolutionFromScale(ngolLayer.maxScale, this.getProjection()));
		});

		this.ol_.on("moveend", () => this.viewportChangeEndListeners.trigger());
	}

	/**
	* @method seg.map.getViewportExtent
	* @returns {external:"ol.Extent"} Viewport extent
	*/
	getViewportExtent() {
		return this.ol_.getView().calculateExtent(this.ol_.getSize());
	}
	getTarget() {
		return this.ol_.getTargetElement();
	}
	setTarget(element) {
		this.ol_.setTarget(document.querySelector(element));
	}
	/**
	* @method seg.map.getLayers
	* @returns {seg.layer.Base[]} Map layers
	*/
	getLayers() {
		return this.layers;
	}

	setupLayer(ngolLayer) {
		if (typeof ngolLayer.setMap === "function")
			ngolLayer.setMap(this);

		if (ngolLayer.minScale)
			ngolLayer.ol_.setMaxResolution(this.projectionService.getResolutionFromScale(ngolLayer.minScale, this.getProjection()));

		if (ngolLayer.maxScale)
			ngolLayer.ol_.setMinResolution(this.projectionService.getResolutionFromScale(ngolLayer.maxScale, this.getProjection()));

		if (typeof ngolLayer.forEachLayerInLayerTree === "function")
			ngolLayer.forEachLayerInLayerTree(layer => this.setupLayer(layer));
	}
	/**
	* @method seg.map.addLayers
	* @param {seg.layer.Base[]} Layers to add
	*/
	addLayers(segLayers) {
		segLayers = [].concat(segLayers);

		segLayers.forEach(segLayer => {
			segLayer.transform(this.getProjection(), this.sourceService);

			this.setupLayer(segLayer);

			this.layers.push(segLayer);
			this.ol_.addLayer(segLayer.ol_);
		});
	}
	/**
	* @method seg.map.addLayer
	* @param {seg.layer.Base} Layer to add
	*/
	addLayer(ngolLayer) {
		this.addLayers(ngolLayer);
	}
	/**
	* @method seg.map.removeLayer
	* @param {seg.layer.Base} Layer to remove
	*/
	removeLayer(layerToRemove) {
		this.getLayers().some((layer, i) => {
			if (layer === layerToRemove) {
				this.layers.splice(i, 1);
				this.ol_.removeLayer(layer.ol_);
				return true;
			}

			if (layer.removeLayer)
				return layer.removeLayer(layerToRemove);
		});
	}
	/**
	* Invokes **callback** passing each layer in the map as an argument
	* @method seg.map.forEachLayerInLayerTree
	* @param {function} callback
	*/
	forEachLayerInLayerTree(callback) {
		this.getLayers().forEach((layer, i) => {
			callback(layer, i);

			if (layer.forEachLayerInLayerTree)
				layer.forEachLayerInLayerTree(callback);
		});
	}
	/**
	* This registers a layer into an array. This array represents layers that have preference when selecting features
	* This is done, for instance, to prevent selecting an EO instead of a VDS, since they"re always intersecting but usually the user wants to select the VDS, not the EO.
	* @method seg.map.registerPreferentialLayers
	* @param {string} layer
	**/
	registerPreferentialLayers(layer) {
		this.preferentialLayers.push(layer);
	}
	/**
	* This returns the array of layers that have preference when selecting features
	* @method seg.map.getPreferentialLayers
	* @param {string} layer
	**/
	getPreferentialLayers() {
		return this.preferentialLayers;
	}
	/**
	* @method seg.map.getProjection
	* @returns {external:"ol.ProjectionLike"} Current map projection
	*/
	getProjection() {
		return this.getView().getProjection();
	}
	/**
	* @method seg.map.setProjection
	* @param {external:"ol.ProjectionLike"} projection
	*/
	setProjection(newProjection) {
		const oldProjection = this.getProjection();

		this.ol_.setView(new View({
			center: proj.fromLonLat(this.getCenter(), newProjection),
			projection: newProjection,
			resolution: this.projectionService.getResolutionFromScale(this.getScale(), newProjection),
			minResolution: this.projectionService.getResolutionFromScale(MIN_SCALE, newProjection),
			maxResolution: this.projectionService.getResolutionFromScale(MAX_SCALE, newProjection)
		}));

		this.setupViewportListeners();

		this.getLayers().forEach(ngolLayer => {
			if (ngolLayer.transform)
				ngolLayer.transform(newProjection, oldProjection);
		});

		this.forEachLayerInLayerTree(ngolLayer => {
			if (ngolLayer.minScale)
				ngolLayer.ol_.setMaxResolution(this.projectionService.getResolutionFromScale(ngolLayer.minScale, this.getProjection()));

			if (ngolLayer.maxScale)
				ngolLayer.ol_.setMinResolution(this.projectionService.getResolutionFromScale(ngolLayer.maxScale, this.getProjection()));

			if (ngolLayer.getSource && ngolLayer.getSource() instanceof VectorSource)
				ngolLayer.getSource().getFeatures().forEach(feature => feature.getGeometry().transform(oldProjection, newProjection));
		});

		this.projectionChangeListeners.trigger(newProjection, oldProjection);
		this.viewportChangeListeners.trigger();

		// preferencesService.setMapPreference("projection", newProjection);
	}
	/**
	* @method seg.map.onProjectionChange
	* @param {function} callback - Called with (`newProjection`, `oldProjection`)
	* @returns {function} Deregistration function
	*/
	onProjectionChange(callback) {
		return this.projectionChangeListeners.register(callback);
	}
	/**
	* @method seg.map.onViewportSizeChange
	* @param {function} callback - Called with (`newSize`, `oldSize`)
	* @returns {function} Deregistration function
	*/
	onViewportSizeChange(callback){
		return this.viewportSizeChangeListeners.register(callback);
	}
	/**
	* @method seg.map.onViewportChange
	* @param {function} callback
	* @returns {function} Deregistration function
	*/
	onViewportChange(callback) {
		return this.viewportChangeListeners.register(callback);
	}
	/**
	* Called when the map finishes updating (basically a listener to `moveend` on {@link external:"ol:Map"})
	* @method seg.map.onViewportChangeEnd
	* @param {function} callback
	* @returns {function} Deregistration function
	*/
	onViewportChangeEnd(callback) {
		return this.viewportChangeEndListeners.register(callback);
	}
	/**
	* @method seg.map.getScale
	* @returns {number} Current scale
	*/
	getScale() {
		return this.projectionService.getScaleFromResolution(this.getView().getResolution(), this.getProjection());
	}
	/**
	* @method seg.map.setScale
	* @param {number} scale
	*/
	setScale(newScale) {
		const oldScale = this.getScale();

		if (newScale !== oldScale)
			this.getView().setResolution(this.projectionService.getResolutionFromScale(newScale, this.getProjection()));
	}
	/**
	* @method seg.map.onScaleChange
	* @param {function} callback - Called with (`newScale`, `oldScale`)
	* @returns {function} Deregistration function
	*/
	onScaleChange(callback) {
		return this.scaleChangeListeners.register(callback);
	}

	// controls
	hasControl(control) {
		return control && this.ol_.getControls().getArray().some(existingControl => existingControl === control.ol_);
	}

	addControl(control) {
		if (!this.hasControl(control))
			this.ol_.addControl(control.ol_);
	}

	removeControl(control) {
		if (this.hasControl(control))
			this.ol_.removeControl(control.ol_);
	}

	// interactions
	hasInteraction(interaction) {
		return this.ol_.getInteractions().getArray().some(existingInteraction => existingInteraction === interaction);
	}

	addInteraction(interaction) {
		if (!this.hasInteraction(interaction))
			this.ol_.addInteraction(interaction);
	}

	removeInteraction(interaction) {
		if (this.hasInteraction(interaction))
			this.ol_.removeInteraction(interaction);
	}
	// overlays
	hasOverlay(overlay) {
		return this.ol_.getOverlays().getArray().some(existingOverlay => existingOverlay === overlay);
	}

	addOverlay(overlay) {
		if (!this.hasOverlay(overlay))
			this.ol_.addOverlay(overlay);
	}

	removeOverlay(overlay) {
		if (this.hasOverlay)
			this.ol_.removeOverlay(overlay);
	}

	/**
	* Similar to {@link external:"ol.Map"}.getCoordinateFromPixel, but always returns [lat, lon] no matter the current projection
	* @method seg.map.getCoordinateFromPixel
	* @param {external:"ol.Pixel"} pixel
	* @returns {external:"ol.Coordinate"} Coordinate in EPSG:4326
	*/
	getCoordinateFromPixel(pixel) {
		return proj.toLonLat(this.ol_.getCoordinateFromPixel(pixel) || [0, 0], this.getProjection());
	}
	/**
	* Similar to {@link external:"ol.View"}.getCenter, but always returns [lat, lon] no matter the current projection
	* @method seg.map.getCenter
	* @returns {external:"ol.Coordinate"} Viewport center coordinate in EPSG:4326
	*/
	getCenter() {
		return proj.toLonLat(this.getView().getCenter(), this.getProjection());
	}

	getPixelFromCoordinate(coordinate) {
		return this.ol_.getPixelFromCoordinate(coordinate);
	}

	centerOn(featuresCoordinatesOrGeometries, minResolution) {
		const geometries = [];

		featuresCoordinatesOrGeometries = [].concat(featuresCoordinatesOrGeometries);

		if (typeof featuresCoordinatesOrGeometries[0] === "number")
			return this.getView().setCenter(featuresCoordinatesOrGeometries);

		featuresCoordinatesOrGeometries.forEach(featureOrCoordinate => {
			if (featureOrCoordinate instanceof Feature)
				geometries.push(featureOrCoordinate.getGeometry());
			else if (featureOrCoordinate instanceof Geometry)
				geometries.push(featureOrCoordinate);
		});

		this.getView().fit((new GeometryCollection(geometries)).getExtent(), {
			minResolution: minResolution || this.projectionService.getResolutionFromScale(125000, this.getProjection())
		});
	}

	getFeaturesIntersectingGeometry(geometry) {
		const intersectedFeatures = [];

		this.forEachLayerInLayerTree(layer => {
			if (layer.getSource && layer.getSource() instanceof VectorSource)
				intersectedFeatures.push(...this.sourceService.getFeaturesIntersectingGeometry(layer.getSource(), geometry));
		});

		return intersectedFeatures;
	}

	zoomIn() {
		this.getView().setResolution(this.getView().constrainResolution(this.getView().getResolution() / 2));
	}

	zoomOut() {
		this.getView().setResolution(this.getView().constrainResolution(this.getView().getResolution() * 2));
	}

	setupViewportListeners() {
		this.getView().on("change:resolution", e => {
			const projection = this.getProjection(),
				newScale = this.projectionService.getScaleFromResolution(this.getView().getResolution(), projection),
				oldScale = this.projectionService.getScaleFromResolution(e.oldValue, projection);

			this.scaleChangeListeners.trigger(newScale, oldScale);
			this.viewportChangeListeners.trigger();
		});

		this.getView().on("change:center", e => {
			this.centerChangeListeners.trigger(this.getView().getCenter(), e.oldValue);
			this.viewportChangeListeners.trigger();
		});
	}

	getView() {
		return this.ol_.getView();
	}

	getWidth(units) {
		switch (units) {
			case "pixels": return this.ol_.getSize()[0];
			case "degrees": return extent.getWidth(proj.transformExtent(this.getViewportExtent(), this.getProjection(), "EPSG:4326"));
		}
	}

	getSize() {
		return this.ol_.getSize();
	}

	lockInteractions(bool) {
		if (bool && !this.interactions) {
			this.interactions = this.ol_.getInteractions().getArray().slice();
			this.interactions.forEach(interact => this.ol_.removeInteraction(interact));
		}
		if (!bool && this.interactions) {
			this.interactions.forEach(interact => this.ol_.addInteraction(interact));
			this.interactions = null;
		}
	}
	/**
	 * Finds a layer with the given id in the map layer tree
	 * @method seg.map.findLayerById
	 * @param {string} id - Layer id
	 * @returns {seg.layer.Base} SEG layer with the corresponding id.
	*/
	findLayerById(id) {
		let ngolLayer = false;
		const ngolLayers = this.getLayers();

		for (let i = 0; i < ngolLayers.length && !ngolLayer; i++) {
			if (ngolLayers[i].get("id") === id)
				ngolLayer = ngolLayers[i];
			else if (ngolLayers[i].findLayerById)
				ngolLayer = ngolLayers[i].findLayerById(id);
		}

		return ngolLayer;
	}

	getBaseLayers() {
		return this.findLayerById("base_layers");
	}
}
