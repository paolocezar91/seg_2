import { Injectable } from '@angular/core';
import OverviewMap from 'ol/control/OverviewMap';
import Graticule from 'ol/Graticule';
import { Stroke, Text, Fill } from 'ol/style';
import { PreferencesService } from '@core/services/preferences.service';

@Injectable({
	providedIn: 'root'
})
export class SegGraticule {

	ol_: any;

	constructor(private preferencesService: PreferencesService) {
		this.ol_ = new Graticule({
			strokeStyle: new Stroke({
				color: "rgba(0,0,0,0.8)",
				width: 1
			}),
			projection: "EPSG:4326",
			map: null,
			maxLines: 10,
			showLabels: true,
			// baixo
			lonLabelFormatter(lon) {
				// return coordinateFilter(Number(lon.toFixed(2)), this.preferencesService.getWorkspacePreference("coordinateFormat"), "lon");
			},
			lonLabelPosition: 0,
			lonLabelStyle: new Text({
				font: "16px Calibri,sans-serif",
				textBaseline: "bottom",
				fill: new Fill({
					color: "rgba(0,0,0,1)"
				}),
				stroke: new Stroke({
					color: "rgba(255,255,255,1)",
					width: 1
				})
			}),
			//lado
			latLabelFormatter(lat) {
				// return coordinateFilter(Number(lat.toFixed(2)), this.preferencesService.getWorkspacePreference("coordinateFormat"), "lat");
			},
			latLabelPosition: 0.965,
			latLabelStyle: new Text({
				font: "16px Calibri,sans-serif",
				textBaseline: "middle",
				fill: new Fill({
					color: "rgba(0,0,0,1)"
				}),
				stroke: new Stroke({
					color: "rgba(255,255,255,1)",
					width: 1
				})
			})
		});
	}

}