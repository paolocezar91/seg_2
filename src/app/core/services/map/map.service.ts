import { Injectable } from "@angular/core";
import { Map, View } from "ol";
import TileLayer from "ol/layer/Tile";
import Layer from "ol/layer/Layer";
import OSM from "ol/source/OSM";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import GeoJSON from "ol/format/GeoJSON";
import { fromLonLat } from "ol/proj";
import { Fill, Stroke, Style } from "ol/style";
import * as proj from "ol/proj";

import { DrawService } from "@core/services/interactions/draw.service";
import { LocationService } from "@core/services/location.service";
import { PreferencesService } from "@core/services/preferences.service";
import { ProjectionService } from "@core/services/map/projection.service";
import { SegGraticule } from "@core/services/map/SegGraticule";
import { SegMapOverview } from "@core/services/map/SegMapOverview";
import { SegScaleLine } from "@core/services/map/SegScaleLine";
import { SelectionService } from "@core/services/interactions/selection.service";
import { SourceService } from "@core/services/layer/source.service";
import { SegMap } from "@core/services/map/SegMap";
import * as $o from "libs/seg-object";

import { map } from "rxjs/operators";
import { LogService } from "@core/services/log.service";
import { Observable } from "rxjs";

export interface SegMapOptions {
	projection: string;
	scale: string;
	center: Array<number>;
}

@Injectable({
	providedIn: "root"
})
export class MapService {

	private map: any;
	mapOverview: any;
	mapOverviewProjListener: any;
	mapScaleLine: any;
	graticule: any;
	mapReadyListeners: any;
	ENUM_LAYER_VISIBILITY = { FOUND_VISIBLE: 0, FOUND_NOT_VISIBLE: 1, NOT_FOUND: 2 };
	modes = {
		ACQ_FILTER: {
			fn: (feature) => {
				return feature.get("isACQResult");
			},
			enabled: false
		},
		SEARCH_FILTER: {
			fn: (feature) => {
				return feature.get("isSearchResult");
			},
			enabled: false
		},
		SELECT_FILTER: {
			fn: (feature) => {
				return feature.get("wasSelected");
			},
			enabled: false
		},
		TTT_FILTER: {
			fn: (feature) => {
				return feature.get("isVisibleInTTT") !== false;
			},
			enabled: false
		},
		IMPORT_FILTER: {
			fn: (feature) => {
				return feature.get("isImported");
			},
			enabled: false
		}
	};

	constructor(
		private projectionService: ProjectionService,
		private locationService: LocationService,
		private preferencesService: PreferencesService,
		private sourceService: SourceService,
		private logService: LogService
	) {
		this.mapReadyListeners = new $o.EventListenerQueue();

		this.mapOverview = {
			instance: null,
			isVisible: () => {
				return this.mapOverview.instance != null;
			},
			toggle: (value) => {
				if (typeof value === "undefined")
					return;

				if (!value) {
					if (typeof this.mapOverview.instance !== "undefined")
						this.map.removeControl(this.mapOverview.instance);

					this.mapOverview.instance = null;

					if (this.mapOverviewProjListener) {
						this.mapOverviewProjListener();
						this.mapOverviewProjListener = null;
					}
				} else {
					this.mapOverview.instance = new SegMapOverview({
						layers: this.map.ol_.getLayers().item(0).getLayers(),
						view: new View({
							projection: this.map.getProjection()
						})
					});

					this.map.addControl(this.mapOverview.instance);

					this.mapOverviewProjListener = this.map.onProjectionChange(newProjection => {
						const oldView = this.mapOverview.instance.ol_.getOverviewMap().getView(),
							oldProjection = oldView.getProjection();

						this.mapOverview.instance.ol_.getOverviewMap().setView(new View({
							center: proj.fromLonLat(oldView.getCenter(), newProjection),
							projection: newProjection,
							resolution: projectionService.getResolutionFromScale(projectionService.getScaleFromResolution(oldView.getResolution(), oldProjection), newProjection)
						}));
					});
				}

				this.preferencesService.setWorkspacePreference("mapOverview", value);
			}
		};

		this.graticule = {
			instance: null,
			isVisible: () => {
				return this.graticule.instance != null;
			},
			toggle: (value) => {
				if (typeof value === "undefined")
					return;

				if (!value) {
					if (this.graticule.instance != null)
						this.graticule.instance.ol_.setMap(null);

					this.graticule.instance = null;
				} else {
					this.graticule.instance = new SegGraticule(preferencesService);
					this.graticule.instance.ol_.setMap(this.map.ol_);
				}

				this.preferencesService.setWorkspacePreference("mapGraticule", value);
			}
		};

		this.mapScaleLine = {
			instance: null,
			setUnits: value => {
				if (typeof value === "undefined")
					return;

				if (!value) {
					if (this.mapScaleLine.instance)
						this.map.removeControl(this.mapScaleLine);

					this.mapScaleLine.instance = null;
				} else {
					switch (value) {
						case "nm":
							value = "nautical";
							break;
						case "m":
							value = "metric";
							break;
						case "km":
							value = "metric";
							break;
					}

					this.map.removeControl(this.mapScaleLine.instance);

					this.mapScaleLine.instance = new SegScaleLine({
						className: "scale-line",
						target: document.getElementById("scale-line"),
						units: value
					});

					this.map.addControl(this.mapScaleLine.instance);
				}
			}
		};
	}

	onMapReady(callback) {
		return this.mapReadyListeners.register(callback);
	}

	addMapFilterMode(mode, filterFunction) {
		this.modes[mode].fn = filterFunction;
	}

	enableMode(mode) {
		this.modes[mode].enabled = true;
		this.map.forEachLayerInLayerTree(layer => layer.getSource && layer.getSource().changed());
	}

	disableMode(mode) {
		this.modes[mode].enabled = false;
		this.map.forEachLayerInLayerTree(layer => layer.getSource && layer.getSource().changed());
	}

	inMode(mode) {
		return this.modes[mode] && this.modes[mode].enabled;
	}

	getEnabledModes() {
		return Object.values(this.modes).filter(({enabled}) => enabled);
	}

	async getMapOptions(CONFIG: any = {}): Promise<SegMapOptions> {
		// get valid operation for this user
		this.projectionService.load(CONFIG.projections);

		// XXX This is a fix and in a near future can be removed
		const areaUnits = this.preferencesService.getWorkspacePreference("areaUnits");

		if (areaUnits === "" || areaUnits.length > 5)
			this.preferencesService.setWorkspacePreference("areaUnits", "nm2");

		// prevent removed projections from crashing SEG
		let projection = this.preferencesService.getMapPreference("projection");

		if (!this.projectionService.isAvailable(projection))
			projection = CONFIG.default_projection || "EPSG:4326";

		const scale = this.preferencesService.getWorkspacePreference("scale") || 100000;
		let center = [0, 0];

		if (this.preferencesService.getWorkspacePreference("userSpecifiedCenter")) {
			center = this.preferencesService.getWorkspacePreference("center");
		} else {
			try {
				const {coords}: Position = await this.locationService.getCurrentLocation().toPromise();
				center = [coords.longitude, coords.latitude];
			} catch (e) {
				this.logService.error("ERROR_MAP_GET_CURRENT_LOCATION", e);
			}
		}

		return {
			projection,
			scale,
			center
		};
	}

	applyMapPreferences() {
		this.getMap().forEachLayerInLayerTree(layer => {
			const preferences = this.preferencesService.getMapPreference("layers." + layer.get("id"));

			if (preferences)
				Object.entries(preferences).forEach(([key, value]: [string, Array<any>]) => {

					switch (key) {
						case "opacity": layer.ol_.setOpacity(value || 1); break;
						case "selectedLayers": {
							const params = Object.assign(layer.getSource().getParams(), {
								LAYERS: value
							});

							if (params.LAYERS.length)
								layer.getSource().updateParams(params);

							const supportedLayers = layer.getSource().get("supportedLayers");

							if (supportedLayers)
								supportedLayers.forEach(layer_ => {
									if (~value.indexOf(layer_.name))
										layer_.enabled = true;
								});

							break;
						}
						case "filters": {
							// layer.get("filters") && Array.isArray(layer.get("filters")) && layer.get("filters").forEach(filter => {
							// 	const filterPrefs = value[filter.name];

							// 	if(filterPrefs) {
							// 		const {enabled, options} = filterPrefs;

							// 		filter.setActive(typeof enabled === "undefined"?filter.getActive():enabled);

							// 		if(options)
							// 			filter.options.forEach(option => {
							// 				const enabled = options[option.name];

							// 				option.setActive(typeof enabled === "undefined"?option.getActive():enabled);
							// 			});
							// 	}
							// });

							break;
						}
						default: layer.ol_.set(key, value);
					}
				});
		});
	}

	getMap() {
		return this.map;
	}

	createMap(options) {
		this.map = new SegMap(options, this.sourceService, this.projectionService);
		this.map.ol_.on("change:target", () => {
			this.mapReadyListeners.trigger();
			this.mapReadyListeners.clear();
		});
		return this.map;
	}

	preorder() {
		const a = this.getMap().ol_.getLayers().getArray();

		for (let i = 1; i < a.length; i++ ) {
			const index = (a.length) - i;

			if (!a[i].get("order")) {
				a[i].set("order", index);
				this.preferencesService.setMapPreference("layers." + a[i].get("id") + ".order",  index);
			}
		}

		this.preferencesService.setMapPreference("layers." + a[0].get("id") + ".order", 0);
	}

	orderMap(preorder) {
		if (preorder)
			this.preorder(); // this is reseting user preferences order of layers if set before this.applyMapPreferences();

		const layers = this.map.ol_.getLayers().getArray();
		let swapped;

		do {
			swapped = false;

			for (let i = 0; i < layers.length - 1; i++)
				if (layers[i].get("order") > layers[i + 1].get("order")) {
					const temp = layers[i];
					layers[i] = layers[i + 1];
					layers[i + 1] = temp;
					swapped = true;
				}
		} while (swapped);
	}

	isLayerVisible(layer, listLayers) {
		const isLayerVisible_ = (self, layer_, listLayers_) => {
			let ret = self.ENUM_LAYER_VISIBILITY.NOT_FOUND;

			for (let i = 0; i < listLayers_.length; i++) {
				const listLayer = listLayers_[i];

				if (listLayer.getLayers) {
					ret = isLayerVisible_(self, layer_, listLayer.getLayers());

					if (ret === self.ENUM_LAYER_VISIBILITY.FOUND_VISIBLE) {
						if (listLayer.getVisible())
							return self.ENUM_LAYER_VISIBILITY.FOUND_VISIBLE;

						return self.ENUM_LAYER_VISIBILITY.FOUND_NOT_VISIBLE;
					}

					if (ret === self.ENUM_LAYER_VISIBILITY.FOUND_NOT_VISIBLE)
						return self.ENUM_LAYER_VISIBILITY.FOUND_NOT_VISIBLE;
				}

				if (listLayer === layer_) {
					if (layer_.getVisible())
						return self.ENUM_LAYER_VISIBILITY.FOUND_VISIBLE;

					return self.ENUM_LAYER_VISIBILITY.FOUND_NOT_VISIBLE;
				}
			}

			return self.ENUM_LAYER_VISIBILITY.NOT_FOUND;
		};

		if (isLayerVisible_(this, layer, listLayers) === this.ENUM_LAYER_VISIBILITY.FOUND_VISIBLE)
			return true;

		return false;
	}
}
