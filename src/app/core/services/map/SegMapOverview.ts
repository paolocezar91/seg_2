import { Injectable } from "@angular/core";
import OverviewMap from "ol/control/OverviewMap";

export class SegMapOverview {

	ol_: any;

	constructor(options) {
		this.ol_ = new OverviewMap(options);
		this.ol_.element.firstElementChild.onclick = (e) => {
			const view = this.ol_.getMap().getView();
			view.setCenter(this.ol_.getOverviewMap().getEventCoordinate(e));
		};
	}

}
