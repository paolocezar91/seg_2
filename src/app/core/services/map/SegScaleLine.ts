import { Injectable } from "@angular/core";
import ScaleLine from "ol/control/ScaleLine";

export class SegScaleLine {

	ol_: any;

	constructor(options) {
		this.ol_ = new ScaleLine(options);
	}

}
