import { Injectable } from '@angular/core';
import { RequestService } from '@core/services/request/request.service';
import { LogService } from '@core/services/log.service';

@Injectable({
	providedIn: 'root'
})
export class InfoService {

	private segInfo: any;

	constructor(
		private requestService: RequestService,
		private logService: LogService
	) { }

	fetch() {
		return this.requestService.get("info")
			// .subscribe(res => this.segInfo = res, e => this.logService.error("ERROR_INFO_FETCH", e.error + ": " + e.result));
	}

	setInfo(info) {
		this.segInfo = info;
	}

	getInfo() {
		return this.segInfo;
	}
}
