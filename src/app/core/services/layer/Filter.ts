// import { Injectable } from '@angular/core';

// @Injectable({
// 	providedIn: 'root'
// })
// export class FiltersService {

// 	register = (layer, filters) => {
// 		layer.set("filters", filters.map(config => new Filter(config, layer)));

// 		const originalStyle = layer.getStyle();

// 		layer.setStyle(feature => {
// 			const filters = layer.get("filters");

// 			for (let i = 0; i < filters.length; i++) {
// 				const filter = filters[i];

// 				if (filter.inclusive) {
// 					if (filter.active) {
// 						if (!filter.options.some(option => option.active && option.filterFn(feature)))
// 							return feature.set("hidden", true);
// 					} else {
// 						return feature.set("hidden", true);
// 					}
// 				} else {
// 					if (filter.active) {
// 						if (filter.options.some(option => !option.active && option.filterFn(feature)))
// 							return feature.set("hidden", true);
// 					} else {
// 						if (filter.options.some(option => option.filterFn(feature)))
// 							return feature.set("hidden", true);
// 					}
// 				}
// 			}

// 			feature.unset("hidden");

// 			return typeof originalStyle === "function" ? originalStyle(feature) : originalStyle;
// 		});
// 	};

// 	/**
// 	 * Set new filters for a given layer
// 	 * @method seg.Filter.setFilters
// 	 * @param {string|Layer} layer - Layer id
// 	 * @param {Filter[]} filters - Filters for this layer (Vector only). Use in conjunction with {@link seg.ui.layerMenu.fromFilters} to create the menu items.
// 	 */
// 	setFilters = (layer, filters) => {
// 		if (typeof layer === "string")
// 			layer = this.get(layer);

// 		if (filters && filters.length)
// 			layer.set("filters", filters.map(config => new Filter(config, layer)));
// 	};

// 	/**
// 	 * Add new filters for a given layer
// 	 * @method seg.Filter.addFilters
// 	 * @param {string|Layer} layer - Layer id
// 	 * @param {Filter[]} filters - Filters for this layer (Vector only). Use in conjunction with {@link seg.ui.layerMenu.fromFilters} to create the menu items.
// 	 */
// 	addFilters = (layer, filters) => {
// 		let addedFilter = [];

// 		if (typeof layer === "string")
// 			layer = this.get(layer);

// 		if (filters && filters.length){
// 			addedFilter = filters.map(config => new Filter(config, layer));
// 			layer.set("filters", [].concat(addedFilter).concat(layer.get("filters") || []));
// 		}
// 		return addedFilter;
// 	};

// 	removeFilters = (layer, filters_to_remove) => {
// 		if (typeof layer === "string")
// 			layer = this.get(layer);

// 		if (filters_to_remove && filters_to_remove.length) {
// 			layer.set("filters", layer.get("filters").filter(filter => !filters_to_remove.some(ftr => ftr === filter.name)));
// 		}
// 	};

// 	/**
// 	 * Sets new filters options for a given filter
// 	 * @method seg.Filter.setFilterOptions
// 	 * @param {Filter} filter - filter object
// 	 * @param {FilterOptions[]} newFilterOptions - new FilterOptions for this filter
// 	 */
// 	setFilterOptions = (filter, newFilterOptions) => {
// 		if (!filter || // check if filter exists
// 			filter.constructor.name !== "Filter" || // if it is a instance of Filter
// 			!Array.isArray(filter.options) || // if options is an array
// 			!Object.keys(newFilterOptions).length // and if new options array have more than zero objects
// 		)
// 			return;

// 		filter.options = Object.entries(newFilterOptions).map(entry => new FilterOption(filter.layer, ...entry));
// 	};

// 	/**
// 	 * Add new filters options for a given filter
// 	 * @method seg.Filter.addFilterOptions
// 	 * @param {Filter} filter - filter object
// 	 * @param {FilterOptions[]} newFilterOptions - new FilterOptions for this filter
// 	 */
// 	addFilterOptions = (filter, addedFilterOptions) => {
// 		if (!filter || // check if filter exists
// 			filter.constructor.name !== "Filter" || // if it is a instance of Filter
// 			!Array.isArray(filter.options) || // if options is an array
// 			!Object.keys(addedFilterOptions).length // and if new options array have more than zero objects
// 		)
// 			return;

// 		filter.options = filter.options.concat(Object.entries(addedFilterOptions).map(entry => new FilterOption(filter.layer, ...entry)));
// 	};

// 	removeAllFilterOtions = (filter) => {
// 		if (!filter || // check if filter exists
// 			filter.constructor.name !== "Filter" || // if it is a instance of Filter
// 			!Array.isArray(filter.options) // if options is an array
// 		)
// 			return;

// 		filter.options = [];
// 	};

// 	/*
// 	This is used for chained filters creation (filters with subfilters)
// 	Returns one single filter with all combinations possible according to the given configuration
// 	*/
// 	buildMultiLevelFilters(config, parent) {
// 		let filter = {};
// 		const name = (parent) ? parent.name + " " + config.name:config.name;

// 		if (config.fn)
// 			if (parent)
// 				filter[name] = (feature) => {
// 					return parent.fn ? config.fn(feature) && parent.fn(feature) : config.fn(feature);
// 				};
// 			else
// 				filter[name] = (feature) => {
// 					return config.fn(feature);
// 				};

// 		if (config.options)
// 			if (Array.isArray(config.options))
// 				config.options.forEach(option => {
// 					const updatedConfig = Object.assign(config, {name});
// 					filter = Object.assign(filter, this.buildMultiLevelFilters(option, updatedConfig));
// 				});
// 			else
// 				Object.keys(config.options).forEach(key => {
// 					if (config.options.hasOwnProperty(key))
// 						filter[name + " " + key] = ((key) => (feature) => {
// 							return config.fn? config.options[key](feature) && config.fn(feature):config.options[key](feature);
// 						})(key);
// 				});

// 		return filter;
// 	};
	
// }


// /**
//  * @typedef seg.Filter.FilterOptions
//  * @prop {string} name - Filter name
//  * @prop {boolean} inclusive - If true, displays features that match any of the selected options. If false/undefined, hides features that match any of the unselected options
//  * @prop {object} options - Filter rules. The key is the label, and the value is a filter function that receives a feature as an argument and returns a true/false depending on whether the feature matches the rule
//  *
//  * @description
//  * This constructor is __**not**__ used directly. It is invoked by {@link seg.layer.create}.
//  * Instances of `Filter` are available through `seg.layer.Layer#get("filters")`
//  * @class seg.layer.Filter
//  * @param {seg.layer.FilterOptions} config
//  * @param {seg.layer.Layer} layer
// */
// export class Filter {
// 	constructor(config, layer) {
// 		const {
// 			name, inclusive, options, afterSetActive, afterSetActiveOptions
// 		} = config;

// 		Object.assign(this, {
// 			name,
// 			inclusive,
// 			layer,
// 			afterSetActive,
// 			options: Object.entries(options).map(entry => new FilterOption(layer, ...entry, afterSetActiveOptions)),
// 			active: true
// 		});
// 	}
// 	getActive() {
// 		return this.active;
// 	}
// 	isPartial() {
// 		return this.options.some(option => option.getActive()) && this.options.some(option => !option.getActive());
// 	}
// 	allActive(){
// 		return this.options.every(option => option.getActive());
// 	}
// 	setActive(state) {
// 		this.active = state;
// 		this.layer.ol_.changed();
// 	}
// 	toggle() {
// 		this.setActive(!this.getActive);
// 	}
// }

// export class FilterOption {
	// constructor(layer, name, filterFn?, afterSetActive?) {
	// 	Object.assign(this, {
	// 		name,
	// 		filterFn,
	// 		layer,
	// 		afterSetActive
	// 	});
	// 	this.setActive(true);
	// }
	// setActive(state) {
	// 	this.active = state;

	// 	this.layer.getSource().changed();
	// }
	// getActive() {
	// 	return this.active;
	// }
	// toggle(state) {
	// 	if (typeof state === "undefined")
	// 		return this.setActive(!this.getActive());
	// 	this.setActive(state);
	// }
// }