import { SegBaseLayer } from '@core/services/layer/SegBaseLayer';
import * as proj from 'ol/proj';
import { SourceService } from '@core/services/layer/source.service';
import Layer from 'ol/layer/Layer';

/**
	This constructor is __**not**__ used directly. It is invoked by {@link seg.layer.create}.
	@class seg.layer.Layer
	@extends seg.layer.Base
	@prop {external:"ol.layer.Layer"} ol_ - Returns the OpenLayers layer instance associated with this layer
*/

export class SegLayer extends SegBaseLayer {

	private sourceService: SourceService;

	constructor(options, olSourceLayerSubclass) {
		super(options, olSourceLayerSubclass || Layer);

		const segSource = options.source;

		if (typeof segSource === 'object') {
			this.setSource(segSource);
		}
	}

	setSource(source) {
		return;
	}
}

SegLayer.prototype = Object.create(SegBaseLayer.prototype);

Object.assign(SegLayer.prototype, {
	/**
	 *	Alias for `ol_.setSource`
	 *	@method seg.layer.Layer#setSource
	 *	@param {external:"ol.source.Source"} source
	 */
	setSource(source) {
		this.ol_.setSource(source);
	},

	/**
	 *	Alias for `ol_.getSource`
	 *	@method seg.layer.Layer#setSource
	 *	@returns {external:"ol.source.Source"} source
	 */
	getSource() {
		return this.ol_.getSource();
	},

	/**
	 *	Transforms layer from **oldProjection** to **newProjection**
	 *	@method seg.layer.Layer#transform
	 *	@param {external:"ol.ProjectionLike"} newProjection
	 *	@param {external:"ol.ProjectionLike"} oldProjection
	 */
	transform(newProjection, oldProjection) {
		const extent = this.ol_.getExtent();

		if (extent)
			this.ol_.setExtent(proj.transformExtent(extent, newProjection, this.ol_.getSource().getProjection()));

		this.ol_.setSource(
			this.sourceService.transform(
				this.ol_.getSource(),
				newProjection
				// oldProjection || this.ol_.getSource().getProjection()
			)
		);
	}
})