import BaseLayer from 'ol/layer/Base';
import Layer from 'ol/layer/Layer';

export class SegBaseLayer extends BaseLayer {

	ol_: any;
	minScale: any;
	maxScale: any;

	constructor(options, olBaseLayerSubclass = null) {
		super(options);

		Object.assign(this, {
			minScale: options.minScale,
			maxScale: options.maxScale
		});

		delete options.minScale;
		delete options.maxScale;

		this.ol_ = olBaseLayerSubclass !== null ?
			new olBaseLayerSubclass(options) :
			new BaseLayer(options);
	}
}

SegBaseLayer.prototype = Object.create(BaseLayer.prototype);

Object.assign(SegBaseLayer.prototype, {
	getVisible(): boolean {
		return this.ol_.getVisible();
	},
	setVisible(value) {
		this.ol_.setVisible(value);
	},
	refresh(): void {
		this.ol_.changed();
	}
});
