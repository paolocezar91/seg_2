import { Injectable } from "@angular/core";
import * as ol from "ol";
import * as proj from "ol/proj";
import * as _source from "ol/source";
import * as loadingstrategy from "ol/loadingstrategy.js";
import GeoJSON from "ol/format/GeoJSON";
import { LogService } from "@core/services/log.service";
import { RequestService } from "@core/services/request/request.service";
import * as turf from "@turf/turf";
import * as $utils from "libs/seg-utils";
import * as $o from "libs/seg-object";

@Injectable({
	providedIn: "root"
})
export class SourceService {

	sourceOptionsCache = new Map();
	URL_IMAGE_SOURCES = [_source.TileImage, _source.ImageWMS, _source.ImageArcGISRest, _source.ImageMapGuide];
	URL_TILE_IMAGE_SOURCES = [_source.TileImage];
	URL_TILE_WMS_IMAGE_SOURCES = [_source.TileWMS];
	URL_RASTER_SOURCES = [...this.URL_IMAGE_SOURCES, ...this.URL_TILE_IMAGE_SOURCES];

	constructor(
		private requestService: RequestService,
		private logService: LogService
	) {}

	async rasterLoadFunction (img, url) {
		const params = $utils.getParams(url);

		// Specific case for CMAP wms requests, limit screen sizes (see extensibility/layers/base_layers)
		if (!(
			url.includes("CMAP") ||
			url.includes("NauticalChart") ||
			url.includes("imdateGeoServer")
		)) {
			params.WIDTH = Math.min(2000, params.WIDTH);
			params.HEIGHT = Math.min(2000, params.HEIGHT);
		}

		// Specific case for WMS area layers, sometimes no layers are sent on the requests, which results a error in the WS
		// it should render no image, but open layers tries to be smart and, on error, doesn"t change the image source,
		// therefore we use a transparent 1x1px to fake that
		const transparent = (!params.LAYERS || params.LAYERS === "" || (Array.isArray(params.LAYERS) && params.LAYERS.length === 0)),
			// String representing a data64 1x1 transparent img
			transparent1x1Image = () => "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=";


		img.getImage().src = transparent ? transparent1x1Image() : (await window.seg.request.getDataURL($utils.addParamsToURL(url, params), params).toPromise());
	}

	private getLoadFunctionName(type: any): string {
		if (this.URL_TILE_WMS_IMAGE_SOURCES.some(tileImageSource => $o.inherits(type, tileImageSource)))
			return "tileLoadFunction";
		if (this.URL_IMAGE_SOURCES.some(imageSource => $o.inherits(type, imageSource)))
			return "imageLoadFunction";
		if (this.URL_TILE_IMAGE_SOURCES.some(tileImageSource => $o.inherits(type, tileImageSource)))
			return "tileLoadFunction";
		// return this.logService.error("seg.source", "Source has no load function", type);
	}

	private hasLoadFunction(type) {
		return this.URL_RASTER_SOURCES.some(rasterType => $o.inherits(type, rasterType));
	}

	private isVector(type) {
		return $o.inherits(type, _source.Vector);
	}

	private isImage(type) {
		return this.URL_IMAGE_SOURCES.some(imageType => $o.inherits(type, imageType));
	}


	/**
	 *	@method seg.source.create
	 *	@param {seg.source.SourceOptions} options
	 *	@returns {external:"ol.source.Source"} OpenLayers source instance
	*/
	create(options) {
		const {supportedLayers, name, type, projections, projection, source, unique, customLoadFunction} = options,
			constructor = _source[type];

		if (this.hasLoadFunction(constructor))
			options[this.getLoadFunctionName(constructor)] = customLoadFunction || this.rasterLoadFunction;
		else if (this.isVector(constructor))
			options.strategy = loadingstrategy.bbox;

		if (this.isImage(constructor)) {
			options.hidpi = false;
			options.ratio = 1;
		}

		if (projections && !projection)
			options.projection = projections.find(_projection => proj.get(_projection));

		if (source && !(source instanceof _source.Source))
			options.source = this.create(source);

		const createdSource = new constructor(options);

		if (supportedLayers) {
			createdSource.set("supportedLayers", supportedLayers);
			createdSource.set("unique", !!unique);
		}

		if (projections)
			createdSource.set("supportedProjections", projections);

		if (projections)
			this.sourceOptionsCache.set(createdSource, options);

		if (name)
			createdSource.set("name", name);

		return createdSource;
	}

	/**
	@method seg.source.transform
	@param {external:"ol.source.Source"} source
	@param {seg.source} projection
	@returns {external:"ol.source.Source"} OpenLayers source instance
	*/
	transform(source, projection) {
		const supportedProjections = source.get("supportedProjections");

		if (supportedProjections) {
			const sourceOptions = this.sourceOptionsCache.get(source);

			this.sourceOptionsCache.delete(source);

			if (supportedProjections && !~supportedProjections.indexOf(proj.get(projection).getCode()))
				// if layer doesn"t support the projection, find one supported by seg and use that
				projection = supportedProjections.find(_projection => proj.get(_projection));

			const style = source.getStyle && source.getStyle();

			return this.create(Object.assign(sourceOptions, {
				projection,
				style
			}));
		}

		return source;
	}

	findFeature = (source, feature) => source.getFeatures().find(existingFeature => existingFeature === feature);

	getFeaturesIntersectingGeometry(source, geometry) {
		const features = [];

		source.forEachFeatureIntersectingExtent(geometry.getExtent(), feature => {
			const featureGeometry = JSON.parse((new GeoJSON()).writeGeometry(feature.getGeometry())),
				featureGeometryCoordinates = turf.explode(featureGeometry).features.map(_feature => _feature.geometry.coordinates);

			if (featureGeometryCoordinates.some(coordinate => geometry.intersectsCoordinate(coordinate)))
				features.push(feature);
		});

		return features;
	}
}
