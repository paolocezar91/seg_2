import { SegLayer } from '@core/services/layer/SegLayer';
import VectorLayer from 'ol/layer/Vector';

/**
	This constructor is __**not**__ used directly. It is invoked by {@link seg.layer.create}.
	@class seg.layer.Vector
	@extends seg.layer.Layer
	@prop {external:"ol.layer.Vector"} ol_ - Returns the OpenLayers layer instance associated with this layer
*/
export class SegVectorLayer extends SegLayer {

	constructor(options, olVectorLayerSubclass) {
		super(options, olVectorLayerSubclass || VectorLayer);
	}
}

SegVectorLayer.prototype = Object.create(SegLayer.prototype);

Object.assign(SegVectorLayer.prototype, {
	/**
	 *	Alias for `ol_.getStyle`
	 *	@method seg.layer.Vector#getStyle
	 *	@returns {external:"ol.style.Style"|external:"ol.style.Style"[]|external:"ol.StyleFunction"}
	 */
	getStyle() {
		return this.ol_.getStyle();
	},
	/**
	 *	Alias for `ol_.setStyle`
	 *	@method seg.layer.Vector#setStyle
	 *	@param {external:"ol.style.Style"|external:"ol.style.Style"[]|external:"ol.StyleFunction"}
	 */
	setStyle(style) {
		this.ol_.setStyle(style);
	}
})