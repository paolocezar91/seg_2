import { TestBed } from '@angular/core/testing';

import { LayerFilterService } from './layer-filter.service';

describe('LayerFilterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LayerFilterService = TestBed.get(LayerFilterService);
    expect(service).toBeTruthy();
  });
});
