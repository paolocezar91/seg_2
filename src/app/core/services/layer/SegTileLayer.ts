import { SegLayer } from '@core/services/layer/SegLayer';
import { Tile } from 'ol/layer';

/**
This constructor is __**not**__ used directly. It is invoked by {@link seg.layer.create}.
@class seg.layer.Tile
@extends seg.layer.Layer
@prop {external:"ol.layer.Tile"} ol_ - Returns the OpenLayers layer instance associated with this layer
*/
export class SegTileLayer extends SegLayer {
	
	constructor(options, olTileLayerSubclass) {
		super(options, olTileLayerSubclass || Tile);
	}

}

SegTileLayer.prototype = Object.create(SegLayer.prototype);