import { TooltipService } from "@main/services/tooltip.service";
import { Injectable } from "@angular/core";
import { map, catchError } from "rxjs/operators";

import * as _source from "ol/source";
import WMSCapabilities from "ol/format/WMSCapabilities";

import { SegImageLayer } from "@core/services/layer/SegImageLayer";
import { SegTileLayer } from "@core/services/layer/SegTileLayer";
import { SegLayerGroup } from "@core/services/layer/SegLayerGroup";
import { SegVectorLayer } from "@core/services/layer/SegVectorLayer";
import { MapService } from "@core/services/map/map.service";
import { LogService } from "@core/services/log.service";
import { RequestService } from "@core/services/request/request.service";
import { SourceService } from "@core/services/layer/source.service";
import { Observable } from "rxjs";


@Injectable({
	providedIn: "root"
})
export class LayerService {

	segLayer = {
		Group: SegLayerGroup,
		Image: SegImageLayer,
		Tile: SegTileLayer,
		Vector: SegVectorLayer,
	}

	usedIds = [];
	tooltip;

	constructor(
		private mapService: MapService,
		private logService: LogService,
		private requestService: RequestService,
		private sourceService: SourceService,
		private tooltipService: TooltipService,
	) {
	}

	/**
	 * @description
	 * Enhance a layer configuration.
	 *
	 * This is used for WMS layers that get their children from GetCapabilities
	 *
	 * @method seg.layer.enhance
	 * @param {seg.layer.LayerOptions} options
	 *
	 * @returns {Promise<seg.layer.LayerOptions>} enhanced options
	 */
	enhance(options) {
		return Observable.create(obs => {

			const {source} = options;

			if (
				source &&
				source.type === "ImageWMS" &&
				!source.supportedLayers &&
				(!source.params || !source.params.LAYERS)
			) {
				this.getCapabilities(source.url, source.params.VERSION)
					.subscribe((data) => {
						const {Capability, version} = data;

						if (!(Capability && version))
							obs.next(options);

						source.projections = Capability.Layer.CRS.filter(proj => !~proj.indexOf(","));

						source.supportedLayers = Capability.Layer.Layer.map(({Name, Title}) => ({
							alias: Title.replace(/_/g, " "),
							name: Name
						}));

						if (source.only)
							source.supportedLayers = source.supportedLayers.filter(layer => !!~ source.only.findIndex(only => only === layer.name));

						// const selectedLayers = seg.preferences.map.get("layers." + options.id + ".selectedLayers") || [];

						// source.params = Object.assign({}, source.params, {
						// 	VERSION: version,
						// 	LAYERS: source.supportedLayers.reduce((layers, layer) => selectedLayers.some(selectedLayer => selectedLayer === layer.name) ? layers.concat(layer.name) : layers, []).join(",")
						// });
						
						obs.next(Object.assign(options, {source}));
					}, e => obs.error(e));
			} else
				obs.next(options);
		}, e => console.log(e));
	}

	/**
	 * @description
	 * Create a layer that is managed by SEG.
	 *
	 * @method seg.layer.create
	 * @param {seg.layer.LayerOptions} options
	 *
	 * @returns {seg.layer.Layer} SEG layer of the specified **type**
	 */
	create(options: any = {}, ancestors = []) {
		const {
			tooltip, /*label, tooltip, commandInfo, search, timerange, filters, */id
		} = options;

		let type = options.type;

		if (!id)
			throw this.logService.error("seg.layer.create", "You must specify a layer id", options);
		else if (this.usedIds.indexOf(id) !== -1)
			throw this.logService.error("seg.layer.create", "A layer with the id \"" + id + "\" already exists", options);
		else this.usedIds.push(id);

		let children;

		if (Array.isArray(options.layers)) {
			type = "Group",
			children = options.layers;

			delete options.layers;
		}

		if (options.source)
			options.source = this.sourceService.create(options.source);

		const layer = new this.segLayer[type](options);
		// need to add the singleton sourceService to layer instance
		layer.sourceService = this.sourceService;

		ancestors.push(layer);

		if (children)
			layer.addLayers(children.map(child => this.create(child, ancestors)));

		if (options.source instanceof _source.Vector) {
			const source = options.source;
			source.on("addfeature", ({feature}) => feature.set("seg_type", layer.get("id")));
		}

		// if (options.source instanceof _source.ImageWMS || options.source instanceof _source.Image)
			// layer.ol_.setVisible(seg.preferences.map.get("layers." + options.id + ".visible"));

		// if (label)
		// 	labelService.register(layer, label, ancestors);
		if (tooltip) {
			this.tooltipService.register(layer, tooltip);
		}
		// if (commandInfo)
		// 	commandInfoService.register(layer, commandInfo);
		// if (search)
		// 	searchService.register(layer, search);
		// if (timerange)
		// 	timesliderService.register(layer, timerange);
		// if (filters) {
		// 	filterService.register(layer, filters);
		// }

		if (layer.getStyle) {
			const originalStyle = layer.getStyle();
			layer.setStyle(feature => {
				const enabledModes = this.mapService.getEnabledModes();
				if(!enabledModes.length || enabledModes.every(mode => mode.fn(feature)))
					return typeof originalStyle === "function" ? originalStyle(feature) : originalStyle;
			});
		}

		return layer;
	};

	/**
	 * Utility method implemented from {@link external:"ol.format.WMSCapabilities"}
	 * @method seg.layer.getCapabilities
	 * @param {string} url
	 * @returns {object} capabilities
	 */
	getCapabilities(url, VERSION = "1.3.0"){
		return this.requestService.get(url, {
			params: {
				REQUEST: "GetCapabilities",
				SERVICE: "WMS",
				VERSION
			},
			responseType: "text",
			timeout: 60 * 1000 // 30 ms
		}).pipe(
			map(res => {
				if (
					new DOMParser().parseFromString(res.toString(),"text/xml").getElementsByTagName("WMS_Capabilities").length ||
					new DOMParser().parseFromString(res.toString(),"text/xml").getElementsByTagName("GetCapabilities").length
				)
					return (new WMSCapabilities()).read(res);
				return {};
			}), 
			catchError((e, caught) => {
				this.logService.error("ERROR_LAYER_GET_CAPABILITIES", e.error + ": " + e.result);
				return caught;
			})
		);
	}

	findLayerInLayerCapabilities(layerName, capability) {
		if (capability.Name === layerName)
			return capability;
		else if (Array.isArray(capability.Layer))
			for (let i = 0; i < capability.Layer.length; i++) {
				const foundCapability = this.findLayerInLayerCapabilities(layerName, capability.Layer[i]);

				if (foundCapability)
					return foundCapability;
			}
	};

	/**
	 * @method seg.layer.findLayerInCapabilities
	 * @param {string} layer - String used in the LAYERS parameter for WMS queries
	 * @param {object} capabilities - Object returned from {@link seg.layer.getCapabilities}
	 * @returns {object} - layerCapabilities - Properties for a specific layer found in a GetCapabilities object
	 */
	findLayerInCapabilities(layer, capability) {
		if (typeof layer.source.params.LAYERS === "undefined"){
			this.logService.error("seg.layer", "layer has no params.LAYERS defined", layer);
			return;
		}

		if (typeof capability.Capability === "undefined"){
			this.logService.error("seg.layer", "capability is not defined", layer);
			return;
		}

		return this.findLayerInLayerCapabilities(layer.source.params.LAYERS, capability.Capability.Layer);
	};

	/**
	 * Alias for {@link seg.map.findLayerById}
	 * @method seg.layer.get
	 * @param {string} id - Layer id
	 * @returns {Layer} SEG layer with the corresponding id.
	 */
	get(id){
		return this.mapService.getMap().findLayerById(id);
	}
}
