import { SegLayer } from '@core/services/layer/SegLayer';
import { Image } from 'ol/layer';

/**
This constructor is __**not**__ used directly. It is invoked by {@link seg.layer.create}.
@class seg.layer.Image
@extends seg.layer.Layer
@prop {external:"ol.layer.Image"} ol_ - Returns the OpenLayers layer instance associated with this layer
*/
export class SegImageLayer extends SegLayer {

	constructor(options, olImageLayerSubclass) {
		super(options, olImageLayerSubclass || Image);
	}
}

SegImageLayer.prototype = Object.create(SegLayer.prototype);

Object.assign(SegImageLayer.prototype, {
	getSource() {
		return this.ol_.getSource();
	}
})
