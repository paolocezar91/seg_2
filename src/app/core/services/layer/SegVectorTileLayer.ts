// /**
// This constructor is __**not**__ used directly. It is invoked by {@link seg.layer.create}.
// @class seg.layer.VectorTile
// @extends seg.layer.Vector
// @prop {external:"ol.layer.VectorTile"} ol_ - Returns the OpenLayers layer instance associated with this layer
// */
// angular.module("seg.layer")
// 	.factory("seg.layer.VectorTile", ["seg.layer.Vector", ngolVectorLayer => {
// 		function ngolVectorTileLayer(options) {
// 			ngolVectorLayer.call(this, options, ol.layer.VectorTile);
// 		}

// 		ngolVectorTileLayer.prototype = Object.create(ngolVectorLayer.prototype);

// 		return ngolVectorTileLayer;
// 	}]);

