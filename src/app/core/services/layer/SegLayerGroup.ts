import { SegBaseLayer } from '@core/services/layer/SegBaseLayer';
import { Collection } from 'ol';
import LayerGroup from 'ol/layer/Group';
import * as $o from 'libs/seg-object';

/**
	This constructor is __**not**__ used directly. It is invoked by {@link seg.layer.create}.
	@class seg.layer.Group
	@extends seg.layer.Base
	@prop {external:"ol.layer.Group"} ol_ - Returns the OpenLayers layer instance associated with this layer
*/
export class SegLayerGroup extends SegBaseLayer {

	layerAddedListeners: any;
	layers: Array<any>;

	constructor(options) {
		super(options, LayerGroup);

		this.layerAddedListeners = new $o.EventListenerQueue;
		this.layers = [];

		const segLayers = options.layers;
		delete options.layers;
	}
}

SegLayerGroup.prototype = Object.create(SegBaseLayer.prototype);

Object.assign(SegLayerGroup.prototype, {
	/**
	@method seg.layer.Group#getLayers
	@returns {seg.layer.Base[]}
	*/
	getLayers() {
		return this.layers;
	},
	/**
	@method seg.layer.Group#addLayers
	@param {seg.layer.Base[]} layers
	*/
	addLayers(segLayers) {
		segLayers = [].concat(segLayers);

		this.layers = this.layers.concat(segLayers);

		this.updateOlLayerTree(this);

		this.layerAddedListeners.trigger(segLayers);
	},
	/**
	@method seg.layer.Group#addLayers
	@param {seg.layer.Base} layer
	*/
	addLayer(segLayer) {
		this.addLayers(segLayer);
	},
	/**
	@method seg.layer.Group#removeLayer
	@param {seg.layer.Base} layer
	@returns {boolean}
	*/
	removeLayer(layerToRemove) {
		return this.layers.some((layer, i) => {
			if(layer === layerToRemove) {
				this.layers.splice(i, 1);

				this.updateOlLayerTree(this);

				return true;
			}

			if(layer.removeLayer)
				return layer.removeLayer(layerToRemove);
		});
	},
	/**
	Transforms all child layers from **oldProjection** to **newProjection**
	@method seg.layer.Group#transform
	@param {external:"ol.ProjectionLike"} newProjection
	@param {external:"ol.ProjectionLike"} oldProjection
	*/
	transform(newProj, oldProj) {
		this.forEachLayerInLayerTree(layer => layer.transform(newProj, oldProj));
	},
	/**
	Invokes **callback** for each of the layer's descendants, passing that layer as an argument
	@method seg.layer.Group#forEachLayerInLayerTree
	@param {function} callback
	*/
	forEachLayerInLayerTree(callback) {
		this.getLayers().forEach(layer => {
			callback(layer);

			if(layer.forEachLayerInLayerTree)
				layer.forEachLayerInLayerTree(callback);
		});
	},
	/**
	Invokes **callback** whenever a new layer is added
	@method seg.layer.Group#onLayerAdded
	@param {function} callback
	@returns {function} Deregistration callback
	*/
	onLayerAdded(callback) {
		return this.layerAddedListeners.register(callback);
	},
	/**
	Finds a layer with the given id in the layer's descendants
	@method seg.layer.Group#findLayerById
	@param {string} id - Layer id
	@returns {Layer} SEG layer with the corresponding id.
	*/
	findLayerById(id) {
		let segLayer = false;
		const segLayers = this.getLayers();

		for(let i = 0; i < segLayers.length && !segLayer; i++) {
			if(segLayers[i].get("id") === id)
				segLayer = segLayers[i];
			else if(segLayers[i].findLayerById)
				segLayer = segLayers[i].findLayerById(id);
		}

		return segLayer;
	},

	updateOlLayerTree(layer) {
		layer.ol_.setLayers(new Collection(layer.layers.map(segLayer => segLayer.ol_)));
	}
});