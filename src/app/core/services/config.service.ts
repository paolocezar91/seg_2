import { Injectable } from "@angular/core";
import * as $o from "libs/seg-object";

@Injectable({
  providedIn: "root"
})
export class ConfigService {

	CONFIG;

	constructor() {	}

	setConfig(config) {
		this.CONFIG = config;
	}

	getValue(path) {
		return $o.getValue(this.CONFIG, path);
	}
}
