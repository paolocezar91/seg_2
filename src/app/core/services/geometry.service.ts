import { Injectable } from '@angular/core';
import { MapService } from "@core/services/map/map.service";
import * as turf from "@turf/turf";
import * as ol from "ol";
import * as geom from "ol/geom";
import * as GeometryCollection from "ol/geom/GeometryCollection";
import * as formatOL from "ol/format";
import * as proj from "ol/proj";
import * as extent from "ol/extent";

@Injectable({
  providedIn: 'root'
})
export class GeometryService {

  constructor(
		private mapService: MapService
  ) { 
    Object.assign(this, geom);
  }

  fromExtent = geom.Polygon.fromExtent;

  createHolesInGeometry(features){
    features.forEach(hole => {
      hole = hole.getGeometry();
      features.forEach(area => {
        area = area.getGeometry();
        if (hole.ol_uid !== area.ol_uid){
          if(turf.booleanContains(turf.polygon(area.getCoordinates()), turf.polygon(hole.getCoordinates()))){
            const newCoordinates = area.getCoordinates().concat(hole.getCoordinates());
            area.setCoordinates(newCoordinates);
          }
        }
      });
    });
  }
  wraps(geometry){
    if(typeof geometry.forEachSegment !== "function")
      throw new Error("seg.nggeometry.wraps: The geometry doesn\"t have a forEachSegment method");

    return geometry.forEachSegment(this.segmentWraps);
  }
  splitAtAntimeridian(geometry){
    if(typeof geometry.forEachSegment !== "function")
      throw new Error("seg.nggeometry.splitAtAntimeridian: The geometry doesn\"t have a forEachSegment method");

    const multiLineString = new geom.MultiLineString([[[0,0], [0,0]]]);

    let lineString = new geom.LineString();

    lineString.appendCoordinate(geometry.getFirstCoordinate());

    geometry.forEachSegment((start, end) => {
      const wraps = this.segmentWraps(start, end);

      if(wraps) {
        const normalizedStart = [Math.abs(start[0] + (wraps < 0?360:0)), start[1]],
          normalizedEnd = [Math.abs(end[0] + (wraps > 0?360:0)), end[1]];
        //line ends at -180 if it wraps west, 180 if it wraps east
        const midpoint = [wraps*180, this.interpolateYatX(normalizedStart, normalizedEnd, 180)];
        lineString.appendCoordinate(midpoint);
        multiLineString.appendLineString(lineString);
        lineString = new geom.LineString();
        //line starts at 180 if it wraps west, -180 if it wraps east
        midpoint[0] *= -1
        lineString.appendCoordinate(midpoint);
      }

      lineString.appendCoordinate(end);
    });

    multiLineString.appendLineString(lineString);

    return multiLineString;
  }
  parse(geometry, format, options){
    const self = this;
    let formatter;

    if(typeof format[format] === "function"){
      formatter = new formatOL[format];
    }

    if(Array.isArray(geometry)) {
      const geometryCollection = new GeometryCollection();

      geometryCollection.setGeometries(geometry.map((geometry) => {
        return self.parse(geometry, format, options);
      }));

      return geometryCollection;
    }

    return formatter.readGeometry(geometry, options);
  }
  serialize(geometry, format, options){
    const self = this,
      formatter = new formatOL[format];

    if(Array.isArray(geometry))
      return {
        type: "GeometryCollection",
        geometries: geometry.map((geometry) => {
          return self.serialize(geometry, format, options);
        })
      };

    return formatter.writeGeometryObject(geometry, options);
  }
  getWidth(extentOrGeometry){
    if(extentOrGeometry instanceof geom.Geometry)
      extentOrGeometry = extentOrGeometry.getExtent();

    if(!Array.isArray(extentOrGeometry))
      throw new Error("seg.geometry.getWidth: Invalid extent or geometry!");

    return extent.getWidth(extentOrGeometry);
  }
  getHeight(extentOrGeometry){
    if(extentOrGeometry instanceof geom.Geometry)
      extentOrGeometry = extentOrGeometry.getExtent();

    if(!Array.isArray(extentOrGeometry))
      throw new Error("seg.geometry.getHeight: Invalid extent or geometry!");

    return extent.getHeight(extentOrGeometry);
  }
  aoiToExtent(aois, opt){
    const options = Object.assign({
      add_x: 0,
      add_y: 0
    }, opt);

    let ret = "array";

    if(!Array.isArray(aois)){
      aois = [aois];
      ret = "object";
    }

    if (aois.length) {
      const bbox = {
        minx: Number.MAX_VALUE,
        maxx: -Number.MAX_VALUE,
        miny: Number.MAX_VALUE,
        maxy: -Number.MAX_VALUE
      };

      aois = aois.map(aoi => {
        let _extent = proj.transformExtent(aoi.getGeometry().getExtent(), this.mapService.getMap().getProjection(), "EPSG:4326");
        const center = extent.getCenter(_extent);

        _extent = [_extent[2] - _extent[0], _extent[3] - _extent[1]];

        bbox.minx = Math.min(bbox.minx, (center[0] - _extent[0]/2) - options.add_x);
        bbox.maxx = Math.max(bbox.maxx, (center[0] + _extent[0]/2) + options.add_x);
        bbox.miny = Math.min(bbox.miny, (center[1] - _extent[1]/2) - options.add_y);
        bbox.maxy = Math.max(bbox.maxy, (center[1] + _extent[1]/2) + options.add_y);

        return bbox;
      });

      if(ret === "array")
        return aois;
      else if(ret === "object")
        return aois[0];

    }
  }
  rectangleToExtent(aois, opt){
    const options = Object.assign({
      add_x: 0,
      add_y: 0
    }, opt);

    if(!Array.isArray(aois)){
      aois = [aois];
    }

    if (aois.length) {
      const bbox = {
        minx: Number.MAX_VALUE,
        maxx: -Number.MAX_VALUE,
        miny: Number.MAX_VALUE,
        maxy: -Number.MAX_VALUE
      };

      return aois.map(aoi => {
        let _extent = proj.transformExtent(aoi.getGeometry().getExtent(), this.mapService.getMap().getProjection(), "EPSG:4326");
        const center = extent.getCenter(_extent);

        _extent = [_extent[2] - _extent[0], _extent[3] - _extent[1]];

        bbox.minx = Math.min(bbox.minx, (center[0] - _extent[0]/2) - options.add_x);
        bbox.maxx = Math.max(bbox.maxx, (center[0] + _extent[0]/2) + options.add_x);
        bbox.miny = Math.min(bbox.miny, (center[1] - _extent[1]/2) - options.add_y);
        bbox.maxy = Math.max(bbox.maxy, (center[1] + _extent[1]/2) + options.add_y);

        return bbox;
      });
    }
  }
  segmentWraps(start, end){
		//wraps west
		if (start[0] <= -150 && end[0] >= 150 && start[0] < end[0])
			return -1;
		//wraps east
		if (end[0] <= -150 && start[0] >= 150 && end[0] < start[0])
			return 1;

		return 0;
	}

	interpolateYatX(start, end, x){
		//return      start[1] + (end[1] - start[1]) * (x - start[0]) / (end[0] - start[0]);
		return (start[1] * (end[0] - x) + end[1] * (x - start[0])) / (end[0] - start[0]);
	}
}
