import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class CleanablesService {

	constructor() { }

	cleanables = {};

	add(label, callback) {
		this.cleanables[label] = () => {
			callback();
			delete this.cleanables[label];
		};
	}

	remove(label) {
		delete this.cleanables[label];
	}

	call(label) {
		return typeof this.cleanables[label] === "function" && this.cleanables[label]();
	}

	get(label) {
		if (this.cleanables[label])
			return this.cleanables[label];
	}
}
