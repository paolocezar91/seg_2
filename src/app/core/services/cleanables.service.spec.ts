import { TestBed } from '@angular/core/testing';

import { CleanablesService } from './cleanables.service';

describe('CleanablesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CleanablesService = TestBed.get(CleanablesService);
    expect(service).toBeTruthy();
  });
});
