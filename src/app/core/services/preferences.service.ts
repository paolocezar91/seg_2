import { Injectable } from '@angular/core';
import { RequestService } from '@core/services/request/request.service';
import { UserService } from '@core/services/user.service';
import { LogService } from '@core/services/log.service';
import * as $o from 'libs/seg-object';
import { map, first, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';


const DEFAULT_WORKSPACE = {
		coordinateFormat: 'DMS',
		distanceUnits: 'nm',
		areaUnits: 'nm2',
		timeFormat: 'Local',
		timezone: 0,
		positionTimestampFormat: 'Absolute timestamp',
		projection: 'EPSG:3395',
		scale: '100000',
		userSpecifiedCenter: false,
		center: [0, 0],
		show: {
			magnifyAndCenterButtons: true
		}
	},
	DEFAULT_MAP_PREFERENCES = {
		layers: {}
	},
	DEFAULT_OPERATION_PREFERENCES = {
		workspace: 'Default',
		savedWorkspaces: {
			Default: DEFAULT_WORKSPACE
		},
		mapPreferences: 'Default',
		savedMapPreferences: {
			Default: DEFAULT_MAP_PREFERENCES
		}
	};

@Injectable({
	providedIn: 'root'
})
export class PreferencesService {

	preferences = {
		global: {},
		operation: DEFAULT_OPERATION_PREFERENCES,
		session: {}
	};
	preferenceChangeListeners = {};
	pendingUpdates = {};
	pendingPromises = {};
	operationName;
	memoizedWorkspacePref;
	memoizedMapPref;
	memoizedSessionPref;

	constructor(
  		private requestService: RequestService,
  		private userService: UserService,
  		private logService: LogService
	) { }

	//global preferences
	loadGlobalPreferences(): Observable<any> {
		return this.loadPreferences('global')
			.pipe(map((res: any) => res && res.result));
	}

	setGlobalPreferences(_global) {
		this.preferences.global = _global;
	}

	setGlobalPreference(path, value) {
		return this.setPreference('global.'+path, value);
	}

	getPaths(obj) {
		return Object.entries(obj).reduce((paths, entry) => {
			const [key, value] = entry;

			if(value === null || Array.isArray(value) || typeof value !== 'object')
				paths[key] = value;
			else Object.entries(this.getPaths(value)).forEach(entry => {
				const [path, value] = entry;

				paths[key+'.'+path] = value;
			});

			return paths;
		}, {});
	}

	//operation preferences
	loadOperationPreferences(operation) {
		return Observable.create((obs) => {
			this.operationName = operation.id;

			//XXX quick dirty fix to allow modules being loaded after map
			if(Array.isArray(operation.modules))
				operation.modules.forEach(module => {
					if(module.preferences)
						Object.entries(module.preferences).forEach(entry => {
							const [path, value] = entry;
							$o.setValue(DEFAULT_WORKSPACE, path, value);
						});
				});

			const paths = this.loadPreferences(this.operationName)
				.pipe(
					first(),
					catchError(e => of(this.logService.error('ERROR_LOAD_OPERATION_PREFERENCES', e.error + ': ' + e.result)))
				).subscribe((paths) => {
					if(paths){
						Object.entries(this.getPaths(paths.result)).forEach(entry => {
							const [path, value] = entry;
							$o.setValue(DEFAULT_OPERATION_PREFERENCES, path, value);
						});
						obs.next(true);
					} else {
						obs.next(false);
					}
				})
		})
	}

	getOperationPreference(path){
		return this.getPreference('operation.'+path);
	}

	setOperationPreference(path, value) {
		return this.setPreference('operation.'+path, value);
	}

	unsetOperationPreference(path){
		return this.unsetPreference('operation.'+path);
	}

	//workspace preferences
	getWorkspacePreferences() {
		return this.getPreference('operation.savedWorkspaces');
	}

	getWorkspacePreference(path) {
		if(this.memoizedWorkspacePref && this.memoizedWorkspacePref.path === path)
			return this.memoizedWorkspacePref.value;

		const value = this.getPreference('operation.savedWorkspaces.' + this.preferences.operation.workspace + '.' + path);

		this.memoizedWorkspacePref = {
			path,
			value
		};

		return value;
	}

	setWorkspacePreference(path, value) {
		return this.setPreference('operation.savedWorkspaces.'+this.preferences.operation.workspace+'.'+path, value);
	}

	unsetWorkspacePreference(path) {
		return this.unsetPreference('operation.savedWorkspaces.'+this.preferences.operation.workspace+'.'+path);
	}

	onWorkspacePreferenceChange(path, callback) {
		return this.onPreferenceChange('operation.savedWorkspaces.'+this.preferences.operation.workspace+'.'+path, callback);
	}

	//map preferences
	getMapPreference(path) {
		if(this.memoizedMapPref && this.memoizedMapPref.path === path)
			return this.memoizedMapPref.value;

		const value = this.getPreference('operation.savedMapPreferences.' + this.preferences.operation.mapPreferences + (path ? ('.' + path) : ''));

		this.memoizedMapPref = {
			path,
			value
		};

		return value;
	}

	getSavedMapPreferences(path) {
		return this.getPreference('operation.savedMapPreferences'+(path?'.'+path:''));
	}

	removeSavedMapPreference(path) {
		delete this.preferences.operation.savedMapPreferences[path];

		return this.setPreference('operation.savedMapPreferences', this.preferences.operation.savedMapPreferences);
	}

	setMapPreference(path, value) {
		this.setPreference('operation.savedMapPreferences.'+this.preferences.operation.mapPreferences+(path?'.'+path:''), value);
	}

	setMapPreferences(path, value) {
		this.setPreference('operation.savedMapPreferences.'+path, value);
	}

	//session preferences
	getSessionPreference(path) {
		if(this.memoizedSessionPref && this.memoizedSessionPref.path === path)
			return this.memoizedSessionPref.value;

		const value = this.getPreference('session.'+path);

		this.memoizedSessionPref = {
			path,
			value
		};

		return value;
	}

	setSessionPreference(path, value) {
		return this.setPreference('session.' + path, value);
	}

	onSessionPreferenceChange(path, callback) {
		return this.onPreferenceChange('session.' + path, callback);
	}

	loadPreferences(path): Observable<any> {
		return this.requestService.get('v1/userdetails/preferences/'+ this.userService.getCurrentUser().username+'/'+path);
	}

	public getPreference(path) {
		const preference = $o.getValue(this.preferences, path);

		if (typeof preference === 'object'){
			if (preference){
				if (preference.constructor.name === 'Array')
					return preference.slice(0);
				return Object.assign({}, preference);
			}
		}

		return preference;
	}

	public setPreference(path, newValue) {
		const oldValue = $o.getValue(this.preferences, path);

		// returns Observable somehow
		if(oldValue === newValue)
			return new Promise(() => null);

		this.memoizedWorkspacePref = this.memoizedMapPref = this.memoizedSessionPref = null;

		if(typeof newValue !== 'undefined')
			$o.setValue(this.preferences, path, newValue);
		else $o.unsetValue(this.preferences, path);

		if (this.preferenceChangeListeners[path])
			this.preferenceChangeListeners[path].trigger(newValue, oldValue);

		const arrayPath = path.split('.');

		path = arrayPath[0];

		if (path === 'session')
			return;
		if (path === 'operation')
			path = this.operationName;

		if (this.pendingUpdates[path])
			clearTimeout(this.pendingUpdates[path]);
		// else this.pendingPromises[path] = $q.defer();

		this.pendingUpdates[path] = setTimeout(() => {
			if (this.pendingPromises[path]) {
				return this.pendingPromises[path] = this.requestService.put('v1/userdetails/preferences/' + this.userService.getCurrentUser().username+'/'+path, {
					data: this.preferences[arrayPath[0]]
				}).toPromise().then(res => {
					delete this.pendingUpdates[path];
					delete this.pendingPromises[path];

					return res;
				}, e => this.logService.error('ERROR_PREFERENCES_PENDING_PROMISES', e.error + ': ' + e.result));
			}
		}, 100);

		return this.pendingPromises[path];
	}

	setValue(object, path: string, value) {
		const pathArray = path.split('.');

		// run over path
		while (pathArray.length) {
			// get current key
			const currentKey = pathArray.shift();

			/* if that was the last key,
			 * it means we got to our destination,
			 * so set value
			 */
			if (!pathArray.length)
				object[currentKey] = value;
			else {// else move down the tree
				// or if node doesn't exist, create it first
				if (typeof object[currentKey] === 'undefined')
					object[currentKey] = {};

				object = object[currentKey];
			}
		}
	}

	unsetValue(object, path) {
		const parentPath = path.substr(0, path.lastIndexOf('.'));
		const keyToDelete = path.substr(path.lastIndexOf('.')+1);

		const parentNode = this.getValue(object, parentPath);

		delete parentNode[keyToDelete];
	}

	getValue(object, path) {
		this.traverse(object, path, (currentNode, currentKey/*, currentPath*/) => {
			object = object[currentKey];
		});

		return object;
	}

	traverse(object, path, callback) {
		path = path.split('.');

		return path.every((currentKey, i) => {
			if(typeof object === 'undefined')
				return false;

			object = object[currentKey];

			callback(object, currentKey, path.slice(0, i+1).join('.'));

			return true;
		});
	}

	unsetPreference(path) {
		return this.setPreference(path, undefined);
	}

	onPreferenceChange(path, callback) {
		if(typeof this.preferenceChangeListeners[path] === 'undefined')
			this.preferenceChangeListeners[path] = new $o.EventListenerQueue();

		return this.preferenceChangeListeners[path].register(callback);
	}
}
