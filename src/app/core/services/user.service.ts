import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

	private currentUser: any;
 
	constructor() { }

	getCurrentUser(): any {
		return this.currentUser;
	}

	setCurrentUser(user) {
		this.currentUser = user;
	}
}
