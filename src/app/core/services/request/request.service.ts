import { Injectable } from "@angular/core";
import { UserService } from "@core/services/user.service";
import { LogService } from "@core/services/log.service";
import { InterceptorService } from "@core/services/request/interceptor.service";
import { HttpClient, HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { Observable, throwError, of } from "rxjs";
import { catchError, tap, map } from "rxjs/operators";
import * as $utils from "libs/seg-utils";


@Injectable({
	providedIn: "root"
})
export class RequestService {

	REQUEST_WORKER_URL = "assets/requestWorker.js";
	// mime: any = require("mime");
	project: any;

	constructor(
		private http: HttpClient,
		private userService: UserService,
		private logService: LogService,
		private interceptorService: InterceptorService
	) { }

	setProject(project_) {
		this.project = project_;
	}

	get(url, config: any = {}) {
		return this.request(url, Object.assign({
			method: "get"
		}, config));
	}

	post(url, config: any = {}) {
		return this.request(url, Object.assign({
			method: "post"
		}, config));
	}

	put(url, config: any = {}) {
		return this.request(url, Object.assign({
			method: "put"
		}, config));
	}

	remove(url, config) {
		return this.request(url, Object.assign({
			method: "delete"
		}, config));
	}

	// proto(url, config: any = {}){
	// 	const protobuf = require("protobufjs");
	// 	protobuf.util.Long = require("long");
	// 	protobuf.configure();

	// 	return this.get(url, {
	// 		params: config.params,
	// 		responseType:"arraybuffer"
	// 	}).then(buffer => {
	// 		return protobuf.load(config.protoFile).then((root) => {
	// 			let messages;

	// 			try {
	// 				messages = config.keys.map(key => ({
	// 					[key]: root.lookupType(key).decode(new Uint8Array(buffer))
	// 				}));
	// 			} catch (err) {
	// 				if (err instanceof protobuf.util.ProtocolError)
	// 					this.logService.error("Error decoding proto message", err);
	// 				else
	// 					this.logService.error("Error decoding proto message", err);
	// 			}
	// 			return messages;
	// 		}, err => {
	// 			if (err instanceof protobuf.util.ProtocolError)
	// 				return this.logService.error("Error decoding proto message", err);
	// 		});
	// 	});
	// }

	base64ToBlob(base64, mimetype, slicesize) {
		if (!window.atob || !Uint8Array) {
			// The current browser doesn"t have the atob function. Cannot continue
			return null;
		}
		mimetype = mimetype || "";
		slicesize = slicesize || 512;
		const bytechars = atob(base64);
		const bytearrays = [];
		for (let offset = 0; offset < bytechars.length; offset += slicesize) {
			const slice = bytechars.slice(offset, offset + slicesize);
			const bytenums = new Array(slice.length);
			for (let i = 0; i < slice.length; i++) {
				bytenums[i] = slice.charCodeAt(i);
			}
			const bytearray = new Uint8Array(bytenums);
			bytearrays[bytearrays.length] = bytearray;
		}
		return new Blob(bytearrays, {type: mimetype});
	}

	// download(url, config, filename, loader) {

	// 	const requestFunction = function() {
	// 		if(config.base64)
	// 			return $q.resolve(base64ToBlob(url, this.mime.lookup(config.extension), 512));

	// 		return this.request(url, Object.assign({
	// 			method: "get",
	// 			responseType: "blob"
	// 		}, config));
	// 	};

	// 	return requestFunction().then(response => {
	// 		if (loader){
	// 			loader.setContent("Download Complete");
	// 			modalService.closeModalTimeout(loader.title);
	// 		}

	// 		const link = document.createElement("a");
	// 		document.body.appendChild(link);
	// 		link.setAttribute("type", "hidden");
	// 		link.href = window.URL.createObjectURL(response);
	// 		link.download = filename;
	// 		link.click();
	// 		//window.URL.revokeObjectURL(link.href);

	// 		const downloadObject = {
	// 			repeatDownload() {
	// 				return download(url, config, filename, loader);
	// 			},
	// 			url: link.href,
	// 			request,
	// 			timestamp: moment(),
	// 			date: moment(),
	// 			filename,
	// 			extension: filename.substr(filename.lastIndexOf(".") + 1),
	// 			size: response.size,
	// 			link
	// 		};

	// 		$injector.get("seg.services.downloadHistoryService").add(downloadObject);
	// 	}, e => seg.this.logService.error("ERROR_BACKEND_DOWNLOAD", e.error + ": " + e.result));
	// }

	getDataURL(url, config?): Observable<any> { 
		return Observable.create((obs) => {
			return this.request(url, Object.assign({
					method: "get",
					responseType: "blob",
				}, config), {
					observe: "response"
			}).subscribe((res: any) => {
				const reader = new FileReader();
				reader.onerror = err => obs.error(err);
  				reader.onabort = err => obs.error(err);
				reader.onload = () => {
					obs.next(reader.result);
				};
				reader.onloadend = () => obs.complete();
				const ret = res instanceof Blob ? res : res.body;
				return reader.readAsDataURL(ret);
			});
		});
	}

	upload(config) {
		/*const form = new FormData();

		for(const key in config)
			form.append(key, config[key]);*/

		return this.post(config.url || "v1/file/upload", {
			form: config,
			headers: {
				"Content-Type": undefined
			}
		});
	}

	request(url, config, other_opts = null): Observable<HttpResponse<any>> {
		if (url.startsWith("v1/") && this.userService.getCurrentUser()) {
			if (!config.params)
				config.params = {};

			Object.assign(config.params, {
				user: this.userService.getCurrentUser().username,
				project: this.project
				// operation: operations
			});
		}

		const existingParams = $utils.getParams(url);
		url = $utils.stripParams(url);

		const intercepted = this.interceptorService.request(url, config);
		url = intercepted.url;
		config = intercepted.config;

		config.params = Object.entries(Object.assign({}, existingParams, config.params)).reduce((params, [key, value]) => value === null ? params : Object.assign(params, {
			[key]: value
		}), {});

		// if (!config.timeout)
		// 	config.timeout = 50 * 1000;

		// XXX
		if (JSON.parse(localStorage.getItem("disableWebWorkers"))) {
			return this.http[config.method](url, Object.assign({}, config, {
				params: Object.entries(config.params || {}).reduce((params, [key, value]) => {
					if (value)
						Object.assign(params, {[key]: decodeURIComponent(value.toString())});
					return params;
				}, {})
			}, other_opts)).pipe(catchError(this.handleError(url, config, config.method)));
		} else {
			const requestWorker = new Worker(location.href.replace(/\?.*$/, "") + this.REQUEST_WORKER_URL);

			Object.assign(config, {url});

			return Observable.create((obs) => {
				requestWorker.postMessage(config);

				requestWorker.addEventListener("message", ({data}) => {
					try	{
						data = this.interceptorService.response(config, data);
						if (typeof data === "object" && ("error" in data))
							obs.error(of(data));
						else
							obs.next(data);
						obs.complete();
					} catch (e) {
						this.logService.error("ERROR_RESPONSE_INTERCEPTOR", e, {config, data});
					}

					setTimeout(() => requestWorker.terminate(), 500);
				});

				// return Object.assign(deferred.promise, {
				// 	cancel() {
				// 		requestWorker.terminate();
				// 		deferred.reject("Cancelled");
				// 	}
				// });
			}).pipe(catchError(this.handleError(url, config, config.method)));
		}
	}

	handleError(url, config, operation: String) {
		return (err: any) => {
			const errMsg = `error in ${operation}() retrieving ${url}; status: ${err.error}, ${err.result} `;
			return throwError(errMsg);
		};
	}
}
