import { Injectable } from "@angular/core";

@Injectable({
	providedIn: "root"
})
export class InterceptorService {

	isIntegrationEnv: boolean;
	mock: boolean;
	env: string;
	ENVS: any = {
		dev: "http://clientes.gmv.com.pt:11500/SEGServer/",
		preprod: "https://portal-pp.emsa.europa.eu/SEGServer/",
		prod: "https://portal.emsa.europa.eu/SEGServer/"
	};
	TEST_OPERATIONS: Array<string> = [
		"IMDatE Service Default",
		"IMDatE Service MSPILOT",
		"IMDatE Service FRONTEX",
		"IMDatE Service MAOC-N",
		"IMDatE Service MARSURV_I",
		"IMDatE Service Atlantic",
		"IMDatE Service Mediterranean",
		"IMDatE Service COPERNICUS",
		"IMDatE Service CleanSeaNet",
		"IMDatE Service SAFEMED",
		"IMDatE Service TRACECA",
		"IMDatE Service EFCA IMS"
	];

	constructor() {
		this.mock = !!JSON.parse(localStorage.getItem("segdebug:mocks")),
		this.env = localStorage.getItem("env") || "dev",
		this.isIntegrationEnv = !location.hostname.match(/localhost|127\.0\.0\.1|innovagencyhost/);

		if (!this.isIntegrationEnv) {
			const localOperation = localStorage.getItem("operation");

			if (localOperation)
				sessionStorage.setItem("operation", localOperation);
		}
	}

	request(url, config) {
		if (!this.isIntegrationEnv) {
			if (this.mock) {
				if (url.match("v1/eo/stmid"))
					url = "mocks/stmid.json";
				else if (url.match("v1/grids"))
					url = "mocks/grids.json";
				else if (url.match(/v1\/eo\/wfs\/ext\/ows/) && (config.params.correlated_position === "UNCORRELATED_POSITION"))
					url = "mocks/uncorrelated.json";
				else if (url.match(/v1\/eo\/wfs\/ext\/ows/) && (config.params.correlated_position === "CORRELATED_POSITION"))
					url = "mocks/vds.json";
				else if (url.match(/v1\/eo\/wfs$/))
					url = "mocks/oilspills.json";
				else if (url.match(/v1\/tracks\/bbox$/))
					url = "mocks/track.json";
				else if (url.match(/v1\/thetis\/ship\/validated_inspections$/))
					url = "mocks/inspections.json";
				else if (url.match(/v1\/ship\/ship_call/) && config.params.hazmat)
					url = "mocks/hazmat.json";
				else if (url.match(/v1\/ship\/ship_call\/mrs/))
					url = "mocks/mrs.json";
				else if (url.match(/v1\/ship\/getCurrentVesselPosition/))
					url = "mocks/currentPosition.json";
				else if (url.match(/v1\/advanced_search\/project_descriptor/))
					url = "mocks/project_descriptor.json";
				else if (url.match(/v1\/advanced_search\/facets/))
					url = "mocks/facets.json";
				else if (url.match(/v1\/ship\/ship_call\/incident/))
					url = "mocks/incidents.json";
				else if (url.match(/v1\/ssn\/alerts/))
					url = "mocks/alerts.json";
				else if (url.match(/v1\/extension\/wms\/group/) && config.params.group)
					url = "mocks/" + config.params.group + ".json";
				else if (url.match(/v1\/eo\/wfs\/cgdAreas/) && config.params.group)
					url = "mocks/cgd.json";
			}

			if (url.match(/^\/?v1/) || url === "info")
				url = this.ENVS[this.env] + url;

			/* if(url.match("/v1/eo/wfs/cgdAreas")) {
				if (config.params.type == "FISHERIES")
					url = "mocks/fao.json";
				if (config.params.type == "EO_SPILL_ALERTING_A")
					url = "mocks/cgd.json";
			}*/

			// if ((url.match("v1/abm/surveillance/instance") && config.params.op === "find"))
			// 	url = "mocks/abm_find.json";

			// if (url.match("v1/abm/am/alerts") || url.match("v1/abm/am/alertsForShip"))
			// 	url = "mocks/alerts_new.json";

			// // LUIS
			// if (url.match("v1/abm/surveillance/template"))
			// 	url = "mocks/abm_sa_template.json";

			// // LUIS
			// if (url.match("v1/abm/dlm/search"))
			// 	url = "mocks/abm_distribution_list.json";

			// if (url.match("v1/ship/ports")){
			// 	url = "mocks/ports_new.json";
			// }

			// if (url.match("v1/ship/lastKnownVesselsPosition")){
				// 	url = "mocks/lastKnownVesselsPosition.json";
				// }

				// if(url.match(/v1\/cod\/detail/))
				// 	url = "mocks/cod-detail.json";

				const matchGebco = url.match(/v1\/extension\/wms\/gebco(.+)?/);
				if (matchGebco)
					url = "http://www.gebco.net/data_and_products/gebco_web_services/web_map_service/mapserv" + (matchGebco[1] || "");

				const matchDemis = url.match(/v1\/extension\/wms\/demis(.+)?/);
				if (matchDemis)
					url = "http://www2.demis.nl/WMS/wms.asp" + (matchDemis[1] || "");
			}

			if (!url.startsWith("http"))
				url = location.href.replace(/\?.*$/, "") + url;

			return {url, config};
	}

	response(config, res) {
		if (!this.isIntegrationEnv) {
			if (config.url.match("userdetails") && res) {
				if (!res.result)
					res.result = {};
				else if (typeof res.result === "string")
					res.result = JSON.parse(res.result);
			}

			// if (config.url.match("v1/ship/positions") && config.params.maxPositionsCount){
			// 	res = {"result":"Positions count [321] greater than provided "maxPositionsCount" [5]","status":"warning"};
			// }

			if (JSON.parse(localStorage.getItem("segdebug:mockpref"))) {
				if (config.method.match(/get/i)) {
					if (config.url.match(/\/?v1\/userdetails\/preferences/))
						res = {
							status: "ok",
							result: {}
						};
					else if (config.url.match(/\/?v1\/userdetails\/current_user/))
						res.result.applications = [{
							groups: this.TEST_OPERATIONS
						}];
				} else if (config.method.match(/put/i) && config.url.match(/\/?v1\/userdetails\//)) {
					res = {status: "ok"};
				}
			}
		}

		return res;
	}
}
