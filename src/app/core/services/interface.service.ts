import { Injectable } from "@angular/core";
import { CleanablesService } from "@core/services/cleanables.service";
import { ConfigService } from "@core/services/config.service";
import { DrawService } from "@core/services/interactions/draw.service";
import { FavouritesService } from "@core/services/favourites.service";
import { LayerService } from "@core/services/layer/layer.service";
import { LogService } from "@core/services/log.service";
import { MapService } from "@core/services/map/map.service";
import { ModalService } from "@main/services/modal/modal.service";
import { PreferencesService } from "@core/services/preferences.service";
import { RequestService } from "@core/services/request/request.service";
import { SelectionService } from "@core/services/interactions/selection.service";
import { ShortcutsService } from "@core/services/shortcuts.service";
import { SourceService } from "@core/services/layer/source.service";
import { StyleService } from "@core/services/style.service";
import { TablesService } from "@main/services/ttt/tables.service";
import { UserService } from "@core/services/user.service";
import * as utils from "libs/seg-utils";
import * as ol from "ol";
import * as proj from "ol/proj";
import * as style from "ol/style";
import * as format from "ol/proj";
import * as color from "ol/color";
import GeoJSON from "ol/format/GeoJSON";
import * as Observable from "rxjs";
import * as operators from "rxjs/operators";
import * as moment from "moment";
import { UiModule } from "@ui/ui.module";
declare global {
   interface Window { seg: any; opera: any; ol: any; moment: any; UiModule: any; }
}

@Injectable({
	providedIn: "root"
})
export class InterfaceService {

	map: any;

	constructor(
	  	// ACQ,
	  	// countryService,
	  	// csnService,
	  	// downloadHistory,
	  	// exportService,
	  	// filterService: FilterService,
	  	// geometryService,
	  	// image,
	  	// importService,
	  	// popupService,
	  	// queryPanelService,
	  	// searchService,
	  	// timeline,
	  	cleanableService: CleanablesService,
	  	CONFIG: ConfigService,
	  	drawService: DrawService,
	  	favouritesService: FavouritesService,
	  	layerService: LayerService,
	  	logService: LogService,
	  	mapService: MapService,
	  	modalService: ModalService,
	  	preferencesService: PreferencesService,
		requestService: RequestService,
	  	selectionService: SelectionService,
	  	shortcutsService: ShortcutsService,
	  	sourceService: SourceService,
	  	styleService: StyleService,
	  	tablesService: TablesService,
	  	userService: UserService,
	) {
		mapService.onMapReady(() => this.map = Object.assign(mapService.getMap(), {
			addFilter(tag, filter) {
				return mapService.addMapFilterMode(tag, filter);
			},
			enableFilter(filter) {
				return mapService.enableMode(filter);
			},
			disableFilter(filter) {
				return mapService.disableMode(filter);
			},
			isFilterEnabled(filter) {
				return mapService.inMode(filter);
			}
		}));

		window.UiModule = UiModule;
		window.moment = moment;
		window.ol = Object.assign(ol, {style, proj, color, format: Object.assign(format, {GeoJSON})});
		window.seg = Object.assign(this, {
			observable: Object.assign(Observable, {operators}),
			promise: Object.assign((resolve) => new Promise(resolve), Promise),
			utils,
			log: logService,
			CONFIG,
			timeout: window.setTimeout,
			style: styleService,
			request: requestService,
			source: sourceService,
			favourites: favouritesService,
			// timeline: timeline.API,z
			layer: layerService, // Object.assign({}, layerService, {filter: filterService}),
			// search: searchService,
			preferences: {
				workspace: {
					getPreference: preferencesService.getPreference,
					setPreference: preferencesService.setPreference,
					unsetPreference: preferencesService.unsetPreference,
					preferences: preferencesService.preferences,
					onPreferenceChange: preferencesService.onPreferenceChange,

					get: preferencesService.getWorkspacePreference,
					set: preferencesService.setWorkspacePreference,
					unset: preferencesService.unsetWorkspacePreference,
					onChange: preferencesService.onWorkspacePreferenceChange
				},
				session: {
					getPreference: preferencesService.getPreference,
					setPreference: preferencesService.setPreference,
					unsetPreference: preferencesService.unsetPreference,
					preferences: preferencesService.preferences,
					onPreferenceChange: preferencesService.onPreferenceChange,

					get: preferencesService.getSessionPreference,
					set: preferencesService.setSessionPreference,
					onChange: preferencesService.onSessionPreferenceChange
				},
				operation: {
					getPreference: preferencesService.getPreference,
					setPreference: preferencesService.setPreference,
					unsetPreference: preferencesService.unsetPreference,
					preferences: preferencesService.preferences,

					get: preferencesService.getOperationPreference,
					set: preferencesService.setOperationPreference,
					unset: preferencesService.unsetOperationPreference,
					// onChange: preferencesService.onOperationPreferenceChange
				},
				map: {
					getPreference: preferencesService.getPreference,
					setPreference: preferencesService.setPreference,
					unsetPreference: preferencesService.unsetPreference,
					preferences: preferencesService.preferences,

					get: preferencesService.getMapPreference,
					set: preferencesService.setMapPreference,
					// onChange: preferencesService.onMapPreferenceChange
				}
			},
			user: userService,
			ui: {
				ttt: {
					tables: tablesService
				},
				cleanables: cleanableService
			},
			// geometry: geometryService,
			modal: modalService,
			// popup: {
			// 	openPopup: popupService.openPopup,
			// 	close: popupService.closePopup
			// },
			shortcuts: shortcutsService,
			// image,
			// downloads: downloadHistory,
			// countries: countryService,
			// csn: csnService,
			draw: drawService,
			// import: importService,
			// export: exportService,
			// ACQ: Object.assign((...args) => ACQ.submit(...args), {
			// 	open: queryPanelService.openACQ
			// }),
			selection: selectionService,
			isMobile: () => {
				let check = false;
				(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw-(n|u)|c55\/|capi|ccwa|cdm-|cell|chtm|cldc|cmd-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc-s|devi|dica|dmob|do(c|p)o|ds(12|-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(-|_)|g1 u|g560|gene|gf-5|g-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd-(m|p|t)|hei-|hi(pt|ta)|hp( i|ip)|hs-c|ht(c(-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i-(20|go|ma)|i230|iac( |-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|-[a-w])|libw|lynx|m1-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|-([1-8]|c))|phil|pire|pl(ay|uc)|pn-2|po(ck|rt|se)|prox|psio|pt-g|qa-a|qc(07|12|21|32|60|-[2-7]|i-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h-|oo|p-)|sdk\/|se(c(-|0|1)|47|mc|nd|ri)|sgh-|shar|sie(-|m)|sk-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h-|v-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl-|tdg-|tel(i|m)|tim-|t-mo|to(pl|sh)|ts(70|m-|m3|m5)|tx-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas-|your|zeto|zte-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
				return check;
			}
		});
	}


}
