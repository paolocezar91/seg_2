import { MapService } from '@core/services/map/map.service';
import { SelectionService } from '@core/services/interactions/selection.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {

  constructor(
    private selectionService: SelectionService,
    private mapService: MapService
  ) { }

  alerts = [];

	addFreshDummy(text)
	{
		this.addFreshAlert({ id: 3, label: text, feature: null});
	};

	addFreshAlert(config)
	{
		config.freshAlert = true;
		config.read = false;

		if(!document.hidden)
			setTimeout(() => {
				this.cleanFreshState(config);
			}, config.stayMilTime || 8000);

		this.alerts.push(config);
	};

	getAlerts()
	{
		return this.alerts;
	};

	getNotReadAlerts() {
		return this.alerts.filter(alert => !alert.read);
	}

	getFreshAlerts()
	{
		const freshAlerts = [];
		for(let i = 0; i < this.alerts.length; i++)
		{
			if (this.alerts[i].freshAlert)
			{
				freshAlerts.push(this.alerts[i]);
			}
		}

		return freshAlerts;
	};

	add(config)
	{
		this.alerts.push(config);
	};

	goToAlert(config)
	{
		this.selectionService.select(config.feature);
		this.mapService.getMap().centerOn(config.feature);
	};

	remove(config) {
		for(let i = 0; i < this.alerts.length; i++)
		{
			if (this.alerts[i] === config)
			{
				this.alerts.splice(i, 1);
			}
		}
	};

	markAlertsAsRead()
	{
		for(let i = 0; i < this.alerts.length; i++)
		{
			this.alerts[i].read = true;
		}
	};

	unMarkFreshAlerts()
	{
		for(let i = 0; i < this.alerts.length; i++)
		{
			this.alerts[i].freshAlert = false;
		}
	};

	cleanFreshState(config)
	{
		config.hide = true;
		setTimeout(() => {
			config.freshAlert = false;
		}, 500);
	};

	removeWithAnimation(config)
	{
		config.hide = true;
		setTimeout(() => {
			this.remove(config);
		}, 500);
	};

}