import { CleanablesService } from '@core/services/cleanables.service';
import { UserService } from "@core/services/user.service";
import { SourceService } from "@core/services/layer/source.service";
import { MapService } from "@core/services/map/map.service";
import { MapToolsService } from "@main/services/map-tools.service";
import { PreferencesService } from "@core/services/preferences.service";
import { SelectionService } from "./interactions/selection.service";
import { LogService } from "@core/services/log.service";
import { LayerService } from "@core/services/layer/layer.service";
import { Injectable, OnInit } from "@angular/core";

import GeoJSON from "ol/format/GeoJSON";
import Feature from "ol/Feature";
import Geometry from "ol/geom/Geometry";
import Point from "ol/geom/Point";
import VectorSource from "ol/source/Vector";
import * as source from "ol/source";
import * as $o from "libs/seg-object";
import * as moment from "moment";

const LOG_TAG = "seg.favourites";

// Favourite Class definition
class Favourite {

	type: string;
	name: string;
	feature: Feature | Geometry;

	constructor(featureOrGeometry, name, private favouritesService) {
		this.init(featureOrGeometry, name);
	}

	init(featureOrGeometry, name) {
		if (!name && featureOrGeometry instanceof Feature) {
			const TOItype = this.favouritesService.getTOItype(featureOrGeometry);

			if (TOItype && TOItype.label)
				name = TOItype.label(featureOrGeometry);
			else
				name = featureOrGeometry.get("DESCRIPTION") ? featureOrGeometry.get("DESCRIPTION") : (featureOrGeometry.getId() && featureOrGeometry.getId().toString());
		}

		if (!name)
			name = "unnamed";

		if (featureOrGeometry instanceof Geometry) {
			featureOrGeometry = new Feature({
				geometry: featureOrGeometry
			});

			this.type = "aoi";
		} else if (/*!(featureOrGeometry instanceof Feature) ||*/ !featureOrGeometry.getGeometry())
			this.favouritesService.logService.error(LOG_TAG + ".Favourite", "You must provide either a geometry or a feature with a geometry");
		else if (featureOrGeometry.get("seg_type"))
			this.type = featureOrGeometry.get("seg_type");

		this.feature = featureOrGeometry;

		this.name = name;
	}

	isType(type, favourite) {
		return this.favouritesService(type, favourite);
	}
}

export interface FavouriteGroup {
	favourites: Array<Favourite>;
	name: string;
	color: string;
	locked: boolean;
	file?: string;
}

@Injectable({
	providedIn: "root"
})
export class FavouritesService {

	geoJSONFormatter = new GeoJSON();
	TOItypes = [];
	AOILayer;
	AOISource;
	groups: FavouriteGroup[];
	sessionFavourites;
	selectedFavourites: FavouriteGroup;
	name;
	type;
	feature;

	constructor(
		private layerService: LayerService,
		private sourceService: SourceService,
		private mapService: MapService,
		private selectionService: SelectionService,
		private logService: LogService,
		private preferencesService: PreferencesService,
		private cleanablesService: CleanablesService,
		private mapToolsService: MapToolsService,
		private userService: UserService
	) {	}

	init() {
		this.sessionFavourites = {
			favourites: [],
			name: "Session",
			color: "#FF0000",
			locked: false,
			file: "Session"
		};

		this.selectedFavourites = {
			favourites: [],
			name: "Selected",
			color: "#FF0000",
			locked: false,
			file: "Session"
		};

		this.groups = [
			this.sessionFavourites,
			this.selectedFavourites
		];

		this.AOILayer = this.layerService.get("aoi");
		this.AOISource = this.AOILayer.getSource();
		this.selectionService.onSelectionChange(() => this.updateSelectedGroup());

		window.addEventListener("keyup", e => {
			if (e.code === "Delete" && e.target === document.body)
				this.removeSelectedSessionAOIs();

			// this.buildFavAndSessionMenus();
		});

		this.loadFavourites();
	}

	isFavourite(feature) {
		return this.getFavouriteGroups().some(group => group.favourites.some(favourite => favourite.feature === feature));
	}

	// Need an Angular 7 way to do this.
	// broadcast listener is in top-left-menu\index.js
	// probably using BehaviourSubject or Observable
	// open() {
	// 		$rootScope.$broadcast("openFavourites");
	// 	}

	// buildFavAndSessionMenus() {
	// 	this.buildSessionMenu();
	// 	this.getFavouritesLayer().forEach(layer => this.buildFavAreasMenu(layer));
	// };

	// buildSessionMenu() {
	// 	const aoiMenu = layerMenuService.findInTreeByLayer(AOILayer, {depth: 2});

	// 	if(aoiMenu){
	// 		const sessionFilter = AOILayer.get("filters").find(opt => opt.name === "Session"),
	// 			options = (() => {
	// 				return sessionFavourites
	// 					.filter(session => session.type === AOILayer.get("id"))
	// 					.flatten().reduce((options, session) =>  {
	// 						return Object.assign(options, {
	// 							[session.name]: feature => {
	// 								return feature.get("group_name") === "Session" && feature.get("favourite_name") === session.name;
	// 							}
	// 						});
	// 					}, {});
	// 			})();

	// 		if(Object.keys(options).length)
	// 			filterService.setFilterOptions(sessionFilter, options);
	// 		else
	// 			filterService.removeAllFilterOptions(sessionFilter);

	// 		aoiMenu.options = layerMenuService.fromFilters(AOILayer);
	// 		// layerMenuService.fromFilters(AOILayer)[0].bindings.filter.setActive(true);
	// 	}
	// };

	/*this.buildFavAreasMenu = (layer, favouriteGroups? = null) => {
		// creating area favourites menu tree
		if (!layer)
			return;

		const layerMenu = seg.ui.layerMenu.findInTreeByLayer(layer, {depth: 2});

		if(!favouriteGroups)
			favouriteGroups = [].concat(this.getFavouriteGroups().reduce((favGroups, group) => {
				if(group.some(favourite => favourite.type === layer.get("id")))
					favGroups.push(group);
				return favGroups;
			}, []));

		if (favouriteGroups.length && layerMenu){
			const favouriteFilters = [{
				name: "Favourites",
				options: filterService.buildMultiLevelFilters({
					name: "Favourites",
					options: (() => {
						const filters = [];

						favouriteGroups.forEach(favouriteAreasGroup => {
							filters.push({
								name: favouriteAreasGroup.name,
								fn: (feature) => {
									return feature.get("group_name") === favouriteAreasGroup.name;
								},
								options: (() => {
									return favouriteAreasGroup
										.filter(favourite => favourite.type === layer.get("id"))
										.flatten().reduce((options, favourite) =>  {
											return Object.assign(options, {
												[favourite.name]: feature => {
													return feature.get("favourite_name") === favourite.name;
												}
											});
										}, {});
								})()
							});
						});

						filters.push({
							name: "N/A",
							fn: (feature) => {
								return !feature.get("group_name") && !feature.get("favourite_name");
							}
						});

						return filters;
					})()
				})
			}];

			filterService.removeFilters(layer, ["Favourites"]);
			filterService.addFilters(layer, favouriteFilters);

			layerMenuService.unregister("Favourites", {depth: 2}, layerMenu.options);

			const favMenu = layerMenuService.buildMultiLevelMenu(layerMenuService.fromFilters(layer)[0], 1);
			layerMenu.options.push(favMenu);
		}
	};*/

	getGroup(groupName) {
		return this.groups.find(group => group.name === groupName);
	}

	getAllFavourites() {
		return [].concat(...this.getFavouriteGroups());
	}

	getFavouritesLayer() {
		return this.getAllFavourites().map(feature => this.layerService.get(feature.type)).filter((value, index, self) => self.indexOf(value) === index);
	}

	getGroupFromFeature(feature) {
		if (this.isFavourite(feature)) {
			const favouriteGroups = this.getFavouriteGroups();
			for (let i = 0; i < favouriteGroups.length; i++)
				for (let j = 0; j < favouriteGroups[i].favourites.length; j++)
					if (favouriteGroups[i].favourites[j].feature === feature)
						return favouriteGroups[i];
		}
	}

	createGroup(group) {
		if (this.groups.find(existingGroup => existingGroup.name === group.name))
			throw this.logService.error("Favourites", "Group " + group.name + " already exists");

		const newGroup: FavouriteGroup = {
			favourites: [],
			name: "",
			color: "",
			locked: false
		};

		newGroup.name = group.name;

		this.groups.push(newGroup);

		if (group.color)
			this.setGroupColor(newGroup.name, group.color);

		/*	if(group.file)
		this.setGroupFile(newGroup.name, group.file);*/

		return this.saveFavourites().then(() => newGroup, e => this.logService.error("ERROR_FAVORITES_CREATE_GROUP", e.error + ": " + e.result));
	}

	addFeaturesToGroup(groupName, features, name?) {
		const group = this.getGroup(groupName);

		if (!group)
			return;

		features = [].concat(features)
			.filter(feature => (/*feature instanceof Feature &&*/ feature.getGeometry() instanceof Geometry) || (feature instanceof Geometry))
			.map(feature => (feature instanceof Geometry) ? new Feature({feature}) : feature);

		if (!features.length)
			return;

		if (groupName === "Session") {
			const existingAOIs = this.AOISource.getFeatures();

			this.AOISource.addFeatures(features.filter(feature => !~existingAOIs.indexOf(feature)).map((ft) => {
				ft.getGeometry().unset("draw");
				ft.setId(Math.floor(Math.random() * (99999 - 0 + 1)) + 0);
				return ft;
			}));
		}

		const favourites = features.map(feature => {
			// go through existing groups
			for (let i = 0; i < this.groups.length; i++) {
				// go through existing favourites
				const currArray = this.groups[i].favourites || [];

				for (let j = 0; j < currArray.length; j++) {
					const fav = this.groups[i][j];

					// if we find a favourite with the same feature
					if (fav && fav.feature === feature) {
						// if the group is in session, remove it from session group
						if (this.groups[i] === this.sessionFavourites)
							$o.removeFromArray(this.sessionFavourites, fav);

						feature.set("group_name", groupName);
						feature.set("favourite_name", fav.name);
						return fav;
					}
				}
			}

			name = this.suffixName(name);

			feature.set("group_name", groupName);
			feature.set("favourite_name", feature.get("favourite_name") || name);
			const favourite = new Favourite(feature, name, this);

			return favourite;
		});

		group.favourites.push(...favourites);


		if(Object.values(this.sessionFavourites).some(favourite => this.isType("AOI", favourite)))
			this.cleanablesService.add("Session AOIs", this.removeSessionAOIs);
		else this.cleanablesService.remove("Session AOIs");

		features.forEach(feature => feature.changed());

		if (!groupName.match(/Selected|Session/))
			return this.saveFavourites();

		this.updateSelectedGroup();
	}

	setGroupColor(groupName, color) {
		this.getGroup(groupName).color = color;
	}

	suffixName(name) {
		let suffix = 0;

		this.groups.forEach(group => {
			if (group.favourites) {
				group.favourites.forEach(existingFavourite => {
					let match;

					if (typeof existingFavourite.name === "string")
						match = existingFavourite.name.match(new RegExp("^" + name.replace(/[-/\\^$*+?.()|[\]{}]/g, "\\$&") + "(?:_(\\d+))?$"));

					if (match)
						suffix = Math.max(suffix, (+match[1] + 1 || 1));
				});
			}
		});

		if (suffix)
			name += "_" + suffix;

		return name;
	}

	/*setGroupFile = (groupName, file) => {
		return $q((resolve, reject) => {
			const group = this.getGroup(groupName);

			if(group && file) {
				const path = "/"+ this.userService.getCurrentUser().username+"/favourites/"+group.name+"/"+file.name;
				*/
				/*resolve(backend.upload({
					file: file,
					pathToSave: path
				}).then(() => group.file = path), e => seg.log.error("ERROR_FAVORITES_SET_GROUP_FILE", e.error + ": " + e.result));*/
/*			}
			else reject();
		});
	};*/

	loadMobileFavourites(vesselsMobileFavourites = {}) {
		if (Object.keys(vesselsMobileFavourites).length)
			return [{
				name: "Vessels (Mobile)",
				locked: true,
				features: Object.keys(vesselsMobileFavourites).map(key => {
					const vessel = vesselsMobileFavourites[key];
					vessel.coordinates = [vessel.lon || vessel.longitude || 0, vessel.lat || vessel.latitude || 0];
					return {
						name: vessel.shipName || ("MMSI: " + vessel.mmsi) || ("IMO: " + vessel.imo),
						type: "positions",
						feature: {
							geometry: {
								coordinates: vessel.coordinates,
								type: "Point"
							},
							id: vessel.vid,
							properties: {
								vid: vessel.vid,
								shipName: vessel.shipName,
								mmsi: vessel.mmsi,
								imo: vessel.imo
							},
							type: "Feature"
						}
					};
				})
			}];
		else
			return [];
	}

	loadFavourites() {
		let favouriteGroups = [
			...this.preferencesService.getOperationPreference("favourites") || [],
			...this.loadMobileFavourites(this.preferencesService.getOperationPreference("mobile.favourites.vessels") || {})
		];

		if (!favouriteGroups.length)
			return;

		favouriteGroups = favouriteGroups.map(favouriteGroup => {
			let favourites = [];

			if (favouriteGroup.features)
				favourites = favouriteGroup.features.map(favourite => {
					const type = favourite.type;
					if (type === "sar_surpic")
						return;

					Object.assign(favourite.feature.properties, {
						group_name: favouriteGroup.name,
						favourite_name: favourite.name
					});

					const parsedFeature = this.geoJSONFormatter.readFeature(favourite.feature, {
						dataProjection: "EPSG:4326",
						featureProjection: this.mapService.getMap().getProjection()
					});

					const layer = this.layerService.get(type);

					if (layer)
						layer.getSource().addFeature(parsedFeature);

					favourite.name = this.suffixName(favourite.name);

					return new Favourite(parsedFeature, favourite.name, this);
				}).filter(favourite => favourite);

			delete favouriteGroup.features;

			return {
				favourites,
				name: favouriteGroup.name,
				color: favouriteGroup.color,
				locked: false
			};
		});

		this.groups.push(...favouriteGroups);
	}

	saveFavourites() {
		const serializedGroups = [];

		this.groups.forEach(group => {
			if (group === this.sessionFavourites || group === this.selectedFavourites || group.locked)
				return;

			serializedGroups.push({
				name: group.name,
				color: group.color,
				file: group.file,
				features: group.favourites.map(favourite => {
					const featureProperties = favourite.feature.getProperties();

					delete featureProperties.layers; // removing some unused and open_layers data
					delete featureProperties.imageLayer; // removing some unused and open_layers data
					delete featureProperties.selected; // removing some unused and open_layers data
					delete featureProperties.seg_type; // removing some unused and open_layers data
					delete featureProperties.photos; // removing base64 data
					delete featureProperties.thumbnail; // removing base64 data
					delete featureProperties.vessel;
					delete featureProperties.isSearchResult;
					delete featureProperties.wasSelected;
					delete featureProperties.isSmartSearchResult;

					if (featureProperties.ts) // need to convert to enter in database
						featureProperties.ts = moment(featureProperties.ts).valueOf(); // enforcing timestamp as long for emsa"s mobile app usage
					if (featureProperties.timestamp) // need to convert to enter in database
						featureProperties.timestamp = moment(featureProperties.timestamp).valueOf(); // enforcing timestamp as long for emsa"s mobile app usage
					if (featureProperties.timeStamp)
						featureProperties.timeStamp = moment(featureProperties.timeStamp).valueOf(); // enforcing timestamp as long for emsa"s mobile app usage

					return {
						name: favourite.name,
						type: favourite.type,
						feature: Object.assign(JSON.parse(this.geoJSONFormatter.writeFeature(new Feature(featureProperties), {
							dataProjection: "EPSG:4326",
							featureProjection: this.mapService.getMap().getProjection()
						})), {
							id: favourite.feature.getId()
						})
					};
				})
			});
		});

		const sessionAOIs = this.getSessionAOIs().map(favourite => favourite.feature);
		this.addFeaturesToGroup("Session", this.AOISource.getFeatures().filter(aoi => !this.isFavourite(aoi) && !~sessionAOIs.indexOf(aoi)));
		// this.buildFavAndSessionMenus();

		return this.preferencesService.setOperationPreference("favourites", serializedGroups);
	}

	getGroups() {
		return  this.groups;
	}

	getFavouriteGroups() {
		return this.groups.filter(group => group !== this.sessionFavourites && group !== this.selectedFavourites);
	}

	registerTOItype(config) {
		if (!(config.source instanceof VectorSource))
			throw this.logService.error(LOG_TAG + ".registerTOItype", "source must be of type Vector Source");
		if (typeof config.label !== "undefined" && typeof config.label !== "function")
			throw this.logService.error(LOG_TAG + ".registerTOItype", "label must be a function");

		this.TOItypes.push(config);
	}

	updateSelectedGroup(/*self = this*/) {
		/*this.selectedFavourites.favourites.slice().forEach(favourite => {
			if (favourite) {
				const condition = !favourite.feature.get("selected") || this.groups.find(group => {
					return group.name !== "Selected" && group.favourites.find(existingFavourite => existingFavourite === favourite);
				});

				if (condition)
					this.selectedFavourites.favourites.splice(this.selectedFavourites.favourites.indexOf(favourite), 1);
			}
		});

		this.selectionService.getSelectedFeatures().forEach(feature => {
			if (!this.groups || !this.groups.find(group => group.favourites.find(favourite => favourite.feature === feature)))
				this.addFeaturesToGroup("Selected", feature);
		});*/

		// this.buildFavAndSessionMenus();
	}

	getSessionAOIs() {
		return this.sessionFavourites.favourites.filter(favourite => this.isType("AOI", favourite));
	}

	removeAOI(aoi) {
		const AOIFavourite = this.sessionFavourites.favourites.find(favourite => favourite.feature === aoi);

		if (AOIFavourite) {
			this.AOISource.removeFeature(aoi);
			$o.removeFromArray(this.sessionFavourites, AOIFavourite);

			
			if(!Object.values(this.sessionFavourites).some(favourite => this.isType("AOI", favourite)))
				this.cleanablesService.remove("Session AOIs");
			
		}
	}

	deleteGroup(group) {
		this.groups = this.groups.filter(existingGroup => existingGroup !== group);
	}

	removeSelectedSessionAOIs() {
		const AOIsToRemove = [],
		AOIFeaturesToRemove = [];

		this.sessionFavourites.favourites.forEach(favourite => {
			if (this.isType("AOI", favourite) && favourite.feature.get("selected")) {
				AOIsToRemove.push(favourite);
				this.AOISource.removeFeature(favourite.feature);
				AOIFeaturesToRemove.push(favourite.feature);
			}
		});

		this.selectionService.deselect(AOIFeaturesToRemove);
		$o.removeFromArray(this.sessionFavourites.favourites, AOIsToRemove);

		if(!this.sessionFavourites.favourites.some(favourite => this.isType("AOI", favourite)))
		this.cleanablesService.remove("Session AOIs");
	}

	removeSessionAOIs() {
		this.removeSelectedSessionAOIs();

		const AOIsToRemove = [],
		AOIFeaturesToRemove = [];

		this.sessionFavourites.favourites.forEach(favourite => {
			if (this.isType("AOI", favourite)) {
				AOIsToRemove.push(favourite);
				AOIFeaturesToRemove.push(favourite.feature);
			}
		});

		// If this is triggered by LinePolygon, we need to close the popup
		// this.popupService.popup.close("LinePolygon Coordinates");///////////////////////////////////////////////////

		if(this.AOISource.getFeatures().length)
			AOIFeaturesToRemove.forEach(feature => this.AOISource.removeFeature(feature));

		$o.removeFromArray(this.sessionFavourites, AOIsToRemove);
	}

	getTOItype(feature) {
		return this.TOItypes.find(TOItype => this.sourceService.findFeature(TOItype.source, feature));
	}

	isType(type, favourite) {
		if(favourite.feature && favourite.feature.getGeometry){
			const geometry = favourite.feature.getGeometry();

			if (type === "TOI")
				return !!this.getTOItype(favourite.feature);

			if (type === "BBOX") {
				if (geometry.getLinearRingCount() !== 1)
					return false;

				const coords = geometry.getLinearRing(0).getCoordinates();

				if (coords.length !== 5 || coords[0][0] !== coords[coords.length - 1][0] || coords[0][1] !== coords[coords.length - 1][1])
					return false;

				for (let i = 0; i < coords.length - 1; i++)
					if (coords[i][0] !== coords[i + 1][0] && coords[i][1] !== coords[i + 1][1])
						return false;

				return true;
			}

			if (type === "AOI")
				return !(geometry instanceof Point);
		}
		return false;
	}
}
