import { SelectionService } from "./../selection.service";
import { MapService } from "@core/services/map/map.service";
import { PreferencesService } from "@core/services/preferences.service";
import { LogService } from "@core/services/log.service";
import { RequestService } from "@core/services/request/request.service";
import { circular as circularPolygon } from "ol/geom/Polygon";
import Overlay from "ol/Overlay";
import { Injectable, OnInit } from "@angular/core";
import * as GeometryCollection from "ol/geom/GeometryCollection";
import * as Point from "ol/geom/Point";
import * as proj from "ol/proj";
import * as utils from "libs/seg-utils";
import { cloneDeep } from "lodash";

@Injectable({
	providedIn: "root"
})
export class FixedRingService implements OnInit {

	DEFAULT_RING_VALUE = [10000, 30000, 40000]; // preferencias? está em metros
	DEFAULT_INTERVAL = 1000;
	measureElement =  null;
	fixedRingFeature = null;
	measureService = null;
	coordinateLatOptions = ["N", "S"];
	coordinateLonOptions = ["E", "W"];
	currentFixedRangeRing;
	masterTemplate;

	constructor(
		private requestService: RequestService,
		private logService: LogService,
		private preferencesService: PreferencesService,
		private mapService: MapService,
		private selectionService: SelectionService
	) {	}

	ngOnInit() {
		this.requestService.get("core/services/interactions/measure/overlay.template.html")
			.subscribe(template => this.masterTemplate = template, e => this.logService.error("ERROR_FIXED_RING", e.status + ": " + e.message));
		this.initFixedRing();
	}


	getObject() {
		return this.currentFixedRangeRing;
	}

	getDistanceUnitName() {
		return this.preferencesService.getWorkspacePreference("distanceUnits");
	}

	initFixedRing(fixedRing?) {
		this.fixedRingFeature = fixedRing;
		this.measureElement = this.measureService.getMeasureObjectElement(fixedRing);
		this.measureElement.ringsValue = this.measureElement.maxValue ? [
		(this.measureElement.maxValue / 3),
		(this.measureElement.maxValue / 2),
		this.measureElement.maxValue
		] : (
			this.measureElement.ringsValue ?
			this.measureElement.ringsValue :
			this.DEFAULT_RING_VALUE
			);

		//this.measureElement.ringsValue = this.measureElement.ringsValue || this.DEFAULT_RING_VALUE;
		const projection = this.mapService.getMap().getProjection();
		const geometryCollection = fixedRing.getGeometry().getGeometries();

		this.currentFixedRangeRing = {
			coordinates: geometryCollection.reduce((array, geometry) => {
				if (geometry instanceof Point) {
					let lonLat;

					if (geometryCollection.length === 1)
						lonLat = proj.transform(geometry.getCoordinates(), projection, "EPSG:4326");
					else
						lonLat = geometry.getCoordinates();

					return array.concat((utils.lonLatToDMS(lonLat)));
				}
				return array;
			}, []),
			rings: this.convertRingFromMetersToDistanceUnit(this.measureElement.ringsValue).join(", ")
		};

		if (this.fixedRingFeature.getGeometry().getGeometries().length === 1) {
			this.setFixedRing(this.currentFixedRangeRing);
		}
	}

	setFixedRing(fixedRing) {
		const ringsValue = this.measureElement.ringsValue;
		const collectionGeom = [];
		const projection = this.mapService.getMap().getProjection();

		this.measureElement.measureMarkersOverlay.forEach((markerOverlay) => {
			this.mapService.getMap().removeOverlay(markerOverlay);
		});

		// clean the list to append new overlays
		this.measureElement.measureMarkersOverlay = [];

		let lastCoordinate = null;
		for (let i = 0; i < fixedRing.coordinates.length; i++) {
			let radius, output = "";
			const coordinateDecimal = utils.dmsToDecimal(fixedRing.coordinates[i]),
			newCoord = proj.transform(coordinateDecimal, "EPSG:4326", projection);

			if (lastCoordinate && lastCoordinate[0] === coordinateDecimal[0] && lastCoordinate[1] === coordinateDecimal[1])
				continue;

			lastCoordinate = coordinateDecimal;

			collectionGeom.push(new Point(coordinateDecimal));

			// create the center overlay
			output += this.createOverlayForCenterPosition(coordinateDecimal);

			for(let j = 0; j < ringsValue.length; j++) {
				const ringDimension = ringsValue[j];
				radius = parseFloat(ringDimension);

				// append new geomtry
				const polygon = circularPolygon(proj.toLonLat(newCoord, projection), radius);
				collectionGeom.push(polygon.transform("EPSG:4326", projection));
				output += this.createOverlayForCirclePosition(polygon, j + 1);
			}

			// create overlay html element
			const overlayHTML = document.createElement("div");
			overlayHTML.className = "tooltip tooltip-measure-fixed";

			// create overlay popup
			const popupOverlay = new Overlay({
				element: overlayHTML,
				offset: [0, -25],
				positioning: "bottom-center",
				stopEvent: false
			});

			newCoord[1] += radius;

			// set overlay position
			popupOverlay.setPosition(newCoord);

			// add overlay to map
			this.mapService.getMap().addOverlay(popupOverlay);

			// compile html
			/*const finalTemplate = $compile(this.masterTemplate)(angular.extend($rootScope.$new(), {output}));
				angular.element(overlayHTML).append(finalTemplate);
			*/

			// add to measureElement
			this.measureElement.measureMarkersOverlay.push(popupOverlay);
		}
		// create feature geom collection
		const geomCollection = new GeometryCollection(collectionGeom);

		// set geometry
		this.fixedRingFeature.setGeometry(geomCollection);
	}

	getCircleRadius = (projection, valueInMeters) => {
		return valueInMeters / projection.getMetersPerUnit();
	}

	createOverlayForCirclePosition = (circleGeometry, i) => {
		// get the distance and bearing values
		let output = this.measureService.formatCircle(circleGeometry, circleGeometry.getLastCoordinate());

		// set distance and bearing
		output = "<span style=\"display: block;\">Distance #" + i + ": <span style=\"margin-left:1px; font-weight: bold;\" distance=\"" + output[0] + "\"></span></span>"; // distance

		return output;
	}

	createOverlayForCenterPosition = (coordinate) => {
		// newCoord is the cernter [the point coordinate]
		let coordinateHTML = "<div style=\"display: inline;\">Center:</div><span style=\"margin-left: 5px; font-weight: bold;\" [coordinate]=\"[" + coordinate[1] + ", 'lat']\"></span>";
		coordinateHTML += "<span style=\"margin-left: 4px; font-weight: bold;\" [coordinate]=\"[" + coordinate[0] + ", 'lon']\"></span> ";

		return coordinateHTML;
	}

	addNewCoordinateToCurrentFixedRangeRing = (coordinatesDMS) => {
		this.currentFixedRangeRing.coordinates.push({
			lat: {
				d: coordinatesDMS.lat.d,
				m: coordinatesDMS.lat.m,
				s: coordinatesDMS.lat.s,
				h: coordinatesDMS.lat.h
			},
			lon: {
				d: coordinatesDMS.lon.d,
				m: coordinatesDMS.lon.m,
				s: coordinatesDMS.lon.s,
				h: coordinatesDMS.lon.h
			}
		});
	}

	addNewCoordinateRowClick() {
		const lastRow = this.currentFixedRangeRing.coordinates[this.currentFixedRangeRing.coordinates.length - 1];

		const newLine = {};

		cloneDeep(lastRow, newLine);

		this.addNewCoordinateToCurrentFixedRangeRing(newLine);
	}

	removeCoordinateRowClick = (index) => {
		this.currentFixedRangeRing.coordinates.splice(index, 1);
	}

	setFixedRingClick() {
		this.measureElement.ringsValue = this.convertRingFromDistanceUnitToMeters(this.currentFixedRangeRing.rings.split(","));

		this.setFixedRing(this.currentFixedRangeRing);

		this.selectionService.select([]);
	}

	convertRingFromMetersToDistanceUnit = (arrayRings) => {
		const values = [];
		const format = this.getDistanceUnitName();

		for (let i = 0; i < arrayRings.length; i++) {
			let val = arrayRings[i];
			if ((val && val !== "" && !isNaN(val))) {
				val = parseFloat(val);
				val = utils.convertFromMeters(val, format).toFixed(2);
				values.push(parseFloat(val));
			}
		}

		return values;
	}

	convertRingFromDistanceUnitToMeters = (arrayRings) => {
		const values = [];
		const format = this.getDistanceUnitName();

		for (let i = 0; i < arrayRings.length; i++) {
			let val = arrayRings[i];
			if ((val && val !== "" && !isNaN(val))) {
				val = parseFloat(val);
				val = utils.convertToMeters(val, format).toFixed(2);
				values.push(parseFloat(val));
			}
		}

		return values;
	}

	isValidToSet() {
		const arrayRings = this.currentFixedRangeRing.rings.split(",");

		for(let i = 0; i < arrayRings.length; i++) {
			const val = arrayRings[i];
			if (!(val && val !== "" && !isNaN(val)) || parseFloat(val) <= 0) {
				return false;
			}
		}

		for(let i = 0; i < this.currentFixedRangeRing.coordinates.length; i++) {
			const coordinate = this.currentFixedRangeRing.coordinates[i];

			if (!utils.isValidDMS(coordinate))
				return false;

			if (parseInt(coordinate.lat.d) > 90)
				coordinate.lat.d = 90;
			else if (parseInt(coordinate.lat.d) < -90)
				coordinate.lat.d = -90;
			if (parseInt(coordinate.lat.m) >= 60)
				coordinate.lat.m = 59;
			else if (parseInt(coordinate.lat.m) < 0)
				coordinate.lat.m = 0;
			if (parseInt(coordinate.lat.s) >= 60)
				coordinate.lat.s = 59;
			else if (parseInt(coordinate.lat.s) < 0)
				coordinate.lat.s = 0;

			if (parseInt(coordinate.lon.d) > 180)
				coordinate.lon.d = 180;
			else if (parseInt(coordinate.lon.d) < -180)
				coordinate.lon.d = -180;
			if (parseInt(coordinate.lon.m) >= 60)
				coordinate.lon.m = 59;
			else if (parseInt(coordinate.lon.m) < 0)
				coordinate.lon.m = 0;
			if (parseInt(coordinate.lon.s) >= 60)
				coordinate.lon.s = 59;
			else if (parseInt(coordinate.lon.s) < 0)
				coordinate.lon.s = 0;
		}

		return true;
	}
}

