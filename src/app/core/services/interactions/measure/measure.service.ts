import { CleanablesService } from '@core/services/cleanables.service';
import { Injectable, OnInit, ElementRef } from "@angular/core";
import { DrawService } from "@core/services/interactions/draw.service";
import { FixedRingService } from "@core/services/interactions/measure/fixed-ring.service";
import { LayerService } from "@core/services/layer/layer.service";
import { LogService } from "@core/services/log.service";
import { MapService } from "@core/services/map/map.service";
import { RequestService } from "@core/services/request/request.service";
import { SelectionService } from "@core/services/interactions/selection.service";
import Feature from "ol/Feature";
import Overlay from "ol/Overlay";
import * as geom from "ol/geom";
import GeometryCollection from "ol/geom/GeometryCollection";
import * as proj from "ol/proj";
import * as style from "ol/style";
import * as sphere from "ol/sphere";
import * as extent from "ol/extent";
import * as utils from "libs/seg-utils";
import * as turf from "@turf/turf";
import { template } from "lodash";

@Injectable({
	providedIn: "root"
})
export class MeasureService implements OnInit {

	tooltipScopeArray = [];
	map = this.mapService.getMap();
	measuresObjList = [];
	measureTooltipElement;
	lastMousePosition;
	sketch;
	masterTemplate;
	measureRingsLayer;
	measureLayer;

	constructor(
		private mapService: MapService,
		private layerService: LayerService,
		private logService: LogService,
		private selectionService: SelectionService,
		private drawService: DrawService,
		private requestService: RequestService,
		private cleanablesService: CleanablesService,
		private fixedRingService: FixedRingService
	) {

	}

	ngOnInit() {
		this.requestService.get("./core/services/interactions/measure/overlay.template.html")
			.subscribe(templ => this.masterTemplate = templ,
				e => this.logService.error("ERROR_MEASURE_TEMPLATE", e.status + ": " + e.message)
			);

		this.measureRingsLayer = this.layerService.create({
			id: "measureRings",
			type: "Vector",
			// commandInfo: {
			// 	templateUrl: "./core/services/interactions/measure/fixed-ring.service.html",
			// 	featureAs: "fixedRing",
			// 	bindings: {
			// 		fixedRingFunctions: this.fixedRingService,
			// 		measureService: {
			// 			getMeasureObjectElement: (feature) => {
			// 				for (let i = 0; i < this.measuresObjList.length; i++)
			// 					if (this.measuresObjList[i].measureFeature === feature)
			// 						return this.measuresObjList[i];
			// 			},
			// 			formatCircle: this.formatCircle                    ////////////////////////////////////////////////////why is it wrong????
			// 			/*
			// 			compileHTML: (html) => {
			// 				// compile html
			// 				const compiledHTML = this.el.nativeElement(html);/////////////////////////////////////////////////////////////////////
			// 				const newScope = $rootScope.$new();
			// 				const linkFun = $compile(compiledHTML);/////////////////////////////////////////////////////////////////////
			// 				const finalTemplate = linkFun(newScope);
			// 				return finalTemplate;
			// 			},*/
			// 		}
			// 	}
			// },
			source: {
				type: "Vector"
			},
			style(feature) {
				return new style.Style({
					fill: new style.Fill({
						color: "rgba(255,255,255,0.0)"
					}),
					stroke: new style.Stroke({
						color: feature.get("selected") ? "yellow" : "#3399CC",
						width: 2.5
					})
				});
			}
		});

		this.measureLayer = this.layerService.create({
			id: "measure",
			type: "Vector",
			source: {
				type: "Vector"
			},
			style(feature) {
				return new style.Style({
					fill: new style.Fill({
						color: "rgba(255,255,255,0.0)"
					}),
					stroke: new style.Stroke({
						color: feature.get("selected") ? "yellow" : "#3399CC",
						width: 2.5
					})
				});
			}
		});

		this.map.addLayer(this.measureLayer);
		this.map.addLayer(this.measureRingsLayer);

		window.addEventListener("keyup", e => {
			if (e.key === "Delete") {
				const source: any = event.target || event.srcElement;

				if (!source || !source.type || source.type !== "text")
					this.cleanSelected();
			}
		});

		this.mapService.getMap().onProjectionChange((newProj, oldProj) => {
			this.measuresObjList.forEach((measure) => {
				measure.measureMarkersOverlay.forEach((markerOverlay) => {
					const newPosition = proj.transform(markerOverlay.getPosition(), oldProj, newProj);
					markerOverlay.setPosition(newPosition);
				});
				measure.measureFeature.setGeometry(measure.measureFeature.getGeometry().transform(oldProj, newProj));

				const geometry = measure.measureFeature.getGeometry();

				if (geometry instanceof GeometryCollection) {
					const geomtryCollection = geometry.getGeometries();

					for	(let i = 0; i < geomtryCollection.length; i++) {
						geomtryCollection[i].transform(oldProj, newProj);
					}
				} else
					measure.measureFeature.setGeometry(measure.measureFeature.getGeometry().transform(oldProj, newProj));
			});
		});
	}

	measureLine() {
		this.drawService.drawLine({dragStartHandler: this.dragStartHandler})
			.subscribe((geometry) => {
				const feature = new Feature({
					geometry: geometry
				});

				this.measureTooltipElement.innerHTML = "";
				this.measureTooltipElement = null;

				this.measureLayer.getSource().addFeature(feature);

				const segments = feature.getGeometry().getCoordinates();

				const measureElements = {
					measureFeature: feature,
					measureMarkersOverlay: []
				};

				let output = "";

				for (let i = 0; i < segments.length - 1; i++) {
					const positionA = segments[i];
					const positionB = segments[i + 1];

					// coordinates of point A and B
					const coordinate = [proj.transform(positionA, this.mapService.getMap().getProjection().getCode(), "EPSG:4326"), proj.transform(positionB, this.mapService.getMap().getProjection().getCode(), "EPSG:4326")];

					// get distance and bearing bettewn points
					const formatString = this.formatLineString([positionA, positionB]);

					// set distance and bearing
					output = "<span style=\"display: block;\">Distance: <span style=\"margin-left:1px; font-weight: bold;\" distance=\"" + String(formatString[0]) + "\"></span></span> "; // distance
					output += "<span style=\"display: block;\">Bearing: <span style=\"margin-left:6px; font-weight: bold;\">" + formatString[1] + "</span></span> "; // bearing

					// create overlay html element
					const overlayHTML = document.createElement("div");
					overlayHTML.className = "tooltip tooltip-measure-fixed";

					// create overlay popup
					const popupOverlay = new Overlay({
						element: overlayHTML,
						offset: [0, -15],
						positioning: "bottom-center",
						stopEvent: false
					});

					// set overlay position
					// we use turf midpoint to determine midpoint of each segment
					// and we need to transform back and forth from EPSG:4326 to garantee accurate positioning
					popupOverlay.setPosition(
						proj.transform(
							turf.midpoint(
								turf.point(coordinate[0]),
								turf.point(coordinate[1])
								).geometry.coordinates,
							"EPSG:4326",
							this.mapService.getMap().getProjection().getCode()
							)
						);

					// add overlay to map
					this.map.addOverlay(popupOverlay);

					// compile html
						/*const finalTemplate = $compile(this.masterTemplate)(Object.assign($rootScope.$new(), {output}));/////////////////////////////////////////
						this.tooltipScopeArray.push(finalTemplate.scope());
						angular.element(overlayHTML).append(finalTemplate);/////////////////////////////////////////////////////////////////////////////
						*/
						// add to measureElements
						measureElements.measureMarkersOverlay.push(popupOverlay);
				}

					// add to measure object list
				this.measuresObjList.push(measureElements);

				// add cleanAll function
				this.cleanablesService.add("measures", this.cleanAll);///////////////////////////////////////////////////////////////cleanables doesn't exist yet
		}, e => this.logService.error("ERROR_DRAW_LINE", e.status + ": " + e.message));
	}

	measureBearingCircle() {
		this.drawService.drawCircle({pointerMoveHandler: this.bearingCirclePointerMoveHandler, dragStartHandler: this.dragStartHandler})
			.subscribe((geometry) => {
				const feature = new Feature({
					geometry: geometry
				});

				this.measureTooltipElement.innerHTML = "";
				this.measureTooltipElement = null;

				const circleGeometry = feature.getGeometry();

				// create line geometry
				// const lineGeometry = new geom.LineString([circleGeometry.getCenter(), lastMousePosition]);
				const lineGeometry = new geom.LineString([extent.getCenter(circleGeometry.getExtent()), this.lastMousePosition]);

				// create feature geom collection
				const geomCollection = new GeometryCollection([circleGeometry, lineGeometry]);

				// set feature new geomtry collection
				feature.setGeometry(geomCollection);

				// add feature to source
				this.measureLayer.getSource().addFeature(feature);

				// create object
				const measureElements = {
					measureFeature: feature,
					measureMarkersOverlay: []
				};

				// get the center position
				const centerCoord = extent.getCenter(circleGeometry.getExtent());
				// circleGeometry.getCenter();

				const lonLat4326 = proj.transform(centerCoord, this.mapService.getMap().getProjection(), "EPSG:4326");

				let output = "<div style=\"display: inline;\">Center:</div><span style=\"margin-left: 5px; font-weight: bold;\" [coordinate]=\"[" + lonLat4326[1] + ", 'lat']\"></span>";
				output += "<span style=\"margin-left: 37px; display: block; font-weight: bold;\" [coordinate]=\"[" + lonLat4326[0] + ", 'lon']\"></span> ";
				// second fase..

				// get the distance and bearing values
				// set distance and bearing
				const distanceAndBearing = this.formatCircle(circleGeometry, this.lastMousePosition);
				output += "<span style=\"display: block;\">Distance: <span style=\"margin-left:1px; font-weight: bold;\" distance=\"" + distanceAndBearing[0] + "\"></span></span> "; // distance
				output += "<span style=\"display: block;\">Bearing: <span style=\"margin-left:6px; font-weight: bold;\">" + distanceAndBearing[1] + "</span></span> "; // bearing

				// create overlay html element
				const secondOverlayHTML = document.createElement("div");
				secondOverlayHTML.className = "tooltip tooltip-measure-fixed";

				// create overlay popup
				const popupOverlay = new Overlay({
					element: secondOverlayHTML,
					offset: [0, -25],
					positioning: "bottom-center",
					stopEvent: false
				});

				// set overlay position
				const coord = extent.getCenter(feature.getGeometry().getExtent());
				coord[1] += distanceAndBearing[0];

				popupOverlay.setPosition(coord);

				// add overlay to map
				this.map.addOverlay(popupOverlay);

				// compile html
				/*const finalTemplate = $compile(this.masterTemplate)(Object.assign($rootScope.$new(), {output}));//////////////////////////////////////////////////
				this.tooltipScopeArray.push(finalTemplate.scope());
				angular.element(secondOverlayHTML).append(finalTemplate);/////////////////////////////////////////////////////////////////////////////////
				*/
				// add to measureElements
				measureElements.measureMarkersOverlay.push(popupOverlay);

				// add to measure object list the measure element
				this.measuresObjList.push(measureElements);

				// add cleanAll function
				this.cleanablesService.add("measures", this.cleanAll);///////////////////////////////////////////////////////////////cleanables doesn't exist yet
			}, e => this.logService.error("ERROR_DRAW_BEARING_CIRCLE", e.status + ": " + e.message));
	}

	measureFixedRing() {
		this.drawService.drawCircle({pointerMoveHandler: this.bearingCirclePointerMoveHandler, dragStartHandler: this.dragStartHandler})
			.subscribe((geometry) => {
				const ringFeature = new Feature({
					geometry: new GeometryCollection([new geom.Point(extent.getCenter(geometry.getExtent()))])
				});

				const output = this.formatCircle(geometry, this.lastMousePosition);

				ringFeature.sourceName = "fixedRing";

				ringFeature.set("fixedRing", true);

				// add feature to source
				this.measureRingsLayer.getSource().addFeature(ringFeature);

				// create object
				const measureElements = {
					measureFeature: ringFeature,
					measureMarkersOverlay: [],
					maxValue: output[0]
				};

				// add to measure object list the measure element
				this.measuresObjList.push(measureElements);

				// this.selectionService.select(ringFeature);///////////////////////////////////////////////////////////////selection doesn't exist yet

				// add cleanAll function
				this.cleanablesService.add("measures", this.cleanAll);///////////////////////////////////////////////////////////////cleanables doesn't exist yet
			}, e => this.logService.error("ERROR_DRAW_FIXED_CIRCLE", e.status + ": " + e.message));
	}

	measureCompass() {
		this.drawService.drawArc({ pointerMoveHandler: this.compassPointerHandler, dragStartHandler: this.dragStartHandler })
			.subscribe((geometry) => {
				const feature = new Feature({
					geometry: geometry
				});

				this.measureTooltipElement.innerHTML = "";
				this.measureTooltipElement = null;

				this.measureLayer.getSource().addFeature(feature);

				const _geom = feature.getGeometry();

				const measureElements = {
					measureFeature: feature,
					measureMarkersOverlay: []
				};

				const line1 = _geom.getLineString(0);
				const line2 = _geom.getLineString(1);

				const sourceProj = this.map.getView().getProjection();

				const a = proj.transform(line1.getFirstCoordinate(), sourceProj, "EPSG:4326");
				const b = proj.transform(line1.getLastCoordinate(), sourceProj, "EPSG:4326");
				const c = proj.transform(line2.getLastCoordinate(), sourceProj, "EPSG:4326");

				const getDistance = this.formatLineString([a, b])[0];
				const getDistance2 = this.formatLineString([a, c])[0];
				let radius = (getDistance > getDistance2 ? getDistance : getDistance2) * 0.5;//////////////////Why doesn't it work?//////////////////////
				radius = 0.01;

				// center, first coordinate, second coordinate
				let output = utils.getAngleBetweenCandB(a, b, c);
				// const radians = output * (Math.PI / 180);

				const polygon = turf.polygon([
					[
					[a[0] * 1000, a[1] * 1000],
					[b[0] * 1000, b[1] * 1000],
					[c[0] * 1000, c[1] * 1000],
					[a[0] * 1000, a[1] * 1000]
					]
					]);

				for (let newRadians = 0 ; newRadians < 2 * Math.PI; newRadians = newRadians + 0.001) {
					const x = radius * Math.cos(newRadians); // + radio;
					const y = radius * Math.sin(newRadians); // + radio;

					const x2 = radius * Math.cos(newRadians + 0.01); // + radio;
					const y2 = radius * Math.sin(newRadians + 0.01); // + radio;


					const coordinateNewLine = [a[0] - x, a[1] - y];
					const coordinateNewLine2 = [a[0] - x2, a[1] - y2];

					const point = turf.point([coordinateNewLine[0] * 1000, coordinateNewLine[1] * 1000]);
					const point2 = turf.point([coordinateNewLine2[0] * 1000, coordinateNewLine2[1] * 1000]);


					if (turf.inside(point, polygon))
						if (turf.inside(point2, polygon))
							_geom.appendLineString(new geom.LineString([coordinateNewLine, coordinateNewLine2]));
				}
				// _geom.transform("EPSG:4326",sourceProj);

				output = "<span style=\"display: block;\">Angle: <span style=\"margin-left:6px; font-weight:bold;\">" + output.toFixed(2) + "°" + "</span></span> ";
				// bearing
				// create overlay html element
				const overlayHTML = document.createElement("div");
				overlayHTML.className = "tooltip tooltip-measure-fixed";

				// create overlay popup
				const popupOverlay = new Overlay({
					element: overlayHTML,
					offset: [0, -15],
					positioning: "bottom-center",
					stopEvent: false
				});

				// set overlay position
				popupOverlay.setPosition(line1.getFirstCoordinate());

				// add overlay to map
				this.map.addOverlay(popupOverlay);

					/*const finalTemplate = $compile(this.masterTemplate)(Object.assign($rootScope.$new(), {output}));////////////////////////////
					this.tooltipScopeArray.push(finalTemplate.scope());
					angular.element(overlayHTML).append(finalTemplate);/////////////////////////////////////////////////////////////////////
					*/
					// add to measureElements
					measureElements.measureMarkersOverlay.push(popupOverlay);

					// add to measure object list
					this.measuresObjList.push(measureElements);

					// add cleanAll function
					this.cleanablesService.add("measures", this.cleanAll);///////////////////////////////////////////////////////////////cleanables doesn't exist yet
			}, e => this.logService.error("ERROR_DRAW_ARC", e.status + ": " + e.message));
	}

	removeFeature(feature) {
		const selected = this.selectionService.getSelectedFeatures(); ///////////////////////////////////////////////////////////////cleanables doesn't exist yet
		this.selectionService.deselect(selected); ///////////////////////////////////////////////////////////////cleanables doesn't exist yet
		if(feature.get("fixedRing")) {
			this.measureRingsLayer.getSource().removeFeature(feature);
		} else {
			this.measureLayer.getSource().removeFeature(feature);
		}
	}

	cleanAll() {
		this.measuresObjList.forEach((measure) => {
			measure.measureMarkersOverlay.forEach((markerOverlay) => {
				this.map.removeOverlay(markerOverlay);
			});

			this.removeFeature(measure.measureFeature);
		});

		if (this.tooltipScopeArray.length) {
			this.tooltipScopeArray.forEach((item, index, object) => {
				if (item && item.$destroy) {
					item.$destroy();
					object.splice(index, 1);
				}
			});
		}

		this.measuresObjList = [];
	}

	bearingCirclePointerMoveHandler(evt) {
		if (evt.dragging) { return; }

		const tooltipCoord = evt.coordinate;

		if (this.sketch)
			this.lastMousePosition = tooltipCoord;
	}

	compassPointerHandler(evt) {
		if (evt.dragging)
			return;

		const tooltipCoord = evt.coordinate;

		if (this.sketch)
			this.lastMousePosition = tooltipCoord;
	}

	formatCircle(circle, mouseCoordinate) {
		const sourceProj = this.map.getView().getProjection(),
		c1 = proj.transform(extent.getCenter(circle.getExtent()), sourceProj, "EPSG:4326"),
		c2 = proj.transform(mouseCoordinate, sourceProj, "EPSG:4326"),
		radius = sphere.getDistance(c1, c2),
		bearing = this.getAngleOfLineBetweenTwoCoordinates([c1, c2]),
		output = (Math.round(radius * 100) / 100); // meters
		return [output, bearing.toFixed(2) + "°"];
	}

	formatLineString(coordinates): any[] {
		let length = 0;
		let lastBearing = 0;

		const sourceProj = this.map.getView().getProjection();
		for (let i = 0, ii = coordinates.length - 1; i < ii; ++i) {
			const c1 = proj.transform(coordinates[i], sourceProj, "EPSG:4326"),
			c2 = proj.transform(coordinates[i + 1], sourceProj, "EPSG:4326");

			length += sphere.getDistance(c1, c2);

			const newBearing = this.getAngleOfLineBetweenTwoCoordinates([c1, c2]);

			if (newBearing !== 0) {
				lastBearing = newBearing;
			}
		}

		const output = (Math.round(length * 100) / 100); // meters

		return [output, lastBearing.toFixed(2) + "°"];
	}

	getAngleOfLineBetweenTwoCoordinates(coordinates) {
		const sourceProj = this.map.getView().getProjection();

		const c1 = proj.transform(coordinates[0], sourceProj, "EPSG:4326");
		const c2 = proj.transform(coordinates[1], sourceProj, "EPSG:4326");

		const X = 0;
		const Y = 1;

		const coords1 = { x: c1[X], y: c1[Y]};
		const coords2 = { x: c2[X], y: c2[Y]};

		if (coords1.x === coords2.x && coords1.y === coords2.y) {
			return 0;
		}

		let azimut = 0;

		// If we  calculate the angle of the vector in a cartesian plane.
		// const radians = Math.atan((coords2.y - coords1.y) / (coords2.x - coords1.x));
		// let ang = radians * 180 / Math.PI;

		// atan throws a 0 - 90º value....but we wanted  a 360º value....
		// To do so
		const xx = coords2.x - coords1.x;
		const yy = coords2.y - coords1.y;

		if (xx > 0)
			azimut = 90 - (Math.atan(yy / xx) * 180 / Math.PI);
		else if (xx < 0)
			azimut = 270 - (Math.atan(yy / xx) * 180 / Math.PI);
		else if (xx === 0)
			if (yy > 0)
				azimut = 0;
			else
				azimut = 180;

			return azimut;
	}

	cleanSelected() {
		for (let i = 0; i < this.measuresObjList.length; i++) {
			if (this.measuresObjList[i].measureFeature.get("selected"))	{
				this.measuresObjList[i].measureMarkersOverlay.forEach((markerOverlay) => {
					this.map.removeOverlay(markerOverlay);
				});

				this.removeFeature(this.measuresObjList[i].measureFeature);

				//this.selectionService.deselect([this.measuresObjList[i].measureFeature]);///////////////////////////////////////////////////////////////selection doesn't exist yet

				this.measuresObjList.splice(i, 1);
			}
		}

		if (this.measuresObjList.length === 0) {
			this.cleanablesService.remove("measures");///////////////////////////////////////////////////////////////cleanables doesn't exist yet
		}
	}

	dragStartHandler = (evt) => {
		const createMeasureTooltip = () => {
			if (this.measureTooltipElement && this.measureTooltipElement.parentNode) {
				this.measureTooltipElement.parentNode.removeChild(this.measureTooltipElement);
			}

			this.measureTooltipElement = document.createElement("div");
			this.measureTooltipElement.className = "tooltip tooltip-measure";
		};

		createMeasureTooltip();
		// set sketch
		this.sketch = evt.feature;
	}
}
