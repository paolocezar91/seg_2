import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { createRegularPolygon } from "ol/interaction/Draw";
import Draw from "ol/interaction/Draw.js";
import MultiLineString from "ol/geom/MultiLineString";
import LineString from "ol/geom/LineString";
import Polygon from "ol/geom/Polygon.js";
import { circular as circularPolygon } from "ol/geom/Polygon";
import * as condition from "ol/events/condition";
import * as proj from "ol/proj";

import { MapService } from "@core/services/map/map.service";
import * as utils from "libs/seg-utils";


@Injectable({
	providedIn: "root"
})
export class DrawService {

	map;
	active: boolean = false;
	unRegisterHandlersOnNewDraw = [];
	lastInteraction: Draw;
	drawInteract: Draw;

	constructor(
		private mapService: MapService
	) {
	}

	drawSquare(options?) {
		return this.draw(Object.assign({
			type: "Circle",
			freehandCondition: condition.never,
			geometryFunction: createRegularPolygon(4)
		}, options));
	}

	drawCircle(options?) {
		return this.draw(Object.assign({
			type: "Circle",
			freehandCondition: condition.never,
			//geometryFunction: createRegularPolygon(0)
			geometryFunction: (coordinates, opt_geometry) => {
				const [center, end] = coordinates,
					radius = Math.sqrt(utils.squaredDistance(center, end)),
					angle = Math.atan((end[1] - center[1]) / (end[0] - center[0]));

				let geometry = opt_geometry ? opt_geometry :
					circularPolygon(
						proj.toLonLat(center, this.mapService.getMap().getProjection()),
						radius
					);

				geometry = createRegularPolygon(null, angle)(coordinates, geometry);

				geometry.setProperties({
					"end": end,
					"type": "circle",
					"center": center,
					"radius": radius
				});
				return geometry;
			}
		}, options));
	}

	drawRectangle(options?) {
		return this.draw(Object.assign({
			type: "LineString",
			maxPoints: 2,
			freehandCondition: condition.never,
			geometryFunction: (coordinates, geometry) => {
				if (!geometry)
					geometry = new Polygon([[0,0]]);

				const [start, end] = coordinates;

				geometry.setCoordinates([
					[start, [start[0], end[1]], end, [end[0], start[1]], start]
				]);
				geometry.set("type", "rectangle");

				return geometry;
			}
		}, options));
	}

	drawPolygon(options?) {
		return this.draw(Object.assign({
			type: "Polygon"
		}, options));
	}

	drawLine(options?) {
		return this.draw(Object.assign({
			type: "LineString",
			freehandCondition: condition.never
		}, options));
	}

	//drawArc = function(center, radius, alpha, omega, segments, flag)
	drawArc(options?) {
		return this.draw(Object.assign({
			type: "LineString",
			freehandCondition: condition.never,
			maxPoints: 3,
			geometryFunction: (coordinates, geometry) => {
				const [coord1, coord2, coord3] = coordinates,
					line1Points = [coord1, coord2],
					line2Points = coord3 ? [coord1, coord3] : [];

				if (!geometry)
					geometry = new MultiLineString([[[0,0], [0,0]]]);
				else geometry.setCoordinates([]);

				geometry.appendLineString(new LineString(line1Points));

				if (coord3) {
					geometry.appendLineString(new LineString(line2Points));
				}

				return geometry;
			}
		}, options));
	}

	drawPoint(options?) {
		return this.draw(Object.assign({
			type: "Point"
		}, options));
	}

	createInteraction(callBackDrawEnd) {
		this.cleanInteraction();

		this.drawInteract = new Draw({
			type: "Point"
		});

		this.mapService.getMap().addInteraction(this.drawInteract);

		this.drawInteract.on("drawend", (e) => {
			setTimeout(() => {
				this.cleanInteraction();
				callBackDrawEnd(e.feature.getGeometry().getCoordinates());
			});
		});

		this.lastInteraction = this.drawInteract;
	}

	cleanInteraction() {
		if (this.lastInteraction)
			this.mapService.getMap().removeInteraction(this.lastInteraction);
	}

	addNewRegisterHandlersKey(key) {
		this.unRegisterHandlersOnNewDraw.push(key);
	}

	removeNewRegisterHandlersKey(key) {
		for (let i = 0; i < this.unRegisterHandlersOnNewDraw.length; i++) {
			if (this.unRegisterHandlersOnNewDraw[i] === key) {
				this.unRegisterHandlersOnNewDraw.splice(i, 1);
			}
		}
	}

	draw(options) {
		this.cleanInteraction();
		this.active = true;
		this.drawInteract = new Draw(options);
		this.mapService.getMap().addInteraction(this.drawInteract);
		this.lastInteraction = this.drawInteract;
		this.unRegisterHandlersOnNewDraw = [];

		if (typeof options.pointerMoveHandler === "function")
			this.unRegisterHandlersOnNewDraw.push(this.mapService.getMap().ol_.on("pointermove", options.pointerMoveHandler));

		if (typeof options.dragStartHandler === "function")
		this.drawInteract.on("drawstart", options.dragStartHandler);

		return Observable.create((obs) => {
			this.drawInteract.on("drawend", (e) => {
				for (let i = 0; i < this.unRegisterHandlersOnNewDraw.length; i++)
					this.mapService.getMap().ol_.un("pointermove", this.unRegisterHandlersOnNewDraw[i]);

				setTimeout(() => {
					this.mapService.getMap().removeInteraction(this.drawInteract);

					const geo = e.feature.getGeometry();

					geo.set("draw", true);

					this.active = false;
					obs.next(geo);
				});
			});
		});
	}
}
