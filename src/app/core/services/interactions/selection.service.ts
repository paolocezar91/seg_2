import { Injectable } from "@angular/core";
import * as $o from "libs/seg-object";
import { MapService } from "@core/services/map/map.service";
import { DrawService } from "@core/services/interactions/draw.service";

@Injectable({
	providedIn: "root"
})
export class SelectionService {

	map;
	selectedFeatures = [];
	selectionListeners = new $o.EventListenerQueue();

	constructor(
		private mapService: MapService,
		private drawService: DrawService
	) {
	}

	registerClickToMap() {
		this.map = this.mapService.getMap();
		// Registering clicks for map
		this.map.ol_.on("click", e => {
			if (!this.drawService.active) { // if drawService is active, other mouse interactions should not work
				let featuresAtPixel = [], preferentialFeaturesAtPixel = [];

				// collect features at pixel
				this.map.ol_.forEachFeatureAtPixel(e.pixel, feature => {
					featuresAtPixel.push(feature);
				});

				// if none, deselect all
				if (!featuresAtPixel.length){
					this.select([], e.originalEvent.shiftKey);
				} else {
					// else, look for next feature to be selected
					let lastIndex = -1;
					// get index of last selected feature
					preferentialFeaturesAtPixel = featuresAtPixel.filter(ft => this.map.getPreferentialLayers().some(layer => layer === ft.get("seg_type")));

					if (preferentialFeaturesAtPixel.length)
						featuresAtPixel = preferentialFeaturesAtPixel;

					featuresAtPixel.forEach((ft, i) => ft.get("selected") && (lastIndex = i));
					/*
					and select next feature mod featuresAtPixel.length
					this will allow us to cycle through overlapping features
					*/
					this.select(featuresAtPixel[(lastIndex + 1) % featuresAtPixel.length], e.originalEvent.shiftKey, e);
				}
			}
		});
	}

	select(featuresToSelect, cumulative?, event?) {
		if (!cumulative)
			this.deselect(this.selectedFeatures);

		featuresToSelect = [].concat(featuresToSelect).filter(feature => this.selectedFeatures.indexOf(feature) === -1);

		featuresToSelect.forEach(feature => {
			feature.set("selected", true);
			this.selectedFeatures.push(feature);
		});

		this.selectionListeners.trigger(featuresToSelect, this.selectedFeatures, [], event);
	}

	onSelectionChange(callback) {
		return this.selectionListeners.register(callback);
	}

	deselect(featuresToDeselect) {
		featuresToDeselect = [].concat(featuresToDeselect);

		featuresToDeselect.forEach(feature => feature.set("selected", false));

		this.selectedFeatures = this.selectedFeatures.filter(feature => featuresToDeselect.indexOf(feature) === -1);

		this.selectionListeners.trigger([], this.selectedFeatures, featuresToDeselect);
	}

	getSelectedFeatures() {
		return this.selectedFeatures;
	}

	selectFeatureAndCenter(feature, cumulative, maxZoom) {
		this.select(feature, cumulative);

		if (feature && (!Array.isArray(feature) || feature.length))
			this.mapService.getMap().centerOn(feature, maxZoom);
	}

	centerFeature(feature, maxZoom) {
		if (feature && (!Array.isArray(feature) || feature.length))
			this.mapService.getMap().centerOn(feature, maxZoom);
	}

	getSelected(id) {
		return typeof id === "string" ?
			this.mapService.getMap().findLayerById(id).getSource().getFeatures().filter(feature => feature.get("selected")) :
			this.getSelectedFeatures();
	}
}
