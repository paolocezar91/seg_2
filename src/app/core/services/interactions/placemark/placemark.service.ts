import { StyleService } from "@core/services/style.service";
import { TablesService } from "@main/services/ttt/tables.service";
import { PreferencesService } from "@core/services/preferences.service";
import { FavouritesService } from "@core/services/favourites.service";
import { SelectionService } from "@core/services/interactions/selection.service";
import { DrawService } from "@core/services/interactions/draw.service";
import { LayerService } from "@core/services/layer/layer.service";
import { MapService } from "@core/services/map/map.service";
import { Injectable } from "@angular/core";
import * as format from "ol/format";
import * as style from "ol/style";
import * as Feature from "ol/Feature";
import * as proj from "ol/proj";
import * as condition from "ol/events/condition";
import * as utils from "libs/seg-utils";
import { HttpClient } from "@angular/common/http";

@Injectable({
	providedIn: "root"
})
export class PlacemarkService {

	placemarksObjList = [];
	DEFAULT_PLACEMARK_COLOR = "#001e87";
	map = null;
	placemarkIcon;
	placemarkLayer = null;
	layer = null;

	constructor(
		private layerService: LayerService,
		private mapService: MapService,
		private drawService: DrawService,
		private selectionService: SelectionService,
		private styleService: StyleService,
		// private searchService: searchService,
		private tttTables: TablesService,
		private favouritesService: FavouritesService,
		private preferencesService: PreferencesService,
		private http: HttpClient
	) { }

	async init() {
		this.map = this.mapService.getMap();
		const placemarkerString = await this.styleService.requestSVG("assets/icons/ui/placemarker.svg").toPromise();
		this.placemarkIcon = utils.htmlToElement(placemarkerString);

		this.placemarkLayer = this.layerService.create({
			id: "placemarks",
			type: "Vector",
			renderMode: "image",
			name: "Placemarks",
			visible: true,
			source: {
				type: "Vector",
				projection: "EPSG:4326",
				renderBuffer: 20
			},
			style: (feature) => {
				const styles = [];

				if (feature.get("isSearchResult"))
					styles.push(new style.Style({
						image: new style.Circle({
							radius: this.getPlacemarkInSearchResultSize() / 2,
							fill: new style.Fill({
								color: "rgba(88, 166, 74, .5)"
							})
						})
					}));

				if (feature.get("selected"))
					styles.push(new style.Style({
						image: new style.Circle({
							radius: this.getPlacemarkSelectedSize() / 2,
							stroke: new style.Stroke({
								color: "#006ebc",
								width: 2
							})
						})
					}));

				if (this.favouritesService.isFavourite(feature))
					styles.push(new style.Style({
						image: new style.Circle({
							radius: (this.getPlacemarkSelectedSize() - 4) / 2,
							stroke: new style.Stroke({
								color: "rgb(201, 151, 38)",
								width: 2
							})
						})
					}));

				styles.push(new style.Style({
					image: this.styleService.SVG({
						svg: this.placemarkIcon,
						height: this.getPlacemarkHeight(),
						width: this.getPlacemarkWidth(),
						fill: feature.get("color") || this.DEFAULT_PLACEMARK_COLOR
					})
				}));

				return styles;
			},
			tooltip: {
				featureAs: "pmFeature",
				template: `<window>
	<window-title>
		<div id="info">
			<div class="text-size-sm">
				<value>{{pmFeature.get('name')}}</value>
			</div>
		</div>
	</window-title>
	<window-content class="background-color-3">
		<section class="border-bottom-radius">
			<div id="info" class="text-size-xs">
				<div>
					<label>Name:</label> <b><value>{{pmFeature.get('name')}}</value></b>
				</div>
				<div ng-if="pmFeature.get('label')">
					<label>Label:</label> <b><value>{{pmFeature.get('label')}}</value></b>
				</div>
				<div ng-if="pmFeature.get('description')">
					<label>Description:</label> <b><value>{{pmFeature.get('description')}}</value></b>
				</div>
			</div>
		</section>
	</window-content>
</window>`
			},
			commandInfo: {
				featureAs: "pmFeature",
				onClose: (pmFeature) => {
					if (!pmFeature.get("saved"))
						this.placemarkLayer.getSource().removeFeature(pmFeature);
				},
				bindings: {
					placemarkFunctions: {
						placemarkFeature: null,
						placemarkModel: null,
						saved: false,
						init(pmFeature) {
							this.placemarkFeature = pmFeature;

							const coordinates = proj.transform(pmFeature.getGeometry().getCoordinates(), this.mapService.getMap().getProjection(), "EPSG:4326");

							this.placemarkModel = Object.assign({
								label: pmFeature.get("label"),
								name: pmFeature.get("name"),
								description: pmFeature.get("description"),
								color: pmFeature.get("color") || this.DEFAULT_PLACEMARK_COLOR
							}, {coordinates});
						},
						coordinateLatOptions: ["N", "S"],
						coordinateLonOptions: ["E", "W"],
						isValidToSet() {
							const placemark = this.placemarkModel;

							if (!placemark || !placemark.name || !placemark.label) {
								return false;
							}

							const coordinates = placemark.coordinates;

							if (parseInt(coordinates[0], 10) < -180)
								coordinates[0] = -179.99999;
							if (parseInt(coordinates[0], 10) > 180)
								coordinates[0] = 179.99999;

							if (parseInt(coordinates[1], 10) < -90)
								coordinates[1] = -89.99999;
							if (parseInt(coordinates[1], 10) > 90)
								coordinates[1] = 89.99999;

							return true;
						},
						setPlacemarkClick() {
							const placemark = this.placemarkModel,
								pmFeature = this.placemarkFeature;

							pmFeature.set("label", placemark.label);
							pmFeature.set("name", placemark.name);
							pmFeature.set("description", placemark.description);
							pmFeature.set("color", placemark.color);
							pmFeature.set("saved", true);

							const projection = this.mapService.getMap().getProjection();

							const lonLat = proj.transform(pmFeature.getGeometry().getCoordinates(), projection, "EPSG:4326");
							pmFeature.set("lon", lonLat[0]);
							pmFeature.set("lat", lonLat[1]);

							let found = false;
							for (let i = 0; i < this.placemarksObjList.length; i++) {
								if (this.placemarksObjList[i] === pmFeature) {
									found = true;
									break;
								}
							}

							if (!found) {
								let id = 0;
								if (this.placemarksObjList.length > 0) {
									const id_str = this.placemarksObjList[this.placemarksObjList.length - 1].getId();
									id = parseInt(id_str.substring(id_str.indexOf("_") + 1), 10);
								}

								id++;

								pmFeature.setId("PLACEMARK_" + id);
								this.placemarksObjList.push(pmFeature);
							}

							this.savePlacemarks();
							setTimeout(() => this.placemarkLayer.getSource().changed(), 100);
						},
						showDeleteBtn() {
							return this.placemarkFeature.get("label") && this.placemarkFeature.get("name");
						},
						deletePlacemark() {
							this.removeFeature(this.placemarkFeature, true);
						},
						updateCoordinates() {
							const projection = this.mapService.getMap().getProjection(),
								lonLat = proj.transform(this.placemarkModel.coordinates, "EPSG:4326", projection);

							this.placemarkFeature.getGeometry().setCoordinates(lonLat);
						}
					}
				},
				templateUrl: "placemark.template.html"
			}
		});

		this.placemarkLayer.ol_.set("order", this.map.layers.length);
		this.placemarkLayer.ol_.set("order", this.map.layers.length);

		this.map.addLayer(this.placemarkLayer);

		this.layer = this.placemarkLayer;

		/*this.searchService.register(this.placemarkLayer, {
			keys: ["name"],
			label: "label"
		});*/

		this.tttTables.register({
			customToolbar: {
				template: "<span class=\"text-size-sm\">Number of Placemarks: <b>{{grid.filteredRows.length}}</b></span>"
			},
			label: "Placemarks",
			data: this.placemarksObjList,
			itemAs: "placemark",
			fields: {
				name: {
					label: "Name",
					template: "{{placemark.get(\"name\")}}",
					visible: true
				},
				label: {
					label: "Label",
					template: "{{placemark.get(\"label\")}}",
					visible: true
				},
				latitude: {
					label: "Latitude",
					template: "<span [coordinate]=\"[placemark.get('lat'), 'lat']\"></span>",
					visible: true
				},
				longitude: {
					label: "Longitude",
					template: "<span [coordinate]=\"[placemark.get('lon'), 'lon']\"></span>",
					visible: true
				}
			},
			onSelectionChange(changedRows, selectedRows) {
				this.selectionService.selectFeatureAndCenter(selectedRows.map(row => row.item));
			}
		});

		this.placemarkLayer.ol_.on("change:visible", this.lazyLoader);
		this.lazyLoader();

	}

	getLayer() {
		return this.layer;
	}

	lazyLoader() {
		if (!this.placemarkLayer.getVisible())
			return;

		if (!this.placemarkLayer.get("loading")) {
			this.placemarkLayer.set("loading", true);
			let savedPlacemarks = this.preferencesService.getOperationPreference("placemarks");

			if (savedPlacemarks) {
				savedPlacemarks = (new format.GeoJSON()).readFeatures(savedPlacemarks, {
					dataProjection: "EPSG:4326",
					featureProjection: this.mapService.getMap().getProjection()
				});

				this.placemarksObjList.push(...savedPlacemarks);
				this.placemarkLayer.getSource().addFeatures(savedPlacemarks);
			}
		}
	}

	// XXX refactor this and the serialization function in favourites to the same
	savePlacemarks() {
		return this.preferencesService.setOperationPreference("placemarks", JSON.parse((new format.GeoJSON()).writeFeatures(this.placemarksObjList.map(placemark => {
			const properties = placemark.getProperties();

			delete properties.layers;
			delete properties.selected;

			const feature = new Feature(properties);

			feature.setId(placemark.getId());

			return feature;
		}), {
			featureProjection: this.mapService.getMap().getProjection(),
			dataProjection: "EPSG:4326"
		})));
	}


	drawPlacemark() {
		return this.drawService.drawPoint({
			freehandCondition: condition.never
		});
	}

	removeFeature(feature) {
		for (let i = 0; i < this.placemarksObjList.length; i++) {
			if (this.placemarksObjList[i] === feature) {
				this.placemarksObjList.splice(i, 1);
				break;
			}
		}

		const selected = this.selectionService.getSelectedFeatures();
		this.selectionService.deselect(selected);
		this.placemarkLayer.getSource().removeFeature(feature);

		this.savePlacemarks();
		this.placemarkLayer.getSource().changed();
	}

	getPlacemarkHeight() {
		const value = Math.round(6600000 / this.map.getScale()) * 12;

		if (value > 32)
			return 32;
		if (value < 6)
			return 6;

		return value;
	}

	getPlacemarkWidth() {
		const value = Math.round(6600000 / this.map.getScale()) * 12;

		if (value > 20)
			return 20;
		if (value < 4)
			return 4;

		return value;
	}

	getPlacemarkSelectedSize() {
		const value = Math.round(6600000 / this.map.getScale()) * 12;

		if (value > 32)
			return 32;
		if (value < 1)
			return 1;

		return value;
	}

	getPlacemarkInSearchResultSize() {
		const value = Math.round(7000000 / this.map.getScale()) * 12;

		if (value > 25)
			return 25;
		if (value < 1)
			return 1;

		return value;
	}
}
