import { Injectable } from "@angular/core";
import { RequestService } from "@core/services/request/request.service";
import { LogService } from "@core/services/log.service";
import { LayerService } from "@core/services/layer/layer.service";
import { merge } from "rxjs";
import { map } from "rxjs/operators";


const COUNTRY_TYPES = ["MEMBER_STATE", "LRIT_FLAG_TYPE", "EULRITDC_TYPE"],
	flagCache = {},
	locodeCache = {},
	countryListCache = {};

@Injectable({
  providedIn: "root"
})
export class CountryService {

	constructor(
		private requestService: RequestService,
		private logService: LogService,
		private layerService: LayerService
	) { }

	init() {
		const countryTypes$ = COUNTRY_TYPES.map(type => this.requestService.get("v1/ccd/country_list?", {
				params: {
					country_type: type
				},
				timeout: 5 * 1000
			}).pipe(map((res: any) => {
				countryListCache[type] = res.result.CountryList[0].Country;
			}, e => this.logService.error("ERROR_COUNTRIES_INIT", e.error + ": " + e.result))
		));

		merge(countryTypes$).subscribe();
	}

	getFromLocode(locode) {
		if (typeof locode === "undefined")
			return null;


		if (locodeCache[locode])
			return new Promise(resolve => resolve(locodeCache[locode]));

		const ports = this.layerService.get("ports").getSource().getFeatures();
		if (ports.length) {
			const port = ports.find(_port => _port.get("locode") === locode);

			if (port)
				locodeCache[locode] = port.getProperties();

			if (locodeCache[locode])
				return new Promise(resolve => resolve(locodeCache[locode]));
		}

		return this.requestService.get("v1/cld/locodes?", {
			params: {
				locode
			}
		}).toPromise().then((res: any) => {
			const {result} = res;
			locodeCache[locode] = result;
			return locodeCache[locode];
		}, e => this.logService.error("ERROR_GET_FROM_LOCODE", e.error + ": " + e.result));
	}

	getFlag(code) {
		if (flagCache[code])
			return new Promise(resolve => resolve(flagCache[code]));

		if (code) {
			const img = new Image();
			img.src = "./assets/flags/ic_" + code.toLowerCase() + ".png";

			flagCache[code] = "./assets/flags/ic_" + code.toLowerCase() + ".png";
			return new Promise(resolve => resolve(flagCache[code]));
		}

		return new Promise(resolve => resolve(null));

		/* this service will never ever never ever never work AMOR DE SEG 1975
		if (typeof code === "undefined" || code === ""  )
			return $q.reject();//XXX


		return $q.resolve(this.requestService.get("v1/ccd/country_flag?", { params: {
			alpha_code: code,
			image_type: "S"
		}}).then(res=>{
			let img =  "data:image/png;base64,"+res.result.CountryImage.Image.Small;
			flagCache[code] = img;
			return img;
		}, error => {

			let img = new Image();
			img.src= "./assets/flags/ic_"+code.toLowerCase()+".png";

			flagCache[code] = "./assets/flags/ic_"+code.toLowerCase()+".png";
			return "./assets/flags/ic_"+code.toLowerCase()+".png";
		}))
		*/
	}

	getCountries(type) {
		if (typeof type === "undefined" || type === "" || type === null)
			type = "MEMBER_STATE";
		return countryListCache[type];
	}

	getCountriesRequest(type) {
		if (countryListCache[type])
			return countryListCache[type];

		const params = (typeof type === "undefined") ? {country_type: "MEMBER_STATE"} : {country_type: type};

		return this.requestService.get("v1/ccd/country_list?", {params}).toPromise().then((res: any) => {
			countryListCache[type] = res.result.CountryList[0].Country;
			return countryListCache[type];
		}, e => this.logService.error("ERROR_GET_COUNTRIES_REQUEST", e.error + ": " + e.result));
	}
}

