import { Injectable } from "@angular/core";
import { RequestService } from "@core/services/request/request.service";
import { PreferencesService } from "@core/services/preferences.service";
import { Observable } from "rxjs";
declare const extensibility: any;

@Injectable({
  providedIn: "root"
})
export class OperationService {

  currentOperation: any;

  	constructor(
  		private requestService: RequestService,
  		private preferencesService: PreferencesService
	) {	}

	parseOperations(user) {
		let roles, parsedOperations;

		// Use case for user info v1 and v2
		if (!user.v2) { // v1
			roles = [].concat(...user.applications.map(application => application.groups));
			parsedOperations = [];
		} else { // v2
			roles = [].concat(...user.rolesInfo.map(({code}) => code));
			parsedOperations = [].concat(...user.operationsInfo.map(({code}) => code));
		}

		return {roles, parsedOperations};
	}

	loadOperation(op, modules) {
		return Observable.create(obs => {
			const operation = op.config(modules);
			operation.modules = operation.modules.filter(module => !!module);

			Object.assign(operation, op); // add operation config properties to operation object
			Object.assign(window.seg, {operation}); // add reference to operation to API

			this.requestService.setProject(op.project);
			// requestService.setOperations(op.alias);

			this.currentOperation = operation;
			this.preferencesService.loadOperationPreferences(operation)
				.subscribe((done) => {
					if (done)
						obs.next(operation);
					else
						obs.error();
				});
		});
	}

	getCurrentOperation() {
		return this.currentOperation;
	}

	getValidOperation = (operations: Array<any>, _global) => {
		const latestOperation = this.preferencesService.getSessionPreference("latestOperation"),
			sessionOperation = sessionStorage.getItem("operation"),
			latestGlobalOperation = _global.operation;

		const operationString = "OPR_MSPILOT" || latestOperation || sessionOperation || latestGlobalOperation ||  Object.values(operations)[0].alias;

		if (latestOperation === "undefined") {
			this.preferencesService.setSessionPreference("latestOperation", operationString);
			sessionStorage.setItem("operation", operationString);
		}

		// prevent user access to operation they don"t have
		return operations.find(op => op.alias === operationString) || operations[0];
	}
}
