import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  	LOG: number = 0;
	ERROR: number = 1;
	messages: Array<any> = [];
  	
	constructor() { }

	/**
	 *	@method seg.log
	 *	@param {string} tag
	 *	@param {string} message
	 *	@param {*} data
	*/
	log(tag: string, message: string, data?, level = this.LOG) {
		const Console = console,
			event = {
				timestamp: Date.now(),
				tag,
				message,
				data,
				level
			};

		this.messages.push(event);

		const logArgs = [message, data].filter(item => typeof item !== "undefined");

		if(level === this.LOG)
			Console.log(tag+":", ...logArgs);
		else if(level === this.ERROR)
			Console.error(tag+":", ...logArgs);

		return event;
	};

	/**
	 *	@method seg.log.error
	 *	@param {string} tag
	 *	@param {string} message
	 *	@param {*} data
	*/
	error(tag: string, message: string, data? : any) {
		return this.log(tag, message, data, this.ERROR)
	};

	/**
 	 *	@method seg.log.getLogs
 	 *	@returns {object[]} logs
	*/
	getLogs() {
		return this.messages
	};

}
