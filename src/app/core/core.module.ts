import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, COMPILER_OPTIONS, CompilerFactory, Compiler } from "@angular/core";
import { UiModule } from "@ui/ui.module";
import { CommonModule } from "@angular/common";
import { CleanablesService } from "@core/services/cleanables.service";
import { DrawService } from "@core/services/interactions/draw.service";
import { GeometryService } from "@core/services/geometry.service";
import { InterfaceService } from "@core/services/interface.service";
import { LayerService } from "@core/services/layer/layer.service";
import { LocationService } from "@core/services/location.service";
import { LogService } from "@core/services/log.service";
import { MapService } from "@core/services/map/map.service";
import { MeasureService } from "@core/services/interactions/measure/measure.service";
import { OperationService } from "@core/services/operation.service";
import { PlacemarkService } from "@core/services/interactions/placemark/placemark.service";
import { ProjectionService } from "@core/services/map/projection.service";
import { RequestService } from "@core/services/request/request.service";
import { ScriptsService } from "@core/services/request/scripts.service";
import { SelectionService } from "@core/services/interactions/selection.service";
import { ShortcutsService } from "@core/services/shortcuts.service";
import { SourceService } from "@core/services/layer/source.service";
import { StyleService } from "@core/services/style.service";
import { UserService } from "@core/services/user.service";

import { JitCompilerFactory } from "@angular/platform-browser-dynamic";
export function createCompiler(compilerFactory: CompilerFactory) {
	return compilerFactory.createCompiler();
}

@NgModule({
	declarations: [],
	exports: [

	],
	imports: [
		CommonModule,
		UiModule
	],
	providers: [
		CleanablesService,
		DrawService,
		InterfaceService,
		LayerService,
		LocationService,
		LogService,
		MapService,
		MeasureService,
		OperationService,
		PlacemarkService,
		ProjectionService,
		RequestService,
		ScriptsService,
		SelectionService,
		ShortcutsService,
		SourceService,
		StyleService,
		GeometryService,
		UserService,
		{provide: COMPILER_OPTIONS, useValue: {}, multi: true},
		{provide: CompilerFactory, useClass: JitCompilerFactory, deps: [COMPILER_OPTIONS]},
		{provide: Compiler, useFactory: createCompiler, deps: [CompilerFactory]}
	],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ]
})
export class CoreModule { }
