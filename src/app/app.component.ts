import { Component, OnInit, Input } from "@angular/core";
import { Map } from "ol";
import { Observable, forkJoin, of, throwError, zip, merge, combineLatest } from "rxjs";
import { mergeMap, concatMap, catchError, take, map, first, finalize, takeWhile, switchMap } from "rxjs/operators";
import * as moment from "moment";

// CORE
import { ConfigService } from "@core/services/config.service";
import { FavouritesService } from "@core/services/favourites.service";
import { InfoService } from "@core/services/info.service";
import { InterfaceService } from "@core/services/interface.service";
import { LayerService } from "@core/services/layer/layer.service";
import { LocationService } from "@core/services/location.service";
import { LogService } from "@core/services/log.service";
import { MapService } from "@core/services/map/map.service";
import { OperationService } from "@core/services/operation.service";
import { PlacemarkService } from "@core/services/interactions/placemark/placemark.service";
import { PreferencesService } from "@core/services/preferences.service";
import { RequestService } from "@core/services/request/request.service";
import { ScriptsService } from "@core/services/request/scripts.service";
import { UserService } from "@core/services/user.service";
// MAIN
import { ModalService } from "@main/services/modal/modal.service";
import { TablesService } from "@main/services/ttt/tables.service";

declare const extensibility: any;

@Component({
	selector: "seg-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {

	private roles;
	private operationConfig;
	private parsedOperations;
	private selectedBaseLayer;
	layerConfigs: any;

	CONFIG = require("../seg.config.json");

	constructor(
		// CORE
		private configService: ConfigService,
		private infoService: InfoService,
		private interfaceService: InterfaceService,
		private layerService: LayerService,
		private locationService: LocationService,
		private logService: LogService,
		private mapService: MapService,
		private operationService: OperationService,
		private preferencesService: PreferencesService,
		private requestService: RequestService,
		private scriptsService: ScriptsService,
		private userService: UserService,
		// MAIN
		private modalService: ModalService,
		private tablesService: TablesService,
	) {

	}

	ngOnInit() {
		const logs = this.logService.getLogs();

		this.configService.setConfig(this.CONFIG);

		this.tablesService.register({
			label: "Logs",
			itemAs: "logItem",
			data: logs,
			fields: {
				timestamp: { label: "Timestamp", template: "{{logItem.timestamp | timestamp}}", visible: true },
				tag: { label: "Tag", template: "{{logItem.tag}}", visible: true },
				message: { label: "Message", template: "{{logItem.message}}", visible: true }
			}
		});

		// let i = 0;

		// while (i < 100) {
		// 	this.logService.log("TAG_" + i, "MESSAGE_" + i);
		// 	i++;
		// }

		this.bootstrapInit()
			.pipe(concatMap(([_, global_, config_]) => {
				return this.setUpExtensibilityAndBaseLayers(global_, config_);
			}))
			.subscribe((layerConfigs) => {
				this.layerConfigs = layerConfigs;
			});
	}

	/**
	 * Calls segInfo & userInfo services, sets them up, then call extensibility and global preferences that will be required on map component
	 */
	bootstrapInit(): Observable<any> {
		// SEG INFO
		const segInfo$ = this.infoService.fetch()
				.pipe(first(), catchError(e => of("ERROR_SEG_INFO"))),
			// USER INFO
			userInfo$ = this.requestService.get("v1/userdetails/current_user")
				.pipe(first(), catchError(e => of("ERROR_CURRENT_USER")));

		//
		return forkJoin(segInfo$, userInfo$).pipe(
			first(),
			concatMap(([segInfo_, userInfo_]: Array<any>): Observable<any> => {
				this.setUserData(segInfo_, userInfo_);
				// creates extensibility script loader and loads it
				const extensibility$ = this.scriptsService.load({name: "ext", src: this.CONFIG.extensibility_path + "extensibility.min.js", loaded: false}),
					globalPreferences$ = this.preferencesService.loadGlobalPreferences(),
					extensibilityConfig$ = this.requestService.get(this.CONFIG.extensibility_path + "config.json");

				return forkJoin(extensibility$, globalPreferences$, extensibilityConfig$);
			})
		);
	}

	/**
	 * Set up segInfo and userInfo in their services
	 * @param {[type]} segInfo_  SEG version and other info
	 * @param {[type]} userInfo_ User data, like name, email, roles and permissions
	 */
	setUserData(segInfo_, userInfo_) {
		// Set SEG INFO data
		this.infoService.setInfo(segInfo_);

		// parse Operations info based on user data
		const {roles, parsedOperations} = this.operationService.parseOperations(userInfo_.result);
		this.roles = roles;
		this.parsedOperations = parsedOperations;

		// Saves current user with parsed operations
		this.userService.setCurrentUser(Object.assign(userInfo_.result, {operations: this.parsedOperations}));
	}

	/**
	 * Sets up global preferences, calls exensibility function to apply roles and permissions into modules, returning instance of operation wwith their modules
	 * @param {undefined} ext_    This subscription always returns undefined
	 * @param {Object} global_ Global JSON object returned by SEG server
	 * @param {Object} config_ Extensibility"s config file that will set base layers and other primary data
	 * @returns {Observable<any>} returns the layerConfigs that will be created further in the pipeline
	 */
	setUpExtensibilityAndBaseLayers(global_, config_): Observable<any> {
		Object.assign(this.CONFIG, config_);
		this.preferencesService.setGlobalPreferences(global_);

		// setting up extensibility and getting modules and operations from it
		const {modules, operations} = extensibility(this.roles, this.parsedOperations),
			loadOperation = this.operationService.loadOperation(this.operationService.getValidOperation(operations, global_), modules),
			modal = this.modalService.openModal("Loading modules", "Loading modules...");

		return loadOperation
			.pipe(concatMap((operation: any) => {
				this.operationConfig = operation;

				const baseLayers = [
					...this.CONFIG.base_layers,
					...((operation.base_layers && operation.base_layers.include) || [])
				].filter(layer => !~((operation.base_layers && operation.base_layers.exclude) || []).indexOf(layer));

				return zip(
					...baseLayers.map(id => this.requestService.get(this.CONFIG.extensibility_path + this.CONFIG.base_layers_path + id + ".json")
						.pipe(catchError(e => of("ERROR_SETUP_EXTENSIBILITY_AND_BASE_LAYERS")))
					)
				).pipe(
					take(1),
					concatMap((layerConfigs): Observable<any> => {
						return zip(...layerConfigs.map(layerConfig => this.layerService.enhance(layerConfig)));
					}),
					catchError(e => of("ERROR_SETUP_EXTENSIBILITY_AND_BASE_LAYERS"))
				);
			}));
	}
}
