import { ElementService } from './element.service';
import { Injectable, ElementRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FloatService {

  constructor(
    private elementService: ElementService
  ) { 
    requestAnimationFrame(this.reposition);
  }

  floatingElements = [];

	register = element => {

		//prevent double push since directives other than float might register their elements directly
		if(this.floatingElements.indexOf(element) === -1)
			this.floatingElements.push(element);
	};

	unregister = element => {
		this.floatingElements.splice(this.floatingElements.indexOf(element), 1);
	};

	reposition = () => {
		this.floatingElements.forEach(element => {
			const boundingBox = element.getBoundingClientRect();

			if(boundingBox.top < 10 && boundingBox.height < window.innerHeight - 20)
				this.elementService.ui(element).transform.translateY(10 - boundingBox.top);
			else if(boundingBox.bottom > window.innerHeight - 10)
				this.elementService.ui(element).transform.translateY(- boundingBox.bottom + window.innerHeight - 10);

			if(boundingBox.left < 10 && boundingBox.width < window.innerWidth - 20)
				this.elementService.ui(element).transform.translateX(10 - boundingBox.left);
			else if(boundingBox.right > window.innerWidth - 10)
				this.elementService.ui(element).transform.translateX(- boundingBox.right + window.innerWidth - 10 );
		});

		requestAnimationFrame(this.reposition);
	};

}