import { Injectable } from "@angular/core";
import * as $ui from "libs/seg-ui";

@Injectable({
	providedIn: "root"
})
export class ElementService {

	undockedWindows = [];
	focusedElement;
	transform;
	positionSetters;
	Events;
	isIE = navigator.userAgent.match(/Trident/);
	IDENTITY_MATRIX = [1, 0, 0, 1, 0, 0];
	TRANSLATE_2D = {X: 4, Y: 5, SCALE_X: 0, SCALE_Y: 3};
	TRANSLATE_3D = {X: 12, Y: 13, Z: 14};

	constructor() {
		this.transform = {
			translate: (matrix, x, y, z?) => {
				if (matrix.length === 2 * 3) {
					if (x) matrix[this.TRANSLATE_2D.X] += x;
					if (y) matrix[this.TRANSLATE_2D.Y] += y;
				} else {
					if (x) matrix[this.TRANSLATE_3D.X] += x;
					if (y) matrix[this.TRANSLATE_3D.Y] += y;
					if (z) matrix[this.TRANSLATE_3D.Z] += z;
				}

				return matrix;
			},
			position: (matrix, x, y, z?) => {
				if (matrix.length === 2 * 3) {
					if (typeof x !== "undefined") matrix[this.TRANSLATE_2D.X] = x;
					if (typeof y !== "undefined") matrix[this.TRANSLATE_2D.Y] = y;
				} else {
					if (typeof x !== "undefined") matrix[this.TRANSLATE_3D.X] = x;
					if (typeof y !== "undefined") matrix[this.TRANSLATE_3D.Y] = y;
					if (typeof z !== "undefined") matrix[this.TRANSLATE_3D.Z] = z;
				}

				return matrix;
			},
			setScale: (matrix, factor) => {
				if (matrix.length === 2 * 3) {
					if (typeof factor !== "undefined") {
						matrix[this.TRANSLATE_2D.SCALE_X] = factor;
						matrix[this.TRANSLATE_2D.SCALE_Y] = factor;
					}
				}

				return matrix;
			}
		};

		this.positionSetters = {
			setLeft: (element, value) => {
				const margin = 10;
				const windowSize = window["innerWidth"];
				const computedSize = parseFloat(window.getComputedStyle(element)["width"]) || element.getBoundingClientRect()["width"];

				$ui.RAF(() => {
					element.style["width"] = Math.min(windowSize - margin - computedSize, Math.max(margin, value)) + "px";
				});
			},
			setTop: (element, value) => {
				const margin = 10;
				const windowSize = window["innerHeight"];
				const computedSize = parseFloat(window.getComputedStyle(element)["height"]) || element.getBoundingClientRect()["height"];

				$ui.RAF(() => {
					element.style["height"] = Math.min(windowSize - margin - computedSize, Math.max(margin, value)) + "px";
				});
			}
		};

		this.Events = {
			resize: {
				setup: (element, callback) => {

					if (!element.querySelector("object[ui-scaffold=\"resize\"]")) {
						if (window.getComputedStyle(element).position === "static") element.style.position = "relative";

						const scaffold = document.createElement("object");

						scaffold.setAttribute("ui-scaffold", "resize");
						scaffold.setAttribute("style", "display: block; position: absolute; top: 0; left: 0; height: 100%; width: 100%; overflow: hidden; pointer-events: none; z-index: -1;");
						scaffold.onload = () => {
							element.contentDocument.defaultView.addEventListener("resize", () => {
								element.dispatchEvent(new Event("resize"));
							});
						};
						scaffold.type = "text/html";
						if (this.isIE) element.appendChild(scaffold);
						scaffold.data = "about:blank";
						if (!this.isIE) element.appendChild(scaffold);
					}

					return () => { $ui.RAF(callback); };
				}
			}
		};
	}

	ui(element) {
		const self = this;

		return {
			// on
			on: function(event, callback) {
				return addEventListener(element, event, callback);
			},
			// children
			get children() {
				const children = [];

				if (!element.children)
					return [];

				for (let i = 0; i < element.children.length; i++)
					children.push(element.children[i]);

				return children;
			},
			set children (newChildren) {
				self.setChildren(element, newChildren);
			},
			// position
			position: {
				set left(value) {
					self.positionSetters.setLeft(element, value);
				},
				set top(value) {
					self.positionSetters.setTop(element, value);
				}
			},
			isAncestorOf: function(allegedChild) {
				return self.isAncestorOf(element, allegedChild);
			},
			// transform
			transform: {
				get matrix() {
					const transformMatrix = window.getComputedStyle(element).transform;

					if (transformMatrix === "none")
						return self.IDENTITY_MATRIX;

					return transformMatrix.replace(/matrix(?:3d)?\(|\)/g, "").split(", ").map((value) => {
						return parseFloat(value);
					});
				},
				set matrix(matrix) {
					element.style.transform = "matrix" + (matrix.length ===  2 * 3 ? "" : "3d") + "(" + matrix.map((value) => {
						return Math.round(value);
					}).join(", ") + ")";
				},
				translate: (x, y) => {
					self.ui(element).transform.matrix = self.transform.translate(self.ui(element).transform.matrix, x, y);
				},
				translateX: (x) => {
					self.ui(element).transform.matrix = self.transform.translate(self.ui(element).transform.matrix, x, 0);
				},
				translateY: (y) => {
					self.ui(element).transform.matrix = self.transform.translate(self.ui(element).transform.matrix, 0, y);
				},
				position: (x?, y?) => {
					if (typeof x !== "undefined" || typeof y !== "undefined")
						return self.ui(element).transform.matrix = self.transform.position(self.ui(element).transform.matrix, x, y);

					const matrix = self.ui(element).transform.matrix;

					return {
						x: matrix[self.TRANSLATE_2D.X],
						y: matrix[self.TRANSLATE_2D.Y]
					};
				},
				setScale: function(factor) {
					self.ui(element).transform.matrix = self.transform.setScale(self.ui(element).transform.matrix, factor);
				}
			},
			// undock
			undock: function() {
				self.undock(element);
			}
		};
	}


	focus(element) {
		if (this.focusedElement && element !== this.focusedElement)
			this.focusedElement.classList.remove("ui-focused");

		if (!this.focusedElement || element !== this.focusedElement) {
			element.classList.add("ui-focused");
			this.focusedElement = element;
		}
	}

	addEventListener(element, eventType, callback) {
		if (eventType === "resize")
			callback = this.Events.resize.setup(element, callback);

		element.on(eventType, () => {
			callback();
		});

		return this.ui(element);
	}

	setChildren(element, newChildren) {
		element.children().remove();

		element.append(newChildren);
	}

	isAncestorOf(element, allegedChild) {
		let currentAncestor = allegedChild.parentElement;

		while (currentAncestor) {
			if (currentAncestor === element) return true;
			currentAncestor = currentAncestor.parentElement;
		}
	}

	undock(element) {
		const bounds = element.getBoundingClientRect(),
			popup = window.open("", "", "menubar=no, width=" + bounds.width + ", height=" + bounds.height + ", toolbar=no, titlebar=no, status=no, location=no"),
			stylesheets: NodeListOf<Element> = document.querySelectorAll("link[rel=\"stylesheet\"]");

		for (let i = 0; i < stylesheets.length; i++) {
			const sheet = document.createElement("link");
			sheet.setAttribute("href", stylesheets[i].getAttribute("href"));
			sheet.setAttribute("rel", "stylesheet");
			popup.document.head.append(sheet);
		}

		popup.document.body.append(element);

		this.undockedWindows.push(popup);
	}
}
