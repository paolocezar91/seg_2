import { TestBed } from '@angular/core/testing';

import { FloatService } from './float.service';

describe('FloatService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FloatService = TestBed.get(FloatService);
    expect(service).toBeTruthy();
  });
});
