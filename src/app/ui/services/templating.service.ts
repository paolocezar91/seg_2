import { Injectable, ViewContainerRef, Compiler, ElementRef, Component, Injector, Inject, ModuleWithComponentFactories, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, NgModule, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { CommonModule } from "@angular/common";
import { template } from "lodash";
// import { UiModule } from "@ui/ui.module";
import { MainModule } from "@main/main.module";
import { PipesModule } from "@ui/pipes/pipes.module";

@Injectable({
	providedIn: "root"
})
export class TemplatingService {

	cache = {};

	constructor(
		private compiler: Compiler
	) { }

	set(key, data) {
		this.cache[key] = data;
	}

	get(key) {
		return this.cache[key];
	}

	renderWithLodash(templ, scope) {
		let compiledScope/* = this.get(templ + JSON.stringify(scope))*/;
		if (!compiledScope) {
			const compiled = template(templ, {interpolate: /{{([\s\S]+?)}}/g});
			compiledScope = compiled(scope);

			// this.set(templ + JSON.stringify(scope), compiledScope);
		}
		return compiledScope;
	}

	renderWithVCR(templ, scope, viewContainerRef, loadUiModule?: boolean) {
		const cachedComponent = this.get(templ);
		if (cachedComponent) {
			const injector = Injector.create({providers: [{provide: "scope", useValue: scope}], parent: viewContainerRef.parentInjector});

			return new Promise(resolve => resolve({factory: cachedComponent.factory, injector}));
		}

		@Component({selector: "dynamic-template", template: templ.toString(), changeDetection: ChangeDetectionStrategy.OnPush})
		class DynamicComponent {
			constructor(@Inject("scope") scope_, private cd: ChangeDetectorRef) {
				Object.assign(this, scope_);
				this.cd.markForCheck();
			}
		}

		const imports = [CommonModule, PipesModule];
		if (loadUiModule)
			imports.push(window.UiModule);

		@NgModule({ imports, declarations: [DynamicComponent], schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ] })
		class DynamicHtmlModule { }

		return this.compiler.compileModuleAndAllComponentsAsync(DynamicHtmlModule)
			.then((moduleWithComponentFactory: ModuleWithComponentFactories<any>) => {
				const factory = moduleWithComponentFactory.componentFactories.find(x => x.componentType === DynamicComponent),
					injector = Injector.create({providers: [{provide: "scope", useValue: scope}], parent: viewContainerRef.parentInjector});

				this.set(templ, {factory, injector});
				return {factory, injector};
			});
	}
}
