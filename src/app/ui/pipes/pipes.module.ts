import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DateFormatPipe } from "@ui/pipes/date-format.pipe";
import { TimestampPipe } from "@ui/pipes/timestamp.pipe";
import { CoordinatePipe } from "@ui/pipes/coordinate.pipe";
import { CountryPipe } from './country.pipe';

@NgModule({
	declarations: [
		DateFormatPipe,
		TimestampPipe,
		CoordinatePipe,
		CountryPipe
	],
	imports: [
		CommonModule,
	],
	exports: [
		// PIPES
		DateFormatPipe,
		TimestampPipe,
		CoordinatePipe,
		CountryPipe
	]
})
export class PipesModule { }
