import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "coordinate"
})
export class CoordinatePipe implements PipeTransform {

	transform(value: any, format: string, latOrLon: string): any {
		let template;

		format = format || "DMS";

		switch (format) {
			case "DMS": template = "D{0}°M{0}'s{0}\"H"; break;
			case "DM": template = "D{0}°M{2}'H"; break;
			case "Decimal": template = "D{5}°"; break;
		}

		let degrees = value;
		const minutes = Math.abs(60 * (degrees - Math.trunc(degrees)));
		const seconds = 60 * (minutes - Math.trunc(minutes));
		let hemisphere;

		if (template.match("H")) {
			if (latOrLon === "lat")
				hemisphere = degrees > 0 ? "N" : "S";
			else if (latOrLon === "lon")
				hemisphere = degrees > 0 ? " E " : "W";

			degrees = Math.abs(degrees);
		}

		return template ? template
			.replace("H", hemisphere)
			.replace(/D{(\d+)}/, (match, capture) => {
				// latitude should always have 2 digits
				// longitude should always have 3 digits
				return this.padLeft(this.truncN(degrees, parseInt(capture, 10)), latOrLon === "lat" ? 2 : 3);
			}).replace(/M{(\d+)}/, (match, capture) => {
				return this.padLeft(this.truncN(minutes, parseInt(capture, 10)), 2);
			}).replace(/s{(\d+)}/, (match, capture) => {
				return this.padLeft(this.truncN(seconds, parseInt(capture, 10)), 2);
			}) : value;
  }

	truncN(value, decimalPlaces) {
		return Math.trunc(value * Math.pow(10, decimalPlaces)) / Math.pow(10, decimalPlaces);
	}

	padLeft(value, padding) {
		const abs = Math.abs(value),
			sign = value ? value / abs : 0,
			paddingString = Array(padding).fill(0).join("");

		return (sign >= 0 ? "" : "-") + paddingString.substr(0, paddingString.length - Math.trunc(abs).toString().length) + abs;
	}

}
