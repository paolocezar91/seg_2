import { Pipe, PipeTransform } from "@angular/core";
import * as moment from "moment";

@Pipe({
	name: "dateformat"
})
export class DateFormatPipe implements PipeTransform {

	transform(value: any, format?: any): any {
		return moment(value).format(format);
	}

}
