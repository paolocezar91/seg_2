import { Pipe, PipeTransform, OnDestroy } from "@angular/core";
import { PreferencesService } from "@core/services/preferences.service";
import * as moment from "moment";

@Pipe({
	name: "timestamp"
})
export class TimestampPipe implements PipeTransform, OnDestroy {

	timestamp;
	format;
	timezone;
	timeFormatListener;
	timezoneListener;

	constructor(private preferencesService: PreferencesService) {
		this.format = this.preferencesService.getWorkspacePreference("timeFormat"),
		this.timezone = this.preferencesService.getWorkspacePreference("timezone");

		this.timeFormatListener = preferencesService.onWorkspacePreferenceChange("timeFormat", this.updateFormat),
		this.timezoneListener = preferencesService.onWorkspacePreferenceChange("timezone", this.updateTimezone);
	}

	transform(timestamp: any, format: string = "UTC", timezoneOffset: number = 0): any {
		this.timestamp = timestamp;
		const baseFormat = "YYYY-MM-DD HH:mm:ss";

		if (format === "UTC") {
			const offsetSuffix = timezoneOffset === 0 ? "Z" : ((timezoneOffset > 0 ? "+" : "-") + (Math.abs(timezoneOffset) < 10 ? "0" : "") + Math.abs(timezoneOffset) + ":00");
			return moment.utc(timestamp).add(timezoneOffset, "h").format(baseFormat) + offsetSuffix;
		} else if (format === "Local")
			return moment(timestamp).format(baseFormat);
	}

	ngOnDestroy() {
		this.timeFormatListener();
		this.timezoneListener();
	}

	updateFormat(format) {
		this.format = format;

		this.updateTimestamp();
	}

	updateTimezone(timezone) {
		this.timezone = timezone;

		this.updateTimestamp();
	}

	// update timestamp value
	updateTimestamp() {
		this.transform(this.timestamp, this.format, this.timezone);
	}
}
