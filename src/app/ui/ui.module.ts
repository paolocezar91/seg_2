import { LayerService } from "@core/services/layer/layer.service";
import { CoreModule } from "./../core/core.module";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { PipesModule } from "@ui/pipes/pipes.module";

/* components */
import { CalendarComponent } from "@ui/components/calendar/calendar.component";
import { CheckboxComponent } from "@ui/components/checkbox/checkbox.component";
import { ClockComponent } from "@ui/components/clock/clock.component";
import { ComboComponent } from "@ui/components/combo/combo.component";
import { CommonModule } from "@angular/common";
import { DatetimeComponent } from "@ui/components/datetime/datetime.component";
import { FlagComponent } from "@ui/components/flag/flag.component";
import { ForecastDatetimeComponent } from "@ui/components/forecast-datetime/forecast-datetime.component";
import { GridComponent } from "@ui/components/grid/grid.component";
import { IconComponent } from "@ui/components/icon/icon.component";
import { MultiDropdownComponent } from "@ui/components/multi-dropdown/multi-dropdown.component";
import { PagerComponent, PagerControlsComponent } from "@ui/components/pager/pager.component";
import { ProgressBarComponent } from "@ui/components/progress-bar/progress-bar.component";
import { RadioComponent } from "@ui/components/radio/radio.component";
import { SectionComponent } from "@ui/components/section/section.component";
import { SlideshowComponent } from "@ui/components/slideshow/slideshow.component";
import { TabComponent } from "@ui/components/tab-group/tab.component";
import { TabGroupComponent } from "@ui/components/tab-group/tab-group.component";
import { TimerangeComponent } from "@ui/components/timerange/timerange.component";
import { DynamicTabsDirective } from "@ui/components/tab-group/dynamic-tabs.directive";
import { ToggleComponent } from "@ui/components/toggle/toggle.component";

/* components created but not used yet */
import { ImageToolsComponent } from "@ui/components/image-tools/image-tools.component";
import { ColorpickerComponent } from "@ui/components/colorpicker/colorpicker.component";

/* directives */
import { IncludeDirective } from "@ui/directives/include.directive";
import { NgInitDirective } from "@ui/directives/init.directive";
import { CoordinateDirective } from "@ui/directives/coordinate.directive";
import { PositionTimestampDirective } from "@ui/directives/position-timestamp.directive";
import { ScrollableDirective } from "@ui/directives/scrollable/scrollable.directive";
import { DebounceDirective } from "@ui/directives/on-debounce.directive";
import { FocusableDirective } from './directives/focusable.directive';
import { DraggableDirective } from './directives/draggable.directive';

import { TemplatingService } from "@ui/services/templating.service";
import { ElementService } from "@ui/services/element.service";
import { FloatService } from "@ui/services/float.service";

@NgModule({
	declarations: [
		// COMPONENTS
		CalendarComponent,
		CheckboxComponent,
		ClockComponent,
		ComboComponent,
		DatetimeComponent,
		FlagComponent,
		ForecastDatetimeComponent,
		GridComponent,
		IconComponent,
		MultiDropdownComponent,
		PagerComponent,
		PagerControlsComponent,
		ProgressBarComponent,
		RadioComponent,
		SectionComponent,
		SlideshowComponent,
		TabComponent,
		TabGroupComponent,
		TimerangeComponent,
		ToggleComponent,
		ImageToolsComponent,
		ColorpickerComponent,
		// DIRECTIVES
		NgInitDirective,
		IncludeDirective,
		DynamicTabsDirective,
		CoordinateDirective,
		PositionTimestampDirective,
		ScrollableDirective,
		DebounceDirective,
		FocusableDirective,
		DraggableDirective,
	],
	exports: [
		// COMPONENTS
		CalendarComponent,
		CheckboxComponent,
		ClockComponent,
		ComboComponent,
		DatetimeComponent,
		FlagComponent,
		ForecastDatetimeComponent,
		GridComponent,
		IconComponent,
		MultiDropdownComponent,
		PagerComponent,
		PagerControlsComponent,
		ProgressBarComponent,
		RadioComponent,
		SectionComponent,
		SlideshowComponent,
		TabGroupComponent,
		TabComponent,
		TimerangeComponent,
		ToggleComponent,
		// DIRECTIVES
		NgInitDirective,
		IncludeDirective,
		DynamicTabsDirective,
		CoordinateDirective,
		PositionTimestampDirective,
		ScrollableDirective,
		DebounceDirective,
		FocusableDirective,
		DraggableDirective,
	],
	imports: [
		CommonModule,
		FormsModule,
		PipesModule
	],
	entryComponents: [TabComponent],
	providers: [ElementService, TemplatingService, FloatService],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ]
})
export class UiModule { }
