import { ElementService } from './../services/element.service';
import { Directive, OnInit, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[focusable]'
})
export class FocusableDirective implements OnInit {

  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    private elementService: ElementService
  ) { }

  ngOnInit(): void {
    
    this.focus();

    this.renderer.listen(this.el.nativeElement, "mousedown", e => {
			this.focus();
		});
  }

  focus = () => {
		this.elementService.focus(this.el.nativeElement);
	};

}