import { Directive, OnDestroy, OnInit, Input, ElementRef, OnChanges } from "@angular/core";
import { PreferencesService } from "@core/services/preferences.service";
import { TimestampPipe } from "@ui/pipes/timestamp.pipe";
import { interval } from "rxjs";
import * as moment from "moment";


class Clock {
	tick_ = false;
	callback;
	gears;

	constructor(callback) {
		this.tick_ = false;
		this.callback = callback;
	}

	start() {
		this.gears = interval(1000).subscribe(() => {
			this.tick();
		});

		this.tick();
	}

	stop() {
		if (this.gears)
			this.gears.unsubscribe();
	}

	tick() {
		this.callback();
	}
}

@Directive({
  selector: "[position-timestamp]",
  providers: [TimestampPipe]
})
export class PositionTimestampDirective implements OnInit, OnChanges, OnDestroy {

	@Input("position-timestamp") positionTimestamp;
	positionTimestampFormatListener;
	filteredTimestamp;
	format;
	clock;

	constructor(
		private preferencesService: PreferencesService,
		private el: ElementRef,
		private timestampPipe: TimestampPipe
	) {
		this.clock = new Clock(this.updateTimestamp);
	}

	ngOnInit() {
		// set preference change listeners
		this.positionTimestampFormatListener = this.preferencesService.onWorkspacePreferenceChange("positionTimestampFormat", (format) => this.updateFormat(format));

		// set initial position timestamp format
		this.updateFormat(this.preferencesService.getWorkspacePreference("positionTimestampFormat"));
		this.updateElement();
	}

	ngOnChanges() {
		if (this.format)
			this.updateElement();
	}

	ngOnDestroy() {
		// remove listeners on element remove
		if(this.positionTimestampFormatListener)
			this.positionTimestampFormatListener();

		if (this.clock)
			this.clock.stop();
	}

	updateElement() {
		if (this.format === "Absolute timestamp") {
			this.el.nativeElement.insertAdjacentHTML("beforeend", this.timestampPipe.transform(this.positionTimestamp));
		} else if (this.format === "Time relative") {
			this.el.nativeElement.insertAdjacentHTML("beforeend", this.positionTimestamp);
		}
	}

	updateTimestamp() {
		const now = moment(),
			timestamp = moment(this.positionTimestamp),
			past = timestamp.isBefore(now);

		const keys: moment.unitOfTime.DurationConstructor[] = ["years", "months", "weeks", "days", "hours", "minutes", "seconds"],
			abbrev = ["y", "M", "w", "d", "h", "m", "s"],
			diff = keys.reduce((diff_, key: moment.unitOfTime.DurationConstructor, i) => {
				if (i)
					timestamp.add(diff_[keys[i - 1]], keys[i - 1]);

				diff_[key] = now.diff(timestamp, /*key*/);

				return diff_;
			}, {}),
			timeString = Object.entries(diff).filter(entry => entry[1]).map((entry: any[]) => {
				const [key, value] = entry;

				return Math.abs(value) + abbrev[keys.indexOf(key)];
			}).join(" ") || "0s";

		this.filteredTimestamp = past ? timeString + " ago" : "in " + timeString;
	}

	updateFormat(format) {
		this.format = format || "Absolute timestamp";

		if (this.clock)
			this.clock.stop();

		if (format === "Time relative") {
			this.clock.start();
		}
	}

}
