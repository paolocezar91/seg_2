import { Directive, OnInit, ElementRef, Input, OnDestroy } from "@angular/core";
import { CoordinatePipe } from "@ui/pipes/coordinate.pipe";
import { PreferencesService } from "@core/services/preferences.service";

@Directive({
  selector: "[coordinate]",
  providers: [CoordinatePipe]
})
export class CoordinateDirective implements OnInit, OnDestroy {

	coordinateFormatListener;
	format: string;
	value: number;

	constructor(
		private preferencesService: PreferencesService,
		private coordinatePipe: CoordinatePipe,
		private el: ElementRef
	) { }

	@Input() set coordinate(val) {
		this.value = val;
		this.updateCoordinate();
	}

	ngOnInit() {
		// set preference listener for coordinate format
		this.coordinateFormatListener = this.preferencesService.onWorkspacePreferenceChange("coordinateFormat", (format) => this.updateFormat(format));

		// set initial coordinate value
		this.format = this.preferencesService.getWorkspacePreference("coordinateFormat");
	}

	ngOnDestroy() {
		if (this.coordinateFormatListener)
			this.coordinateFormatListener();
	}

	updateFormat(format) {
		this.format = format;

		this.updateCoordinate();
	}

	// update coordinate value
	updateCoordinate() {
		this.el.nativeElement.insertAdjacentHTML("beforeend", this.value[0] && this.value[1] ? this.coordinatePipe.transform(this.value[0], this.format, this.value[1]).replace("°", "&deg;") : "N/A");
	}
}
