import { FloatService } from '@ui/services/float.service';
import { ElementService } from '@ui/services/element.service';
import { Directive, ElementRef, OnInit, OnDestroy } from '@angular/core';
import * as Hammer from "hammerjs";

@Directive({
  selector: '[draggable]'
})
export class DraggableDirective implements OnInit, OnDestroy {

  constructor(
    private el: ElementRef,
    private elementService: ElementService,
    private floatService: FloatService,
  ) { }

  moving = false;
  lastCoordinates;
  mc;

  ngOnInit(){
    this.floatService.register(this.el.nativeElement);
    this.initMouseListener();
  }

  ngOnDestroy(){
    if(this.mc)
      this.mc.destroy()
  }

  mouseDownListener(e) {
    this.moving = true,
    this.lastCoordinates = {
      x: e.pointers[0].clientX, //e.x
      y: e.pointers[0].clientY //x.y
    };
  }

  initMouseListener = () => {
    this.mc = new Hammer.Manager(this.el.nativeElement);

    this.mc.add(new Hammer.Pan({
      direction: Hammer.DIRECTION_ALL
    }));

    this.mc.on("panstart", e => {
      let target = e.target;
      const titlebar = this.el.nativeElement.querySelector("window-title");

      while(target !== this.el.nativeElement) {
        if(target === titlebar)
          return this.mouseDownListener(e);

        target = target.parentElement;
      }
    });

    this.mc.on("panmove", e => {
      if (this.moving) {
        const deltaX = e.pointers[0].clientX - this.lastCoordinates.x,
          deltaY = e.pointers[0].clientY - this.lastCoordinates.y;

          this.elementService.ui(this.el.nativeElement).transform.translate(deltaX, deltaY);

        this.lastCoordinates = {
          x: e.pointers[0].clientX,
          y: e.pointers[0].clientY
        };
      }
    });

    this.mc.on("panend pancancel", () => this.moving = false);
  };

}