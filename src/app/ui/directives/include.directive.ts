import { ViewContainerRef, Compiler, ElementRef, ComponentRef, OnChanges, OnDestroy, Directive, Input, ChangeDetectorRef } from "@angular/core";
import { TemplatingService } from "@ui/services/templating.service";

const ANGULAR_REGEX = /((\<calendar)|(\<checkbox)|(\<clock)|(\<combo)|(\<datetime)|(\<forecast-datetime)|(\<grid)|(\<icon)|(\<multi-dropdown)|(\<pager)|(\<pager-controls)|(\<progress-bar)|(\<radio)|(\<section)|(\<slideshow)|(\<tab)|(\<tab-group)|(\<timerange)|(\<toggle))/,
	PIPE_REGEX = /\|\s*((number)|(timestamp)|(keyvalue)|(coordinate)|(country))/,
	DIRECTIVE_REGEX = /((\[coordinate\])|(\[init\])|(\[position-timestamp\])|(\[include\]))/,
	CURLY_BRACES_REGEX = /\{{([^}]+)\}}/;


/**
 * This directive receives a template and a scope, interpolates and renders the content
 * This used either with lodash template function, if the template is simple or dynamicModule and dynamicComponent
 * We should create a cache for templating to increase speed of re-renders
 * Usage : <include [template]="template" [scope]="scope"></include>
 */
@Directive({
	selector: "include,[include]"
})
export class IncludeDirective implements OnChanges, OnDestroy {
	@Input() template: string;
	@Input() scope: string;
	@Input() set triggerChanges(val) {
		if (this._triggerChanges !== val) {
			this._triggerChanges = val;
		}
	}
	_triggerChanges;
	cmpRef: ComponentRef<any>;

	constructor(
		private vcRef: ViewContainerRef,
		private el: ElementRef,
		private templatingService: TemplatingService,
		private cdr: ChangeDetectorRef
	) { }

	ngOnChanges() {
		this.cleanInclude();

		const templ = this.template,
			scope = this.scope;

		if (!templ) return;

		const matchCurlyBraces = templ.match(CURLY_BRACES_REGEX),
			matchPipe = templ.match(PIPE_REGEX),
			matchComponent = templ.match(ANGULAR_REGEX),
			matchDirective = templ.match(DIRECTIVE_REGEX);

		// match any angular components or pipes that are being used, if none are, use lite-include, else use regular include
		if (matchPipe || matchComponent || matchDirective) {
			if (this.cmpRef)
				this.cmpRef.destroy();

			const loadModule = (!!matchComponent || !!matchDirective);

			this.templatingService.renderWithVCR(templ, scope, this.vcRef, loadModule).then(({factory, injector}) => {
				this.cmpRef = this.vcRef.createComponent(factory, 0, injector, []);
				this.cdr.markForCheck();
			});
		} else {
			// If template does not have curly braces, we should just print the value. Especially for simple labels.
			if (!matchCurlyBraces) {
				this.el.nativeElement.insertAdjacentHTML("beforeend", templ);
			} else {
				this.cleanInclude();
				this.el.nativeElement.insertAdjacentHTML("beforeend", this.templatingService.renderWithLodash(templ, scope));
			}
		}
	}



	ngOnDestroy() {
		if (this.cmpRef) {
			this.cmpRef.destroy();
		}
	}

	cleanInclude() {
		while (this.el.nativeElement.firstChild) {
			this.el.nativeElement.removeChild(this.el.nativeElement.firstChild);
		}
	}

}
