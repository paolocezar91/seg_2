import { ElementService } from "@ui/services/element.service";
import { LogService } from "@core/services/log.service";
import { HttpClient } from "@angular/common/http";
import { Directive, ElementRef, OnInit, OnDestroy, Input, Renderer2, HostListener, ViewChild } from "@angular/core";
import * as $ui from "libs/seg-ui";
import * as Hammer from "hammerjs";
import * as utils from "libs/seg-utils";

@Directive({
	selector: "[scrollable]"
})
export class ScrollableDirective implements OnInit, OnDestroy {

	horizontalScrollbar;
	verticalScrollbar;
	overflowX;
	overflowY;
	lastOverflowX;
	lastOverflowY;
	pos: {x: number, y: number} = {x: 0, y: 0};
	inertiaVelocity: {x: number, y: number} = {x: 0, y: 0};
	scrollCheck;
	interactionTimestamp;
	mc;
	touchListenInitH = {};
	touchListenEndH = {};
	touchListenInitV = {};
	touchListenEndV = {};

	VELOCITY_EPSILON = .1;
	TIME_CONSTANT = 325;

	_scrollTo;
	@Input("scroll-to") set scrollTo(value) {
		if (value !== this._scrollTo)
			setTimeout(() => this.el.nativeElement.scrollTop = value, 200);

		this._scrollTo = value;
	}
	@Input() resetOnResize;

	constructor(
		private el: ElementRef,
		private http: HttpClient,
		private logService: LogService,
		private renderer: Renderer2,
		private elementService: ElementService
	) { }

	ngOnInit() {

		const template = "<scroll-track><scroll-handler></scroll-handler></scroll-track>";

		this.horizontalScrollbar = utils.htmlToElement(template);
		this.verticalScrollbar = utils.htmlToElement(template);

		this.renderer.addClass(this.horizontalScrollbar, "horizontal");
		this.renderer.addClass(this.verticalScrollbar, "vertical");

		this.el.nativeElement.append(this.horizontalScrollbar);
		this.el.nativeElement.append(this.verticalScrollbar);

		this.renderer.listen(this.el.nativeElement, "touchstart", e => {
			const touch = e.touches.item(0);
			this.pos = {x: touch.clientX, y: touch.clientY};
			this.inertiaVelocity = {x: 0, y: 0};
			this.interactionTimestamp = Date.now();
		});

		const scrollbar = {h: null, v: null};
		["touchstart", "mousedown"].forEach(eventType => {
			this.renderer.listen(this.horizontalScrollbar, eventType, e => e.stopPropagation());
			this.renderer.listen(this.verticalScrollbar, eventType, e => e.stopPropagation());
		});

		["touchstart", "mousedown"].forEach(eventType => {
			this.renderer.listen(this.horizontalScrollbar.querySelector("scroll-handler"), eventType, e => {
				e.preventDefault();

				const pointer = (e.touches && e.touches.item(0)) || e;
				scrollbar.h = pointer.clientX;

				const dragH = event => {
					event.preventDefault();

					if (typeof scrollbar.h === "undefined")
						return;

					const _pointer = (e.touches && e.touches.item(0)) || event;
					this.el.nativeElement.scrollLeft += (_pointer.clientX - scrollbar.h) / this.overflowX;
					scrollbar.h = _pointer.clientX;
				};

				const endH = () => {
					delete scrollbar.h;
					["touchmove", "mousemove"].forEach(_eventType => this.touchListenInitH[_eventType]());
					["touchend", "mouseup"].forEach(_eventType => this.touchListenEndH[_eventType]());
					this.touchListenInitH = {};
					this.touchListenEndH = {};
				};

				["touchmove", "mousemove"]
					.forEach(_eventType => this.touchListenInitH[_eventType] = this.renderer.listen(window, _eventType, (e_) => dragH(e_)));
				["touchend", "mouseup"]
					.forEach(_eventType => this.touchListenEndH[_eventType] = this.renderer.listen(window, _eventType, () => endH()));

			});

			this.renderer.listen(this.verticalScrollbar.querySelector("scroll-handler"), eventType, e => {
				e.preventDefault();

				const pointer = (e.touches && e.touches.item(0)) || e;
				scrollbar.v = pointer.clientY;

				const dragV = event => {
					if (typeof scrollbar.v === "undefined")
						return;
					const _pointer = (event.touches && event.touches.item(0)) || event;
					this.el.nativeElement.scrollTop += (_pointer.clientY - scrollbar.v) / this.overflowY;
					scrollbar.v = _pointer.clientY;
				};
				const endV = () => {
					delete scrollbar.v;
					["touchmove", "mousemove"].forEach(_eventType => this.touchListenInitV[_eventType]());
					["touchend", "mouseup"].forEach(_eventType => this.touchListenEndV[_eventType]());
					this.touchListenInitV = {};
					this.touchListenEndV = {};
				};

				["touchmove", "mousemove"]
					.forEach(_eventType => this.touchListenInitV[_eventType] = this.renderer.listen(window, _eventType, (e_) => dragV(e_)));

				["touchend", "mouseup"]
					.forEach(_eventType => this.touchListenEndV[_eventType] = this.renderer.listen(window, _eventType, () => endV()));
			});
		});

		this.mc = new Hammer.Manager(this.el.nativeElement);
		this.mc.add(new Hammer.Pan({direction: Hammer.DIRECTION_ALL}));

		this.mc.on("panmove", e => {
			if (typeof scrollbar.h !== "undefined" || typeof scrollbar.v !== "undefined")
				return;

			const deltaT = Date.now() - this.interactionTimestamp;
			if (!deltaT)
				return;

			const touch = e.pointers[0];
			if (this.pos.x) {
				const clientX = touch.clientX,
				deltaX = this.pos.x - clientX;
				this.el.nativeElement.scrollLeft += deltaX;
				this.inertiaVelocity.x = .8 * deltaX / deltaT + .2 * this.inertiaVelocity.x,
				this.pos.x = clientX;
			}

			if (this.pos.y) {
				const clientY = touch.clientY,
				deltaY = this.pos.y - clientY;
				this.el.nativeElement.scrollTop += deltaY;
				this.inertiaVelocity.y = .8 * deltaY / deltaT + .2 * this.inertiaVelocity.y,
				this.pos.y = clientY;
			}
		});

		this.mc.on("panend pancancel", () => {
			this.pos = {x: 0, y: 0};

			if (Math.abs(this.inertiaVelocity.x) > this.VELOCITY_EPSILON || Math.abs(this.inertiaVelocity.y) > this.VELOCITY_EPSILON)
				requestAnimationFrame(this.inertialScroll);
		});

		this.renderer.listen( this.el.nativeElement, "mousewheel", e => {
			if (this.overflowY < 1)
				this.el.nativeElement.scrollTop += e.deltaY;
			else if (this.overflowX < 1)
				this.el.nativeElement.scrollLeft += e.deltaY;
		});

		this.scrollCheck = requestAnimationFrame(() => this.updateScrollbars());
	}

	ngOnDestroy() {
		if (this.mc)
			this.mc.destroy();

		cancelAnimationFrame(this.scrollCheck);
	}

	inertialScroll = () => {
		const deltaT = Date.now() - this.interactionTimestamp,
		deltaX = this.inertiaVelocity.x * Math.exp(-deltaT / this.TIME_CONSTANT),
		deltaY = this.inertiaVelocity.y * Math.exp(-deltaT / this.TIME_CONSTANT);

		if (Math.abs(deltaX) < .1 && Math.abs(deltaY) < .1)
			return;

		this.el.nativeElement.scrollLeft += deltaX;
		this.el.nativeElement.scrollTop += deltaY;

		requestAnimationFrame(() => this.inertialScroll());
	}

	updateScrollbars = () => {
		this.overflowX = this.el.nativeElement.offsetWidth / this.el.nativeElement.scrollWidth;
		this.overflowY = this.el.nativeElement.offsetHeight / this.el.nativeElement.scrollHeight;

		const isIE = !!navigator.userAgent.match("Trident/") || !!navigator.userAgent.match("MSIE ");

		if (isIE) {
			this.el.nativeElement.setAttribute("scrollable", "overflow-y overflow-x");
		} else {
			this.el.nativeElement.setAttribute("scrollable", ((this.overflowY < 1 ? "overflow-y" : "") + " ") + (this.overflowX < 1 ? "overflow-x" : ""));
			let scrollPosition, scrollHandlerPosition;

			// When overflow value changes, the scroll resets to 0 position
			// This is used on filtering in TTT mainly
			if (this.overflowX < 1) {
				if (this.resetOnResize && this.lastOverflowX !== this.overflowX) {
					scrollPosition = 0;
					scrollHandlerPosition = 0;
					this.el.nativeElement.scrollLeft = 0;
				} else {
					scrollPosition = this.el.nativeElement.scrollLeft;
					scrollHandlerPosition = this.horizontalScrollbar.offsetWidth * this.el.nativeElement.scrollLeft / this.el.nativeElement.scrollWidth;
				}

				this.elementService.ui(this.horizontalScrollbar).transform.position(scrollPosition, this.el.nativeElement.scrollTop);

				const scrollHandler = this.horizontalScrollbar.querySelector("scroll-handler");
				scrollHandler.style.setProperty("width", (this.overflowX * 100) + "%");

				this.elementService.ui(scrollHandler).transform.position(scrollHandlerPosition);
			}

			if (this.overflowY < 1) {
				if (this.resetOnResize && this.lastOverflowY !== this.overflowY) {
					scrollPosition = 0;
					scrollHandlerPosition = 0;
					this.el.nativeElement.scrollTop = 0;
				} else {
					scrollPosition = this.el.nativeElement.scrollTop;
					scrollHandlerPosition = this.verticalScrollbar.offsetHeight * this.el.nativeElement.scrollTop / this.el.nativeElement.scrollHeight;
				}

				this.elementService.ui(this.verticalScrollbar).transform.position(this.el.nativeElement.scrollLeft, scrollPosition);
				const scrollHandler = this.verticalScrollbar.querySelector("scroll-handler");
				scrollHandler.style["height"] = ((this.overflowY * 100) + "%");
				this.elementService.ui(scrollHandler).transform.position(0, scrollHandlerPosition);
			}
		}

		this.lastOverflowX = this.overflowX;
		this.lastOverflowY = this.overflowY;

		this.scrollCheck = requestAnimationFrame(() => this.updateScrollbars());
	}

}
