import { Component, OnInit, ElementRef, EventEmitter, Output, Input, HostListener } from '@angular/core';


// Usage
// <radio class="seg" [(model)]="bool"></radio>

@Component({
	selector: 'radio',
	templateUrl: './radio.component.html',
	styleUrls: ['./radio.component.scss']
})
export class RadioComponent implements OnInit {

	@Input() model;
	@Output() modelChange = new EventEmitter<boolean>();

	constructor(private el: ElementRef) { }

	ngOnInit() {
		if(this.model)
			this.onClick(this.model);
	}

	@HostListener('click') onClick(value? : boolean){
		this.model = value || !this.model;

		if(this.model)			
			this.el.nativeElement.setAttribute('checked', true);
		else 
			this.el.nativeElement.removeAttribute('checked');
		
		this.modelChange.emit(this.model);
	}

}
