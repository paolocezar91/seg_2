import { Component, OnInit, Input, ElementRef, Output, EventEmitter } from "@angular/core";
import * as $ui from "libs/seg-ui";

@Component({
	selector: "multi-dropdown",
	templateUrl: "./multi-dropdown.component.html",
	styleUrls: ["./multi-dropdown.component.scss"]
})
export class MultiDropdownComponent implements OnInit {

	container;
	frame;
	optionsDropdown;
	open_: boolean = false;
	open: boolean = false;
	selectedOptions;

	@Input() label;
	@Input() options;
	@Input() model;
	@Output() modelChange = new EventEmitter<boolean>();
	@Input() asString;

	constructor(private el: ElementRef) { }

	ngOnInit() {

		if(this.model == null)
			this.model = [];
		this.selectedOptions = this.model || [];
		this.selectedOptions.forEach(opt => {
			this.options.forEach(ctry => {
				if(ctry.value === opt){
					ctry.selected = true;
					return;
				}
			});
		});
		this.container = this.el.nativeElement.querySelector("#container");
	}

	ngOnDestroy(): void {
		cancelAnimationFrame(this.frame);
	}

	addOption = (value) => {
		this.selectedOptions.push(value);
	};

	remOption = (value) => {
		this.selectedOptions.splice(this.selectedOptions.indexOf(value),1);
	};

	getSelectedOptions() {
		let results = [],
		resultsString = "";
		this.options.forEach((option) => {
			if (option.selected)
				results.push(option);
		});

		if (this.asString){
			resultsString = results.toString();
			this.model = resultsString
			this.modelChange.emit(this.model);
			return;
		}

		this.model = results;
		this.modelChange.emit(this.model);
	};

	removeValue = key => {
		this.options.find(opt => opt.name === key).selected = false;
		this.model = this.model.filter(res => res.name !== key);
		this.modelChange.emit(this.model);
	};

	getLabel = key => this.options.find(opt => opt.name === key).name;

	ngDoCheck() {
		if(this.open_ !== this.open){
			if(this.open){
				this.frame = requestAnimationFrame(() => {
					this.optionsDropdown = this.container.querySelector("ul");
					this.updateDropdownPosition();
				});
			} else if (this.frame)
			cancelAnimationFrame(this.frame);

			this.open_ = this.open;
		}
	}

	updateDropdownPosition = () => {
		const IEFix = (notIE, IE) => !~window.navigator.userAgent.indexOf("Trident") ? notIE : IE,
		parent = $ui.getParents(this.el.nativeElement, document.querySelector("[scrollable]")),
		containerScrollTop = parent ? parent.scrollTop : 0,
		containerScrollLeft = parent ? parent.scrollLeft : 0,
		buttonBounds = $ui.getBoundingRect(this.container),
		dropdownBounds = $ui.getBoundingRect(this.optionsDropdown),
		min = {
			x: -buttonBounds.left,
			y: -buttonBounds.top
		},
		max = {
			x: window.innerWidth - buttonBounds.left - dropdownBounds.width,
			y: window.innerHeight - buttonBounds.top - dropdownBounds.height
		},
		targetPosition = {
			x: Math.min(max.x, Math.max(min.x, buttonBounds.width / 2)),
			y: Math.min(max.y, Math.max(min.y, buttonBounds.height / 2 - dropdownBounds.height / 2))
		};

		if (targetPosition.x < buttonBounds.width / 4)
			targetPosition.x = Math.min(max.x, Math.max(min.x, -dropdownBounds.width + buttonBounds.width / 2));

		const posX = targetPosition.x - IEFix(0, buttonBounds.width) - containerScrollLeft,
		posY = targetPosition.y - containerScrollTop;

		$ui.transform.position(this.optionsDropdown, posX, posY);
		this.frame = requestAnimationFrame(this.updateDropdownPosition);
	};

}
