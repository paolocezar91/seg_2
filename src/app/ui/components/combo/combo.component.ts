import { Component, OnInit, Input, Output, ElementRef, EventEmitter, OnDestroy, DoCheck, HostListener } from "@angular/core";
import { template } from "lodash";
import * as $ui from "libs/seg-ui";

/**
 * Usage for combo component:
 * If you have an array of objects, you have to specify the attribute that should work as value and the attribute that should work as label
 * ``<combo [(model)]="value" [data]="objectArray" value-as="name" placeholder="ObjectArray"></combo>``
 *
 * If you specify only the attribute to work as value it will be set as the label as well
 * ``<combo [(model)]="value" [data]="objectArray" value-as="name" placeholder="ObjectArray"></combo>``
 *
 * If its a regular array, its value and label will be the same
 * ``<combo [(model)]="value" [data]="array"  placeholder="Array"></combo>``
 */
@Component({
	selector: "combo",
	templateUrl: "./combo.component.html",
	styleUrls: ["./combo.component.scss"]
})
export class ComboComponent implements OnInit, OnDestroy, DoCheck {

	open = false;
	open_ = false;
	valueAsFn: any = null;
	labelAsFn: any = null;
	container: Element;
	optionsDropdown: Element;
	frame: number;
	modelOption: any;
	inputValue: any;

	@Input() data: Array<any>;
	@Input() placeholder: String;
	@Input("input-placeholder") inputPlaceholder: String;
	@Input("value-as") valueAs: String;
	@Input("label-as") labelAs: String;
	@Input("click-callback") clickCallback;
	@Input("change-callback") changeCallback;
	@Input("remove-callback") removeCallback;
	// @Input() parseFn;
	_model;
	@Input() set model(value) {
		this._model = value;
		if(this.valueAsFn)
			this.modelOption = this.data.find(option => this.valueAsFn(option) === value);
	}

	@Output() modelChange = new EventEmitter<any>();

	@HostListener("document:click", ["$event"]) clickout(event) {
		if(!event.path.some(dom => dom === this.el.nativeElement))
			this.open = false;
	}

	constructor(private el: ElementRef) { }

	ngOnInit() {
		this.container = this.el.nativeElement.querySelector("#container");

		// If no valueAs or labelAs are provided, they should just be the value of the array
		this.valueAsFn = (opt) => opt;
		this.labelAsFn = (label) => label;

		if (this.valueAs || this.labelAs) {
			// if the valueAs is provided it should be parsed
			if (this.valueAs)
				this.valueAsFn = template("{{ " + this.valueAs + " }}", {interpolate: /{{([\s\S]+?)}}/g});

			// if the labelAs is provided it should be parsed
			if (this.labelAs)
				this.labelAsFn = template("{{ " + this.labelAs + " }}", {interpolate: /{{([\s\S]+?)}}/g});
			else if (this.valueAs) // else, and there is a valueAs, the labelAs is the same as valueAs
				this.labelAsFn = template("{{ " + this.valueAs + " }}", {interpolate: /{{([\s\S]+?)}}/g});
		}

		if (this._model)
			this.modelOption = this.data.find(option => this.valueAsFn(option) === this._model);
	}

	ngOnDestroy(): void {
		cancelAnimationFrame(this.frame);
	}

	// opens/closes combo
	ngDoCheck() {
		if (this.open_ !== this.open) {
			if (this.open) {
				this.frame = requestAnimationFrame(() => {
					this.optionsDropdown = this.container.querySelector("ul");
					this.updateDropdownPosition();
				});
			} else if (this.frame)
				cancelAnimationFrame(this.frame);

			this.open_ = this.open;
		}
	}

	select(option) {
		this.modelOption = option;
		this.modelChange.emit(this.valueAsFn(option));
		this.open = false;
	}

	updateDropdownPosition() {
		const IEFix = (notIE, IE) => !~window.navigator.userAgent.indexOf("Trident") ? notIE : IE,
			parent = $ui.getParents(this.el.nativeElement, document.querySelector("[scrollable]")),
			containerScrollTop = parent ? parent.scrollTop : 0,
			containerScrollLeft = parent ? parent.scrollLeft : 0,
			buttonBounds = $ui.getBoundingRect(this.container),
			dropdownBounds = $ui.getBoundingRect(this.optionsDropdown),
			min = {
				x: -buttonBounds.left,
				y: -buttonBounds.top
			},
			max = {
				x: window.innerWidth - buttonBounds.left - dropdownBounds.width,
				y: window.innerHeight - buttonBounds.top - dropdownBounds.height
			},
			targetPosition = {
				x: Math.min(max.x, Math.max(min.x, buttonBounds.width / 2)),
				y: Math.min(max.y, Math.max(min.y, buttonBounds.height / 2 - dropdownBounds.height / 2))
			};

		if (targetPosition.x < buttonBounds.width / 4)
			targetPosition.x = Math.min(max.x, Math.max(min.x, -dropdownBounds.width + buttonBounds.width / 2));

		const posX = targetPosition.x - IEFix(0, buttonBounds.width) - containerScrollLeft,
		posY = targetPosition.y - containerScrollTop;

		$ui.transform.position(this.optionsDropdown, posX, posY);
		this.frame = requestAnimationFrame(() => this.updateDropdownPosition());
	}

	callClickCallback() {
		if (this.clickCallback)
			this.clickCallback(this.inputValue);
	}

	callChangeCallback() {
		if (this.changeCallback)
			this.changeCallback(this.inputValue);
	}

	remove(key) {
		this.removeCallback(key);
	}

}
