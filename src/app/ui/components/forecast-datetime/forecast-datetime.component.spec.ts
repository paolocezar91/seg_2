import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastDatetimeComponent } from './forecast-datetime.component';

describe('ForecastDatetimeComponent', () => {
  let component: ForecastDatetimeComponent;
  let fixture: ComponentFixture<ForecastDatetimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastDatetimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastDatetimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
