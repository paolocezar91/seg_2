import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'forecast-datetime',
  templateUrl: './forecast-datetime.component.html',
  styleUrls: ['./forecast-datetime.component.scss']
})
export class ForecastDatetimeComponent implements OnInit {

  chosenDay;
  chosenTime;
  days = [moment(), moment().add(1,"d"), moment().add(2,"d"), moment().add(3,"d"), moment().add(4,"d"), moment().add(5,"d")]

  @Output() ngModelChange = new EventEmitter<boolean>();

  @Input() ngModel;

  @Input() times;

  @Input() availableTimes;

  constructor() { }

  ngOnInit() {
  }

  getTimeInterval(day, index) {
    day = day ? day : moment();

    this.chosenDay = day;

    const i = index ? index : 0;

    this.times = []

    this.availableTimes[i].array.forEach(hour => {
      const parsedTime = moment().set({"year": day.year(), "month": day.month(), "day": day.date()-1, "hour":hour, "minute":0});
      this.times.push(parsedTime);
    });

  }

  updateModel(datetime){
    this.chosenTime = datetime;
    this.ngModelChange.emit(this.ngModel);
    this.ngModel = datetime;
  }

}
