import { Component, OnInit, OnDestroy, ElementRef, Input, Output, EventEmitter,  DoCheck } from '@angular/core';
import * as moment from 'moment';
import * as $ui from 'libs/seg-ui';

let self;

@Component({
  selector: 'datetime',
  templateUrl: './datetime.component.html',
  styleUrls: ['./datetime.component.scss']
})
export class DatetimeComponent implements OnInit, OnDestroy, DoCheck {

	myElement: any
	container: any
	datetimepicker: any
	open: boolean;
	open_: boolean;
	@Input() min: moment.Moment;
	@Input() max: moment.Moment;
	@Input() model: moment.Moment;
	@Output() modelChange =  new EventEmitter<moment.Moment>();
	model_: moment.Moment;
	frame: any

	constructor(private el: ElementRef) { }

	ngOnInit() {
		self = this;
		window.addEventListener("click", this.close);
		this.myElement = this.el.nativeElement;
		this.container = this.el.nativeElement.querySelector("#container");
	};

	ngOnDestroy() {
		window.removeEventListener("click", this.close);
	}

	ngDoCheck() {
		if(this.model_ !== this.model){
			this.model_ = this.model;
			this.modelChange.emit(this.model_);
		}

		if(this.open_ !== this.open){
			if(this.open){
				this.frame = requestAnimationFrame(() => {
					this.datetimepicker = this.container.querySelector("datetimepicker");

					this.updateDropdownPosition();
				});
			} else if (this.frame)
				cancelAnimationFrame(this.frame);

			this.open_ = this.open;
		}
	}

	close(event) {
		let target = null;
		if (event){
			target = event.target || event.srcElement;
		}
		if (target){
			if (!self.myElement.contains(target)){
				self.open = false;
			}
		} else {
			self.open = false;
		}
	}

	updateDropdownPosition = () => {
		const IEFix = (notIE, IE) => !~window.navigator.userAgent.indexOf("Trident") ? notIE : IE,
			parent_ = $ui.getParents(this.el.nativeElement, document.querySelector("[scrollable]")),
			containerScrollTop = parent_ ? parent_.scrollTop : 0,
			containerScrollLeft = parent_ ? parent_.scrollLeft : 0,
			buttonBounds = $ui.getBoundingRect(this.container),
			datetimepickerBounds = $ui.getBoundingRect(this.datetimepicker),
			min = {
				x: -buttonBounds.left,
				y: -buttonBounds.top
			},
			max = {
				x: window.innerWidth - buttonBounds.left - datetimepickerBounds.width,
				y: window.innerHeight - buttonBounds.top - datetimepickerBounds.height - buttonBounds.height
			},
			targetPosition = {
				x: Math.min(max.x, Math.max(min.x, buttonBounds.width)),
				y: Math.min(max.y, Math.max(min.y, -buttonBounds.height))
			},
			posX = targetPosition.x - IEFix(0, buttonBounds.width) - containerScrollLeft,
			posY = targetPosition.y - containerScrollTop;

		$ui.transform.position(this.datetimepicker, posX, posY);
		this.frame = requestAnimationFrame(this.updateDropdownPosition);
	};

	getCurrentMoment () {
		return moment();
	};

	isDateValid (date) {
		if(!date)
			return false;

		if(this.min && date.isBefore(this.min))
			return false;

		if(this.max && date.isAfter(this.max))
			return false;

		return true;
	};

}
