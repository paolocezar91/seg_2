import { Component, Input, Output, ElementRef, EventEmitter, Renderer2 } from "@angular/core";

@Component({
  selector: "toggle",
  templateUrl: "./toggle.component.html",
  styleUrls: ["./toggle.component.scss"]
})
export class ToggleComponent {

	@Input() set model(value: any) {

		if (value !== this.model_)
			this.model_ = value;

		if (this.model_)
			this.renderer.setAttribute(this.el.nativeElement, "checked", "true");
		else {
			// this.el.nativeElement.removeAttribute("partial");
			this.renderer.removeAttribute(this.el.nativeElement, "checked");
		}
	}

	@Output() modelChange = new EventEmitter<boolean>();
	model_: boolean;
	partial_: boolean;

	@Input() set partial (value: any) {
	   this.partial_ = value;

		if (this.partial_)
			this.renderer.setAttribute(this.el.nativeElement, "partial", "true");
		else {
			this.renderer.removeAttribute(this.el.nativeElement, "partial");
			// this.el.nativeElement.removeAttribute("checked");
		}
	}

	constructor(private el: ElementRef, private renderer: Renderer2) {
		el.nativeElement.onclick = () => {
			this.model = !this.model_;
			this.modelChange.emit(this.model_);
		};
	}

}
