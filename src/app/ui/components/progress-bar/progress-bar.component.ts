import { Component, OnInit, Input } from "@angular/core";

// Usage
// <progress-bar [model]="progress" [max]="150" class="flex-1 background-color-11"></progress-bar>
// max value is optional, otherwise it will be 100
// model is value relative to max that the bar will be filled. If max is not passed, it is the percentage 

@Component({
  selector: "progress-bar",
  templateUrl: "./progress-bar.component.html",
  styleUrls: ["./progress-bar.component.scss"]
})
export class ProgressBarComponent implements OnInit {

	@Input() model;
	@Input() max;

	constructor() { }

	ngOnInit() {
		this.max = this.max || 100;
	}

}
