import { Component, OnInit, OnDestroy, OnChanges, Input, ElementRef, Renderer2, SimpleChanges, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { PreferencesService } from "@core/services/preferences.service";
import { SelectionService } from "@core/services/interactions/selection.service";
import { template } from "lodash";
import { KeyValue } from "@angular/common";
import { BehaviorSubject } from "rxjs";

@Component({
  selector: "grid",
  templateUrl: "./grid.component.html",
  styleUrls: ["./grid.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridComponent implements OnInit, OnDestroy, OnChanges {

	@Input() set table(val) {
		Object.assign(this, val);
	}
	@Input("on-filter") onFilter;

	gridAPI: object;
	name: string;
	rows: any[];
	filteredRows: any[] = [];
	filteredRowsLength: number;
	paginatedData: any[];
	data: Array<any>;
	dataChanges: BehaviorSubject<any[]>;

	editable = false;
	itemAs: string;
	fields: any;
	customToolbar: any;
	checkboxes: boolean;
	hideToolbar: boolean;
	noResize: boolean;

	sortKeys: any[] = [];
	sortKey;
	sortKeyOrder;

	grid: HTMLElement;
	searchAll = true;
	oldSearchTerm = "";
	searchTerm = "";

	removeRow: Function;
	onSelectionChange: Function;
	onCheckboxChange: Function;
	onDoubleClick: Function;
	selectionEvent;
	selectionEventHandler;

	allVisible: boolean;
	searchPlaceholder: string;
	selectedPage: number;
	selectedIndex: number;
	itemsPerPage: number;
	lastSelectedRowIndex: number;
	oldItemsPerPage: number;

	paginate: any;
	page = 0;
	paginationShow = true;

	constructor(
		private el: ElementRef,
		private preferencesService: PreferencesService,
		private selectionService: SelectionService,
		private renderer: Renderer2,
		private cdr: ChangeDetectorRef,
		// private exportService: ExportService
	) { 
		
	}

	ngOnInit() {
		// console.time(this.name);
		this.grid = this.el.nativeElement;
		this.renderer.setAttribute(this.grid, "id", this.name);

		this.toggleSearchAll();

		if (this.sortKey) {
			if (this.sortKeyOrder && this.sortKeyOrder === "asc")
				this.sortBy(this.sortKey);
			this.sortBy(this.sortKey, true);
		}

		Object.keys(this.fields).forEach((key) => {
			if (!this.editable)
				this.sortKeys.push("+" + "values." + key);
		});

		this.selectionEventHandler = this.selectionService.onSelectionChange(() => {
			let selected;

			if (
				Array.isArray(this.filteredRows) &&
				this.filteredRows[0] &&
				this.filteredRows[0].item &&
				this.filteredRows[0].item.get
			) {
				selected = this.filteredRows[0].item.get && this.selectionService.getSelected(this.filteredRows[0].item.get("seg_type"))[0];
				if (selected) {
					if (this.paginate) {
						for (let i = 0; i < this.filteredRows.length; i++) {
							if (selected === this.filteredRows[i].item) {
								this.selectedPage = Math.floor(i / this.itemsPerPage);
								this.selectedIndex = i % this.itemsPerPage;
							}
						}
					} else {
						this.selectedPage = 0;
						this.selectedIndex = this.filteredRows.findIndex(({item}) => item === selected);
					}

					this.page = this.selectedPage;
				}
				this.cdr.detectChanges();
			}
		});

		if (this.customToolbar) {
			if (this.customToolbar.scope)
				this.customToolbar.scope = Object.assign(this.customToolbar.scope, {grid: this});
			else
				this.customToolbar = Object.assign(this.customToolbar, {scope: {grid: this}});
		}

		// Listen to data changes to updateRows;
		this.dataChanges.subscribe(data => {
			this.data = data;
			if (data.length) {
				this.updateRows();
				this.cdr.detectChanges();
			}
		});
	}

	ngOnDestroy() {
		// Need to erase searchTerm and run filter again, lest hidden features remain hidden after closing grid
		this.searchTerm = "";
		if (this.rows && this.rows.length)
			this.filteredRows = this.rows.filter((row) => this.filterRows(row));
		if (typeof this.onFilter !== "undefined") {
			this.onFilter(this.filteredRows, this.rows);
		}

		// remove selection event handler
		this.selectionEventHandler();
		this.dataChanges.unsubscribe();
	}

	ngOnChanges(changes: SimpleChanges) {
		if (this.paginate)
			this.itemsPerPage = Number(this.preferencesService.getMapPreference("grid." + this.name + ".itemsPerPage") || this.paginate.itemsPerPage || 50);

		this.updateRows();
	}

	valueAscOrder = (a: any, b: any): number => {
		return a.value.order < b.value.order ? -1 : (b.value.order < a.value.order ? 1 : 0);
	}

	triggerToolbarChanges() {
		return 
	}

	doFilter() {
		this.getValues();

		this.filteredRows = this.rows.map((row) => this.filterRows(row));
		this.filteredRowsLength = this.filteredRows.filter(row => !row.hide).length;
		

		this.oldSearchTerm = this.searchTerm;
		this.groupToPages();
	}

	getValues() {
		[].slice.call(this.el.nativeElement.querySelectorAll(".rows"))
			.forEach((row) => {
				const index = Number(row.id.replace("row_", ""));
				row.querySelectorAll(".values")
					.forEach((cell) => {
						this.rows[index].values = Object.assign(this.rows[index].values, {[cell.id]: cell.textContent});
					});
			});
	}

	updateRows() {
		this.filteredRows = this.rows = this.data.map((item, i) => ({
			id: i,
			api: Object.assign({}, {[this.itemAs]: item}, this.gridAPI),
			item,
			values: {}
		}));
		this.filteredRowsLength = this.filteredRows.filter(row => !row.hide).length;

		this.groupToPages();
	}

	groupToPages() {
		this.paginatedData = [];
		this.page = 0;

		if (this.paginate && Array.isArray(this.filteredRows)) {
			for (let i = 0; i < this.filteredRows.length; i++) {
				if (i % this.itemsPerPage === 0) {
					this.paginatedData[Math.floor(i / this.itemsPerPage)] = [ this.filteredRows[i] ];
				} else {
					this.paginatedData[Math.floor(i / this.itemsPerPage)].push(this.filteredRows[i]);
				}

			}
		} else {
			this.paginatedData[0] = this.filteredRows;
		}

		this.paginationShow = !(this.paginatedData.length === 1);
	}

	showingItems() {
		let msg = "";
		if (this.paginate && Array.isArray(this.filteredRows)) {
			msg += "Showing items ";
			msg += ((this.itemsPerPage * this.page) + 1);
			msg += " to ";
			if ((this.itemsPerPage * (this.page + 1)) < this.filteredRows.length) {
				msg += (this.itemsPerPage * (this.page + 1));
			} else {
				msg += this.filteredRows.length;
			}
			msg += " of ";
			msg += this.filteredRows.length;
		}
		return msg;
	}

	getColspan() {
		return Object.keys(this.fields).filter(field => this.fields[field].visible).length;
	}

	filterRows(row) {
		if (this.searchTerm !== "") {
			const found = Object.entries(this.fields)
				.filter((field: Array<any>) => (field.length && field[1].visible))
				.some(([key, field]: Array<any>) => {
					if (!field.searchable)
						return;
					else
						return row.values[key] && row.values[key].toLowerCase().match(this.searchTerm.toLowerCase());
				});

			if (!found)
				row.hide = true;
			else
				delete row.hide;

			return row;
		}

		delete row.hide;
		return row;
	}

	updateSearchPlaceholder() {
		let searchableFields = Object.values(this.fields).filter((field: any) => field.searchable).map((filter: any) => filter.label.replace(/.\(.*\)/, "")).join(", ");

		if (searchableFields.length > 36)
			searchableFields = searchableFields.substr(0, 33) + "...";

		this.searchPlaceholder = searchableFields.length ? searchableFields : "No fields selected";
	}

	isSearchable() {
		return Object.values(this.fields).some((field: any) => field.searchable);
	}

	sortBy(key: string, shouldSort?: boolean) {
		let asc;
		const regex = new RegExp("([+-])values." + key);

		for (let i = 0; i < this.sortKeys.length; i++) {
			const match = regex.exec(this.sortKeys[i]);

			if (match) {
				asc = match[1] === "+" ? "-" : "+";
				this.sortKeys.splice(i, 1);
				break;
			}
		}

		this.sortKeys.unshift(asc + "values." + key);

		if (shouldSort) {
			this.getValues();
			this.filteredRows = this.filteredRows.sort(this.sortMultiFields(this.sortKeys));
			this.filteredRowsLength = this.filteredRows.filter(row => !row.hide).length;
		}
	}

	sortMultiFields(prop) {
		return (a, b) => {
			for (let i = 0; i < prop.length; i++) {
				const reg = /^\d+$/;
				let x = 1, field = prop[i];

				if (prop[i].indexOf("-") === 0) {
					field = prop[i].substr(1, prop[i].length);
					x -= x;
				}

				if (reg.test(a[field])) {
					a[field] = parseFloat(a[field]);
					b[field] = parseFloat(b[field]);
				}

				if (a[field] > b[field])
					return x;

				else if (a[field] < b[field])
					return -x;
			}
		};
	}

	toggleSearchAll() {
		Object.values(this.fields).forEach((field: any) => field.searchable = this.searchAll);
		this.updateSearchPlaceholder();
	}

	changeItemsPerPage($event) {
		const length = ((this.filteredRows && this.filteredRows.length) || 0);
		if (length / this.itemsPerPage > 20)
			this.itemsPerPage = Math.ceil(length / 20);

		if (this.itemsPerPage !== this.oldItemsPerPage) {
			this.groupToPages();
			this.preferencesService.setMapPreference("grid." + this.name + ".itemsPerPage", Number(this.itemsPerPage));
		}

		this.oldItemsPerPage = this.itemsPerPage;
	}

	updateVisibleFields() {
		setTimeout(() => {
			Object.values(this.fields).forEach((field: any) => field.visible = this.allVisible);
		});
	}

	getSortDirection(key) {
		const regex = new RegExp("([+-])values." + key);

		for (let i = 0; i < this.sortKeys.length; i++) {
			const match = regex.exec(this.sortKeys[i]);

			if (match)
				return match[1] === "+" ? "chevron-up" : "chevron-down";
		}

		return "chevron-up";
	}

	allChecked() {
		return this.filteredRows && this.filteredRows.every(({selected}) => selected);
	}

	checkboxAll() {
		const selectedStatus = this.allChecked();
		this.filteredRows.forEach(row => row.selected = !selectedStatus);

		if (!selectedStatus) {
			this.selectionService.deselect(this.selectionService.getSelectedFeatures());
			this.selectionService.select(this.filteredRows.map(row => row.item));
		} else {
			this.selectionService.deselect(this.filteredRows.map(row => row.item));
		}

		if (this.onCheckboxChange)
			this.onCheckboxChange({
				// changedRows,
				// selectedRows
			});
	}

	checkboxRow(clickedRow) {
		clickedRow.selected = !clickedRow.selected;
		this.selectionService.deselect(this.selectionService.getSelectedFeatures());
		this.selectionService.select(this.filteredRows.filter(row => row.selected).map(row => row.item));

		if (this.onCheckboxChange)
			this.onCheckboxChange({
				// changedRows,
				// selectedRows
			});
	}

	selectRow(clickedRow, shiftKey, ctrlKey) {
		let selectedRows = [];
		const changedRows = [];

		if (!shiftKey && !ctrlKey)
			this.filteredRows.forEach((row, i) => {
				const previousState = row.selected;

				if (row === clickedRow) {
					row.selected = true;
					selectedRows.push(row);
					this.lastSelectedRowIndex = i;
				} else if (row.selected)
					row.selected = false;

				if (previousState !== row.selected)
					changedRows.push(row);
			});
		else if (shiftKey) {
			const indexOfClicked = this.filteredRows.indexOf(clickedRow),
				min = Math.min(indexOfClicked, this.lastSelectedRowIndex),
				max = Math.max(indexOfClicked, this.lastSelectedRowIndex);

			this.filteredRows.forEach((row, i) => {
				const previousState = row.selected;

				if (i < min || i > max) {
					row.selected = false;
				} else {
					selectedRows.push(row);
					if (!row.selected)
						row.selected = true;
				}

				if (previousState !== row.selected)
					changedRows.push(row);
			});
		} else if (ctrlKey) {
			const previousValue = clickedRow.selected;
			clickedRow.selected = !previousValue;
			changedRows.push(clickedRow);
			selectedRows = this.filteredRows.filter(row => row.selected);
			if (clickedRow.selected)
				this.lastSelectedRowIndex = this.filteredRows.indexOf(clickedRow);
		}

		if (this.onSelectionChange)
			this.onSelectionChange(
				changedRows,
				selectedRows
			);
	}

	clearSelection() {
		this.rows.forEach(row => row.selected = false);
	}

	export() {
		if (this.filteredRows && this.filteredRows.length) {
			let data = this.filteredRows.filter(({selected}) => selected);

			// We have to update templates in case for export of all fields.
			// Since we don't generate templates that are visible we must do this
			// this.updateTemplates({exportAll: true});

			// If one or none rows are selected, export all data
			if (data.length < 2)
				data = this.filteredRows;

			// this.exportService.promptExport({
			// 	filename: this.name,
			// 	data: data.map(({item}) => item),
			// 	view: {
			// 		columns: Object.entries(this.fields).reduce((columns, [key, {visible, label}]) => Object.assign(columns, {
			// 			[key]: {
			// 				visible,
			// 				label: template(label)(this.gridAPI)
			// 			}
			// 		}), {}),
			// 		rows: data.map(({values}) => values)
			// 	},
			// 	fromGrid: true
			// });
		}
	}

}
