import { Component, ElementRef, OnInit, Input, ContentChild, AfterContentInit, forwardRef, OnChanges, SimpleChanges } from "@angular/core";

@Component({
	selector: "pager-controls",
	templateUrl: "./pager-controls.component.html",
	styleUrls: ["./pager.component.scss"]
})
export class PagerControlsComponent {

	public pager: any;

	constructor() {
	}

	prevXPage() {
		this.pager.setXPage(this.pager.currentXPage() - 1);
	}

	nextXPage() {
		this.pager.setXPage(this.pager.currentXPage() + 1);
	}

	prevYPage() {
		this.pager.setYPage(this.pager.currentYPage() - 1);
	}

	nextYPage() {
		this.pager.setYPage(this.pager.currentYPage() + 1);
	}
}

@Component({
	selector: "pager",
	template: "<ng-content selector='pager-content'></ng-content>"
})
export class PagerComponent implements OnInit, AfterContentInit {

	private element;
	private spacer;
	private self = this;

	constructor(private el: ElementRef) {
	}

	@ContentChild(forwardRef(() => PagerControlsComponent)) private pagerControls: PagerControlsComponent;


	ngOnInit() {
		this.element = this.el.nativeElement.querySelector("pager-content");

		if (!this.element)
			throw new Error("ui.components.pager: No pager-content element found!");

		this.spacer = document.createElement("DIV");
		this.spacer.className = "spacer";
		this.spacer.style.display = "none";
		this.element.appendChild(this.spacer);
	}

	ngAfterContentInit() {
		this.pagerControls.pager = this;
	}

	currentXPage() {
		return Math.ceil(this.element.scrollWidth / (this.element.scrollWidth - this.element.scrollLeft));
	}

	currentYPage() {
		return Math.ceil(this.element.scrollHeight / (this.element.scrollHeight - this.element.scrollTop));
	}

	setXPage(page) {
		if (page > 0 && page <= this.getXPages()) {
			this.element.scrollLeft = (page - 1) * this.element.offsetWidth;
		}
	}

	setYPage(page) {
		if (page > 0 && page <= this.getYPages())
			this.element.scrollTop = (page - 1) * this.element.offsetHeight;
	}

	getXPages() {
		const xPages = Math.ceil(this.element.scrollWidth / this.element.offsetWidth);
		this.spacer.style.width = (xPages * 100) + "%";
		return xPages;
	}

	getYPages() {
		return Math.ceil(this.element.scrollHeight / this.element.offsetHeight);
	}

}
