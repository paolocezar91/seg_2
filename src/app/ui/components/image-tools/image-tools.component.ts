import { Component, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { PreferencesService } from '@core/services/preferences.service';
import { MapService } from '@core/services/map/map.service';
import { SegLayer } from '@core/services/layer/SegLayer';

let preferencesService = PreferencesService.prototype;
let mapService = MapService.prototype;
let SEGinterface = undefined;
let ol = undefined;

@Component({
	selector: 'image-tools',
	templateUrl: './image-tools.component.html',
	styleUrls: ['./image-tools.component.scss']
})
export class ImageToolsComponent /*extends SegLayer */implements OnInit {

	alpha = 255;
	contrast = 0;
	brightness = 0;
	show = false;
	original = null;
	hasImage = false;

	@Input() image;

	@Input() container;

	@Input() feature;

	@Output() imageChange = new EventEmitter<boolean>();

	constructor(private el: ElementRef/*, options, olSourceLayerSubclass*/) { 
	/*
    super(options, olSourceLayerSubclass);
    const segSource = options.source;

    if(typeof segSource === "object")
      this.setSource(segSource);
	*/
	}

	ngOnInit() {
		if (typeof this.feature.get("imageLayer") !== "undefined"){
			this.hasImage = true;
			const preferences = preferencesService.getSessionPreference("EO."+this.feature.get("doi")+".imagetransformation");
			const imageLayerSource = this.feature.get("imageLayer").ol_.get("source");
			const maxResolution = this.feature.get("imageLayer").ol_.getMaxResolution();
			const extent = this.feature.get("geometry").getExtent();
			const projection = mapService.getMap().getProjection();

			if (typeof preferences !== "undefined"){
				this.brightness = preferences.brightness;
				this.alpha = preferences.alpha;
				this.contrast = preferences.contrast;
			}

			if (typeof imageLayerSource!== "undefined"){
				const image =	imageLayerSource.getImageInternal(extent,maxResolution, ol.has.DEVICE_PIXEL_RATIO, projection).getImage();
				this.original = SEGinterface.image.getImageAsCanvas(image);
			}
		} else
			this.original = SEGinterface.image.getImageAsCanvas(this.feature.get("thumbnail"));
	}

	setBrightness (brightness,contrast,alpha){
		const image = this.feature.get("imageLayer").ol_.getSource().getImageInternal(this.feature.get("geometry").getExtent(),this.feature.get("imageLayer").ol_.getMinResolution(), ol.has.DEVICE_PIXEL_RATIO, mapService.getMap().getProjection() ).getImage();

		const c = SEGinterface.image.applyImageTransformation(image.src,brightness,contrast,alpha);

		if (typeof c !== "undefined"){
			image.src = c.toDataURL();
			this.feature.get("imageLayer").ol_.getImageInternal(this.feature.get("geometry").getExtent(),this.feature.get("imageLayer").ol_.getMinResolution(), ol.has.DEVICE_PIXEL_RATIO, mapService.getMap().getProjection()).setImage(image);
		}
	}

	updateImage () {
		this.setBrightness (this.brightness,this.contrast,this.alpha);
		this.feature.get("imageLayer").ol_.getSource().changed();
		preferencesService.setSessionPreference("EO."+this.feature.get("doi")+".imagetransformation",{brightness: this.brightness,contrast: this.contrast, alpha: this.alpha});

		//	self.feature.get("imageLayer").ol_.getSource().changed();
		//update cached images
	}

	reset (){
		this.brightness = 0;
		this.alpha = 255;
		this.contrast = 0;

		this.setBrightness (this.brightness,this.contrast,this.alpha);
		this.feature.get("imageLayer").ol_.getSource().changed();
		preferencesService.setSessionPreference("EO."+this.feature.get("doi")+".imagetransformation",{brightness: this.brightness,contrast: this.contrast, alpha: this.alpha});
	}

	close () {

	}

}
