import { Component, OnInit, Input, OnChanges, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { CountryService } from "@core/services/country.service";

@Component({
	selector: "flag",
	template: `<img [src]="data">`,
	styleUrls: ["./flag.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class FlagComponent implements OnInit, OnChanges {

	constructor(
		private countryService: CountryService,
		private cdr: ChangeDetectorRef
	) { }

	@Input() flag;
	data;

	ngOnInit() {
		return this.getFlag();
	}

	ngOnChanges() {
		this.getFlag();
		return;
	}

	getFlag() {
		return this.countryService.getFlag(this.flag)
			.then((res) => {
				this.data = res;
				this.cdr.detectChanges();
			});
	}

}
