import { Component, ViewChild, ComponentFactoryResolver, ContentChildren, AfterContentInit, QueryList, EventEmitter, Output } from "@angular/core";
import * as $o from "libs/seg-object";
import { TabComponent } from "@ui/components/tab-group/tab.component";
import { DynamicTabsDirective } from "@ui/components/tab-group/dynamic-tabs.directive";

@Component({
	selector: "tab-group",
	templateUrl: "./tab-group.component.html",
	styleUrls: ["./tab-group.component.scss"]
})
export class TabGroupComponent implements AfterContentInit {

	dynamicTabs: TabComponent[] = [];
	@ContentChildren(TabComponent) tabs: QueryList<TabComponent>;
	@ViewChild(DynamicTabsDirective) dynamicTabPlaceholder: DynamicTabsDirective;
	@Output() closeChange: EventEmitter<TabComponent> = new EventEmitter<TabComponent>();

	constructor(private _componentFactoryResolver: ComponentFactoryResolver) {}

	ngAfterContentInit() {
		// get all active tabs
		const activeTabs = this.tabs.filter(tab => tab.open);

		// if there is no active tab set, activate the first
		if (this.tabs.length > 0 && !activeTabs.length)
			this.selectTab(this.tabs.first);
	}

	createTab(label: string, template, data, persistent = false, open = false) {
		const componentFactory = this._componentFactoryResolver.resolveComponentFactory(TabComponent), // get a component factory for our TabComponent
			viewContainerRef = this.dynamicTabPlaceholder.viewContainer, // fetch the view container reference from our anchor directive
			componentRef = viewContainerRef.createComponent(componentFactory), // create a component instance
			instance: TabComponent = componentRef.instance as TabComponent; // set the according properties on our component instance

		instance.label = label;
		instance.template = template;
		instance.dataContext = data;
		instance.persistent = persistent;

		// remember the dynamic component for rendering the
		// tab navigation headers
		this.dynamicTabs.push(componentRef.instance as TabComponent);

		// set it active
		if (open)
			this.selectTab(this.dynamicTabs[this.dynamicTabs.length - 1]);
	}

	selectTab(tab: TabComponent) {
		// deactivate all tabs
		this.tabs.toArray().forEach(tab_ => (tab_.open = false));
		this.dynamicTabs.forEach(tab_ => (tab_.open = false));

		// activate the tab the user has clicked on.
		tab.open = true;
	}

	closeTab(tab: TabComponent) {
		for (let i = 0; i < this.dynamicTabs.length; i++) {
			if (this.dynamicTabs[i] === tab) {
				// remove the tab from our array
				this.dynamicTabs.splice(i, 1);

				// destroy our dynamically created component again
				const viewContainerRef = this.dynamicTabPlaceholder.viewContainer;
				// let viewContainerRef = this.dynamicTabPlaceholder;
				viewContainerRef.remove(i);

				// set tab index to 1st one
				const activeTabs = this.tabs.toArray().concat(this.dynamicTabs);

				if (activeTabs.length)
					this.selectTab(activeTabs[0]);

				this.closeChange.emit(tab);
				break;
			}
		}

	}

	closeActiveTab() {
		const activeTabs = this.dynamicTabs.filter(tab => tab.open);
		if (activeTabs.length > 0) {
			// close the 1st active tab (should only be one at a time)
			this.closeTab(activeTabs[0]);
		}
	}

	// showTabs() {
	// 	this.tabComponents.forEach((tabComponent, index) => {
	// 		tabComponent.tabGroup = this;

	// 		if (this.tabs[index] !== tabComponent)
	// 			this.addTab(tabComponent);

	// 		if (tabComponent.open_)
	// 			tabComponent.show();
	// 		else
	// 			tabComponent.hide();
	// 	});

	// 	if (!this.tabComponents.some(tab => tab.open_))
	// 		this.tabComponents.first.show();
	// }

	// addTab(tab) {
	// 	this.tabs.push(tab);

	// 	if (!this.selectedTab)
	// 		this.select(tab);
	// }

	// selectTab(tab) {
	// 	// if we"re trying to open the same tab, do nothing
	// 	if (this.selectedTab === tab)
	// 		return;

	// 	if (this.selectedTab) {
	// 		this.selectedTab.hide();
	// 		this.selectedTab.open = false;
	// 	}

	// 	this.selectedTab = tab;
	// 	tab.show();
	// }

	// removeTab(tab: TabComponent, element?: HTMLElement) {
	// 	// try to open previous tab or next tab
	// 	if (this.selectedTab === tab) {
	// 		const indexOfTab = this.tabs.indexOf(tab);

	// 		if (indexOfTab > 0)
	// 			this.select(this.tabs[indexOfTab - 1]);
	// 		else if (indexOfTab + 1 < this.tabs.length)
	// 			this.select(this.tabs[indexOfTab + 1]);
	// 	}

	// 	$o.removeFromArray(this.tabs, tab);

	// 	if (element)
	// 		element.remove();

	// 	if (this.openTabsChange && this.openTabs.find(_tab => tab.label === _tab.label)) {
	// 		this.openTabsChange.emit(this.openTabs.filter(_tab => tab.label !== _tab.label));
	// 	}
	// }

}
