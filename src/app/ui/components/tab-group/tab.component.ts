import { Component, Input } from "@angular/core";

@Component({
	selector: "tab",
	template: `<div [style.display]="!open ? 'none' : 'inherit'" class="pane">
		<ng-content></ng-content>
		<ng-container *ngIf="template"
			[ngTemplateOutlet]="template"
			[ngTemplateOutletContext]="{ data: dataContext }"
		></ng-container>
	</div>`
})
export class TabComponent {

	@Input() label;
	@Input() onOpen;
	@Input() open = false;
  	@Input() persistent = false;
	@Input() template;
	@Input() dataContext;
	@Input() disabled;

}
