import { Component, Input, ElementRef, Renderer2, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef, OnChanges } from "@angular/core";
import { StyleService } from "@core/services/style.service";
import { isObservable } from "rxjs";

@Component({
	selector: "icon",
	templateUrl: "./icon.component.html",
	styleUrls: ["./icon.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconComponent implements OnChanges, OnDestroy {

	private svgFile_: string;
	@Input("svg-file") set svgFile(val: string) {
		this.svgFile_ = val;
		this.cdr.detectChanges();
	}

	constructor(
		private el: ElementRef,
		private styleService: StyleService,
		private renderer: Renderer2,
		private cdr: ChangeDetectorRef
	) { }

	ngOnChanges() {
		if (!window["$iconCache"])
			window["$iconCache"] = {};

		if (this.svgFile_) {
			const iconCache = window["$iconCache"][this.svgFile_];

			if (!iconCache) {
				// two step cache, since sometimes it triggers requests several times for the same icon, first creates cache for the request and then for the content
				window["$iconCache"][this.svgFile_] = this.styleService.requestSVG("assets/icons/ui/" + this.svgFile_ + ".svg");
				window["$iconCache"][this.svgFile_].subscribe((svgTemplate) => this.appendAndAddToCache(svgTemplate));
			} else {
				if (typeof iconCache === "string") // template is already set
					this.appendAndAddToCache(iconCache);
				else // subscription to template is available, so do that
					iconCache.subscribe((svgTemplate) => this.appendAndAddToCache(svgTemplate));
			}
		}
	}

	ngOnDestroy() {
		const iconCache = window["$iconCache"][this.svgFile_];
		if (iconCache.unsubscribe)
			iconCache.unsubscribe();
	}

	appendAndAddToCache(svgTemplate) {
		window["$iconCache"][this.svgFile_] = svgTemplate;
		this.removeIcons();
		this.el.nativeElement.insertAdjacentHTML("beforeend", svgTemplate);
	}

	removeIcons() {
		Array.from(this.el.nativeElement.children).forEach(child => {
			this.renderer.removeChild(this.el.nativeElement, child);
		});
	}
}
