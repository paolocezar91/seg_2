import { Component, OnInit, ElementRef, forwardRef, Input, Output, EventEmitter } from "@angular/core";
import { PreferencesService } from "@core/services/preferences.service";
import * as moment from "moment";
import { DateFormatPipe } from "@ui/pipes/date-format.pipe";


// Usage
// <clock [min]="min" [max]="max" [(value)]="moment"></clock>
@Component({
	selector: "clock",
	templateUrl: "./clock.component.html",
	styleUrls: ["./clock.component.scss"],
	providers: [DateFormatPipe]
})
export class ClockComponent implements OnInit {

	@Input() min: any;
	@Input() max: any;
	@Input() model: moment.Moment;
	@Output() modelChange = new EventEmitter<moment.Moment>();

	hoursInput: HTMLInputElement;
	minutesInput: HTMLInputElement;
	title: string;

	constructor(
		private el: ElementRef,
		private preferencesService: PreferencesService,
		private dateFormat: DateFormatPipe
	) { }

	ngOnInit() {
		if (!this.title)
			this.title = "TIME";

		this.title += (" (" + this.preferencesService.getWorkspacePreference("timeFormat") + ")");


		this.hoursInput = Object.assign(<HTMLInputElement> this.el.nativeElement.querySelector("#hours > input"), {
			oninput: (e) => {
				let value = parseInt(e.target.value.substr(0, 2), 10);

				if (isNaN(value)) {
					if (e.target.value !== "")
						e.target.value = "";
				} else {
					if (value > 23)
						value = parseInt(e.target.value[0], 10);

					e.target.value = value;
				}
			},
			onchange: (e) => {
				if (e.target.value !== "")
					this.set("hour", e.target.value);
			}
		});

		this.minutesInput = Object.assign(<HTMLInputElement> this.el.nativeElement.querySelector("#minutes > input"), {
			oninput: (e) => {
				let value = parseInt(e.target.value.substr(0, 2), 10);

				if (isNaN(value)) {
					if (e.target.value !== "")
						e.target.value = "";
				} else {
					if (value > 59)
						value = parseInt(e.target.value[0], 10);

					e.target.value = value;
				}
			},
			onchange: (e) => {
				if (e.target.value !== "")
					this.set("minute", e.target.value);
			}
		});

		// this.ngModelController.$render = writeValue;
		this.writeValue(this.model);
	}

	add(hourOrMinute, value) {
		this.set(hourOrMinute, this.model[hourOrMinute]() + value);
	}

	set(hourOrMinute, value) {
		// if(this.modulo)
		// 	value = value.mod(hourOrMinute === "hour" ? 24 : 60);

		let newMoment = moment(this.model)[hourOrMinute](value);

		if (this.min)
			newMoment = moment.max(this.min, newMoment);
		if (this.max)
			newMoment = moment.min(this.max, newMoment);

		this.writeValue(newMoment);
	}

	canAdd(hourOrMinute, value) {
		const newMoment = moment(this.model).add(value, hourOrMinute);

		return (!this.min || newMoment.isAfter(this.min)) && (!this.max || newMoment.isBefore(this.max));
	}

	writeValue(value: moment.Moment): void {
		this.model = value;

		// When date/time was different from UTC this value was showing incorrectly causing confusion to users
		const valueToShow = this.preferencesService.getWorkspacePreference("timeFormat") === "UTC" ?
			this.model.clone().utc().format("YYYY-MM-DD HH:mm:ss") :
			this.model.clone();

		this.hoursInput.value = this.dateFormat.transform(valueToShow, "HH");
		this.minutesInput.value = this.dateFormat.transform(valueToShow, "mm");

		this.modelChange.emit(this.model.clone());
	}

}
