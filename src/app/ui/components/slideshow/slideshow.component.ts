import { Component, Input, ElementRef } from '@angular/core';

// Usage:
// Father must call ViewChild to access inner functions like nextSlide and prevSlide
// @ViewChild(SlideshowComponent) private slideshow: SlideshowComponent;
// photos: Array<any>;
// 

@Component({
	selector: 'slideshow',
	templateUrl: './slideshow.component.html',
	styleUrls: ['./slideshow.component.scss']
})
export class SlideshowComponent {

	images_ : Array<string>;

	@Input() set images(value:any){
		this.images_ = value;
		if(!this.currentSlide && this.images_ && this.images_.length)
			this.setSlide(0);
	}

	currentSlide;

	constructor(private el: ElementRef) { }

	slides() {
		return (this.images_ && this.images_.length) || 0;
	}

	ngAfterViewInit(): void {
		if(!this.currentSlide && this.images_ && this.images_.length)
			this.setSlide(0);
	}

	nextSlide() {
		if(this.currentSlide === this.images_.length - 1)
			this.setSlide(0);
		else this.setSlide(this.currentSlide + 1);
	}

	prevSlide() {
		if(this.currentSlide === 0)
			this.setSlide(this.images_.length - 1);
		else this.setSlide(this.currentSlide - 1);
	}

	setSlide(index) {
		this.currentSlide = index;
		this.el.nativeElement.querySelector("div").style.backgroundImage = "url("+this.images_[index]+")";
	}

}
