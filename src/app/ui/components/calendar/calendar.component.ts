import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { PreferencesService } from '@core/services/preferences.service';


// Usage
// <calendar [min]="min" [max]="max" [(model)]="model"></calendar>
@Component({
  selector: 'calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

	@Input() min;
	@Input() max;
	@Input() datesInRange;
	@Input() model;
	@Output() modelChange = new EventEmitter<moment.Moment>();
	dateToShow: moment.Moment;
	invalidDates = [];
	weeks = [];
	month: any;

	constructor(private preferencesService: PreferencesService) { }

	ngOnInit() {
		// this.setDate(this.model || moment());
		this.setMonth(this.model || moment());
	}

	isMonthInRange(month) {
		if(this.min && month.endOf("month").isBefore(this.min))
			return false;

		if(this.max && month.startOf("month").isAfter(this.max))
			return false;

		return true;
	};

	rangeValidator(range) {
		if(this.min && range.start.isBefore(this.min))
			range.start = this.min;
		if(this.max && range.end.isAfter(this.max))
			range.end = this.max;

		if(this.datesInRange)
			return this.datesInRange({range});

		const dates = [];

		for(let current = range.start.clone(); current.isSameOrBefore(range.end); current.add(1, "d"))
			dates.push(current.clone());

		return dates;
	};

	isDateValid(date) {
		return this.invalidDates.indexOf(date) === -1;
	}

	getWeeks(){
		return this.weeks;
	}

	setDate(date) {
		const model_ = this.preferencesService.getWorkspacePreference("timeFormat") === "UTC" ?
			moment(date.clone().utc().format("YYYY-MM-DD HH:mm:ss")) :
			date.clone();
		this.model = model_.hour(this.model.hour()).minute(this.model.minute()).second(0).millisecond(0)

		this.modelChange.emit(this.model.clone())
	};

	setMonth(month) {
		this.month = month.clone();

		this.invalidDates = [],
		this.weeks = [];

		const datesInRange = this.rangeValidator({
			start: month.clone().startOf("month"),
			end: month.clone().endOf("month")
		});

		for(let current = this.month.clone().set({date: 1, day: 0}); current.isSameOrBefore(month, "month");) {
			const end = current.clone().add(1, "week"),
				days = [];

			for(; current.isBefore(end); current.add(1, "d")) {
				if(current.isSame(month, "month")) {
					const date = datesInRange.find(date => date.isSame(current, "d"));

					if(!date) {
						const currentClone = current.clone();

						this.invalidDates.push(currentClone);
						days.push(currentClone);
					}
					else days.push(date.clone());
				}
				else days.push(null);
			}

			this.weeks.push(days);
		}
	};

}
