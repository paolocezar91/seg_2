import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

@Component({
	selector: "seg-map-info",
	template: "<seg-status-bar></seg-status-bar><seg-map-scale></seg-map-scale>",
	styleUrls: ["./map-info.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapInfoComponent implements OnInit {

	constructor(private cdr: ChangeDetectorRef) { }

	ngOnInit() {
		this.cdr.detectChanges();
	}

}
