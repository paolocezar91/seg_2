import { Component, OnInit, ElementRef, Renderer2, ChangeDetectionStrategy } from "@angular/core";
import { TimestampPipe } from "@ui/pipes/timestamp.pipe";
import { CoordinatePipe } from "@ui/pipes/coordinate.pipe";
import { PreferencesService } from "@core/services/preferences.service";
import { MapService } from "@core/services/map/map.service";
import * as utils from "libs/seg-utils";
import * as moment from "moment";

@Component({
	selector: "seg-status-bar",
	templateUrl: "./status-bar.component.html",
	styleUrls: ["./status-bar.component.scss"],
	providers: [CoordinatePipe, TimestampPipe],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatusBarComponent implements OnInit {

	coordsDiv;
	latSpan;
	lonSpan;
	timeDiv;

	constructor(
		private el: ElementRef,
		private mapService: MapService,
		private preferences: PreferencesService,
		private coordinatePipe: CoordinatePipe,
		private timestampPipe: TimestampPipe,
		private renderer: Renderer2
	) { }

	ngOnInit() {
		this.mapService.onMapReady(() => {
			let coordsVisible;

			this.coordsDiv = this.el.nativeElement.querySelector("div#coords"),
			this.latSpan = this.coordsDiv.querySelector("span#lat"),
			this.lonSpan = this.coordsDiv.querySelector("span#lon"),
			this.timeDiv = this.el.nativeElement.querySelector("div#time");

			// set listener for mouse move
			this.renderer.listen(this.mapService.getMap().getTarget(), "mousemove", e => {
				if (!coordsVisible)
					this.coordsDiv.style.display = coordsVisible = "block";

				[this.lonSpan.innerHTML, this.latSpan.innerHTML] = this.mapService.getMap().getCoordinateFromPixel(
					[e.clientX, e.clientY]
				).map((val, isLat) => this.coordinatePipe.transform(
						isLat ? utils.modulo(val + 90, 180) - 90 : utils.modulo(val + 180, 360) - 180,
						this.preferences.getWorkspacePreference("coordinateFormat"), isLat ? "lat" : "lon"
					).replace("°", "&deg;"));
			});

			setInterval(() => this.timeDiv.innerText = this.timestampPipe.transform(moment(), this.preferences.getWorkspacePreference("timeFormat"), this.preferences.getWorkspacePreference("timezone")), 1000);
   		});
	}
}
