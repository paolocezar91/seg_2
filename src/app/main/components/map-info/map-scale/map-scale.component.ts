import { Input, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { MapService } from "@core/services/map/map.service";
import { PreferencesService } from "@core/services/preferences.service";
import { Component, OnInit } from "@angular/core";

@Component({
	selector: "seg-map-scale",
	templateUrl: "./map-scale.component.html",
	styleUrls: ["./map-scale.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapScaleComponent implements OnInit {

	scales: {value: number, label: string}[] = [
		// 125000000,
		64000000,
		32000000,
		16000000,
		8000000,
		6000000,
		4000000,
		2000000,
		1000000,
		500000,
		250000,
		125000,
		64000,
		32000
	].map(value => ({value, label: "1 : " + value}));

	private _scale;
	@Input("scale")
	set scale(scale) {
		this._scale = scale;
		this.changeScale(scale);
	}
	get scale() {
		return this._scale;
	}

	constructor(
		private preferencesService: PreferencesService,
		private mapService: MapService,
		private cdr: ChangeDetectorRef
	) { }

	ngOnInit() {
		this.mapService.onMapReady(() => {
			this.preferencesService.onWorkspacePreferenceChange("distanceUnits", (val) => this.mapService.mapScaleLine.setUnits(val));
			// self.scaleLineUnits = updateFormat(preferencesService.getWorkspacePreference("distanceUnits"));

			this.mapService.mapScaleLine.setUnits(this.preferencesService.getWorkspacePreference("distanceUnits"));

			this.scale = this.mapService.getMap().getScale();

			this.mapService.getMap().onScaleChange(scale => {
				this.scale = scale;
			});
			this.cdr.detectChanges();
   		});
	}

	changeScale(value) {
		this.mapService.getMap().setScale(value);
		this.cdr.detectChanges();
	}

	// this should be created as a variable or else we lose the "this" reference
	clickCallback = value => {
		// parse value to integer
		value = +value;

		if (value > 0) {
			if ((this.scales.findIndex( (scale) => scale.value === value) === -1) && (value !== "") && (value !== "0") && (typeof value !== "undefined")) {
				this.scales = this.scales.concat({value: value, label: "1 : " + value}).sort( (a, b) => {
					if (a.value > b.value) return -1;
					if (a.value < b.value) return 1;
					return 0;
				});
			}

			this._scale = value;
			this.changeScale(value);
		}
	}

	// this should be created as a variable or else we lose the "this" reference
	removeCallback = option => {
		const key = this.scales.findIndex( (scale) => scale.value === option.value);

		if (key !== -1) {
			this.scales = this.scales.filter(scale => this.scales[key].value !== scale.value);
		}
	}
}
