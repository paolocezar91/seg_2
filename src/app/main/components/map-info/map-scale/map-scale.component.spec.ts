import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapScaleComponent } from './map-scale.component';

describe('MapScaleComponent', () => {
  let component: MapScaleComponent;
  let fixture: ComponentFixture<MapScaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapScaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapScaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
