import { AlertsService } from '@core/services/alerts.service';
import { Component, OnInit } from "@angular/core";
import { ShortcutsService } from "@core/services/shortcuts.service";
import { CleanablesService } from "@core/services/cleanables.service";

@Component({
	selector: "seg-top-right-menu",
	templateUrl: "./top-right-menu.component.html",
	styleUrls: ["./top-right-menu.component.scss"]
})
export class TopRightMenuComponent {

	alertMenuWasOpen;

	constructor(
		private shortcutsService: ShortcutsService,
		private cleanablesService: CleanablesService,
		private alertsService: AlertsService
	) { 
	}

	hasCleanables() {
		return !!Object.keys(this.cleanablesService.cleanables).length;
	}

	hasShortcuts() {
		return this.shortcutsService.hasShortcuts() || this.shortcutsService.filterActive();
	}

	//	this.getApps = () => appsService.apps;

	//	this.openApp = app => {
	//		appsService.open(app.name);

	//		this.appMenuOpen = false;
	//	};

	//	this.closeAllApps = () => appsService.closeAll();
	
	markAlertsAsRead = () => this.alertsService.markAlertsAsRead();

	unMarkFreshAlerts = () => this.alertsService.unMarkFreshAlerts();

	getNotReadAlerts = () => this.alertsService.getNotReadAlerts().length;

	getAlerts = () => this.alertsService.getAlerts().length;

	checkCleanAlerts = () => {
		if (this.alertMenuWasOpen) {
			this.markAlertsAsRead();
			this.unMarkFreshAlerts();
		}
	};
	
}
