import { AlertsService } from '@core/services/alerts.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'alert',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss']
})
export class AlertsComponent implements OnInit {


  @Input() model;
	@Output() modelChange = new EventEmitter<boolean>();

  constructor(
    private alertsService: AlertsService
  ) { }

  ngOnInit() {
		
  }

  getAlerts()
	{
		return this.alertsService.getAlerts();
	};

	close()
	{
		this.model = !this.model;
		this.modelChange.emit(this.model);
	};

	goToAlert(item)
	{
		this.alertsService.goToAlert(item);
	};

	remove(item)
	{
		this.alertsService.remove(item);
	};

}
