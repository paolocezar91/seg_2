import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreshAlertComponent } from './fresh-alert.component';

describe('FreshAlertComponent', () => {
  let component: FreshAlertComponent;
  let fixture: ComponentFixture<FreshAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreshAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreshAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
