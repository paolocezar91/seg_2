import { AlertsService } from '@core/services/alerts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fresh-alert',
  templateUrl: './fresh-alert.component.html',
  styleUrls: ['./fresh-alert.component.scss']
})
export class FreshAlertComponent implements OnInit {

  constructor(
    private alertsService: AlertsService
  ) { }

  ngOnInit() {
  }

  getFreshAlerts = function()
	{
		return this.alertsService.getFreshAlerts();
	};

	removeWithAnimation = function(item)
	{
		this.alertsService.removeWithAnimation(item);
	};

	goToAlert = function(item)
	{
		this.alertsService.goToAlert(item);
	};

	cleanFreshState = function(item)
	{
		this.alertsService.cleanFreshState(item);
	};

}