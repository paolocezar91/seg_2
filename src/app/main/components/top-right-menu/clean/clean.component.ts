import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { CleanablesService } from "@core/services/cleanables.service";

@Component({
	selector: "clean",
	templateUrl: "./clean.component.html",
	styleUrls: ["./clean.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CleanComponent {

  	constructor(
  		private cleanablesService: CleanablesService,
  		private cdr: ChangeDetectorRef
  	) { }

	getCleanables() {
		return this.cleanablesService.cleanables;
	}

	cleanAll() {
		Object.keys(this.getCleanables())
			.forEach(label => this.getCleanables()[label] && this.getCleanables()[label]());
		this.cdr.detectChanges();
	}

}
