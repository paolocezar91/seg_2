import { SelectionService } from "@core/services/interactions/selection.service";
import { FavouritesService } from "@core/services/favourites.service";
import { ShortcutsService } from "@core/services/shortcuts.service";
import { Component } from "@angular/core";

@Component({
	selector: "shortcuts",
	templateUrl: "./shortcuts.component.html",
	styleUrls: ["./shortcuts.component.scss"]
})
export class ShortcutsComponent {

	constructor(
		private shortcutsService: ShortcutsService,
		private favouritesService: FavouritesService,
		private selectionService: SelectionService
	) { }

	getShortcuts = () => this.shortcutsService.getShortcuts();

	// close = () => this.topRightMenu.shortcutsOpen = false;

	getSelectedFeatures = () => this.selectionService.getSelectedFeatures();

	// openFavourites = () => this.favouritesService.open();

	filterOut = () => this.shortcutsService.filterOut();

	filterActive = () => this.shortcutsService.filterActive();
}
