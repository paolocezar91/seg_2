import { OperationService } from '@core/services/operation.service';
import { PreferencesService } from '@core/services/preferences.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'preferences-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.scss']
})
export class UnitsComponent implements OnInit {

  appsModule;
  speed;
  time;
  coordinates;
  distance;
  area;
  timeFormat;
  timezone;
  positionTimestampFormat;

  constructor(
    private preferencesService: PreferencesService,
    private operationService: OperationService,
  ) { }

  ngOnInit() {

    let prefService = this.preferencesService
    this.appsModule = this.operationService.getCurrentOperation().modules.find(mod => mod.name === "Apps");
	  if(this.appsModule && this.appsModule.hasAlerts){
      this.speed = {
        get selected() {
          return prefService.getWorkspacePreference("speedUnits") || "m/s";
        },
        set selected(val) {
          prefService.setWorkspacePreference("speedUnits", val);
        },
        options: [{
          label: "Meters per second",
          value: "m/s"
        },{
          label: "Knots",
          value: "knots"
        }]
		  };

      this.time = {
        get selected() {
          return prefService.getWorkspacePreference("timeUnits") || "s";
        },
        set selected(val) {
          prefService.setWorkspacePreference("timeUnits", val);
        },
        options: [{
          label: "Seconds",
          value: "sec"
        }, {
          label: "Minutes",
          value: "min"
        }, {
          label: "Hours",
          value: "h"
        }]
      };
    }
    
    this.coordinates = {
      get selected() {
        return prefService.getWorkspacePreference("coordinateFormat");
      },
      set selected(val) {
        prefService.setWorkspacePreference("coordinateFormat", val);
      },
      options: [{
        label: "DMS",
        format: "DMS"
      },
      {
        label: "DM (DD MM:mm)",
        format: "DM"
      },
      {
        label: "Decimal (DDD.ddddd)",
        format: "Decimal"
      }]
    };
  
    this.distance = {
      get selected() {
        return prefService.getWorkspacePreference("distanceUnits");
      },
      set selected(val) {
        prefService.setWorkspacePreference("distanceUnits", val);
      },
      options: [{
        label: "Nautical miles",
        value: "nm"
      },
      {
        label: "Meters",
        value: "m"
      },{
        label: "Kilometers",
        value: "km"
      }]
    };
  
    this.area = {
      get selected() {
        return prefService.getWorkspacePreference("areaUnits");
      },
      set selected(val) {
        prefService.setWorkspacePreference("areaUnits", val);
      },
      options: [{
        label: "Square Nautical miles",
        value: "nm2"
      },{
        label: "Square Meters",
        value: "m2"
      },{
        label: "Square Kilometers",
        value: "km2"
      }]
    };
  
    this.timeFormat = {
      get selected() {
        return prefService.getWorkspacePreference("timeFormat");
      },
      set selected(val) {
        prefService.setWorkspacePreference("timeFormat", val);
      },
      options: ["UTC", "Local"]
    };
  
    this.timezone = {
      get selected() {
        return prefService.getWorkspacePreference("timezone");
      },
      set selected(val) {
        prefService.setWorkspacePreference("timezone", val);
      },
      options: (function() {
        const timeZoneOptions = [];
  
        for(let i=-12; i<=12; i++)
          timeZoneOptions.push({
            label: "UTC"+(i>=0?"+":"-")+(Math.abs(i)<10?"0":"")+Math.abs(i)+":00",
            offset: i
          });
  
        return timeZoneOptions;
      })()
    };
  
    this.positionTimestampFormat = {
      get selected() {
        return prefService.getWorkspacePreference("positionTimestampFormat");
      },
      set selected(val) {
        prefService.setWorkspacePreference("positionTimestampFormat", val);
      },
      options: ["Absolute timestamp", "Time relative"]
    };

  }

}