import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PreferencesPanelService } from '@main/services/preferences-panel.service';
import { MapService } from '@core/services/map/map.service';
import { InfoService } from '@core/services/info.service';
import { OperationService } from '@core/services/operation.service';
import { UserService } from '@core/services/user.service';
import { LogService } from '@core/services/log.service';
import { LocationService } from '@core/services/location.service';
import { PreferencesService } from '@core/services/preferences.service';
import * as utils from "libs/seg-utils";
import { hasLifecycleHook } from '@angular/compiler/src/lifecycle_reflector';

@Component({
  selector: 'preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss']
})
export class PreferencesComponent implements OnInit {

  constructor(
    private preferencesService: PreferencesService,
    private locationService: LocationService,
    private preferencesPanelService: PreferencesPanelService,
    private logService: LogService,
    private userService: UserService,
    private operationService: OperationService,
    private infoService: InfoService,
    private mapService: MapService
  ) { }

  @Input() model;
  @Output() modelChange = new EventEmitter<boolean>();

  center;
  operations;
  tabs;
	userSpecifiedCenter_;

  ngOnInit() {
    new Promise(((resolve) => {
			const center = this.preferencesService.getWorkspacePreference("center");

			if(center)
				resolve(center);
			else this.locationService.getCurrentLocation().subscribe(resolve, () => {
				resolve([0,0]);
			});
		})).then((currentLocation) => {
			this.center = utils.lonLatToDMS(currentLocation);
		}, e => this.logService.error("ERROR_PREFERENCES_ON_INIT", e.error + ": " + e.result));

		this.operations = this.getOperations();

		this.tabs = this.preferencesPanelService.getTabs();
		
    this.userSpecifiedCenter_ = {
      options: [
        "Current geographical position",
        "User specified"
      ].map((label, index) => ({index, label}))
		}
		
	}
	
	get userSpecifiedCenter(){
		return this.preferencesService.getWorkspacePreference("userSpecifiedCenter") ? "1" : "0";
	}

	set userSpecifiedCenter(val){
		this.preferencesService.setWorkspacePreference("userSpecifiedCenter", val == "1" ? true : false);
	}

  get currentOperation(){
    let vals: any[] = Object.values(this.userService.getCurrentUser().operations);
    return vals.find(operation => operation === this.operationService.getCurrentOperation().project);
	};
	
	set currentOperation(val){
		//Save preferences to prefernces Service
		this.preferencesService.setSessionPreference("latestOperation", val.alias);
		sessionStorage.setItem("operation", val.alias);
		this.preferencesService.setGlobalPreference("operation", val.alias);
		location.reload()
  };
  
  // Filters duplicate alias
	getOperations = () => this.userService.getCurrentUser().operationsInfo.map(operation => 
			({
				value:operation.code,
				label:operation.description
			})
	);
  
	get mapOverview() {
			return this.mapService.mapOverview.isVisible();
	};
	set mapOverview(val) {
		this.mapService.mapOverview.toggle(val);
	};

	get mapGraticule(){
		return this.mapService.graticule.isVisible();
	};
	
	set mapGraticule(val){
		this.mapService.graticule.toggle(val);
  };
  
/*
	userSpecifiedCenter = function(val) {
		//get
		if(!arguments.length)
			return +!!this.preferencesService.getWorkspacePreference("userSpecifiedCenter");
		//set
		this.preferencesService.setWorkspacePreference("userSpecifiedCenter", !!val);
  };
  
  userSpecifiedCenter.options = [
		"Current geographical position",
		"User specified"
	].map((label, index) => ({index, label}));
*/

	get scale() {
		return this.preferencesService.getWorkspacePreference("scale");
	};

	set scale(val) {
		this.preferencesService.setWorkspacePreference("scale", val);
	};

	setToCurrent() {
		this.scale = this.mapService.getMap().getScale();
		this.applyValues(this.mapService.getMap().getCenter());
	};

	applyValues(val) {
		if(typeof val === "undefined")
			val = utils.dmsToDecimal(this.center);

		this.preferencesService.setWorkspacePreference("center", val);
		this.center = utils.lonLatToDMS(val);
	};

	close()	{
    this.model = false;
    this.modelChange.emit(false);
  };
  
  getUserInfo() {
		return this.userService.getCurrentUser();
	};

	getSegInfo() {
		return this.infoService.getInfo();
	};

	getBrowserInfo()
	{
		const nAgt = navigator.userAgent;
		// nVer = navigator.appVersion;

		let browserName  = navigator.appName,
			fullVersion  = ""+parseFloat(navigator.appVersion),
			majorVersion = parseInt(navigator.appVersion,10),
			nameOffset, verOffset, ix;

		// In Opera, the true version is after "Opera" or after "Version"
		if ((verOffset=nAgt.indexOf("Opera"))!==-1) {
			browserName = "Opera";
			fullVersion = nAgt.substring(verOffset+6);
			if ((verOffset=nAgt.indexOf("Version"))!==-1)
				fullVersion = nAgt.substring(verOffset+8);
		}
		// In MSIE, the true version is after "MSIE" in userAgent
		else if ((verOffset=nAgt.indexOf("MSIE"))!==-1) {
			browserName = "Microsoft Internet Explorer";
			fullVersion = nAgt.substring(verOffset+5);
		}
		// In MSIE11, the true version is after "rv" in userAgent
		else if ((verOffset=nAgt.indexOf("Trident"))!==-1) {
			browserName = "Microsoft Internet Explorer";
			if ((verOffset=nAgt.indexOf("rv:"))!==-1) {
				fullVersion = nAgt.substring(verOffset+3,verOffset+7);
			}
		}
		// In Chrome, the true version is after "Chrome"
		else if ((verOffset=nAgt.indexOf("Chrome"))!==-1) {
			browserName = "Google Chrome";
			fullVersion = nAgt.substring(verOffset+7);
		}
		// In Safari, the true version is after "Safari" or after "Version"
		else if ((verOffset=nAgt.indexOf("Safari"))!==-1) {
			browserName = "Safari";
			fullVersion = nAgt.substring(verOffset+7);
			if ((verOffset=nAgt.indexOf("Version"))!==-1)
				fullVersion = nAgt.substring(verOffset+8);
		}
		// In Firefox, the true version is after "Firefox"
		else if ((verOffset=nAgt.indexOf("Firefox"))!==-1) {
			browserName = "Firefox";
			fullVersion = nAgt.substring(verOffset+8);

		}
		// In most other browsers, "name/version" is at the end of userAgent
		else if ( (nameOffset=nAgt.lastIndexOf(" ")+1) < (verOffset=nAgt.lastIndexOf("/")) )
		{
			browserName = nAgt.substring(nameOffset,verOffset);
			fullVersion = nAgt.substring(verOffset+1);
			if (browserName.toLowerCase()===browserName.toUpperCase()) {
				browserName = navigator.appName;
			}
		}
		// trim the fullVersion string at semicolon/space if present
		if ((ix=fullVersion.indexOf(";"))!==-1)
			fullVersion=fullVersion.substring(0,ix);
		if ((ix=fullVersion.indexOf(" "))!==-1)
			fullVersion=fullVersion.substring(0,ix);

		majorVersion = parseInt(""+fullVersion,10);
		if (isNaN(majorVersion)) {
			fullVersion  = ""+parseFloat(navigator.appVersion);
			majorVersion = parseInt(navigator.appVersion,10);
		}

		return browserName + " Version " + fullVersion;
	};

}