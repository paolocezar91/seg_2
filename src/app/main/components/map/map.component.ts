import { Component, OnInit, Input } from "@angular/core";
import { Map } from "ol";
import { Observable, forkJoin, of, throwError, zip, merge, combineLatest } from "rxjs";
import { mergeMap, concatMap, catchError, take, map, first, finalize, takeWhile, switchMap } from "rxjs/operators";
import * as moment from "moment";

// CORE
import { ConfigService } from "@core/services/config.service";
import { FavouritesService } from "@core/services/favourites.service";
import { InfoService } from "@core/services/info.service";
import { LayerService } from "@core/services/layer/layer.service";
import { LocationService } from "@core/services/location.service";
import { LogService } from "@core/services/log.service";
import { MapService } from "@core/services/map/map.service";
import { OperationService } from "@core/services/operation.service";
import { PlacemarkService } from "@core/services/interactions/placemark/placemark.service";
import { PreferencesService } from "@core/services/preferences.service";
import { RequestService } from "@core/services/request/request.service";
import { ScriptsService } from "@core/services/request/scripts.service";
import { SelectionService } from "@core/services/interactions/selection.service";
import { UserService } from "@core/services/user.service";
// MAIN
import { ModalService } from "@main/services/modal/modal.service";
import { TablesService } from "@main/services/ttt/tables.service";

declare const extensibility: any;

@Component({
	selector: "seg-map",
	templateUrl: "./map.component.html",
	styleUrls: ["./map.component.scss"]
})
export class MapComponent implements OnInit {

	@Input("operation-config") set operationConfig(value) {
		this.operationConfig_ = value;
	}
	operationConfig_;
	@Input("layer-configs") set layerConfigs(value) {
		this.layerConfigs_ = value;
	}
	layerConfigs_;
	loadedModules;
	layersInLoadedModulesLength: number;
	mapInstance: Map;
	baseLayerGroup: any;
	selectedBaseLayer: any;

	constructor(
		private configService: ConfigService,
		private mapService: MapService,
		private modalService: ModalService,
		private logService: LogService,
		private favouritesService: FavouritesService,
		private placemarkService: PlacemarkService,
		private layerService: LayerService,
		private selectionService: SelectionService,
		private preferencesService: PreferencesService,
	) {
	}

	ngOnInit() {
		this.loadedModules = this.operationConfig_.modules;
		this.layersInLoadedModulesLength = this.operationConfig_.modules.reduce((sum, {layers}) => sum + layers.length, 0);

		this.createMapAndSetUpModules(this.layerConfigs_)
			.pipe(take(this.layersInLoadedModulesLength))
			.subscribe((segLayer) => {
				this.mapService.getMap().addLayers(segLayer);
			},
			e => of(e),
			() => {
				this.mapService.orderMap(true);
				this.mapService.applyMapPreferences();
				this.favouritesService.init();
				this.placemarkService.init();
				document.getElementById("app-loading").style.display = "none";

				forkJoin(...this.loadedModules.map(({init, onLoad, name}) => {
					// initializing modules for the user
					return new Promise((resolve, reject) => {
						try	{
							resolve(init());
						} catch (e) {
							this.logService.error(name, "failed", e);
							reject (e);
						}
					});
				})).subscribe(() => {
					// this.favouritesService.buildFavAndSessionMenus();
					const modal = this.modalService.get("Loading modules");
					modal.setContent("All modules were <span class=\"retrieved-success\">loaded</span>");
					this.modalService.closeTimeout(modal.title);
				});
			});

	}

	createMapAndSetUpModules(layerConfigs) {
		return this.createMap(layerConfigs)
			.pipe(
				switchMap((_) => {
					return this.setLayersFromModules()
						.pipe(
							take(this.loadedModules.length),
							mergeMap(val => val)
						);
				}),
				take(this.loadedModules.reduce((sum, {layers}) => sum + layers.length, 0))
			);
	}

	/**
	 * Receives the base layer configs and creates a map with preferential projection, scale, center.
	 * @param {[type]} enhancedLayerConfigs [description]
	 */
	createMap(enhancedLayerConfigs) {
		return Observable.create(obs => {
			this.baseLayerGroup = this.layerService.create({
				id: "base_layers",
				name: "Base Layers",
				layers: []
			});

			this.selectedBaseLayer = this.preferencesService.getMapPreference("baseLayer") || // Looks for preferences
				this.operationConfig_.default_base_layer || // Then if there is a default_base_layers on operation"s config
				this.configService.getValue("default_base_layer"); // Then the default_base_layer on SEG configuration

			enhancedLayerConfigs.forEach(enhancedLayerConfig => {
				const enhancedLayer = this.layerService.create(enhancedLayerConfig);
				enhancedLayer.setVisible(this.selectedBaseLayer === enhancedLayer.get("id"));
				this.baseLayerGroup.addLayers(enhancedLayer);
			});

			// to see declaration of extensibility function, check root build-extensibility.js;
			// this loads extensibility and sets configuration of roles of the user for each available user operation
			this.mapService.getMapOptions(this.configService.CONFIG).then(({center, projection, scale}) => {
				this.mapInstance = this.mapService.createMap({
					layers: [
						this.baseLayerGroup
					],
					projection,
					scale,
					center,
					// target: "map"
				});

				this.mapInstance.setTarget("#map");

				// Register clicking in the map with selection service
				this.selectionService.registerClickToMap();
				obs.next();
			});
		});
	}

	/**
	 * From the modules that were loaded from extensibility extracts the layers configuration, and creates them to be added to the map
	 */
	setLayersFromModules() {
		const mergedLayerConfigs: Observable<any>[] = this.loadedModules.map(module => {
			if (Array.isArray(module.layers)) {
				return merge(
					...module.layers.map(layer => this.layerService.enhance(layer)
						.pipe(map((segLayer) => segLayer && this.layerService.create(segLayer)))
					)
				).pipe(
					catchError(e => of(e, "ERROR_BIND_MODULES_TO_MAP"))
				);

			}
		});
		return merge(mergedLayerConfigs);
	}

}
