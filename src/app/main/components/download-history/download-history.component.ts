import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { LogService } from "@core/services/log.service";
import { DownloadHistoryService } from "@main/services/download-history.service";
import * as moment from "moment";

@Component({
	selector: "seg-download-history",
	templateUrl: "./download-history.component.html",
	styleUrls: ["./download-history.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DownloadHistoryComponent implements OnInit {

	open = false;
	downloads = [];
	currentIndex = 0;
	currentItem;
	loading = false;

	constructor(
		private logService: LogService,
		private downloadHistoryService: DownloadHistoryService,
		private cdr: ChangeDetectorRef
	) { }

	ngOnInit() {
		this.updateDownloads();
		this.currentItem = this.downloads[0];
		this.cdr.detectChanges();
	}

	toggleDownloadHistory() {
		this.updateDownloads();
		this.open = !this.open;
		// this.downloads = this.downloadHistoryService.get()?this.downloadHistoryService.get():{};
	}

	repeatDownload = (item) => {
		if (typeof item.repeatDownload !== "undefined") {
			this.loading = true;
			const promise = item.repeatDownload();
			Promise.all([promise]).then(() => {
				setTimeout(() => {
					this.loading = false;
				}, 3000);
			}, e => this.logService.error("ERROR_REPEAT_DOWNLOAD", e.error + ": " + e.result));
		}
	}

	nextItem() {
		this.updateDownloads();
		if ((this.currentIndex + 1) >= this.downloads.length)
			return;
		this.currentIndex += 1;
	}

	previousItem =  () => {
		this.updateDownloads();
		if ((this.currentIndex - 1) < 0)
			return;
		this.currentIndex -= 1;
	}

	hasDownloads() {
		const data = this.downloadHistoryService.get() ? this.downloadHistoryService.get() : {};
		const downloads = Object.keys(data).map((k) => {
			return data[k];
		});

		if (downloads.length > 0) {
			return true;
		} else {
			return false;
		}
	}

	updateDownloads = () => {
		const downloads = this.downloadHistoryService.get() ? this.downloadHistoryService.get() : {};
		this.downloads = Object.keys(downloads).map((k) => {
			return downloads[k];
		});
		this.orderDownloads();
	}

	orderDownloads = () => {
		this.downloads.sort((a, b) => {
			const keyA = moment(a.date),
				keyB = moment(b.date);
			// Compare the 2 dates

			if (keyA > keyB) return -1;
			if (keyA < keyB) return 1;
			return 0;
		});
	}

}
