import { Component, OnInit, ElementRef, ViewChild, ViewChildren, QueryList, AfterViewInit, OnDestroy } from "@angular/core";
import { TablesService } from "@main/services/ttt/tables.service";
import { TabComponent } from "@ui/components/tab-group/tab.component";
import { TabGroupComponent } from "@ui/components/tab-group/tab-group.component";
import { difference } from "lodash";

@Component({
  selector: "seg-ttt",
  templateUrl: "./ttt.component.html",
  styleUrls: ["./ttt.component.scss"]
})
export class TttComponent implements OnInit, OnDestroy {

	@ViewChild(TabGroupComponent) tabGroupComponent: TabGroupComponent;
	@ViewChild("gridTemplate") gridTemplate;
	registeredTables = [];
	openTables = [];
	openTableTabs = [];
	timeline = {
		isActive: () => false
	};
	isOpen = false;
	openTab = null;
	tools;
	tablePicker = false;
	toolsOpen = false;

	constructor(
		private tablesService: TablesService,
		private el: ElementRef
	) { }

	ngOnInit() {
		// set listener to open gallery tab
		// galleryService.onGalleryOpen(() => this.open("gallery"));

		// set listener to open tables tab
		this.tablesService.onTableOpen(() => this.open("tables"));

		// timeline.onOpen(() => {
		// 	this.isOpen = true;
		// 	this.open("timeline");
		// 	this.toolsOpen = true;
		// });

		this.tablesService.registeredTables.subscribe((registeredTables: any[]) => this.registeredTables = registeredTables);
		// we need to know what tables to show in the open menu
		this.tablesService.openTables.subscribe((newOpenTables: any[]) => {

			// This checks if subscription was triggered by the SEG interface closing a table (seg.ui.ttt.tables.close)
			const diff = difference(this.openTables, newOpenTables);
			if (diff.length)
				this.openTableTabs.filter(openTab => diff.some(d => openTab.label === d.label))
					.forEach(openTab => this.tabGroupComponent.closeTab(openTab));

			this.openTables = newOpenTables;

			// this.tabGroupComponent.closeTab;

			if (this.tabGroupComponent && this.openTables.length)
				this.createScheduledTabs();

			if (!this.openTables.length) {
				this.isOpen = false;
				this.openTab = null;
			}

			// Need to check previous openTables and see if they were closed by others (clenables for example)
		});

		// XXX
		// this.timeline = timeline;
	}

	ngOnDestroy() {
		this.tablesService.registeredTables.unsubscribe();
		this.tablesService.openTables.unsubscribe();
	}

	createScheduledTabs() {
		this.openTableTabs = [].concat(this.tabGroupComponent.dynamicTabs);

		if (/*this.isOpen && */this.openTables.length) {
			this.openTables.forEach(table => {
				if (!this.openTableTabs.find(ott => ott.label === table.label)) {
					this.tabGroupComponent.createTab(table.label, this.gridTemplate, table, false, true);
					this.openTableTabs = [].concat(this.tabGroupComponent.dynamicTabs);
				}
			});
		} else {
			this.openTab = null;
			this.isOpen = false;
		}
	}

	openTable(table) {
		this.open("tables");
		this.tablesService.open(Object.assign({}, table));
	}

	closeTable(tabComponent: TabComponent) {
		this.tablesService.close(tabComponent.label);
		if (!this.openTables.length) {
			this.openTab = null;
			this.isOpen = false;
		}
	}

	isTableOpen = (table) => {
		return this.openTables.indexOf(table) !== -1;
	}


	toggle() {
		this.isOpen = !this.isOpen;

		// if (this.isOpen && this.openTables.length) {
		// 	this.openTables.filter(table).forEach(table => {
		// 		this.tabGroupComponent.openTab(table.label, this.gridTemplate, table);
		// 	});
		// }
	}


	open(tab) {
		this.openTab = tab;
		this.isOpen = true;

		if (tab === "tables")
			this.createScheduledTabs();
	}
}
