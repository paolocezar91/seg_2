import { Component, OnInit, ElementRef, Renderer2, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { ModalService } from "@main/services/modal/modal.service";
import { LogService } from "@core/services/log.service";
import { RequestService } from "@core/services/request/request.service";
import { MapService } from "@core/services/map/map.service";

import { transformExtent } from "ol/proj.js";
import * as $o from "libs/seg-object";
import GeoJSON from "ol/format/GeoJSON";

@Component({
	selector: "seg-layer-info",
	templateUrl: "./layer-info.component.html",
	styleUrls: ["./layer-info.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayerInfoComponent implements OnInit {

	map;
	title = "";
	templateAndScopes = [];
	open = false;

	constructor(
		private mapService: MapService,
		private requestService: RequestService,
		private logService: LogService,
		private modalService: ModalService,
		private el: ElementRef,
		private renderer: Renderer2,
		private cdr: ChangeDetectorRef
	) {}

	ngOnInit() {
		this.mapService.onMapReady(() => {
			this.map = this.mapService.getMap();
			this.map.ol_.on("click", e => {
				// Extract Layer Names For Request
				const position = e.pixel,
					image_size = this.map.ol_.getSize(),
					extent = transformExtent(this.map.getView().calculateExtent(image_size), this.map.getProjection(), "EPSG:4326"),
					RequestWMSLayersAtPixel = [];

				const modal = {};

				this.map.ol_.forEachLayerAtPixel(position, layer => {
					const selectable = layer.get("selectable");
					if (selectable) {
						modal["areaInfo_" + layer.get("name")] = this.modalService.openModal("areaInfo_" + layer.get("name"), "Requesting " + layer.get("name") + " info...");
						const params = Object.assign({
							service: "WMS",
							request: "GetFeatureInfo",
							exceptions: "application/vnd.ogc.se_xml",
							info_format: layer.get("selectable") === "html" ? "text/html" : "application/json",
							query_layers: layer.getSource().getParams().LAYERS,
							feature_count: 50,
							LAYERS: layer.getSource().getParams().LAYERS,
							VERSION: "1.1.1",
							width: image_size[0],
							height: image_size[1],
							x: position[0],
							y: position[1],
							styles: "",
							bbox: extent.join(","),
							crs:  "EPSG:4326"
						}, selectable.additionalParams || {});

						if ($o.compareVersions(params.VERSION, "1.3.0") >= 0) {
							params.i = params.x;
							params.j = params.y;
							delete params.x;
							delete params.y;
						}

						const request = this.requestService.get(layer.getSource().getUrl(), {params}).toPromise()
							.then((res: any) => {
								if (layer.get("selectable") === "html") {
									const parser = new DOMParser();
									return {layer, scope: null, template: parser.parseFromString(res, "text/html").querySelector("body").innerHTML};
								}

								// if(res.features && res.features.length) {

								// 	const layerAs = layer.get("selectable").layerAs?layer.get("selectable").layerAs:"layer",
								// 		scope = {},
								// 		feature = (new GeoJSON()).readFeatures(res, {
								// 			featureProjection: mapService.getProjection()
								// 		})[0];

								// 	feature.setProperties({
								// 		timestamp: moment(feature.get("date")+" "+feature.get("time")),
								// 		coordinate: mapService.getMap().getCoordinateFromPixel(position) || []
								// 	});

								// 	scope.fields = layer.get("selectable").fields;
								// 	scope[layerAs] = feature;

								// 	return $compile(angular.element(`
								// 		<div>
								// 			<p><b>${layer.get("name")}</b></p>
								// 			<ul>
								// 				<li ng-repeat="field in fields">
								// 					<span><b>{{field.label}}: </b></span>
								// 					<include template="{{field.template}}" scope="${layerAs}"></include>
								// 				</li>
								// 			</ul>
								// 			<br>
								// 		</div>`))(scope);
								// } else {
								// 	this.logService.error("ERROR_LAYER_INFO_REQUEST_NO_FEATURES", layer.get("id"), res);
								// 	return;
								// }
							}, e => this.logService.error("ERROR_LAYER_INFO_REQUEST_WMS_LAYERS_AT_PIXEL", layer.get("id"), e));
							// .then(contents => {
							// 	return {layer, contents};
							// }, e => this.logService.error("ERROR_LAYER_INFO_REQUEST_WMS_LAYER_AT_PIXEL_THEN", "e", e));

						RequestWMSLayersAtPixel.push(request);
					}
				});

				Promise.all(RequestWMSLayersAtPixel).then(res => {
					if (res.length) {
						this.templateAndScopes = res.map(({template, scope}) => ({template, scope}));

						if (this.templateAndScopes.some(({template}) => template.trim().length)) {
							this.title = res.length === 1 ? res[0].layer.get("name") : "Layer Information";

							const leftSide = position[0] < window.innerWidth / 2;
							this.renderer.setStyle(this.el.nativeElement, "transform",  "translate(calc(" + position[0] + "px" + (leftSide ? " + " : " - 100% - ") + "20px), calc(" + position[1] + "px - 50%))");

							modal["areaInfo_" + res[0].layer.get("name")].setContent(res[0].layer.get("name") + " info request successful");
							this.modalService.closeTimeout(modal["areaInfo_" + res[0].layer.get("name")].title);
						} else {
							if (modal["areaInfo_" + res[0].layer.get("name")]) {
								modal["areaInfo_" + res[0].layer.get("name")].setContent("Area has no information");
								this.modalService.closeTimeout(modal["areaInfo_" + res[0].layer.get("name")].title);
							}
						}

						this.open = true;
						this.cdr.detectChanges();
					}
				});
			});
		});
	}

	close() {
		this.open = false;
	}

}
