import { Component, OnInit, OnDestroy } from "@angular/core";
import * as proj from "ol/proj";
import { LocationService } from "@core/services/location.service";
import { LogService } from "@core/services/log.service";
import { MapService } from "@core/services/map/map.service";
import { MapToolsService } from "@main/services/map-tools.service";
import { PreferencesService } from "@core/services/preferences.service";

@Component({
	selector: "seg-map-tools",
	templateUrl: "./map-tools.component.html",
	styleUrls: ["./map-tools.component.scss"]
})
export class MapToolsComponent implements OnInit, OnDestroy {

	magnifyAndCenterPreferenceListener: any;
	showMagnifyAndCenterButtons: any;
	toolbars: any = {draw: false, measure: false};

	constructor(
		private mapService: MapService,
		private locationService: LocationService,
		private preferencesService: PreferencesService,
		private logService: LogService,
		private mapToolsService: MapToolsService
	) {	}

	ngOnInit() {
		this.magnifyAndCenterPreferenceListener = this.preferencesService.onWorkspacePreferenceChange("show.magnifyAndCenterButtons", (value) => {
			this.showMagnifyAndCenterButtons = value;
		});

		this.showMagnifyAndCenterButtons = this.preferencesService.getWorkspacePreference("show.magnifyAndCenterButtons");
	}

	ngOnDestroy() {
		this.magnifyAndCenterPreferenceListener();
	}

	toggle(toolbar) {
		this.mapToolsService.toggle(toolbar);
		this.toolbars = this.mapToolsService.toolbars;
	}

	zoomIn() {
		this.mapService.getMap().zoomIn();
	}

	zoomOut() {
		this.mapService.getMap().zoomOut();
	}

	async center() {
		if (this.preferencesService.getWorkspacePreference("userSpecifiedCenter")) {
			const center = this.preferencesService.getWorkspacePreference("center");

			this.mapService.getMap().centerOn(proj.fromLonLat(center, this.mapService.getMap().getProjection()));
			this.mapService.getMap().setScale(this.preferencesService.getWorkspacePreference("scale"));
		} else {
			try {
				const {coords}: Position = await this.locationService.getCurrentLocation().toPromise();
				this.mapService.getMap().centerOn(proj.fromLonLat(coords, this.mapService.getMap().getProjection()));
			} catch (e) {
				this.logService.error("ERROR_CURRENT_LOCATION", e);
			}
		}

	}
}



