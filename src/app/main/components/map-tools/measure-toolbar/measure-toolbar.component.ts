import { Component } from "@angular/core";
import { MapToolsService } from "@main/services/map-tools.service";
import { MeasureService } from "@core/services/interactions/measure/measure.service";

@Component({
	selector: "seg-measure-toolbar",
	templateUrl: "./measure-toolbar.component.html",
	styleUrls: ["./measure-toolbar.component.scss"]
})
export class MeasureToolbarComponent {

	constructor(
		private mapToolsService: MapToolsService,
		private measureService: MeasureService
	) { }

	drawMeasureLine() {
		this.measureService.measureLine();
		this.close();
	}

	drawMeasureBearingCircle() {
		this.measureService.measureBearingCircle();
		this.close();
	}

	drawMeasureFixedRings() {
		this.measureService.measureFixedRing();
		this.close();
	}

	drawMeasureCompass() {
		this.measureService.measureCompass();
		this.close();
	}

	close() {
		this.mapToolsService.toggle("measure", false);
	}
}
