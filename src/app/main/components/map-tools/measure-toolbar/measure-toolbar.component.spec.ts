import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasureToolbarComponent } from './measure-toolbar.component';

describe('MeasureToolbarComponent', () => {
  let component: MeasureToolbarComponent;
  let fixture: ComponentFixture<MeasureToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasureToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasureToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
