import { CleanablesService } from './../../../../core/services/cleanables.service';
import { Component } from "@angular/core";

import { FavouritesService } from "@core/services/favourites.service";
import { LayerService } from "@core/services/layer/layer.service";

import * as condition from "ol/events/condition";
import Feature from "ol/Feature";
import * as interaction from "ol/interaction";
import { DrawService } from "@core/services/interactions/draw.service";
import { LogService } from "@core/services/log.service";
import { MapService } from "@core/services/map/map.service";
import { PlacemarkService } from "@core/services/interactions/placemark/placemark.service";
import { PreferencesService } from "@core/services/preferences.service";
import { SelectionService } from "@core/services/interactions/selection.service";
import { MapToolsService } from "@main/services/map-tools.service";

@Component({
	selector: "seg-draw-toolbar",
	templateUrl: "./draw-toolbar.component.html",
	styleUrls: ["./draw-toolbar.component.scss"]
})
export class DrawToolbarComponent {

	constructor(
		private drawService: DrawService,
		private placemarkService: PlacemarkService,
		private mapService: MapService,
		private layerService: LayerService,
		private logService: LogService,
		private favouritesService: FavouritesService,
		private preferencesService: PreferencesService,
		private selectionService: SelectionService,
		private mapToolsService: MapToolsService,
		private cleanablesService: CleanablesService
	) {}

	dragHandler = null;
	activeTool = {
		drawPlacemark: false,
		drawCircle: false,
		drawPolygon: false,
		drawBBOX: false,
		toggleDraggableMode: false,
		multiSelectionDraw: false
	};
	drawObservable = null;

	makeFeature(geometry) {
		return new Feature({geometry});
	}

	close() {
		this.mapToolsService.toggle("draw", false);
	}

	toggleAOILayer() {
		const aoiLayer = this.layerService.get("aoi");
		aoiLayer.ol_.setVisible(true);
		this.preferencesService.setMapPreference("layers." + aoiLayer.get("id") + ".visible", aoiLayer.ol_.getVisible());
	}

	drawBBOX() {
		this.toggleActiveClass("drawBBOX", true);
		if(this.drawObservable){
			this.toggleActiveClass("drawBBOX", false);
			this.drawObservable.cancel({error: "Cancelled by the user", result: ""});
			this.drawObservable = null;
		}else{
			this.drawObservable = this.drawService.drawRectangle();
			this.drawObservable.subscribe((geometry) => {
				const bbox = this.makeFeature(geometry);
	
				this.favouritesService.addFeaturesToGroup("Session", bbox, "BBOX");
				this.selectionService.select(bbox);
	
				this.toggleActiveClass("drawBBOX", false);
				this.toggleAOILayer();
				this.close();
			}, e => this.logService.error("ERROR_DRAW_RECTANGLE", e.error + ": " + e.result));
		}

	}

	drawPolygon() {
		this.toggleActiveClass("drawPolygon", true);
		if(this.drawObservable){
			this.toggleActiveClass("drawPolygon", false);
			this.drawObservable.cancel({error: "Cancelled by the user", result: ""});
			this.drawObservable = null;
		}else{
			this.drawObservable = this.drawService.drawPolygon({freehandCondition: condition.never});
			this.drawObservable.subscribe((geometry) => {
				const polygon = this.makeFeature(geometry);
	
				this.favouritesService.addFeaturesToGroup("Session", polygon, "Polygon");
				this.selectionService.select(polygon);
	
				this.toggleActiveClass("drawPolygon", false);
				this.toggleAOILayer();
				this.close();
			}, e => this.logService.error("ERROR_DRAW_POLYGON", e.error + ": " + e.result));		
		}
	}

	drawLinePolygon() {
		this.toggleActiveClass("drawLinePolygon", true);
		if(this.drawObservable){
			this.toggleActiveClass("drawLinePolygon", false);
			this.drawObservable.cancel({error: "Cancelled by the user", result: ""});
			this.drawObservable = null;
		}else{
			this.drawObservable = this.drawService.drawLine({})
			this.drawObservable.subscribe((geometry) => {
				geometry.setProperties({
					"type": "linePolygon"
				});
	
				const linePolygon = this.makeFeature(geometry);
	
				this.favouritesService.addFeaturesToGroup("Session", linePolygon, "LinePolygon");
				this.selectionService.select(linePolygon);
	
				this.toggleActiveClass("drawLinePolygon", false);
				this.toggleAOILayer();
	
				this.close();
			}, e => this.logService.error("ERROR_DRAW_LINE_POLYGON", e.error + ": " + e.result));
		}
	}

	drawPlacemark() {
		this.toggleActiveClass("drawPlacemark", true);
		if(this.drawObservable){
			this.toggleActiveClass("drawPlacemark", false);
			this.drawObservable.cancel({error: "Cancelled by the user", result: ""});
			this.drawObservable = null;
		}else{
			this.drawObservable = this.placemarkService.drawPlacemark();
			this.drawObservable.subscribe(geometry => {
				const newFeature = new Feature({
					geometry
				});
				// add feature to source
				newFeature.set("saved", false);
				console.log(this.layerService.get("placemarks"))
				this.layerService.get("placemarks").getSource().addFeature(newFeature);
				this.selectionService.select(newFeature);
	
				this.toggleActiveClass("drawPlacemark", false);
				this.close();
			}, e => this.logService.error("ERROR_DRAW_PLACEMARK", e.error + ": " + e.result));
		}
	}

	multiSelectionDraw() {
		this.toggleActiveClass("multiSelectionDraw", true);
		if(this.drawObservable){
			this.toggleActiveClass("multiSelectionDraw", false);
			this.drawObservable.cancel({error: "Cancelled by the user", result: ""});
			this.drawObservable = null;
		}else{
			this.drawObservable = this.drawService.drawPolygon();
			this.drawObservable.subscribe(geometry => {
				const featuresIntersectinGeometry = this.mapService.getMap().getFeaturesIntersectingGeometry(geometry).filter(feature => {
					// check visibility of layer of feature;
					const isLayerVisible = this.mapService.isLayerVisible(this.layerService.get(feature.get("seg_type")), this.mapService.getMap().getLayers());
	
					return isLayerVisible && !feature.get("hidden");
				});
				this.selectionService.select(featuresIntersectinGeometry);
	
				this.toggleActiveClass("multiSelectionDraw", false);
				this.toggleAOILayer();
				this.close();
			}, e => this.logService.error("ERROR_MULTI_SELECTION_DRAW_POLYGON", e.error + ": " + e.result));
		}
	}

	drawCircle() {
		this.toggleActiveClass("drawCircle", true);
		if(this.drawObservable){
			this.toggleActiveClass("drawCircle", false);
			this.drawObservable.cancel({error: "Cancelled by the user", result: ""});
			this.drawObservable = null;
		}else{
			this.drawObservable = this.drawService.drawCircle();
			this.drawObservable.subscribe((geometry) => {
				const circle = this.makeFeature(geometry);
	
				this.favouritesService.addFeaturesToGroup("Session", circle, "Circle");
				this.selectionService.select(circle);
	
				this.toggleActiveClass("drawCircle", false);
				this.toggleAOILayer();
				this.close();
			}, e => this.logService.error("ERROR_DRAW_DRAW_CIRCLE", e.error + ": " + e.result));
		}
	}

	removeDrag() {
		this.cleanablesService.remove("Deactivate Drag");///////////////////////////////////////////////////////////////cleanables doesn"t exist yet
		this.mapService.getMap().ol_.removeInteraction(this.dragHandler);

		this.dragHandler = null;
		this.toggleActiveClass("toggleDraggableMode", false);
	}

	toggleDraggableMode() {
		if (!this.dragHandler) {
			this.toggleActiveClass("toggleDraggableMode", true);
			this.dragHandler = new interaction.Translate({layers: ["aoi", "sar_surpic"].map(layer => this.layerService.get(layer).ol_)});
			this.dragHandler.on("translatestart", (e) => {
				// Filtering active or past sar_surpic as they should not be translatable
				if (~e.features.getArray().findIndex(feature => feature.get("seg_type") === "sar_surpic" && feature.get("status") !== "candidate")){
					// resets draggable mode, preventing translation
					this.removeDrag();
					this.toggleDraggableMode();
				}
			});
			this.cleanablesService.add("Deactivate Drag", this.removeDrag);///////////////////////////////////////////////////////////////cleanables doesn"t exist yet
			this.mapService.getMap().ol_.addInteraction(this.dragHandler);
		}else{
			this.removeDrag();
		}
	}

	toggleActiveClass(tool, bool) {
		Object.keys(this.activeTool).forEach((key) => {
			this.activeTool[key] = false;
		});

		if (tool) {
			this.activeTool[tool] = bool;
		}

		this.cleanablesService.remove("Deactivate Drag");///////////////////////////////////////////////////////////////cleanables doesn"t exist yet
		this.mapService.getMap().ol_.removeInteraction(this.dragHandler);
		this.dragHandler = null;
	}
}
