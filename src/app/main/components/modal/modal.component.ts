import { Component, OnInit, OnDestroy, ElementRef } from "@angular/core";
import { ModalService } from "@main/services/modal/modal.service";

@Component({
	selector: "seg-modal",
	templateUrl: "./modal.component.html",
	styleUrls: ["./modal.component.scss"]
})
export class ModalComponent implements OnInit, OnDestroy {

	openModals;
	title;

	constructor(
		private modalService: ModalService
	) { }

	ngOnInit() {
		this.modalService.openModals.subscribe(openModals => {
			this.openModals = openModals;
		});
	}

	ngOnDestroy() {
		this.modalService.close(this.title);
	}

	hasOpenModals() {
		return Object.keys(this.openModals).length;
	}

	close(modal) {
		delete this.openModals[modal.title];
	}

	hideModal(modal) {
		modal.hide = !modal.hide;
	}

	dismiss(modal) {
		if (modal.onDismiss)
			modal.onDismiss();

		if (this.modalService.openModal(modal.title, null, null).onDismiss)
			this.modalService.openModal(modal.title, null, null).onDismiss();

		this.close(modal);
	}

	hasDismiss(modal) {
		if (this.modalService.openModal(modal.title, null, null).onDismiss)
			return true;
		return false;
	}

	showConfirmFooter(modal) {
		return modal.onConfirm ? true : false;
	}

	confirm(modal) {
		if (modal.onConfirm)
			modal.onConfirm();

		modal.close();
	}

}
