///*

import { ScriptsService } from "@core/services/request/scripts.service";
import { LocationService } from "@core/services/location.service";
import { MapService } from "@core/services/map/map.service";
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

declare const twttr: any;

@Component({
	selector: "seg-news",
	templateUrl: "./news.component.html",
	styleUrls: ["./news.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewsComponent implements OnInit {

	container = "twttr_container";
	open = false;
	ready = false;
	currentPost = 0;
	posts = null;
	twttr;

	// tweeter query
// #SAR OR #SearchAndRescue OR #MaritimeAccident OR #MaritimeIncident OR #MaritimeEmergency OR #MaritimeSafety OR #MaritimeSecurity OR #MaritimeDistress OR #VesselDistress #VesselAccident OR #VesselIncident OR #VesselSafety OR  #VesselSecurity OR #VesselEmergency OR  #IntegratedMaritimeService OR #Mayday OR #AIS OR #LRIT OR #VMS OR #SARSAT OR #VTS OR #VesselTrafficServices OR #MRCC OR #MRC OR #MaritimeRescueCoordinationCentre OR #MaritimeRescueCentre  OR #JointRescueCoordinationCentre OR #JRCC OR #SARPointofContact OR #SPOC OR #SearchandRescueRegion OR #SRR  OR #RescueCoordinationCentre OR #RCC OR #CoastGuard OR #OilSpill OR #ChemicalSpill OR #HNSSpill

	constructor(
		private mapService: MapService,
		private locationService: LocationService,
		private scriptsService: ScriptsService,
		private cdr: ChangeDetectorRef
	) {
	 }

	ngOnInit() {
		document.getElementById("news-wrapper").style.display = "none";
		const mainContainer = document.getElementById("news-content");

		const node = document.createElement("div");
		node.setAttribute("id", this.container);

		mainContainer.appendChild(node);
		document.getElementById(this.container).style.display = "none";

		this.cdr.detectChanges();
	}

	createElementFromHTML = (htmlString) => {
		const div = document.createElement("div");
		div.innerHTML = htmlString.trim();

		// Change this to div.childNodes to support multiple top-level nodes
		return div.firstChild;
	}

	toggleNewsFeed() {
		if (!this.ready) {
			// injecting twitter widget and then executing timeline functions. This achieves assynchrounous loading of twitter feed, only when the screen is opened
			document.getElementById(this.container).innerHTML = "<a data-chrome=\"noheader nofooter noscrollbar\" class=\"twitter-timeline\" href=\"https://twitter.com/SEG_EMSA/lists/emsa-twitter-feed?ref_src=twsrc%5Etfw\">";

			this.scriptsService.load({name: "twitter-newsfeed", src: "//platform.twitter.com/widgets.js", loaded: false}).subscribe(() => {
				this.ready = true;

				twttr.events.bind("rendered", () => {
					[].slice.call(document.querySelectorAll("iframe.twitter-timeline")).forEach((e) => {
						e.contentDocument.getElementsByTagName("head")[0]
						.append(this.createElementFromHTML(`<style type="text/css">
							.TweetAuthor-name{font-size:12px!important;}
							.timeline-Tweet-text{font-size:11px !important;}
							.timeline-Body{border-top: none;}
							.timeline-Tweet{position:relative;}
							.timeline-Tweet-retweetCredit,.timeline-Tweet-actions,.timeline-Tweet-brand{display:none;}
							.timeline-Tweet-metadata{position: absolute; top: 10px; right: 10px;}
							.timeline-Viewport{height: 180px !important;}
							</style>`));
					});
					this.posts = this.getTweetNodes();
				});
			});
		}

		this.open = !this.open;

		if (!this.open) {
			document.getElementById("news-wrapper").style.display = "none";
			document.getElementById("twttr_container").style.display = "none";
			return;
		} else {
			document.getElementById("news-wrapper").style.display = "flex";
			document.getElementById("twttr_container").style.display = "block";
			// fix to customize twitter widget css
		}

		this.cdr.detectChanges();
	}

	getIframeDOM() {
		return document.getElementsByTagName("iframe")[0].contentWindow.document;
	}

	getTweetsViewportNode() {
		return this.getIframeDOM().getElementsByClassName("timeline-Viewport")[0];
	}

	getTweetNodes() {
		return this.getIframeDOM().getElementsByClassName("timeline-TweetList-tweet");
	}

	previousPost() {
		if (this.currentPost > 0) {
			if (!this.posts)
				this.posts = this.getTweetNodes();

			this.currentPost -= 1;

			this.getTweetsViewportNode().scrollTop -= (this.posts[this.currentPost]).getBoundingClientRect().height;
		}
	}

	nextPost() {
		if (!this.posts)
			this.posts = this.getTweetNodes();

		if (this.currentPost >= this.posts.length - 1)
			return;

		this.currentPost += 1;

		this.getTweetsViewportNode().scrollTop += (this.posts[this.currentPost]).getBoundingClientRect().top;
	}

}


//*/




/////////////////////////////////// FOR ALERT TESTING ////////////////////////////////////////
/*

import { AlertsService } from './../../../core/services/alerts.service';
import { ScriptsService } from "@core/services/request/scripts.service";
import { LocationService } from "@core/services/location.service";
import { MapService } from "@core/services/map/map.service";
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

declare const twttr: any;

@Component({
	selector: "seg-news",
	templateUrl: "./news.component.html",
	styleUrls: ["./news.component.scss"],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewsComponent implements OnInit {

	container = "twttr_container";
	open = false;
	ready = false;
	currentPost = 0;
	posts = null;
	twttr;
	alertNum = 0;

	// tweeter query
// #SAR OR #SearchAndRescue OR #MaritimeAccident OR #MaritimeIncident OR #MaritimeEmergency OR #MaritimeSafety OR #MaritimeSecurity OR #MaritimeDistress OR #VesselDistress #VesselAccident OR #VesselIncident OR #VesselSafety OR  #VesselSecurity OR #VesselEmergency OR  #IntegratedMaritimeService OR #Mayday OR #AIS OR #LRIT OR #VMS OR #SARSAT OR #VTS OR #VesselTrafficServices OR #MRCC OR #MRC OR #MaritimeRescueCoordinationCentre OR #MaritimeRescueCentre  OR #JointRescueCoordinationCentre OR #JRCC OR #SARPointofContact OR #SPOC OR #SearchandRescueRegion OR #SRR  OR #RescueCoordinationCentre OR #RCC OR #CoastGuard OR #OilSpill OR #ChemicalSpill OR #HNSSpill

	constructor(
		private mapService: MapService,
		private locationService: LocationService,
		private scriptsService: ScriptsService,
		private cdr: ChangeDetectorRef,
		private alertsService: AlertsService
	) {
	 }

	ngOnInit() {
		document.getElementById("news-wrapper").style.display = "none";
		const mainContainer = document.getElementById("news-content");

		const node = document.createElement("div");
		node.setAttribute("id", this.container);

		mainContainer.appendChild(node);
		document.getElementById(this.container).style.display = "none";

		this.cdr.detectChanges();
	}

	createElementFromHTML = (htmlString) => {
		const div = document.createElement("div");
		div.innerHTML = htmlString.trim();

		// Change this to div.childNodes to support multiple top-level nodes
		return div.firstChild;
	}

	toggleNewsFeed() {
		if (!this.ready) {
			// injecting twitter widget and then executing timeline functions. This achieves assynchrounous loading of twitter feed, only when the screen is opened
			document.getElementById(this.container).innerHTML = "<a data-chrome=\"noheader nofooter noscrollbar\" class=\"twitter-timeline\" href=\"https://twitter.com/SEG_EMSA/lists/emsa-twitter-feed?ref_src=twsrc%5Etfw\">";

			this.scriptsService.load({name: "twitter-newsfeed", src: "//platform.twitter.com/widgets.js", loaded: false}).subscribe(() => {
				this.ready = true;

				twttr.events.bind("rendered", () => {
					[].slice.call(document.querySelectorAll("iframe.twitter-timeline")).forEach((e) => {
						e.contentDocument.getElementsByTagName("head")[0]
						.append(this.createElementFromHTML(`<style type="text/css">
							.TweetAuthor-name{font-size:12px!important;}
							.timeline-Tweet-text{font-size:11px !important;}
							.timeline-Body{border-top: none;}
							.timeline-Tweet{position:relative;}
							.timeline-Tweet-retweetCredit,.timeline-Tweet-actions,.timeline-Tweet-brand{display:none;}
							.timeline-Tweet-metadata{position: absolute; top: 10px; right: 10px;}
							.timeline-Viewport{height: 180px !important;}
							</style>`));
					});
					this.posts = this.getTweetNodes();
				});
			});
		}

		this.open = !this.open;

		if (!this.open) {
			document.getElementById("news-wrapper").style.display = "none";
			document.getElementById("twttr_container").style.display = "none";
			return;
		} else {
			document.getElementById("news-wrapper").style.display = "flex";
			document.getElementById("twttr_container").style.display = "block";
			// fix to customize twitter widget css
		}

		this.cdr.detectChanges();
	}

	getIframeDOM() {
		return document.getElementsByTagName("iframe")[0].contentWindow.document;
	}

	getTweetsViewportNode() {
		return this.getIframeDOM().getElementsByClassName("timeline-Viewport")[0];
	}

	getTweetNodes() {
		return this.getIframeDOM().getElementsByClassName("timeline-TweetList-tweet");
	}

	previousPost() {
		if (this.currentPost > 0) {
			if (!this.posts)
				this.posts = this.getTweetNodes();

			this.currentPost -= 1;

			this.getTweetsViewportNode().scrollTop -= (this.posts[this.currentPost]).getBoundingClientRect().height;
		}
	}

	nextPost() {
		if (!this.posts)
			this.posts = this.getTweetNodes();

		if (this.currentPost >= this.posts.length - 1)
			return;

		this.currentPost += 1;

		this.getTweetsViewportNode().scrollTop += (this.posts[this.currentPost]).getBoundingClientRect().top;
	}

	addNewTestAlert(){
		//this.alertsService.add({ id: this.alertNum, label: this.getRandomString(), feature: null, read:false, freshAlert:false})
		this.alertsService.addFreshDummy(this.getRandomString())
		this.alertNum++;
	}

	getRandomString(){
		let wordSize = 1 + Math.floor(Math.random()*20)
		let str: String = '';
		for(let i = 0; i < wordSize; i++){
			let num = 65 + Math.floor(Math.random() * 26);
			str += String.fromCharCode(num);
		}
		return str;
	}

	markRead(){
		this.alertsService.markAlertsAsRead();
		this.alertNum++;
	}

}

*/