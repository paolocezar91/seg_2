import { Component, OnInit, ElementRef, Renderer2 } from "@angular/core";
import { Observable } from "rxjs";
import { LogService } from "@core/services/log.service";
import { DrawService } from "@core/services/interactions/draw.service";
import { TooltipService } from "@main/services/tooltip.service";
import { PreferencesService } from "@core/services/preferences.service";
import { MapService } from "@core/services/map/map.service";
import * as extent from "ol/extent";

@Component({
  selector: "seg-tooltip",
  templateUrl: "./tooltip.component.html",
  styleUrls: ["./tooltip.component.scss"]
})

export class TooltipComponent implements OnInit {

	tooltipScope = {};
  	hoveredFeature;
  	compiledTooltip;

	constructor(
		private el: ElementRef,
		private mapService: MapService,
		private logService: LogService,
		private drawService: DrawService,
		private tooltipService: TooltipService,
		private preferencesService: PreferencesService,
		private renderer: Renderer2
	) { }

	ngOnInit() {
		this.mapService.onMapReady(() => {
			this.setupListener(this.mapService.getMap());
   		});
	}

	layerFilter(candidate) {
		return candidate.get("tooltip");
	}

	showTooltip(map, pixel) {
		let showingTooltip, extendFeatureArray;

		if (map.ol_.hasFeatureAtPixel(pixel)) {
			let featuresAtPixel = [], preferentialFeaturesAtPixel = [];

			map.ol_.forEachFeatureAtPixel(pixel, (feature, layer) => {
				featuresAtPixel.push({feature, layer});
			}, {layerFilter: this.layerFilter});

			preferentialFeaturesAtPixel = featuresAtPixel.filter(({feature}) => this.mapService.getMap().getPreferentialLayers().some(layer => layer === feature.get("seg_type")));

			if (preferentialFeaturesAtPixel.length)
				featuresAtPixel = preferentialFeaturesAtPixel;

			featuresAtPixel.forEach(({feature, layer}) => {
				if (!this.drawService.active) { // if drawService is active, other mouse interactions should not work
					const tooltipConfig = this.tooltipService.get(layer);

					if (!tooltipConfig.multiTooltip) {
						showingTooltip = this.hoveredFeature === feature;

						if (showingTooltip || !tooltipConfig)
							return true;

						showingTooltip = true;

						const template = tooltipConfig.template,
							featurePixel = map.getPixelFromCoordinate(extent.getCenter(feature.getGeometry().getExtent())),
							extendFeature = {};

						extendFeature[tooltipConfig.featureAs] = feature;

						this.tooltipScope = {scope: Object.assign((tooltipConfig.bindings || {}), extendFeature), template};
						this.renderer.removeClass(this.el.nativeElement, "tooltip-right");
						this.renderer.removeClass(this.el.nativeElement, "tooltip-left");
						this.renderer.removeClass(this.el.nativeElement, "tooltip-bottom");
						this.hoveredFeature = feature;

						requestAnimationFrame(() => {
							const [x, y] = featurePixel,
								positions = this.calcTooltipPosition(this.el.nativeElement, x, y);

							if (positions.length > 0)
								this.applyCSS(this.el.nativeElement, positions[0]);
						});
					} else {
						if (layer.id === feature.seg_type) {
							if (!extendFeatureArray)
								extendFeatureArray = {};

							if (!extendFeatureArray[tooltipConfig.featureAs])
								extendFeatureArray[tooltipConfig.featureAs] = [];

							const template = tooltipConfig.template;

							extendFeatureArray[tooltipConfig.featureAs].push(feature);

							this.tooltipScope = {scope: Object.assign(tooltipConfig.bindings, extendFeatureArray), template};
							this.renderer.removeClass(this.el.nativeElement, "tooltip-right");
							this.renderer.removeClass(this.el.nativeElement, "tooltip-left");
							this.renderer.removeClass(this.el.nativeElement, "tooltip-bottom");

							showingTooltip = true;

							requestAnimationFrame(() => {
								const [x, y] = pixel,
									positions = this.calcTooltipPosition(this.el.nativeElement, x, y);

								if (positions.length > 0)
									this.applyCSS(this.el.nativeElement, positions[0]);
							});
						}
					}
				}
			});
		} else {
			map.ol_.forEachLayerAtPixel(pixel, layer => {
				if (!this.drawService.active) { // if drawService is active, other mouse interactions should not work
					const tooltipConfig = this.tooltipService.get(layer),
						toShow = layer.getSource().getImageInternal;

					if (tooltipConfig && toShow) {
						showingTooltip = true;

						const template = tooltipConfig.template,
							extendSource = {};

						extendSource[tooltipConfig.sourceAs] = layer.getSource();

						this.tooltipScope = {scope: Object.assign(tooltipConfig.bindings, extendSource), template};
						this.renderer.removeClass(this.el.nativeElement, "tooltip-right");
						this.renderer.removeClass(this.el.nativeElement, "tooltip-left");
						this.renderer.removeClass(this.el.nativeElement, "tooltip-bottom");

						requestAnimationFrame(() => {
							const positions = this.calcTooltipPosition(this.el.nativeElement, pixel[0], pixel[1]);

							if (positions.length > 0)
								this.applyCSS(this.el.nativeElement, positions[0]);
						});
					}
				}
			}, {layerFilter: this.layerFilter});

			this.hoveredFeature = null;
		}

		if (!showingTooltip) {
			this.hoveredFeature = null;
			if (this.tooltipScope)
				this.tooltipScope = null;
		}
	}

	setupListener(map) {
		map.ol_.on("pointermove", e => {
			if (e.dragging)
				return;

			this.showTooltip(map, map.ol_.getEventPixel(e.originalEvent));
		});
	}

	applyCSS(element, cssData) {
		// This exists since IE has a bug applying .css and .addClass
		// set css
		if (cssData.cssClass !== "")
			this.renderer.addClass(element, cssData.cssClass);

		for (const key in cssData.css)
			if (cssData.css.hasOwnProperty(key))
				element.style[key] = cssData.css[key];
	}

	calcTooltipPosition(tooltip, featureX, featureY) {
		// possible positions
		const basePositions = [],
			dimensions = tooltip.getBoundingClientRect();

		if (featureY - tooltip.offsetHeight >= 0 && (featureX - (tooltip.offsetWidth / 2) >= 0) && (featureX + (tooltip.offsetWidth / 2) < window.innerWidth)) {
			basePositions.push({
				position: "top",
				left: featureX - (tooltip.offsetWidth / 2),
				right: featureX + (tooltip.offsetWidth / 2),
				top: featureY - tooltip.offsetHeight - 20,
				bottom: featureY - 20,
				cssClass: "",
				css: {
					position: "absolute",
					left: featureX - dimensions.width / 2 + "px",
					top: featureY - 20 - dimensions.height + "px"
				}
			});
		}

		if (featureY + tooltip.offsetHeight < window.innerHeight && (featureX - (tooltip.offsetWidth / 2) >= 0) && (featureX + (tooltip.offsetWidth / 2) < window.innerWidth)) {
			basePositions.push({
				position: "bottom",
				left: featureX - (tooltip.offsetWidth / 2),
				right: featureX + (tooltip.offsetWidth / 2),
				top: featureY + 20,
				bottom: featureY + tooltip.offsetHeight + 20,
				cssClass: "tooltip-bottom",
				css: {
					position: "absolute",
					left: featureX - dimensions.width / 2 + "px",
					top: featureY + 20 + "px"
				}
			});
		}

		if (featureX + tooltip.offsetWidth < window.innerWidth) {
			basePositions.push({
				position: "right",
				left: featureX + 25,
				right: featureX + 25 + tooltip.offsetWidth,
				top: featureY - (tooltip.offsetHeight / 2),
				bottom: featureY + (tooltip.offsetHeight / 2),
				cssClass: "tooltip-right",
				css: {
					position: "absolute",
					left: featureX + 25 + "px",
					top: featureY - 5 - dimensions.height / 2 + "px"
				}

			});
		}

		if (featureX - tooltip.offsetWidth >= 0) {
			basePositions.push({
				position: "left",
				left: featureX - 25 - tooltip.offsetWidth,
				right: featureX - 25,
				top: featureY - (tooltip.offsetHeight / 2),
				bottom: featureY + (tooltip.offsetHeight / 2),
				cssClass: "tooltip-left",
				css: {
					position: "absolute",
					left: featureX - 20 - dimensions.widht + "px",
					top: featureY - 5 - dimensions.height / 2 + "px"
				}

			});
		}

		// verify other panels
		// query-panel, layer-preferences
		const elements = ["query-panel", "layer-preferences"];
		let positions = basePositions;

		elements.forEach(elemTagName => {
			const elem = this.el.nativeElement.querySelector(elemTagName);

			if (elem)
				positions = positions.filter(pos => !this.tooltipOverlap(pos, elem));
		});

		return (positions.length > 0 ? positions : basePositions);
	}

	tooltipOverlap(position, elem) {
		const rect = elem.getBoundingClientRect();

		return !(position.right < rect.left ||
			position.left > rect.right ||
			position.bottom < rect.top ||
			position.top > rect.bottom);
	}
}
