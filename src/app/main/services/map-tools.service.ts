import { Injectable } from "@angular/core";

@Injectable({
	providedIn: "root"
})
export class MapToolsService {

	toolbars = {draw: false, measure: false};

	constructor() {	}

	/**
	 * Toggle map-tools buttons.
	 * @param {string}   toolbarName "draw" | "measure"
	 * @param {boolean}   state [description]
	 */
	toggle(toolbarName: "draw" | "measure" , state?: boolean) {
		this.toolbars[toolbarName] = state || !this.toolbars[toolbarName];

		if (this.toolbars[toolbarName])
			for (const name in this.toolbars)
				this.toolbars[name] = name === toolbarName;
	}
}
