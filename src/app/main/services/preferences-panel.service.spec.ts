import { TestBed } from '@angular/core/testing';

import { PreferencesPanelService } from './preferences-panel.service';

describe('PreferencesPanelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PreferencesPanelService = TestBed.get(PreferencesPanelService);
    expect(service).toBeTruthy();
  });
});
