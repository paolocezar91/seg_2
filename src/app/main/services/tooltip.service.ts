import { LogService } from "@core/services/log.service";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
	providedIn: "root"
})
export class TooltipService {

	registry = new Map();

	constructor(
		private logService: LogService,
		private http: HttpClient,
	) { }

	async register(layer, options) {
		if (options.templateUrl) {
			const template = await this.http.get(options.templateUrl, {responseType: "text"}).toPromise();
			options.template = template; // , e => {this.logService.error("ERROR_TOOLTIP", e + ": " + e.result); console.log(e)});
		}

		this.registry.set(layer.ol_, options);
	}

	get(layer) {
		return this.registry.get(layer);
	}

}

