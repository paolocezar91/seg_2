import { Injectable } from "@angular/core";

@Injectable({
	providedIn: "root"
})
export class DownloadHistoryService {

	downloads = {};

	constructor() { }

	add(config) {
		this.downloads[Date.now()] = config;
		return config;
	}

	get() {
		return this.downloads;
	}

	hasDownloads() {
		return !!Object.keys(this.downloads).length;
	}
}
