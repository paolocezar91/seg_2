import { Injectable } from "@angular/core";
import * as $o from "libs/seg-object";
import { LogService } from "@core/services/log.service";
import { BehaviorSubject } from "rxjs";

const LOG_TAG = "seg.ui.ttt.tables";

@Injectable({
  providedIn: "root"
})
export class TablesService {

	tableOpenListeners = new $o.EventListenerQueue();
	registeredTablesValue = [];
	registeredTables: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

	openTablesValue = [];
	openTables: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

	constructor(private logService: LogService) { }

	register(config) {
		this.validateTable(config, "register");

		if (
			this.registeredTablesValue.find((table) => table.label === config.label) ||
			this.openTablesValue.find((table) => table.label === config.label)
		)
			this.logService.error(LOG_TAG + ".register:", "A table named " + config.label + " already exists");

		this.registeredTablesValue.push(config);
		if (!config.dataChanges)
			config.dataChanges = new BehaviorSubject<any[]>(config.data || []);

		this.registeredTables.next([].concat(this.registeredTablesValue));

		if (!~this.openTablesValue.indexOf(config) && config.open === false)
			this.openTablesValue.push(config);

		return config;
	}

	unregister(label) {
		const found = this.registeredTablesValue.find((table) => table.label === label);

		if (found) {
			$o.removeFromArray(this.registeredTablesValue, found);
			this.registeredTables.next([].concat(this.registeredTablesValue));
		}
	}

	get(label) {
		return this.openTablesValue.find(table => table.label === label);
	}

	addDataToTable (table, data) {
		table.data.push(...data.filter(item => !~table.data.indexOf(item)));
		table.dataChanges.next([].concat(table.data));
	}

	open(labelOrConfig) {
		let foundTab;

		// if argument is string, then we"re trying to open a registered table
		if (typeof labelOrConfig === "string") {
			foundTab = this.registeredTablesValue.find((tab: any) => tab.label === labelOrConfig);
			if (!foundTab)
				throw this.logService.error(LOG_TAG + ".open", "Table " + labelOrConfig + " not found");
		} else {
			this.validateTable(labelOrConfig, "open");
			foundTab = this.openTablesValue.find(table => table.label === labelOrConfig.label) || labelOrConfig;
		}

		if (!foundTab.dataChanges)
			foundTab.dataChanges = new BehaviorSubject<any[]>(foundTab.data || []);

		this.openTablesValue.forEach((tab: any) => tab.open = false);
		// add table to open tables
		if (!~this.openTablesValue.indexOf(foundTab)) {
			foundTab.open = true;
			this.openTablesValue.push(foundTab);
		}

		// this triggers opening of correct table
		this.openTables.next([].concat(this.openTablesValue));

		this.tableOpenListeners.trigger(foundTab);

		if (foundTab.onOpen)
			foundTab.onOpen();

		return foundTab;
	}

	close(labelOrConfig) {
		this.openTablesValue.forEach(table => {
			// remove from open tables
			if (table === labelOrConfig || table.label === labelOrConfig) {
				$o.removeFromArray(this.openTablesValue, table);
				this.openTables.next([].concat(this.openTablesValue));

				if (table.onClose)
					table.onClose();
			}
		});
	}

	onTableOpen(callback) {
		return this.tableOpenListeners.register(callback);
	}

	validateTable(options, callerMethod) {
		if (typeof options.label !== "string")
			throw this.logService.error(LOG_TAG + "." + callerMethod, "No label specified for table");

		if (typeof options.itemAs !== "string")
			throw this.logService.error(LOG_TAG + "." + callerMethod, "You must specify an itemAs alias for table " + options.label);

		if (typeof options.fields !== "object")
			throw this.logService.error(LOG_TAG + "." + callerMethod, "No fields specified for table " + options.label);
	}
}
