import { LogService } from '@core/services/log.service';
import { MapService } from '@core/services/map/map.service';
import { PreferencesService } from '@core/services/preferences.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PreferencesPanelService {

  constructor(
    private preferencesService: PreferencesService,
    private mapService: MapService,
    private logService: LogService
  ) { }

  tabs = {
		"Show": {
			label: "Show",
			sections: [{
				alias: "show",
				template: `
				<div class="row">
					<label class="text-size-sm flex-1">Magnify and center buttons</label>
					<toggle ng-model="show.magnifyAndCenterButtons" ng-model-options="{getterSetter: true}"></toggle>
				</div>
				<div class="row">
					<label class="text-size-sm flex-1">Show Vessel Models</label>
					<toggle ng-model="show.vesselModel" ng-model-options="{getterSetter: true}"></toggle>
				</div>
				`,
				bindings: {
					magnifyAndCenterButtons: val => {
						if(typeof val === "undefined")
							return this.preferencesService.getWorkspacePreference("show.magnifyAndCenterButtons");

						this.preferencesService.setWorkspacePreference("show.magnifyAndCenterButtons", val);
					},
					vesselModel: val => {
						if(typeof val === "undefined")
							return this.preferencesService.getWorkspacePreference("show.vesselModel");

						this.preferencesService.setWorkspacePreference("show.vesselModel", val);
						this.mapService.getMap().findLayerById("positions").getSource().changed();
					}
				}
			}]
		}
	};

	/*
	label = string
	config = {
		template: url,
		bindings: object,
		id: string
	}
	*/
	addSectionToTab = (label, config) => {
		let tab = this.tabs[label];

		if(typeof tab !== "object")
			this.tabs[label] = tab = {
				label,
				sections: []
			};

		if(typeof config.template !== "string" && typeof config.templateUrl !== "string")
			throw this.logService.error(label+".addSectionToTab", "You must specify a template for the section");

		tab.sections.push(config);
	};

	getTabs = () => this.tabs;

}
