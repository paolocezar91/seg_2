import { Injectable } from "@angular/core";
import { Modal } from "./Modal";
import { LoadingModal } from "./LoadingModal";
import { BehaviorSubject } from "rxjs";

@Injectable({
	providedIn: "root"
})
export class ModalService {

	constructor() { }

	openModalsValues = {};
	openModals: BehaviorSubject<any> = new BehaviorSubject<any>(this.openModalsValues);

	get(title) {
		if (!this.openModalsValues[title])
			return null;
		return this.openModalsValues[title];
	}

	openModal = (title, content, scope = {}) => {
		this.openModalsValues[title] = new Modal(title, content, scope);
		this.openModals.next(this.openModalsValues);

		return this.openModalsValues[title];
	}

	openLoader = (title, content, totalItems = 1) => {
		this.openModalsValues[title] = new LoadingModal(title, content, totalItems);
		this.openModals.next(this.openModalsValues);

		return this.openModalsValues[title];
	}

	close = (title) => {
		delete this.openModalsValues[title];
		this.openModals.next(this.openModalsValues);
	}

	// Default timeout to close modal 2.5s
	closeTimeout = title => {
		if (this.openModalsValues[title])
			this.openModalsValues[title].closing = true;
		return setTimeout(() => this.close(title), 2500);
	}

}
