export class Modal {

	title;
	type;
	content;
	scope;
	hide;
	onConfirm;
	onDismiss;

	constructor (title, content?, scope?) {
		this.title = title;
		this.content = content;
		this.scope = scope;
		this.hide = false;
		this.type = "modal";
		this.onConfirm = null;
	}

	setContent(message) {
		this.content = message;
	}

	setDismissCallback(callback) {
		this.onDismiss = callback;
	}

	setConfirmCallback(callback) {
		this.onConfirm = callback;
	}

}
