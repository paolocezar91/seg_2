import { Modal } from "./Modal";

export class LoadingModal extends Modal {
	partialProgresses;

	constructor(title, content?, totalItems?) {
		super(title, content, {
			loader: {
				progress: 0,
				totalItems
			}
		});

		this.type = "loading-modal";
		this.partialProgresses = {};
	}

	setContent(message) {
		this.content = message;
	}

	updateProgress() {
		this.scope.loader.progress += (1 / this.scope.loader.totalItems) * 100;

		/*
		self.scope.loader.progress = Object.keys(self.partialProgresses).length?Object.keys(self.partialProgresses).reduce(function(avg, key) {
			avg += self.partialProgresses[key];

			return avg;
		}, 0) / Object.keys(self.partialProgresses).length:100;*/
	}

	setPartialProgress(key, progress) {

		this.partialProgresses[key] = progress;

		this.scope.loader.progress = Object.keys(this.partialProgresses).length ? (Object.keys(this.partialProgresses).reduce((avg, _key) => {
			avg += this.partialProgresses[_key];
			return avg;
		}, 0) / Object.keys(this.partialProgresses).length) : 100;
	}
}
