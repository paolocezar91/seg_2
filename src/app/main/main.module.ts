import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UiModule } from "@ui/ui.module";
import { CoreModule } from "@core/core.module";
import { PipesModule } from "@ui/pipes/pipes.module";

// TABLES
import { TablesService } from "@main/services/ttt/tables.service";
import { TttComponent } from "@main/components/ttt/ttt.component";

// MAP TOOLS
import { MapToolsComponent } from "@main/components/map-tools/map-tools.component";
import { DrawToolbarComponent } from "@main/components/map-tools/draw-toolbar/draw-toolbar.component";
import { MeasureToolbarComponent } from "@main/components/map-tools/measure-toolbar/measure-toolbar.component";
import { MapToolsService } from "@main/services/map-tools.service";

// TOOLTIP
import { TooltipComponent } from "@main/components/tooltip/tooltip.component";
import { TooltipService } from "@main/services/tooltip.service";

// MAP
import { MapComponent } from "@main/components/map/map.component";
// MAP-INFO
import { MapScaleComponent } from "@main/components/map-info/map-scale/map-scale.component";
import { StatusBarComponent } from "@main/components/map-info/status-bar/status-bar.component";
import { MapInfoComponent } from "@main/components/map-info/map-info.component";
import { TopRightMenuComponent } from "@main/components/top-right-menu/top-right-menu.component";
import { ShortcutsComponent } from "@main/components/top-right-menu/shortcuts/shortcuts.component";

// MODAL
import { ModalService } from "@main/services/modal/modal.service";
import { ModalComponent } from "@main/components/modal/modal.component";

// DOWNLOAD HISTORY
import { DownloadHistoryComponent } from "@main/components/download-history/download-history.component";
import { DownloadHistoryService } from "@main/services/download-history.service";
// LAYER INFO
import { LayerInfoComponent } from "@main/components/layer-info/layer-info.component";
import { CleanComponent } from './components/top-right-menu/clean/clean.component';
// NEWS
import { NewsComponent } from './components/news/news.component';
import { AlertsComponent } from './components/top-right-menu/alerts/alerts.component';
import { FreshAlertComponent } from './components/top-right-menu/fresh-alert/fresh-alert.component';
import { PreferencesComponent } from './components/top-right-menu/preferences/preferences.component';
import { UnitsComponent } from './components/top-right-menu/preferences/units/units.component';

@NgModule({
	declarations: [
		// MAP TOOLS
		MapToolsComponent,
		DrawToolbarComponent,
		MeasureToolbarComponent,
		// TTT
		TttComponent,
		TooltipComponent,
		// MAP
		MapComponent,
		// MAP-INFO
		MapScaleComponent,
		StatusBarComponent,
		MapInfoComponent,
		TopRightMenuComponent,
		ShortcutsComponent,
		// ALERTS
		AlertsComponent,
		// MODAL
		ModalComponent,
		// DOWNLOAD HISTORY
		DownloadHistoryComponent,
		// LAYER INFO
		LayerInfoComponent,
		CleanComponent,
		NewsComponent,
		FreshAlertComponent,
		PreferencesComponent,
		UnitsComponent
	],
	exports: [
		// MAP TOOLS
		MapToolsComponent,
		DrawToolbarComponent,
		MeasureToolbarComponent,
		// TTT
		TooltipComponent,
		TttComponent,
		/// MAP
		MapComponent,
		// MAP-INFO
		MapScaleComponent,
		StatusBarComponent,
		MapInfoComponent,
		TopRightMenuComponent,
		ShortcutsComponent,
		// ALERTS
		AlertsComponent,
		FreshAlertComponent,
		// MODAL
		ModalComponent,
		// DOWNLOAD HISTORY
		DownloadHistoryComponent,
		// LAYER INFO
		LayerInfoComponent,
		// NEWS
		NewsComponent,
		//PREFERENCES
		PreferencesComponent,
		UnitsComponent,
	],
	imports: [
		CommonModule,
		UiModule,
		PipesModule,
		CoreModule
	],
	providers: [
		MapToolsService,
		TablesService,
		TooltipService,
		ModalService,
		DownloadHistoryService
	],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ]
})
export class MainModule { }
