export const environment = {
	production: true,
	url: "https://portal.emsa.europa.eu/SEGServer/",
	extensibility_path: "extensibility/"
};