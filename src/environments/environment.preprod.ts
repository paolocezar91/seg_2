export const environment = {
	production: true,
	url: "https://portal-pp.emsa.europa.eu/SEGServer/",
	extensibility_path: "extensibility/"
};
