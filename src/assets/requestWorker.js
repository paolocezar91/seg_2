var self = this;

self.addEventListener("message", function (message) {

	var data = message.data,
		method = data.method,
		url = data.url,
		params = data.params,
		body = data.data,
		form = data.form,
		req = new XMLHttpRequest();

	url += Object.keys(params).length?"?" + Object.keys(params).map(function (key) {
		return key+"="+params[key];
	}).join("&"):"";

	req.open(method, url);

	if(data.responseType)
		req.responseType = data.responseType;

	if(data.timeout)
		req.timeout = data.timeout;

	req.addEventListener("load", function(e) {
		var target = e.target;

		if(target.readyState === XMLHttpRequest.DONE) {
			var res;

			try {
				res = JSON.parse(target.response);
			}
			catch(e) {
				res = target.response;
			}

			//status error
			if(target.status === 200) {
				//service error
				if(res.status === "error")
					res = {
						error: res.result && res.result.errorCode,
						result: res.result?res.result.errorDescription:res.message
					};
			}
			else res = {
				error: target.status,
				result: target.statusText
			};
			self.postMessage(res);
		}
	});

	req.addEventListener("timeout", function(e) {
		var target = e.target;
		var res = {
			error: target.status,
			result: target.statusText
		};
		return self.postMessage(res);
	});

	if(body)
		try {
			body = JSON.stringify(body);
		}
		catch(e) {
			console.log(e);
		}
	else if(form) {
		body = new FormData();

		Object.keys(form).forEach(function(key) {
			body.append(key, form[key]);
		});
	}

	req.send(body);
});