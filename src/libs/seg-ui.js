const $ui = {
	getParents(el, parentSelector /* optional */) {
		// If no parentSelector defined will bubble up all the way to *document*
		if (parentSelector === undefined) {
			parentSelector = document;
		}

		if (!el)
			return null;

		if (el.parentNode !== parentSelector)
			return this.getParents(el.parentNode, parentSelector);
		else
			return el.parentNode;
	},

	getClosest(element, selector) {
		//check itself
		if(element.matches(selector))
			return element;
		/* check upwards
		 * NOTE: this might be the most common case, for event delegation?
		 */
		const ancestors = document.querySelectorAll(selector);

		for(let i=0; i<ancestors.length; i++)
			if(ancestors[i].contains(element))
				return ancestors[i];
		//check downwards
		return element.querySelector(selector);
	},

	loop(callback) {
		return new Promise((resolve/*, reject*/) => {
			function innerLoop(callback) {
				this.RAF(() => {
					if(callback())
						innerLoop(callback);
					else resolve();
				});
			}

			innerLoop(callback);
		});
	},

	transform: {
		position: function(element, x, y, z) {
			let matrix, is2D;

			matrix = $ui.transform.transformMatrix(element),
			is2D = matrix.length === 2*3;

			if(arguments.length > 1) {
				if(is2D) {
					typeof x === "number" && (matrix[$ui.transform.transformMatrix_2D.INDEX.X] = x);
					typeof y === "number" && (matrix[$ui.transform.transformMatrix_2D.INDEX.Y] = y);
				}
				else {
					typeof x === "number" && (matrix[$ui.transform.transformMatrix_3D.INDEX.X] = x);
					typeof y === "number" && (matrix[$ui.transform.transformMatrix_3D.INDEX.Y] = y);
					typeof z === "number" && (matrix[$ui.transform.transformMatrix_3D.INDEX.Z] = z);
				}

				$ui.transform.transformMatrix(element, matrix);
			}

			return is2D?{
				x: matrix[$ui.transform.transformMatrix_2D.INDEX.X],
				y: matrix[$ui.transform.transformMatrix_2D.INDEX.Y]
			}:{
				x: matrix[$ui.transform.transformMatrix_3D.INDEX.X],
				Y: matrix[$ui.transform.transformMatrix_3D.INDEX.Y],
				z: matrix[$ui.transform.transformMatrix_3D.INDEX.Z]
			};
		},
		transformMatrix: (element, matrix) => {
			if(!Array.isArray(matrix)) {
				matrix = window.getComputedStyle(element).transform;

				if(matrix === "none")
					return $ui.transform.transformMatrix_2D.IDENTITY;

				return matrix.replace(/matrix(?:3d)?\(|\)/g, "").split(", ").map((value) => {
					return parseFloat(value);
				});
			}

			element.style.transform = "matrix"+(matrix.length === 2*3?"":"3d")+"("+matrix.map((value) => {
				return Math.round(value);
			}).join(", ")+")";
		},
		transformMatrix_2D: {
			IDENTITY: [1,0, 0,1, 0,0],
			// [1,0,
			//  0,1,
			//  0,0]
			INDEX: {X: 4, Y: 5}
		},
		transformMatrix_3D: {
			INDEX: {X: 12, Y: 13, Z: 14}
		}
	},

	RAF: function(callback) {
		const args = [];

		for(let i=0; i<arguments.length; i++)
			args.push(arguments[i]);

		requestAnimationFrame(() => {
			callback.apply(callback, args);
		});
	},

	//calculates Bounding Rect with margins and paddings Cross browser solution
	getBoundingRect: element => {
		const style = window.getComputedStyle(element);
		const margin = {
			left: parseInt(style["margin-left"]),
			right: parseInt(style["margin-right"]),
			top: parseInt(style["margin-top"]),
			bottom: parseInt(style["margin-bottom"])
		};
		const padding = {
			left: parseInt(style["padding-left"]),
			right: parseInt(style["padding-right"]),
			top: parseInt(style["padding-top"]),
			bottom: parseInt(style["padding-bottom"])
		};
		const border = {
			left: parseInt(style["border-left"] || 0),
			right: parseInt(style["border-right"] || 0),
			top: parseInt(style["border-top"] || 0),
			bottom: parseInt(style["border-bottom"] || 0)
		};


		let rect = element.getBoundingClientRect();
		rect = {
			left: rect.left - margin.left,
			right: rect.right - margin.right - padding.left - padding.right,
			top: rect.top - margin.top,
			bottom: rect.bottom - margin.bottom - padding.top - padding.bottom - border.bottom
		};
		rect.width = rect.right - rect.left;
		rect.height = rect.bottom - rect.top;
		return rect;

	}
}

if (typeof module != 'undefined' && module.exports) module.exports = $ui; // CommonJS, node.js
if (typeof define == 'function' && define.amd) define([], function() { return $ui; }); // AMD