const DMS_STRING_REGEX = /(\d+)°(\d+)'(\d+)(?:''|")\s*([NSEW])/;

const $utils = {
	stripParams(url) {
		return url.split("?")[0];
	},
	stripUrl(url) {
		return url.split("?")[1];
	},
	getParams(url) {
		const params = this.stripUrl(url);

		if(!params)
			return {};

		return params.split("&").reduce((params, param) => {
			const [key, value] = param.split("=");

			params[key] = value;

			return params;
		}, {});
	},
	addParamsToURL(url, newParams) {
		const params = Object.assign(this.getParams(url), newParams),
			hasParams = !!Object.keys(params).length;

		return this.stripParams(url)+(hasParams?("?"+ Object.entries(params).map(entry => entry.join("=")).join("&")):"");
	},
	chunkify (array, chunkSize) {
		const temporal = [];

		for (let i = 0; i < array.length; i+= chunkSize){
			temporal.push(array.slice(i,i+chunkSize));
		}

		return temporal;
	},
	deepClone(obj) {
		return JSON.parse(JSON.stringify(obj));
	},
	deepEquals(x, y) {
		if (x === y) {
			return true; // if both x and y are null or undefined and exactly the same
		} else if (!(x instanceof Object) || !(y instanceof Object)) {
			return false; // if they are not strictly equal, they both need to be Objects
		} else if (x.constructor !== y.constructor) {
			// they must have the exact same prototype chain, the closest we can do is
			// test their constructor.
			return false;
		} else {
			for (const p in x) {
				if (!x.hasOwnProperty(p)) {
			continue; // other properties were tested using x.constructor === y.constructor
		}
		if (!y.hasOwnProperty(p)) {
			return false; // allows to compare x[ p ] and y[ p ] when set to undefined
		}
		if (x[p] === y[p]) {
			continue; // if they have the same strict value or identity then they are equal
		}
		if (typeof (x[p]) !== 'object') {
			return false; // Numbers, Strings, Functions, Booleans must be strictly equal
		}
		if (!$utils.deepEquals(x[p], y[p])) {
			return false;
		}
		}
		for (const p in y) {
			if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) {
				return false;
			}
		}
		return true;
		}
	},
	// Maps the properties for core components like C&I, tooltip, labels, TTT, etc
	// Look for properties file on extensibility to see usage example
	mapItemProperties (defaultProperties, {override}) {
		let finalProperties = defaultProperties;
		if (override){
			finalProperties = Object.keys(defaultProperties).reduce((accumulator, key) => {
				if (typeof override[key] !== "undefined") {
					// if the defaultProperty is a part of include returns true
					accumulator[key] = Object.assign({}, defaultProperties[key]);
					if (override[key])
						accumulator[key] = Object.assign(accumulator[key], override[key]);
				}
				return accumulator;
			}, {});
		}
		return (finalProperties);
	},
	sortObjects(objects) {
		const newObject = {};
		const sortedArray = this.sortProperties(objects, "order");
		for (let i = 0; i < sortedArray.length; i++) {
			const key = sortedArray[i][0];
			const value = sortedArray[i][1];
			newObject[key] = value;
		}
		return newObject;
	},
	sortProperties (obj, sortedBy) {
		sortedBy = sortedBy || 1; // by default first key
		const sortable = [];
		for (const key in obj) {
			if (obj.hasOwnProperty(key)) {
				sortable.push([key, obj[key]]);
			}
		}
		sortable.sort((a, b) => (a[1][sortedBy] - b[1][sortedBy]));
		return sortable;
	},
	copyToClipboard (event) {
		const range = document.createRange();
		range.selectNode(event.currentTarget);
		window.getSelection().removeAllRanges();
		window.getSelection().addRange(range);

		try {
			document.execCommand("copy");
		} catch (err) {
			
		}
	},
	/**
	@method seg.utils.lonLatToDMS
	@param {external:"ol.Coordinate"} coordinates
	@returns {seg.DMSCoordinate}
	*/
	lonLatToDMS (coordinates) {
		return coordinates.reduce((DMS, coordinate, lat) => {
			const degrees = Math.trunc(Math.abs(coordinate)),
				minutes = 60*(Math.abs(coordinate)-degrees),
				seconds = 60*(minutes-Math.trunc(minutes));

			DMS[lat ? "lat" : "lon"] = {
				d: degrees,
				m: Math.trunc(minutes),
				s: Math.round(seconds),
				h: lat?(coordinate>0?"N":"S"):(coordinate>0?"E":"W")
			};

			return DMS;
		}, {});
	},
	coordDMStoDecimalParser(positiveHemisphere) {
		return ({d, m, s, h}) => (parseFloat(d) + ((parseFloat(m) * 60) + parseFloat(s)) / 3600) * ((h === positiveHemisphere)?1:-1);
	},
	/**
	@method seg.utils.latDMStoDecimal
	@param {seg.DMSCoordinate.lat} dms
	@returns {number}
	*/
	latDMStoDecimal(lon) {
		return this.coordDMStoDecimalParser("N")(lon);
	},
	/**
	@method seg.utils.latDMStoDecimal
	@param {seg.DMSCoordinate.lon} dms
	@returns {number}
	*/
	lonDMStoDecimal(lat) {
		return this.coordDMStoDecimalParser("E")(lat);
	},
	/**
	@method seg.utils.dmsToDecimal
	@param {seg.DMSCoordinate} dms
	@returns {external:"ol.Coordinate"}
	*/
	dmsToDecimal({lon, lat}) {
		return [
			this.lonDMStoDecimal(lon),
			this.latDMStoDecimal(lat)
		];
	},
	isCoordinateDMSString(dmsString) {
		dmsString.match(DMS_STRING_REGEX);
	},
	parseDMSString(dmsString) {
		const [d, m, s, h] = dmsString.match(DMS_STRING_REGEX).slice(1);
		return {d, m, s, h};
	},
	isIntegerish(val) {
		return Number.isInteger(parseFloat(val));
	},
	/**
	Checks if a {@link seg.DMSCoordinate} is valid
	@method seg.utils.isValidDMS
	@param {seg.DMSCoordinate} coordinates
	@returns {boolean}
	*/
	isValidDMS(coordinates) {
		return ["lon", "lat"].every(coordinate => ["d", "m", "s"].every(value => this.isIntegerish(coordinates[coordinate][value])));
	},
	getMagnitude(c1, c2) {
		const val1 = Math.pow((c2[0] - c1[0]),2),
			val2 = Math.pow((c2[1] - c1[1]),2);

		return Math.sqrt(val1 + val2);
	},
	/**
	Return angle in degrees between points B and C of a triangle
	@method seg.utils.getAngleBetweenCandB
	@param {external:"ol.Point"} A
	@param {external:"ol.Point"} B
	@param {external:"ol.Point"} C
	@returns {number} angle
	*/
	getAngleBetweenCandB(A, B, C) {
		const a = this.getMagnitude(C, B),
			b = this.getMagnitude(A, C),
			c = this.getMagnitude(A, B);

		if(c === 0)
			return 0;

		const val1 = (Math.pow(b,2) + Math.pow(c,2) - Math.pow(a,2)) / (2 * b * c);

		return Math.acos(val1) * (180 / Math.PI);
	},
	/**
	@method seg.utils.convertFromMeters
	@param {number} value - Value in meters
	@param {('nm'|'m'|'km')} target - Target unit
	@returns {number} value in the target unit
	*/
	convertFromMeters(value, target) {
		value = Number(value);
		switch(target.toUpperCase()) {
			case "NAUTICAL MILES":
			case "NM": return value * 5.399568034557235e-4; // 1 meter = 1/1852 nautical mile
			case "M":
			case "METERS":
			case "MT":
			case "MTS": return value;
			case "KM":
			case "KILOMETERS": return value * 0.001;
		}
	},
	/**
	@method seg.utils.convertToMeters
	@param {number} value - Value in the source unit
	@param {('nm'|'m'|'km')} source - Source unit
	@returns {number} value in meters
	*/
	convertToMeters(value, source) {
		value = Number(value);
		switch(source.toUpperCase()) {
			case "NAUTICAL MILES":
			case "NM":return value * 1852; // 1 nautical mile = 1852 meters
			case "M":
			case "MTS":
			case "MT":
			case "METERS": return value;
			case "KM":
			case "KILOMETERS": return value * 1000;
		}
	},
	/**
	@method seg.utils.convertFromSquareMeters
	@param {number} value - Value in square meters
	@param {('nm2'|'m2'|'km2')} target - Target unit
	@returns {number} value in the target unit
	*/
	convertFromSquareMeters(value, target) {
		value = Number(value);
		switch(target.toUpperCase()) {
			case "SQUARE NAUTICAL MILES":
			case "NM2": return value * 2.9155334959812281743162490845225e-7; // 5.399568034557235e-4^2
			case "M2":
			case "SQUARE METERS":
			case "MT2":
			case "MTS2": return value;
			case "KM2":
			case "SQUARE KILOMETERS": return value * (0.001 * 0.001);
		}
	},

	/**
	@method seg.utils.convertToSquareMeters
	@param {number} value - Value in the source unit
	@param {('nm2'|'m2'|'km2')} source - Source unit
	@returns {number} value in square meters
	*/
	convertToSquareMeters(value, source) {
		value = Number(value);
		switch(source.toUpperCase()) {
			case "SQUARE NAUTICAL MILES":
			case "NM2":return value * 3429904; // 1852 ^ 2;
			case "M2":
			case "MTS2":
			case "MT2":
			case "SQUARE METERS": return value;
			case "KM2":
			case "SQUARE KILOMETERS": return value * (1000 * 1000);
		}
	},

	removeUndefinedAttributes(myObj) {
		Object.keys(myObj).forEach((key) => ((myObj[key] === null) || (typeof myObj[key] === "undefined")) && delete myObj[key]);
		return myObj;
	},

	modulo (a, b) {
		const r = a % b;
		return r * b < 0 ? r + b : r;
	},

	squaredDistance(coord1, coord2) {
		const dx = coord1[0] - coord2[0];
		const dy = coord1[1] - coord2[1];
		return dx * dx + dy * dy;
	},

	parseExtents(extent, MAX_TILES) {
		const splits = Math.min(Math.sqrt(MAX_TILES), Math.ceil(seg.map.getScale()/1000000)),
			requestExtents = [];

		extent = ol.proj.transformExtent(extent, seg.map.getProjection(), "EPSG:4326").map((value, i) => {
			if(!(i%2)) //if pair == if X
				return this.modulo(value + 180, 360) - 180;
			return this.modulo(value + 90, 180) - 90;
		});

		//if wrapping
		if(extent[2] < extent[0])
			//split into two
			requestExtents.push(
				//maxX 180
				[...extent.slice(0, 2), 180, extent[3]],
				//minX -180
				[-180, ...extent.slice(1)]
			);
		else
			requestExtents.push(extent);

		return requestExtents.reduce((extents, [minX, minY, maxX, maxY]) => {
			const subExtents = [],
				subExtentWidth = (maxX - minX) / splits,
				subExtentHeight = (maxY - minY) / splits;

			for(let i = 0; i < splits; i++) {
				const subMinX = minX + i * subExtentWidth;

				for(let j = 0; j < splits; j++) {
					const subMinY = minY + j * subExtentHeight;
					subExtents.push([subMinX, subMinY, subMinX + subExtentWidth, subMinY + subExtentHeight]);
				}
			}

			return [...extents, ...subExtents];
		}, []);
	},

	sleep(milliseconds) {
		var start = new Date().getTime();
		for (var i = 0; i < 1e7; i++) {
		  if ((new Date().getTime() - start) > milliseconds){
			break;
		  }
		}
	},

	htmlToElement(html) {
		var template = document.createElement('template');
		html = html.trim(); // Never return a text node of whitespace as the result
		template.innerHTML = html;
		return template.content.lastChild;
	}
};

if (typeof module != 'undefined' && module.exports) module.exports = $utils; // CommonJS, node.js
if (typeof define == 'function' && define.amd) define([], function() { return $utils; }); // AMD