const $o = {
	inherits(subclass, superclass) {
		return subclass.prototype instanceof superclass || subclass === superclass;
	},

	subclass(subclass, superclass) {
		subclass.prototype = Object.create(superclass.prototype);
		subclass.constructor = superclass;
	},

	inherit(subclass, superclass) {
		Object.assign(subclass.prototype, superclass.prototype);
	},

	traverse(object, path, callback) {
		path = path.split(".");

		return path.every((currentKey, i) => {
			if(typeof object === "undefined")
				return false;

			object = object[currentKey];

			callback(object, currentKey, path.slice(0, i+1).join("."));

			return true;
		});
	},

	each(collection, callback) {
		collection = new Iterator(collection);

		return collection.some(callback);
	},

	setValue(object, path, value) {
		const pathArray = path.split(".");

		//run over path
		while(pathArray.length) {
			//get current key
			const currentKey = pathArray.shift();

			/* if that was the last key,
			 * it means we got to our destination,
			 * so set value
			 */
			if(!pathArray.length)
				object[currentKey] = value;
			//else move down the tree
			else {
				//or if node doesn"t exist, create it first
				if(typeof object[currentKey] === "undefined")
					object[currentKey] = {};

				object = object[currentKey];
			}
		}
	},

	unsetValue(object, path) {
		const parentPath = path.substr(0, path.lastIndexOf("."));
		const keyToDelete = path.substr(path.lastIndexOf(".")+1);

		const parentNode = this.getValue(object, parentPath);

		delete parentNode[keyToDelete];
	},

	getValue(object, path) {
		this.traverse(object, path, (currentNode, currentKey/*, currentPath*/) => {
			object = object[currentKey];
		});

		return object;
	},

	removeFromArray(array, items) {
		spliced = [];
		items = [].concat(items);

		items.forEach((item) => {
			spliced.push(array.splice(array.indexOf(item), 1));
		});

		return spliced;
	},

	binaryFlag(nth) {
		return Math.pow(2, nth-1);
	},

	plotter() {
		const cv = document.createElement("canvas"),
			ctx = cv.getContext("2d");
		let minX, minY, maxX, maxY,
			points = [];

		cv.style.position = "absolute";
		cv.style.background = "white";
		cv.style.transform = "translateZ(10)";
		cv.style.zIndex = "99999";
		cv.style.width = "300px";
		cv.style.height = "150px";
		cv.style.bottom = "0";
		cv.style.opacity = ".8";
		// cv.strokeStyle = "black";
		// cv.lineWidth = "5";
		document.body.appendChild(cv);

		return {
			reset() {
				this.x = this.y = 0;
				minX = minY = Infinity,
				maxX = maxY = -Infinity,
				points = [];
			},
			plot(x, y) {
				Object.assign(this, {x, y});

				minX = Math.min(x, minX),
				minY = Math.min(y, minY),
				maxX = Math.max(x, maxX),
				maxY = Math.max(y, maxY);

				cv.width = maxX - minX,
				cv.height = maxY - minY;

				ctx.lineWidth = (cv.width / 300 + cv.height / 150) / 2;

				ctx.translate(-minX, -minY);

				points.push({x, y});

				ctx.clearRect(0, 0, cv.width, cv.height);
				ctx.beginPath();
				points.forEach((pt, i) => {
					if(!i)
						ctx.moveTo(pt.x, pt.y);
					else ctx.lineTo(pt.x, pt.y);
				});
				ctx.stroke();
			}
		};
	},

	normalize(value) {
		return value/Math.abs(value);
	},

	some(collection, callback) {
		collection = new Iterator(collection);

		return collection.some(callback);
	},

	every(collection, callback) {
		collection = new Iterator(collection);

		return collection.every(callback);
	},

	filter(collection, callback) {
		collection = new Iterator(collection);

		return collection.filter(callback);
	},

	map(collection, callback) {
		collection = new Iterator(collection);

		return collection.map(callback);
	},
	compareVersions(v1, v2, options) {
		const lexicographical = options && options.lexicographical,
			zeroExtend = options && options.zeroExtend;
		let v1parts = v1.split("."),
			v2parts = v2.split(".");

		function isValidPart(x) {
			return (lexicographical ? /^\d+[A-Za-z]*$/ : /^\d+$/).test(x);
		}

		if (!v1parts.every(isValidPart) || !v2parts.every(isValidPart)) {
			return NaN;
		}

		if (zeroExtend) {
			while (v1parts.length < v2parts.length) v1parts.push("0");
			while (v2parts.length < v1parts.length) v2parts.push("0");
		}

		if (!lexicographical) {
			v1parts = v1parts.map(Number);
			v2parts = v2parts.map(Number);
		}

		for (let i = 0; i < v1parts.length; ++i) {
			if (v2parts.length === i) {
				return 1;
			}

			if (v1parts[i] === v2parts[i]) {
				continue;
			}
			else if (v1parts[i] > v2parts[i]) {
				return 1;
			}
			else {
				return -1;
			}
		}

		if (v1parts.length !== v2parts.length) {
			return -1;
		}

		return 0;
	},

	EventListenerQueue: EventListenerQueue,
	Iterator: Iterator
}

//EVENT LISTENER QUEUE
function EventListenerQueue() {
	this.eventListeners = [];
}

EventListenerQueue.prototype.register = function(callback) {
	const self = this;

	if(self.eventListeners.indexOf(callback) === -1)
		self.eventListeners.push(callback);

	return function() {
		const removeFromArray = (array, items) => {
			items = [].concat(items);

			items.forEach((item) => {
				array.splice(array.indexOf(item), 1);
			});
		}
		removeFromArray(self.eventListeners, callback);
	};
};

EventListenerQueue.prototype.trigger = function() {
	const args = arguments;

	this.eventListeners.forEach((callback) => {
		callback.apply(null, args);
	});
};

EventListenerQueue.prototype.clear = function() {
	this.eventListeners = [];
};

//ITERATOR
function Iterator(collection) {
	this.collection = collection;
	this.currentIdx = -1;
}

Iterator.prototype.each = function(callback) {
	if(Array.isArray(this.collection))
		return this.collection.each(callback);

	for(const key in this.collection)
		if(this.collection.hasOwnProperty(key))
			callback(this.collection[key], key);
};

Iterator.prototype.some = function(callback) {
	if(Array.isArray(this.collection))
		return this.collection.some(callback);

	for(const key in this.collection)
		if(this.collection.hasOwnProperty(key))
			if(callback(this.collection[key], key))
				return true;

	return false;
};

Iterator.prototype.every = function(callback) {
	if(Array.isArray(this.collection))
		return this.collection.every(callback);

	for(const key in this.collection)
		if(this.collection.hasOwnProperty(key))
			if(!callback(this.collection[key], key))
				return false;

	return true;
};

Iterator.prototype.filter = function(callback) {
	if(Array.isArray(this.collection))
		return this.collection.filter(callback);

	const result = {};

	for(const key in this.collection)
		if(this.collection.hasOwnProperty(key))
			if(callback(this.collection[key], key))
				result[key] = this.collection[key];

	return result;
};

Iterator.prototype.map = function(callback) {
	if(Array.isArray(this.collection))
		return this.collection.map(callback);

	const result = [];

	for(const key in this.collection)
		if(this.collection.hasOwnProperty(key))
			result.push(callback(this.collection[key], key));

	return result;
};

Iterator.prototype.next = function() {
	this.currentIdx = Math.min(this.length-1, this.currentIdx+1);
	return this.get(this.currentIdx);
};

Iterator.prototype.prev = function() {
	this.currentIdx = Math.max(0, this.currentIdx-1);
	return this.get(this.currentIdx);
};

Iterator.prototype.get = function(idx) {
	if(Array.isArray(this.collection))
		return this.collection[idx];
	return this.collection[Object.keys(this.collection)[idx]];
};

Object.defineProperty(Iterator.prototype, "length", {
	get: function() {
		if(Array.isArray(this.collection))
			return this.collection.length;

		return Object.keys(this.filter(() => {
			return true;
		})).length;
	}
});

if (typeof module != 'undefined' && module.exports) module.exports = $o; // CommonJS, node.js
if (typeof define == 'function' && define.amd) define([], function() { return $o; }); // AMD