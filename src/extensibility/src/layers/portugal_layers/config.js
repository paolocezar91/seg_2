module.exports = {
	id: "portugal_layers",
	name: "Portugal",
	type: "Image",
	source: {
		name: "Portugal",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"Azores S&R Region",
				name: "SRR_Santa_Maria_Line"
			}
		]
	}
};
