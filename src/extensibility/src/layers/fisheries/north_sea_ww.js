module.exports = {
	id: "north_sea_ww",
	name: "North Sea-WW",
	type: "Image",
	source: {
		name: "North Sea-WW",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"Plaice Box",
				name: "plaice_box"
			},
			{
				alias:"Irish Sea Area 1",
				name: "56Irish_Sea_Area1"
			},
			{
				alias:"Irish Sea Area 2",
				name: "57Irish_Sea_Area2"
			},
			{
				alias:"Irish Sea Area 3",
				name: "58Irish_Sea_Area3"
			}
		]
	}

};

