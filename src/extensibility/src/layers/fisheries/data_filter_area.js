module.exports = {
	id: "data_filter_med",
	name: "Data Filter (Med)",
	type: "Image",
	source: {
		name: "Data Filter (Med)",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		params: {
			LAYERS: "BFT_data_filter_med",
			cql_filter: "INCLUDE"
		}
	}
};
