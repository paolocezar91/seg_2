module.exports = {
	id: "tuna_farms",
	name: "Tuna Farms",
	type: "Image",
	source: {
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		params: {
			LAYERS: "tunafarm"
		}
	},
	selectable: "html"
};
