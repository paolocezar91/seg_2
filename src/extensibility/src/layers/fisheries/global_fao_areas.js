module.exports = {
	id: "global_fao_areas",
	name: "Global FAO Areas",
	type: "Image",
	selectable: "html",

	source: {
		name: "Global FAO Areas",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"NAFO StatRect",
				name: "NAFO_StatRect"
			},
			{
				alias:"Fishing Areas FAO0",
				name: "FishingAreas_FAO0"
			},
			{
				alias:"Fishing Areas",
				name: "FishingAreas"
			}
		]
	}

};

