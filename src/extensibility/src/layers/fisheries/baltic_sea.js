module.exports = {
	id: "baltic_sea",
	name: "Baltic Sea",
	type: "Image",
	source: {
		name: "Baltic Sea",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"Bornholm Deep",
				name: "Bornholm_Deep"
			},
			{
				alias:"Gdansk Deep",
				name: "Gdanskdeep"
			},
			{
				alias:"Gotland Deep",
				name: "Gotlanddeep"
			},
			{
				alias:"Oder Bank",
				name: "Oderbank"
			}
		]
	}
};
