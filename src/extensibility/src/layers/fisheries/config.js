const FISHERIES_LAYERS = {
	general_fishery: require("./general_fishery/config.js"),
	global_fao_areas: require("./global_fao_areas.js"),
	data_filter_area: require("./data_filter_area.js"),
	rmfo_specific_areas: require("./rmfo_specific_areas.js"),
	NEAFC: require("./NEAFC.js"),
	north_sea_ww: require("./north_sea_ww.js"),
	ww_pelagic: require("./ww_pelagic.js"),
	baltic_sea: require("./baltic_sea.js"),
	NAFO: require("./NAFO.js"),
	tuna_farms: require("./tuna_farms.js")
};

module.exports = layers => ({
	id: "fisheries",
	name: "Fisheries",
	layers: layers.map(id => FISHERIES_LAYERS[id])
});