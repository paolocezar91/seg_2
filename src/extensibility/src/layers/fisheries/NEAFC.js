module.exports = {
	id: "neafc",
	name: "NEAFC",
	type: "Image",
	selectable: "html",
	source: {
		name: "NEAFC",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"ALTAIR",
				name: "23ALTAIR"
			},
			{
				alias:"ANTIALTAIR",
				name: "24ANTIALTAIR"
			},
			{
				alias:"BAR_1",
				name: "25BAR_1"
			},
			{
				alias:"BLUE_LING",
				name: "26BLUE_LING"
			},
			{
				alias:"EDORA_BANK",
				name: "27EDORA_BANK"
			},
			{
				alias:"HADDOCK_BOX",
				name: "28HADDOCK_BOX"
			},
			{
				alias:"HAR_1",
				name: "29HAR_1"
			},
			{
				alias:"HAR_2",
				name: "30HAR_2"
			},
			{
				alias:"HAR_3",
				name: "31HAR_3"
			},
			{
				alias:"HAR_4",
				name: "32HAR_4"
			},
			{
				alias:"HAR_5",
				name: "33HAR_5"
			},
			{
				alias:"HATTON_BANK",
				name: "34HATTON_BANK"
			},
			{
				alias:"LOGACHEV_MOUNDS",
				name: "36LOGACHEV_MOUNDS"
			},
			{
				alias:"MAR_1",
				name: "37MAR_1"
			},
			{
				alias:"MAR_2",
				name: "38MAR_2"
			},
			{
				alias:"MAR_3",
				name: "39MAR_3"
			},
			{
				alias:"MAR_4",
				name: "40MAR_4"
			},
			{
				alias:"MAR_5",
				name: "41MAR_5"
			},
			{
				alias:"MIDDLE_MAR",
				name: "42MIDDLE_MAR"
			},
			{
				alias:"NORTHERN_MAR",
				name: "43NORTHERN_MAR"
			},
			{
				alias:"NW_ROCKALL",
				name: "44NW_ROCKALL"
			},
			{
				alias:"REDFISH_FISHING_AREA_(RFA)",
				name: "45REDFISH_FISHING_AREA_(RFA)"
			},
			{
				alias:"MIDDLE_MAR",
				name: "46REYKJANES_RIDGE"
			},
			{
				alias:"SOUTHERN_MAR",
				name: "47SOUTHERN_MAR"
			},
			{
				alias:"SW_ROCKALL_1",
				name: "48SW_ROCKALL_1"
			},
			{
				alias:"SW_ROCKALL_2",
				name: "49SW_ROCKALL_2"
			},
			{
				alias:"SW_ROCKALL_3",
				name: "50SW_ROCKALL_3"
			},
			{
				alias:"WEST_ROCKALL",
				name: "51WEST_ROCKALL"
			}
		]
	}
};

