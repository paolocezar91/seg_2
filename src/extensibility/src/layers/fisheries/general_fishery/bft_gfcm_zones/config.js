module.exports = {
	id: "bft_gfcm_zones",
	name: "BFT GFCM Zones",
	type: "Image",
	selectable: "html",
	source: {
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		params: {
			LAYERS: "BFT_GFCM"
		}
	}
};
