module.exports = {
	id: "ices_areas",
	name: "Ices Areas",
	type: "Image",
	selectable: "html",
	source: {
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		params: {
			LAYERS: "NS_ICES_area"
		}
	}
};
