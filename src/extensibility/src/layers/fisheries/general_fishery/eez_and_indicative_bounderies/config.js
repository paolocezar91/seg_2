module.exports = {
	id: "eez_indicative_bounderies",
	name: "EEZ and Indicative Bounderies",
	type: "Image",
	selectable: "html",
	source: {
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		params: {
			LAYERS: "EEZ_BOUNDARIES_V10"
		}
	}
};
