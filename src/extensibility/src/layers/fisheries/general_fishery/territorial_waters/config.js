module.exports = {
	id: "territorial_waters",
	name: "Territorial Waters",
	type: "Image",
	selectable: "html",
	source: {
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		params: {
			LAYERS: "BFT_MaritBound_EUplus"
		}
	}
};
