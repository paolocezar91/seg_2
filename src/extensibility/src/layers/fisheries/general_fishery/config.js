module.exports = {
	id: "general_areas",
	name: "General Areas",
	layers: [
		require("./bft_gfcm_zones/config.js"),
		require("./eez_and_indicative_bounderies/config.js"),
		require("./territorial_waters/config.js"),
		require("./ices_areas/config.js")
	]
};

