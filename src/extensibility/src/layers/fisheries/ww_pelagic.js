module.exports = {
	id: "ww_pelagic",
	name: "WW Pelagic",
	type: "Image",
	source: {
		name: "WW Pelagic",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"Mackerel Box",
				name: "Mackerel_Box"
			}
		]
	}
};
