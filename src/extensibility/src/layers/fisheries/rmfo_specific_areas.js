module.exports = {
	id: "rmfo_specific_areas",
	name: "RMFO SPECIFIC AREAS",
	type: "Image",
	source: {
		name: "RMFO SPECIFIC AREAS",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"NAFO EEZ",
				name: "NAFO_EEZ_canada"
			},
			{
				alias:"54NEAFC NE",
				name: "52NEAFC_NE"
			},
			{
				alias:"54NEAFC SW",
				name: "54NEAFC_SW"
			},
			{
				alias:"54NEAFC NW",
				name: "54NEAFC_NW"
			}
		]
	}
};
