module.exports = {
	id: "nafo",
	name: "NAFO",
	type: "Image",
	selectable: "html",
	source: {
		name: "NAFO",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"Beothuk Knoll",
				name: "NAFOB_3_Beothuk_Knoll"
			},
			{
				alias:"Coral Closure",
				name: "2CORAL_CLOSURE"
			},
			{
				alias:"FOGO Seamounts 2",
				name: "3CORNER_SEAMOUNTS"
			},
			{
				alias:"Easter Flemish Cap",
				name: "NAFOB_4_Eastern_Flemish_Cap"
			},
			{
				alias:"Flemish Pass/Easter Canyon",
				name: "5Flemish_Pass_Eastern_Canyon"
			},
			{
				alias:"FOGO Seamounts 1",
				name: "6FOGO_SEAMOUNTS_1"
			},
			{
				alias:"FOGO Seamounts 2",
				name: "7FOGO_SEAMOUNTS_2"
			},
			{
				alias:"NAFO Footprint Map",
				name: "NAFO_footprint"
			},
			{
				alias:"Newfoundland Seamounts",
				name: "8NEWFOUNDLAND_SEAMOUNTS"
			},
			{
				alias:"New England Seamounts",
				name: "9NEW_ENGLAND_SEAMOUNTS"
			},
			{
				alias:"Northeast Flemish Cap",
				name: "10NORTHEAST_FLEMISH_CAP"
			},
			{
				alias:"Northeast Flemish Cap 1",
				name: "11Northern_Flemish_Cap_1"
			},
			{
				alias:"Northeast Flemish Cap 2",
				name: "12Northern_Flemish_Cap_2"
			},
			{
				alias:"Northeast Flemish Cap 3",
				name: "NAFOB_9_Northern_flemish_cap3"
			},
			{
				alias:"Northwest Flemish Cap 1",
				name: "14Northwest_Flemish_Cap_1"
			},
			{
				alias:"Northwest Flemish Cap 2",
				name: "NAFOB_11_Norwest_flemish_cap2"
			},
			{
				alias:"Northwest Flemish Cap 3",
				name: "16Northwest_Flemish_Cap_3"
			},
			{
				alias:"Orphan knoll",
				name: "17ORPHAN_KNOLL"
			},
			{
				alias:"Sackville Spur",
				name: "NAFOB_6_Sackville_Spur"
			},
			{
				alias:"Shrimp in Division 3L",
				name: "NAFOB_1_Shrimp_L"
			},
			{
				alias:"Tail of th Bank",
				name: "NAFOB_1_tail_of_the_bank"
			}
		]
	}

};


