module.exports =   {
	id: "italy_layers",
	name: "Italy",
	type: "Image",
	source: {
		name: "Italy",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"Fisheries Line of 3Nm",
				name: "Fisheries_Line_of_3Nm"
			},
			{
				alias:"Fisheries Bathymetry Line of 50M",
				name: "Fisheries_Bathymetry_Line_50M"
			}
		]
	}
};
