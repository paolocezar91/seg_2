module.exports = {
	id: "maritime_boundaries",
	name: "Maritime Boundaries",
	type: "Image",
	selectable: "html",
	source: {
		name: "Maritime Boundaries",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers:[
			{
				alias:"12 NM Zone",
				name: "12 NM Zone"
			},
			{
				alias:"Indicative EEZs",
				name: "EEZ_BOUNDARIES_V10"
			}
		]
	}
};