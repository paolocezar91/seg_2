module.exports = {
	id: "neafc_wms_layers",
	name: "NEAFC Areas",
	type: "Image",
	selectable: "html",
	source: {
		name: "NEAFC Areas WMS",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias: "NEAFC Convention Area",
				name: "EFCA_IMS:NEAFC_CONV_AREA_WGS1984_simpl"
			},
			{
				alias: "NEAFC New Bottom Fishing Area",
				name: "EFCA_IMS:NEAFC_FRA_FOR_SEG_WGS1984_BTM_FISH_NEW_ONLY"
			}
		]
	}
};
