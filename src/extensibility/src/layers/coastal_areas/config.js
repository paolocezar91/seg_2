module.exports = {
	id: "coastal_standing_orders",
	name: "LRIT",
	type: "Vector",
	source: {
		name: "Coastal Standing Orders",
		type: "Vector",
		loader() {
			return seg.request.get("v1/lrit/LIST", {
				params: {
					iso_code: "PRT"
				}
			}).then(res => {
				this.addFeatures((new ol.format.GeoJSON()).readFeatures(res.result[0].CustomCoastalAreas, {
					featureProjection: seg.map.getProjection()
				}));
			}, e => seg.log.error("ERROR_COASTAL_AREAS_LOADER", e.error + ": " + e.result));
		},
		projection: "EPSG:4326"
	},
	style(feature) {
		return new ol.style.Style({
			stroke: {
				color: feature.get("selected") ? "yellow" : "blue",
				width: 1
			},
			fill: "rgba(255,255,255,.1)"
		});
	},
	tooltip: {
		featureAs: "area",
		templateUrl: resolvePath("../../modules/areas/coastal_standing_orders.tooltip.html")
	}
};
