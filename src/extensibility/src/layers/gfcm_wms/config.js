module.exports = {
	id: "gfcm_wms_layers",
	name: "GFCM Areas",
	type: "Image",
	selectable: "html",
	source: {
		name: "GFCM Areas WMS",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias: "Deepwater Fisheries Management",
				name: "EFCA_IMS:GFCM_FRA_FOR_SEG_WGS1984_DEEPWATER_ONLY"
			}
		]
	}
};
