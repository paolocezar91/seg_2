module.exports = {
	id: "sweden_layers",
	name: "Sweden",
	type: "Image",
	source: {
		name: "Sweden",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"Bornholm Deep",
				name: "Bornholmsdjupet"
			},
			{
				alias:"Gdansk Deep",
				name: "Gdansk"
			},
			{
				alias:"Gotland Deep",
				name: "Gotlandsdjupet"
			}
		]
	}
};
