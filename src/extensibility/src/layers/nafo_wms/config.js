module.exports = {
	id: "nafo_wms_layers",
	name: "NAFO Areas",
	type: "Image",
	selectable: "html",
	source: {
		name: "NAFO Areas WMS",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias: "Shrimp in 3L",
				name: "EFCA_IMS:NAFO_OTHER_SHRIMP3L_WGS1984"
			},
			{
				alias: "Other Access Bottom Fishing Outside Existing Area",
				name: "EFCA_IMS:NAFO_FRA_FOR_SEG_WGS1984_BTM_UNFISHED_ONLY"
			}
		]
	}
};
