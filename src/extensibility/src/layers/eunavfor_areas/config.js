module.exports = [{
	name: "Eunavfor_IRTC",
	alias: "IRTC"
},{
	name: "Eunavfor_Fishing_zones",
	alias: "Fishing Zones"
},{
	name: "Eunafor_High_risk_area_pol",
	alias: "Voluntary Reporting Area (VRA)"
},{
	name: "patrol_area_final3",
	alias: "Patrol Areas"
},{
	name: "Area_of_operation",
	alias: "Area of Operation"
},{
	name: "Eunavfor_PORT_Gulf_Aden",
	alias: "Ports"
},{
	name: "Deg5_Graticule",
	alias: "Grid 5 Degrees"
}].map(config => {
	return {
		id: "eunavfor_"+config.alias,
		name: config.alias,
		type: "Image",
		source: {
			type: "ImageWMS",
			url: "v1/extension/wms/imdateGeoServer",
			projections: [
				"EPSG:4326",
				"EPSG:3395"
			],
			params: {
				LAYERS: config.name
			}
		}
	};
});