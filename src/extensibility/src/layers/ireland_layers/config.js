module.exports =   {
	id: "ireland_layers",
	name: "Ireland",
	type: "Image",
	source: {
		name: "Ireland",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"IRCG Administration",
				name: "IRCG_IRCG_Administration"
			},
			{
				alias:"MRCC & MRCS",
				name: "IRCG_MRCC_MRCS"
			},
			{
				alias:"CHC Helicopters",
				name: "IRCG_CHC_Helicopters"
			},
			{
				alias:"Pollution and Salvage",
				name: "IRCG_Pollution_and_Salvage"
			},
			{
				alias:"Coastal Teams",
				name: "IRCG_Coastal_Teams"
			},
			{
				alias:"Coastal Boat,Cliff Teams",
				name: "IRCG_Coastal_Boat_Cliff_Teams"
			},
			{
				alias:"Coastal, Inland Boat Teams",
				name: "IRCG_Coastal_Inland_Boat_Teams"
			},
			{
				alias:"Coastal Cliff Teams",
				name: "IRCG_Coastal_Cliff_Teams"
			},
			{
				alias:"CRBI",
				name: "IRCG_CRBI"
			}
		]
	},
	selectable: "html"
};
