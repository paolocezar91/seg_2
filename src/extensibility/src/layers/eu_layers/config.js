const layers = {
	territorial_waters: {
		alias:"Territorial Waters",
		name: "BFT_MaritBound_EUplus"
	},
	indicative_eez: {
		alias:"Indicative EEZs",
		name: "EEZ_BOUNDARIES_V10"
	}
};

const DEFAULT_LAYERS = Object.values(layers);

module.exports = config => ({
	id: "eu_layers",
	name: "EU",
	type: "Image",
	source: {
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: config.layers?config.layers.map(layer => layers[layer]):DEFAULT_LAYERS
	}
});