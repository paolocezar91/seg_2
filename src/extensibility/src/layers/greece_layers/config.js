module.exports = {
	id: "greece_layers",
	name: "Greece",
	type: "Image",
	source: {
		name: "Greece",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"FIR Athinai SRR",
				name: "Greece_SRR"
			}
		]
	}
};
