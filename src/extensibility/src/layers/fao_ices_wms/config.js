module.exports = {
	id: "fao_ices_layers",
	name: "FAO/ICES Areas",
	type: "Image",
	selectable: "html",
	source: {
		name: "FAO/ICES Areas WMS",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias: "Ices Areas",
				name: "NS_ICES_area"
			}
		]
	}
};
