module.exports = {
	id: "density_layers",
	name: "Density Map",
	type: "Image",
	source: {
		name: "Density",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias: "All Europe Dens 600DPI",
				name: "imdate:alleurope_dens_600dpi"
			}
		]
	}
};
