module.exports = {
	id: "malta_layers",
	name: "Malta",
	type: "Image",
	source: {
		name: "Malta",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"SRR Zone",
				name: "SRR"
			},
			{
				alias:"Fishing Zone A",
				name: "zoneA"
			},
			{
				alias:"Fishing Zone B",
				name: "zoneB"
			},
			{
				alias:"Fishing Zone E",
				name: "zoneE"
			},
			{
				alias:"Fishing Zone F",
				name: "zoneF"
			},
			{
				alias:"Fishing Zone G",
				name: "zoneG"
			},
			{
				alias:"Fishing Zone H",
				name: "zoneH"
			},
			{
				alias:"Fishing Zone I",
				name: "zoneI"
			},
			{
				alias:"Fishing Zone J",
				name: "zoneJ"
			},
			{
				alias:"Fishing Zone K",
				name: "zoneK"
			},
			{
				alias:"Fishing Zone L",
				name: "zoneL"
			},
			{
				alias:"Fishing Zone M",
				name: "zoneM"
			},
			{
				alias:"Fishing Zone N",
				name: "zoneN"
			},
			{
				alias:"BTF Aquaculture B",
				name: "BFT_aquaculture_B"
			}

		]
	}
};
