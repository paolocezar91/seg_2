module.exports = {
	id: "base_stations",
	name: "AIS Coastal Stations",
	type: "Vector",
	source: {
		type: "Vector",
		projection: "EPSG:4326"
	},
	style: require("./style.js"),
	commandInfo: {
		featureAs: "station",
		bindings: {
			baseStationCommandInfo: require("./scope.js")
		},
		//template url for this C&I
		templateUrl: resolvePath("./templates/command_info.html")
	},
	tooltip: {
		featureAs: "station",
		bindings: {
			baseStationTooltip: require("./scope.js")
		},
		templateUrl: resolvePath("./templates/tooltip.html")
	}
};
