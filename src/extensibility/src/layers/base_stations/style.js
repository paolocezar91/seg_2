const styleCache = {},
	selectedStyleCache = {};

module.exports = station => {
	const styles = [];
	let height = Math.round(4400000 / seg.map.getScale()) * 12;

	//size adjustements to scale
	if (height > 15)
		height = 20;
	if (height < 1)
		height = 10;

	if(!styleCache[height])
		styleCache[height] = new ol.style.Style({
			image: new seg.style.SVG({
				svg: require("./base-station-icon.svg"),
				fill: "black",
				stroke: {
					color: "white",
					width: 1
				},
				height: height
			})
		});

	styles.push(styleCache[height]);

	if (station.get("selected")) {
		const radius = Math.min(40, Math.max(1, Math.round(8800000 / seg.map.getScale()) * 12)) / 2;

		if (!selectedStyleCache[radius])
			selectedStyleCache[radius] = new ol.style.Style({
				image: new ol.style.Circle({
					radius,
					stroke: new ol.style.Stroke({
						color: "#006ebc",
						width: 2
					})
				})
			});

		styles.push(selectedStyleCache[radius]);
	}

	return styles;
};
