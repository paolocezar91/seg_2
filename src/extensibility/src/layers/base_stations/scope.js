module.exports = {
	displayPreferences_: {
		bsid: {
			label: "AIS Aerial Location",
			template: "{{station.get('aisAerialLocation')||N/A}}",
			visible: true
		},
		latitude: {
			label: "Latitude",
			template: "<span coordinate=\"[station.get('latitude'), 'lat']\"></span>",
			visible: true
		},
		longitude: {
			label: "Longitude",
			template: "<span coordinate=\"[station.get('longitude'), 'lon']\"></span>",
			visible: true
		},
		gridRef: {
			label: "Grid Reference",
			template: "{{station.get('gridRef')||N/A}}",
			visible: true
		},
		heightAboveGroundLevel: {
			label: "Height above Ground Level",
			template: "{{station.get('heightAboveGroundLevel')||N/A}}",
			visible: true
		},
		directionOfAe: {
			label: "Direction of Ae",
			template: "{{station.get('directionOfAe')||N/A}}",
			visible: true
		},
		mmsi: {
			label: "MMSI",
			template: "{{station.get('mmsi')||N/A}}",
			visible: true
		}
	},
	displayTooltipPreferences_: {
		bsid: {
			label: "AIS Aerial Location",
			template: "{{station.get('aisAerialLocation')}}",
			visible: true
		},
		latitude: {
			label: "Latitude",
			template: "<span coordinate=\"{{station.get('latitude')}}\"></span>",
			visible: false
		},
		longitude: {
			label: "Longitude",
			template: "<coordinate ng-model=\"{{station.get('longitude')}}\"></coordinate>",
			visible: false
		},
		gridRef: {
			label: "Grid Reference",
			template: "{{station.get('gridRef')}}",
			visible: false
		},
		heightAboveGroundLevel: {
			label: "Height above Ground Level",
			template: "{{station.get('heightAboveGroundLevel')}}",
			visible: false
		},
		directionOfAe: {
			label: "Direction of Ae",
			template: "{{station.get('directionOfAe')}}",
			visible: false
		},
		mmsi: {
			label: "MMSI",
			template: "{{station.get('mmsi')}}",
			visible: true
		}
	},
	get displayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("baseStations.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayPreferences_[key])
				self.displayPreferences_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayPreferences_;
	},
	get displayPreferencesForDisplay() {
		const preferences = this.displayPreferences;
		const result = [];

		angular.forEach(preferences, (value) => {
			if (value.visible) {
				result.push(value);
			}
		});

		return result;
	},
	toggleDisplayPreference(key) {
		//we only want to save the "visible" property, the rest is always the same
		seg.preferences.workspace.set("baseStations.commandInfo.displayOptions." + key, !this.displayPreferences[key].visible);
	},
	get tooltipDisplayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("baseStations.tooltip.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayTooltipPreferences_[key]) {
				self.displayTooltipPreferences_[key].visible = savedPreferences[key];
			}
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayTooltipPreferences_;
	}
};
