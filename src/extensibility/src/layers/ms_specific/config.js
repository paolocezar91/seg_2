module.exports = config => {
	const layers = {
		eu_layers: require("../eu_layers/config.js")(config.eu_layers),
		ireland_layers: require("../ireland_layers/config.js"),
		italy_layers: require("../italy_layers/config.js"),
		malta_layers: require("../malta_layers/config.js"),
		netherlands_layers: require("../netherlands_layers/config.js"),
		portugal_layers: require("../portugal_layers/config.js"),
		sweden_layers: require("../sweden_layers/config.js"),
		uk_layers: require("../uk_layers/config.js"),
		greece_layers: require("../greece_layers/config.js")
	};

	return Object.keys(config).map(layer => layers[layer]);
};