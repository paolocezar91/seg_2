module.exports = {
	id: "iccat_wms_layers",
	name: "ICCAT Areas",
	type: "Image",
	selectable: "html",
	source: {
		name: "ICCAT Areas",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias: "ICCAT Convention Area",
				name: "EFCA_IMS:ICCAT_CONV_AREA_WGS1984_simpl"
			}
		]
	}
};
