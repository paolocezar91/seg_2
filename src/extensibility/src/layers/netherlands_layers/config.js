module.exports = {
	id: "netherlands_layers",
	name: "Netherlands",
	type: "Image",
	source: {
		name: "Netherlands",
		type: "ImageWMS",
		url: "v1/extension/wms/imdateGeoServer",
		projections: [
			"EPSG:4326",
			"EPSG:3395"
		],
		supportedLayers: [
			{
				alias:"Secure Area",
				name: "NL_secure_area"
			},
			{
				alias:"KWC Block D",
				name: "NL_KWC_block_D"
			},
			{
				alias:"KWC Block E",
				name: "NL_KWC_block_E"
			}
		]
	}
};
