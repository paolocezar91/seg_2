/**@namespace extensibility.permissions
@description
Here are defined the avalaible permission roles used to configure the extensibility modules.
They are filtered based on users permissions that are received from SEG database.

These permissions configure some functionalities that can be available for the user on SEG interface.
*/

/**
@typedef extensibility.permissions.permissionsOptions
@prop {object} lrit - LRIT permissions
@prop {object} sarsurpic - SAR_SURPIC permissions
@prop {object} oilspills - Oilspills permissions
@prop {object} vessels - Vessels permissions
*/

//ROLES
const
	// v1 roles
	CSNDC_UP09_CSNCSADMIN                   	= "CSNDC UP09 csncsadmin",
	CSNDC_UP09_CSNCSOPERATIONAL             	= "CSNDC UP09 csncsoperational",
	CSNDC_UP09_CSNCSUSERGROUP               	= "CSNDC UP09 csncsusergroup",
	CSNDC_UP09_CSNCSUSER                    	= "CSNDC UP09 csncsuser",
	CSNDC_UP09_CSNCSPLANNING                	= "CSNDC UP09 csncsplanning",
	CSNDC_UP09_CSNCSCOMMONSERVICEDESK       	= "CSNDC UP09 Commonservicedesk",
	CSNDC_UP05_CSNAUTHOFFICER					= "CSNDC UP05 csnauthofficer",
	CSNDC_UP06_CSNCSADMIN						= "CSNDC UP06 csncsadmin",
	CSNDC_UP07_CSNCSOPERATIONAL					= "CSNDC UP07 csncsoperational",
	CSNDC_UP08_CSNUSERGROUP 					= "CSNDC UP08 csnusergroup",
	CSNDC_UP10_CSNCSPLANNING					= "CSNDC UP10 csncsplanning",
	CSNDC_UP20_CSNALERTCONFIG					= "CSNDC UP20 csnalertconfig",
	CSNDC_UP21_CSNCOMMONSERVICEDESK				= "CSNDC UP21 csncommonservicedesk",
	CSNDC_UP21_CSNDCCOMMONSERVICEDESK			= "CSNDC UP21 csndccommonservicedesk",
	LRITDC_ALL                              	= "LRITDC ALL",
	LRITDC_EU_DC_ADMINISTRATOR              	= "LRITDC EU DC Administrator",
	LRITDC_SAR                              	= "LRITDC SAR",
	LRITDC_SAR_COASTAL_CONSULTATION         	= "LRITDC SAR-Coastal Consultation",
	LRITDC_SAR_COASTAL_FULL                 	= "LRITDC SAR-Coastal Full",
	LRITDC_SAR_FLAG_CONSULTATION            	= "LRITDC SAR-Flag Consultation",
	LRITDC_SAR_FLAG_FULL                    	= "LRITDC SAR-Flag Full",
	LRITDC_SAR_FLAG_COASTAL_FULL            	= "LRITDC SAR-Flag-Coastal Full",
	LRITDC_SAR_FLAG_COASTAL_PORT_FULL       	= "LRITDC SAR-Flag-Coastal-Port Full",
	LRITDC_MSS_OPERATOR                     	= "LRITDC MSS Operator",
	LRITDC_NCA                              	= "LRITDC NCA",
	LRITDC_COASTAL_FULL                     	= "LRITDC Coastal Full",
	LRITDC_COASTAL_FULL_FLAG_CONSULTATION   	= "LRITDC Coastal Full-Flag Consultation",
	LRITDC_COASTAL_FLAG_PORT_FULL           	= "LRITDC Coastal-Flag-Port Full",
	LRITDC_FLAG_FULL                        	= "LRITDC Flag Full",
	LRITDC_PORT_FULL                        	= "LRITDC Port Full",
	IMDATE_VIEW_VOYAGES							= "IMDatE VIEW_VOYAGES",
	IMDATE_SSN_REQUESTOR						= "IMDatE SSN REQUESTOR",
	// v2 roles
	ROL_CSNDC_UP09 								= "ROL_CSNDC_UP09", // CSNDC_UP09_CSNCSADMIN, CSNDC_UP09_CSNCSOPERATIONAL, CSNDC_UP09_CSNCSUSERGROUP, CSNDC_UP09_CSNCSUSER, CSNDC_UP09_CSNCSPLANNING, CSNDC_UP09_CSNCSCOMMONSERVICEDESK
	ROL_CSNDC_UP05 								= "ROL_CSNDC_UP05", // CSNDC_UP05_CSNAUTHOFFICER
	ROL_CSNDC_UP06 								= "ROL_CSNDC_UP06", // CSNDC_UP06_CSNCSADMIN
	ROL_CSNDC_UP07 								= "ROL_CSNDC_UP07", // CSNDC_UP07_CSNCSOPERATIONAL
	ROL_CSNDC_UP10 								= "ROL_CSNDC_UP10", // CSNDC_UP10_CSNCSPLANNING
	ROL_CSNDC_UP20 								= "ROL_CSNDC_UP20", // CSNDC_UP20_CSNALERTCONFIG
	ROL_CSNDC_UP21 								= "ROL_CSNDC_UP21", // CSNDC_UP21_CSNCOMMONSERVICEDESK, CSNDC_UP21_CSNDCCOMMONSERVICEDESK
	ROL_LRITDC_ALL 								= "ROL_LRITDC_ALL", // LRITDC_ALL
	ROL_LRITDC_ADMIN 							= "ROL_LRITDC_ADMIN", // LRITDC_EU_DC_ADMINISTRATOR
	ROL_LRITDC_SAR 								= "ROL_LRITDC_SAR", // LRITDC_SAR
	ROL_LRITDC_SAR_COASTAL_CONSULTATION 		= "ROL_LRITDC_SAR_COASTAL_CONSULTATION", // LRITDC_SAR_COASTAL_CONSULTATION
	ROL_LRITDC_SAR_COASTAL_FULL 				= "ROL_LRITDC_SAR_COASTAL_FULL", // LRITDC_SAR_COASTAL_FULL
	ROL_LRITDC_SAR_FLAG_CONSULTATION 			= "ROL_LRITDC_SAR_FLAG_CONSULTATION", // LRITDC_SAR_FLAG_CONSULTATION
	ROL_LRITDC_SAR_FLAG_FULL 					= "ROL_LRITDC_SAR_FLAG_FULL", // LRITDC_SAR_FLAG_FULL
	ROL_LRITDC_SAR_FLAG_COASTAL_FULL 			= "ROL_LRITDC_SAR_FLAG_COASTAL_FULL", // LRITDC_SAR_FLAG_COASTAL_FULL
	ROL_LRITDC_SAR_FLAG_COASTAL_PORT_FULL 		= "ROL_LRITDC_SAR_FLAG_COASTAL_PORT_FULL", // LRITDC_SAR_FLAG_COASTAL_PORT_FULL
	ROL_LRITDC_MSS 								= "ROL_LRITDC_MSS", // LRITDC_MSS_OPERATOR
	ROL_LRITDC_NCA 								= "ROL_LRITDC_NCA", // LRITDC_NCA
	ROL_LRITDC_COASTAL_FULL 					= "ROL_LRITDC_COASTAL_FULL", //LRITDC_COASTAL_FULL
	ROL_LRITDC_COASTAL_FULL_FLAG_CONSULTATION 	= "ROL_LRITDC_COASTAL_FULL_FLAG_CONSULTATION", // LRITDC_COASTAL_FULL_FLAG_CONSULTATION
	ROL_LRITDC_COASTAL_FLAG_PORT_FULL 			= "ROL_LRITDC_COASTAL_FLAG_PORT_FULL", // LRITDC_COASTAL_FLAG_PORT_FULL
	ROL_LRITDC_FLAG_FULL 						= "ROL_LRITDC_FLAG_FULL", // LRITDC_FLAG_FULL
	ROL_LRITDC_PORT_FULL 						= "ROL_LRITDC_PORT_FULL", // LRITDC_PORT_FULL
	// ROL_IMDATE_BASIC_VIEWER 					= "ROL_IMDATE_BASIC_VIEWER",
	ROL_IMDATE_VIEW_VOYAGES 					= "ROL_IMDATE_VIEW_VOYAGES", // IMDATE_VIEW_VOYAGES
	ROL_IMDATE_SSN_REQUESTOR					= "ROL_IMDATE_SSN REQUESTOR",
	ROL_MANAGE_ALL_ABM							= "ROL_MANAGE_ALL_ABM",
	ROL_MANAGE_ALL_DIST_LIST					= "ROL_MANAGE_ALL_DIST_LIST",
	ROL_MANAGE_ABM								= "ROL_MANAGE_ABM",
	ROL_MANAGE_DIST_LIST						= "ROL_MANAGE_DIST_LIST",
	ROL_VDS_REPROCESSING						= "ROL_VDS_REPROCESSING";


//SAR SURPIC
const arraySARSUPIC = [
	// v1
	LRITDC_ALL,
	LRITDC_EU_DC_ADMINISTRATOR,
	LRITDC_SAR,
	LRITDC_SAR_COASTAL_CONSULTATION,
	LRITDC_SAR_COASTAL_FULL,
	LRITDC_SAR_FLAG_CONSULTATION,
	LRITDC_SAR_FLAG_FULL,
	LRITDC_SAR_FLAG_COASTAL_FULL,
	LRITDC_SAR_FLAG_COASTAL_PORT_FULL,
	LRITDC_MSS_OPERATOR,
	LRITDC_NCA,
	// v2
	ROL_LRITDC_ALL,
	ROL_LRITDC_ADMIN,
	ROL_LRITDC_SAR,
	ROL_LRITDC_SAR_COASTAL_CONSULTATION,
	ROL_LRITDC_SAR_COASTAL_FULL,
	ROL_LRITDC_SAR_FLAG_CONSULTATION,
	ROL_LRITDC_SAR_FLAG_FULL,
	ROL_LRITDC_SAR_FLAG_COASTAL_FULL,
	ROL_LRITDC_SAR_FLAG_COASTAL_PORT_FULL,
	ROL_LRITDC_MSS,
	ROL_LRITDC_NCA
];

//LRIT
const arrayLRIT = [
	// v1
	LRITDC_ALL,
	LRITDC_EU_DC_ADMINISTRATOR,
	LRITDC_SAR,
	LRITDC_SAR_COASTAL_CONSULTATION,
	LRITDC_SAR_COASTAL_FULL,
	LRITDC_SAR_FLAG_CONSULTATION,
	LRITDC_SAR_FLAG_FULL,
	LRITDC_SAR_FLAG_COASTAL_FULL,
	LRITDC_SAR_FLAG_COASTAL_PORT_FULL,
	LRITDC_COASTAL_FULL,
	LRITDC_COASTAL_FULL_FLAG_CONSULTATION,
	LRITDC_COASTAL_FLAG_PORT_FULL,
	LRITDC_FLAG_FULL,
	LRITDC_MSS_OPERATOR,
	LRITDC_NCA,
	LRITDC_PORT_FULL,
	// v2
	ROL_LRITDC_ALL,
	ROL_LRITDC_ADMIN,
	ROL_LRITDC_SAR,
	ROL_LRITDC_SAR_COASTAL_CONSULTATION,
	ROL_LRITDC_SAR_COASTAL_FULL,
	ROL_LRITDC_SAR_FLAG_CONSULTATION,
	ROL_LRITDC_SAR_FLAG_FULL,
	ROL_LRITDC_SAR_FLAG_COASTAL_FULL,
	ROL_LRITDC_SAR_FLAG_COASTAL_PORT_FULL,
	ROL_LRITDC_COASTAL_FULL,
	ROL_LRITDC_COASTAL_FULL_FLAG_CONSULTATION,
	ROL_LRITDC_COASTAL_FLAG_PORT_FULL,
	ROL_LRITDC_FLAG_FULL,
	ROL_LRITDC_MSS,
	ROL_LRITDC_NCA,
	ROL_LRITDC_PORT_FULL
];

//ACCESS TYPE - PORT
const arrayAccessTypePort = [
	// v1
	LRITDC_ALL,
	LRITDC_EU_DC_ADMINISTRATOR,
	LRITDC_SAR_FLAG_COASTAL_PORT_FULL,
	LRITDC_COASTAL_FLAG_PORT_FULL,
	LRITDC_MSS_OPERATOR,
	LRITDC_NCA,
	LRITDC_PORT_FULL,
	// v2
	ROL_LRITDC_ALL,
	ROL_LRITDC_ADMIN,
	ROL_LRITDC_SAR_FLAG_COASTAL_PORT_FULL,
	ROL_LRITDC_COASTAL_FLAG_PORT_FULL,
	ROL_LRITDC_MSS,
	ROL_LRITDC_NCA,
	ROL_LRITDC_PORT_FULL
];

//ACCESS TYPE - COASTAL
const arrayAccessTypeCoastal = [
	// v1
	LRITDC_ALL,
	LRITDC_EU_DC_ADMINISTRATOR,
	LRITDC_SAR_COASTAL_FULL,
	LRITDC_SAR_FLAG_COASTAL_FULL,
	LRITDC_SAR_FLAG_COASTAL_PORT_FULL,
	LRITDC_COASTAL_FULL,
	LRITDC_COASTAL_FULL_FLAG_CONSULTATION,
	LRITDC_COASTAL_FLAG_PORT_FULL,
	LRITDC_MSS_OPERATOR,
	LRITDC_NCA,
	//v2
	ROL_LRITDC_ALL,
	ROL_LRITDC_ADMIN,
	ROL_LRITDC_SAR_COASTAL_FULL,
	ROL_LRITDC_SAR_FLAG_COASTAL_FULL,
	ROL_LRITDC_SAR_FLAG_COASTAL_PORT_FULL,
	ROL_LRITDC_COASTAL_FULL,
	ROL_LRITDC_COASTAL_FULL_FLAG_CONSULTATION,
	ROL_LRITDC_COASTAL_FLAG_PORT_FULL,
	ROL_LRITDC_MSS,
	ROL_LRITDC_NCA
];

//ACCESS TYPE - FLAG
const arrayAccessTypeFlag = [
	// v1
	LRITDC_ALL,
	LRITDC_EU_DC_ADMINISTRATOR,
	LRITDC_SAR_FLAG_FULL,
	LRITDC_SAR_FLAG_COASTAL_FULL,
	LRITDC_SAR_FLAG_COASTAL_PORT_FULL,
	LRITDC_COASTAL_FLAG_PORT_FULL,
	LRITDC_FLAG_FULL,
	LRITDC_MSS_OPERATOR,
	LRITDC_NCA,
	// v2
	ROL_LRITDC_ALL,
	ROL_LRITDC_ADMIN,
	ROL_LRITDC_SAR_FLAG_FULL,
	ROL_LRITDC_SAR_FLAG_COASTAL_FULL,
	ROL_LRITDC_SAR_FLAG_COASTAL_PORT_FULL,
	ROL_LRITDC_COASTAL_FLAG_PORT_FULL,
	ROL_LRITDC_FLAG_FULL,
	ROL_LRITDC_MSS,
	ROL_LRITDC_NCA
];

//ACCESS TYPE - SAR
const arrayAccessTypeSAR = [
	// v1
	LRITDC_ALL,
	LRITDC_EU_DC_ADMINISTRATOR,
	LRITDC_SAR,
	LRITDC_SAR_COASTAL_FULL,
	LRITDC_SAR_FLAG_FULL,
	LRITDC_SAR_FLAG_COASTAL_FULL,
	LRITDC_SAR_FLAG_COASTAL_PORT_FULL,
	LRITDC_MSS_OPERATOR,
	LRITDC_NCA,
	// v2
	ROL_LRITDC_ALL,
	ROL_LRITDC_ADMIN,
	ROL_LRITDC_SAR,
	ROL_LRITDC_SAR_COASTAL_FULL,
	ROL_LRITDC_SAR_FLAG_FULL,
	ROL_LRITDC_SAR_FLAG_COASTAL_FULL,
	ROL_LRITDC_SAR_FLAG_COASTAL_PORT_FULL,
	ROL_LRITDC_MSS,
	ROL_LRITDC_NCA
];

//OIL SPILL FEEDBACK
const arrayAccessOilSpillsFeedback = [
	// v1
	CSNDC_UP09_CSNCSADMIN,
	CSNDC_UP09_CSNCSOPERATIONAL,
	CSNDC_UP09_CSNCSUSERGROUP,
	CSNDC_UP09_CSNCSUSER,
	CSNDC_UP09_CSNCSPLANNING,
	CSNDC_UP09_CSNCSCOMMONSERVICEDESK,
	CSNDC_UP05_CSNAUTHOFFICER,
	CSNDC_UP06_CSNCSADMIN,
	CSNDC_UP07_CSNCSOPERATIONAL,
	CSNDC_UP08_CSNUSERGROUP,
	CSNDC_UP10_CSNCSPLANNING,
	CSNDC_UP20_CSNALERTCONFIG,
	CSNDC_UP21_CSNCOMMONSERVICEDESK,
	CSNDC_UP21_CSNDCCOMMONSERVICEDESK,
	// v2
	ROL_CSNDC_UP09,
	ROL_CSNDC_UP05,
	ROL_CSNDC_UP06,
	ROL_CSNDC_UP07,
	ROL_CSNDC_UP10,
	ROL_CSNDC_UP20,
	ROL_CSNDC_UP21
];

const hasPermission = (userRoles, neededRoles) => neededRoles.some(v => !!~userRoles.indexOf(v));

//Export objects
const lrit = roles => {
	const arrayRequestTypeOptionsBase = [{
		value: "OneTime", // "1"
		alias: "One time poll of ship",
		order: 1
	}, {
		value: "LATEST", // "9"
		alias: "Most recent position report",
		order: 9
	}, {
		value: "ARCHIVED", // "7"
		alias: "Archived data request",
		order: 10
	}];

	const arrayRequestTypeOptionsRemaining = [{
		value: "RESET", // "0"
		alias: "Restart/Reset",
		order: 0
	}, {
		value: "STOP", // "8"
		alias: "Stop / do not start sending position reports",
		order: 11
	}, {
		value: "M15", // "2"
		alias: "Positions every 15 min",
		order: 2
	}, {
		value: "M30", // "3"
		alias: "Positions every 30 min",
		order: 3
	}, {
		value: "H1", // "4"
		alias: "Positions every 1 hour",
		order: 4
	}, {
		value: "H3", // "5"
		alias: "Positions every 3 hours",
		order: 5
	}, {
		value: "H6", // "6"
		alias: "Positions every 6 hours",
		order: 6
	}, {
		value: "H12", // "10"
		alias: "Positions every 12 hours",
		order: 7
	}, {
		value: "H24", // "11"
		alias: "Positions every 24 hours",
		order: 8
	}];

	//Aux functions
	const getLRITaccessTypeOptions = roles => {
		const accessTypeOptions = [];

		if (hasPermission(roles, arrayAccessTypeCoastal))
			accessTypeOptions.push("Coastal");

		if (hasPermission(roles, arrayAccessTypeFlag))
			accessTypeOptions.push("Flag");

		if (hasPermission(roles, arrayAccessTypePort))
			accessTypeOptions.push("Port");

		if (hasPermission(roles, arrayAccessTypeSAR))
			accessTypeOptions.push("SAR");

		return accessTypeOptions;
	};

	return {
		available: hasPermission(roles, arrayLRIT),
		accessTypeOptions : getLRITaccessTypeOptions(roles),
		requestTypeOptionsBase : arrayRequestTypeOptionsBase.sort((a,b) => a.order - b.order),
		requestTypeOptionsFull : [...arrayRequestTypeOptionsBase, ...arrayRequestTypeOptionsRemaining].sort((a,b) => a.order - b.order)
	};
};

const sarsurpic = roles => ({
	available: hasPermission(roles, arraySARSUPIC)
});

const oilspills = roles => ({
	feedback: {
		available: hasPermission(roles, arrayAccessOilSpillsFeedback),
		canRaisePriority: hasPermission(roles, [
			CSNDC_UP21_CSNDCCOMMONSERVICEDESK,
			ROL_CSNDC_UP21
		]),
		canEditAll: hasPermission(roles, [
			CSNDC_UP21_CSNDCCOMMONSERVICEDESK,
			ROL_CSNDC_UP21
		]),
		canDeleteAll: hasPermission(roles, [
			CSNDC_UP21_CSNDCCOMMONSERVICEDESK,
			ROL_CSNDC_UP21
		]),
		canEditOwn: hasPermission(roles, [
			CSNDC_UP06_CSNCSADMIN,
			CSNDC_UP07_CSNCSOPERATIONAL,
			CSNDC_UP08_CSNUSERGROUP,
			CSNDC_UP09_CSNCSUSER,
			ROL_CSNDC_UP06,
			ROL_CSNDC_UP07,
			ROL_CSNDC_UP09
		])
	}
});

const abm_alerts = roles => ({
	canView: hasPermission(roles, [ROL_MANAGE_ALL_ABM, ROL_MANAGE_ALL_DIST_LIST, ROL_MANAGE_ABM, ROL_MANAGE_DIST_LIST]),
	allSurveillance: hasPermission(roles, [ROL_MANAGE_ALL_ABM]),
	allDistributionList: hasPermission(roles, [ROL_MANAGE_ALL_DIST_LIST]),
	ownSurveillance: hasPermission(roles, [ROL_MANAGE_ABM]),
	ownDistributionList: hasPermission(roles, [ROL_MANAGE_DIST_LIST])
});

const vessels = roles => ({
	ssn_enrichment: {
		// XXX pressupoe que para ver tudo tem de ter as duas permissoes VIEW_VOYAGES e SSN REQUESTOR.
		available: hasPermission(roles, [IMDATE_VIEW_VOYAGES, ROL_IMDATE_VIEW_VOYAGES]),// && hasPermission(["IMDatE SSN REQUESTOR"]),
		showmore: hasPermission(roles, [IMDATE_SSN_REQUESTOR, ROL_IMDATE_SSN_REQUESTOR])
	},
	ga_plan: hasPermission(roles, arraySARSUPIC)
});

const footprints = roles => ({
	vds_reprocessing: hasPermission(roles, [ROL_VDS_REPROCESSING])
});

/** @constructs extensibility.permissions.Permissions
@param {object} roles - roles defined on SEG database
@returns {extensibility.permissions.permissionsOptions} Extensibility permissions instance
*/
module.exports = roles => ({
	footprints: footprints(roles),
	lrit: lrit(roles),
	sarsurpic: sarsurpic(roles),
	oilspills: oilspills(roles),
	vessels: vessels(roles),
	abm_alerts: abm_alerts(roles)
});