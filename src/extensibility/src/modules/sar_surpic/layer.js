module.exports = {
	id: "sar_surpic",
	name: "SAR SURPIC",
	type: "Vector",
	source: {
		name: "SAR SURPIC",
		type: "Vector",
		projection: "EPSG:4326"
	},
	style: require("./style.js")
};