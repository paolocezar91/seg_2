/**
@namespace extensibility.modules.sar_surpic
@description
# SAR SURPIC

## Available options
None

## Global configurations and constants
None
*/
const {pastSarSurpicTable, activeSarSurpicsTable} = require("./api");

module.exports = (config, permissions) => {
	if(!permissions.sarsurpic.available)
		return;

	return {
		name: "SAR Surpic",
		layers: [
			require("./layer.js")
		],
		async init() {
			seg.map.addFilter("SURPIC_FILTER", feature => feature.get("isSURPICResult")); // Adding Surpic Filter
			seg.ui.queryPanel.add(require("./query.js")); // adding Surpic Query
			seg.ui.ttt.tables.register(pastSarSurpicTable);
			seg.ui.ttt.tables.register(activeSarSurpicsTable);
		}
	};
};