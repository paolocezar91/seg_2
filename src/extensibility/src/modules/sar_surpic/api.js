const {parseTrackReports, lazyLoader} = require("../vessels/api.js"),
	{mergeDuplicatesTrack} = require("../vessels/tracks/api.js"),
	addSurpicCleanable = () => {
		seg.ui.cleanables.add("Clear All Surpic", () => {
			seg.layer.get("sar_surpic").getSource().getFeatures().forEach(removeSarSurpicAndFeatures);
			toggleSURPICFilter([], false);

			for (let table, i = seg.ui.ttt.tables.openTables.length - 1; i >= 0; i--) {
				table = seg.ui.ttt.tables.openTables[i];
				if (table.label === "Active SAR SURPICs" ||
					table.label === "Past SAR SURPICs" ||
					table.id === "sar_surpic_log" ||
					table.id === "sar_surpic_vessels"
				)
					seg.ui.ttt.tables.close(table);
			}

			lazyLoader();
		});
	},
	removeSarSurpicAndFeatures = (sarSurpic) => {
		const surpicTracksFeatures = sarSurpic.get("surpicTracksFeatures");

		if (surpicTracksFeatures) {
			const {vesselTrackFeatures, vesselFeatures} = surpicTracksFeatures;

			if(vesselTrackFeatures)
				vesselTrackFeatures.forEach(trackFeature => {
					trackFeature = seg.layer.get("tracks").getSource().getFeatureById(trackFeature.getId());
					if(trackFeature){
						const surpic = trackFeature.get("surpic").filter(surpic => surpic !== sarSurpic.getId());
						if (surpic.length === 0)
							seg.layer.get("tracks").getSource().removeFeature(trackFeature);
						else
							trackFeature.set("surpic", surpic);
					}
				});

			if (vesselFeatures)
				vesselFeatures.forEach(vesselFeature => {
					vesselFeature = seg.layer.get("positions").getSource().getFeatureById(vesselFeature.getId());
					if(vesselFeature){
						const surpic = vesselFeature.get("surpic").filter(surpic => surpic !== sarSurpic.getId());
						if (surpic.length === 0)
							seg.layer.get("positions").getSource().removeFeature(vesselFeature);
						else
							vesselFeature.set("surpic", surpic);

					}
				});
		}

		if (seg.ui.ttt.tables.get("Past SAR SURPICs"))
			seg.ui.ttt.tables.get("Past SAR SURPICs").data = seg.ui.ttt.tables.get("Past SAR SURPICs").data.filter(pastSurpic => pastSurpic !== sarSurpic);

		if (seg.ui.ttt.tables.get("Active SAR SURPICs"))
			seg.ui.ttt.tables.get("Active SAR SURPICs").data = seg.ui.ttt.tables.get("Active SAR SURPICs").data.filter(activeSurpic => activeSurpic !== sarSurpic);

		if(seg.layer.get("sar_surpic").getSource().getFeatures().length)
			seg.layer.get("sar_surpic").getSource().removeFeature(sarSurpic);
	},
	getLastVesselPosition = vessel => {
		const vesselPositions = vessel.positions.features,
			lastPosition = vesselPositions[vesselPositions.length - 1].properties;

		return lastPosition;
	},
	getDistanceToCenterInMeters = (center, vessel) => {
		const {longitude, latitude} = getLastVesselPosition(vessel),
			distance = ol.sphere.getDistance([longitude, latitude], center);
		return Math.round(distance);
	},
	// mapping for surpic logs table
	openLogTable = surpic => {
		const data = surpic.get("logs").entries || surpic.get("journal").entries || [];
		return seg.ui.ttt.tables.open({
			label: "SAR SURPIC "+surpic.get("name")+" LOG",
			id: "sar_surpic_log",
			data,
			itemAs: "log",
			fields: {
				start: {
					label: "Time Stamp",
					template: "<span position-timestamp='log.ts'></span>",
					visible: true
				},
				stop: {
					label: "Message",
					template: "{{log.msg}}",
					visible: true
				}
			}
		});
	},
	// mapping for surpic vessels table
	openVesselsTable = surpic => {
		const center = ol.proj.toLonLat(ol.extent.getCenter(surpic.getGeometry().getExtent()), seg.map.getProjection()),
			data = surpic.get("vessels_ref") || surpic.get("positions") || [],
			positionsSource = seg.layer.get("positions").getSource();

		seg.ui.ttt.tables.open({
			label: "SURPIC "+surpic.get("name")+" Vessels",
			id: "sar_surpic_vessels",
			customToolbar: {
				template: "<span class=\"text-size-sm\">Number of Vessels: <b>{{grid.filteredResults.length}}</b></span>"
			},
			data,
			itemAs: "vessel",
			gridAPI:{
				getDistanceToCenterInMeters(vessel) {
					return getDistanceToCenterInMeters(center, vessel);
				},
				getETAinMinutes(vessel) {
					const distance = getDistanceToCenterInMeters(center, vessel),
						distanceInNM = seg.utils.convertFromMeters(distance, "nm"),
						{speed} = getLastVesselPosition(vessel);

					return Math.round(distanceInNM/speed/60);
				},
				getLastVesselPosition,
				getDistanceUnits:() => seg.preferences.workspace.get("distanceUnits")
			},
			fields: {
				distance: {
					label: "Distance",
					template: "<span>{{(getDistanceToCenterInMeters(vessel) | distance:'m' | number)}} {{getDistanceUnits().toUpperCase()}}</span>",
					visible: true
				},
				eta: {
					label: "ETA",
					template: "{{getETAinMinutes(vessel)?((getETAinMinutes(vessel) | number) + ' min'): 'N/A'}}",
					visible: true
				},
				timestamp: {
					label: "Timestamp",
					template: "<span position-timestamp=\"getLastVesselPosition(vessel).timestamp\"></span>",
					visible: true
				},
				latitude: {
					label: "Latitude",
					template: "<span coordinate=\"[getLastVesselPosition(vessel).latitude, 'lat']\"></span>",
					visible: true
				},
				longitude: {
					label: "Longitude",
					template: "<span coordinate=\"[getLastVesselPosition(vessel).longitude, 'lon']\"></span>",
					visible: true
				},
				speed: {
					label: "Speed",
					template: "<span>{{getLastVesselPosition(vessel).speed | number}} KN</span>",
					visible: true
				},
				mmsi: {
					label: "MMSI",
					template: "{{vessel.shipParticulars.mmsi || NA}}",
					visible: true
				},
				emsaId: {
					label: "EMSAID",
					template: "{{vessel.shipParticulars.emsaId || NA}}",
					visible: true
				},
				cs: {
					label: "Call Sign",
					template: "{{vessel.shipParticulars.callSign || NA}}",
					visible: true
				},
				flag: {
					label: "Flag",
					template: "{{vessel.shipParticulars.flagState || NA}}",
					visible: true
				},
				name: {
					label: "Name",
					template: "{{vessel.shipParticulars.name || NA}}",
					visible: true
				},
				imo: {
					label: "IMO",
					template: "{{vessel.shipParticulars.imo || NA}}",
					visible: true
				}
			},
			onSelectionChange(changedRows, selectedRows) {
				seg.selection.selectFeatureAndCenter(positionsSource.getFeatureById(selectedRows[0].item.shipParticulars.emsaId));
			}
		});
	},
	// When a surpic is selected on TTT, opens the log table and the vessels table, and then centers it
	selectSarSurpic = surpic => {
		openLogTable(surpic);
		openVesselsTable(surpic);
		seg.selection.selectFeatureAndCenter(surpic);
	},
	transformTrackReportsForSource = trackReports => trackReports.map(({positions, shipParticulars}) => ({
		positions,
		track: {
			crs: positions.crs,
			geometry: {
				type: "LineString",
				coordinates: positions.features.map(position => position.geometry.coordinates)
			},
			type: "Feature"
		},
		particulars: {
			CallSign: shipParticulars.callSign,
			EMSAId: shipParticulars.emsaId,
			FlagState: shipParticulars.flagState,
			IMO: shipParticulars.imo,
			MMSI: shipParticulars.MMSI,
			Name: shipParticulars.name
		}
	})),
	toggleSURPICFilter = (features, val) => {
		seg.map[(val?"enable":"disable")+"Filter"]("SURPIC_FILTER");

		features.forEach(ft => {
			ft.set("isSURPICResult", val);
			ft.set("visible", val);
		});
	},
	// request specific surpic data based on id
	getSurpicData = surpicId => seg.request.get("v1/ship/sarsurpic/get_data?", {
		params: {
			surpicId,
			configRevision: 0,
			journalRevision: 0,
			trackRevision: 0
		}
	}).then(({result}) => {
		if(result) {
			result.config.id = surpicId;
			return result;
		}
		else seg.log.error("ERROR_SAR_SURPIC_GET_SURPIC_DATA", result.status + ": " + result.message);
	}, e => seg.log.error("ERROR_SAR_SURPIC_GET_SURPIC_DATA", e.error + ": " + e.result)),
	// Discover all surpic according to params (active or inactive)
	discoverSurpics = async (active) => {
		return (await seg.request.get("v1/ship/sarsurpic/discovery", {
			params: {
				active
			}
		})).result;
	},
	// get discovered surpics and request surpic specific data through getSurpicData
	buildAndFilterSurpicQuery = async active => (await seg.promise.all(
		(await discoverSurpics(active)).map(getSurpicData)
	)).filter(surpic => !!surpic),
	requestSurpics = async ({active}) => (await buildAndFilterSurpicQuery(active))
		.reduce((surpics, {config, journal, positions}) => {
			if (!(config && journal && positions))
				return surpics;
			else {
				config.properties = Object.assign(config.properties, {
					logs: journal,
					vessels_ref: positions,
					status: active ? "active" : "past"
				});

				const newSurpic = (new ol.format.GeoJSON()).readFeature(config, {
					dataProjection: "EPSG:4326",
					featureProjection: seg.map.getProjection()
				});

				newSurpic.on("change:selected", () => {
					if (newSurpic.get("selected"))
						seg.ui.cleanables.add("Selected SAR SURPIC",  () => {
							removeSarSurpicAndFeatures(newSurpic);
							if(!seg.layer.get("sar_surpic").getSource().getFeatures().length)
								seg.ui.cleanables.call("Clear All Surpic");
						});
					else {
						seg.ui.cleanables.remove("Candidate SAR SURPIC");
						seg.ui.cleanables.remove("Selected SAR SURPIC");
					}
				});

				return surpics.concat([newSurpic]);
			}
		}, []),
	parseSarSurpics = async (surpics) => {
		if (!surpics.length)
			return [];

		seg.layer.get("sar_surpic").getSource().clear();
		seg.layer.get("positions").getSource().clear();
		seg.layer.get("tracks").getSource().clear();

		// Parsing surpic data, such as tracks and vessel positions, geometry etc, then adding feature to source
		return seg.promise.all(surpics.map(async (surpic) => {
			let geometry;
			const positionsSource = seg.layer.get("positions").getSource(),
				area = surpic.get("area"),
				{trackReportsByVessel, vesselTrackFeatures} = await parseTrackReports(transformTrackReportsForSource(surpic.get("vessels_ref")), {simple: {enabled: true}}),
				vesselFeatures = Object.keys(trackReportsByVessel).map(vid => positionsSource.getFeatureById(vid));

			vesselTrackFeatures.forEach(vesselTrack => vesselTrack.set("surpic", [].concat(vesselTrack.get("surpic"), surpic.getId()).filter(s => s)));
			vesselFeatures.forEach(vessel => vessel.set("surpic", [].concat(vessel.get("surpic"), surpic.getId()).filter(s => s)));

			seg.layer.get("tracks").getSource().addFeatures(mergeDuplicatesTrack(vesselTrackFeatures));
			seg.layer.get("positions").getSource().addFeatures(vesselFeatures);

			if (area.type === "bbox"){
				let extent = ol.proj.transformExtent([area.minX, area.minY, area.maxX, area.maxY], seg.map.getProjection(), "EPSG:4326");
				extent = ol.proj.transformExtent(extent, "EPSG:4326", seg.map.getProjection());
				geometry = ol.geom.Polygon.fromExtent(ol.proj.transformExtent(extent, "EPSG:4326", seg.map.getProjection()));
			} else {
				const radius = area.radius,
					point = ol.proj.transform([area.lon, area.lat], "EPSG:4326", seg.map.getProjection()),
					circle = new ol.geom.Circle(point, radius);
				geometry = ol.geom.Polygon.fromCircle(circle);
			}
			surpic.setGeometry(geometry);
			surpic.set("surpicTracksFeatures", {
				vesselTrackFeatures,
				vesselFeatures
			});

			toggleSURPICFilter([...vesselTrackFeatures, ...vesselFeatures, surpic], true);
			seg.layer.get("sar_surpic").getSource().addFeature(surpic);

			return surpic;
		}));
	},
	activeSarSurpicsTable = (() => {
		const table = {
			label: "Active SAR SURPICs",
			customToolbar: {
				template: "<span class=\"text-size-sm\">Number of Active SAR SURPIC: <b>{{grid.filteredResults.length}}</b></span>"
			},
			itemAs: "sarSurpic",
			data: [],
			fields: require("./properties.js")({override: false}),
			onSelectionChange(changedRows, selectedRows) {
				if (!selectedRows.length)
					return;

				const surpics = selectedRows.map(row => row.item);

				parseSarSurpics(surpics).then(surpic => {
					if (surpic.length === 1)
						selectSarSurpic(surpic[0]);
					addSurpicCleanable();
				}, e => seg.log.error("ERROR_SAR_SURPIC_ACTIVE_ON_SELECTION_CHANGE_PARSE_SAR_SURPICS", e.error + ": " + e.result));

			},
			onOpen: async () => {
				const modal = seg.modal.openModal("SAR SURPIC", "Requesting Active SAR SURPIC...", {class: "info"});
				table.data = await requestSurpics({active: true});
				seg.modal.closeTimeout(modal.title);
			}
		};
		return table;
	})(),
	pastSarSurpicTable = (() => {
		const table = {
			label: "Past SAR SURPICs",
			customToolbar: {
				template: "<span class=\"text-size-sm\">Number of Past SAR SURPIC: <b>{{grid.filteredResults.length}}</b></span>"
			},
			itemAs: "sarSurpic",
			data: [],
			fields: require("./properties.js")({
				override: {
					name: false,
					start: false,
					stop: false,
					owner: false
				}
			}),
			onSelectionChange(changedRows, selectedRows) {
				if (!selectedRows.length)
					return;

				const surpics = selectedRows.map(row => row.item);
				// when surpics are selected, they are parsed and then added to source
				parseSarSurpics(surpics).then(() => {
					if (surpics.length === 1)
						selectSarSurpic(surpics[0]);
					addSurpicCleanable();
				}, e => seg.log.error("ERROR_SAR_SURPIC_PAST_ON_SELECTION_CHANGE_PARSE_SAR_SURPICS", e.error + ": " + e.result));
			},
			onOpen: async () => {
				// On open requests and populate table, but doesnt add sar surpic features to source yet
				const modal = seg.modal.openModal("SAR SURPIC", "Requesting Past SAR SURPIC...", {class: "info"});
				table.data = await requestSurpics({active: false});
				seg.modal.closeTimeout(modal.title);
			}
		};
		return table;
	})();

module.exports = {
	selectSarSurpic,
	pastSarSurpicTable,
	activeSarSurpicsTable,
	transformTrackReportsForSource,
	toggleSURPICFilter,
	requestSurpics
};
