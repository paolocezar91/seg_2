const lifebuoyStyle = new seg.style.SVG({
		svg: require("./sar-surpic.svg"),
		fill: "white",
		stroke: {
			color: "red",
			width: 1
		},
		height: 20
	}),
	areaStyleCache = {},
	styleCache = {};

module.exports = feature => {
	const selected = !!(feature.get("selected")),
		status = feature.get("status");

	let cacheKey = JSON.stringify({
		selected,
		status
	});

	//lifebuoy
	if(feature.get("vessels_ref")) {
		if(!areaStyleCache[cacheKey])
			areaStyleCache[cacheKey] = new ol.style.Style(Object.assign({
				stroke: new ol.style.Stroke({
					color: selected ? "yellow" : "blue",
					width: selected ? 3 : 2
				})
			}, status === "candidate" ? {
				fill: new ol.style.Fill({
					color: "rgba(255,0,0,.2)"
				})
			} : {}));

		return [
			new ol.style.Style({
				image: lifebuoyStyle,
				geometry: new ol.geom.Point(ol.extent.getCenter(feature.getGeometry().getExtent()))
			}),
			areaStyleCache[cacheKey]
		];
	}

	const rotation = feature.get("heading") * Math.PI / 180;
	cacheKey = JSON.stringify({selected, rotation});

	if(!styleCache[cacheKey])
		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG({
				svg: require("../vessels/positions/symbolizers/IALA/iala.svg"),
				fill: selected?"#090":"#0f0",
				stroke: {
					color: "#fff",
					width: 2
				},
				height: 20,
				rotation
			})
		});

	return styleCache[cacheKey];
};
