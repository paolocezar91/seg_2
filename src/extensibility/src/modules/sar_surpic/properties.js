const defaultProperties = {
	id: {
		label: "ID",
		template: "{{::sarSurpic.getId()}}",
		visible: true,
		order: 0
	},
	name: {
		label: "Name",
		template: "{{::sarSurpic.get('name')}}",
		visible: true,
		order: 1
	},
	start: {
		label: "Start Time",
		template: "<span position-timestamp=\"::sarSurpic.get('startTime')\"></span>",
		visible: true,
		order: 2
	},
	stop: {
		label: "End Time",
		template: "<span position-timestamp=\"::sarSurpic.get('modificationTime')\"></span>",
		visible: true,
		order: 3
	},
	owner: {
		label: "Owner",
		template: "{{::sarSurpic.get('owner')}}",
		visible: true,
		order: 4
	}
};

const mapProperties = (options) => seg.utils.mapItemProperties(defaultProperties, options);

module.exports = mapProperties;
