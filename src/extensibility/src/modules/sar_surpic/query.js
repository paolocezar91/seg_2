module.exports = {
	label: "SAR SURPIC",
	persistent: true,
	template: resolvePath("./templates/query.html"),
	scope: {
		sarsurpic: (() => {
			const sarSurpicSource = seg.layer.get("sar_surpic").getSource(),
				submit = () => {
					const candidate = seg.selection.getSelected("sar_surpic").find(sar_surpic => sar_surpic.get("status") === "candidate");
					if (candidate) {
						const loaderModal = seg.modal.openModal("SAR SURPIC", "Submiting SAR SURPIC...", {class: "info"}),
							params = {
								name: config.data.name,
								description: config.data.description,
								areaType: config.data.type,
								force: true
							};

						if (config.data.type === "bbox") {
							const bbox = ol.proj.transformExtent(config.data.bbox, seg.map.getProjection(), "EPSG:4326");
							Object.assign(params, {
								minX: bbox[0],
								maxX: bbox[2],
								minY: bbox[1],
								maxY: bbox[3]
							});
						} else {
							const [longitude, latitude] = config.data.coordinate;
							Object.assign(params, {
								longitude,
								latitude,
								radius: config.data.radius * 1852
							});
						}

						seg.request.get("v1/ship/sarsurpic/create?", {
							params
						}).then(
							() => {
								loaderModal.setContent("Completed SAR SURPIC Request");
								loaderModal.setClass("success");
								seg.modal.closeTimeout(loaderModal.title);
								seg.ui.cleanables.remove("Clear All Surpic");
								seg.ui.cleanables.remove("Candidate SAR SURPIC");
								seg.ui.ttt.tables.open("Active SAR SURPICs");
								config.lock = true;
							},
							e => {
								seg.log.error("ERROR_SAR_SURPIC_SUBMIT_CREATE", e.error + ": " + e.result);
								loaderModal.setContent(e.status === 403 ? "User is not allowed to access this resource." : "Request Failed");
								seg.modal.closeTimeout(loaderModal.title);
							}
						);

					}
				},
				canSubmit = () => {
					return isSelected() &&
						config.data.name !== "" &&
						config.data.description !== "" &&
						!config.lock &&
						(
							isValidBBOX() ||
							seg.selection.getSelected("sar_surpic")[0]
						);
				},
				isSelected = () => {
					return !!seg.selection.getSelected("sar_surpic").length && seg.selection.getSelected("sar_surpic")[0].get("status") === "candidate";
				},
				resetForm = () => {
					config.data = {
						id: null,
						type: options.type[1],
						name: "",
						description: "",
						width: options.width[0],
						height: options.height[0],
						radius: options.radius[2],
						coordinate: ol.proj.transform(seg.map.ol_.getView().getCenter(), seg.map.getProjection(), "EPSG:4326"),
						bbox: null
					};
					draw();
				},
				draw = () => {
					seg.ui.cleanables.call("Candidate SAR SURPIC");

					let bbox, geometry, candidate;
					const {type, width, height, radius, coordinate} = config.data,
						selected = seg.selection.getSelected("sar_surpic").find(sar_surpic => sar_surpic.get("status") === "candidate");

					if (type === "bbox") { // is bbox
						bbox = getBBox(coordinate[1], coordinate[0], width, height);
						geometry = ol.geom.Polygon.fromExtent(bbox);
					} else { // is circle
						const ratio = Math.cosh(coordinate[1]/6378137);

						bbox = getBBox(coordinate[1], coordinate[0], ratio * radius * 2, ratio * radius * 2);
						geometry = ol.geom.Polygon.circular(coordinate, radius * 1852).transform("EPSG:4326", seg.map.getProjection());
					}
					config.data.bbox = bbox;
					geometry.on("change", ({target}) => {
						const extent = ol.proj.transformExtent(target.getExtent(), seg.map.getProjection(), "EPSG:4326");
						config.data.coordinate = ol.extent.getCenter(extent);
					});

					// Creates a new feature if there is no sar_surpic feature selected
					if (!selected){
						candidate = new ol.Feature({
							radius,
							width,
							height,
							type,
							vessels_ref: [],
							status: "candidate",
							isSURPICResult: true
						});
						sarSurpicSource.addFeature(candidate);
						seg.selection.select(candidate);
						candidate.on("change:selected", (event) => {
							const target = event.target;
							if (target.get("selected")) {
								const extent = ol.proj.transformExtent(target.getGeometry().getExtent(), seg.map.getProjection(), "EPSG:4326");
								config.data = Object.assign({
									coordinate: ol.extent.getCenter(extent),
									radius: target.get("radius"),
									width: target.get("width"),
									height: target.get("height"),
									type: target.get("type")
								});
							}
						});
					} else // if there is a selected sar_surpic feature, it becomes the candidate
						candidate = selected;

					if (candidate.get("status") === "candidate") {
						candidate.setGeometry(geometry);
						// seg.map.centerOn(candidate);
						seg.ui.cleanables.add("Candidate SAR SURPIC",  () => {
							if (sarSurpicSource.getFeatures().length){
								sarSurpicSource.removeFeature(candidate);
							}
						});
					}
				},
				isValidBBOX = () => {
					if(config.data.type === "bbox" && config.data.bbox) {
						const bbox = ol.proj.transformExtent(config.data.bbox, seg.map.getProjection(), "EPSG:3395"),
							limit = seg.utils.convertToMeters(2000, "nm");

						return ol.extent.getWidth(bbox) <= limit && ol.extent.getHeight(bbox) <= limit;
					}

					return true;
				},
				getBBox = (lat, lon, width, height) => {
					width *= 1852;
					height *= 1852;
					//const point = new ol.geom.Point([lon,lat]);
					let point = ol.proj.transform([lon, lat], "EPSG:4326", "EPSG:3395");
					const minX = point[0] - width / 2;
					const maxX = point[0] + width / 2;
					const minY = point[1] - height / 2;
					const maxY = point[1] + height / 2;

					point = ol.proj.transform([minX, minY], "EPSG:3395", seg.map.getProjection());
					const point2 = ol.proj.transform([maxX, maxY], "EPSG:3395", seg.map.getProjection());
					return [point[0], point[1], point2[0], point2[1]];
				},
				options = {
					type: ["bbox", "circle"],
					width: [10, 20, 50, 100, 200, 300, 500, 1000, 2000],
					height: [10, 20, 50, 100, 200, 300, 500, 1000, 2000],
					radius: [10, 20, 50, 100, 200, 300, 500, 600, 700, 800, 999]
				},
				data = {
					id: null,
					type: options.type[1],
					name: "",
					description: "",
					width: options.width[0],
					height: options.height[0],
					radius: options.radius[2],
					coordinate: ol.proj.transform(seg.map.ol_.getView().getCenter(), seg.map.getProjection(), "EPSG:4326"),
					bbox: null
				},
				config = {
					options,
					data,
					canSubmit,
					isSelected,
					submit,
					resetForm,
					draw,
					lock: true,
					getUnits: () => seg.preferences.workspace.get("distanceUnits")
				};

			return config;
		})()
	}
};
