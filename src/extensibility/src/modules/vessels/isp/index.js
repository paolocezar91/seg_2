const {getTracks, getLabel} = require("../api.js");

module.exports = {
	label: "Get profile",
	template: resolvePath("./templates/isp.html"),
	scopeAs: "isp",
	scope: () => {
		const config = {};

		config.timePeriod = {
			from: moment().subtract(1, "d"),
			to: moment()
		};

		config.options = {
			tracks: true,
			hazmat: false,
			voyage: false,
			mrs: false
		};

		config.sources = "All";

		config.positionSources = require("../api.js").POSITION_SOURCES.reduce((sources, source) => {
			sources[source] = true;

			return sources;
		}, {});

		config.canSubmit = () => config.someOptionsEnabled() && config.validTrackOptions() && config.isVesselSelected() && config.isTimeValid();

		config.submit = () => {
			const trackOptions = {
					from: config.timePeriod.from,
					to: config.timePeriod.to,
					vessels: seg.selection.getSelected("positions"),
					isp: true
				},
				requests = [],
				vessel = trackOptions.vessels[0];

			if (config.options.hazmat || config.options.voyage || config.options.mrs)
				trackOptions.ssnEnrichment = true;

			if (config.sources === "Select...")
				trackOptions.source = Object.keys(config.positionSources).filter(source => config.positionSources[source]).join(",");

			const timelineConfig = {
				delimiters: [
					trackOptions.from,
					trackOptions.to
				],
				regions: [],
				groups: [],
				info: {
					templateUrl: resolvePath("./templates/timeline_info.html")
				}
			};

			const getTracksRequest = getTracks(trackOptions).then(track => {
				if (!track)
					return;

				// track.marker = {
				// 	template: "",
				// 	bindings: {
				// 		getColor(items) {
				// 			//XXX somehow config is being called without items the first time...
				// 			if(!items)
				// 				return;

				// 			switch(items[0].position.get("source")) {
				// 				case "T-AIS": return "linear-gradient(45deg, #45ff00 0%, #39b200 100%)";
				// 				case "Ship-AIS": return "linear-gradient(45deg, #39b200 0%, #266600 100%)";
				// 				case "Sat-AIS": return "linear-gradient(45deg, #266600 0%, #091600 100%)";
				// 				case "LRIT": return "linear-gradient(45deg, #ff00ff 0%, #79007f 100%)";
				// 				case "VDS": return "linear-gradient(45deg, #ff7bac 0%, #7f3e59 100%)";
				// 				case "Radar": return "linear-gradient(45deg, #ff931e 0%, #7f4811 100%)";
				// 				case "VMS": return "linear-gradient(135deg, #2e3192 0%, #1d205a 79%, #181c4c 100%)";
				// 			}
				// 		}
				// 	}
				// };

				timelineConfig.groups.push(track);
			}, e => seg.log.error("ERROR_VESSELS_GET_TRACKS_REQUEST", e.error + ": " + e.result));

			requests.push(getTracksRequest);

			if (config.options.voyage || config.options.hazmat) {
				let tableId, voyagesTable;
				const modal = seg.modal.openModal("Get ISP " + getLabel(vessel), "Retrieving profile for " + getLabel(vessel) +"...", {class: "info"}),
					params = {
						details: "GetAllReportedVoyagesOfSelectedShip",
						startTime: trackOptions.from.toISOString(),
						endTime: trackOptions.to.toISOString()
					};

				if (vessel.get("imo")) tableId = params.imoNumber = vessel.get("imo");
				else if (vessel.get("mmsi")) tableId = params.mmsiNumber = vessel.get("mmsi");
				else if (vessel.get("csid")) tableId = params.csdId = vessel.get("csid");

				const hazmatRequest = seg.request.get("v1/ship/ship_call", {
					params
				}).then(res => {
					//send to timeline the hazmats
					const {status, message} = res;

					if (!(status === "success" || status === "OK")) {
						modal.setContent(status + ": See console for details.");
						modal.setClass("no-result");

						seg.log.error("ERROR_ISP_HAZMAT_REQUEST", status + ": " + message);
					} else {
						let {result} = res;

						if (result.hasOwnProperty("Body"))
							result = result.Body;

						if (!result)
							return;

						const voyages = result.QueryResults.PortPlusNotificationList;

						if (voyages.length) {
							const voyagesFields = {
								portofcall: {
									label: "Port of Call",
									visible: true,
									template: "{{voyage.VoyageInformation.PortOfCall}}"
								},
								ETAToPortOfCall: {
									label: "ETA to Port Of Call",
									visible: true,
									template: `
									<span>
										<span ng-if="voyage.VoyageInformation.ETAToPortOfCall" position-timestamp="voyage.VoyageInformation.ETAToPortOfCall"></span>
										<span ng-if="!voyage.VoyageInformation.ETAToPortOfCall">N/A</span>
									</span>`
								},
								ATAPortOfCall: {
									label: "ATA to Port Of Call",
									visible: true,
									template: `
									<span>
										<span ng-if="voyage.VoyageInformation.ATAToPortOfCall" position-timestamp="voyage.VoyageInformation.ATAPortOfCall"></span>
										<span ng-if="!voyage.VoyageInformation.ATAToPortOfCall">N/A</span>
									</span>	`
								},
								ETDFromPortOfCall: {
									label: "ETD from Port Of Call",
									visible: true,
									template: `
									<span>
										<span ng-if="voyage.VoyageInformation.ETDFromPortOfCall" position-timestamp="voyage.VoyageInformation.ETDFromPortOfCall"></span>
										<span ng-if="!voyage.VoyageInformation.ETDFromPortOfCall">N/A</span>
									</span>`
								},
								ATDPortOfCall: {
									label: "ATD from Port Of Call",
									visible: true,
									template: `
									<span>
										<span ng-if="voyage.VoyageInformation.ATDFromPortOfCall" position-timestamp="voyage.VoyageInformation.ATDFromPortOfCall"></span>
										<span ng-if="!voyage.VoyageInformation.ATDFromPortOfCall">N/A</span>
									</span>`
								},
								lastport: {
									label: "Last Port",
									visible: true,
									template: "{{voyage.VoyageInformation.LastPort}}"
								},
								ETDFromLastPort: {
									label: "ETD from Last Port",
									visible: true,
									template: "<span position-timestamp=\"voyage.VoyageInformation.ETDFromLastPort\"></span>"
								},
								nextPort: {
									label: "Next Port",
									visible: true,
									template: "{{voyage.VoyageInformation.NextPort}}"
								},
								hazmat: {
									label: "Hazmat",
									visible: true,
									template: "{{voyage.HazmatConfirmation.HazmatOnBoardYorN}}"
								},
								pob: {
									label: "People on Board",
									visible: true,
									template: "{{voyage.PreArrival24HoursNotificationDetails.POBVoyageTowardsPortOfCall}}"
								}
							};

							voyagesTable = seg.ui.ttt.tables.openTables.find(table => table.label === "Voyages vessel #" + tableId);
							if (!voyagesTable)
								voyagesTable = seg.ui.ttt.tables.open({
									label: "Voyages vessel #" + tableId,
									customToolbar: {
										template: "<span class=\"text-size-sm\">Number of voyages: <b>{{grid.filteredResults.length}}</b></span>"
									},
									data: voyages,
									itemAs: "voyage",
									fields: voyagesFields
								});
							else
								voyagesTable.data = voyages;

							voyages.reverse();

							voyages.forEach((voyage) => {
								const timelineLine = {
									marker: {
										template: "<img ng-src=\"extensibility/modules/vessels/isp/{{items[0].type}}.png\">"
									},
									tooltip: {
										template: `
										<ul class="default-tooltip">
											<li><label>{{items[0].label}}</label></li>
											<li><label>{{items[0].port}}</label></li>
											<li><label position-timestamp="items[0].timestamp"></label></li>
										</ul>`
									},
									items: [],
									onTimeChange(/*timestamp, before*/) {
										// const currentPositionUniqueVessels = before.reverse() // reversing, so that sooner positions are first on listing
										// 	.filter((thing, index, self) => // filtering by vessel id of each position, therefore having the soonest position of each unique vessel
										// 		index === self.findIndex((t) => (
										// 			t.position.get("vessel").getId() === thing.position.get("vessel").getId()
										// 		))
										// 	)
										// 	.map(b => b.position); // mapping the position to use seg.selection
										// seg.selection.select(currentPositionUniqueVessels);
									}
								};

								// ETD From Port of Call (yellow circle)
								if (voyage.VoyageInformation) {
									//Last Port
									if (voyage.VoyageInformation.ETDFromLastPort)
										timelineLine.items.push({
											timestamp: moment(voyage.VoyageInformation.ETDFromLastPort),
											type: "etdlastport",
											label: "Estimated Time of Departure From Last Port",
											port: voyage.VoyageInformation.LastPort
										});
									if (voyage.VoyageInformation.ETAToLastPort)
										timelineLine.items.push({
											timestamp: moment(voyage.VoyageInformation.ETAToLastPort),
											type: "etalastport",
											label: "Estimated Time of Arrival to Last Port",
											port: voyage.VoyageInformation.LastPort
										});
									//Next Port
									if (voyage.VoyageInformation.ETDFromNextPort)
										timelineLine.items.push({
											timestamp: moment(voyage.VoyageInformation.ETDFromNextPort),
											type: "etdnextport",
											label: "Estimated Time of Departure From Next Port",
											port: voyage.VoyageInformation.NextPort
										});
									if (voyage.VoyageInformation.ETAToNextPort)
										timelineLine.items.push({
											timestamp: moment(voyage.VoyageInformation.ETAToNextPort),
											type: "etanextport",
											label: "Estimated Time of Arrival to Next Port",
											port: voyage.VoyageInformation.NextPort
										});
									//Port of Call
									if (voyage.VoyageInformation.ETAToPortOfCall)
										timelineLine.items.push({
											timestamp: moment(voyage.VoyageInformation.ETAToPortOfCall),
											type: "eta",
											label: "Estimated Time of Arrival to Port Of Call",
											port: voyage.VoyageInformation.PortOfCall
										});
									if (voyage.VoyageInformation.ETDFromPortOfCall)
										timelineLine.items.push({
											timestamp: moment(voyage.VoyageInformation.ETDFromPortOfCall),
											type: "etd",
											label: "Estimated Time of Departure From Port Of Call",
											port: voyage.VoyageInformation.PortOfCall
										});
								}

								if (voyage.DepartureNotificationDetails && voyage.DepartureNotificationDetails.ATDPortOfCall)
									timelineLine.items.push({
										timestamp: moment( voyage.DepartureNotificationDetails.ATDPortOfCall),
										type: "atd",
										label: "Actual Time of Departure From Port Of Call",
										port: voyage.VoyageInformation.PortOfCall
									});
								if (voyage.ArrivalNotificationDetails && voyage.ArrivalNotificationDetails.ATAPortOfCall)
									timelineLine.items.push({
										timestamp: moment(voyage.ArrivalNotificationDetails.ATAPortOfCall),
										type: "ata",
										label: "Actual Time of Arrival to Port Of Call",
										port: voyage.VoyageInformation.PortOfCall
									});
								if (config.options.hazmat && voyage.HazmatConfirmation && (voyage.HazmatConfirmation.HazmatOnBoardYorN === "Y"))
									timelineConfig.regions.push({
										end: moment(voyage.DepartureNotificationDetails?voyage.DepartureNotificationDetails.ATDPortOfCall:voyage.VoyageInformation.ETDFromPortOfCall),
										start: moment(voyage.ArrivalNotificationDetails?voyage.ArrivalNotificationDetails.ATAPortOfCall:voyage.VoyageInformation.ETAToPortOfCall),
										color: "rgba(190,0,0,0.2)"
									});
								timelineConfig.groups.push(timelineLine);
							});
						}
					}
					seg.modal.closeTimeout(modal.title);

					seg.ui.cleanables.add("Get Profiles", () => {
						seg.ui.cleanables.call("Tracks");
						seg.ui.ttt.tables.close(voyagesTable);
						seg.timeline.clear();
					});

				}, e => {seg.log.error("ERROR_ISP_HAZMAT_REQUEST", e.error + ": " + e.result); return {};}, () => {});

				modal.setDismissCallback(() => {
					hazmatRequest.cancel();
				});

				requests.push(hazmatRequest);
			}

			seg.promise.all(requests).then(() => {
				seg.timeline.set(timelineConfig), e => seg.log.error("ERROR_ISP_SUBMIT", e.error + ": " + e.result);
			});
		};

		config.isVesselSelected = () => seg.selection.getSelected("positions").length === 1;
		config.isTimeValid = () => config.timePeriod.from.isBefore(config.timePeriod.to);

		config.someOptionsEnabled = () =>  {
			return Object.values(config.options).some(option => option);
		};

		config.validTrackOptions = () => {
			return config.options.tracks && (
				config.sources === "All" || (
					config.sources === "Select..." &&
					Object.values(config.positionSources).some(source => source)
				)
			);
		};

		return config;
	}
};
