let vesselsLayer,
	positionsLayer,
	positionsSource,
	positionsSubscription,
	tracksSource,
	lastUpdate,
	refreshTimeout = [],
	trackTables = [],
	trackReportsByVessel = {},
	shouldRefresh = true,
	// previousZoomLevel = false, // CLUSTERS_REGRESSION
	MAX_TILES,
	REFRESH_TIME;

// PSC Ship Types enumeration, used in tooltip, TTT, C&I and other places
const PSC_SHIP_TYPES = {"Tankship+cc": 310, "NLS Tanker": 311, "Combination Carrier": 312, "Oil Tanker": 313, "Vegetank": 314, "Fishing Vessel": 315, "Warship And Naval Auxiliary": 316, "Wooden Ship Of Primitive Build": 317, "Governement Ship used for Non-commercial Purpose": 318, "Pleasure Yacht Not Engaged In Trade": 319, "Gas Carrier": 320, "Gas Carrier LPG": 321, "Gas carrier ING": 322, "Commercial yacht": 323, "Chemical Tanker": 330, "Bulk Carrier": 340, "Cement Carrier": 341, "Unit/vessel": 350, "Barge carrier": 351, "Vehicle Carrier": 352, "Container": 353, "Pallet Carrier": 354, "Ro-ro Cargo": 355, "General Cargo": 360, "Refrigerated Cargo": 361, "Livestock Carrier": 367, "Ro-ro Passenger Ship": 370, "Passenger Ship": 371, "Ice Breaker": 372, "Fish Factory": 373, "Research Ship": 374, "Heavy Load": 375, "Offshore Supply": 376, "Stand By Ship": 377, "Dredger": 378, "MODu & FPSO": 380, "Dyncraft": 381, "Special Purpose Ship": 382, "High Speed Passenger Craft": 383, "High Speed Cargo": 384, "Tug": 385, "Other Special Activities": 399},
	// Navigational Status enumeration, used in TTT, C&I and other places
	NAVIGATIONAL_STATUS = ["Under way using engine", "At anchor", "Not under command", "Restricted manoeuverability", "Constrained by her draught", "Moored", "Aground", "Engaged in Fishing", "Under way sailing"],
	// Possible position sources
	POSITION_SOURCES = ["T-AIS", "Ship-AIS", "Sat-AIS", "LRIT", "VDS", "Radar", "VMS", "MRS"],
	// Function for NAVIGATIONAL_STATUS, used on C&I on opening and other places
	getNavigationalStatus = i => NAVIGATIONAL_STATUS[i] || "Other",
	// Request done by C&I to get vessel dimensions, used on C&I on opening
	// If vessel models is toggled in config, and scale is small, draws a geometry for the vessel
	getDimensions = (vessel, direction) => {
		let res;

		if (direction === "length") {
			if (vessel.get("dimensions") && (vessel.get("dimensions").a + vessel.get("dimensions").b > 0))
				res = vessel.get("dimensions").a + vessel.get("dimensions").b;
			else if(vessel.get("lengthOverall") >= 0)
				res = vessel.get("lengthOverall");
		} else if (direction === "breadth") {
			if (vessel.get("dimensions") && (vessel.get("dimensions").c + vessel.get("dimensions").d > 0))
				res = vessel.get("dimensions").c + vessel.get("dimensions").d;
			else if(vessel.get("beam") >= 0)
				res = vessel.get("beam");
		}

		return res ? res  + " mts" : "N/A";
	},
	// Request for particulars of a vessel. Called by C&I on opening and when refreshPositions finds new vessels
	getParticulars = (vessel) => {
		if (vessel.get("particulars"))
			return;

		return seg.request.get("v1/ship/particulars/" + vessel.getId()).pipe(
			seg.observable.operators.map(({result}) => {
				Object.keys(result).forEach(key => vessel.set(key, result[key]));
				vessel.set("particulars", true);
			}),
			seg.observable.operators.catchError(e => seg.log.error("ERROR_VESSELS_GET_PARTICULARS", e.error + ": " + e.result))
		);
	},
	// Filters when we should update position values of a vessel
	shouldUpdatePosition = (vessel) => {
		return !vessel.get("surpic") && // SURPIC VESSELS SHOULD NOT BE UPDATED
			!vessel.get("isImported") &&
			!vessel.get("lastPositionSearched") && // VESSELS THAT WERE ALREADY SEARCHED SHOULD NOT BE SEARCHED AGAIN, EVEN IF TIMESTAMP IS OLD
			(
				typeof vessel.get("ts") === "undefined" || // IF NO TIMESTAMP IS SET
				(vessel.get("ts") && moment(vessel.get("ts")).isBefore(moment().subtract(1, "day"))) || // OR IF TIMESTAMP IS OLDER THAN 1 DAY, THEN SEARCH FOR LATEST POSITION
				typeof vessel.get("hdg") === "undefined" ||
				typeof vessel.get("src") === "undefined"
			);
	},
	// Event function to sync properties of a vessel to tracks of the same vessel
	// This is used to handle selection symbology of tracks and vessels
	// Used on getTracks functions
	syncVesselPropWithTracks = prop => ({target}) => {
		// Get
		const trackLine = tracksSource.getFeatures().find(ft => ft.get("isTrackLine") && (
				ft.get("vessel") === target ||
				ft.get("positions").find(position => position.getId() === target.getId())
			)),
			propValue = target.get(prop);

		if(trackLine) {
			trackLine.set(prop, propValue);
			trackLine.get("positions").forEach(pos => pos.set(prop, propValue));
		}
	},
	// Event function to sync properties of a track to its vessel
	// This is used to handle selection symbology of tracks and vessels
	// Used on getTracks functions
	syncTrackPropWithVessel = prop => ({target}) => {
		// target is either a track position or the trackline, they both have a reference to the vessel
		const vesselTracked = target.get("vessel"),
			propValue = target.get(prop);
		// setting the vesselTracked prop to the propValue will trigger syncVesselPropWithTracks and do for its tracks and trackline
		if(!vesselTracked.getListeners("change:"+prop))
			vesselTracked.on("change:"+prop, syncVesselPropWithTracks(prop));

		vesselTracked.set(prop, propValue);
	},
	// Merge vessels function, used in several places
	mergeVessels = (src, vessels, riskLevels) => vessels.map(vessel => {
		const setVesselDimensions = (vessel) => {
			const a = vessel.get("a") || 0,
				b = vessel.get("b") || 0,
				c = vessel.get("c") || 0,
				d = vessel.get("d") || 0;

			return {a, b, c, d};
		};
		// smart vessel search returns an identity atribute, which contains the id
		vessel.setProperties(Object.assign(vessel.getProperties(), vessel.get("identity")));
		vessel.unset("identity", true);
		vessel.set("dimensions", Object.assign(setVesselDimensions(vessel), vessel.get("dimensions")));

		const id = vessel.getId() || vessel.get("id"),
			existing = positionsSource.getFeatureById(id);

		//XXX shipType can be provided by both shipType and thetisTypeCode getting identity is no enough

		if ((typeof vessel.get("shipType")==="undefined" || vessel.get("shipType")==="") && vessel.get("thetisTypeCode"))
			vessel.set("shipType",vessel.get("thetisTypeCode"));


		if (existing) {
			const overridePropsAndGeometry = !existing.get("ts") ||
				moment(existing.get("ts")).isBefore(moment(vessel.get("ts"))) ||
				typeof existing.get("hdg") === "undefined" ||
				typeof existing.get("src") === "undefined";

			const overrideShipTypeWithThetis = (typeof existing.get("shipType") === "undefined" || existing.get("shipType") ==="")  && vessel.get("thetisTypeCode");

			// Regardless of timestamp all blank properties will be filled by default
			Object.keys(vessel.getProperties()).forEach((key) => {
				if((typeof existing.get(key) === "undefined" || existing.get(key) === "") && vessel.get(key) !== "")
					existing.set(key, vessel.get(key));
			});


			if (overridePropsAndGeometry){
				// There are some attributes that if they already exist, should not be updated when a new position is returned
				// So we create an object with the conditions that they should not be updated, and check if the existing vessel attributes fulfill those conditions
				// If it does, we set them on the new vessel attributes, so that they are not overriden on the last command

				const shouldNotOverride = {
					"sai": (sai) => !!(sai && sai !== ""),
					"enrichment": (enrichment) => !!enrichment,
					"shipType" : (shipType) => !!shipType
				};

				Object.entries(shouldNotOverride).forEach(([key, check]) => {
					if(check(existing.get(key)) && !angular.equals(existing.get(key), vessel.get(key)))
						vessel.set(key, existing.get(key));
				});

				existing.getGeometry().setCoordinates(vessel.getGeometry().getCoordinates());
				existing.setProperties(Object.assign(existing.getProperties(), vessel.getProperties()));
			}

			if (overrideShipTypeWithThetis){
				//In case thetisTypeCode is provided instead of shipType
				//This happens regardless of ts
				existing.set("shipType",vessel.get("thetisTypeCode"));
			}

			return existing;
		}

		if (typeof riskLevels !== "undefined" && id in riskLevels)
			vessel.set("risk", riskLevels[id], true);

		vessel.on("change:risk", ({target}) => {
			if (target.get("risk"))
				seg.preferences.workspace.set("vessels.positions.riskLevels." + target.getId(), target.get("risk"));
			else seg.preferences.workspace.unset("vessels.positions.riskLevels." + target.getId());
		});
		vessel.on("change:isSearchResult", syncVesselPropWithTracks("isSearchResult"));
		vessel.on("change:wasSelected", syncVesselPropWithTracks("wasSelected"));
		vessel.on("change:isVisibleInTTT", syncVesselPropWithTracks("isVisibleInTTT"));

		return vessel;
	}),
	// Merge duplicate vessels when new are requested
	mergeDuplicates = vessels => {
		const riskLevels = seg.preferences.workspace.get("vessels.positions.riskLevels"),
			chunks = [];

		for(let i=0; i<vessels.length; i+=seg.CONFIG.getValue("BATCH_SIZE"))
			chunks.push(vessels.slice(i, i+seg.CONFIG.getValue("BATCH_SIZE")));

		return [].concat(...chunks.map(vessels => mergeVessels(positionsSource, vessels, riskLevels)));
		// return seg.promise.all(proms);
		// 
		// 
		// 
	},
	// Retrieves the label of a vessel, either its shipName, imo, ir, mmsi...
	getLabel = vessel => {
		let label;
		if (vessel.get("shipName"))
			label = vessel.get("shipName");
		else if (vessel.get("imo"))
			label = "IMO: " + vessel.get("imo");
		else if (vessel.get("ir"))
			label = "IR: " + vessel.get("ir");
		else if (vessel.get("mmsi"))
			label = "MMSI: " + vessel.get("mmsi");
		else if (vessel.get("callSign"))
			label = "Call Sign: " + vessel.get("callSign");
		else if (vessel.get("favourite_name"))
			label = vessel.get("favourite_name");
		return label;
	},
	// Get Track functions: prepares timeline object for Tracks
	createTimelineFromTrackReports = trbv => ({
		items: [].concat(...Object.values(trbv).map(trackReport => trackReport.trackPositions.map(position => {
			return {
				timestamp: moment(position.get("timestamp")),
				position,
				vessel: trackReport.vessel
			};
		}))),
		onTimeChange(timestamp, before) {
			const currentPositionUniqueVessels = before.reverse() // reversing, so that sooner positions are first on listing
				.filter((thing, index, self) => // filtering by vessel id of each position, therefore having the soonest position of each unique vessel
					index === self.findIndex((t) => (
						t.position.get("vessel").getId() === thing.position.get("vessel").getId()
					))
				)
				.map(b => b.position); // mapping the position to use seg.selection
			
			if(currentPositionUniqueVessels.length)
				seg.selection.select(currentPositionUniqueVessels);
		}
	}),
	// Get Track functions: create table and cleanable actions for Tracks
	createTableAndCleanablesFromTrackReports = (parsedReports, options) => {
		const trbv = parsedReports.trackReportsByVessel,
			modal = seg.modal.openLoader("Get Tracks"); // getting instance of modal
		modal.setPartialProgress("tracks", 100); // set modal progress

		const numberOfTrackReports = Object.keys(trbv).length;
		//if no track reports were retrieved, return
		if (!numberOfTrackReports) {
			modal.setContent("Retrieved <span class=\"retrieved-no-result\"><b>" + numberOfTrackReports + "</b> track reports</span>");
			modal.setClass("no-result");
			seg.modal.closeTimeout(modal.title);
			return trbv;
		}

		const retrievedVessels = Object.keys(trbv).map(vesselId => trbv[vesselId].vessel);
		const trackTTTFields = require("./tracks/properties").mapProperties({
				override: {
					shipName: false,
					mmsi: false,
					imo: false,
					ir: false,
					callSign: false,
					timestamp: false,
					latitude: false,
					longitude: false,
					heading: false,
					speed: false,
					source: false
				}
			}),
			vesselTTTFields = require("./positions/properties").mapProperties({
				override: {
					mmsi: {visible: true},
					imo: {visible: true},
					ir: {visible: true},
					shipName: {visible: true},
					callSign: {visible: true},
					speed: {visible: true},
					lat: {visible: true},
					lon: {visible: true},
					flagState: {visible: true},
					ns: {visible: true},
					thetisDescription: {visible: true}
				}
			});

		let vesselTable = seg.ui.ttt.tables.openTablesValue.find(tab => tab.label === "Vessel Tracks");

		if (!vesselTable) {
			const vesselTableConfig = {
				label: "Vessel Tracks",
				customToolbar: {
					template: "<span class=\"text-size-sm\">Number of vessels: <b>{{grid.filteredRows.length}}</b>;</span> <span class=\"text-size-sm\">Number of positions: <b>{{numberOfPositions()}}</b></span>",
					scope: {
						numberOfPositions() {
							return Object.keys(trbv).reduce((numberOfPositions, vesselId) => numberOfPositions + trbv[vesselId].trackPositions.length, 0);
						}
					}
				},
				gridAPI: {getNavigationalStatus},
				data: [],
				itemAs: "vessel",
				checkboxes: true,
				paginate: {
					itemsPerPage: 10
				},
				fields: vesselTTTFields,
				onCheckboxChange() {
					positionsSource.changed();
					tracksSource.changed();
				},
				onSelectionChange(changedRows, selectedRows) {
					if (selectedRows.length === 1)
//						seg.ui.commandInfo.open("positions", selectedRows[0].item);
					//XXX should bring seg.selection?
					// selectedRows.forEach(row => trbv[row.item.getId()].trackPositions[0].set("selected", true));
					selectedRows.concat(changedRows)
						.filter((value, index, self) => self.indexOf(value) === index)
						.forEach(row => {
							if (!row.selected) {
								trbv[row.item.getId()].trackPositions[0].set("selected", false);
								return;
							}

							// For rare cases when we have track of different vessels with same labels (search for Alice II), we increment a counter to diff them
							let label = getLabel(row.item);
							// regex to match "Alice II" but also "Alice II (2)" or "Alice II (3) and so on"
							const repeatedLabels = seg.ui.ttt.tables.openTablesValue.filter(table => table.label.match(new RegExp(getLabel(row.item) + ".?(.\+?\d+\.)?"))).length;
							if (repeatedLabels)
								label += " ("+ (repeatedLabels + 1).toString() +")";

							seg.selection.select(trbv[row.item.getId()].trackPositions[trbv[row.item.getId()].trackPositions.length - 1]);
							seg.map.centerOn(trbv[row.item.getId()].trackPositions, 1);

							trackTables.push(seg.ui.ttt.tables.open({
								label,
								customToolbar: {
									template: "<span class=\"text-size-sm\">Number of positions: <b>{{numberOfPositions}}</b></span>",
									scope: {
										get numberOfPositions() {
											return trbv[row.item.getId()].trackPositions.length;
										}
									}
								},
								data: trbv[row.item.getId()].trackPositions,
								itemAs: "trackPosition",
								fields: trackTTTFields,
								paginate: {
									itemsPerPage: 100
								},
								onSelectionChange(changedRows, selectedRows) {
									seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
								}
							}));
						});
					positionsSource.changed();
					tracksSource.changed();
				},
				open: false,
				onClose() {
					seg.ui.ttt.tables.unregister("Vessel Tracks");
				}
			};
			seg.ui.ttt.tables.unregister("Vessel Tracks");
			vesselTable = seg.ui.ttt.tables.register(vesselTableConfig);
		}

		seg.ui.ttt.tables.addDataToTable(vesselTable, retrievedVessels);

		//add clean function
		if (options.acq)
			seg.ui.queryPanel.ACQ.onClear(() => {
				if (!tracksSource.getFeatures().some(track => track.get("isImported"))){
					seg.ui.ttt.tables.close(vesselTable);
					trackTables.forEach(seg.ui.ttt.tables.close);
					trackTables = [];
					tracksSource.clear();
					seg.ui.layerMenu.unregister("Imported Positions", {depth: 1});
				}

				trackReportsByVessel = {};
				const features = positionsSource.getFeatures();
				features.filter(position => position.get("old_vessel"))
					.forEach(position => positionsSource.removeFeature(position));
				features.filter(position => position.get("old_coordinates"))
					.forEach(position => position.getGeometry().setCoordinates(position.get("old_coordinates")));
			});
		else
			seg.ui.cleanables.add("Tracks", () => {
				seg.ui.ttt.tables.close(vesselTable);
				tracksSource.clear();
				// seg.timeline.clear();
				// seg.layer.filter.removeFilters(seg.layer.get("positions"), ["Imported Positions"]);
				// seg.layer.filter.removeFilters(seg.layer.get("tracks"), ["Imported Positions"]);
				// seg.ui.layerMenu.unregister("Imported Positions", 1);

				const features = positionsSource.getFeatures();
				features.filter(position => position.get("old_vessel"))
					.forEach(position => positionsSource.removeFeature(position));
				features.filter(position => position.get("old_coordinates"))
					.forEach(position => position.getGeometry().setCoordinates(position.get("old_coordinates")));
				trackTables.forEach(config => seg.ui.ttt.tables.close(config.label));
				trackTables = [];
				trackReportsByVessel = {};

				// seg.ui.layerMenu.unregister("Imported Positions", {depth: 1});

				// seg.layer.filter.removeFilters(seg.layer.get("positions"), ["Imported Positions"]);
				// seg.layer.filter.removeFilters(seg.layer.get("tracks"), ["Imported Positions"]);
			});
		return trbv;
	},
	// Get Track functions: Retrieves a vessel from a trackReport
	// Used to create a vessel feature from data from a track report, if the feature doesn't already exist
	getVesselFromTrackReport = (trackReport) => {
		let vessel = positionsSource.getFeatureById(trackReport.particulars.EMSAId);

		//if not, create it and add it to the vessel source
		if (!vessel) {
			vessel = new ol.Feature({
				callSign: trackReport.particulars.CallSign || null,
				thetisTypeCode: trackReport.particulars.ShipType || null,
				thetisTypeDescription: getShipType(Number(trackReport.particulars.ShipType)),
				flagState: getProperty(trackReport.particulars, /flag(?:state)?/i) || null,
				imo: getProperty(trackReport.particulars, /imo/i) || null,
				mmsi: getProperty(trackReport.particulars, /mmsi/i) || null,
				shipName: getProperty(trackReport.particulars, /name/i) || null,
				ir: getProperty(trackReport.particulars, /ircs/i) || null,
				old_vessel: true,
				isImported: trackReport.isImported,
				isACQResult: trackReport.isImported
			});
			vessel.setId(trackReport.particulars.EMSAId);
		}

		return vessel;
	},
	// Get Track functions: Retrieves a vessel from a trackReport
	// Used to create a vessel feature from data from a track report, if the feature doesn't already exist
	transformVesselIntoTrackPosition = vessel => {
		const props = vessel.getProperties(),
			positionProps = {
				geometry: vessel.getGeometry().clone(),
				heading: props.hdg,
				latitude: props.lat,
				longitude: props.lon,
				source: props.src,
				speed: props.speed,
				timestamp: props.ts,
				vf: props.vf
			},
			position = new ol.Feature(positionProps);

		position.setId(vessel.getId() + Date.now());

		return position;
	},
	// Get Track functions: Concatenate trackReports with importedTrackReports to create a single line and single object
	concatTrackReports = (trackReports, importedTrackReports) => {
		// Concat trackReport with importedTrackReports
		trackReports = [].concat(...trackReports)
			.filter(({particulars}) => Object.keys(particulars).length);

		importedTrackReports = [].concat(...importedTrackReports)
			.filter(({particulars}) => Object.keys(particulars).length);

		// If both exists, it means that imported tracks required external positions
		// So we concatenate the track geometry and positions
		if (trackReports.length && importedTrackReports.length) {
			importedTrackReports.forEach(importedTrackReport => {
				const trackReport = trackReports.find(trackReport => {
					return importedTrackReport.particulars.EMSAId === trackReport.particulars.EMSAId;
				});

				trackReport.isImported = true;
				trackReport.isACQResult = true;
				trackReport.positions.features = importedTrackReport.positions.features
					.concat(trackReport.positions.features)
					.sort((a, b) => {
						const aMoment = moment(a.properties.positionReport ? a.properties.positionReport.timestamp : a.properties.timestamp);
						const bMoment = moment(b.properties.positionReport ? b.properties.positionReport.timestamp : b.properties.timestamp);
						return aMoment.diff(bMoment);
					});

				trackReport.track.geometry.coordinates = trackReport.positions.features
					.map(feature => {
						const lon = feature.properties.positionReport ? feature.properties.positionReport.longitude : feature.geometry.coordinates[0],
							lat = feature.properties.positionReport ? feature.properties.positionReport.latitude : feature.geometry.coordinates[1];

						return [lon, lat];
					});
			});
		} else if(!trackReports.length && importedTrackReports.length) {
			// if only importedTrackReports exists, we don't have to concatenate nothing
			trackReports = importedTrackReports;
		}

		return trackReports;
	},
	// Get Track functions: Parse tracks reports from data received from service to usage on SEG
	parseTrackReports = async (trackReports, options) => {
		let downsampled = false;

		const modal = seg.modal.openLoader("Get Tracks"); // getting instance of modal
		//merge all the track reports from all requests
		trackReports = [].concat(...trackReports);
		// since empty isn't realy an empty data structure we have to check
		if (trackReports.length === 1)
			if (!Object.keys(trackReports[0].particulars).length)
				trackReports = [];

		/*
		create an object to store track reports by vessel,
		so we can open a table with the vessels and one with the tracks
		or just a single tracks table if we only got tracks for one vessel
		*/
		const trackReportsByVessel = {},
			trackLines = [],
			vesselTrackFeatures = [];

		//for each track report received
		trackReports.forEach(trackReport => {
			//check if we already have a vessel with the received vid
			const DEGREE_TO_M = 111319.489680,
				projectionCode = seg.map.getProjection().getCode(),
				vessel = getVesselFromTrackReport(trackReport),
				//add this vessel's track position to the map
				trackPositions = (new ol.format.GeoJSON()).readFeatures(trackReport.positions, {dataProjection: "EPSG:4326", featureProjection: seg.map.getProjection()}),
				//convert geometry to ol
				// check if geometry wraps around the date line
				wraps = null;//seg.geometry.wraps(seg.geometry.parse(trackReport.track.geometry, "GeoJSON", {featureProjection: "EPSG:4326"}));

			if(trackReport.simplifiedResults)
				downsampled = trackReport.simplifiedResults;

			let latestTrackPosition;

			// parsing trackPositions
			trackPositions.forEach((trackPosition, trackIndex) => {
				// if wraps
				if (wraps) {
					const trackCoordinates = ol.proj.transform(trackPosition.getGeometry().getCoordinates(), projectionCode, "EPSG:4326");

					if (trackCoordinates[0] > 0 && wraps > 0)
						trackCoordinates[0] = -(((wraps * 180) - trackCoordinates[0]) + (wraps * 180));
					else if (trackCoordinates[0] < 0 && wraps < 0)
						trackCoordinates[0] = -(((wraps * 180) - trackCoordinates[0]) + (wraps * 180));

					trackReport.track.geometry.coordinates[trackIndex] = trackCoordinates;

					if(projectionCode === "EPSG:3395"){
						const _trackCoordinates = ol.proj.transform(trackCoordinates, "EPSG:4326", projectionCode);
						_trackCoordinates[0] = trackCoordinates[0] * DEGREE_TO_M;
						trackPosition.getGeometry().setCoordinates(_trackCoordinates);
					} else
						trackPosition.getGeometry().setCoordinates(ol.proj.transform(trackCoordinates, "EPSG:4326", projectionCode));
				}

				trackPosition.setId(vessel.getId()+"_"+trackPosition.getId());
				trackPosition.setProperties({
					"vessel": vessel,
					"isImported": trackReport.isImported,
					"isACQResult": trackReport.isImported,
					"visible": (
						//acq but user asked for all positions, or
						(options.simple && !options.simple.enabled) ||
						//position is first/last
						(options.simple && options.simple.first && (trackIndex === 0)) ||
						(options.simple && options.simple.latest && (trackIndex === trackPositions.length - 1))
					)
				});

				const positionReport = trackPosition.get("positionReport");
				//extract properties inside track positionReport
				if(positionReport) {
					for (const propt in positionReport)
						trackPosition.set(propt, positionReport[propt]);
					trackPosition.unset("positionReport");
				}

				trackPosition.on("change:isSearchResult", syncTrackPropWithVessel("isSearchResult"));
				trackPosition.on("change:wasSelected", syncTrackPropWithVessel("wasSelected"));

				const trackPositionTimestamp = moment(trackPosition.get("timestamp"));

				//find the latest position among those received
				if (!latestTrackPosition) // first
					latestTrackPosition = trackPosition;

				if (trackPositionTimestamp.isAfter(moment(latestTrackPosition.get("timestamp"))))
					latestTrackPosition = trackPosition;

				vesselTrackFeatures.push(trackPosition);
			});

			// parsing trackLine
			const trackLineProperties = Object.assign({}, trackReport.track,
				{
					id: vessel.getId() + "_" + Date.now(),
					properties: {
						vessel,
						isTrackLine: true,
						positions: trackPositions,
						isImported: trackReport.isImported,
						isACQResult: trackReport.isImported
					}
				});

			const trackLine = (new ol.format.GeoJSON()).readFeatures(trackLineProperties, {
				dataProjection: "EPSG:4326",
				featureProjection: projectionCode
			})[0];

			if (wraps && (projectionCode === "EPSG:3395")){
				trackLine.getGeometry().setCoordinates(trackReport.track.geometry.coordinates.map(trackCoordinates => {
					const _trackCoordinates = ol.proj.transform(trackCoordinates, "EPSG:4326", projectionCode);
					_trackCoordinates[0] = trackCoordinates[0] * DEGREE_TO_M;
					return _trackCoordinates;
				}));
			}

			trackLine.on("change:isSearchResult", syncTrackPropWithVessel("isSearchResult"));
			trackLine.on("change:wasSelected", syncTrackPropWithVessel("wasSelected"));
			trackLines.push(trackLine);
			/*
			if we found a track position that's more recent than the information we already had
			then we update the vessel properties with the latest ones
			*/
			if (vessel.get("ts") && moment(vessel.get("ts")).isAfter(moment(latestTrackPosition.get("timestamp"))))
				vessel.set("old_coordinates", vessel.getGeometry().getCoordinates());

			if(latestTrackPosition) {
				const additionalInfo = latestTrackPosition.get("AdditionalInfo");

				vessel.setProperties({
					"lat": latestTrackPosition.get("latitude"),
					"lon": latestTrackPosition.get("longitude"),
					"ts": latestTrackPosition.get("timestamp"),
					"hdg": latestTrackPosition.get("heading"),
					"speed": latestTrackPosition.get("speed"),
					"src": latestTrackPosition.get("src") || latestTrackPosition.get("source") || latestTrackPosition.get("uploadType")
				});

				if (additionalInfo && additionalInfo.PreProcessingSpecific && additionalInfo.PreProcessingSpecific.KalmanValidationOutput)
					vessel.set("vf", latestTrackPosition.get("AdditionalInfo").PreProcessingSpecific.KalmanValidationOutput.ValidityFlag);

				// also update the coordinates of the vessel
				const latestTrackCoordinates = ol.proj.transform(latestTrackPosition.getGeometry().getCoordinates(), projectionCode, "EPSG:4326");
				const newCoords = ol.proj.transform(latestTrackCoordinates, "EPSG:4326", projectionCode);

				if (vessel.getGeometry())
					vessel.getGeometry().setCoordinates(newCoords);
				else
					vessel.setGeometry(new ol.geom.Point(newCoords));
			}

			if (options.ssnEnrichment) {
				//if we only got tracks for one vessel, open just one table
				const params = {
					details: "GetAllReportedVoyagesOfSelectedShip",
					startTime: moment(options.from).format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
					endTime:  moment(options.to).format("YYYY-MM-DD[T]HH:mm:ss[Z]")
				};

				if (vessel.get("imo"))
					params.imoNumber = vessel.get("imo");
				else if (vessel.get("mmsi"))
					params.mmsiNumber = vessel.get("mmsi");
				else if (vessel.get("csdid"))
					params.csdId = vessel.get("csdid");

				// SSN EVENT PROCEDURE
				if (params.csdId || params.imoNumber || params.mmsiNumber)
					seg.request.get("v1/ship/ship_call", {params}).then((res) => {
						const {status, message} = res;

						if (!(status === "success" || status === "OK")) {
							modal.setContent("<span class=\"retrieved-no-result\"><b>" + status + ": See console for details</b></span>");
							modal.setClass("no-result");
							seg.log.error("ERROR_VESSELS_TRACKS_SSN", status + ": " + message);
						} else {
							let {result} = res;

							if (result.hasOwnProperty("Body"))
								result = result.Body;

							let trackCandidate = null,
								voyages = result.QueryResults.PortPlusNotificationList;

							voyages = voyages.filter(voyage => moment(voyage.Source.LastUpdateReceivedAt).isBetween(moment(trackLine.get("positions")[0].get("timestamp")), moment(trackLine.get("positions")[trackLine.get("positions").length - 1].get("timestamp"))));

							// Registered voyages done OR schedules for the vessel
							voyages.forEach(voyage => {
								trackReportsByVessel[vessel.getId()].trackPositions.forEach(trackPosition => {

									// We take the timestamp from trackpositoin and from voyage
									const trackPositionTimestamp = moment(trackPosition.get("timestamp")),
										voyageTimestamp = moment(voyage.Source.LastUpdateReceivedAt);

									// here we get the trackCandidate by checking the closest track timestamp of the voyage timestamp
									if (trackPositionTimestamp.isBefore(voyageTimestamp) && moment.duration(voyageTimestamp.diff(trackPositionTimestamp)).asHours() < 2){
										trackCandidate = trackPosition;
										return;
									}
								});

								// finally we set the properties voyage properties to this trackCandidate
								if (trackCandidate)
									trackCandidate.set("ssn_event", {
										Notification: "PortPlus",
										ShipCallId: voyage.VoyageInformation.ShipCallId,
										Sender: voyage.Source.ProviderOfLastUpdate,
										Date: moment(voyage.Source.LastUpdateReceivedAt),
										HazmatOnBoardYorN: voyage.HazmatConfirmation.HazmatOnBoardYorN
									});
								trackCandidate = null;
							});
						}
					}, (e) => {
						const modal = seg.modal.openLoader("Get Tracks");
						modal.setContent("Failed to get SSN Enrichment details");
						modal.setClass("error");
						seg.modal.closeTimeout(modal.title);
						seg.log.error("ERROR_VESSELS_TRACKS_SSN", e.error + ": " + e.result);
					});
			}

			// trackReportsByVessel
			// check if we already had tracks for this vessel in this request
			// if not, create an entry in the trackReportsByVessel object
			if (typeof trackReportsByVessel[vessel.getId()] === "undefined")
				trackReportsByVessel[vessel.getId()] = {
					vessel: vessel,
					trackPositions: []
				};

			// add the retrieved track positions to the trackReportsByVessel object
			trackReportsByVessel[vessel.getId()].trackPositions = trackReportsByVessel[vessel.getId()].trackPositions.concat(trackPositions);
		});

		vesselTrackFeatures.push(...trackLines);
		if(vesselTrackFeatures.length) {
			positionsSource.addFeatures(mergeDuplicates(trackLines.map(line => line.get("vessel"))));

			const numberOfTrackReports = Object.keys(trackReportsByVessel).length;
			modal.setContent("Retrieved <span class=\"retrieved-success\"><b>" + numberOfTrackReports + "</b> track reports</span>");

			if(downsampled) {
				const modalDownsample = seg.modal.openModal("TRACKS_DOWNSAMPLE", "OBS: Not all available tracks are displayed. Try to restrict your query.", "info");
				seg.modal.closeTimeout(modalDownsample.title);
			}

			seg.modal.closeTimeout(modal.title);

			if (options.acq) {
				//add features to be managed by acq mode
				seg.ui.queryPanel.ACQ.addFeatures(Object.keys(trackReportsByVessel).reduce((vessels, id) => [...vessels, positionsSource.getFeatureById(id)], []));
				seg.ui.queryPanel.ACQ.addFeatures(vesselTrackFeatures);
			}

			if (options.withExternal)
				vesselTrackFeatures.forEach(vessel => {
					vessel.set("isImported", true);
					vessel.set("isACQResult", true);
				});

			tracksSource.addFeatures(vesselTrackFeatures);
			seg.map.centerOn(vesselTrackFeatures);

			return {
				trackReportsByVessel,
				vesselTrackFeatures,
				trackLines
			};
		}
	},
	// Get Track functions: Triggers getTracks chain of promises, starting with request according to params
	getTracks = async options => {
		let message;

		const {importedTrackReports = [], withExternal} = options,
			collection = [].concat(options.vessels || options.bboxes);

		if(withExternal !== false) {
			//default options: from the last 24 h
			options.from = options.from || moment().subtract(1, "d");
			options.to = options.to || moment();

			options.simple = options.simple || {
				enabled: false,
				latest: false,
				first: false
			};

			//collection to search: either vessels or bounding boxes
			if (!options.acq)
				if (collection.length > 1)
					message = "Retrieving " + collection.length + " vessel tracks...";
				else
					message = "Retrieving vessel tracks for " + (!importedTrackReports.length ? getLabel(collection[0]) : "imported vessel") + "...";
			else
				message = "Retrieving vessel tracks...";

		} else
			message = "Retrieving Vessels...";

		const modal = seg.modal.openLoader("Get Tracks", message, {class: "info"});
		modal.setPartialProgress("tracks", 0);
		// try to execute a previously registered clean function to clean existing tracks
		// unless withExternal is defined, which means this is being called from import app

		if(typeof withExternal === "undefined" && !options.acq)
			seg.ui.cleanables.call("Tracks");

		// Calling tracks WS
		return seg.promise.all(collection.map(async vesselOrBBOX => {
			if(withExternal === false)
				return [];

			const params = seg.utils.removeUndefinedAttributes(Object.assign({
				beginTimestamp: moment(options.from).format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
				endTimestamp: moment(options.to).format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
				source: options.source,
				flag: options.flag,
				type: options.type,
				maxRecords: options.maxRecords
				//if vessel, pass vid
			}, options.vessels ? {
				vesselId: (!importedTrackReports.length ? vesselOrBBOX.getId() : vesselOrBBOX),
				vesselIdCriteria: "emsaId",
				fusedAndSmoothed: options.fusesmoth,
				extrapolated: options.extrapolate,
				stepInSeconds: options.extrapInterStepSec //extrapolationDurationMin: options.extrapolationDurationMin > por mapear
				//else pass bounding box coordinates
			} : {
				minX: vesselOrBBOX[0],
				minY: vesselOrBBOX[1],
				maxX: vesselOrBBOX[2],
				maxY: vesselOrBBOX[3]
			}));

			//request tracks for vessel or for bounding box
			const trackReportsRequest = seg.request.get("v1/tracks/" + (options.vessels ? "vessel" : "bbox"), {params}).toPromise();

			modal.setDismissCallback(() => {
				trackReportsRequest.unsubscribe();
			});

			return trackReportsRequest.then(res => {
				if(res.result && res.result[0].positions.features.length)
					return res.result;

				if(options.vessels)
					if (!importedTrackReports.length)
						modal.setContent("Retrieved 0 track reports for vessel " + getLabel(vesselOrBBOX) + ".");
					else
						modal.setContent("Retrieved 0 track reports for imported vessel.");
				else
					modal.setContent("Retrieved 0 track reports for area.");

				modal.setClass("no-result");

				seg.modal.closeTimeout(modal.title);
				seg.map.disableFilter("ACQ_FILTER");
				return res.result;
			}, (e) => {
				modal.setContent("Error retrieving tracks: " + e.result);
				modal.setClass("error");

				seg.modal.closeTimeout(modal.title, 2000);
				return Promise.reject(e);
			}, e => seg.log.error("ERROR_VESSELS_GET_TRACKS", e.error + ": " + e.result));
		}))
			.then(trackReports => concatTrackReports(trackReports, importedTrackReports)) // filtering
			.then(trackReports => parseTrackReports(trackReports, options)) // parsing
			.then(parsedReports => createTableAndCleanablesFromTrackReports(parsedReports, options)) // creating table
			.then(trbv => createTimelineFromTrackReports(trbv)); // creating timeline
	},
	// Check if layer is Visible
	// Used on startup of module
	isVisible = () => {
		//XXX FIX MINSCALE is only defined here. positionsLayer has it as undefined.
		positionsLayer.set("minScale", seg.CONFIG.getValue("vessels.positions.MINSCALE"));
		//console.log(vesselsLayer.getVisible(), positionsLayer.getVisible(),seg.map.getScale(),positionsLayer.get("minScale") ,seg.map.isFilterEnabled("SELECT_FILTER"), seg.map.isFilterEnabled("SURPIC_FILTER"),seg.map.isFilterEnabled("ACQ_FILTER"));
		return vesselsLayer.getVisible() &&
			positionsLayer.getVisible() &&
			seg.map.getScale() < positionsLayer.get("minScale") && // CLUSTERS_REGRESSION
			!seg.map.isFilterEnabled("SELECT_FILTER") &&
			!seg.map.isFilterEnabled("SURPIC_FILTER") &&
			!seg.map.isFilterEnabled("ACQ_FILTER");
	},
	// Toggle follow function for vessel
	// Used in C&I
	toggleFollow = (vessel) => {
		let followVesselModal;
		seg.map.centerOn(vessel, seg.map.getView().getResolution());

		const cleanFunction = () => {
			ol.Observable.unByKey(seg.preferences.session.get("following").key);
			seg.preferences.session.set("following", undefined);
			seg.preferences.workspace.set("following", undefined);
			seg.layer.get("positions").getSource().refresh();
		};

		const followVessel = (v) => {
			const key = v.on("change:ts", () => {
				seg.map.centerOn(vessel, seg.map.getView().getResolution());
			});

			seg.preferences.session.set("following", {
				feature: v,
				key: key
			});

			seg.preferences.workspace.set("following", {
				featureId: v.getId()
			});

			seg.ui.cleanables.add("Follow Vessel", cleanFunction);
			seg.selection.deselect(v);
		};

		// if there wasn't a vessel being followed, follow the one selected
		if (typeof seg.preferences.session.get("following") === "undefined") {
			followVessel(vessel);
			followVesselModal = seg.modal.openModal("followVessel", {class: "info"});
			followVesselModal.setContent("Now following vessel " + getLabel(vessel) + ".");
			return;
		}

		// else get the one being followed
		const followed = seg.preferences.session.get("following").feature;

		// check if the one selected is the same that was being followed
		if (Number(followed.getId()) !== Number(vessel.getId())) {
			// if it is not, remove observable and bind it to the new one
			if (seg.preferences.session.get("following").key)
				ol.Observable.unByKey(seg.preferences.session.get("following").key);
			followVessel(vessel);
			followVesselModal = seg.modal.openModal("followVessel", {class: "info"});
			followVesselModal.setContent("Now following vessel " + getLabel(vessel) + ".");
		} else {
			// if it is simply clean it
			cleanFunction();
			followVesselModal = seg.modal.openModal("followVessel", {class: "info"});
			followVesselModal.setContent("Stopped following vessel " + getLabel(vessel) + ".");
			seg.modal.closeTimeout(followVesselModal.title);
		}

		seg.layer.get("positions").getSource().refresh();
		seg.ui.cleanables.remove("Follow Vessel");
	},
	// Updates positions source after positions request
	// Also caches new positions agains tracks to avoid a tracked vessel to update it's position
	/**
	 * Receives features merges them with eventual duplicates while avoiding tracked vessels
	 * @param  {ol.Feature.Feature[]} features Array of features (vessel position)
	 */
	updateSource = (features) => {
		// if positions request is in refresh mode, filter out any new positions that dont have yet a feature and then get their particulars.
		if (shouldRefresh) {
			const particulars = [].concat(features.reduce((acc, vessel) => {
				if (!positionsSource.getFeatureById(vessel.getId()))
					acc.push(vessel);
				return acc;
			}, []).map(getParticulars));

			if (particulars.length)
				seg.observable.forkJoin(particulars).subscribe();
		}

		const vessels = mergeDuplicates(features),
			vesselTracks = tracksSource.getFeatures();

		if (vesselTracks.length)
			vessels.forEach(vessel => {
				const vesselTrack = vesselTracks.find(track => track.get("isTrackLine") && track.get("vesselId") === vessel.getId());
				if(vesselTrack) {
					const existingPositions = vesselTrack.get("positions");

					const lastPosition = existingPositions[existingPositions.length - 1],
						lastPositionTs = moment(lastPosition.get("timestamp")),
						currentPositionTs = moment(vessel.get("ts"));

					if(lastPositionTs.isAfter(currentPositionTs))
						return;

					const position = transformVesselIntoTrackPosition(vessel);
					vesselTrack.getGeometry().appendCoordinate(position.getGeometry().getCoordinates());
					vesselTrack.set("positions", [...existingPositions, position]);

					tracksSource.addFeature(position);
					// removing old_vessel attributes that may have been added on getTracks function
					vessel.unset("old_vessel");
				}

			});

		positionsSource.addFeatures(vessels);

		positionsSource.dispatchEvent({
			type: "update",
			features: vessels
		});

		const followedVessel = seg.preferences.session.get("following") ? seg.preferences.session.get("following").feature : undefined;

		if (followedVessel && !vessels.find(vessel => vessel.getId() === followedVessel.getId())){

			// const followedVesselPosition = await getLastKnownPosition({
			// 	imo: followedVessel.get("imo") ? followedVessel.get("imo") : undefined,
			// 	mmsi: followedVessel.get("mmsi") ? followedVessel.get("mmsi") : undefined,
			// 	imdateId: followedVessel.get("vid") ? followedVessel.get("vid") : undefined,
			// 	id: followedVessel.getId() ? followedVessel.getId() : undefined
			// });

			// if(followedVesselPosition)
			// 	positionsSource.addFeatures(mergeDuplicates([followedVesselPosition]));

		}
	},
	// LASTPOSITION_REGRESSION
	// Get last position in the last 24h of a vessel, used on getLastKnownPosition
	getCurrentPosition = params => seg.request.get("v1/ship/getCurrentVesselPosition?", {params: seg.utils.removeUndefinedAttributes(params)})
		.then(res => {
			if (res.result && res.result.features && res.result.features.length)
				return new ol.format.GeoJSON()
					.readFeature(res.result.features[0], {
						dataProjection: "EPSG:4326",
						featureProjection: seg.map.getProjection()
					});
		}, e => {
			seg.log.error("ERROR_VESSELS_GET_CURRENT_POSITION", e.error + ": " + e.result);
			return undefined;
		}),
	// LASTPOSITION_REGRESSION
	// Get last position of a vessel with track services, used on getLastKnownPosition
	getLastPositionWithTracks = params => seg.request.get("v1/tracks/vessel", {params})
		.then(({result}) => {
			if(result && result[0] && result[0].positions.features){
				const particulars = result[0].particulars,
					lastPosition = result[0].positions.features[result[0].positions.features.length - 1]; // last position data

				if(!lastPosition)
					return;
				else
					return (new ol.format.GeoJSON())
						.readFeature({
							id: particulars.EMSAId,
							type: lastPosition.type,
							geometry: lastPosition.geometry,
							properties: {
								shipName: particulars.Name,
								callSign: particulars.CallSign,
								imo: particulars.IMO,
								mmsi: particulars.MMSI,
								src: lastPosition.properties.positionReport.source,
								lat: lastPosition.properties.positionReport.latitude,
								lon: lastPosition.properties.positionReport.longitude,
								speed: lastPosition.properties.positionReport.speed,
								hdg: lastPosition.properties.positionReport.heading,
								th: lastPosition.properties.positionReport.heading,
								ts: lastPosition.properties.positionReport.timestamp
							}
						}, {
							dataProjection: "EPSG:4326",
							featureProjection: seg.map.getProjection()
						});
			}
		}, e => {
			seg.log.error("ERROR_VESSELS_GET_TRACKS_FILTERING", e.error + ": " + e.result);
			return undefined;
		}),
	// Pass an array of ids, and the service return the last known position for those vessel ids in the last year
	// Since the service can only get 100 vessels at time, we chunkify the array befeore searching
	getLastKnownVesselsPosition = ids  => {
		if (Array.isArray(ids) && ids.length) {
			return seg.promise.all(seg.utils.chunkify(ids, 100).map(chunked_ids => {
				return seg.request.post("v1/ship/lastKnownVesselsPosition?", {data: chunked_ids}).toPromise()
					.then(({result}) => {
						if (result && result.result && result.result.length) {

							const formattedData = {
								crs: {
									properties: {name: "EPSG:4326"},
									type:"name"
								},
								type: "FeatureCollection",
								features: result.result.map(properties => {
									return {
										properties,
										id: properties.vid,
										type: "Feature",
										geometry: {
											type: "Point",
											coordinates: ([Number(properties.lon), Number(properties.lat)])
										}
									};
								})
							};

							return new ol.format.GeoJSON()
								.readFeatures(formattedData, {
									dataProjection: "EPSG:4326",
									featureProjection: seg.map.getProjection()
								});
						}
						return;
					}, e => {
						seg.log.error("ERROR_VESSELS_GET_LAST_KNOWN_VESSELS_POSITION", e.error + ": " + e.result);
						return undefined;
					});
			})).then(vessels => vessels.reduce((a, b) => a.concat(b), []));
		}
	},
	// Get the last known position of a vessel, by id, mmsi, imo or csdid, with current vessel position or tracks
	// Used in several locations
	getLastKnownPosition = async ({imo, mmsi, csdId, id}) => {
		let vesselCurrentPosition;
		// probably this will cover all ends in the future´, who knows when?

		if (!vesselCurrentPosition) { // Attempting getCurrentPosition
			//getLastKnownVesselPosition uses a post methor sendig a comma separated string list with vesselId
			//In this case only one is sent
			vesselCurrentPosition = (await getLastKnownVesselsPosition([id])).filter(v => v);

			if (vesselCurrentPosition)
				vesselCurrentPosition.forEach(vcp => vcp.setId(vcp.get("identity").id));
		}

		if (!vesselCurrentPosition || (Array.isArray(vesselCurrentPosition) && !vesselCurrentPosition.length)) { // Attempting getCurrentPosition
			vesselCurrentPosition = await getCurrentPosition({
				vesselId: id,
				imo: imo,
				mmsi: mmsi,
				csdId: csdId
			});

			if (vesselCurrentPosition)
				vesselCurrentPosition.setId(vesselCurrentPosition.get("identity").id);
		}

		// if (!vesselCurrentPosition || (Array.isArray(vesselCurrentPosition) && !vesselCurrentPosition.length)) { // Attempting getLastPositionWithTracks
		// 	const params = {
		// 		beginTimestamp: moment().subtract(24, "M").format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
		// 		endTimestamp: moment().format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
		// 		vesselId: id,
		// 		vesselIdCriteria: "emsaId",
		// 		maxRecords: 1 // look for only 1 record
		// 	};

		// 	// if no emsaId is available we should instead query tracks with mmsi
		// 	if (!params.vesselId && mmsi)
		// 		Object.assign(params, {vesselId: mmsi, vesselIdCriteria: "mmsi"});

		// 	// and if no mmsi is available we should instead query tracks with imo
		// 	if (!params.vesselId && imo)
		// 		Object.assign(params, {vesselId: imo, vesselIdCriteria: "imo"});

		// 	vesselCurrentPosition = await getLastPositionWithTracks(params);
		// }

		return vesselCurrentPosition;
	},
	// Return ShipType String according to ID
	getShipType = (value) => {
		const found = Object.entries(PSC_SHIP_TYPES).find(type => type[1] === value);
		if (found)
			return found[0];
	},
	// Get PSC Inpsection by service
	// Used on C&I
	getPSCInspections = async vessel => {
		const modal = seg.modal.openModal("psc", "Requesting PSC Inspections...");
		let title;

		try {
			const res = await seg.request.get("v1/thetis/ship/validated_inspections", {params: {imo: vessel.get("imo")}});
			let inspections = res.result.getValidatedInspectionsResponse.ValidatedInspectionsReceipt.Inspection;

			if(!inspections)
				modal.setContent("No results found");
			else {
				if (!Array.isArray(inspections))
					inspections = [inspections];

				if (inspections.length) {
					modal.setContent("Found "+inspections.length+" inspections");

					inspections = inspections.map(inspection => {
						if (inspection.InspectionDetails && inspection.InspectionDetails.DateOfFinalVisit)
							inspection.InspectionDetails.DateOfFinalVisit = moment(inspection.InspectionDetails.DateOfFinalVisit, "YYYY-MM-DDZ");
						if (inspection.InspectionDetails && inspection.InspectionDetails.DateOfFirstVisit)
							inspection.InspectionDetails.DateOfFirstVisit = moment(inspection.InspectionDetails.DateOfFirstVisit, "YYYY-MM-DDZ");
						if (inspection.ShipParticulars && inspection.ShipParticulars.KeelDate && inspection.ShipParticulars.KeelDate.Value)
							inspection.ShipParticulars.KeelDate.Value = moment(inspection.ShipParticulars.KeelDate.Value, "YYYY-MM-DDZ");
						return inspection;
					});

					seg.ui.ttt.tables.open({
						label: "Inspection on " + getLabel(vessel),
						customToolbar: {
							template: "<span class=\"text-size-sm\">Number of inspections: <b>{{grid.filteredRows.length}}</b></span>"
						},
						data: inspections,
						itemAs: "inspection",
						fields: require("./positions/psc_inspection.fields.js"),
						sortKey: "dateOfFinalVisit",
						sortKeyOrder: "asc",
						onSelectionChange(changedRows, selectedRows) {
							if (selectedRows.length === 1) {
								if (title)
									seg.popup.close(title);

								const item = selectedRows[0].item;
								title = "Inspection " + item.InspectionDetails.InspectionID;

								seg.popup.openPopup({
									title,
									templateUrl: resolvePath("./positions/templates/psc_inspection.html"),
									bindings: {
										inspection: item,
										getShipType,
										ShipAreaFields: {
											Area: {
												label: "Area",
												template: "{{item.Area || \"N/A\"}}",
												visible: true
											}
										},
										OperationalControlsFields: {
											OperaionalControl: {
												label: "Operational Control",
												template: "{{item.OperationalControl || \"N/A\"}}",
												visible: true
											}
										}
									}
								});
							}
						}
					});
				}
				else modal.setContent("No results found");
			}
		}
		catch(e) {
			seg.log.error("PSC Inspections", e.result.errorCode+": "+e.result.errorDescription, e);
			modal.setContent("An error ocurred");
		}

		seg.modal.closeTimeout("psc");
	},
	/**
	 * Request positions based on viewport
	 * @param  {Number[]} extent Array of coordinates that correspond to the limit of the user's screen
	 * @return {[type]}                [description]
	 */
	requestExtent = (extent /*shouldMaxCount = false*/) => { // CLUSTERS_REGRESSION
		let filtersToQuery = Object.assign({}, positionsLayer.ol_.get("filtersToQuery"));

		// While clustering is visible, should always call positions to see if we can turn cluster off
		// if(seg.layer.get("clusters").getVisible())
		// 	shouldRefresh = false;


		const requestExtents = seg.utils.parseExtents(extent, MAX_TILES),
			minReceivedTs = lastUpdate && (shouldRefresh ? lastUpdate.toISOString().split(".")[0]+"Z" : null),
		//	maxPositionsCount = shouldRefresh ? null : MAX_POSITIONS_COUNT;
			modal = seg.modal.openLoader("Vessel positions", shouldRefresh ? "Refreshing vessel positions... <b>(0%)</b>" : "Requesting vessel positions... <b>(0%)</b>", requestExtents.length);
		
		modal.setPartialProgress("Vessel positions", 0);

		// Workaround because service only accepts one value of each parameter instead of many
		if (filtersToQuery["source"] && filtersToQuery["source"].length > 1)
			filtersToQuery["source"] = [];

		if (filtersToQuery["flagState"] && filtersToQuery["flagState"].length > 1)
			filtersToQuery["flagState"] = [];

		if (filtersToQuery["shipType"] && filtersToQuery["shipType"].length > 1)
			filtersToQuery["shipType"] = [];

		filtersToQuery = Object.entries(filtersToQuery).reduce((filtersParsed, [key, value]) => {
			// if(value.length)
			// 	filtersParsed[key] = value.join(",");
			if(value.length === 1)
				filtersParsed[key] = value[0];
			return filtersParsed;
		}, {});

		// merge executes the queries asynchronously so as soon as they are returned, the pipeline goes on and the positions are added to the map
		return seg.observable.merge(...requestExtents.map(([minX, minY, maxX, maxY]) => {
			const params = seg.utils.removeUndefinedAttributes(Object.assign({minX, minY, maxX, maxY, minReceivedTs}, filtersToQuery));

			return Object.assign(seg.request.get(shouldRefresh ? "v1/ship/refreshPositions" : "v1/ship/positions", {
				params
			}), {id: Math.floor(Math.random()*(1000-0+1)+0)});
		}));
	},
	// get a property from a vessel with a regex
	getProperty = (obj, regex) => obj[Object.keys(obj).find(key => key.toString().match(regex))],
	/**
	 * Get the EMSA id of a vessel querying another primary key (mmsi, imo, etc)
	 * @param  {string} value id value
	 * @param  {string} key  key that will be queried
	 * @return {string} EMSA id
	 */
	getEMSAId = async (value, key) => {
		const {result} = await seg.request.get("v1/ship/imdateID", {
			params: {
				[prop]: value
			}
		});

		return (result && result[0] && result[0].id) || "noId";
	},
	/**
	 * Unsubscribes any positionsSubscription and resets shouldRefresh variable
	 * @return {[type]} [description]
	 */
	resetRefresher = () => {
		if(positionsSubscription)
			positionsSubscription.unsubscribe();

		shouldRefresh = false;
	},
	/**
	 * Set's up and fires and observable with a given interval to request, parse and add vessel positions to the source
	 * @param  {Number[]} extent Array of coordinates that correspond to the limit of the user's screen
	 * @return {Subscription} positionSubscription
	 */
	setupRefresher = (extent) => {
		return seg.observable.timer(0, REFRESH_TIME) // Interval creates a recurrent observable
			.pipe(
				seg.observable.operators.takeWhile(() => isVisible() && !seg.map.isFilterEnabled("SELECT_FILTER")), // only run if positions layer is visible
				seg.observable.operators.switchMap((flag) => { // returns observable of the request extent(s) to return to the subscription
					// since function timer returns iteration, if this is the second time onward it is running, shouldRefresh positions instead of getting
					if(flag)
						shouldRefresh = true;
					
					lastUpdate = moment().clone().subtract(1, "minutes");
					return requestExtent(extent);
				}),
				seg.observable.operators.catchError(() => {
					const modal = seg.modal.get("Vessel positions");
					modal.setContent("<span class=\"text-color-warning\">Error getting vessel positions.</span>");
					seg.modal.closeTimeout(modal.title);
				})
			).subscribe((res) => {
				// if((res.status === "warning" || res.message === "412 - Precondition Failed")){
				// 		const shouldCluster = (
				// 			!seg.map.isFilterEnabled("SELECT_FILTER") && // if also select filter is not toggled
				// 			!seg.selection.getSelected().some(selected => seg.favourites.isFavourite(selected)) && // no favourites are selected
				// 			!seg.selection.getSelected().some(selected => selected.get("isSearchResult")) // and no searchResults are selected
				// 		);
				// 		// shouldCluster
				// 		if(shouldCluster){
				// 			updateClusters(true);
				// 			positionsSource.getFeatures().forEach((vessel) => {
				// 			if(!vessel.get("favourite_name"))
				// 				positionsSource.removeFeature(vessel);
				// 			});
				// 			positionsLayer.ol_.setOpacity(0);
				// 			// modal.setContent("<span class=\"retrieved-error\">Vessel positions exceeded " + MAX_POSITIONS_COUNT + " limit.</span> Toggling cluster mode");
				// 		} else {
				// 			eraseClusters();
				// 			positionsLayer.ol_.setOpacity(1);
				// 		}
				//		seg.modal.closeTimeout(modal.title);
				// } else {
				// 		positionsLayer.ol_.setOpacity(1);
				// 		eraseClusters();
				// 		modal.setContent("Vessel positions <span class=\"retrieved-success\">updated</span>.");
				// }

				updateSource(new ol.format.GeoJSON().readFeatures(res.result, {dataProjection: "EPSG:4326", featureProjection: seg.map.getProjection()}), shouldRefresh);
				
				const modal = seg.modal.get("Vessel positions");
				modal.updateProgress();
				modal.setContent("Vessel positions updating... <b>(" + modal.scope.loader.progress.toFixed(2) + "%)</b>");
				
				if(Math.ceil(modal.scope.loader.progress) >= 100){
					modal.setContent("<span class=\"text-color-success\">Vessels positions updated. <b>(100%)</b></span>");
					seg.modal.closeTimeout(modal.title);
				}

				return;
			});
	},
	lazyLoader = (_) => {
		resetRefresher();
		positionsSubscription = setupRefresher(seg.map.getViewportExtent());
	},
	// LASTPOSITION_REGRESSION
	getPositionOfFavourites = () => {
		getLastKnownVesselsPosition(positionsSource.getFeatures()
			.filter(vessel => vessel.get("favourite_name") && vessel.get("group_name"))
			.map(vessel => Number(vessel.getId()))
		);
	},
	initPositions = () => {
		MAX_TILES = seg.CONFIG.getValue("vessels.positions.MAX_TILES");
		REFRESH_TIME = seg.CONFIG.getValue("vessels.positions.REFRESH_TIME");

		vesselsLayer = seg.layer.get("vessels"),
		positionsLayer = seg.layer.get("positions"),
		positionsSource = positionsLayer.getSource(),
		tracksSource = seg.layer.get("tracks").getSource();

		// eraseClusters();
		getPositionOfFavourites();

		positionsLayer.ol_.on("change:filtersToQuery", () => seg.timeout(() => lazyLoader("change:filtersToQuery"), 100));
		vesselsLayer.ol_.on("change:visible", () => seg.timeout(() => lazyLoader("change:visible"), 100));
		positionsLayer.ol_.on("change:visible", () => seg.timeout(() => lazyLoader("change:visible"), 100));
		seg.map.onViewportChangeEnd(() => seg.timeout(() => lazyLoader("onViewportChangeEnd"), 100));
		seg.map.onScaleChange((newScale, oldScale) => {
			const layerMinScale = positionsLayer.get("minScale");
			resetRefresher();
			if (newScale <= layerMinScale && oldScale > layerMinScale && isVisible())
				setupRefresher(seg.map.getViewportExtent());
		});
	},
	openQueryPanel = () => seg.ui.queryPanel.openTab("Get Track");

module.exports = {
	PSC_SHIP_TYPES,
	getShipType,
	getPSCInspections,
	getParticulars,
	getLabel,
	openQueryPanel,
	shouldUpdatePosition,
	getTracks,
	getLastKnownPosition,
	getLastKnownVesselsPosition,
	getCurrentPosition,
	getLastPositionWithTracks,
	getProperty,
	getEMSAId,
	getNavigationalStatus,
	getDimensions,
	toggleFollow,
	POSITION_SOURCES,
	parseTrackReports,
	mergeDuplicates,
	initPositions,
	lazyLoader
};
