module.exports = {
	placeOfInspection: {
		label: "Place of Inspection",
		template: "<span>{{inspection.InspectionDetails.PlaceOfInspection}}</span>",
		visible: true
	},
	inspectionID: {
		label: "Inspection ID",
		template: "<span>{{inspection.InspectionDetails.InspectionID}}</span>",
		visible: true
	},
	dateOfFinalVisit: {
		label: "Date of Final Visit",
		template: "<span position-timestamp='inspection.InspectionDetails.DateOfFinalVisit'></span>",
		visible: true
	},
	dateOfFirstVisit: {
		label: "Date of First Visit",
		template: "<span position-timestamp='inspection.InspectionDetails.DateOfFirstVisit'></span>",
		visible: true
	},
	inspectionType: {
		label: "Inspection Type",
		template: "<span>{{inspection.InspectionDetails.TypeOfInspection.PSCInspectionType}}</span>",
		visible: true
	},
	nrOfDeficiencies: {
		label: "Nr. of Deficiencies",
		template: "<span>{{inspection.InspectionDetails.NrOfDeficiencies || \"N/A\" }}</span>",
		visible: true
	},
	reportingAuthority: {
		label: "Reporting Authority",
		template: "<span>{{inspection.InspectionDetails.ReportingAuthority | country}}</span>",
		visible: true
	},
	detained: {
		label: "Detained",
		template: "<span>{{ inspection.Detention ? \"Yes\" : \"No\" }}</span>",
		visible: true
	},
	durationOfDetention: {
		label: "Duration of Detention",
		template: "<span>{{ inspection.Detention ? (inspection.Detention.DurationOfDetention + \" days\") : \"N/A\"}}</span>",
		visible: true
	}
};