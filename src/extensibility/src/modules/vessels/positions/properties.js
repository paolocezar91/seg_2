const defaultProperties = {
	mmsi: {
		"label": "MMSI",
		"template": "{{vessel.get('mmsi') || \"N/A\"}}",
		"visible": true,
		"order": 0
	},
	imo: {
		"label": "IMO",
		"template": "{{vessel.get('imo') || \"N/A\"}}",
		"visible": false,
		"order": 1
	},
	ir: {
		"label": "IR",
		"template": "{{vessel.get('ir') || 'N/A'}}",
		"visible": false,
		"order": 2
	},
	shipName: {
		"label": "Name",
		"template": "{{vessel.get('shipName') || \"N/A\"}}",
		"visible": true,
		"order": 3
	},
	callSign: {
		"label": "Call Sign",
		"template": "{{vessel.get('callSign')}}",
		"visible": false,
		"order": 4
	},
	speed: {
		"label": "Speed",
		"template": "<span>{{(vessel.get('speed')>=0)?vessel.get('speed')+' KNOTS':'N/A'}}</span>",
		"visible": true,
		"order": 5
	},
	lat: {
		"label": "Latitude",
		"template": "<span [coordinate]=\"[vessel.get('lat'), 'lat']\"></span>",
		"visible": false,
		"order": 6
	},
	lon: {
		"label": "Longitude",
		"template": "<span [coordinate]=\"[vessel.get('lon'), 'lon']\"></span>",
		"visible": false,
		"order": 7
	},
	flagState: {
		"label": "Flag",
		"template": "{{(vessel.get('flagState') | country) || \"N/A\"}}",
		"visible": false,
		"order": 8
	},
	ns: {
		"label": "Navigational Status",
		"template": "{{getNavigationalStatus(vessel.get('ns')) || \"N/A\"}}",
		"visible": false,
		"order": 9
	},
	src: {
		"label": "Source",
		"template": "{{vessel.get('src')}}",
		"visible": true,
		"order": 10
	},
	hdg: {
		"label": "Heading",
		"template": "{{vessel.get('hdg') ? vessel.get('hdg') + '°' : 'N/A'}}",
		"visible": true,
		"order": 11
	},
	th: {
		"label": "True Heading",
		"template": "<span>{{vessel.get('th')>=0?vessel.get('th')+'°':'N/A'}}</span>",
		"visible": false,
		"order": 12
	},
	orig: {
		"label": "Originator",
		"template": "{{vessel.get('orig') || 'N/A'}}",
		"visible": false,
		"order": 13
	},
	ts: {
		"label": "Timestamp",
		"template": "<span [position-timestamp]=\"vessel.get('ts')\"></span>",
		"visible": false,
		"order": 14
	},
	thetisDescription: {
		label: "Type Description",
		template: "<span>{{vessel.get('thetisTypeDescription') ||'N/A' }}</span>",
		visible: false,
		order: 15
	},
	// destLocode: {
	// 	label: "Destination LOCODE",
	// 	template: "<span>{{vessel.get('loc') || 'N/A'}}</span>",
	// 	visible: false,
	// 	order: 16
	// },
	dimensions_l: {
		label: "Length",
		template: "<span>{{getDimensions(vessel, 'length')}}</span>",
		visible: false,
		order: 17
	},
	dimensions_b: {
		label: "Breadth",
		template: "<span>{{getDimensions(vessel, 'breadth')}}</span>",
		visible: false,
		order: 18
	},
	// from positionDetail, still not available
	destination: {
		label: "Destination LOCODE",
		template: "<span>{{'N/A'}}</span>",
		visible: false,
		order: 19
	},
	ETAtoDestination: {
		label: "ETA to Destination",
		template: "<span>{{'N/A'}}</span>",
		visible: false,
		order: 20
	},
	draught: {
		label: "Draught",
		template: "<span>{{'N/A'}}</span>",
		visible: false,
		order: 21
	},
	cargoType: {
		label: "Cargo Type",
		template: "<span>{{'N/A'}}</span>",
		visible: false,
		order: 22
	},
	AISType: {
		label: "AIS Type",
		template: "<span>{{'N/A'}}</span>",
		visible: false,
		order: 23
	},
	positioningSystem: {
		label: "Positioning System",
		template: "<span>{{'N/A'}}</span>",
		visible: false,
		order: 24
	}
};

const mapProperties = (options) => seg.utils.mapItemProperties(defaultProperties, options);

const getTooltipDisplayProperties = () => {
	const props = Object.assign({}, defaultProperties);
	Object.values(defaultProperties).forEach((values) => {
		const visibility = seg.preferences.workspace.get("Vessels.tooltip." + values.label);
		if(typeof visibility !== "undefined")
			values.visible = visibility;
	});
	return props;
};

const getProperty = key => defaultProperties[key];

module.exports =  {
	mapProperties,
	getTooltipDisplayProperties,
	getProperty
};
