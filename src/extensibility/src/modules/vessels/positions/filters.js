/**@namespace extensibility.modules.vessels*/
/**
 * Enum for position filters
 * @readonly
 * @enum{object}
 */
const {PSC_SHIP_TYPES} = require("../api"),
	{
		FLAG_STATES,
		EU_COUNTRIES,
		ICCAT_PERMIT_TYPES,
		POSITION_SOURCE,
		LRIT_VESSEL_TYPE,
		NAVIGATION_STATUS,
		GT_CLASS,
		LOA_CLASS,
		PWR_CLASS,
		USER_RISK_STATUS,
		RISK_LEVELS,
		VOYAGE_INFORMATION,
		FAO_TYPES,
		GEAR_TYPES
	} = require("./filters.enum");

const flagRegister = (vessel) => vessel.get("flagState") || (vessel.get("csdb") && vessel.get("csdb").Current_FlagRegistry && vessel.get("csdb").Current_FlagRegistry.content);

const buildFAOTypeMultiLevelFilters = (name) => ({
	name,
	fn: vessel => Object.values(FAO_TYPES[name]).some(type => vessel.get("sai") && vessel.get("sai")["Type"] === type),
	options: (() => {
		const filters = {};
		for (const key in FAO_TYPES[name])
			filters[key] = vessel => vessel.get("sai") && vessel.get("sai")["Type"] === FAO_TYPES[name][key];
		return filters;
	})()
});

const buildFAOTypeOneLevelFilters = (name) => Object.assign({}, {name, fn: vessel => vessel.get("sai") && vessel.get("sai")["Type"] === FAO_TYPES[name]});

// Default AfterSetActive callback
// Receives "this" (Filter Class object bindings),
// "key" of filtersToQuery
// "valueGetter" is a function to determine the value of the key, each case one function
const afterSetActiveCallback = (self, key, valueGetter, alternativeKey) => {
	return () => {
		let filtersToQuery = Object.assign({}, seg.layer.get("positions").ol_.get("filtersToQuery")), value;

		if(!filtersToQuery)
			filtersToQuery = {};

		if (self.active){
			if (!filtersToQuery[alternativeKey || key])
				filtersToQuery[alternativeKey || key] = [];

			self.options.forEach(option => {
				value = valueGetter(option);

				if(value){
					if (!option.active)
						filtersToQuery[alternativeKey || key] = filtersToQuery[alternativeKey || key].filter(f => f !== value);
					else if(!~filtersToQuery[alternativeKey || key].findIndex(ftq => ftq === value))
						filtersToQuery[alternativeKey || key].push(value);
				}
			});
		} else
			filtersToQuery[alternativeKey || key] = [];

		seg.layer.get("positions").ol_.set("filtersToQuery", filtersToQuery);
	};
};

const filters = [
	{
		name: "Voyage information",
		inclusive: true,
		options: (() => {
			const filters = {};

			for (const key in VOYAGE_INFORMATION){
				if(!VOYAGE_INFORMATION[key].isPresent)
					filters[key] = vessel => vessel.get("enrichment") && vessel.get("enrichment")[VOYAGE_INFORMATION[key].enrichment_key] === VOYAGE_INFORMATION[key].value;
				else
					filters[key] = vessel => vessel.get("enrichment") && vessel.get("enrichment")[VOYAGE_INFORMATION[key].enrichment_key];
			}

			filters["N/A"] = vessel => {
				const enrichment = vessel.get("enrichment");
				return !enrichment ||
					(enrichment &&
						(
							((enrichment.HazmatOnBoardYorN === "N") || (!enrichment.HazmatOnBoardYorN)) &&
							((enrichment.mrsHazmatOnBoard === "N") || (!enrichment.mrsHazmatOnBoard)) &&
							((enrichment.activeIncidents === "N") || (!enrichment.activeIncidents)) &&
							( /*(enrichment.CurrentSecurityLevel === "SL1") || */ (!enrichment.CurrentSecurityLevel)) &&
							( /*(enrichment.WasteDeliveryStatus === "None") || */ (!enrichment.WasteDeliveryStatus)) &&
							((enrichment.exemptionYorN === "N") || (!enrichment.exemptionYorN)) &&
							((enrichment.detainedYorN === "N") || (!enrichment.detainedYorN)) &&
							((enrichment.bannedYorN === "N") || (!enrichment.bannedYorN)) &&
							((!enrichment.SHT))
						)
					);
			};
			return filters;
		})()
	},
	{
		name: "Position Source",
		options: (() => {
			const filters = {};
			for (const key in POSITION_SOURCE)
				filters[key] = vessel => vessel.get("src") === POSITION_SOURCE[key];
			filters["N/A"] = vessel => !vessel.get("src") || vessel.get("src") === "" || typeof vessel.get("src") === "undefined";
			return filters;
		})(),
		afterSetActive() {
			return afterSetActiveCallback(this, "src", (option) => (option.name || undefined), "source");
		}
	},
	{
		name: "Vessel Flag",
		options: (() => {
			let filters = {};
			filters["N/A"] = vessel => !vessel.get("flagState") || (vessel.get("flagState") === "" || typeof vessel.get("flagState") === "undefined") ||
				!(flagRegister(vessel)) || ((flagRegister(vessel)) === "" || typeof (flagRegister(vessel)) === "undefined");
			filters = Object.assign(filters, Object.keys(FLAG_STATES).reduce((options, index) => {
				const {code, name} = FLAG_STATES[index];
				options[name] = vessel => (flagRegister(vessel)) === code;
				return options;
			}, {}));
			return filters;
		})(),
		afterSetActive() {
			return afterSetActiveCallback(this, "flagState", (option) => (FLAG_STATES.find(flag => flag.name === option.name) || {}).code, "flag");
		}
	},
	{
		name: "Vessel Type (PSC Types)",
		options: (() => {
			const filters = {};
			for (const key in PSC_SHIP_TYPES)
				filters[key] = vessel => vessel.get("shipType") === PSC_SHIP_TYPES[key];
			filters["other"] = vessel => (typeof vessel.get("shipType") === "undefined") || (Object.values(PSC_SHIP_TYPES).indexOf(vessel.get("shipType")) === -1);
			return filters;
		})(),
		afterSetActive() {
			return afterSetActiveCallback(this, "shipType", (option) => (PSC_SHIP_TYPES[option.name] || undefined), "vesselType");
		}
	},
	{
		name: "Vessel Type (LRIT)",
		options: (() =>  {
			const filters = {};
			for (const key in LRIT_VESSEL_TYPE)
				filters[key] = vessel => vessel.get("shipType") === LRIT_VESSEL_TYPE[key];
			filters["other"] = vessel => (typeof vessel.get("shipType") === "undefined") || (Object.values(LRIT_VESSEL_TYPE).indexOf(vessel.get("shipType")) === -1);
			return filters;
		})()
	},
	{
		name: "Navigation Status (AIS)",
		options: (() => {
			const filters = {};
			for (const key in NAVIGATION_STATUS)
				filters[key] = vessel => vessel.get("ns") === NAVIGATION_STATUS[key];
			filters["other"] = vessel =>  vessel.get("ns") > 8;
			filters["N/A"] = vessel => typeof vessel.get("ns") === "undefined";
			return filters;
		})()
	},
	{
		name: "User Risk Status",
		options: (() => {
			const filters = {};
			for (let key = 0; key < USER_RISK_STATUS.length; key++)
				filters[USER_RISK_STATUS[key]] = vessel => vessel.get("risk") === USER_RISK_STATUS[key];
			filters["None"] = vessel => ((typeof vessel.get("risk") === "undefined") || (vessel.get("risk") === "None"));
			return filters;
		})()
	},
	{
		name: "Fishing vessels",
		options: {
			"ICCAT vessels": vessel => typeof vessel.get("sai") === "object",
			"Non ICCAT fishing vessel": vessel => (typeof vessel.get("sai") !== "object" && vessel.get("shipType") === 30) || vessel.get("ns") === 7,
			"Non fishing vessel": vessel => typeof vessel.get("sai") !== "object" && vessel.get("shipType") !== 30 && vessel.get("ns") !== 7
		}
	},
	{
		name: "Risk Levels",
		options: (() => {
			const filters = {};
			for(const key in RISK_LEVELS)
				filters[key] = vessel => vessel.get("sai") && vessel.get("sai").RiskAssessment === key.toUpperCase();
			filters["Unknown"] = vessel => !vessel.get("sai") || vessel.get("sai") && !vessel.get("sai").RiskAssessment;
			return filters;
		})()
	},
	{
		name: "Special Categories",
		options: {
			"WFP and AMISOM": vessel => vessel.get("sai") && vessel.get("sai").Category === "WFP",
			"Vessel of interest": vessel => vessel.get("sai") && vessel.get("sai").Category === "VOI",
			"Pirated": vessel => vessel.get("sai") && vessel.get("sai").pirated,
			"Not Categorised": vessel => !vessel.get("sai") || (!vessel.get("sai").Category && !vessel.get("sai").pirated)
		}
	},
	{
		name: "ICCAT",
		options: seg.layer.filter.buildMultiLevelFilters({
			name: "ICCAT",
			inclusive: true,
			options: [
				{
					name: "FV list/ICCAT FPV",
					fn: vessel => vessel.get("sai") && vessel.get("sai")["ICCAT CODE"] === "Y"
				},
				{
					name: "Has Fishing Permit",
					fn: vessel => vessel.get("sai") && vessel.get("sai")["CATCH PERMIT"] === "Y"
				},
				{
					name: "Fishing Permit Type",
					fn: vessel => vessel.get("sai") && Object.keys(ICCAT_PERMIT_TYPES).some(type => vessel.get("sai")["PERMIT TYPE"] === type),
					inclusive: true,
					options: (() => {
						const filters = [];
						for (const key in ICCAT_PERMIT_TYPES)
							filters.push({
								name: ICCAT_PERMIT_TYPES[key],
								fn: vessel => vessel.get("sai")["PERMIT TYPE"] === key
							});
						return filters;
					})()
				},
				{
					name: "N/A",
					fn: vessel => !vessel.get("sai") || (vessel.get("sai")["ICCAT CODE"] === "" || typeof vessel.get("sai")["ICCAT CODE"] === "undefined")
				}
			]
		}, null)
	},
	{
		name: "NAFO",
		options: {
			"NAFO FV list/ NAFO FPV": vessel => vessel.get("sai") && vessel.get("sai")["NAFO CODE"] === "Y",
			"GHL Fishing": vessel => vessel.get("sai") && vessel.get("sai")["GHL"] === "Y",
			"N/A": vessel => !vessel.get("sai") || ((vessel.get("sai")["GHL"] === "" || typeof vessel.get("sai")["GHL"] === "undefined") && (vessel.get("sai")["NAFO CODE"] === "" || typeof vessel.get("sai")["NAFO CODE"] === "undefined"))
		}
	},
	{
		name: "NEAFC",
		options: {
			"NEAFC FV list/ NEAFC FPV": vessel => vessel.get("sai") && vessel.get("sai")["NEAFC CODE"] === "Y",
			"N/A": vessel => !vessel.get("sai") || (vessel.get("sai")["NEAFC CODE"] === "" || typeof vessel.get("sai")["NEAFC CODE"] === "undefined")
		}
	},
	{
		name: "EU Licence",
		options: {
			"EU Licence": vessel => vessel.get("sai") && vessel.get("sai")["EU CODE"] === "Y",
			"N/A": vessel => !vessel.get("sai") || (vessel.get("sai")["EU CODE"] === "" || typeof vessel.get("sai")["EU CODE"] === "undefined")
		}
	},
	{
		name: "Gear Type",
		options: seg.layer.filter.buildMultiLevelFilters({
			name: "Gear Type",
			inclusive: true,
			options: (() => {
				const filters = [];
				for (const key in GEAR_TYPES) {
					filters.push({
						name: key,
						inclusive: true,
						fn: vessel => GEAR_TYPES[key].some(type => vessel.get("sai") && vessel.get("sai")["Gear"] && vessel.get("sai")["Gear"] === type),
						options: (() => {
							const filters = {};
							GEAR_TYPES[key].forEach(filter => {
								filters[filter] = vessel => vessel.get("sai") && vessel.get("sai")["Gear"] && vessel.get("sai")["Gear"] === filter;
							});
							return filters;
						})()
					});
				}
				filters.push({name: "N/A", fn: vessel => !vessel.get("sai") || (vessel.get("sai")["Gear"] === "" || typeof vessel.get("sai")["Gear"] === "undefined")});
				return filters;
			})()
		}, null)
	},
	{
		name: "Flag",
		options: seg.layer.filter.buildMultiLevelFilters({
			name: "Flag",
			options: (() => {
				return [
					{
						name: "By Country",
						fn: vessel => vessel.get("flagState"),
						options: (() => Object.keys(FLAG_STATES).reduce((options, index) => {
							const {code, name} = FLAG_STATES[index];
							options[name] = vessel => (flagRegister(vessel)) === code;
							return options;
						}, {}))()
					}, {
						name: "EU/Non EU",
						fn: vessel => vessel.get("flagState"),
						options: {
							"EU": vessel => {return vessel.get("flagState") && ~(EU_COUNTRIES.indexOf(vessel.get("flagState")));},
							"Non EU": vessel => vessel.get("flagState") && (EU_COUNTRIES.indexOf(vessel.get("flagState")) < 0)
						}
					}, {
						name: "N/A",
						fn: vessel => !vessel.get("flagState") || (vessel.get("flagState") === "" || typeof vessel.get("flagState") === "undefined") ||
							!(flagRegister(vessel)) || ((flagRegister(vessel)) === "" || typeof (flagRegister(vessel)) === "undefined")
					}
				];
			})()
		}, null)
	},
	{
		name: "FAO Types",
		options : seg.layer.filter.buildMultiLevelFilters({
			name: "FAO Types",
			options: [
				buildFAOTypeMultiLevelFilters("Trawlers"),
				buildFAOTypeMultiLevelFilters("Seiners"),
				buildFAOTypeMultiLevelFilters("Dredgers"),
				buildFAOTypeMultiLevelFilters("Lift Netters"),
				buildFAOTypeOneLevelFilters("Gilnetters"),
				buildFAOTypeMultiLevelFilters("Trap Setters"),
				buildFAOTypeMultiLevelFilters("Liners"),
				buildFAOTypeOneLevelFilters("Using Pumps for Fishing"),
				buildFAOTypeMultiLevelFilters("Multipurpose Vessels"),
				buildFAOTypeOneLevelFilters("Recreational Fishing Vessels"),
				buildFAOTypeOneLevelFilters("Fishing Vessel Not Spec"),
				buildFAOTypeMultiLevelFilters("Motherships"),
				buildFAOTypeOneLevelFilters("Fishing Carriers"),
				buildFAOTypeOneLevelFilters("Holpital Vessel"),
				buildFAOTypeOneLevelFilters("Protection And Survey"),
				buildFAOTypeOneLevelFilters("Fishery Research"),
				buildFAOTypeOneLevelFilters("Fishery Training"),
				buildFAOTypeOneLevelFilters("Non-fishing vessels"),
				{name: "N/A", fn: vessel => !vessel.get("sai") || (vessel.get("sai") && vessel.get("sai")["Type"] === "" || vessel.get("sai") && typeof vessel.get("sai")["Type"] === "undefined")}
			]
		}, null)
	},
	{
		name: "PWR Class",
		options: (() => {
			const filters = {};
			for (const key in PWR_CLASS)
				filters[key] = vessel => vessel.get("sai") && vessel.get("sai")["PWR_CLASS"] && Number(vessel.get("sai")["PWR_CLASS"]) === Number(PWR_CLASS[key]);
			filters["N/A"] = vessel => !vessel.get("sai") || (vessel.get("sai")["PWR_CLASS"] === "" || typeof vessel.get("sai")["PWR_CLASS"] === "undefined");
			return filters;
		})()
	},
	{
		name: "GT Class",
		options: (() => {
			const filters = {};
			for (const key in GT_CLASS)
				filters[key] = vessel => vessel.get("sai") && vessel.get("sai")["GT_CLASS"] && Number(vessel.get("sai")["GT_CLASS"]) === Number(GT_CLASS[key]);
			filters["N/A"] = vessel => !vessel.get("sai") || (vessel.get("sai")["GT_CLASS"] === "" || typeof vessel.get("sai")["GT_CLASS"] === "undefined");
			return filters;
		})()
	},
	{
		name: "LOA Class",
		options: (() => {
			const filters = {};
			for (const key in LOA_CLASS)
				filters[key] = vessel => vessel.get("sai") && vessel.get("sai")["LOA_CLASS"] && Number(vessel.get("sai")["LOA_CLASS"]) === Number(LOA_CLASS[key]);
			filters["N/A"] = vessel => !vessel.get("sai") || (vessel.get("sai")["LOA_CLASS"] === "" || typeof vessel.get("sai")["LOA_CLASS"] === "undefined");
			return filters;
		})()
	}
];

const buildFilters = (config) => {
	const _filters = filters.filter(filter => config.filters.indexOf(filter.name) !== -1);
	if (config.filters_override && config.filters_override.length > 0)
		return _filters.map(filter => config.filters_override.find(filter_o => filter.name === filter_o.name) || filter);
	return _filters;
};

module.exports = {
	buildFilters
};
