const displayProperties = {
	"ICCAT ID": {
		"label": "ICCAT ID",
		"template": "{{vessel.get('sai').ICCATID || 'N/A'}}"
	},
	"FLAG": {
		"label": "Flag",
		"template": "{{vessel.get('sai').FLAG | country}}"
	},
	"EU CODE": {
		"label": "EU Code",
		"template": "{{vessel.get('sai').EUCODE || 'N/A'}}"
	},
	"IR": {
		"label": "CFR",
		"template": "{{vessel.get('sai').IR || 'N/A'}}"
	},
	"Vessel Name": {
		"label": "Reg Vessel Name",
		"template": "{{vessel.get('sai').VesselName || 'N/A'}}"
	},
	"IRCS": {
		"label": "IRCS",
		"template": "{{vessel.get('sai').IRCS || 'N/A'}}"
	},
	"Ext Marking": {
		"label": "Ext Marking",
		"template": "{{vessel.get('sai').ExtMarking || 'N/A'}}"
	},
	"Port_Name": {
		"label": "Reg Port Name",
		"template": "{{vessel.get('sai').Port_Name || 'N/A' }}"
	},
	"VMS Code": {
		"label": "VMS Code",
		"template": "{{vessel.get('sai').VMSCode || 'N/A'}}"
	},
	"MMSI": {
		"label": "Registry MMSI",
		"template": "{{vessel.get('sai').MMSI || 'N/A'}}"
	},
	"GT": {
		"label": "GT",
		"template": "{{vessel.get('sai').GT_CLASS && vesselCommandInfo.getGTClass(vessel.get('sai').GT_CLASS) || 'N/A'}}"
	},
	"LOA": {
		"label": "LOA",
		"template": "{{vessel.get('sai').LOA_CLASS && vesselCommandInfo.getLOAClass(vessel.get('sai').LOA_CLASS) || 'N/A'}}"
	},
	"KW": {
		"label": "KW",
		"template": "{{vessel.get('sai').KW || 'N/A'}}"
	},
	"GHL": {
		"label": "GHL CODE",
		"template": "{{vessel.get('sai').GHL || 'N/A'}}"
	},
	"NAFO CODE": {
		"label": "Nafo Code",
		"template": "{{vessel.get('sai').NAFOCODE || 'N'}}"
	},
	"CATCH  PERMIT": {
		"label": "Catch Permit",
		"template": "{{vessel.get('sai').CATCHPERMIT || 'N/A'}}"
	},
	"PERMIT  TYPE": {
		"label": "Permit Type",
		"template": "{{vessel.get('sai').PERMITTYPE || 'N/A'}}"
	},
	"PERMIT  FROM": {
		"label": "Permit From",
		"template": "{{vessel.get('sai').PERMITFROM || 'N/A'}}"
	},
	"PERMIT  TO": {
		"label": "Permit Until",
		"template": "{{vessel.get('sai').PERMITTO || 'N/A'}}"
	},
	"Gear": {
		"label": "Gear",
		"template": "{{vessel.get('sai').Gear || 'N/A'}}"
	},
	"NEAFC CODE": {
		"label": "NEAF Code",
		"template": "{{vessel.get('sai').NEAFCCODE || 'N'}}"
	},
	"ICCAT  CODE": {
		"label": "Iccat Code",
		"template": "{{vessel.get('sai').ICCATCODE || 'N'}}"
	},
	"FAO Type": {
		"label": "Type",
		"template": "{{vessel.get('sai').Type || 'N/A'}}"
	}
};

module.exports = {
	template: `
		<ul class="flexbox flex-wrap flex-direction-vertical property-list efca-properties">
			<li ng-repeat="(key, option) in displayProperties">
				<label bind-template="option.label"></label>
				<value bind-template="option.template" ng-click="copyToClipboard($event)"></value>
			</li>
		</ul>
		<style>
			ul.property-list.efca-properties{
				padding:10px;
			}
		</style>
	`,
	bindings: {
		displayProperties
	}
};
