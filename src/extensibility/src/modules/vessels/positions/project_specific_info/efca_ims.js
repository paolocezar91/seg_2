const CommonSpecificInformation = {
	"IR": {
		"label": "CFR",
		"template": "{{vessel.get('sai').IR || 'N/A'}}"
	},
	"ExtMarking": {
		"label": "Ext. Marking",
		"template": "{{vessel.get('sai').ExtMarking || 'N/A'}}"
	},
	"Gear": {
		"label": "Main Gear (EU)",
		"template": "{{vessel.get('sai').Gear || 'N/A'}}"
	},
	"PowerClass": {
		"label": "PWR Class",
		"template": "{{vessel.get('sai').KW_CLASS && vesselCommandInfo.getKWClass(vessel.get('sai').KW_CLASS) || 'N/A'}}"
	},
	"GrossTonnageClass": {
		"label": "GT Class",
		"template": "{{vessel.get('sai').GT_CLASS && vesselCommandInfo.getGTClass(vessel.get('sai').GT_CLASS) || 'N/A'}}"
	},
	"LenghtOverallClass":{
		"label": "LOA Class",
		"template": "{{vessel.get('sai').LOA_CLASS && vesselCommandInfo.getLOAClass(vessel.get('sai').LOA_CLASS) || 'N/A'}}"
	},
	"FAOType":{
		"label": "FV Type (FAO)",
		"template": "{{vessel.get('sai').LOA_CLASS && vesselCommandInfo.getFAOType(vessel.get('sai').Type) || 'N/A'}}"
	},
	"VMSObligation":{
		"label": "VMS Obligation",
		"template": "{{vessel.get('sai').VMSCode || 'N/A'}}"
	},
	"Homeport":{
		"label": "Homeport",
		"template": "{{vessel.get('sai').Port_Name || 'N/A'}}"
	}
};

const ICCATSpecificInformation = {
	"ICCATID": {
		"label": "ICCAT Number",
		"template": "{{vessel.get('sai').ICCATID || 'N/A'}}"
	},
	"OnICCATList": {
		"label": "On ICCAT List",
		"template": "{{vessel.get('sai').ICCATCODE || 'N'}}"
	},
	"FishingPermit": {
		"label": "Permit",
		"template": "{{vessel.get('sai').CATCHPERMIT || 'N/A'}}"
	},
	"FishingPermitValidityStart": {
		"label": "Permit from",
		"template": "{{vessel.get('sai').PERMITFROM || 'N/A'}}"
	},
	"FishingPermitValidityEnd": {
		"label": "Permit until",
		"template": "{{vessel.get('sai').PERMITTO || 'N/A'}}"
	},
	"FishingPermitType": {
		"label": "Permit Type",
		"template": "{{vessel.get('sai').PERMITTYPE || 'N/A'}}"
	}
};

const NAFOSpecificInformation = {
	"OnNAFOList": {
		"label": "NAFO Code",
		"template": "{{vessel.get('sai').NAFOCODE || 'N'}}"
	},
	"GHL": {
		"label": "GHL Code",
		"template": "{{vessel.get('sai').GHL || 'N/A'}}"
	}
};

const NEAFCSpecificInformation = {
	"OnNEAFCList": {
		"label": "NEAFC CODE",
		"template": "{{vessel.get('sai').NEAFCCODE || 'N'}}"
	}
};

const EUSpecificInformation = {
	"FishingLicense": {
		"label": "EU License",
		"template": "{{vessel.get('sai').EUCODE || 'N/A'}}"
	}
};


module.exports = {
	template: `
	<tab-group id="efca-iuu-information">
		<tab persistent="true" label="Common">
				<pager class="flex-1">
					<pager-content>
						<ul class="flexbox flex-wrap flex-direction-vertical property-list">
							<li ng-repeat="(key, option) in CommonSpecificInformation">
								<label bind-template="option.label"></label>
								<value ng-click="copyToClipboard($event)" bind-template="option.template"></value>
							</li>
						</ul>
					</pager-content>
				</pager>
		</tab>
		<tab persistent="true" persistent="true" label="ICCAT">
				<pager class="flex-1">
					<pager-content>
						<ul class="flexbox flex-wrap flex-direction-vertical property-list">
							<li ng-repeat="(key, option) in ICCATSpecificInformation">
								<label bind-template="option.label"></label>
								<value ng-click="copyToClipboard($event)" bind-template="option.template"></value>
							</li>
						</ul>
					</pager-content>
				</pager>
		</tab>
		<tab persistent="true" label="NAFO">
				<pager class="flex-1">
					<pager-content>
						<ul class="flexbox flex-wrap flex-direction-vertical property-list">
							<li ng-repeat="(key, option) in NAFOSpecificInformation">
								<label bind-template="option.label"></label>
								<value ng-click="copyToClipboard($event)" bind-template="option.template"></value>
							</li>
						</ul>
					</pager-content>
				</pager>
		</tab>
		<tab persistent="true" label="NEAFC ">
				<pager class="flex-1">
					<pager-content>
						<ul class="flexbox flex-wrap flex-direction-vertical property-list">
							<li ng-repeat="(key, option) in NEAFCSpecificInformation">
								<label bind-template="option.label"></label>
								<value ng-click="copyToClipboard($event)" bind-template="option.template"></value>
							</li>
						</ul>
					</pager-content>
				</pager>
		</tab>
		<tab persistent="true" label="EU">
				<pager class="flex-1">
					<pager-content>
						<ul class="flexbox flex-wrap flex-direction-vertical property-list">
							<li ng-repeat="(key, option) in EUSpecificInformation">
								<label bind-template="option.label"></label>
								<value ng-click="copyToClipboard($event)" bind-template="option.template"></value>
							</li>
						</ul>
					</pager-content>
				</pager>
		</tab>
	</tab-group>
	<style>
		#efca-iuu-information>tab>pager{
			padding:10px;
			height:260px;
		}
		#efca-iuu-information>tab>pager>pager-content>ul>li>label{
			font-weight:bold;
		}
	</style>`,
	bindings: {
		CommonSpecificInformation,
		ICCATSpecificInformation,
		NAFOSpecificInformation,
		NEAFCSpecificInformation,
		EUSpecificInformation
	}
};
