const EFCA = require("./efca"),
	EUNAVFOR = require("./eunavfor"),
	EFCA_IMS = require("./efca_ims");

module.exports = {
	ATLANTIC: EFCA,
	OPR_EFCA_ATLANTIC: EFCA,
	MEDITERRANEAN: EFCA,
	OPR_EFCA_MEDITERRANEAN: EFCA,
	EUNAVFOR,
	OPR_EUNAVFOR: EUNAVFOR,
	EFCA_IMS: EFCA_IMS,
	OPR_EFCA_IMS: EFCA_IMS
};
