const spmLabels = {
	AIS_On_GOA : "AIS On GOA",
	AIS_on_HOA : "AIS on HOA",
	Armed_Security : "Armed Security",
	CCTV : "CCTV",
	Citadel : "Citadel",
	Crew_Briefing : "Crew Briefing",
	Crew_Drills_Completed : "Crew Drills Completed",
	DUMMIES_POSTED : "Dummies Posted",
	External_Communication_Plan : "External Communication Plan",
	Extra_Lookouts : "Extra Lookouts",
	Firehoses_Rigged : "Firehoses Rigged",
	Firepump_Ready : "Firepump Ready",
	Group : "Group",
	Locked_Doors : "Locked Doors",
	Manned_Engine_Room : "Manned Engine Room",
	Night_Vision_Optics : "Night Vision Optics",
	Outboard_Ladders_Stowed : "Outboard Ladders Stowed",
	Part_of_National_Convoy : "Part of National Convoy",
	Razor_Wire : "Razor Wire",
	Unarmed_Security : "Unarmed Security",
	ContactInfo: "ContactInfo",
	Email: "Email"
};

const riskLabels = {
	LOW : "Low",
	MODERATE : "Moderate",
	SEVERE : "Severe",
	SUBSTANTIAL : "Substantial"
};

const categoryLabels = {
	VOI : "Vessel of Interest",
	WFP : "WFP"
};

const getSPM = sai => {
	const templates = [];

	Object.keys(sai).forEach(key => {
		if(key in spmLabels)
			templates.push(spmLabels[key]);
	});

	return templates.join(", ");
};

const displayProperties = {
	"Email": {
		"label": "Email",
		"template": "{{vessel.get('sai').Email || \"N/A\"}}"
	},
	"ContactInfo": {
		"label": "Contact Info",
		"template": "{{vessel.get('sai').ContactInfo || \"N/A\"}}"
	},
	"Pirated": {
		"label": "Pirated",
		"template": "{{vessel.get('sai').pirated?\"Yes\":\"No\"}}"
	},
	"Risk": {
		"label": "Risk",
		"template": "{{riskLabels[vessel.get('sai').RiskAssessment]}}"
	},
	"Category": {
		"label": "Category",
		"template": "{{categoryLabels[vessel.get('sai').Category] || \"N/A\"}}"
	},
	"SPM": {
		"label": "SPM",
		"template": "{{getSPM(vessel.get('sai'))}}"
	}
};

module.exports = {
	template: `
	<ul class="flexbox flex-wrap flex-direction-vertical property-list" id="eunavfor-specific">
		<li ng-repeat="(key, option) in displayProperties">
			<label bind-template="option.label"></label>
			<value bind-template="option.template" ng-click="copyToClipboard($event)"></value>
		</li>
	</ul>
	<style>
		panel#enrichment-information #eunavfor-specific {
			flex-wrap: nowrap;
			padding: 10px;
		}
		panel#enrichment-information #eunavfor-specific > li {
			width: auto;
			min-height: initial;
		}
	</style>`,
	bindings: {
		riskLabels,
		categoryLabels,
		getSPM,
		displayProperties
	}
};
