const {getSelected} = require("./symbolizers/index.js");

module.exports = {
	name: "Vessel Symbology",
	selectedSymbology: function() {
		return getSelected().name;
	}
};
