const waste = {
	basicinfo: {
		wasteProperties: {
			WasteDeliveryStatus: {
				label: "Waste Delivery Status",
				template: "<span>{{vessel.get('enrichment').WasteDeliveryStatus || \"N/A\"}}</span>",
				visible: true
			}
		},
		wasteLastPortProperties: {
			LastPortDelivered: {
				label: "Last Port Delivered",
				template: "<span ng-if=\"vessel.get('waste').LastPortDelivered\" locode=\"vessel.get('waste').LastPortDelivered\"></span>",
				visible: true
			},
			LastPortDeliveredDate: {
				label: "Last Port Delivered Date",
				template: "{{vessel.get('waste').LastPortDeliveredDate || \"N/A\"}}",
				visible: true
			}
		}
	},
	details: {
		wasteDetailsHeader: {
			ProviderOfLastUpdate: {
				label: "Last Port Delivered",
				template: "<span ng-if=\"vessel.get('waste').ProviderOfLastUpdate\" locode=\"vessel.get('waste').ProviderOfLastUpdate\"></span>",
				visible: true
			},
			LastUpdateRecievedAt: {
				label: "Last Port Delivered Date",
				template: "<span>{{vessel.get('waste').LastUpdateRecievedAt || \"N/A\"}}</span>",
				visible: true
			}
		},
		wasteDetailListFields: {
			wastecode: {
				label: "Waste Type",
				template: "{{wasteTypeValues(item.WasteType.WasteCode) || \"N/A\"}}",
				visible: true
			},
			wastedesc: {
				label: "Waste Description",
				template: "{{item.WasteType.WasteDescription || \"N/A\"}}",
				visible: true
			},
			wastedelivered: {
				label: "Amount to be Delivered",
				template: "{{item.ToBeDelivered.Quantity}} {{item.ToBeDelivered.UnitOfMeasurement}}",
				visible: true
			}
		}
	},
	ttt: {
		detailFields: {
			wastecode: {
				label: "Waste Type",
				template: "{{wasteTypeValues(item.WasteType.WasteCode) || \"N/A\"}}",
				visible: true
			},
			wastedesc: {
				label: "Waste Description",
				template: "{{item.WasteType.WasteDescription || \"N/A\"}}",
				visible: true
			},
			wastedelivered: {
				label: "Amount to be Delivered",
				template: "{{item.ToBeDelivered.Quantity}} {{item.ToBeDelivered.UnitOfMeasurement}}",
				visible: true
			},
			wastemaxstorage: {
				label: "Maximum dedicated storage capacity",
				template: "{{item.MaxStorage.Quantity}} {{item.MaxStorage.UnitOfMeasurement}}",
				visible: true
			},
			onboard: {
				label: "Amount of waste retained on board",
				template: "{{item.RetainedOnBoard.Quantity}} {{item.RetainedOnBoard.UnitOfMeasurement}}",
				visible: true
			},
			PortDeliveryRemainingWaste: {
				label: "Port of delivery of remaining waste",
				template: "{{item.PortDeliveryRemainingWaste || \"N/A\"}}",
				visible: true
			},
			EstimateGenerated: {
				label: "Estimated amount of waste to be generated",
				template: "{{item.EstimateGenerated.Quantity }} {{item.EstimateGenerated.UnitOfMeasurement}}",
				visible: true
			},
			DeliveredAtLastPort: {
				label: "Waste Delivered at Last Port ",
				template: "{{item.DeliveredAtLastPort.Quantity }} {{item.DeliveredAtLastPort.UnitOfMeasurement}}",
				visible: true
			}
		}
	}
};

const security = {
	basicinfo: {
		securityProperties: {
			currentSecurityLevel: {
				label: "Current Security Level",
				template: "{{vessel.get('security').CurrentSecurityLevel || \"N/A\"}}",
				visible: true
			}
		},
		securityAgentProperties: {
			phone: {
				label: "Phone",
				template: "{{vessel.get('security').AgentInPortAtArrival.Phone || \"N/A\"}}",
				visible: true
			},
			name: {
				label: "Name",
				template: "{{vessel.get('security').AgentInPortAtArrival.AgentName || \"N/A\"}}",
				visible: true
			},
			email: {
				label: "E-mail",
				template: "{{vessel.get('security').AgentInPortAtArrival.EMail || \"N/A\"}}",
				visible: true
			},
			fax: {
				label: "Fax",
				template: "{{vessel.get('security').AgentInPortAtArrival.Fax || \"N/A\"}}",
				visible: true
			}
		}
	},
	details: {
		previousCallAtPortFacilityFields: {
			departureDate: {
				label: "Departure",
				template: "{{item.DateOfDeparture}}",
				visible: true
			},
			arrivalDate: {
				label: "Arrival",
				template: "{{item.DateOfArrival}}",
				visible: true
			},
			port: {
				label: "Port",
				template: "<span ng-if=\"item.Port\" locode=\"item.Port\"></span>",
				visible: true
			},
			SecurityLevel: {
				label: "Sec. Level ",
				template: "{{item.SecurityLevel}}",
				visible: true
			}
		},
		shipToShipActivityFields: {
			DateFrom: {
				label: "Date From",
				template: "{{item.DateFrom}}",
				visible: true
			},
			Activity: {
				label: "Activity",
				template: "{{item.Activity}}",
				visible: true
			},
			SecurityMeasures: {
				label: "Security Measues",
				template: "{{item.SecurityMeasures}}",
				visible: true
			},
			DateTo: {
				label: "Date To",
				template: "{{item.DateTo}}",
				visible: true
			},
			Location: {
				label: "Location",
				template: "{{item.Location.LocationName}}",
				visible: true
			}
		},
		ISSCProperties: {
			ISSCType: {
				label: "ISSC Type",
				template: "{{vessel.get('securityDetails').ISSC.ISSCType}}",
				visible: true
			},
			ExpiryDate: {
				label: "Expiry Date",
				template: "{{vessel.get('securityDetails').ISSC.ExpiryDate}}",
				visible: true
			},
			IssuerType: {
				label: "Issuer Type",
				template: "{{vessel.get('securityDetails').ISSC.IssuerType}}",
				visible: true
			},
			Issuer: {
				label: "Issuer",
				template: "{{vessel.get('securityDetails').ISSC.Issuer}}",
				visible: true
			}
		},
		securityDetails: {
			approvedSecurityDetails: {
				label: "Aproved Security Details",
				template: "{{vessel.get('securityDetails').ApprovedSecurityDetails}}",
				visible: true
			},
			approvedSecurityPlan: {
				label: "Aproved Security Plan",
				template: "{{vessel.get('securityDetails').ApprovedSecurityPlan}}",
				visible: true
			},
			validISSC: {
				label: "Valid ISSC",
				template: "{{vessel.get('securityDetails').ValidISSC}}",
				visible: true
			},
			SecurityRelatedMatterToReport: {
				label: "Security  Related Matter To Report",
				template: "{{vessel.get('securityDetails').SecurityRelatedMatterToReport}}",
				visible: true
			}
		},
		CSO: {
			FirstName: {
				label: "First Name",
				template: "{{vessel.get('securityDetails').CSO.FirstName}}",
				visible: true
			},
			LastName: {
				label: "Last Name",
				template: "{{vessel.get('securityDetails').CSO.LastName}}",
				visible: true
			},
			Phone: {
				label: "Phone",
				template: "{{vessel.get('securityDetails').CSO.Phone}}",
				visible: true
			},
			email: {
				label: "E-mail",
				template: "{{vessel.get('securityDetails').CSO.EMail}}",
				visible: true
			},
			Fax: {
				label: "Fax",
				template: "{{vessel.get('securityDetails').CSO.Fax}}",
				visible: true
			}
		}
	},
	ttt: {
		sendPreviousCallFields: {
			port: {
				label: "Port",
				template: "<span ng-if=\"item.Port\" locode=\"item.Port\"></span>",
				visible: true
			},
			arrivalDate: {
				label: "Arrival Date",
				template: "{{item.DateOfArrival}}",
				visible: true
			},
			departureDate: {
				label: "Departure Date",
				template: "{{item.DateOfDeparture}}",
				visible: true
			},
			PortFacility: {
				label: "Port Facility ",
				template: "{{item.PortFacility}}",
				visible: true
			},
			SecurityLevel: {
				label: "Security Level ",
				template: "{{item.SecurityLevel}}",
				visible: true
			},
			SpecialOrAdditionalSecurityMeasures: {
				label: "Special Or Additional Security Measures",
				template: "{{item.SpecialOrAdditionalSecurityMeasures}}",
				visible: true
			}
		}
	}
};

const hazmat = {
	basicinfo: {
		HazmatOnBoardYorN: {
			label: "Hazmat on Board",
			template: "<span>{{vessel.get('enrichment').HazmatOnBoardYorN || \"N/A\"}}</span>",
			visible: true
		}
	},
	details: {
		hazmatDPGItemsFields: {
			TotalQuantityNet: {
				label: "Total Qty Net",
				template: "{{((item.TotalQuantityNet.Quantity) + item.TotalQuantityNet.UnitOfMeasurement) || \"N/A\"}}",
				visible: true
			},
			TotalQuantityGross: {
				label: "Total Qty Gross",
				template: "{{((item.TotalQuantityGross.Quantity) + item.TotalQuantityGross.UnitOfMeasurement) || \"N/A\"}}",
				visible: true
			},
			DGClassification: {
				label: "DG Classification",
				template: "{{item.DGClassification}}",
				visible: true
			},
			TextualReference: {
				label: "Textual Reference",
				template: "{{item.TextualReference || 'N/A'}}",
				visible: true
			}
		}
	},
	ttt: {
		DPGItemsFields: {
			PackingGroup: {
				label: "Packing Group",
				template: "{{item.PackingGroup || \"N/A\"}}",
				visible: true
			},
			TotalQuantityNet: {
				label: "Total Qty Net",
				template: "{{((item.TotalQuantityNet.Quantity) + item.TotalQuantityNet.UnitOfMeasurement)|| \"N/A\"}}",
				visible: true
			},
			TotalQuantityGross: {
				label: "Total Qty Gross",
				template: "{{((item.TotalQuantityGross.Quantity) + item.TotalQuantityNet.UnitOfMeasurement)|| \"N/A\"}}",
				visible: true
			},
			DGClassification: {
				label: "DG Classification",
				template: "{{item.DGClassification}}",
				visible: true
			},
			NonTransportEquipmentUnit: {
				label: "Non-transport Eq. Unit",
				template: "{{item.NonTransportEquipmentUnit.length? \"VIEW DETAIL\" : \"N/A\" }}",
				visible: true
			},
			TransportEquipmentUnit: {
				label: "Transport Eq. Unit",
				template: "{{item.TransportEquipmentUnit.length? \"VIEW DETAIL\" : \"N/A\" }}",
				visible: true
			},
			UNNumber: {
				label: "UN Number",
				template: "{{item.UNNumber || \"N/A\"}}",
				visible: true
			},
			IMOHazardClass: {
				label: "IMO Hazard Class",
				template: "{{item.IMOHazardClass || \"N/A\"}}",
				visible: true
			},
			MarpolCode: {
				label: "Marpol Code",
				template: "{{item.MarpolCode || \"N/A\"}}",
				visible: true
			},
			TextualReference: {
				label: "Textual Reference",
				template: `<span ng-if="item.TextualReference">
						<a target="_blank" title="Find more in the CHD" href="https://portal.emsa.europa.eu/group/chd/search">{{item.TextualReference}}</a>
					</span>
					<span ng-if="!item.TextualReference">N/A</span>
				`,
				visible: true
			},
			PackageType: {
				label: "Package Type",
				template: "{{item.PackageType || \"N/A\"}}",
				visible: true
			},
			EmS: {
				label: "EmS",
				template: "{{(item.EmS && item.EmS.EmSNumber) || \"N/A\"}}",
				visible: true
			},
			FlashPoint: {
				label: "FlashPoint",
				template: "{{item.FlashPoint || \"N/A\"}}",
				visible: true
			},
			TotalNrOfPackages: {
				label: "TotalNrOfPackages",
				template: "{{item.TotalNrOfPackages || \"N/A\"}}",
				visible: true
			},
			SubsidiaryRisks: {
				label: "SubsidiaryRisks",
				template: "{{(item.SubsidiaryRisks && item.SubsidiaryRisks.SubsidiaryRisk) || \"N/A\"}}",
				visible: true
			},
			AdditionalInformation: {
				label: "Additional Information",
				template: "{{item.AdditionalInformation || \"N/A\"}}",
				visible: true
			}
		},
		nonTrsnsportEquipmentUnitFields: {
			NoOfPackages: {
				label: "No Of Packages",
				template: "{{item.NoOfPackages || \"N/A\"}}",
				visible: true
			},
			LocationOnBoard: {
				label: "Location On Board",
				template: "{{item.LocationOnBoard || \"N/A\"}}",
				visible: true
			},
			QuantityNet: {
				label: "Quantity Net",
				template: "{{((item.QuantityNet.Quantity) + item.QuantityNet.UnitOfMeasurement) || \"N/A\"}}",
				visible: true
			},
			QuantityGross: {
				label: "Quantity Gross",
				template: "{{((item.QuantityGross.Quantity) + item.QuantityGross.UnitOfMeasurement) || \"N/A\"}}",
				visible: true
			}
		},
		trsnsportEquipmentUnitFields: {
			TransUnitId: {
				label: "Transport Unit Id",
				template: "{{item.TransUnitId || \"N/A\"}}",
				visible: true
			},
			NoOfPackages: {
				label: "No Of Packages",
				template: "{{item.NoOfPackages || \"N/A\"}}",
				visible: true
			},
			LocationOnBoard: {
				label: "Location On Board",
				template: "{{item.LocationOnBoard || \"N/A\"}}",
				visible: true
			},
			QuantityNet: {
				label: "Quantity Net",
				template: "{{((item.QuantityNet.Quantity) + item.QuantityNet.UnitOfMeasurement) || \"N/A\"}}",
				visible: true
			},
			QuantityGross: {
				label: "Quantity Gross",
				template: "{{((item.QuantityGross.Quantity) + item.QuantityGross.UnitOfMeasurement) || \"N/A\"}}",
				visible: true
			}
		},
		consignmentFields: {
			TransportDocumentID: {
				label: "Transport Document ID",
				template: "<span>{{consignment.TransportDocumentID || \"N/A\"}}</span>",
				visible: true
			},
			PortOfLoading: {
				label: "Port of Loading",
				template: "<span ng-if='consignment.PortOfLoading' locode='consignment.PortOfLoading'></span>",
				visible: true
			},
			PortOfDischarge: {
				label: "Port of Discharge",
				template: "<span ng-if='consignment.PortOfDischarge' locode='consignment.PortOfDischarge'></span>",
				visible: true
			}
		}
	},
	cargoManifest: {
		firstName: {
			label: "FirstName",
			template: "{{cargoManifest.ContactDetails.FirstName || 'N/A'}}",
			visible: true
		},
		lastName: {
			label: "LastName",
			template: "{{cargoManifest.ContactDetails.LastName || 'N/A'}}",
			visible: true
		},
		phone: {
			label: "Phone",
			template: "{{cargoManifest.ContactDetails.Phone || 'N/A'}}",
			visible: true
		},
		email: {
			label: "E-mail",
			template: "{{cargoManifest.ContactDetails.EMail || 'N/A'}}",
			visible: true
		},
		Fax: {
			label: "Fax",
			template: "{{cargoManifest.ContactDetails.Fax || 'N/A'}}",
			visible: true
		},
		LoCode: {
			label: "LoCode",
			template: "<span ng-if='cargoManifest.ContactDetails.LoCode' locode='cargoManifest.ContactDetails.LoCode'></span>",
			visible: true
		},
		urlDetails: {
			label: "URL",
			template: `<a ng-if="cargoManifest.UrlDetails.Url" target="_blank" href="{{cargoManifest.UrlDetails.Url}}">{{cargoManifest.UrlDetails.Url}}</a>
				<span ng-if="!cargoManifest.UrlDetails.Url">N/A</span>
			`,
			visible: true
		}
	}

};

const mrs = {
	basicinfo: {
		mrsProperties: {
			Nextportofcall: {
				label: "Next port of call",
				template: "<span ng-if=\"(vessel.get('mrs').NextPortOfCall)\" locode=\"(vessel.get('mrs').NextPortOfCall)\"></span>",
				visible: true
			},
			ETAtoPort: {
				label: "ETA to Port",
				template: "<span>{{(vessel.get('mrs').ETA)?(vessel.get('mrs').ETA):\"N/A\"}}</span>",
				visible: true
			},
			TotalPersonsOnBoard: {
				label: "Total Persons on Board",
				template: "<span>{{(vessel.get('mrs').TotalPersonsOnBoard)?vessel.get('mrs').TotalPersonsOnBoard:\"N/A\"}}</span>",
				visible: true
			},
			AnyDG: {
				label: "Any DG",
				template: "<span>{{(vessel.get('mrs').AnyDG)?vessel.get('mrs').AnyDG:\"N/A\"}}</span>",
				visible: true
			},
			Latitude: {
				label: "Latitude",
				template: "<span [coordinate]=\"[vessel.get('mrs').Latitude,'lat']\"></span>",
				visible: true
			},
			Longitude: {
				label: "Longitude",
				template: "<span [coordinate]=\"[vessel.get('mrs').Longitude,'lon']\"></span>",
				visible: true
			},
			ReportedAt: {
				label: "Reported At",
				template: "<span >{{(vessel.get('mrs').ReportingDateAndTime)?vessel.get('mrs').ReportingDateAndTime:\"N/A\"}}</span>",
				visible: true
			}
		},
		mrsCargo: {
			cargoType: {
				label: "Cargo Type",
				template: "<span>{{vessel.get('mrs').MRSCargoInformation.CargoType || \"N/A\"}}</span>",
				visible: true
			},
			cargoOther: {
				label: "Cargo Additional Information",
				template: "<span>{{vessel.get('mrs').MRSCargoInformation.DG.AOI || \"N/A\"}}</span>",
				visible: true
			}
		},
		mrsContact: {
			lastName: {
				label: "Last Name",
				template: "<span>{{vessel.get('mrs').MRSCargoInformation.ContactDetails.LastName || \"N/A\"}}</span>",
				visible: true
			},
			firstName: {
				label: "Last Name",
				template: "<span>{{vessel.get('mrs').MRSCargoInformation.ContactDetails.FirstName || \"N/A\"}}</span>",
				visible: true
			},
			location: {
				label: "Location",
				template: "<span ng-if=\"vessel.get('mrs').MRSCargoInformation.ContactDetails.LoCode\" locode=\"vessel.get('mrs').MRSCargoInformation.ContactDetails.LoCode\"></span>",
				visible: true
			},
			phone: {
				label: "Phone",
				template: "<span>{{vessel.get('mrs').MRSCargoInformation.ContactDetails.Phone || \"N/A\"}}</span>",
				visible: true
			},
			fax: {
				label: "Fax",
				template: "<span>{{vessel.get('mrs').MRSCargoInformation.ContactDetails.Fax || \"N/A\"}}</span>",
				visible: true
			},
			email: {
				label: "Email",
				template: "<span>{{vessel.get('mrs').MRSCargoInformation.ContactDetails.Email || \"N/A\"}}</span>",
				visible: true
			}
		}
	},
	details: {
		mrsDetails: {
			COG: {
				label: "COG",
				template: "<span>{{(vessel.get('mrsDetails').MRSDynamicInformation.COG/10) || \"N/A\"}}</span>",
				visible: true
			},
			SOG: {
				label: "SOG",
				template: "<span>{{(vessel.get('mrsDetails').MRSDynamicInformation.SOG/10) || \"N/A\"}}</span>",
				visible: true
			},
			navigationalStatus: {
				label: "Nav Status",
				template: "<span>{{vessel.get('mrsDetails').MRSDynamicInformation.NavigationalStatus}}</span>",
				visible: true
			},
			bunkerChars: {
				label: "BunkerS",
				template: "<span>{{vessel.get('mrsDetails').MRSDynamicInformation.Bunker.Chars || \"N/A\"}}</span>",
				visible: true
			},
			bunkerQty: {
				label: "Bunker Quantity",
				template: "<span>{{(vessel.get('mrsDetails').MRSDynamicInformation.Bunker.Quantity) || \"N/A\"}}</span>",
				visible: true
			},
			cstid: {
				label: "CST Id",
				template: "<span>{{vessel.get('mrsDetails').MRSInformation.CSTIdentification || \"N/A\"}}</span>",
				visible: true

			},
			MRSid: {
				label: "MRS Id",
				template: "<span>{{vessel.get('mrsDetails').MRSInformation.MRSIdentification || \"N/A\"}}</span>",
				visible: true
			}
		}
	},
	ttt: {
		mrsCargoFields: {
			imoclass: {
				label: "IMO Class",
				template: "{{item.IMOClass || \"N/A\"}}",
				visible: true
			},
			qty: {
				label: "Quantity",
				template: "{{(item.Quantity) || \"N/A\"}}",
				visible: true
			}
		}

	}
};

const vi = {
	basicinfo: {
		viProperties: {
			ShipCallId: {
				label: "ShipCall ID",
				template: "<span>{{(vessel.get('vi').ShipCallId)?vessel.get('vi').ShipCallId:\"N/A\"}}</span>",
				visible: true
			},
			BriefCargoDescription: {
				label: "Brief Cargo Description",
				template: "<span>{{(vessel.get('vi').BriefCargoDescription)?vessel.get('vi').BriefCargoDescription:\"N/A\"}}</span>",
				visible: true
			},
			PortOfCall: {
				label: "Port of call",
				template: "<span ng-if=\"(vessel.get('vi').PortOfCall)\" locode=\"(vessel.get('vi').PortOfCall)\"></span><span ng-if=\"!(vessel.get('vi').PortOfCall)\">N/A</span>",
				visible: true
			},
			ATAPortOfCall: {
				label: "ATA Port Of Call",
				template: "<span ng-if=\"vessel.get('vi').ATAPortOfCall\" position-timestamp=\"vessel.get('vi').ATAPortOfCall\"></span><span ng-if=\"!vessel.get('vi').ATAPortOfCall\">N/A</span>",
				visible: true
			},
			ETAToPortOfCall: {
				label: "ETA Port of Call",
				template: "<span ng-if=\"vessel.get('vi').ETAToPortOfCall\" position-timestamp=\"vessel.get('vi').ETAToPortOfCall\"></span><span ng-if=\"!vessel.get('vi').ETAToPortOfCall\">N/A</span>",
				visible: true
			},
			PurposeOfCall: {
				label: "Purpose of Call",
				template: "<span>{{(vessel.get('vi').PurposeOfCall)?vessel.get('vi').PurposeOfCall.CallPurposeCode:\"N/A\"}}</span>",
				visible: true
			},
			ETDFromLastPort: {
				label: "ETD From Last Port",
				template: "<span ng-if=\"vessel.get('vi').ETDFromLastPort\" position-timestamp=\"vessel.get('vi').ETDFromLastPort\"></span><span ng-if=\"!vessel.get('vi').ETDFromLastPort\">N/A</span>",
				visible: true
			},
			LastPort: {
				label: "Last Port",
				template: "<span ng-if=\"vessel.get('vi').LastPort\" locode=\"(vessel.get('vi').LastPort)\"></span><span ng-if=\"!vessel.get('vi').LastPort\">N/A</span>",
				visible: true
			},
			NextPort: {
				label: "Next Port",
				template: "<span ng-if=\"vessel.get('vi').NextPort\" locode=\"(vessel.get('vi').NextPort)\"></span><span ng-if=\"!vessel.get('vi').NextPort\">N/A</span>",
				visible: true
			},
			ETAToNextPort: {
				label: "ETA To Next Port",
				template: "<span ng-if=\"vessel.get('vi').ETAToNextPort\" position-timestamp=\"vessel.get('vi').ETAToNextPort\"></span><span ng-if=\"!vessel.get('vi').ETAToNextPort\">N/A</span>",
				visible: true
			},
			TotalPersonsOnBoard: {
				label: "Total Persons on Board",
				template: "<span>{{(vessel.get('vi').POBVoyageTowardsPortOfCall)?vessel.get('vi').POBVoyageTowardsPortOfCall:\"N/A\"}}</span>",
				visible: true
			},
			PositionInPortOfCall: {
				label: "Position in Port Of Call",
				template: "<span>{{(vessel.get('vi').PositionInPortOfCall)?vessel.get('vi').PositionInPortOfCall:\"N/A\"}}</span>",
				visible: true
			}
		},
		certificateOfRegistry: {
			IssueDate: {
				label: "Issue Date",
				template: "<span>{{(vessel.get('vi').CertificateOfRegistry)?vessel.get('vi').CertificateOfRegistry.IssueDate:\"N/A\"}}</span>",
				visible: true
			},
			CertificateNumber: {
				label: "Certificate Number",
				template: "<span>{{(vessel.get('vi').CertificateOfRegistry)?vessel.get('vi').CertificateOfRegistry.CertificateNumber:\"N/A\"}}</span>",
				visible: true
			},
			LoCode: {
				label: "Locode",
				template: "<span>{{(vessel.get('vi').CertificateOfRegistry&&vessel.get('vi').CertificateOfRegistry.PortOfRegistry)&&vessel.get('vi').CertificateOfRegistry.PortOfRegistry.LoCode?vessel.get('vi').CertificateOfRegistry.PortOfRegistry.LoCode:\"N/A\"}}</span>",
				visible: true
			},
			LocationName: {
				label: "Location Name",
				template: "<span>{{(vessel.get('vi').CertificateOfRegistry&&vessel.get('vi').CertificateOfRegistry.PortOfRegistry)&&vessel.get('vi').CertificateOfRegistry.PortOfRegistry.LocationName?vessel.get('vi').CertificateOfRegistry.PortOfRegistry.LocationName:\"N/A\"}}</span>",
				visible: true
			}
		},
		sourceProperties: {
			LastUpdateReceivedAt: {
				label: "Last Update Received At",
				template: "<span>{{(vessel.get('vi').LastUpdateReceivedAt)?vessel.get('vi').LastUpdateReceivedAt:\"N/A\"}}</span>",
				visible: true
			},
			ProviderOfLastUpdate: {
				label: "Provider of Last Update",
				template: "<span>{{(vessel.get('vi').ProviderOfLastUpdate)?vessel.get('vi').ProviderOfLastUpdate:\"N/A\"}}</span>",
				visible: true
			}
		},
		vesselDetails: {
			ShipType: {
				label: "Ship Type",
				template: "<span>{{vesselCommandInfo.getShipType(vessel.get('vi').ShipType)||'N/A'}}</span>",
				visible: true
			},
			GrossTonnage: {
				label: "Gross Tonnage",
				template: "<span>{{(vessel.get('vi').GrossTonnage)?vessel.get('vi').GrossTonnage:\"N/A\"}}</span>",
				visible: true
			},
			InmarsatCallNumber: {
				label: "Inmarsat Call Number",
				template: "<span>{{(vessel.get('vi').InmarsatCallNumber&&vessel.get('vi').InmarsatCallNumber.Inmarsat)?vessel.get('vi').InmarsatCallNumber.Inmarsat:\"N/A\"}}</span>",
				visible: true
			},
			CompanyName: {
				label: "Company Name",
				template: "<span>{{(vessel.get('vi').Company&&vessel.get('vi').Company.CompanyName)?vessel.get('vi').Company.CompanyName:\"N/A\"}}</span>",
				visible: true
			},
			IMOCompanyNr: {
				label: "IMO Company Nr",
				template: "<span>{{(vessel.get('vi').Company&&vessel.get('vi').Company.IMOCompanyNr)?vessel.get('vi').Company.IMOCompanyNr:\"N/A\"}}</span>",
				visible: true
			}
		},
		PreArrival3DaysNotificationDetails: {
			PossibleAnchorage: {
				label: "Possible Anchorage",
				template: "<span>{{(vessel.get('vi').PossibleAnchorage)?vessel.get('vi').PossibleAnchorage:\"N/A\"}}</span>",
				visible: true
			},
			ShipConfiguration: {
				label: "Ship Configuration",
				template: "<span>{{(vessel.get('vi').ShipConfiguration)?vessel.get('vi').ShipConfiguration:\"N/A\"}}</span>",
				visible: true
			},
			PlannedOperations: {
				label: "Planned Operations",
				template: "<span>{{(vessel.get('vi').ShipConfiguration)?vessel.get('vi').PlannedOperations:\"N/A\"}}</span>",
				visible: true
			},
			PlannedWorks: {
				label: "Planned Works",
				template: "<span>{{(vessel.get('vi').ShipConfiguration)?vessel.get('vi').PlannedWorks:\"N/A\"}}</span>",
				visible: true
			},
			CargoVolumeNature: {
				label: "Cargo Volume Nature",
				template: "<span>{{(vessel.get('vi').ShipConfiguration)?vessel.get('vi').CargoVolumeNature:\"N/A\"}}</span>",
				visible: true
			},
			ConditionCargoBallastTanks: {
				label: "Condition Cargo Ballast Tanks",
				template: "<span>{{(vessel.get('vi').ShipConfiguration)?vessel.get('vi').ConditionCargoBallastTanks:\"N/A\"}}</span>",
				visible: true
			}
		}

	}
};

module.exports = {
	security, hazmat, waste, mrs, vi
};