const { getTooltipDisplayProperties, getProperty } = require("./properties.js");

module.exports = {
	init: () => {
		seg.ui.preferences.addSectionToTab("Labels", {
			alias: "labels",
			templateUrl: resolvePath("./templates/label_preferences.html"),
			bindings: {
				showingVesselLabels() {
					return seg.preferences.workspace.get("Vessels.label.visible");
				},
				toggleVesselLabels() {
					seg.preferences.workspace.set("Vessels.label.visible", !this.showingVesselLabels());
					// By default Name is always toggled
					if (!this.vesselLabelProperties.filter(prop => this.isPropertySelected(prop)).length)
						this.toggleLabelProperty("shipName");
					seg.layer.get("positions").refresh();
				},
				vesselLabelProperties: [
					"shipName",
					"imo",
					"mmsi",
					"flagState"
				],
				get activationScale() {
					return seg.preferences.workspace.get("Vessels.label.scale");
				},
				set activationScale(value){
					this.setActivationScale(value);
				},
				isPropertySelected(property) {
					return seg.preferences.workspace.get("Vessels.label.properties."+property);
				},
				toggleLabelProperty(property) {
					seg.preferences.workspace.set("Vessels.label.properties."+property, !this.isPropertySelected(property));
					// By default Name is always toggled
					if (!this.vesselLabelProperties.filter(prop => this.isPropertySelected(prop)).length)
						this.toggleLabelProperty("shipName");
					seg.layer.get("positions").refresh();
				},
				setActivationScale(value) {
					seg.preferences.workspace.set("Vessels.label.scale", value);
					seg.layer.get("positions").refresh();
				},
				getCurrentScale() {
					return seg.map.getScale();
				},
				getTooltipDisplayProperties,
				getProperty,
				toggleTooltipProperty(key) {
					seg.preferences.workspace.set("Vessels.tooltip."+key, !seg.preferences.workspace.get("Vessels.tooltip."+key));
				}
			}
		});
	}
};
