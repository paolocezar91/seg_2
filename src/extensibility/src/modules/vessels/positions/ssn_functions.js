const {getLabel} = require("../api"),
	ssnInformation = require("./ssn_properties"),
	ssnVisibilityState = {},
	ssnDetails = {},
	wasteTypeValues = (code) => {
		const codeText = {
			1200: "Oily Bilge water",
			1100: "Oily Residues (Sludge)",
			1301: "Used engine oil",
			1300: "Other",
			3000: "SEWAGE",
			2200: "Plastic",
			2100: "Food waste",
			2301: "International catering waste",
			2102: "Other food waste",
			2302: "Paper products",
			2303: "Rags",
			2304: "Glass",
			2305: "Metal",
			2306: "Bottles",
			2307: "Crockery",
			2310: "Special Items",
			2300: "Other",
			2311: "Cooking oil",
			2308: "Incinerator ashes",
			2600: "Operational wastes",
			2309: "Animal carcasses",
			5102: "Oily (dirty) ballast water",
			5103: "Scale and sludge from tank cleaning",
			5100: "Other",
			5201: "Washing waters containing noxious cargo residues",
			5202: "Ballast water containing noxious cargo residues",
			5200: "Other",
			5301: "Cargo hold washing water containing residues and or cleaning agents or additives harmful to the marine environment",
			5302: "Cargo hold washing water containing residues and or cleaning agents or additives NOT harmful to the marine environment",
			5303: "Dry cargo residues harmful to the marine environment",
			5304: "Dry cargo residues NOT harmful to the marine environment",
			5300: "Other"
		};
		return (codeText[code] || "N/A") + " ("+ code +")";
	},
	ssn_properties = {
		wasteProperties: ssnInformation.waste.basicinfo.wasteProperties,
		wasteLastPortProperties: ssnInformation.waste.basicinfo.wasteLastPortProperties,
		securityProperties: ssnInformation.security.basicinfo.securityProperties,
		securityAgentProperties: ssnInformation.security.basicinfo.securityAgentProperties,
		securityDetails: ssnInformation.security.details.securityDetails,
		ISSCProperties: ssnInformation.security.details.ISSCProperties,
		previousCallAtPortFacilityFields: ssnInformation.security.details.previousCallAtPortFacilityFields,
		shipToShipActivityFields: ssnInformation.security.details.shipToShipActivityFields,
		viProperties: ssnInformation.vi.basicinfo.viProperties,
		sourceProperties: ssnInformation.vi.basicinfo.sourceProperties,
		certificateOfRegistry: ssnInformation.vi.basicinfo.certificateOfRegistry,
		vesselDetails: ssnInformation.vi.basicinfo.vesselDetails,
		PreArrival3DaysNotificationDetails: ssnInformation.vi.basicinfo.PreArrival3DaysNotificationDetails
	};

module.exports = Object.assign({}, ssn_properties, {
	generalTab: true,
	specificTab: false,
	SSNTab: false,
	SSNTabVoyageInformation: false,
	SSNTabHazmat: false,
	SSNTabWaste: false,
	SSNTabSecurity: false,
	getSSNUIState(vid, flag) {
		flag = flag.toLowerCase();

		if(!ssnVisibilityState[vid] || !ssnVisibilityState[vid][flag])
			return "More Information";

		return ssnVisibilityState[vid][flag];
	},
	setSSNUIState(vid, flag, val) {
		flag = flag.toLowerCase();

		if(!ssnVisibilityState[vid])
			ssnVisibilityState[vid] = {};

		ssnVisibilityState[vid][flag] = val;
	},
	getSSNDetails(vid, flag) {
		flag = flag.toLowerCase();
		return ssnDetails[vid] && ssnDetails[vid][flag];
	},
	setSSNDetails(vid, flag, val) {
		flag = flag.toLowerCase();

		if(!ssnDetails[vid])
			ssnDetails[vid] = {};

		ssnDetails[vid][flag] = val;
	},
	getVoyages: vessel => {
		const modal = seg.modal.openModal("Get Voyages " + getLabel(vessel), "Retrieving Voyages for " + getLabel(vessel) + "..."),
			params = {
				details: "GetExpectedandLastTenCalls",
				begin: moment().subtract(60, "days").toISOString()
			};

		if (vessel.get("mmsi")) params.mmsiNumber = vessel.get("mmsi");
		else if (vessel.get("imo")) params.imoNumber = vessel.get("imo");
		else if (vessel.get("csid")) params.csdId = vessel.get("csid");
		else return;

		seg.request.get("v1/ship/ship_call/", {
			params
		}).then((res) => {
			const {status, message} = res;

			if (!(status === "success" || status === "OK")) {
				seg.log.error("ERROR_VESSEL_SSN_GET_VOYAGES_1", status + ": " + message);
				modal.setContent("<span class=\"retrieved-no-result\"><b>" + status + ": See console for details</b></span>");
			} else {
				let {result} = res;

				if (result.hasOwnProperty("Body"))
					result = result.Body;

				if (!result)
					return;

				let voyages = result.QueryResults.PortPlusNotificationList;

				if (!voyages){
					modal.setContent("There are no Voyages registered for " + getLabel(vessel));
					seg.modal.closeTimeout(modal.title);
					return;
				}

				modal.setContent("Voyages Retrieved");
				seg.modal.closeTimeout(modal.title);

				if (!Array.isArray(voyages))
					voyages = [voyages];

				voyages = voyages.map(voyage => {
					if (!voyage.ArrivalNotificationDetails && !voyage.DepartureNotificationDetails){
						const start = voyage.VoyageInformation.ETDFromPortOfCall || voyage.VoyageInformation.ATDPortOfCall,
							end = voyage.VoyageInformation.ETAToPortOfCall || voyage.VoyageInformation.ATAPortOfCall;

						if (start && end)
							voyage.isCurrent = ((moment(start) >= moment()) && (moment(end) <= moment()));
					}

					return voyage;
				}).sort((a,b) => {
					const i = a.isCurrent? 1: 0,
						j = b.isCurrent? 1: 0;
					return (i > j);
				});

				vessel.set("voyagesData", voyages);

				seg.ui.ttt.tables.open({
					label: "Vessel Voyages #" + getLabel(vessel),
					customToolbar: {
						template: "<span class=\"text-size-sm\">Number of voyages: <b>{{grid.filteredResults.length}}</b></span>"
					},
					data: voyages,
					itemAs: "voyage",
					fields: require("./voyage.fields.js"),
					sortKey: "ETAToPortOfCall",
					sortKeyOrder: "desc"
				});
			}
			seg.modal.closeTimeout(modal.title);

		}, e => {
			seg.log.error("ERROR_VESSEL_SSN_GET_VOYAGES_1", e.error + ": " + e.result);
			modal.setContent("There are no SSN Voyages registered for this vessel");
			seg.modal.closeTimeout(modal.title);
		});
	},
	openSSNTab(type) {
		this.basicInfoExpanded = this.SSNTab = true;
		this.generalTab = this.specificTab = false;
		this.SSNTabVoyageInformation = this.SSNTabHazmat = this.SSNTabMrs = this.SSNTabWaste = this.SSNTabSecurity = false;
		switch(type){
			case "voyage information":
				this.SSNTabVoyageInformation = true;
				break;
			case "hazmat":
				this.SSNTabHazmat = true;
				break;
			case "waste":
				this.SSNTabWaste = true;
				break;
			case "security":
				this.SSNTabSecurity = true;
				break;
			case "mrs":
				this.SSNTabMrs = true;
				break;
		}
	},
	showMoreInformation(item, vessel) {
		const vid = vessel.getId();
		if(!this.getSSNDetails(vid, item))
			this.setSSNUIState(vid, item, "Loading");
		else {
			this.setSSNUIState(vid, item, "More Information");
			this.setSSNDetails(vid, item, false);
			return;
		}

		return this.getEnrichmentDetails(vessel, item.toLowerCase());
	},
	getEnrichmentDetails(vessel, type) {
		const modal = seg.modal.openModal("SSN Information " + getLabel(vessel) + " " + type, "Retrieving " + type.toUpperCase() +" information for " + getLabel(vessel) + "..."),
			vid = vessel.getId();

		if (this.getSSNDetails(vid, type)) {
			this.setSSNUIState(vid, type, "More Information");
			this.setSSNDetails(vid, type, false);
			return;
		}

		let params;

		if (type !== "hazmat") {
			params = {
				details: "SelectedShipCall",
				ship_call_id: vessel.get("shipCallId"),
				hazmat: (type === "hazmat") ? true : false,
				waste: (type === "waste") ? true : false,
				security: (type === "security") ? true : false
			};
		} else {
			params = {
				details: "GetActiveHazmatForSelectedShip",
				imoNumber: vessel.get("imo"),
				hazmat: (type === "hazmat") ? true : false,
				waste: (type === "waste") ? true : false,
				security: (type === "security") ? true : false
			};
		}

		const enrichmentRequest = seg.request.get("v1/ship/ship_call/", {params});
		modal.setDismissCallback(() => enrichmentRequest.cancel());

		enrichmentRequest.then((res) => {
			const {status, message} = res;

			if (!(status === "success" || status === "OK")) {
				seg.log.error("ERROR_VESSEL_SSN_GET_" + type.toUpperCase() + "_1", status + ": " + message);
				modal.setContent("<span class=\"retrieved-no-result\"><b>" + status + ": See console for details</b></span>");
				vessel.set(type + "Error", status + ": " + message);
				this.setSSNUIState(vid, type, "More Information");
				seg.modal.closeTimeout(modal.title);
			} else {
				let {result} = res;
				vessel.unset(type + "Error");

				if (!result) {
					modal.setContent("No " + type.toUpperCase() + " details available for " + getLabel(vessel));
					seg.modal.closeTimeout(modal.title);

					this.setSSNUIState(vid, type, "More Information");
					this.setSSNDetails(vid, type, false);
					return;
				}

				if (result.hasOwnProperty("Body"))
					result = result.Body;

				const portPlusNotificationDetails = result.QueryResults.PortPlusNotificationDetails;

				let portPlusNotificationList;
				if(result.QueryResults.PortPlusNotificationList && Array.isArray(result.QueryResults.PortPlusNotificationList))
					portPlusNotificationList = result.QueryResults.PortPlusNotificationList[0];
				else
					portPlusNotificationList =result.QueryResults.PortPlusNotificationList;

				if(!portPlusNotificationDetails || !portPlusNotificationList) {
					modal.setContent("No " + type.toUpperCase() + " details available for " + getLabel(vessel));
					vessel.set(type + "Error", status + ": " + message);
					this.setSSNUIState(vid, type, "More Information");
				} else {
					vessel.unset(type + "Error");

					if (!result) {
						modal.setContent("No " + type.toUpperCase() + " details available for " + getLabel(vessel));
						seg.modal.closeTimeout(modal.title);

						this.setSSNUIState(vid, type, "More Information");
						this.setSSNDetails(vid, type, false);
						return;
					}

					let details, confirmation, cargoManifest;

					switch(type) {
						case "waste":
							if (portPlusNotificationDetails && portPlusNotificationDetails.WasteInformation)
								details = portPlusNotificationDetails.WasteInformation.WasteDetails;

							if (details && details.WasteItem && !Array.isArray(details.WasteItem))
								details.WasteItem = [details.WasteItem];

							if(portPlusNotificationList)
								confirmation = portPlusNotificationList.WasteConfirmation;
							break;
						case "hazmat":
							if (portPlusNotificationDetails && portPlusNotificationDetails.HazmatInformation){
								details = portPlusNotificationDetails.HazmatInformation.HazmatDetails;
								cargoManifest = portPlusNotificationDetails.HazmatInformation.CargoManifest;
							}

							if (details && details.CargoInformation && details.CargoInformation.Consignment){
								if (!Array.isArray(details.CargoInformation.Consignment))
									details.CargoInformation.Consignment = [details.CargoInformation.Consignment];

								details.CargoInformation.Consignment.map(consignment => {
									if (!Array.isArray(consignment.DPGItem))
										consignment.DPGItem = [consignment.DPGItem];
									return consignment;
								});
							}

							if(portPlusNotificationList)
								confirmation = portPlusNotificationList.HazmatConfirmation;
							break;
						case "security":
							if(portPlusNotificationDetails && portPlusNotificationDetails.SecurityInformation)
								details = portPlusNotificationDetails.SecurityInformation.SecurityDetails;

							if (details && details.ShipToShipActivity && !Array.isArray(details.ShipToShipActivity))
								details.ShipToShipActivity = [details.ShipToShipActivity];

							if (details && details.PreviousCallAtPortFacility && !Array.isArray(details.PreviousCallAtPortFacility))
								details.PreviousCallAtPortFacility = [details.PreviousCallAtPortFacility];

							if(portPlusNotificationList)
								confirmation = portPlusNotificationList.SecurityConfirmation;
							break;
					}

					if(cargoManifest) {
						vessel.set("cargoManifest", [cargoManifest]);
					}

					if (!details) {
						modal.setContent("No " + type.toUpperCase() + " details available for " + getLabel(vessel));
						this.setSSNUIState(vid, type, "More Information");
						this.setSSNDetails(vid, type, false);
					} else {
						modal.setContent("<span class=\"retrieved-success\">" + type.toUpperCase() + " details available for " + getLabel(vessel) + "</span>");
						vessel.set(type + "Details", details);
						this.setSSNUIState(vid, type, "Less Information");
					}

					if(confirmation)
						vessel.set(type, confirmation);

					this.setSSNDetails(vid, type, !!details);
				}
				seg.modal.closeTimeout(modal.title);
			}
		}, e => {
			modal.setContent("No " + type.toUpperCase() + " details available for " + getLabel(vessel));
			seg.modal.closeTimeout(modal.title);
			seg.log.error("ERROR_VESSEL_SSN_GET_ENRICHMENT_DETAILS_" + type.toUpperCase() + "_2", e.error + ": " + e.result);
			this.setSSNUIState(vid, type, "More Information");
			this.setSSNDetails(vid, type, false);
			return;
		});
	},
	mrsCargo: ssnInformation.mrs.basicinfo.mrsCargo,
	sendCargoDGtoTTT: vessel => seg.ui.ttt.tables.open({
		label: "Cargo Information #" + getLabel(vessel),
		data: vessel.get("mrsDetails").MRSCargoInformation.DG.DGDetails || [],
		itemAs: "item",
		fields: ssnInformation.mrs.ttt.mrsCargoFields
	}),
	mrsContact: ssnInformation.mrs.basicinfo.mrsContact,
	mrsDetails: ssnInformation.mrs.details.mrsDetails,
	mrsProperties: ssnInformation.mrs.basicinfo.mrsProperties,
	mrsCargoFields: ssnInformation.mrs.ttt.mrsCargoFields,
	getMRS(vessel) {
		const type = "mrs",
			modal = seg.modal.openModal("SSN Information " + getLabel(vessel) + " " + type, "Retrieving " + type.toUpperCase() +" information for " + getLabel(vessel) + "..."),
			vid = vessel.getId();

		if (this.getSSNDetails(vid, type)) {
			this.setSSNUIState(vid, type, "More Information");
			this.setSSNDetails(vid, type, false);
			return;
		}

		this.setSSNUIState(vid, type, "Loading");

		const params = {};

		if (vessel.get("imo"))
			params.imoNumber = vessel.get("imo");
		else if (vessel.get("csdid"))
			params.csdIdNumber = vessel.get("csdid");
		else
			return; // check this return

		const enrichmentRequest = seg.request.get("v1/ship/ship_call/mrs", {params});
		modal.setDismissCallback(() => enrichmentRequest.cancel());

		enrichmentRequest.then((res) => {
			const {status, message} = res;

			if (!(status === "success" || status === "OK")) {
				seg.log.error("ERROR_VESSEL_SSN_GET_MRS_1", status + ": " + message);
				modal.setContent("<span class=\"retrieved-no-result\"><b>" + status + ": See console for details</b></span>");
				vessel.set(type + "Error", status + ": " + message);
				this.setSSNUIState(vid, type, "More Information");
			} else {
				let {result} = res;
				vessel.unset(type + "Error");

				if (!result) {
					modal.setContent("No " + type.toUpperCase() + " details available for " + getLabel(vessel));
					seg.modal.closeTimeout(modal.title);

					this.setSSNUIState(vid, type, "More Information");
					this.setSSNDetails(vid, type, false);
					return;
				}

				if (result.hasOwnProperty("Body"))
					result = result.Body;

				//convert latitude and longitude to meters
				const details = result.NotificationDetails.MRSNotificationDetails;
				details.MRSVoyageInformation.Longitude = (details.MRSVoyageInformation.Longitude/10000)/60;
				details.MRSVoyageInformation.Latitude = (details.MRSVoyageInformation.Latitude/10000)/60;

				if (details.MRSCargoInformation && details.MRSCargoInformation.DG && !Array.isArray(details.MRSCargoInformation.DG.DGDetails))
					details.MRSCargoInformation.DG.DGDetails = [details.MRSCargoInformation.DG.DGDetails];

				vessel.set(type + "Details", details);

				modal.setContent("<span class=\"retrieved-success\">" + type.toUpperCase() + "details available for " + getLabel(vessel) + "</span>");
				this.setSSNUIState(vid, type, "Less Information");
				this.setSSNDetails(vid, type, true);
			}
			seg.modal.closeTimeout(modal.title);

		}, e => {
			seg.log.error("ERROR_VESSEL_SSN_GET_MRS_2", e.error + ": " + e.result);
			modal.setContent("No " + type.toUpperCase() + " details available for " + getLabel(vessel));
			seg.modal.closeTimeout(modal.title);

			this.setSSNUIState(vid, type, "More Information");
			this.setSSNDetails(vid, type, false);
			return;
		});
	},
	mrsDGItems: vessel => vessel.get("mrs") ? vessel.get("mrs").MRSCargoInformation.DG.DGDetails : null,
	hazmatProperties: ssnInformation.hazmat.basicinfo,
	consignmentFields: ssnInformation.hazmat.ttt.consignmentFields,
	hazmatDPGItemsFields: ssnInformation.hazmat.details.hazmatDPGItemsFields,
	hazmatCargoManifest: ssnInformation.hazmat.cargoManifest,
	sendDPGItemsToTTT: (data, vessel, $index) => {
		seg.ui.ttt.tables.open({
			label: "DPGItems " + getLabel(vessel) + " #" + ($index),
			data: data || [],
			itemAs: "item",
			fields: ssnInformation.hazmat.ttt.DPGItemsFields,
			onSelectionChange: (changedRows, selectedRows) => {
				if (!selectedRows || !selectedRows[0])
					return;

				if (selectedRows[0].item.TransportEquipmentUnit)
					seg.ui.ttt.tables.open({
						label: "Transport Equipment Unit " + " #" + ($index) + " " + data.indexOf(selectedRows[0].item),
						data: selectedRows[0].item.TransportEquipmentUnit || [],
						itemAs: "item",
						fields: ssnInformation.hazmat.ttt.trsnsportEquipmentUnitFields
					});

				if(selectedRows[0].item.NonTransportEquipmentUnit)
					seg.ui.ttt.tables.open({
						label: "Non-Transport Equipment Unit " + " #" + ($index) + " " + data.indexOf(selectedRows[0].item),
						data: selectedRows[0].item.NonTransportEquipmentUnit || [],
						itemAs: "item",
						fields: ssnInformation.hazmat.ttt.nonTrsnsportEquipmentUnitFields
					});
			}
		});
	},
	sendPreviousCall: (data, vessel) => seg.ui.ttt.tables.open({
		label: "Previous Call #" + getLabel(vessel),
		data: data || [],
		itemAs: "item",
		fields: ssnInformation.security.ttt.sendPreviousCallFields
	}),
	sendShiptoShip: (data, vessel) => seg.ui.ttt.tables.open({
		label: "Ship to Ship #" + vessel.getId(),
		data: data || [],
		itemAs: "item",
		fields: ssnInformation.security.details.shipToShipActivityFields
	}),
	CSO: ssnInformation.security.details.CSO,
	wasteDetailsHeader: ssnInformation.waste.details.wasteDetailsHeader,
	wasteDetailListFields: ssnInformation.waste.details.wasteDetailListFields,
	sendWasteDetailToTTT: vessel => seg.ui.ttt.tables.open({
		label: "Waste Vessel #" + getLabel(vessel),
		data: vessel.get("wasteDetails").WasteItem || [],
		itemAs: "item",
		gridAPI: {
			wasteTypeValues
		},
		fields: ssnInformation.waste.ttt.detailFields
	}),
	wasteTypeValues,
	getExemptions(vessel) {
		let title;

		if (!vessel.get("exemptions") || !vessel.get("exemptions").length)
			return;

		seg.ui.ttt.tables.open({
			label: "Exemptions Vessel #" + vessel.getId(),
			data: vessel.get("exemptions"),
			itemAs: "item",
			fields: {
				ExemptionID: {
					label: "Exemption ID",
					template: "{{item.ExemptionID}}",
					visible: true
				},
				incidentType: {
					label: "Exemption Type",
					template: "{{item.ExemptionDetails.ExemptionType}}",
					visible: true
				}
			},
			onSelectionChange: (changedRows, selectedRows) => {
				if (selectedRows.length === 1) {
					if (title)
						seg.popup.close(title);

					const row = selectedRows[0];
					title = "Exemption " + row.item.ExemptionDetails.ExemptionType;

					seg.popup.openPopup({
						title,
						templateUrl: resolvePath("./templates/exemptions.html"),
						bindings: {
							item: row.item
						}
					});
				}
			}
		});
	},
	getIncidents(vessel) {
		if (!vessel.get("incidents") || !vessel.get("incidents").length)
			return;

		seg.ui.ttt.tables.open({
			label: "Incidents Vessel #" + vessel.getId(),
			data: vessel.get("incidents"),
			itemAs: "item",
			fields: {
				incidentID: {
					label: "Incident ID",
					template: "{{item.IncidentID}}",
					visible: true
				},
				incidentType: {
					label: "Incident Type",
					template: "{{item.Type}}",
					visible: true
				}
			},
			onSelectionChange: (changedRows, selectedRows) => {
				selectedRows.forEach(row => {
					seg.modal.openModal("Get Incident Detail", "Retrieving incident details...");

					seg.request.get("v1/ship/ship_call/incident", {
						params: {
							incidentId: row.item.IncidentID
						}
					}).then(res => {
						const providedIncidentDetails = res.result.Body.ProvidedIncidentdetails ,
							incidentsData = providedIncidentDetails.Incidents?providedIncidentDetails.Incidents:{},
							feedbackList = providedIncidentDetails.FeedbackList? providedIncidentDetails.FeedbackList:[],
							feedbacksData = feedbackList && [].concat(feedbackList),
							incidentDocument = incidentsData.IncidentDetailsDocument? incidentsData.IncidentDetailsDocument.Base64Details: {};

						seg.modal.close("Get Incident Detail");

						if (incidentsData.IRVesselIdentificationList.IRVesselIdentification && !Array.isArray(incidentsData.IRVesselIdentificationList.IRVesselIdentification)){
							const temp = incidentsData.IRVesselIdentificationList.IRVesselIdentification;
							if (temp)
								incidentsData.IRVesselIdentificationList.IRVesselIdentification = [temp];
							else
								incidentsData.IRVesselIdentificationList.IRVesselIdentification = [];
						}

						//convert coordinates and set items to array
						if (row.item.Type === "POLREP") {
							const incidentDetails = incidentsData.IncidentDetails;

							if(incidentDetails) {
								const POLREPInfo = incidentDetails.POLREPIncidentInformation.POLREPInformation;
								if (POLREPInfo.POLWARN.P2_Position.GeoCoordinates) {
									const POLWARN_Position_Coordinates = POLREPInfo.POLWARN.P2_Position.GeoCoordinates,
										coord = ol.proj.toLonLat([POLWARN_Position_Coordinates.Longitude, POLWARN_Position_Coordinates.Latitude], "EPSG:3395");

									POLWARN_Position_Coordinates.Longitude = coord[0];
									POLWARN_Position_Coordinates.Latitude = coord[1];
								}

								let arrayTransformation;

								if ((typeof POLREPInfo.POLINF.P52_InformedStateOrg === "object") && !Array.isArray(POLREPInfo.POLINF.P52_InformedStateOrg)) {
									arrayTransformation = [POLREPInfo.POLINF.P52_InformedStateOrg];
									POLREPInfo.POLINF.P52_InformedStateOrg = arrayTransformation;
								}

								if ((typeof POLREPInfo.POLINF.P49_ObserverIdentity === "object") && !Array.isArray(POLREPInfo.POLINF.P49_ObserverIdentity)) {
									arrayTransformation = [POLREPInfo.POLINF.P49_ObserverIdentity];
									POLREPInfo.POLINF.P49_ObserverIdentity = arrayTransformation;
								}
							}
						}

						const shipPositionAtTimeOfIncident = Array.isArray(incidentsData.IRVesselIdentificationList.IRVesselIdentification) ?
							incidentsData.IRVesselIdentificationList.IRVesselIdentification[0].ShipPositionAtTimeOfIncident :
							incidentsData.IRVesselIdentificationList.IRVesselIdentification.ShipPositionAtTimeOfIncident;

						if (shipPositionAtTimeOfIncident && shipPositionAtTimeOfIncident.GeoCoordinates) {
							// Coordinates 1/10000 Minutes transformation of coordinates
							shipPositionAtTimeOfIncident.GeoCoordinates.Longitude = (shipPositionAtTimeOfIncident.GeoCoordinates.Longitude/10000)/60;
							shipPositionAtTimeOfIncident.GeoCoordinates.Latitude = (shipPositionAtTimeOfIncident.GeoCoordinates.Latitude/10000)/60;
						}

						const shipPositionAtTimeOfReporting = Array.isArray(incidentsData.IRVesselIdentificationList.IRVesselIdentification) ?
							incidentsData.IRVesselIdentificationList.IRVesselIdentification[0].ShipPositionAtTimeOfReporting :
							incidentsData.IRVesselIdentificationList.IRVesselIdentification.ShipPositionAtTimeOfReporting;

						if (shipPositionAtTimeOfReporting && shipPositionAtTimeOfReporting.GeoCoordinates) {
							// Coordinates 1/10000 Minutes transformation of coordinates
							shipPositionAtTimeOfReporting.GeoCoordinates.Longitude = (shipPositionAtTimeOfReporting.GeoCoordinates.Longitude/10000)/60;
							shipPositionAtTimeOfReporting.GeoCoordinates.Latitude = (shipPositionAtTimeOfReporting.GeoCoordinates.Latitude/10000)/60;
						}

						if (typeof incidentsData.IRDistributionDetails.IRRecipient !== "undefined")
							if (!Array.isArray(incidentsData.IRDistributionDetails.IRRecipient))
								incidentsData.IRDistributionDetails.IRRecipient = [incidentsData.IRDistributionDetails.IRRecipient];

						seg.popup.openPopup({
							title: "Incident " + row.item.Type,
							templateUrl: resolvePath("./templates/incidents.html"),
							bindings: {
								data: {
									incident: incidentsData,
									feedbacks: feedbacksData,
									document: incidentDocument
								},
								downloadDocument() {
									const extension = incidentDocument.DocType.toLowerCase();

									seg.request.download(incidentDocument.Base64Content, {
										base64: true,
										extension
									}, "teste."+extension);
								},
								type: row.item.Type,
								observerIdentityFields: {
									Name: {
										label: "Name",
										template: "{{item.Name || 'N/A'}}",
										visible: true
									},
									HomePort: {
										label: "Home Port",
										template: "{{item.HomePort || 'N/A'}}",
										visible: true
									},
									Flag: {
										label: "Flag",
										template: "{{(item.Flag | country) || 'N/A'}}",
										visible: true
									},
									callsign: {
										label: "Home Port",
										template: "{{item.CallSign || 'N/A'}}",
										visible: true
									}
								},
								P52_OtherStatesAndOrganizationsInformedFields: {
									Name: {
										label: "Name",
										template: "{{item.Name || 'N/A'}}",
										visible: true
									}
								},
								IRDistributionDetailsTTTFields: {
									RecipientCountry: {
										label: "Recipient Country",
										template: "{{(item.RecipientCountry | country) || 'N/A'}}",
										visible: true
									},
									ActionRequestedDetail: {
										label: "Action Req. Detail",
										template: "{{item.ActionRequestedDetail || 'N/A'}}",
										visible: true
									}
								}
							}
						});
					}, e => {
						seg.modal.close("Get Incident Detail");
						seg.log.error("ERROR_VESSELS_GET_INCIDENT_ON_SELECTION_CHANGE", e.error + ": " + e.result);
					});
				});
			}
		});
	},
	enrichmentAvailability(vessel, flag) {
		const normalizedFlag = flag.toUpperCase(),
			enrichment = vessel.get("enrichment");

		if ((vessel.get("imo") && enrichment) && (
			((normalizedFlag === "HAZMAT") && (enrichment.HazmatOnBoardYorN && enrichment.HazmatOnBoardYorN === "Y")) ||
			((normalizedFlag === "MRS") && (enrichment.mrsHazmatOnBoard && enrichment.mrsHazmatOnBoard === "Y")) ||
			((normalizedFlag === "WASTE") && enrichment.WasteDeliveryStatus) ||
			((normalizedFlag === "INCIDENTS") && (enrichment.activeIncidents && enrichment.activeIncidents === "Y") && (vessel.get("incidents") && vessel.get("incidents").length)) ||
			((normalizedFlag === "SECURITY") && (enrichment.CurrentSecurityLevel))
		))
			return;

		return "disabled";
	},
	SSNTabAvailability(vessel) {
		const enrichment = vessel.get("enrichment");

		//if no enrichment or
		if (!enrichment ||
			//no hazmat and
			((enrichment.HazmatOnBoardYorN === "N" || !enrichment.HazmatOnBoardYorN) &&
			//no mrs hazmat and
			(enrichment.mrsHazmatOnBoard === "N" || !enrichment.mrsHazmatOnBoard) &&
			//no security level info and
			!enrichment.CurrentSecurityLevel &&
			//no waste info
			!enrichment.WasteDeliveryStatus))
			return "disabled";

		return "";
	},
	moreInfoOrLoadingHAZMAT: "More Information",
	moreInfoOrLoadingSecurity: "More Information",
	moreInfoOrLoadingWaste: "More Information",
	moreInfoOrLoadingMRS: "More Information"
});
