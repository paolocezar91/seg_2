const { getParticulars } = require("../api.js"),
	{ getSelected } = require("./symbolizers/index.js"),
	VESSEL_OUTLINE_SCALE = 4265;

const selectedStyleCache = {},
	favouriteStyleCache = {},
	searchResultStyleCache = {},
	riskStyleCache = {},
	deadReckoningCache = {},
	followingStyleCache = {},
	creatingModel = {},
	vesselBaseModelCache = {},
	modelUnavailable = {};

const createVesselBaseModel = async vessel => {
	const id = vessel.getId();
	creatingModel[id] = true;

	await getParticulars(vessel);

	delete creatingModel[id];

	const dimensions = vessel.get("dimensions");

	if (dimensions && dimensions.a && dimensions.b && dimensions.c && dimensions.d) {
		const bowLength = (dimensions.a + dimensions.b) / 5;

		return new ol.geom.Polygon([[
			[(-dimensions.c + dimensions.d) / 2, dimensions.a],
			[dimensions.d, dimensions.a - bowLength],
			[dimensions.d, -dimensions.b],
			[-dimensions.c, -dimensions.b],
			[-dimensions.c, dimensions.a - bowLength],
			[(-dimensions.c + dimensions.d) / 2, dimensions.a]
		]]);
	}
};

const getVesselBaseModel = vessel => {
	const id = vessel.getId();

	if (vesselBaseModelCache[id])
		return vesselBaseModelCache[id];

	if (!creatingModel[id] && !modelUnavailable[id])
		createVesselBaseModel(vessel).then(model => {
			vesselBaseModelCache[id] = model;
			vessel.changed();
		}, () => modelUnavailable[id] = true);
};

const style = (vessel) => {
	const show = seg.preferences.workspace.get("show");
	let styles = [];

	if (vessel.get("isSearchResult")) {
		const size = Math.max(1, Math.min(30, Math.round(6600000 / seg.map.getScale()) * 12));

		if(!searchResultStyleCache[size])
			searchResultStyleCache[size] = new ol.style.Style({
				image: new ol.style.Circle({
					radius: size/2,
					fill: new ol.style.Fill({
						color: "rgba(88, 166, 74, .5)"
					})
				})
			});

		styles.push(searchResultStyleCache[size]);
	}

	if (typeof seg.preferences.session.get("following") !== "undefined") {
		if (Number(seg.preferences.session.get("following").feature.getId()) === Number(vessel.getId())) {
			const size = Math.max(1, Math.min(36, Math.round(6600000 / seg.map.getScale()) * 12)),
				cacheKey = JSON.stringify({size});

			if (!followingStyleCache[cacheKey])
				followingStyleCache[cacheKey] = new ol.style.Style({
					image: new ol.style.Circle({
						radius: size/2,
						fill: new ol.style.Fill({
							color: "#e8f5fc"
						})
					})
				});

			styles.push(followingStyleCache[cacheKey]);
		}
	}

	if(show.vesselModel && seg.map.getScale() <= VESSEL_OUTLINE_SCALE && typeof vessel.get("th") !== "undefined") {
		const vesselBaseModel = getVesselBaseModel(vessel);

		if(vesselBaseModel) {
			const position = vessel.getGeometry().getCoordinates(),
				vesselModelClone = vesselBaseModel.clone();

			vesselModelClone.setCoordinates([vesselModelClone.getCoordinates()[0].map(vertex => vertex.map(coordinate => coordinate / seg.map.getProjection().getMetersPerUnit()))]);
			vesselModelClone.translate(position[0], position[1]);
			vesselModelClone.rotate(-vessel.get("th") * Math.PI / 180, position);

			styles.push(new ol.style.Style({
				geometry: vesselModelClone,
				fill: new ol.style.Fill({
					color:"#fff"
				}),
				stroke: new ol.style.Stroke({
					"color": "#00f",
					"width": 1
				})
			}));
		} else
			styles = styles.concat(getSelected().style(vessel));
	} else
		styles = styles.concat(getSelected().style(vessel));

	if (vessel.get("selected")) {
		const radius = 40 / 2;

		if (!selectedStyleCache[radius])
			selectedStyleCache[radius] = new ol.style.Style({
				image: new ol.style.Circle({
					radius,
					stroke: new ol.style.Stroke({
						color: "#006ebc",
						width: 2
					})
				})
			});

		styles.push(selectedStyleCache[radius]);
	}

	// if (seg.favourites.isFavourite(vessel)) {
	// 	// const timestamp = moment(vessel.get("ts")).clone(),
	// 	// 	twoHoursAgo = moment().clone().subtract(170, "minutes");

	// 	// if (!timestamp.isBetween(twoHoursAgo, moment()))
	// 	// 	return;

	// 	if (!vessel.get("visible"))
	// 		return;

	// 	const radius = 40 / 2,
	// 		color = seg.favourites.getGroupFromFeature(vessel).color || "rgb(201, 151, 38)";

	// 	if (!favouriteStyleCache[radius,color])
	// 		favouriteStyleCache[radius,color] = new ol.style.Style({
	// 			image: new ol.style.Circle({
	// 				radius,
	// 				stroke: new ol.style.Stroke({
	// 					color,
	// 					width: 2
	// 				})
	// 			})
	// 		});

	// 	styles.push(favouriteStyleCache[radius,color]);
	// }

	const risk = vessel.get("risk");

	if (risk && risk !== "") {
		const riskBadgeSize = Math.min(25, Math.max(18, Math.round(5500000 / seg.map.getScale()) * 12)),
			riskLetterSize = Math.min(8, Math.round(2200000 / seg.map.getScale()) * 12),
			riskCircleRadius = (seg.preferences.map.get("vessels.positions.riskstatus"))?Math.min(40, Math.max(1, Math.round(8800000 / seg.map.getScale()) * 12)) / 2:0,
			cacheKey = JSON.stringify({
				risk,
				riskBadgeSize,
				riskLetterSize,
				riskCircleRadius
			});

		if(!riskStyleCache[cacheKey]) {
			let color;

			switch(risk) {
				case "High": color = "#F00"; break;
				case "Medium": color = "#c98c01"; break;
				case "Low": color = "#59a64b"; break;
			}

			riskStyleCache[cacheKey] = [
				new ol.style.Style({
					text: new ol.style.Text({
						font: riskBadgeSize+"px sans-serif",
						text: "●",
						fill: new ol.style.Fill({
							color
						}),
						offsetX: 10,
						offsetY: -11,
						textBaseline: "middle"
					})
				}),
				new ol.style.Style({
					text: new ol.style.Text({
						font: riskLetterSize+"px Fira Sans",
						text: risk.substr(0, 1),
						fill: new ol.style.Fill({
							color: "#fff"
						}),
						offsetX: 10,
						offsetY: -8,
						textBaseline: "middle"
					})
				})
			];

			/*if (riskCircleRadius)
				riskStyleCache[cacheKey].push(new ol.style.Style({
						image: new ol.style.Circle({
							riskCircleRadius,
							stroke: new ol.style.Stroke({
								color: color,
								width: 2
							})
						})
					}));*/

		}

		styles.push(...riskStyleCache[cacheKey]);
	}

	if (show.deadReckoning) {
		if (vessel.get("hdg") && vessel.get("speed")) {
			const {lon, lat, hdg, speed} = vessel.getProperties(),
				cacheKey = JSON.stringify({
					lon,
					lat,
					hdg,
					speed,
					projection: seg.map.getProjection().getCode()
				});

			if (!deadReckoningCache[cacheKey]) {
				const vesselCoordinates = [vessel.get("lon"), vessel.get("lat")],
					convertedHeading = vessel.get("hdg") > 180?vessel.get("hdg") - 360:vessel.get("hdg"),
					destinationPoint = turf.destination(turf.point(vesselCoordinates), vessel.get("speed") * 1.852, convertedHeading).geometry.coordinates,
					geometry = new ol.geom.LineString([vesselCoordinates, destinationPoint]);

				geometry.transform("EPSG:4326", seg.map.getProjection());

				deadReckoningCache[cacheKey] = new ol.style.Style({
					geometry,
					stroke: new ol.style.Stroke({
						"color": "#00f",
						"width": 1
					})
				});
			}

			styles.push(deadReckoningCache[cacheKey]);
		}
	}

	return styles;
};

module.exports = style;