const {
		mergeDuplicates,
		getLastKnownPosition,
		getLabel
	} = require("../api.js"),
	//{mapProperties} = require("./properties.js"),
	positionsSource = seg.layer.get("positions").getSource();

const sanitizeParameters = (parameters) => {
	const params = {};

	Object.assign(params, parameters);

	for (const property in params) {
		if (params.hasOwnProperty(property) && (params[property] === null || params[property] === ""))
			delete params[property];
		if(params[property] instanceof moment)
			params[property] = params[property].toISOString();
	}

	return params;
};

const addProjectSpecificParams = (project, projectSpecific) => {
	const parameters = {};
	projectSpecific.forEach(field => {
		let result = "";
		switch(field.mode){
			case "facet":
				if (field.facets)
					field.facets.forEach(facet => {
						if (facet.checked)
							result = !result.length ? result.concat(facet.label) : result.concat("," + facet.label);
					});
				break;
			case "search":
				if (field.type === "string" || field.type === "enum")
					if (typeof field.value_ === "string")
						result = field.value_;
				if(field.type === "date" && (field.value_ instanceof moment))
					if (field.name.includes("PERMIT") && parameters[project + "_CATCHPERMIT"] === "Y")
						result = field.value_.toISOString();
				break;
		}

		parameters[project + "_" + field.name] = result;
	});
	return parameters;
};

const updateSpecificInfo = () =>  {
	const project = seg.operation.project,
		searchConfig = seg.ui.searchAdvanced.getPanelByName("Vessels"),
		{fields, search} = searchConfig.services,
		{showProjectSpecific, projectSpecific} = search;

	if (showProjectSpecific)
		Object.assign(fields, addProjectSpecificParams(project, projectSpecific));

	const specificFieldsRequests = {
		projectDescriptorResponse: seg.request.get("v1/advanced_search/project_descriptor"),
		facetsResponse: seg.request.get("v1/advanced_search", {
			params: showProjectSpecific ? sanitizeParameters(fields) : fields
		})
	};

	seg.promise.all(specificFieldsRequests)
		.then(({projectDescriptorResponse, facetsResponse}) => {
			const facetCounts = facetsResponse.result.facet_counts;

			if (!facetCounts)
				return;

			const facets = facetCounts.facet_fields,
				normalizedFacets = {};

			for (const facetName in facets)
				if (facets.hasOwnProperty(facetName)) {
					const properlyOrganizedFacets = [];
					for (let i = 0; i < facets[facetName].length - 1; i = i + 2){
						properlyOrganizedFacets.push({
							label: facets[facetName][i] ? facets[facetName][i] : "N/A",
							qty: facets[facetName][i + 1],
							checked:((facetName, facetLabel) => {
								// Workaround needed for EFCA_IMS (and any other operation with an underscore on the name), which facets have additional(s) underscore(s) and break the checked script
								const splitLevel = project.split("_").length;

								let _facetName = facetName.split("_")[splitLevel];

								if (facetName.split("_")[splitLevel + 1]) // workaround when facetName has 2 underscores
									_facetName += "_" + facetName.split("_")[splitLevel + 1];

								const facet = projectSpecific.find(field => (field.mode === "facet" && field.name.toUpperCase() === _facetName.toUpperCase()));

								if (facet)
									return facet.facets.filter(f => {
										return (f.checked && f.label === facetLabel);
									}).length ? true : false;

								return false;
							})(facetName, facets[facetName][i] ? facets[facetName][i] : "N/A")
						});
					}
					normalizedFacets[facetName.toUpperCase()] = properlyOrganizedFacets;
				}

			projectDescriptorResponse.result.project_specific_fields.map(field => {
				if ((field.mode === "facet") && (normalizedFacets[project + "_" + field.name.toUpperCase()])){
					projectSpecific.filter(f => f.name.toUpperCase() === field.name.toUpperCase())[0].facets = normalizedFacets[project + "_" + field.name.toUpperCase()].sort((a,b)=> {
						if(a.label < b.label) return -1;
						if(a.label > b.label) return 1;
						return 0;
					});
				}
			});

			searchConfig.services.search.projectSpecific = Object.assign(searchConfig.services.search.projectSpecific, projectSpecific);
		}, e => seg.log.error("ERROR_VESSELS_SEARCH_UPDATE_SPECIFIC_INFO", e.error + ": " + e.result));
};

const searchConfig = (/*configuration*/) => {
	return {
		name: "Vessels",
		template: resolvePath("./templates/search.html"),
		services: (() => {
			const config = {
				fields: {
					imo: null,
					mmsi: null,
					callSign: null,
					shipName: null,
					ir: null,
					shipType: null,
					flagState: null
					// hazmat: null,
					// sl_above_one: null,
					// waste: null,
					// incident: null,
				},
				// ssn_information: configuration.show_ssn_information,
				search: {
					selectedFile: null,
					projectSpecific: [],
					generalOptions: [{
						label: "Yes",
						value: true
					}, {
						label: "No",
						value: false
					}, {
						label: "All",
						value: null
					}],
					wasteOptions: [{
						label: "All",
						value: "All"
					}, {
						label: "Some",
						value: "Some"
					}, {
						label: "None",
						value: "None"
					}, {
						label: "All Status",
						value: null
					}],
					securityOptions: [{
						label: "SL1",
						value: "SL1"
					}, {
						label: "SL2",
						value: "SL2"
					}, {
						label: "SL3",
						value: "SL3"
					}, {
						label: "All Status",
						value: null
					}],
					updateSpecificInfo: updateSpecificInfo,
					vesselType:{
						_fullOptions: Object.entries(require("../api.js").PSC_SHIP_TYPES).map(([label, value]) => ({label, value, selected: false})),
						options: Object.entries(require("../api.js").PSC_SHIP_TYPES).map(([label, value]) => ({label, value, selected: false})),
						changeCallback:filter =>{
							config.search.vesselType.options = config.search.vesselType._fullOptions.filter(type => type.label.toLowerCase().includes(filter.toLowerCase()));
						}
					},
					vesselFlag:{
						_fullOptions: Object.entries(require("../../../common/flag_states_obj")).map((element) => ({label:element[1].name, value:element[1].code, selected: false})),
						options: Object.entries(require("../../../common/flag_states_obj")).map((element) => ({label:element[1].name, value:element[1].code, selected: false})),
						changeCallback:filter => {
							config.search.vesselFlag.options = config.search.vesselFlag._fullOptions.filter(flag => flag.label.toLowerCase().includes(filter.toLowerCase()));
						}
					}
				},
				clearSSNEnrichmentOptions() {
					if (!config.search.SSNEnrichment)
						config.fields.hazmat = config.fields.sl_above_one = config.fields.waste = config.fields.incidents = null;
				},
				selectFile() {
					const input = document.createElement("input");
					input.setAttribute("style", "display: hidden");
					input.type = "file";
					document.body.appendChild(input);
					input.click();
					input.addEventListener("change", e => {
						seg.timeout(async () => {
							config.search.selectedFile = e.target.files[0];
							config.search.selectedFile.importLabel = "advancedSearch";
							return config.search.selectedFile;
						});
					});
				},
				async importCSV(){
					let data;

					const modal = seg.modal.openModal("Importing data", "Importing data from file...");

					try {
						data = (await seg.import(config.search.selectedFile)).vessel;
						modal.setContent("CSV imported successfully.");
						seg.modal.closeTimeout(modal.title);
						config.fields.imo = data.reduce((data, item) => item.IMO ? data.concat(item.IMO) : data, []).join(",");
						config.fields.mmsi = data.reduce((data, item) => item.MMSI ? data.concat(item.MMSI) : data, []).join(",");
						config.fields.callSign = data.reduce((data, item) => item.CALL ? data.concat(item.CALL) : data, []).join(",");
						config.fields.shipName = data.reduce((data, item) => item.NAME ? data.concat(item.NAME) : data, []).join(",");
						config.fields.ir = data.reduce((data, item) => item.IR ? data.concat(item.IR) : data, []).join(",");
					} catch(e) {
						modal.setContent("Error importing for CSV: " + e.message);
						seg.modal.closeTimeout(modal.title);
					}
				}
			};

			const project = seg.operation.project,
				specificFieldsRequests = {
					projectDescriptorResponse: seg.request.get("v1/advanced_search/project_descriptor"),
					facetsResponse: seg.request.get("v1/advanced_search/facets")
				};

			seg.promise.all(specificFieldsRequests)
				.then(({projectDescriptorResponse, facetsResponse}) => {
					const facetCounts = facetsResponse.result.facet_counts;

					if (!facetCounts)
						return;

					const facets = facetCounts.facet_fields,
						normalizedFacets = {};

					for (const facetName in facets)
						if (facets.hasOwnProperty(facetName)) {
							const properlyOrganizedFacets = [];
							for (let i = 0; i < facets[facetName].length - 1; i = i + 2)
								properlyOrganizedFacets.push({
									label: facets[facetName][i] ? facets[facetName][i] : "N/A",
									qty: facets[facetName][i + 1],
									checked: false
								});
							normalizedFacets[facetName.toUpperCase()] = properlyOrganizedFacets;
						}

					const projectDescriptor = projectDescriptorResponse.result.project_specific_fields.map(field => {
						if ((field.mode === "facet") && (normalizedFacets[project + "_" + field.name.toUpperCase()]))
							field.facets = normalizedFacets[project + "_" + field.name.toUpperCase()].sort((a,b) => {
								if(a.label < b.label) return -1;
								if(a.label > b.label) return 1;
								return 0;
							});

						if(field.values && field.values.length && !~field.values.findIndex((value) => value.text === "N/A"))
							field.values.unshift({text: "N/A", value: false});

						if (field.type === "date")
							return Object.assign(field, {value_: moment()});

						if (field.type === "enum")
							return Object.assign(field, {value_: field.values[0]});

						if (field.range)
							return Object.assign(field, {value_: field.min_value});

						return Object.assign(field, {value_: null});
					});

					config.search.projectSpecific = projectDescriptor;
				}, e => seg.log.error("ERROR_VESSELS_SEARCH_CONFIG", e.error + ": " + e.result));

			return config;
		})(),
		canSubmit() {
			//check regular fields
			for (const key in this.services.fields)
				if (this.services.fields.hasOwnProperty(key) && this.services.fields[key] && this.services.fields[key] !== "")
					return true;

			const {showProjectSpecific, projectSpecific} = this.services.search;

			//check project specific facets
			if (projectSpecific && showProjectSpecific)
				return Object.values(addProjectSpecificParams(seg.operation.project, projectSpecific)).some(values => values !== "");
		},
		submit() {
			const modal = seg.modal.openModal("Advanced Search Vessels", "Retrieving search results..."),
				project = seg.operation.project,
				parameters = Object.assign({}, this.services.fields),
				{showProjectSpecific, projectSpecific} = this.services.search;

			if (projectSpecific && showProjectSpecific)
				Object.assign(parameters, addProjectSpecificParams(project, projectSpecific));

			seg.request.get(showProjectSpecific ? "v1/advanced_search?" : "v1/ship/imdateID", {
				params: showProjectSpecific ? sanitizeParameters(parameters) : parameters
			}).then(async ({result}) => {
				if (Array.isArray(result.results))
					result = result.results;

				// reseting isSearchResult value
				positionsSource.getFeatures().forEach(feature => {
					if (feature.get("isSearchResult"))
						feature.set("isSearchResult", false);
				});

				// no results found
				if (!result.length) {
					modal.setContent("<span>Found <b style=\"color:red;\">0</b> results.</span>");
					seg.modal.closeTimeout(modal.title);
				} else {
					// TODO: CREATE FEATURES OF ALL RESULTS WITH 0,0 COORDINATES, THEN FIX GEOMETRY OF THOSE RETURNED BY GETLASTKNOWNETC
					// normalize properties of vessel use get Current to complete information
					// const vesselsWithPositions = await getLastKnownVesselsPosition(result.map(vesselData => Number(vesselData.imdateId || vesselData.id))),
					// 	vesselsWithoutPositions = new ol.format.GeoJSON().readFeatures({
					// 		type: "FeatureCollection",
					// 		crs: {properties: {name: "EPSG:4326"}, type:"name"},
					// 		features: result.filter((vessel) => {
					// 			return !~vesselsWithPositions.findIndex(vwp => {
					// 				return (vessel && vessel.id) === (vwp && (vwp.imdateId || vwp.id));
					// 			});
					// 		}).map(properties => {
					// 			return {
					// 				properties,
					// 				id: properties.vid,
					// 				type: "Feature",
					// 				geometry: {
					// 					type: "Point",
					// 					coordinates: ([0, 0])
					// 				}
					// 			};
					// 		})
					// 	}, {
					// 		dataProjection: "EPSG:4326",
					// 		featureProjection: seg.map.getProjection()
					// 	}),
					// 	normalizedResults = await mergeDuplicates([].concat(vesselsWithPositions, vesselsWithoutPositions).filter(vessel => vessel));

					const normalizedResults = result.map(vessel => ({
						mmsi: vessel.MMSINumber || vessel.mmsi,
						imo: vessel.IMONumber || vessel.imo,
						cs: vessel.CallSign || vessel.callSign || vessel.call_sign,
						shipName: vessel.ShipName || vessel.shipName || vessel[project + "_VesselName"],
						csdId: vessel.CSDID,
						id: vessel.imdateId || vessel.id,
						lastUpdate: vessel.last_update,
						isSearchResult: true,
						shipType: vessel.shipType,
						flagState: vessel.countryCode || vessel.flagState

					}));

					//positionsSource.addFeatures(normalizedResults);

					modal.setContent("<span>Found <b style=\"color:green;\">" + normalizedResults.length + "</b> results.</span>");
					seg.modal.closeTimeout(modal.title);

					let advancedSearchTable = seg.ui.ttt.tables.openTables.find(t => t.label === "Adv. Search Vessels");

					// ttt already registeredm, only updating data
					if (advancedSearchTable)
						advancedSearchTable.data = normalizedResults;
					else //setting up new ttt
						advancedSearchTable = seg.ui.ttt.tables.open({
							label: "Adv. Search Vessels",
							customToolbar: {
								template: "<span class=\"text-size-sm\">Number of vessels: <b>{{grid.filteredRows.length}}</b></span>"
							},
							checkboxes: true,
							data: normalizedResults,
							itemAs: "vessel",
							fields: {
								mmsi: {
									label: "MMSI",
									template: "{{vessel.mmsi || \"N/A\" }}",
									visible: true
								},
								imo: {
									label: "IMO",
									template: "{{vessel.imo || \"N/A\"}}",
									visible: true
								},
								name: {
									label: "Ship Name",
									template: "{{vessel.shipName  || \"N/A\"}}",
									visible: true
								},
								flagState:{
									label: "Flag State",
									template:  "{{vessel.flagState || \"N/A\"}}",
									visible: true
								},
								callSign: {
									label: "Call Sign",
									template: "{{vessel.cs || \"N/A\"}}",
									visible: true
								},
								lastUpdate: {
									label: "Last Update",
									template: "{{vessel.lastUpdate || \"N/A\"}}",
									visible: true
								}
							},
							async onSelectionChange(changedRows, selectedRows) {
								if (!selectedRows || !selectedRows[0])
									return;

								if (selectedRows.length === 1) {
									const selectedVessel = selectedRows[0].item;

									if (selectedVessel) {
										const label = selectedVessel.shipName || "MMSI: " + selectedVessel.mmsi || "IMO: " + selectedVessel.imo;
										const _modal = seg.modal.openModal("Last position", "Searching last known position of " + label);

										let vesselCurrentPosition = positionsSource.getFeatures().find(vessel => {
											return (vessel.get("mmsi") && vessel.get("mmsi") === selectedVessel.mmsi) ||
												(vessel.get("imo") && vessel.get("imo") === selectedVessel.imo);
										});

										if (!vesselCurrentPosition)
											vesselCurrentPosition = (await getLastKnownPosition(selectedVessel));

										// If any position is found, merge it with duplicates, select and center it
										if(vesselCurrentPosition) {
											vesselCurrentPosition.setProperties(Object.assign(vesselCurrentPosition.getProperties(), selectedVessel));

											vesselCurrentPosition = !Array.isArray(vesselCurrentPosition) ? [vesselCurrentPosition] : vesselCurrentPosition;

											const feature = (await mergeDuplicates(vesselCurrentPosition))[0];
											positionsSource.addFeature(feature);
											feature.setProperties({
												"visible": true,
												"isSearchResult": true
											});
											seg.selection.selectFeatureAndCenter(feature);

											_modal.setContent("Last position of " + label + " found");
											seg.modal.closeTimeout(_modal.title);
										} else {
											_modal.setContent("Last position of " + label + " is unknown");
											seg.modal.closeTimeout(_modal.title);
										}
									}
								}
							}
						});

					seg.ui.cleanables.add("Adv. Search Vessels", () => {
						seg.ui.ttt.tables.close("Adv. Search Vessels");
						seg.ui.cleanables.remove("Adv. Search Vessels");
						positionsSource.getFeatures().forEach(feature => {
							if (feature.get("isSearchResult"))
								feature.set("isSearchResult", false);
						});
					});
				}
			}, e => {
				modal.setContent("<span>Search Failed</span>");
				seg.modal.closeTimeout(modal.title);
				seg.log.error("ERROR_VESSELS_SEARCH_SUBMIT", e.error + ": " + e.result);
			});
		},
		reset() {
			delete this.services.fields.project;
			delete this.services.fields.user;

			Object.keys(this.services.fields).forEach(key => {
				this.services.fields[key] = null;
			});

			if (this.services.search.showProjectSpecific) {
				this.services.search.projectSpecific.forEach(field => {
					if (field.type === "string")
						field.value_ = null;
					if (field.type === "enum")
						field.value_ = field.values[0];
					if (Array.isArray(field.facets) && field.facets.length)
						field.facets.forEach(facet => facet.checked = false);
					if (field.type === "date")
						field.value_ = moment();
				});
				updateSpecificInfo();
			}
		},
		save(searchName) {
			seg.search.getSavedSearches().then(res => {
				const savedSearchesSmart = res[1],
					savedSearches = res[0];

				savedSearches.push({
					name: searchName,
					config: this.services,
					src: "Vessels",
					advanced: true
				});

				seg.search.setSavedSearches(savedSearches.concat(savedSearchesSmart));
			}, e => seg.log.error("ERROR_VESSELS_SEARCH_SAVE", e.error + ": " + e.result));
		},
		load(search) {
			//open the advanced search panel
			this.services = search.config;
		}
	};
};

module.exports = searchConfig;
