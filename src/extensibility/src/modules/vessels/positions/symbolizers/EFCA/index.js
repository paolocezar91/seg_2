const {createIALASymbolizer} = require("../IALA/index.js");

const styleCache = {};

const style = vessel => {
	const baseSymbolizer = createIALASymbolizer(vessel),
		{rotation, height} = baseSymbolizer;
	let fill = baseSymbolizer.fill;

	//Doesn"t belong to ICCAT
	if(!vessel.get("sai") && typeof vessel.get("st") !== "undefined" && vessel.get("st") === 315)
		fill = "#ff69b4";
	//belongs to ICCAT
	else if(vessel.get("sai") && (typeof vessel.get("sai")["PERMIT FROM"] !== "undefined") && (typeof vessel.get("sai")["PERMIT TO"] !== "undefined")) {
		if (moment().isSameOrAfter(moment(vessel.get("sai")["PERMIT FROM"], "DD/MM/YYYY")) && moment().isSameOrBefore(moment(vessel.get("sai")["PERMIT TO"], "DD/MM/YYYY").toISOString()))
			fill = "#0F0";
		else fill = "#F00";
	}

	const cacheKey = JSON.stringify({fill, rotation, height});

	if(!styleCache[cacheKey]) {
		baseSymbolizer.fill = fill;

		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG(baseSymbolizer)
		});
	}

	return styleCache[cacheKey];
};

module.exports = {
	name: "EFCA",
	style
};
