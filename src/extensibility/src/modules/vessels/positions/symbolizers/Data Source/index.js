const {createIALASymbolizer} = require("../IALA/index.js"),
	styleCache = {};

const style = vessel => {
	const baseSymbolizer = createIALASymbolizer(vessel),
		{rotation, height} = baseSymbolizer,
		vesselSource = vessel.get("src");
	let {stroke, fill} = baseSymbolizer;

	if (vesselSource !== "undefined") {
		switch (vesselSource) {
			case "T-AIS":
				fill = "#000";
				break;
			case "Sat-AIS":
				fill = "#00f";
				break;
			case "LRIT":
				fill = "#999";
				break;
			case "MRS":
				fill = "#795548";
				break;
			case "VMS":
				fill = "#ff0";
				break;
			case "VDS":
				fill = "#f0f";
				break;
			case "Sighting":
				stroke = {color: "#0066ff", width: 4};
				fill = "#fdfe02";
				break;
			case "Inspections no infrigement":
				stroke = {color: "#0066ff", width: 4};
				fill = "#00cc00";
				break;
			case "Inspections infrigement":
				stroke = {color: "#0066ff", width: 4};
				fill = "#f00";
				break;
			case "Other":
				fill = "#d9d9d9";
				break;
			default:
				fill = "#fff";
		}
	}

	const cacheKey = JSON.stringify({fill, rotation, height, stroke});

	if(!styleCache[cacheKey]) {
		baseSymbolizer.fill = fill;
		baseSymbolizer.stroke = stroke;

		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG(baseSymbolizer)
		});
	}

	return styleCache[cacheKey];
};

module.exports = {
	name: "Data Source",
	style
};
