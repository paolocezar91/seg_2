const {createIALASymbolizer} = require("../IALA/index.js");

const styleCache = {};

const riskLevelColors = {
	"SEVERE":"#F00",
	"SUBSTANTIAL": "#FFA500",
	"MODERATE": "#0F0",
	"LOW":"#00F",
	"undefined": "#FF0"
};
/*
let getRiskLevelColor = vessel => {
	if vessel.get("sai").RiskAssessment

}
*/
const fishingVesselColors = {
	"ICCAT vessels":"#F00",
	"Engaged in Fishing": "#FF69B4",
	"noFishing": "#000"
};


const getFishingVesselColors = vessel => {
	if (vessel.get("ns")===7)
		return "Engaged in Fishing";
	if (typeof vessel.get("sai") === "object")
		return "ICCAT vessels";

	return "noFishing";
};

const style = vessel => {
	const baseSymbolizer = createIALASymbolizer(vessel),
		{fill, rotation, height} = baseSymbolizer,
		risk = vessel.get("sai").RiskAssessment,
		fishing = getFishingVesselColors(vessel);


	const cacheKey = JSON.stringify({fill, rotation, height, risk,fishing});

	if(!styleCache[cacheKey]) {
		baseSymbolizer.fill = riskLevelColors[risk];
		baseSymbolizer.stroke = {
			color: fishingVesselColors[fishing],
			width: 1
		};


		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG(baseSymbolizer)
		});
	}

	return styleCache[cacheKey];
};

module.exports = {
	name: "EUNAVFOR",
	style
};

