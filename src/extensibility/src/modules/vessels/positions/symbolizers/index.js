let DEFAULT_SYMBOLIZER;
const RISK_STATUS_PREFS = "vessels.positions.riskstatus",
	symbolizers = {
		"IALA": require("./IALA/index.js"),
		"EFCA": require("./EFCA/index.js"),
		"Data Source": require("./Data Source/index.js"),
		//symbolizer group
		"Target Age": require("./Target Age/index.js"),
		"Target Age Color": require("./Target Age/color.js"),
		"Target Age Opacity": require("./Target Age/opacity.js"),
		"Vessel Type": require("./Vessel Type/index.js"),
		"Voyage Information": require("./SSN Enrichment/index.js"),
		"EUNAVFOR": require("./EUNAVFOR/index.js"),
		"Gear Type": require("./Gear Type/index.js"),
		"RFMO Status": require("./RFMO Status/index.js")
	},
	getSelected = () => symbolizers[seg.preferences.map.get("vessels.positions.symbolizer")] || DEFAULT_SYMBOLIZER,
	select = symbolizer => seg.preferences.map.set("vessels.positions.symbolizer", symbolizer.name),
	isSelected = symbolizer => symbolizer.name === getSelected().name,
	buildSymbolizerMenu = symbolizerNames => {
		const positionsSource = seg.layer.get("positions").getSource();
		//CHECK HERE FOR SYMBOLIZER NAME
		DEFAULT_SYMBOLIZER = symbolizers[symbolizerNames[0]];

		return Object.assign(seg.ui.layerMenu.create("Symbology"), {
			options: symbolizerNames.map(symbolizerName => {
				const symbolizer = symbolizers[symbolizerName],
					menuItem = seg.ui.layerMenu.create(symbolizer.name);

				if(Array.isArray(symbolizer))
					return Object.assign(menuItem, {
						isChecked() {
							return symbolizer.some(isSelected);
						},
						toggle() {
							if(!symbolizer.some(isSelected)){
								select(symbolizer[0]);
								positionsSource.changed();
							}
						},
						hideSelectAll: true,
						options: symbolizer.map(symbolizer => Object.assign(seg.ui.layerMenu.create(symbolizer.name), {
							isChecked() {
								return isSelected(symbolizer);
							},
							toggle() {
								select(symbolizer);
								positionsSource.changed();
							}
						}))
					});

				if (symbolizerName === "Risk Status"){
					return Object.assign(menuItem, {
						isChecked() {
							return seg.preferences.map.get(RISK_STATUS_PREFS);
						},
						toggle() {
							seg.preferences.map.set(RISK_STATUS_PREFS, !seg.preferences.map.get(RISK_STATUS_PREFS));
							positionsSource.changed();
						}
					});
				}

				return Object.assign(menuItem, {
					isChecked() {
						return isSelected(symbolizer);
					},
					toggle() {
						select(symbolizer);
						positionsSource.changed();
					}
				});
			}),
			hideSelectAll: true,
			info: {
				title: "Symbolizers",
				templateUrl: resolvePath("../templates/menu_tooltip.html"),
				bindings: require("../menu_tooltip.scope.js")
			},
			type: "symbology"
		});
	};

module.exports = {
	buildSymbolizerMenu,
	getSelected,
	symbolizers
};
