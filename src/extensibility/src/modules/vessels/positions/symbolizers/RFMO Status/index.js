const {createIALASymbolizer} = require("../IALA/index.js");

const isFishingVessel = (vessel) =>  ((vessel.get("shipType") && vessel.get("shipType") === 315) || (vessel.get("ns") && vessel.get("ns")===7)) || (vessel.get("sai") && (vessel.get("sai")["Gear"])) ;

const isEngagedInFishing = vessel  => vessel.get("ns") && (vessel.get("ns")===7);

const isPermitAvailable = vessel =>  vessel.get("sai") && (vessel.get("sai")["PERMIT FROM"] && vessel.get("sai")["PERMIT TO"]);

const hasValidPermit = (vessel) => isPermitAvailable(vessel) && moment().isSameOrAfter(moment(vessel.get("sai")["PERMIT FROM"], "DD/MM/YYYY")) && moment().isSameOrBefore(moment(vessel.get("sai")["PERMIT TO"], "DD/MM/YYYY").toISOString());


const RMFOLIST_COLORS = new Map([
	["ICCAT"  , { color: "#de5920", width: 3}],
	["NEAFC"  , { color: "#3e8e44", width: 3}],
	["NAFO"   , { color: "#1cadea", width: 3}],
	["EU"     , { color: "#9d9d9d", width: 3}],
	["VARIOUS", { color: "#fb0013", width: 3}],
	["OTHER"  , { color: "#000000", width: 1}]
]);

const RMFOSTATUS_COLORS = new Map([
	["RMFO_VALIDPERMIT"  ,	"#3bc741"],
	["RMFO_INVALIDPERMIT", 	"#f41616"],
	["RMFO_NOPERMIT"     , 	"#8d37f7"],
	["RMFO_NOFISHING"	 ,	"#0866f8"],
	["RMFO_PATROL"       ,	"#fffc40"],
	["NORMFO_FISHING"    , 	"#8d37f7"],
	["NORMFO_NOFISHING"  ,	"#0866f8"],
	["NORMFO_OTHER"      ,  "#9d9d9d"]
]);

const styleCache = {};

const style = vessel => {
	const baseSymbolizer = createIALASymbolizer(vessel),
		{rotation, height} = baseSymbolizer,
		border = [],
		vesselSpecificInformation = vessel.get("sai");
	let fill = baseSymbolizer.fill;

	///Stroke Color based on RMFO Status
	if (vesselSpecificInformation) {
		if (vesselSpecificInformation["ICCAT CODE"] && (vesselSpecificInformation["ICCAT CODE"] === "Y"))
			border.push(RMFOLIST_COLORS.get("ICCAT"));

		if (vesselSpecificInformation["NAFO CODE"] && vesselSpecificInformation["NAFO CODE"] === "Y")
			border.push(RMFOLIST_COLORS.get("NAFO"));

		if (vesselSpecificInformation["NEAFC CODE"] && vesselSpecificInformation["NEAFC CODE"] === "Y")
			border.push(RMFOLIST_COLORS.get("NEAFC"));

		if (vesselSpecificInformation["EU CODE"] && vesselSpecificInformation["EU CODE"] === "Y")
			border.push(RMFOLIST_COLORS.get("EU"));
	}

	const inRFMO = border.lenght?true:false;

	//Is in more than one list
	if (border.length>1){
		border[0] = RMFOLIST_COLORS.get("VARIOUS");
	}else if (!border.length){
		//Is not in any list
		border.push(RMFOLIST_COLORS.get("OTHER"));
	}

	//Fill based on Vessel Type
	if (isFishingVessel(vessel))
		if (isEngagedInFishing(vessel))
			if (inRFMO)
				if (isPermitAvailable(vessel))
					fill = (hasValidPermit(vessel)) ? RMFOSTATUS_COLORS.get("RMFO_VALIDPERMIT") : RMFOSTATUS_COLORS.get("RMFO_INVALIDPERMIT");
				else
					fill = RMFOSTATUS_COLORS.get("RMFO_NOPERMIT");
			else
				fill = RMFOSTATUS_COLORS.get("NORMFO_FISHING");
		else
			fill = inRFMO ? RMFOSTATUS_COLORS.get("RMFO_NOFISHING") : RMFOSTATUS_COLORS.get("NORMFO_NOFISHING");
	else
		fill = RMFOSTATUS_COLORS.get("NORMFO_OTHER");

	//RiskStatus
	const cacheKey = JSON.stringify({
		fill, border, rotation, height
	});

	if(!styleCache[cacheKey]) {
		baseSymbolizer.fill = fill;
		baseSymbolizer.stroke = border[0];

		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG(baseSymbolizer)
		});
	}

	return styleCache[cacheKey];
};

module.exports = {
	name: "RFMO Status",
	style
};
