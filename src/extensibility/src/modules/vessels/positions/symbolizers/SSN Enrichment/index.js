const {createIALASymbolizer} = require("../IALA/index.js");

const styleCache = {},
	hazmatIconCache = {},
	bannedIconCache = {},
	securityLevelIconCache = {},
	wasteIconCache = {},
	exemptionsIconCache = {};

const style = vessel => {
	const vesselEnrichment = vessel.get("enrichment"),
		styles = [],
		baseSymbolizer = createIALASymbolizer(vessel),
		{rotation, height} = baseSymbolizer,
		hasHeading = !(typeof rotation === "undefined" || rotation === 0),
		// INCIDENT: FILL RED
		fill = vesselEnrichment && vesselEnrichment.activeIncidents === "Y"?"#F00":"#000",
		stroke = {
			// SHT: STROKE DARKER RED
			color: vesselEnrichment && vesselEnrichment.SHT?"#A90000":"#000",
			width: vesselEnrichment && vesselEnrichment.SHT?2:1
		};

	let cacheKey = JSON.stringify({fill, rotation, height, stroke});

	if(!styleCache[cacheKey]) {
		Object.assign(baseSymbolizer, {fill, stroke});

		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG(baseSymbolizer)
		});
	}

	styles.push(styleCache[cacheKey]);

	if(vesselEnrichment) {
		// BANNED: append banned.svg
		if (vesselEnrichment.bannedYorN === "Y") {
			cacheKey = JSON.stringify({rotation});

			if(!bannedIconCache[cacheKey]){
				const image = new seg.style.SVG({
					svg: hasHeading ? require("./banned.svg") : require("./banned-nohdg.svg"),
					height,
					rotation
				});

				bannedIconCache[cacheKey] = new ol.style.Style({
					image
				});
			}

			styles.push(bannedIconCache[cacheKey]);
		}

		// HAZMAT: append hazmat.svg
		if(vesselEnrichment.HazmatOnBoardYorN === "Y") {
			cacheKey = JSON.stringify({rotation});

			if(!hazmatIconCache[cacheKey]){
				const image = new seg.style.SVG({
					svg: hasHeading ? require("./hazmat.svg") : require("./hazmat-nohdg.svg"),
					height,
					rotation
				});

				hazmatIconCache[cacheKey] = new ol.style.Style({
					image
				});
			}

			styles.push(hazmatIconCache[cacheKey]);
		}

		// EXEMPTIONS: append exemptions.svg
		if(vesselEnrichment.exemptionYorN === "Y" || vessel.get("exemptions")) {
			cacheKey = JSON.stringify({rotation});

			if(!exemptionsIconCache[cacheKey]){
				const image = new seg.style.SVG({
					svg: hasHeading ? require("./exemptions.svg") : require("./exemptions-nohdg.svg"),
					height,
					rotation
				});

				exemptionsIconCache[cacheKey] = new ol.style.Style({
					image
				});
			}

			styles.push(exemptionsIconCache[cacheKey]);
		}

		// SECURITY: append security.svg
		if(vesselEnrichment.CurrentSecurityLevel && vesselEnrichment.CurrentSecurityLevel !== "SL1") {
			cacheKey = JSON.stringify({rotation});

			if(!securityLevelIconCache[cacheKey]){
				const image = new seg.style.SVG({
					svg: hasHeading ? require("./security.svg") : require("./security-nohdg.svg"),
					height,
					rotation
				});

				securityLevelIconCache[cacheKey] = new ol.style.Style({
					image
				});
			}

			styles.push(securityLevelIconCache[cacheKey]);
		}

		// WASTE: append waste.svg
		if (vesselEnrichment.WasteDeliveryStatus === "None") {
			cacheKey = JSON.stringify({rotation});

			if(!wasteIconCache[cacheKey]){
				const image = new seg.style.SVG({
					svg: hasHeading ? require("./waste.svg") : require("./waste-nohdg.svg"),
					height,
					rotation
				});

				wasteIconCache[cacheKey] = new ol.style.Style({
					image
				});
			}

			styles.push(wasteIconCache[cacheKey]);
		}
	}

	return styles;
};

module.exports = {
	name: "Voyage Information",
	style
};
