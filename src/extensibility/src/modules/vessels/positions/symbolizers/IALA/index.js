const IALA = require("./iala.svg"),
	IALA_NO_HDG = require("./iala-nohdg.svg");

const styleCache = {};

const getSize = hasHeading => {
	const max = hasHeading?40:30;

	return max;
};

const style = vessel => {
	const baseSymbolizer = createIALASymbolizer(vessel),
		{rotation, height, opacity} = baseSymbolizer;
	let fill = baseSymbolizer.fill;

	if(typeof vessel.get("src") !== "undefined") {
		if(vessel.get("src") === "LRIT")
			fill = "#666";
		else if(vessel.get("src") === "VMS")
			fill = "#c98c01";
		else if(vessel.get("src") === "MRS")
			fill = "#795548";
	}

	const cacheKey = JSON.stringify({fill, rotation, height, opacity});

	if(!styleCache[cacheKey]) {
		baseSymbolizer.fill = fill;

		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG(baseSymbolizer)
		});
	}

	return styleCache[cacheKey];
};

const createIALASymbolizer = vessel => {
	const rotation = ((vessel.get("hdg") === undefined) || Number.isNaN(vessel.get("hdg"))) ? undefined : (vessel.get("hdg") * Math.PI / 180),
		hasHeading = !(typeof rotation === "undefined" || rotation === 0),
		height = getSize(hasHeading),
		fill = "#000";

	let opacity = 1;

	if(moment().diff(vessel.get("ts"), "m") >= 1440)
		opacity = 0.5;

	return {
		svg: hasHeading?IALA:IALA_NO_HDG,
		fill,
		stroke: {
			color: "#fff",
			width: 1
		},
		height,
		rotation,
		opacity
	};
};

module.exports = {
	name: "IALA",
	style,
	createIALASymbolizer
};
