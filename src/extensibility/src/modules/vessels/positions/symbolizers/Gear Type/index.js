const {createIALASymbolizer} = require("../IALA/index.js"),
	{GEAR_TYPES} = require("../../filters.enum.js"),
	GEAR_COLOR_REFERENCE = {
		"Surrounding nets": "#fc8827",
		"Seine nets": "#fb0013",
		"Bottom trawls": "#16771d",
		"Midwater trawls": "#3bc741",
		"Dredges": "#0866f8",
		"Gill nets": "#fb1ac1",
		"Traps and pots": "#915434",
		"Hooks and lines": "#8d37f7",
		"Lift Nets":"#e6b9b8",
		"Other": "#9d9d9d"
	},
	gearColor = new Map(),
	getGearColor = vessel => {
		if (vessel.get("sai") && vessel.get("sai")["Gear"])
			return gearColor.get(vessel.get("sai")["Gear"]);
		return "#000";
	},
	styleCache = {},
	style = vessel => {
		const baseSymbolizer = createIALASymbolizer(vessel),
			{rotation, height} = baseSymbolizer,
			fill = getGearColor(vessel),
			cacheKey = JSON.stringify({fill, rotation, height});

		if(!styleCache[cacheKey]) {
			baseSymbolizer.fill = fill;

			styleCache[cacheKey] = new ol.style.Style({
				image: new seg.style.SVG(baseSymbolizer)
			});
		}

		return styleCache[cacheKey];
	};

Object.entries(GEAR_TYPES).map(gearRef => gearRef[1].map(typeCode => gearColor.set(typeCode, GEAR_COLOR_REFERENCE[gearRef[0]])));
module.exports = {
	name: "Gear Type",
	style
};

