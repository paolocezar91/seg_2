const {createIALASymbolizer} = require("../IALA/index.js");

const styleCache = {};

const style = vessel => {
	const baseSymbolizer = createIALASymbolizer(vessel),
		{fill, rotation, height} = baseSymbolizer,
		timeDifference = moment().diff(vessel.get("ts"), "m");
	let opacity = 1;

	if(timeDifference >= 170)
		opacity = 0.25;
	else if(timeDifference >= 60)
		opacity = 0.50;
	else if(timeDifference >= 10)
		opacity = 0.75;

	const cacheKey = JSON.stringify({fill, rotation, height, opacity});

	if(!styleCache[cacheKey]) {
		Object.assign(baseSymbolizer, {fill, opacity});

		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG(baseSymbolizer)
		});
	}

	return [styleCache[cacheKey]];
};

module.exports = {
	name: "Target Age Opacity",
	style
};
