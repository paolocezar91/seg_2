const {createIALASymbolizer} = require("../IALA/index.js");

const styleCache = {};

const style = vessel => {
	const baseSymbolizer = createIALASymbolizer(vessel),
		{rotation, height} = baseSymbolizer,
		timeDifference = moment().diff(vessel.get("ts"), "m");
	let fill = baseSymbolizer.fill;

	if(timeDifference >= 170)
		fill = "#F00";//red
	else if(timeDifference >= 60)
		fill = "#FFA500";//orange
	else if(timeDifference >= 10)
		fill = "#FF0";//yellow
	else fill = "#0F0";//green

	const cacheKey = JSON.stringify({fill, rotation, height});

	if(!styleCache[cacheKey]) {
		baseSymbolizer.fill = fill;

		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG(baseSymbolizer)
		});
	}

	return styleCache[cacheKey];
};

module.exports = {
	name: "Target Age Color",
	style
};

