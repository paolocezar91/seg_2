const targetAgeSymbolizers = [
	require("./color.js"),
	require("./opacity.js")
];

targetAgeSymbolizers.name = "Target Age";

module.exports = targetAgeSymbolizers;