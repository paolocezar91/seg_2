const {createIALASymbolizer} = require("../IALA/index.js");

const styleCache = {};

const style = vessel => {
	const baseSymbolizer = createIALASymbolizer(vessel),
		{rotation, height} = baseSymbolizer,
		fill = getFill(vessel);

	const cacheKey = JSON.stringify({fill, rotation, height});

	if(!styleCache[cacheKey]) {
		baseSymbolizer.fill = fill;

		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG(baseSymbolizer)
		});
	}

	return styleCache[cacheKey];
};

const getColor = type => {
	// Passenger vessels: BLUE
	if (type === 370 || type === 371 || type === 383 || (type >= 60 && type <= 69))
		return "#0000FF";
	// Cargo vessels: GREEN
	if (type === 355 || type === 360 || type === 361 || type === 384 || (type >= 70 && type <= 79))
		return "#00FF00";
	// Tanker vessels: RED
	if (type === 310 || type === 311 || type === 313 || type === 314 || type === 330 || (type >= 80 && type <= 89))
		return "#FF0000";
	// Tug vessel : LIGHT BLUE
	if (type === 385 || type === 52)
		return "#00ffff";
	// Pleasure vessels : PURPLE
	if (type === 319 || type === 37)
		return "#990099";
	// Fishing vessels: ORANGE
	if (type === 315 || type === 30)
		return "#FF5721";

	return "#808080";
};

const getFill = vessel => getColor(vessel.get("shipType"));

module.exports = {
	name: "Vessel Type",
	style
};
