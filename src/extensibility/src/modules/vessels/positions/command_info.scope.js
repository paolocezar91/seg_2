const {getLabel, toggleFollow} = require("../api"),
	specificInfo = require("./project_specific_info"),
	{mapProperties} = require("./properties"),
	{getLastKnownPosition, mergeDuplicates, shouldUpdatePosition, getShipType} = require("../api"),
	{mergeDuplicatesAndAddToSource, alertTTTFields} = require("../../alerts/api"),
	{GT_CLASS, LOA_CLASS, PWR_CLASS} = require("./filters.enum");

module.exports = Object.assign(require("./ssn_functions"), {
	async getDetails (vessel) {
		if(
			seg.favourites.isFavourite(vessel) ||
			shouldUpdatePosition(vessel)
		) {
			vessel.unset("ts");

			const modal = seg.modal.openModal("Last position", "Searching last known position of " + getLabel(vessel) + "...");

			let vesselCurrentPosition = await getLastKnownPosition({
				imo: vessel.get("imo") ? vessel.get("imo") : undefined,
				mmsi: vessel.get("mmsi") ? vessel.get("mmsi") : undefined,
				imdateId: vessel.get("vid") ? vessel.get("vid") : undefined,
				id: vessel.getId() ? vessel.getId() : undefined
			});
			// If any position is found, merge it with duplicates, select and center it
			if(vesselCurrentPosition) {
				if (Array.isArray(vesselCurrentPosition))
					vesselCurrentPosition = vesselCurrentPosition[0];

				vesselCurrentPosition.setId(vessel.getId());

				vesselCurrentPosition.set("lastPositionSearched", true);
				vesselCurrentPosition = (await mergeDuplicates([vesselCurrentPosition]))[0];
				seg.layer.get("positions").getSource().addFeature(vesselCurrentPosition);
				seg.map.centerOn(vesselCurrentPosition);

				modal.setContent("Last position found");
				seg.modal.closeTimeout(modal.title);
			} else {
				vessel.set("lastPositionSearched", true);
				modal.setContent("Last position for " + getLabel(vessel) + " is unknown");
				seg.modal.closeTimeout(modal.title);
			}
		}

		const mmsi = vessel.get("mmsi"),
			imo = vessel.get("imo");

		if (!vessel.get("photos") && (mmsi || imo)) {
			vessel.set("photos", []);

			let data;

			if(imo)
				data = await seg.request.getDataURL("v1/ship/particulars/image?imo=" + imo);

			if (!data && mmsi)
				data = await seg.request.getDataURL("v1/ship/particulars/image?mmsi=" + mmsi);

			if (data !== "data:" && !data.endsWith("base64,"))
				vessel.get("photos").push(data);

			seg.request.get("v1/file/fileMetadata", {
				params: {
					path: "/vessels/images/" + vessel.getId()
				}
			}).then(res => {
				res.result.features.forEach(feature => {
					seg.request.getDataURL("v1/file/download", {
						params: {
							path: feature.properties.path
						}
					}).then(data => {
						if (!vessel.get("photos"))
							vessel.set("photos", []);
						if (data !== "data:")
							vessel.get("photos").push(data);
					}, e => seg.log.error("ERROR_VESSEL_GET_PHOTOS_1", e.error + ": " + e.result));
				});
			}, e => seg.log.error("ERROR_VESSEL_GET_PHOTOS_2", e.error + ": " + e.result));
		}

		if (!vessel.get("GA"))
			seg.request.get("v1/file/fileMetadata", {
				params: {
					path: "/vessels/ga/" + vessel.getId()
				}
			}).then(res => {
				if (res.result.features.length)
					vessel.set("GA", res.result.features[0].properties.path);
			}, e => seg.log.error("ERROR_VESSELS_GET_GA", e.error + ": " + e.result));

		if ((!vessel.get("enrichmentData") || !vessel.get("voyage")) && vessel.get("imo")) {
			seg.request.get("v1/ship/enrichment_cache/" + vessel.get("imo")).then(({result}) => {
				if (!result)
					return;

				const {VoyageInformation, MRSInformation, IncidentIdentification, ActiveExemptions} = result;

				if (VoyageInformation){
					const start = VoyageInformation.ATDPortOfCall,  // ||  VoyageInformation.ETDFromPortOfCall,
						end = VoyageInformation.ETAToNextPort; // || VoyageInformation.ETAToPortOfCall;

					const voyage = {
						origin: VoyageInformation.LastPort ? VoyageInformation.LastPort.LoCode : "N/A",
						destination: VoyageInformation.PortOfCall ? VoyageInformation.PortOfCall.LoCode : "N/A",
						start: start ? moment(start) : null ,
						end: end ? moment(end) : null,
						pob: VoyageInformation.POBVoyageTowardsPortOfCall || "N/A",
						ATAPortOfCall: VoyageInformation.ATAPortOfCall,
						ETAToPortOfCall: VoyageInformation.ETAToPortOfCall,
						ETAToNextPort: VoyageInformation.ETAToNextPort,
						ATDPortOfCall: VoyageInformation.ATDPortOfCall,
						ETDFromPortOfCall: VoyageInformation.ETDFromPortOfCall
					};

					if (voyage.start && voyage.end){
						const now = moment();
						voyage.completed = ((now - voyage.start) / (voyage.end - voyage.start)) * 100;
					}

					vessel.setProperties({
						"voyage": voyage,
						"security": (VoyageInformation) ? VoyageInformation.SecurityConfirmation : null
					});

					const shipCallId = (VoyageInformation) ? VoyageInformation.ShipCallID : null;
					if (shipCallId) {
						vessel.set("shipCallId", shipCallId);

						seg.request.get("v1/ship/ship_call/", {
							params: {
								details: "SelectedShipCall",
								ship_call_id: shipCallId
							}
						}).then((res) => {
							const {status} = res;
							if (!(status === "success" || status === "OK")) {
								seg.log.error("ERROR_VESSEL_GET_ENRICHMENT_CACHE_1", res.status + ": " + res.message);
								// const modal = seg.modal.openModal("get enrichment_cache", "<span class=\"retrieved-no-result\"><b>" + status + ": " + message + "</b></span>");
								// seg.modal.closeTimeout(modal.title);
							} else {
								let {result} = res;

								if (result.hasOwnProperty("Body"))
									result = result.Body;

								if (!result)
									return;

								if (result.QueryResults) {
									const {QueryResults} = result,
										{
											WasteConfirmation,
											HazmatConfirmation,
											SecurityConfirmation,
											// All below are voyage infos
											VoyageInformation,
											PreArrival24HoursNotificationDetails,
											PreArrival3DaysNotificationDetails,
											VesselDetails,
											Source
										} = (Array.isArray(QueryResults.PortPlusNotificationList) ? QueryResults.PortPlusNotificationList[0] : QueryResults.PortPlusNotificationList.WasteConfirmation);

									vessel.setProperties({
										"enrichmentData": true,
										"waste": WasteConfirmation,
										"hazmat": HazmatConfirmation,
										"security": SecurityConfirmation,
										"vi": Object.assign(VoyageInformation, PreArrival24HoursNotificationDetails, PreArrival3DaysNotificationDetails, VesselDetails, Source)
									});
								}
							}

						}, e => seg.log.error("ERROR_VESSEL_GET_ENRICHMENT_CACHE_2", e.status + ": " + e.result));
					}
				}

				if (MRSInformation){
					if (MRSInformation.MRSVoyageInformation) {
						const mrsInformation = MRSInformation.MRSVoyageInformation;
						// Coordinates 1/10000 Minutes transformation of coordinates
						mrsInformation.Longitude = (mrsInformation.Longitude/10000)/60;
						mrsInformation.Latitude = (mrsInformation.Latitude/10000)/60;
						vessel.set("mrs", mrsInformation);
					}
				}

				if (IncidentIdentification)
					vessel.set("incidents", IncidentIdentification);

				if (ActiveExemptions)
					vessel.set("exemptions", ActiveExemptions);
			}, e => seg.log.error("ERROR_VESSEL_GET_ENRICHMENT_CACHE_3", e.error + ": " + e.result));
		}

		if(!vessel.get("csdb"))
			this.getCSBD(vessel);

		if(vessel.get("pid") && !vessel.get("getTAISDetails")){
			this.getTAISDetails(vessel);
			vessel.set("getTAISDetails", true);
		}
	},
	getCSBD: (vessel) => {
		const imoNumber = vessel.get("imo"),
			mmsiNumber = vessel.get("mmsi");
		let params;

		if(!imoNumber && !mmsiNumber)
			return;

		if(imoNumber) params = {imoNumber};
		else if(mmsiNumber) params = {mmsiNumber};

		seg.request.get("v1/ship/central_ship_db?", {
			params
		}).then(({result}) => {
			if (!result)
				return;

			vessel.set("csdb", result.QueryResults.ShipParticular_LISTitem.VesselIdentification);
		}, e => seg.log.error("ERROR_VESSEL_GET_CSDB", e.error + ": " + e.result));
	},
	uploadPhoto: (file, vessel) => {
		const modal = seg.modal.openModal(file.name, "Uploading image " + file.name + " for " + getLabel(vessel) + "...");
		if (file && vessel) seg.request.upload({
			file: file,
			pathToSave: "/vessels/images/" + vessel.getId() + "/" + file.name
		}).then(() => {
			modal.setContent("Upload successful");
			seg.modal.closeTimeout(modal.title);
			const fr = new FileReader();
			fr.onloadend = function (e) {
				if (e.target.result) {
					if (!vessel.get("photos")) vessel.set("photos", []);
					vessel.get("photos").push(e.target.result);
				}
			};
			fr.readAsDataURL(file);
		}, e => seg.log.error("ERROR_VESSEL_UPLOAD_PHOTO", e.error + ": " + e.result));
	},
	openFavourites: () => seg.favourites.open(),
	uploadGAPlan: (file, vessel) => {
		const modal = seg.modal.openModal("GA Plan", "Uploading GA Plan...");
		if (file && vessel)
			seg.request.upload({
				file,
				pathToSave: "/vessels/ga/" + vessel.getId() + "/" + file.name
			}).then(res => {
				modal.setContent("Upload successful");
				seg.modal.closeTimeout(modal.title);
				vessel.set("GA", res.result.features[0].properties.path);
			}, e => seg.log.error("ERROR_VESSEL_UPLOAD_GA_PLAN", e.error + ": " + e.result));
	},
	getShipType,
	downloadGAPlan: (vessel) => {
		const GA = vessel.get("GA");

		if(GA)
			seg.popup.openPopup({
				title: "Download GA Plan",
				template: `
					<include template-url={{disclaimerURL}}></include>
					<p>Download GA Plan?</p>
					<div class="flexbox" style="margin-top:15px">
						<button title="Download" class="seg positive rounded md" ng-click="download()" style="margin-right:5px;">Yes</button>
						<button title="Cancel Download"class="seg positive rounded md" ng-click="close()">No</button>
					</div>`,
				bindings: {
					disclaimerURL: resolvePath("./templates/ga_download_disclaimer.html"),
					GA,
					close() {
						seg.popup.close("Download GA Plan");
					},
					download() {
						const modal = seg.modal.openModal("GA Plan", "Downloading GA Plan for " + getLabel(vessel));
						seg.request.download("v1/file/download", {
							params: {
								path: vessel.get("GA")
							}
						}, vessel.getId() + "_GA_Plan.pdf").then(() => {
							modal.setContent("Download successful");
							seg.modal.closeTimeout("GA Plan");
							seg.popup.close("Download GA Plan");
						}, (e) => {
							seg.log.error("ERROR_VESSEL_DOWNLOAD_GA_PLAN", e.error + ": " + e.result);
							modal.setContent("Download failed");
							seg.modal.closeTimeout("GA Plan");
							seg.popup.close("Download GA Plan");
						});
					}
				}
			});
	},
	alertsForShip(vessel) {
		// v1/abm/am/alertsForShip
		// •	startTime – string – required
		// •	endTime – string – required
		// •	vesselId – long – required
		// •	surveillanceInstanceName – string
		// •	maxRecords – integer

		// startTime and endTime use last 24h
		// Using values above, you need to create request to get the alert associated with vessel,
		// create a feature for the alert if it doesn't exist, and select it
		seg.request.get("v1/abm/am/alertsForShip", {
			params: {
				startTime: moment().add(-24,"hours").toISOString(),
				endTime: moment().toISOString(),
				vesselId: vessel.getId()
				//surveillanceInstanceName: null, //get all alert types
			}
		}).then((res) => {
			if (!res.result)
				return;

			const data = mergeDuplicatesAndAddToSource(res.result);

			seg.ui.ttt.tables.open({
				label: (data.length === 1) ? "Vessel Alert" : "All Vessel Alerts",
				data: data,
				itemAs: "alert",
				fields: alertTTTFields,
				onSelectionChange(changedRows, selectedRows) {
					seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
				}

			});

		});
	},
	goToEquasis(vessel) {
		if (!vessel.get("imo"))
			return;

		seg.request.get("v1/equasis?", {
			params: {
				username: "ssn", //XXX
				password: "123ssn",
				imo: vessel.get("imo")
			}
		}).then((res) => {
			if (!res.result)
				return;

			const credentials = res.result;

			// Create new form to submit post to another page/tab
			const form = document.createElement("form");
			form.action = "http://www.equasis.org/EquasisWeb/restricted/ShipInfo"; //XXX martelated
			form.method = "POST";
			form.target = "_blank";

			const p1 = document.createElement("textarea");
			p1.name = "P1";
			p1.value = vessel.get("imo");
			form.appendChild(p1);

			const p2 = document.createElement("textarea");
			p2.name = "P2";
			p2.value = credentials.username;
			form.appendChild(p2);

			const p3 = document.createElement("textarea");
			p3.name = "P3";
			p3.value = credentials.password;
			form.appendChild(p3);

			const p4 = document.createElement("textarea");
			p4.name = "P4";
			p4.value = "010"; //XXX martelated
			form.appendChild(p4);

			const p5 = document.createElement("textarea");
			p5.name = "P5";
			p5.value = "0210"; //XXX martelated
			form.appendChild(p5);

			form.style.display = "none";
			document.body.appendChild(form);
			form.submit();
		}, e => seg.log.error("ERROR_VESSEL_GO_TO_EQUASIS", e.error + ": " + e.result));
	},
	getIconForValidationFlag: vessel => {
		if (vessel.get("vf") === "V") return "check";
		if (vessel.get("vf") === "U") return "info";
		return "circle-minus";
	},
	setRisk: (vessel, riskLevel) => {
		if (riskLevel === "None") vessel.unset("risk");
		else vessel.set("risk", riskLevel);
	},
	centerMap: vessel => seg.selection.selectFeatureAndCenter(vessel),
	toggleFollow,
	isFollowed: vessel => {
		const followed = seg.preferences.session.get("following") ? seg.preferences.session.get("following").feature : false;
		return followed && Number(followed.getId()) === Number(vessel.getId());
	},
	downloadISP: vessel => {
		const loader = seg.modal.openModal("ISP Download", "Retrieving ISP File for " + getLabel(vessel) + "...");

		seg.request.download("v1/ship/ispd", {
			params: {
				emsaId: vessel.getId()
			}
		}, vessel.getId() + "_ISPD.pdf").then(() => {
			loader.setContent("Download successful");
			seg.modal.closeTimeout("ISP Download");
		}, e => {
			loader.setContent("Download failed");
			seg.modal.closeTimeout("ISP Download");
			seg.log.error("ERROR_VESSELS_DOWNLOAD_ISP", e.error + ": " + e.result);
		});
	},
	formatCSDBValue: (vessel, flag) => {
		if ((flag === "mmsi") && (vessel.get("mmsi") === vessel.get("csbd").Current_MMSINumber))
			return "<span style='color:red'>{{:: vessel.get('mmsi') || vessel.get('csdb').Current_MMSINumber || 'N/A'}}</span>";
	},
	compareFlagRegisters: (vessel) => ((vessel.get("flagState")) && (vessel.get("csdb") && vessel.get("csdb").Current_FlagRegistry && (vessel.get("csdb").Current_FlagRegistry.content) && (vessel.get("flagState").toString() !== vessel.get("csdb").Current_FlagRegistry.content.toString()))),
	compareCallSignRegisters: (vessel) => {
		let current_callsign = "";

		if (vessel.get("csdb") && vessel.get("csdb").Current_CallSign) {
			if (vessel.get("csdb").Current_CallSign.content) {
				current_callsign = vessel.get("csdb").Current_CallSign.content;
			} else {
				current_callsign = vessel.get("csdb").Current_CallSign;
			}
		} else {
			return false;
		}

		return vessel.get("callSign") && vessel.get("callSign").toString() !== current_callsign.toString();
	},
	compareMMSIRegisters: (vessel) => ((vessel.get("mmsi")) && (vessel.get("csdb") && vessel.get("csdb").Current_MMSINumber && (vessel.get("csdb").Current_MMSINumber.content) && (vessel.get("mmsi").toString() !== vessel.get("csdb").Current_MMSINumber.content.toString()))),
	compareIMORegisters: (vessel) => ((vessel.get("imo")) && (vessel.get("csdb") && vessel.get("csdb").IMONumber && (vessel.get("csdb").IMONumber.content) && (vessel.get("imo").toString() !== vessel.get("csdb").IMONumber.content.toString()))),
	compareSrc: (vessel) => (vessel.get("src").toLowerCase() === "T-AIS".toLowerCase()),
	displayPreferences_: mapProperties({
		override:
			{
				mmsi: {
					template: `<span ng-style="{'color':vesselCommandInfo.compareMMSIRegisters(vessel)?'red':''}">
						<span style="float:left">{{vessel.get("mmsi") || vessel.get("csdb").Current_MMSINumber.content || "N/A"}}</span>
						<span ng-if="vessel.get('csdb').Current_MMSINumber.content && !vessel.get('mmsi')">&nbsp;<small>(CSD)</small></span>
						<button style="float:left; margin-left:2px;" ng-if="vesselCommandInfo.compareMMSIRegisters(vessel)" title="{{vessel.get('csdb').Current_MMSINumber.content}}" class="seg primary round xs inline-block"><i icon="info"></i></button>
					</span>`,
					order: 0
				},
				imo: {
					template: `<span ng-style="{'color':vesselCommandInfo.compareIMORegisters(vessel)?'red':''}">
						<span style="float:left">{{vessel.get("imo") || vessel.get("csdb").IMONumber.content || "N/A"}}</span>
						<button style="float:left" ng-if="vesselCommandInfo.compareIMORegisters(vessel)" title="{{vessel.get('csdb').IMONumber.content}}" class="seg primary round xs inline-block"><i icon="info"></i></button>
						<span ng-if="vessel.get('csdb').IMONumber.content && !vessel.get('imo')">&nbsp;<small>(CSD)</small></span>
					</span>`,
					order: 1
				},
				src: {
					label: "Source",
					template: "<span>{{vessel.get('src') || 'N/A'}}</span>",
					order: 2
				},
				callSign: {
					template: `<span ng-style="{'color':vesselCommandInfo.compareCallSignRegisters(vessel)?'red':''}">
						<span style="float:left;">{{vessel.get("callSign") || (vessel.get("csdb").Current_CallSign && vessel.get("csdb").Current_CallSign.content) || vessel.get("csdb").Current_CallSign || "N/A"}}</span>
						<button style="float:left; margin-left:2px;" ng-if="vesselCommandInfo.compareCallSignRegisters(vessel)" title="{{ ((vessel.get('csdb').Current_CallSign && vessel.get('csdb').Current_CallSign.content) || vessel.get('csdb').Current_CallSign) }}" class="seg primary round xs inline-block"><i icon="info"></i></button>
						<span ng-if="vessel.get('csdb').Current_CallSign && !vessel.get('callSign')">&nbsp;<small>(CSD)</small></span>
					</span>`,
					order: 3
				},
				ir: {
					visible: true,
					order: 4
				},
				hdg: {
					visible: true,
					order: 5
				},
				lat: {
					visible: true,
					order: 6
				},
				lon: {
					visible: true,
					order: 7
				},
				speed: {
					order: 8
				},
				flagState: {
					template: `<span ng-style="::{'color': vesselCommandInfo.compareFlagRegisters(vessel) ?'red':''}">
						<span style="float:left">{{::(vessel.get("flagState") | country) || (vessel.get("csdb").Current_FlagRegistry.content | country) || "N/A"}}</span>
						<button style="float:left; margin-left:2px;" ng-if="vesselCommandInfo.compareFlagRegisters(vessel) " title="{{vessel.get('csdb').Current_FlagRegistry.content}}" class="seg primary round xs inline-block"><i icon="info"></i></button>
						<span ng-if="::vessel.get('csdb').Current_FlagRegistry.content && !vessel.get('flagState')">&nbsp;<small>(CSD)</small></span>
					</span>`,
					visible: true,
					order: 9
				},
				ns: {
					template: "<span>{{::vesselCommandInfo.getNavigationalStatus(vessel.get('ns'))}}</span>",
					visible: true,
					order: 10
				},
				thetisDescription: {
					visible: true,
					order: 11
				},
				th: {
					visible: true,
					order: 12
				},
				originator: {
					visible: true,
					order: 13
				},
				dimensions_l: {
					template: "<span>{{vesselCommandInfo.getDimensions(vessel, 'length')}}</span>",
					visible: true,
					order: 14
				},
				dimensions_b: {
					template: "<span>{{vesselCommandInfo.getDimensions(vessel, 'breadth')}}</span>",
					visible: true,
					order: 15
				},
				voyage_origin: false,
				voyage_destination: false,
				voyage_ETAToPortOfCallToPortOfCallToPortOfCall: false,
				voyage_ETAToNextPort: false,
				voyage_pob: false
			}
	}),
	get displayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("Vessels.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach(key => {
			if (self.displayPreferences_[key])
				self.displayPreferences_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayPreferences_;
	},
	toggleDisplayPreference(key) {
		this.displayPreferences[key].visible = !this.displayPreferences[key].visible;
		//we only want to save the "visible" property, the rest is always the same
		seg.preferences.workspace.set("Vessels.commandInfo.displayOptions." + key, this.displayPreferences[key].visible);
	},
	openISPQueryPanel: () => seg.ui.queryPanel.openTab("Get profile"),
	getISP() {
		const isp = require("../isp/index.js").scope();
		for (const option in isp.options) isp.options[option] = true;
		isp.submit();
	},
	lritEnabled: vessel => !vessel.get("imo"),
	openLRIT: vessel => {
		seg.ui.queryPanel.getTab("LRIT").scope.lrit.services.vesselIMO = vessel.get("imo");
		seg.ui.queryPanel.openTab("LRIT");
	},
	specific() {
		if (seg.operation)
			return specificInfo[seg.operation.project];
	},
	getPWRClass: (val) => {
		return Object.entries(PWR_CLASS).find(([key, value]) => value === Number(val))[0];
	},
	getGTClass: (val) => {
		return Object.entries(GT_CLASS).find(([key, value]) => value === Number(val))[0];
	},
	getLOAClass: (val) => {
		return Object.entries(LOA_CLASS).find(([key, value]) => value === Number(val))[0];
	},
	getTAISDetails: vessel => {
		seg.request.get("v1/ship/positionDetail",{
			params: {
				id: vessel.get("pid")
			}
		}).then((res) => {
			if (Object.keys(res.result).length)
				vessel.setProperties(
					Object.assign({}, vessel.getProperties(), {
						imo: res.result.declaredIdentity.imo,
						shipName: res.result.declaredIdentity.shipName,
						mmsi: res.result.declaredIdentity.mmsi
					})
				);
		}, e => seg.log.error("ERROR_VESSEL_GET_TAIS_DETAILS", e.error + ": " + e.result));
	}
});
