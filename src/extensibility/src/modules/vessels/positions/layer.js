const {getLabel, getTracks, getPSCInspections, getNavigationalStatus, getDimensions, mergeDuplicates, getParticulars, openQueryPanel} = require("../api.js"),
	{wasteTypeValues} = require("./ssn_functions");

const {
	getProperty,
	getTooltipDisplayProperties
} = require("./properties.js");

// const { buildFilters } = require("./filters.js");
const buildPositionsLayer = config => ({
	id: "positions",
	name: "Positions",
	type: "Vector",
	renderMode: "vector",
	style: require("./style.js"),
	label: {
		featureAs: "vessel",
		bindings: {
			getVisibleProperties: function () {
				return seg.preferences.workspace.get("Vessels.label.properties");
			},
			showLabel: function () {
				if (seg.map.isFilterEnabled("SELECT_FILTER") && !this.vessel.get("wasSelected"))
					return false;

					return Object.values(this.getVisibleProperties()).some(val => val);
			},
			getProperty
		},
		templateUrl: resolvePath("./templates/label.html"),
		scalePreference: "Vessels.label.scale",
		visiblePreference: "Vessels.label.visible"
	},
	timerange: {
		timestampProperty: "ts",
		exceptionCheck: (feature) => (
			feature.get("isSURPICResult") ||
			feature.get("isSearchResult") ||
			feature.get("isImported") ||
			feature.get("favourite_name") && feature.get("group_name")
		)
	},
	minScale: seg.CONFIG.getValue("vessels.positions.MINSCALE") || 8000000, // CLUSTERS_REGRESSION XXX this is not available at this point.
	source: {
		name: "Positions",
		type: "Vector",
		projection: "EPSG:4326",
		renderBuffer: 20
	},
	tooltip: {
		featureAs: "vessel",
		bindings: {
			getParticulars,
			getLabel,
			getTooltipDisplayProperties,
			getNavigationalStatus,
			getDimensions,
			getTrack(vessel) {
				const start = moment().subtract(1, "d"),
					end = moment();

				return getTracks({
					from: start,
					to: end,
					vessels: vessel
				}).then(trackTimeline => {
/*					if (trackTimeline && (trackTimeline.items.length > 0))
						seg.timeline.set({
							delimiters: [
								start,
								end
							],
							groups: [trackTimeline]
						}), e => seg.log.error("ERROR_VESSELS_COMMAND_INFO_GET_TRACK", e.error + ": " + e.result);
					else
						seg.map.disableFilter("ACQ_FILTER");
*/				});
			},
		},
		templateUrl: resolvePath("./templates/tooltip.html")
	},
	commandInfo: {
		//alias to access the selected Vessel
		featureAs: "vessel",
		bindings: {
			vesselCommandInfo: Object.assign(require("./command_info.scope.js"), {
				getLabel,
				openQueryPanel,
				getTrack(vessel) {
					const start = moment().subtract(1, "d"),
						end = moment();

					return getTracks({
						from: start,
						to: end,
						vessels: vessel
					}).then(trackTimeline => {
						if (trackTimeline && (trackTimeline.items.length > 0))
							seg.timeline.set({
								open: false,
								delimiters: [
									start,
									end
								],
								groups: [trackTimeline]
							}), e => seg.log.error("ERROR_VESSELS_COMMAND_INFO_GET_TRACK", e.error + ": " + e.result);
						else
							seg.map.disableFilter("ACQ_FILTER");
					});
				},
				wasteFunctions: {wasteTypeValues},
				getPSCInspections,
				getNavigationalStatus,
				getDimensions,
				ssnEnrichment: config.ssn_enrichment,
				ssnEnrichmentShowmore: config.ssn_enrichment_showmore,
				lrit: config.lrit,
				GAPlan: config.ga_plan,
				thetis: config.thetis,
				showGetAlerts: true,
				getSpeedUnits: () => seg.preferences.workspace.get("speedUnits")
			})
		},
		//template url for this C&I
		templateUrl: resolvePath("./templates/command_info.html")
	},
	// search: {
	// 	name: "Vessels",
	// 	keys: ["shipName", "mmsi", "ir", "imo", "callSign", "flagState"],
	// 	label: "shipName",
	// 	otherKeys: ["mmsi", "imo"],
	// 	smartSearch: async (smartSearchResults) => {
	// 		seg.layer.get("positions").getSource().getFeatures().forEach((vessel) => vessel.unset("isSmartSearchResult"));
	// 		const positions = smartSearchResults.POSITIONS_KEY;
	// 		if (positions && positions.features && positions.features.length) {
	// 			const vessels = await mergeDuplicates((new ol.format.GeoJSON()).readFeatures(positions, {
	// 				dataProjection: "EPSG:4326",
	// 				featureProjection: seg.map.getProjection()
	// 			}));
	// 			vessels.forEach((vessel) => vessel.set("isSmartSearchResult", true));
	// 			seg.layer.get("positions").getSource().addFeatures(vessels);
	// 			return vessels;
	// 		}
	// 		return [];
	// 	}
	// },
	// filters: buildFilters(config)
});

module.exports = {
	buildPositionsLayer
};
