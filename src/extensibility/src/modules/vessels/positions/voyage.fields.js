module.exports = {
	portofcall: {
		label: "Port of Call",
		template: `<span ng-if="::voyage.VoyageInformation.PortOfCall">
			<span ng-class="{'bold':voyage.isCurrent}" locode="voyage.VoyageInformation.PortOfCall"></span>
		</span>`,
		visible: true
	},
	ATA: {
		label: "ATA Port Of Call",
		template: `<span>
			<span ng-class="{'bold':voyage.isCurrent}">
				<span ng-if="voyage.ArrivalNotificationDetails.ATAPortOfCall" position-timestamp="voyage.ArrivalNotificationDetails.ATAPortOfCall"></span>
				<span ng-if="!voyage.ArrivalNotificationDetails.ATAPortOfCall">N/A</span>
			</span>
		</span>`,
		visible: true
	},
	ATD: {
		label: "ATD Port Of Call",
		template: `<span>
			<span ng-class="{'bold':voyage.isCurrent}">
				<span ng-if="voyage.DepartureNotificationDetails.ATDPortOfCall" position-timestamp="voyage.DepartureNotificationDetails.ATDPortOfCall"></span>
				<span ng-if="!voyage.DepartureNotificationDetails.ATDPortOfCall">N/A</span>
			</span>
		</span>`,
		visible: true
	},
	Anchorage: {
		label: "Anchorage",
		template: `<span ng-class="{'bold':voyage.isCurrent}">
			{{ voyage.ArrivalNotificationDetails.Anchorage || 'N/A' }}
		</span>`,
		visible: true
	},
	ETAToPortOfCall: {
		label: "ETA to Port Of Call",
		template: `<span>
			<span ng-class="{'bold':voyage.isCurrent}">
				<span ng-if="voyage.VoyageInformation.ETAToPortOfCall" position-timestamp="voyage.VoyageInformation.ETAToPortOfCall"></span>
				<span ng-if="!voyage.VoyageInformation.ETAToPortOfCall">N/A</span>
			</span>
		</span>`,
		visible: true
	},
	ETDFromPortOfCall: {
		label: "ETD from Port Of Call",
		template: `<span>
			<span ng-class="{'bold':voyage.isCurrent}">
				<span ng-if="voyage.VoyageInformation.ETDFromPortOfCall" position-timestamp="voyage.VoyageInformation.ETDFromPortOfCall"></span>
				<span ng-if="!voyage.VoyageInformation.ETDFromPortOfCall">N/A</span>
			</span>
		</span>`,
		visible: true
	},
	lastport: {
		label: "Last Port",
		template: `<span ng-if="::voyage.VoyageInformation.LastPort">
			<span ng-class="{'bold':voyage.isCurrent}" locode="voyage.VoyageInformation.LastPort"></span>
		</span>`,
		visible: true
	},
	ETDFromLastPort: {
		label: "ETD from Last Port",
		template: `<span>
			<span ng-class="{'bold':voyage.isCurrent}">
				<span ng-if="voyage.VoyageInformation.ETDFromLastPort" position-timestamp="voyage.VoyageInformation.ETDFromLastPort"></span>
				<span ng-if="!voyage.VoyageInformation.ETDFromLastPort">N/A</span>
			</span>
		</span>`,
		visible: true
	},
	nextPort: {
		label: "Next Port",
		template: `<span ng-if="::voyage.VoyageInformation.NextPort">
			<span ng-class="{'bold':voyage.isCurrent}" locode="voyage.VoyageInformation.NextPort"></span>
		</span>`,
		visible: true
	},
	ETAtoNextPort: {
		label: "ETA to Next Port",
		template: `<span>
			<span ng-class="{'bold':voyage.isCurrent}">
				<span ng-if="voyage.VoyageInformation.ETAToNextPort" position-timestamp="voyage.VoyageInformation.ETAToNextPort"></span>
				<span ng-if="!voyage.VoyageInformation.ETAToNextPort">N/A</span>
			</span>
		</span>`,
		visible: true
	},
	POBVoyageTowardsPortOfCall: {
		label: "POB Voyage Towards Port Of Call",
		template: `<span ng-class="{'bold':voyage.isCurrent}">
			{{voyage.PreArrival24HoursNotificationDetails.POBVoyageTowardsPortOfCall || "N/A"}}
		</span>`,
		visible: true
	},
	PositionInPortOfCall: {
		label: "Position In Port Of Call",
		template: `<span ng-class="{'bold':voyage.isCurrent}">
			{{voyage.VoyageInformation.PositionInPortOfCall || "N/A"}}
		</span>`,
		visible: true
	},
	hazmat: {
		label: "Hazmat",
		template: `<span>
			<span ng-class="{'bold':voyage.isCurrent}">{{voyage.HazmatConfirmation.HazmatOnBoardYorN}}</span>
		</span>`,
		visible: true
	},
	waste: {
		label: "Waste",
		template: `<span>
			<span ng-class="{'bold':voyage.isCurrent}">{{voyage.WasteConfirmation.WasteDeliveryStatus}}</span>
		</span>`,
		visible: true
	},
	security: {
		label: "Security",
		template: `<span>
			<span ng-class="{'bold':voyage.isCurrent}">{{voyage.SecurityConfirmation.CurrentSecurityLevel}}</span>
		</span>`,
		visible: true
	}
};