const COLOR_SCHEMES = [
	{name: "Lucky Charms", colors:["#e3edff", "#d5d5fe", "#d8c8fc", "#e8bbf8", "#f4b0ee", "#f0a5ce", "#ea9caa", "#e3a293", "#dbb78b", "#d3cc84", "#b5ca7e", "#93bf79", "#75b475"]},
	{name: "Pastel Rainbow", colors: ["#f6c6e6", "#e1c4f4", "#c9c4f4", "#c4daf4", "#c4f0f4", "#c7f5c4", "#effac8", "#fffecc", "#fff4cc", "#fffacc", "#ffe0cc", "#ffcccc"]},
	{name: "Colorful",colors: ["#39c0b3", "#2ab0c5", "#227fb0", "#1f5ea8", "#274389", "#5c4399", "#6c2a6a", "#b32e37", "#eb403b", "#e98931", "#fbb735"]},
	{name: "Pastel Paradise", colors: ["#cbd79d", "#e0e1a8", "#e1cba8", "#e1a9a8", "#daafc9", "#b3afda", "#95bcde", "#afc0da", "#8ca5ca", "#5989ba"]},
	{name: "Cotton Candy", colors: ["#b8e3ff", "#b8d5ff", "#b8bbff", "#ccb8ff", "#e1b8ff", "#ffb8fe", "#ffb8e0", "#ffb8c1", "#ffbeb8"]},
	{name: "Lovely", colors: ["#8ac9b3", "#669b72", "#a3a56d", "#d76072", "#d08f99", "#a7726e", "#956072", "#522e6b"]},
	{name: "Happy Baby",colors: ["#e8f7c5", "#f7f2c5", "#f7e8c5", "#f7d1c5", "#eec5f7", "#c5cef7", "#c5f7eb"]},
	{name: "Fairy", colors: ["#ff838b", "#e2a97b", "#c6af71", "#a8a966", "#6a8d59", "#4a6870"]},
	{name: "iPod Shuffle", colors: ["#c6cdcc", "#6e6ec0", "#70dcda", "#6ed397", "#b50d30"]}
].map(colorScheme => Object.assign(colorScheme, {
	colors: colorScheme.colors.map(colorStep => "rgba("+colorStep.substr(1).match(/../g).map(hex => parseInt(hex, 16)).concat(".6").join(",")+")")
}));

const REFRESH_TIME = seg.CONFIG.getValue("vessels.cluster.REFRESH_TIME"), //1m
	CELL_RADIUS = seg.CONFIG.getValue("vessels.cluster.CELL_RADIUS"),
	SCALE = seg.CONFIG.getValue("vessels.cluster.SCALE"), //1:8000000
	CELL_VERTICES = [];



for (let i = 0; i < 6; i++) {
	const angle = (30 + i * 60) * Math.PI / 180;

	CELL_VERTICES.push([
		Math.round(Math.cos(angle) * CELL_RADIUS),
		Math.round(Math.sin(angle) * CELL_RADIUS)
	]);
}

CELL_VERTICES.push(CELL_VERTICES[0]);

module.exports = {
	COLOR_SCHEMES,
	REFRESH_TIME,
	SCALE,
	CELL_VERTICES,
	CELL_HEIGHT: CELL_RADIUS * 2,
	CELL_WIDTH: Math.round(CELL_RADIUS * Math.sqrt(3))
};
