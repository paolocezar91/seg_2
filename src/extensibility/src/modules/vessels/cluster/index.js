// CLUSTERS_REGRESSION
const {
	COLOR_SCHEMES,
	REFRESH_TIME,
	SCALE,
	CELL_VERTICES,
	CELL_HEIGHT,
	CELL_WIDTH
} = require("./config.js");

//cluster intervals based on color scheme
let clusters = [],
	intervals = [],
	//current color scheme
	colorScheme,
	//data points to calculate distribution
	values = [],
	clusterLayer,
	clusterSource,
	vesselsLayer,
	positionsLayer,
	sarSurpicLayer,
	//flags to prevent unnecessary computation
	shouldRecalculateClusters = false,
	shouldRecalculateIntervals = false,
	shouldUpdateGrid = true,
	shouldScheduleUpdate = false,
	clusterCache = {},
	gridCache,
	currentScale,
	gridSource,
	currentProjection,
	updateTimer,
	gridRequest;


const update = async () => {
	if(isVisible()) {
		const modal = seg.modal.openModal("Clusters", "Updating clusters...", {class: "info"});
		modal.setDismissCallback(() => {
			if (gridRequest){
				gridRequest.cancel();
				gridCache = gridRequest = undefined;
				shouldUpdateGrid = true;
				seg.modal.close(modal.title);
			}
		});

		if(shouldUpdateGrid){ // first call only
			shouldUpdateGrid = false;
			await updateGrid();
		}

		if(shouldRecalculateClusters) {
			shouldRecalculateClusters = false;
			recalculateClusters();
		}

		if(shouldRecalculateIntervals){
			shouldRecalculateIntervals = false;
			recalculateIntervals();
		}

		if(shouldScheduleUpdate) {
			shouldScheduleUpdate = false;
			updateTimer = seg.timeout(() => {
				clusterCache = {};
				gridCache = undefined;
				shouldUpdateGrid = true;
				update();
			}, REFRESH_TIME);
		}

		modal.setContent("Clusters updated.");
		modal.setClass("success");
		seg.modal.closeTimeout(modal.title);
	}

	clusterLayer.setVisible(isVisible());
};

/* this function tells if we should recalculate clusters
 * should be true only if clusters are visible
 * (cluster layer visible + scale + positions layer visible)
*/
const isVisible = () => seg.map.getScale() >= SCALE &&
	vesselsLayer.getVisible() &&
	positionsLayer.getVisible() &&
	(
		!sarSurpicLayer ||
		(sarSurpicLayer && !sarSurpicLayer.getSource().getFeatures().length)
	);

//fetch geohash grid and update grid source
const updateGrid = async () => {
	let gridsResult;

	if (!gridCache){
		gridRequest = seg.request.get("v1/grids");
		gridsResult = (await gridRequest).result;
		gridCache = gridsResult;
		shouldRecalculateClusters = shouldRecalculateIntervals = shouldScheduleUpdate = true;
	} else {
		gridsResult = gridCache;
		shouldRecalculateClusters = shouldRecalculateIntervals = true;
	}

	if (gridSource.getFeatures().length)
		gridSource.clear();

	gridSource.addFeatures(gridsResult.map(cell => {
		const {lon, lat} = Geohash.decode(cell.gh),
			/* we consider each geohash bucket as a point
			 * each point will then fall inside a hexagon */
			cellFeature = new ol.Feature({
				geometry: new ol.geom.Point([lon, lat]).transform("EPSG:4326", seg.map.getProjection()),
				count: cell.c
			});

		cellFeature.setId(cell.gh);

		return cellFeature;
	}));
};

//recalculate clusters
const recalculateClusters = () => {
	values = [];
	clusters = [];
	clusterLayer.ol_.setOpacity(0);

	const scale = seg.map.getScale(),
		projection = seg.map.getProjection(),
		projectionCode = projection.getCode(),
		cachedKey = JSON.stringify({scale, projection: projectionCode});

	if(clusterCache[cachedKey]){
		values = clusterCache[cachedKey].clusters.map(cluster => cluster.count);
		clusters = clusterCache[cachedKey].clusters.map(cluster => new ol.Feature({
			geometry: new ol.geom.Polygon([cluster.geometryVertices]),
			count: cluster.count
		}));
	} else {
		//clear values
		let extent = projection.getExtent();

		//XXX
		if(!extent) {
			if(projectionCode === "EPSG:32661")
				extent = [-45, 0, 135, 0];
			else if(projectionCode === "EPSG:32761")
				extent = [-135, 0, 45, 0];
			else seg.log.error("seg.clusters", "Unsupported projection: " + projectionCode, projection);

			extent = [
				...ol.proj.fromLonLat(ol.extent.getBottomLeft(extent), projection),
				...ol.proj.fromLonLat(ol.extent.getTopRight(extent), projection)
			];
		}

		const topLeft = seg.map.ol_.getPixelFromCoordinate(ol.extent.getTopLeft(extent)),
			bottomRight = seg.map.ol_.getPixelFromCoordinate(ol.extent.getBottomRight(extent));

		clusterCache[cachedKey] = Object.assign({}, clusterCache[cachedKey], {clusters: []});

		for(let line = 0, y = topLeft[1]; y - CELL_HEIGHT / 2 <= bottomRight[1]; y += CELL_HEIGHT * 3 / 4, line++)
			for(let x = topLeft[0] + line % 2 * CELL_WIDTH / 2; x - CELL_WIDTH / 2 <= bottomRight[0]; x += CELL_WIDTH) {
				const geometryVertices = CELL_VERTICES.map(vertex => seg.map.ol_.getCoordinateFromPixel([
						x + vertex[0],
						y + vertex[1]
					])),
					geometry = new ol.geom.Polygon([geometryVertices]),
					//get vessel count in geometry
					count = seg.source.getFeaturesIntersectingGeometry(gridSource, geometry).reduce((count, cell) => {
						return count + cell.get("count");
					}, 0);

				//only add if count > 0
				if(count) {
					const clusterFeature = new ol.Feature({geometry, count});
					clusters.push(clusterFeature);
					values.push(count);
					clusterCache[cachedKey].clusters.push({geometryVertices, count});
				}
			}
	}

	if (clusterSource.getFeatures().length)
		clusterSource.clear();
	clusterSource.addFeatures(clusters);
	clusterLayer.ol_.setOpacity(1);
};

const recalculateIntervals = () => {
	//clear intervals

	const scale = seg.map.getScale(),
		projection = seg.map.getProjection().getCode(),
		cachedKey = JSON.stringify({scale, projection});

	if(clusterCache[cachedKey] && clusterCache[cachedKey].intervals && clusterCache[cachedKey].intervals.length){
		intervals = clusterCache[cachedKey].intervals;
	} else {
		intervals = [];
		//calculate mean and standard deviation
		const mean = values.reduce((sum, value) => sum + value, 0) / values.length,
			sigma = Math.sqrt(values.reduce((sum, value) => sum + Math.pow(value - mean, 2), 0) / values.length),
			step = Math.max(1, Math.round(((mean + sigma * 2) - Math.max(0, mean - sigma * 2)) / colorScheme.colors.length));

		if (step){
			for(let i = 0, start = 0; i < colorScheme.colors.length; i++, start += step)
				intervals.push({
					color: colorScheme.colors[i],
					min: start,
					//last interval goes from step to Infinity
					max: i === colorScheme.colors.length-1?Infinity:(start + step - 1)
				});
			clusterCache[cachedKey] = Object.assign({}, clusterCache[cachedKey], {intervals});
		}
	}
};

const setColorScheme = index => {
	colorScheme = COLOR_SCHEMES[index];
	clusterSource.changed();
	// reseting intervals
	clusterCache = Object.keys(clusterCache)
		.reduce((cache, key) => {
			cache[key] = Object.assign({}, clusterCache[key], {intervals: []});
			return cache;
		}, {});
};

const init = () => {
	//source where geohash clusters are placed
	gridSource = seg.source.create({type: "Vector"});
	//cluster layer that will be shown on map
	clusterLayer = seg.layer.create({
		id: "clusters",
		name: "Clusters",
		type: "Vector",
		renderMode: "image",
		style(feature) {
			const value = feature.get("count");

			return new ol.style.Style({
				fill: new ol.style.Fill({
					color: intervals.find(interval => value >= interval.min && value <= interval.max).color
				}),
				text: new ol.style.Text({
					text: value.toString(),
					font: "11px Fira Sans"
				})
			});
		},
		source: {
			type: "Vector"
		},
		maxScale: SCALE
	}),
	clusterLayer.set("gridSource", gridSource);
	clusterSource = clusterLayer.getSource();
	seg.map.addLayer(clusterLayer);

	// seg.ui.preferences.addSectionToTab("Cluster", {
	// 	alias: "clusters",
	// 	bindings: {
	// 		get visible() {
	// 			return isVisible();
	// 		},
	// 		colorScheme(value) {
	// 			if (typeof value ==="undefined")
	// 				return colorScheme;

	// 			const colorSchemeIndex = COLOR_SCHEMES.indexOf(value);
	// 			seg.preferences.workspace.set("clusters.colorScheme", colorSchemeIndex);
	// 			shouldRecalculateIntervals = true;
	// 			mapChangeListener();
	// 		},
	// 		get colorSchemes() {
	// 			return COLOR_SCHEMES;
	// 		},
	// 		get clusterIntervals() {
	// 			return intervals;
	// 		},
	// 		clusterOptionsTemplate:
	// 		`<label class="flexbox">
	// 			<span class="color-scheme-name flex-1 text-size-sm">{{colorScheme.name}}</span>
	// 			<ul class="flexbox flex-1" style="list-style: none; flex-grow: 2;">
	// 				<li ng-repeat="color in colorScheme.colors" class="square-sm" ng-style="{'background-color': color}"></li>
	// 			</ul>
	// 		</label>`
	// 	},
	// 	templateUrl: resolvePath("./templates/preferences.html")
	// });

	vesselsLayer = seg.layer.get("vessels"),
	positionsLayer  = seg.layer.get("positions");
	sarSurpicLayer = seg.layer.get("sar_surpic");

	//try to update at any of these conditions
	clusterLayer.ol_.on("change:visible", update);
	vesselsLayer.ol_.on("change:visible", update);
	positionsLayer.ol_.on("change:visible", update);

	const mapChangeListener = () => {
		if ((shouldUpdateGrid || shouldScheduleUpdate) && updateTimer)
			seg.timeout.cancel(updateTimer);

		if (COLOR_SCHEMES.indexOf(colorScheme) !== seg.preferences.workspace.get("clusters.colorScheme"))
			setColorScheme(seg.preferences.workspace.get("clusters.colorScheme") || 0);

		const scale = seg.map.getScale(),
			projection = seg.map.getProjection();

		if(scale !== currentScale || projection !== currentProjection) {
			if (projection !== currentProjection){
				shouldUpdateGrid = true;
				currentProjection = projection;
			}

			if (scale !== currentScale)
				currentScale = scale;

			if(currentScale >= SCALE){
				shouldRecalculateClusters = shouldRecalculateIntervals = !!gridSource.getFeatures().length;
			} else {
				if (gridRequest){
					gridRequest.cancel();
					gridCache = gridRequest = undefined;
					shouldUpdateGrid = true;
					seg.modal.close("Clusters");
				}
			}
		}

		update();
	};

	seg.map.onViewportChangeEnd(mapChangeListener);
};

module.exports = {init};