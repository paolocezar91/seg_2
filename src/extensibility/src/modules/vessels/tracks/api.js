const mergeTracks = (tracks) => {
		const tracksSource = seg.layer.get("tracks").getSource();
		return tracks.map(track => {
			// smart vessel search returns an identity atribute, which contains the id
			const id = track.getId(),
				existing = tracksSource.getFeatureById(id);

			if (existing) {
				existing.set("surpic", [].concat(existing.get("surpic"), track.get("surpic")).filter(s => s));
				return existing;
			}

			return track;
		});
	},
	mergeDuplicatesTrack = tracks => {
		const chunks = [];
		const BATCH_SIZE = seg.CONFIG.getValue("BATCH_SIZE");

		for(let i=0; i<tracks.length; i+=BATCH_SIZE)
			chunks.push(tracks.slice(i, i+BATCH_SIZE));

		const mergedChunks = chunks.map(mergeTracks);
		return [].concat(...mergedChunks);
	},
	tracksTooltipDisplayPreferences_ = require("./properties").mapProperties({
		override: {
			timestamp: false,
			vessel_id: false,
			source: false,
			heading: false,
			speed: false
		}
	});

module.exports = {
	mergeDuplicatesTrack,
	get tracksTooltipDisplayPreferences() {
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("vessels.positions.tooltip.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (tracksTooltipDisplayPreferences_[key])
				tracksTooltipDisplayPreferences_[key].selected = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return tracksTooltipDisplayPreferences_;
	}
};