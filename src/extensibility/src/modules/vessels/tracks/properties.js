const defaultProperties = {
	shipName:{
		label: "Name",
		template: "{{trackPosition.get('vessel').get('shipName')}}",
		visible: false,
		order: 0
	},
	mmsi: {
		label: "MMSI",
		template: "{{trackPosition.get('vessel').get('mmsi')}}",
		visible: false,
		order: 1
	},
	imo:{
		label: "IMO",
		template: "{{trackPosition.get('vessel').get('imo')}}",
		visible: false,
		order: 2
	},
	ir: {
		label: "IR",
		template: "{{trackPosition.get('vessel').get('ir') || trackPosition.get('ircs')}}",
		visible: false,
		order: 3
	},
	callSign: {
		label: "Call Sign",
		template: "{{trackPosition.get('vessel').get('callSign')}}",
		visible: false,
		order: 4
	},
	timestamp: {
		label: "Timestamp",
		template: "<span position-timestamp=\"trackPosition.get('timestamp')\"></span>",
		visible: true,
		order: 5
	},
	vessel_id: {
		label: "Vessel ID",
		template: "{{::trackPosition.get('vessel').getId()}}",
		visible: true,
		order: 6
	},
	latitude: {
		label: "Latitude",
		template: "<span coordinate=\"[trackPosition.get('latitude'), 'lat']\"></span>",
		visible: true,
		order: 7
	},
	longitude: {
		label: "Longitude",
		template: "<span coordinate=\"[trackPosition.get('longitude'), 'lon']\"></span>",
		visible: true,
		order: 8
	},
	heading: {
		label: "Heading",
		template: "{{trackPosition.get('heading')?(trackPosition.get('heading')+'°'):''}}",
		visible: true,
		order: 9
	},
	speed: {
		label: "Speed",
		template: "{{trackPosition.get('speed')?(trackPosition.get('speed')+' KNOTS'):''}}",
		visible: true,
		order: 10
	},
	source: {
		label: "Source",
		template: "{{ trackPosition.get('uploadType') || trackPosition.get('source')}}",
		visible: true,
		order: 11
	},
	vdmRawMessage: {
		label: "VDM Raw Message",
		template: "{{::trackPosition.get('vdmRawMessage')}}",
		visible: true,
		order: 12
	}
};

const mapProperties = (options) => seg.utils.mapItemProperties(defaultProperties, options);

module.exports = {mapProperties};

