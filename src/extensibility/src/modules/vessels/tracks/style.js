const stylesCache = {},
	selectedStyleCache = {},
	tracklineStyleCache = {},
	getTrackSelectedSize = () => Math.min(25, Math.max(1, Math.round(5500000 / seg.map.getScale()) * 12)),
	getTrackSize = () => Math.min(15, Math.max(1, Math.round(4400000 / seg.map.getScale()) * 12)),
	TRACK = require("./track-position.svg"),
	TRACK_NO_HDG = require("./track-position-nohdg.svg");

let color_index = 0;

const colors = [
	"#ff0000", // red
	"#9B0000", // red_dark
	"#e57373", // red_light
	"#ff6f00", // orange
	"#ffe57f", // yellow_light
	"#ccff90", // lime_light
	"#76ff03", // lime
	"#00ff00", // green_light
	"#007A47", // green
	"#33691e", // green_dark
	"#00ffff", // cyan
	"#00788D", // cyan_dark
	"#2196f3", // blue_light
	"#0000ff", // blue
	"#000068", // blue_dark
	"#ff00ff", // pink
	"#A10081", // pink_dark
	"#8400C1", // violet
	"#5e35b1", // deep_purple
	"#654515", // brown
	"#8d6e63", // beige
	"#9e9e9e", // grey
	"#546e7a" // grey_dark
];

const getRandomColor = () => {
	color_index++;

	if (color_index >= colors.length + 1)
		color_index = 1;

	return colors[color_index - 1];
};

module.exports = trackPosition => {
	//if line string
	if (trackPosition.get("isTrackLine")) {
		const selected = trackPosition.get("positions").some(position => position.get("selected")) || trackPosition.get("vessel").get("selected"),
			width = selected ? 2 : (seg.preferences.map.get("vessels.tracks.width") || 1);
		let color;

		if(!trackPosition.get("isImported")) {
			color = selected ? "#FFFF00" : (seg.preferences.map.get("vessels.tracks.color") || "#000");
		} else {
			if(!trackPosition.get("randomColor"))
				trackPosition.set("randomColor", getRandomColor());
			color = selected ? "#FFFF00" : (trackPosition.get("randomColor") || "#000");
		}

		const cacheKey = JSON.stringify({id: trackPosition.getId(), width, selected, color});

		if (!tracklineStyleCache[cacheKey]){

			tracklineStyleCache[cacheKey] = new ol.style.Style({
				stroke: new ol.style.Stroke({
					color,
					width
				})
			});
		}

		return tracklineStyleCache[cacheKey];
	}

	if (!trackPosition.get("visible") && !trackPosition.get("selected"))
		return;

	const styles = [];

	if (trackPosition.get("selected")) {
		const radius = getTrackSelectedSize() / 2;

		if (!selectedStyleCache[radius])
			selectedStyleCache[radius] = new ol.style.Style({
				image: new ol.style.Circle({
					radius,
					fill: new ol.style.Fill({
						color: "rgba(255,255,255,0.6)"
					}),
					stroke: new ol.style.Stroke({
						"color": "#3E3E3E",
						"width": 2
					})
				})
			});

		styles.push(selectedStyleCache[radius]);
	}

	const rotation = ((trackPosition.get("heading") === undefined) || Number.isNaN(trackPosition.get("heading"))) ? undefined : (trackPosition.get("heading") * Math.PI / 180),
		hasHeading = typeof rotation !== "undefined",
		height = hasHeading ? getTrackSize() : (getTrackSize() - 2),
		ssnEventHazmatYorN = (trackPosition.get("ssn_event") && trackPosition.get("ssn_event").HazmatOnBoardYorN) || false;

	let fill = "#fff",
		stroke = {
			color: "#000",
			width: 2
		};


	if (trackPosition.get("source"))
		switch (trackPosition.get("source")) {
			case "T-AIS":
				fill = "#000";
				break;
			case "Sat-AIS":
				fill = "#00f";
				break;
			case "LRIT":
				fill = "#999";
				break;
			case "VMS":
				fill = "#ff0";
				break;
			case "MRS":
				fill = "#795548";
				break;
			case "VDS":
				fill = "#f0f";
				break;
			case "Radar":
				fill = "#ff8219";
				break;
			case "FS":
				fill = "#6700ff";
				break;
			case "EX":
				fill = "#ffd000";
				break;
		}

	if (trackPosition.get("uploadType")){
		stroke = {
			color: "#0066ff",
			width: 4
		};

		switch (trackPosition.get("uploadType")) {
			case "Sighting":
				fill = "#fdfe02";
				break;
			case "Inspections no infrigement":
				fill = "#00cc00";
				break;
			case "Inspections infrigement":
				fill = "#f00";
				break;
			case "Other":
				fill = "#d9d9d9";
				break;
		}
	}


	const cacheKey = JSON.stringify({
		fill, height, rotation, ssnEventHazmatYorN
	});

	if (!stylesCache[cacheKey]) {
		if (ssnEventHazmatYorN)
			stylesCache[cacheKey] = [
				new ol.style.Style({
					image: new ol.style.Circle({
						radius: height,
						fill: new ol.style.Fill({
							color: "#FF0"
						})
					})
				}),
				new ol.style.Style({
					text: new ol.style.Text({
						font: height + "px Fira Sans",
						text: ssnEventHazmatYorN === "Y" ? "H" : "P", // if has Hazmat, icon is H otherwise is P
						fill: new ol.style.Fill({
							color: "#000"
						})
					})
				})
			];
		else
			stylesCache[cacheKey] = [
				new ol.style.Style({
					image: new seg.style.SVG({
						svg: hasHeading?TRACK:TRACK_NO_HDG,
						fill,
						height,
						stroke,
						rotation
					})
				})
			];
	}

	styles.push(...stylesCache[cacheKey]);

	return styles;
};
