const countriesList = require("../../../common/flag_states.json"),
	{PSC_SHIP_TYPES, POSITION_SOURCES} = require("../api.js"),
	vesselTypeList = [].concat({label: "None", value: null}, Object.entries(PSC_SHIP_TYPES).map(([key, value]) => ({label: key, value})), ),
	countryList = [].concat({label: "None", value: null}, Object.entries(countriesList).map(([key, value]) => ({label: "(" + key + ") " + value, value: key})));

module.exports = (() => {
	const _config = {
		vesselTracks: {
			mode: "All",
			modes: ["All", "Select..."],
			sources: {
				selected: [],
				options: POSITION_SOURCES,
				toggleSource: (source) => {
					const selectedSources = _config.vesselTracks.sources.selected;

					if(selectedSources.indexOf(source) !== -1)
						selectedSources.splice(selectedSources.indexOf(source), 1);
					else selectedSources.push(source);
				}
			},
			vesselTypeList: {
				selected: null,
				options: vesselTypeList,
				changeCallback: filter => {
					_config.vesselTracks.vesselTypeList.options = vesselTypeList.filter(type => type.label.toLowerCase().includes(filter.toLowerCase()));
				}
			},
			countryList: {
				selected: null,
				options: countryList,
				changeCallback: filter => {
					_config.vesselTracks.countryList.options = countryList.filter(country => country.label.toLowerCase().includes(filter.toLowerCase()));
				}
			},
			enabled: true,
			latest: true,
			simple: {
				enabled: false,
				latest: true,
				first: false,
				toggleSimpleOptions: (option) => {
					const simpleOptions = _config.vesselTracks.simple;

					if (!(simpleOptions.latest && !simpleOptions.first) && option === "latest")
						return simpleOptions.latest = !simpleOptions.latest;

					if (!(simpleOptions.first && !simpleOptions.latest) && option === "first")
						return simpleOptions.first = !simpleOptions.first;
				}
			},
			resetVesselTracks: () => {
				_config.vesselTracks.simple.enabled = false;
				_config.vesselTracks.simple.latest = true;
				_config.vesselTracks.simple.first = false;
				_config.vesselTracks.countryList.selected = null;
				_config.vesselTracks.vesselTypeList.selected = null;
				_config.vesselTracks.sources.selected = [];
				_config.vesselTracks.mode = "All";
			}
		},
		ssnEnrichment: {
			enabled: false
		}
	};
	return _config;
})();