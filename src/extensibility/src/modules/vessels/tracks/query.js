const { getTracks } = require("../api.js"),
	TRACKS_LIMIT_TIME = seg.CONFIG.getValue("vessels.tracks.TRACKS_LIMIT_TIME") || 24, //MONTHS
	buildTrackQueryPanel = trackPanelConfig => ({
		label: "Get Track",
		persistent: true,
		template: resolvePath("./templates/query.html"),
		scope: {
			vesselTrackQuery: (() => {
				const submit = ()  => {
						const trackOptions = {
							from: config.timePeriod.from,
							to: config.timePeriod.to,
							vessels: seg.selection.getSelected("positions"),
							ssnEnrichment: config.services.ssnEnrichment.enabled,
							simple: config.services.vesselTracks.simple
						};
						if(config.services.vesselTracks.mode === "Select...")
							trackOptions.source = config.services.vesselTracks.sources.selected.join(",");

						return getTracks(trackOptions).then(trackTimeline => seg.timeline.set({
							open: false,
							delimiters: [
								trackOptions.from,
								trackOptions.to
							],
							groups: [trackTimeline]
						}), e => seg.log.error("ERROR_VESSELS_QUERY_GET_TRACKS", e.error + ": " + e.result));
					},
					isVesselSelected = ()  => seg.selection.getSelected("positions").length?true:false,
					timerangeCheck = () => {
						const TRACKS_LIMIT_TIME = seg.CONFIG.getValue("vessels.tracks.TRACKS_LIMIT_TIME");

						if (config.timePeriod.to.diff(config.timePeriod.from, "M", true) < TRACKS_LIMIT_TIME)
							return true;
						return false;
					},
					sourcesCheck = () => {
						if(config.services.vesselTracks.mode === "Select..." && config.services.vesselTracks.sources.selected.length === 0)
							return false;
						return true;
					},
					canSubmit = () => {
						if (!timerangeCheck())
							return false;

						if(!sourcesCheck())
							return false;

						return isVesselSelected() && config.timePeriod.from.isBefore(config.timePeriod.to);
					},
					config = {
						timePeriod: {
							from: moment().subtract(1, "d"),
							to: moment()
						},
						services: require("./query.services.js"),
						submit,
						canSubmit,
						isVesselSelected,
						timerangeCheck,
						sourcesCheck,
						selectedVessels: seg.selection.getSelected("positions"),
						ssnEnrichment: trackPanelConfig.ssn_enrichment,
						TRACKS_LIMIT_TIME
					};

				return config;
			})()
		}
	});

module.exports = {
	buildTrackQueryPanel
};
