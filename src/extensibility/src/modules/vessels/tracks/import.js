let importedMenu;

const TYPE_OPTIONS = [
		{label: "Other", value: "Other"},
		{label: "Sighting", value: "Sighting"},
		{label: "Inspections no infrigement", value: "Inspections no infrigement"},
		{label: "Inspections infrigement", value: "Inspections infrigement"}
	],
	{ getTracks, getProperty, getEMSAId } = require("../api"),
	bindings = Object.assign({}, require("./query.services"), {
		withExternal: {
			enabled: false
		},
		timerange: {
			from: moment().subtract(1, "d"), // As default, make query one day before
			to: moment()
		},
		canSubmit: () => {
			const {timerange, withExternal} = bindings;
			const vesselsImported = seg.layer.get("positions").getSource().getFeatures().filter(vessel => vessel.get("isImported")).length;

			if (withExternal.enabled) {
				if (!(timerange.to.diff(timerange.from, "M", true) <= 6))
					return "Please, adjust the time range to less than 6 months";
				if (!(vesselsImported < 10))
					return "Importing is limited to a max of 10 distinct vessels";
			}

			return "ok";
		}
	}),

	parseDataWithImdateIds = async (data) => {
		// if external is true, then imdateid is necessary, because we're querying for tracks
		return await seg.promise.all(distinctDataByKey(data, "mmsi").map(async feature => {
			if(!feature.id && feature.mmsi)
				feature.id = Number((await getEMSAId(feature.mmsi, "mmsi")));
			if(!feature.id)
				feature.id = feature.mmsi;
			return feature;
		})).then(distinctVesselsWithId => {
			return data.map((feature) => {
				feature.id = distinctVesselsWithId.find(vessel => vessel.mmsi === feature.mmsi).id;
				return feature;
			});
		});
	},

	distinctDataByKey = (data, key) => {
		// Checks uploaded data to see distinct data by a given key (for vessels is mmsi, i.e.)
		return data
			.filter((obj, index, arr) => {
				const mapObj = arr.map(mapObj => String(mapObj[key]));
				return mapObj.indexOf(String(obj[key])) === index;
			});
	},

	//make feature track report = array of {particulars, track(LineString geojson), positions(FeatureCollection geojson)} like getTracks is expecting
	parseImportedTrackReports = (features) => {
		return Object.values(features.sort((a, b) => moment(a.timestamp) - moment(b.timestamp))
			.reduce((trackReports, ft) => {
				let longitude, latitude;

				if(!trackReports[ft.id])
					trackReports[ft.id] = {
						particulars: { //parseTrackReports expects EMSAId
							EMSAId: ft.id
						},
						track: {
							type: "Feature",
							geometry: {
								type: "LineString",
								coordinates: []
							},
							properties: {}
						},
						positions: {
							type: "FeatureCollection",
							features: []
						},
						isImported: true
					};

				if(seg.utils.isCoordinateDMSString(String(ft.longitude)))
					longitude = seg.utils.lonDMStoDecimal(seg.utils.parseDMSString(ft.longitude));
				else
					longitude = parseFloat(ft.longitude);

				if(seg.utils.isCoordinateDMSString(String(ft.latitude)))
					latitude = seg.utils.latDMStoDecimal(seg.utils.parseDMSString(ft.latitude));
				else
					latitude =  parseFloat(ft.latitude);
				// ft.uploadType = ft.uploadType.value;

				const trackReport = trackReports[ft.id];
				//merge particulars
				Object.assign(trackReport.particulars, ft, {longitude, latitude});

				//add position to track line
				trackReport.track.geometry.coordinates.push([longitude, latitude]);

				//add position to track
				trackReport.positions.features.push({
					type: "Feature",
					id: ft.pid || ft.timestamp.unix(),
					geometry: {
						type: "Point",
						coordinates: [longitude, latitude]
					},
					properties: Object.assign(ft, {
						visible: true,
						isImported: true,
						longitude,
						latitude
					})
				});

				return trackReports;
			}, {})
		);
	},
	createImportedFilters = (layer, filteredFeatures) =>{
		let importedFilters = layer.get("filters").find(filter => filter.name === "Imported Positions");
		const options = filteredFeatures.reduce((acc, ft) => Object.assign(acc, {
			[ft.name]: ((feature) => {
				const vessel = feature.get("seg_type") === "tracks" ? feature.get("vessel") : feature;
				return vessel.get("mmsi") === ft.mmsi;
			})
		}), {});

		if (!importedFilters) {
			const importedFiltersConfigForPositions = [{
				name: "Imported Positions",
				options: options
			}];
			// add to layer, but do not create menu
			importedFilters = seg.layer.filter.addFilters(layer, importedFiltersConfigForPositions)[0];
		} else {
			seg.layer.filter.addFilterOptions(importedFilters, options);
		}

		return importedFilters;
	};

module.exports = (vesselMenuItem) => {
	return ({
		label: "Vessel",
		featureAs: "vessel",
		//additional fields in table
		maxFeatures: 10,
		maxFeaturesKey: "mmsi",
		sourceOptions:["File", "Manual entry"],
		additionalFields: {
			timestamp: {
				label: "Timestamp",
				template: "<datetime ng-model='vessel.timestamp'></datetime>",
				visible: true
			},
			uploadType: {
				label: "Type",
				template: "<combo ng-model='vessel.uploadType' options='option.value as option.label for option in options'></combo>",
				visible: true,
				bindings: {
					options: TYPE_OPTIONS
				}
			}
		},
		defaultValue: {
			properties: {},
			geometry: {
				coordinates: [0, 0]
			}
		},
		//convert geojson to editable array of records
		toModel: features => features.map(({properties, geometry}) => ({
			// id: parseInt(getProperty(properties, /id/i)),
			timestamp: moment(getProperty(properties, /ts|timestamp|date time group/i)),
			mmsi: parseInt(getProperty(properties, /mmsi/i)),
			ircs: getProperty(properties, /ircs/i),
			"ext. marking": getProperty(properties, /ext\.\s?marking/i),
			name: getProperty(properties, /name/i),
			flag: getProperty(properties, /flag/i),
			uploadType: (TYPE_OPTIONS.find(type => type.label === getProperty(properties, /pos type/i)) || TYPE_OPTIONS[0]).value,
			latitude: parseFloat(geometry.coordinates[1]),
			longitude: parseFloat(geometry.coordinates[0]),
			heading: parseFloat((getProperty(properties, /hdg|heading/i) || "").replace("°",""))
		})),
		//take array of records and import them to SEG
		import: async (data, {timerange, vesselTracks, withExternal}) => {
			//get ids from available identifier
			parseDataWithImdateIds(data)
				.then(features => {
					const filteredFeatures = distinctDataByKey(features, "id"),
						importedTrackReports = parseImportedTrackReports(features),
						options = {
							from: timerange.from,
							to: timerange.to,
							simple: vesselTracks.simple,
							vessels: filteredFeatures.filter(({id}) => id && typeof id === "number" && !isNaN(id)).map(({id}) => id),
							source: (vesselTracks.mode !== "All" ? vesselTracks.sources.selected.join(",") : undefined)
						};

					// Creating filters for positions, this will not be added to menu, they're toggled by tracks filter as a workaraound
					const positionsLayer = seg.layer.get("positions");
					const importedFiltersForPositions = createImportedFilters(positionsLayer, filteredFeatures);

					const tracksLayer = seg.layer.get("tracks");
					const importedFiltersForTracks = createImportedFilters(tracksLayer, filteredFeatures);

					// removing previous menu if it exists
					if (vesselMenuItem.options.some(opt => opt.alias === "Imported Positions"))
						vesselMenuItem.options = vesselMenuItem.options.filter(opt => opt.alias !== "Imported Positions");

					// creating new menu item and adding to vesselMenu
					importedMenu = seg.ui.layerMenu.fromFilter(tracksLayer, importedFiltersForTracks);
					// workaround to trigger 2 filter with only one menu toggle, kids don't try this at home
					importedMenu.options.forEach(option => {
						option.bindings.filter.afterSetActive = () => {
							importedFiltersForPositions.options.find(opt => opt.name === option.alias).setActive(option.bindings.filter.getActive());
						};
					});

					// Reordering by alias
					importedMenu.options = importedMenu.options.sort((a, b) => {
						if (a.alias > b.alias) return 1;
						if (a.alias < b.alias) return -1;
						return 0;
					});
					vesselMenuItem.options.push(importedMenu);

					// if(!seg.map.isFilterEnabled("IMPORT_FILTER"))
					// 	seg.map.enableFilter("IMPORT_FILTER");

					seg.ui.apps.closeAll();
					const MAX_RECORDS = seg.CONFIG.getValue("vessels.tracks.MAX_RECORDS");

					return getTracks(Object.assign(options, {
						importedTrackReports,
						withExternal: withExternal.enabled,
						maxRecords: MAX_RECORDS
					}));
				}, e => seg.log.error("ERROR_VESSELS_PARSE_DATA_WITH_IMDATE_IDS", e.error + ": " + e.result));

		},
		//additional options for import
		additionalOptions: {
			template: `
				<div class="text-size-sm" id="track-import">
					<div>
						<toggle ng-model="withExternal.enabled"></toggle>
						<label>Include external positions</label>
					</div>
					<div class="flexbox flex-2 flex-align-center" id="vessel-track-modes" ng-if-start="withExternal.enabled">
						<radio ng-repeat-start="option in vesselTracks.modes" class="seg" ng-checked="vesselTracks.mode == option" ng-click="vesselTracks.mode = option"></radio>
						<label ng-repeat-end>{{option}}</label>
					</div>
					<ul class="flexbox flex-wrap text-size-sm vesselTracksModes" ng-if="vesselTracks.mode == 'Select...'">
						<li class="flexbox flex-align-center" ng-repeat="source in vesselTracks.sources.options">
							<checkbox ng-checked="vesselTracks.sources.selected.indexOf(source) != -1" ng-click="vesselTracks.sources.toggleSource(source)"></checkbox>
							<label>{{source}}</label>
						</li>
					</ul>
					<timerange class="flexbox" from="timerange.from" to="timerange.to" ng-if-end></timerange>
				</div>
				<style>
					#track-import > :not(:last-child) {
						margin-bottom: 10px;
					}
					#track-import radio + label, #track-import checkbox + label {
						margin-left: 7px;
					}
					#track-import label + radio, #track-import ul.vesselTracksModes li:not(:first-child) {
						margin-left: 10px;
					}
					#track-import timerange{
						width: 75%;
					}
					#track-import timerange{
						width: 75%;
					}
					#track-import timerange > :not(:last-child) {
						margin-right: 10px;
						width: auto;
					}
					.import-app grid combo > #container > ul {
						position: fixed;
					}
					.import-app button.removeRow {
						margin-right:6px;
					}
				</style>
			`,
			bindings
		}
	});
};
