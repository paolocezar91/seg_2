const {getTracks} = require("../api.js"),
	services = require("./query.services.js");

module.exports = ((config = {}) => {
	return {
		name: "Vessels",
		template: resolvePath("./templates/acq.html"),
		services,
		shouldFilter: () => !seg.layer.get("tracks").getSource().getFeatures().some(track => track.get("isImported")),
		submit: async (bboxes, from, to) => {
			if(services.vesselTracks.enabled) {
				const trackOptions = {
					from,
					to,
					bboxes,
					acq: true,
					ssnEnrichment: services.ssnEnrichment.enabled,
					simple: services.vesselTracks.simple,
					flag: services.vesselTracks.countryList.selected,
					type: services.vesselTracks.vesselTypeList.selected
				};

				if(services.vesselTracks.mode === "Select...")
					trackOptions.source = services.vesselTracks.sources.selected.join(",");

				// GetPrediction Here
				/*
				getPrediction(trackOptions).then(res => {
					let numberOfResults = res.result,
						predictedResultsModalConfiguration = Object.assign({},getPredictedResultsWarningOrError(numberOfResults,ACQ_WARNING_THRESHOLD,ACQ_ERROR_THRESHOLD));

					if (predictedResultsModalConfiguration){
						if (!predictedResultsModalConfiguration.title.includes("Error"))
							Object.assign(predictedResultsModalConfiguration, {
								bindings:{
									go: async ()=>{
										seg.popup.close("ACQ Warning");
										getTracks(trackOptions);
									},
									close: () => {
										seg.popup.close("ACQ Warning");
									}
								}
							});
						seg.popup.openPopup(predictedResultsModalConfiguration);
					}
				});
				*/
				// ACQ should return object for timeline component, which get tracks does
				return getTracks(trackOptions);
			}
		},
		isAreaValid(selectedAreas){
			const ACQ_AREA_LIMIT = seg.CONFIG.getValue("vessels.tracks.ACQ_AREA_LIMIT"),
				limitArea = config.limitArea || ACQ_AREA_LIMIT;
			// an area limit is imposed if vessel tracks are seleted in any
			if (!Array.isArray(selectedAreas) || !selectedAreas.length){
				this.services.vesselTracks.areaStatus = {
					invalid: true,
					msg: "Select an area."
				};
			} else {
				//If vessel tracks are enabled then check area limit of 15 000 nm^2
				const convertedToNauticalMilesAreas = selectedAreas.reduce((areas, area) => {
					const clone = area && area.getGeometry().clone();
					if (clone && clone.getCoordinates) {
						const nmArea = Object.assign({}, {
							nm: seg.utils.convertFromSquareMeters(Math.abs(ol.sphere.getArea(clone)), "NM2")
							// selected: area.get("selected")
						});

						if ((nmArea.nm < limitArea))
							return areas.concat(nmArea);
					}
					return areas;
				}, []);

				this.services.vesselTracks.areaStatus = {
					invalid: (!selectedAreas.length || !convertedToNauticalMilesAreas.length),
					msg: "Area should be smaller than " + Math.ceil(seg.utils.convertFromSquareMeters(seg.utils.convertToSquareMeters(15000, "NM2"), seg.preferences.workspace.get("areaUnits"))) + " " + seg.preferences.workspace.get("areaUnits") + "."
				};
			}
			return !this.services.vesselTracks.areaStatus.invalid;
		},
		isTimeValid(start, end) {
			const ACQ_TIME_LIMIT = seg.CONFIG.getValue("vessels.tracks.ACQ_TIME_LIMIT");
			const limitTime = config.limitTime || ACQ_TIME_LIMIT;
			this.services.vesselTracks.timeStatus = {
				invalid: !(end.diff(start, "d", true) <= limitTime),
				msg: "Time range should be less than " + limitTime + " day(s)."
			};
			return !this.services.vesselTracks.timeStatus.invalid;
		},
		isSelectionValid() {
			this.services.vesselTracks.selectionStatus = {
				invalid: this.services.vesselTracks.mode === "Select..." && //Either no service is selected no footprints in ACQ and we"re in Select... mode
					!this.services.vesselTracks.sources.selected.length
				,
				msg: "Select at least one source."
			};

			return !this.services.vesselTracks.selectionStatus.invalid;
		}
	};
});

// const ACQ_WARNING_THRESHOLD = 10;
// const ACQ_ERROR_THRESHOLD = 20;
// const ACQ_MAX_RECORD = 20;

// const ACQ_PREDICTION_TEMPLATE = {
// 	"Warning": `
// 		The query parameters are too wide and track query results will probably be truncated (some intermediate positions may be lost).
// 		Would you like to continue anyway?"
// 		<div class="flexbox" style="margin-top:15px">
// 			<button title="Yes" class="seg positive rounded md" ng-click="go()" style="margin-right:5px;">Continue</button>
// 			<button title="No"class="seg positive rounded md" ng-click="close()">No</button>
// 		</div>
// 		`,
// 	"Error": `
// 		The query parameters are too wide. Restrict area size or time range to continue.
// 		<div class="flexbox" style="margin-top:15px">
// 			<button title="No"class="seg positive rounded md" ng-click="close()">OK</button>
// 		</div>
// 		`
// };

// const buildACQPredictionModal = (Type) => {
// 	return {
// 		title:"ACQ "+Type,
// 		template: ACQ_PREDICTION_TEMPLATE[Type],
// 		bindings: {
// 			async close() {
// 				seg.popup.close("ACQ "+Type);
// 			}
// 		}
// 	};
// };

// const getPredictedResultsWarningOrError = (numberOfResults, warningThreshold, errorThreshold) => {
// 	const overWarningThreshold = (numberOfResults > warningThreshold),
// 		overErrorThreshold = (numberOfResults > errorThreshold);

// 	if (!overErrorThreshold && !overWarningThreshold)
// 		return null;

// 	return (overWarningThreshold && overErrorThreshold)?buildACQPredictionModal("Error"):buildACQPredictionModal("Warning");
// };

// const getPrediction = async (options) => {
// 	return await seg.request.get("v1/imdate/predicted_result", {
// 		params: {
// 			beginTimestamp: moment(options.from).format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
// 			endTimestamp: moment(options.to).format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
// 			source: options.sources,
// 			minX: options.bboxes[0],
// 			minY: options.bboxes[1],
// 			maxX: options.bboxes[2],
// 			maxY: options.bboxes[3]
// 		}
// 	}).then(res => {
// 		let modalConfiguration = getPredictedResultsWarningOrError(res.result,ACQ_WARNING_THRESHOLD,ACQ_ERROR_THRESHOLD);
// 		return modalConfiguration?modalConfiguration:null;
// 	});
// }