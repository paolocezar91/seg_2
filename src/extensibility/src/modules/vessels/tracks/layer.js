const {getLabel} = require("../api"),
	{tracksTooltipDisplayPreferences} = require("./api");

module.exports = {
	id: "tracks",
	name: "Tracks",
	type: "Vector",
	renderMode: "image",
	style: require("./style.js"),
	source: {
		type: "Vector",
		name: "Vessel Tracks",
		projection: "EPSG:4326",
		wrapX: false
	},
	tooltip: {
		featureAs: "trackPosition",
		bindings: {
			getLabel(trackPosition) {
				return getLabel(trackPosition.get("vessel"));
			},
			tracksTooltipDisplayPreferences
		},
		templateUrl: resolvePath("./templates/tooltip.html")
	},
	filters:[{
		name:"Track Position Source",
		options: {
			"T-AIS": (track)  => track.get("source") === "T-AIS",
			"Ship-AIS": (track) => track.get("source") === "Ship-AIS",
			"Sat-AIS": (track) => track.get("source") === "Sat-AIS",
			"LRIT": (track) => track.get("source") === "LRIT",
			"VDS": (track) => track.get("source") === "VDS",
			"MRS": (track) => track.get("source") === "MRS",
			"VMS": (track) => track.get("source") === "VMS",
			"Coastal radar": (track) => track.get("source") === "Radar",
			"ISA Safe Trx": (track) => track.get("source") === "ISA Safe Trx", //not sure about this
			"N/A": (track) => !track.get("source") || track.get("source") === "" || typeof track.get("source") === "undefined" //should be when track has no src property
		}
	}]
};
