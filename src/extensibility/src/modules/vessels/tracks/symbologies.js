module.exports = [
	{
		title: "Track Source",
		symbologies: [
			{
				class: "tais",
				label: "T-AIS",
				style: "fill: #000000"
			},
			// {
			//     class: "shipais",
			//     label: "Ship-AIS",
			//     style: "fill: #a9d130"
			// },
			{
				class: "satais",
				label: "SAT-AIS",
				style: "fill: #0000FF"
			},
			{
				class: "lrit",
				label: "LRIT",
				style: "fill: #999999"
			},
			{
				class: "mrs",
				label: "MRS",
				style: "fill: #ff00ff"
			},
			{
				class: "vds",
				label: "VDS",
				style: "fill: #795548"
			},
			{
				class: "_radar",
				label: "Radar",
				style: "fill: #ff8219"
			},
			{
				class: "vms",
				label: "VMS",
				style: "fill: #ffff00"
			},
			{
				class: "fs",
				label: "FS",
				style: "fill: #6700ff"
			},
			{
				class: "ex",
				label: "EX",
				style: "fill: #f1e767"
			}
		]
	},
	{
		title: "Symbology",
		symbologies: [
			{
				class: "",
				label: "Track position",
				style: "fill: #ffffff"
			},
			{
				class: "sightings",
				label: "Sightings",
				style: "fill: #fdfe02; stroke: #0066ff; stroke-width: 3"
			},
			{
				class: "inspections-no-infringement",
				label: "Inspections no infringement",
				style: "fill: #00cc00; stroke: #0066ff; stroke-width: 3"
			},
			{
				class: "inspections-infringements",
				label: "Inspections infringement",
				style: "fill: #ff0000; stroke: #0066ff; stroke-width: 3"
			},
			{
				class: "other",
				label: "Other/Upload",
				style: "fill: #d9d9d9; stroke: #0066ff; stroke-width: 3"
			}
		]
	}
];