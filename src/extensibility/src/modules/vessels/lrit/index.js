const {
	mergeDuplicates,
	getCurrentPosition
} = require("../api.js");

module.exports = (permissions) => ({
	label: "LRIT",
	persistent: true,
	template: resolvePath("./templates/lrit.html"),
	scope: {
		lrit: (() => {
			const requestSARTypeOptions = [],
				accessTypeOptions = permissions.lrit.accessTypeOptions,
				requestTypeOptionsBase = permissions.lrit.requestTypeOptionsBase,
				requestTypeOptionsFull = permissions.lrit.requestTypeOptionsFull,
				portOptions = [ "Port 1", "Port 2", "Port 3"],
				config = {
					services: {
						vesselIMO: "",
						accessType: accessTypeOptions[0],
						accessTypeOptions,
						requestTypeBase: requestTypeOptionsBase[0].value,
						requestTypeOptionsBase,
						requestTypeFull: requestTypeOptionsFull[0].value,
						requestTypeOptionsFull,
						requestSARTypeOptions,
						countryList: seg.countries.getCountries("MEMBER_STATE"),
						country: null,
						startDate: moment(),
						endDate: moment().add(1,"d"),
						portOptions,
						port: null,
						distance: 0
					},
					submit,
					canSubmit,
					selectedVessels: [],
					isNewVessel: false,
					isOneTime
				};

			const makeLRITRequest = (params) => {
				if(params.accessType !== "SAR") {
					// open modal to ask permition
					const template = `There is a fee associated with this request. <br>
						Do you wish to continue?
						<div class="row" style="padding-top:5px;">
							<button class="seg flex-1 positive rounded" style="margin-right:5px" ng-click="popup.close(); submitRequest()">OK</button>
							<button class="seg flex-1 positive rounded" ng-click="popup.close()">Cancel</button>
						</div>`,
						bindings = {
							submitRequest: () => submitRequest(params)
						};
					seg.popup.openPopup({title: "LRIT Request", template, bindings});

				} else {
					submitRequest(params);
				}
			};

			const canSubmit = () => {
				return ((config.selectedVessels.length === 1 && config.selectedVessels.some(vessel => vessel.get("selected") && vessel.get("imo"))) || isValidIMO()) &&
					(isOneTime() || (config.services.startDate.isSameOrAfter(moment(), "days") && config.services.startDate.isBefore(config.services.endDate)));
			};

			const isValidIMO = () => {
				if(config.isNewVessel && config.services.vesselIMO) {
					const IMO = config.services.vesselIMO.toString().split("");

					if(IMO.length !== 7)
						return false;

					return IMO.slice(0, 6).reduce((check, current, i) => check + current * (7 - i), 0).toString().substr(-1) === IMO[6];
				}

				return false;
			};

			const submit = () => {
				const params = {
					imoNumber: (config.services.vesselIMO !== "") ? config.services.vesselIMO : config.selectedVessels[0].get("imo"),
					accessType: config.services.accessType,
					periodicRate: (config.services.accessType === "SAR") ? config.services.requestTypeBase : config.services.requestTypeFull,
					startDateTime: config.services.startDate.toISOString(),
					stopDateTime: config.services.endDate.toISOString()
				};

				makeLRITRequest(params);
			};

			const isOneTime = () => ((config.services.accessType === "SAR" && config.services.requestTypeBase === "OneTime") || (config.services.requestTypeFull === "OneTime"));

			const submitRequest = (params) => {
				const loaderModal = seg.modal.openModal("LRIT", "Submiting LRIT request...", {class: "info"});

				if(params.periodicRate === "OneTime") {
					delete params.startDateTime;
					delete params.stopDateTime;
				}

				const lritRequest = seg.request.get("v1/ship/lrit/periodic_rate?", {
					params
				});

				loaderModal.setDismissCallback(() => lritRequest.cancel());

				lritRequest.then(res => {
					if(params.periodicRate === "OneTime") {
						const positionsSource = seg.layer.get("positions").getSource(),
							latestPosition = positionsSource.getFeatures().find(vessel => vessel.get("imo") === params.imoNumber),
							latestPositionTs = latestPosition && latestPosition.get("ts"),
							positionUpdateListener = positionsSource.on("update", async ({features}) => {
								let vessel = features.find(vessel => vessel.get("imo") === params.imoNumber);
								if(!vessel) {
									vessel = await getCurrentPosition({imo: params.imoNumber});
									if(vessel)
										positionsSource.addFeature(await mergeDuplicates(vessel)[0]);
								}

								if(!latestPositionTs || vessel.get("ts") > latestPositionTs) {
									seg.timeout(() => seg.ui.alerts.addFreshAlert({
										label: "New position for vessel (IMO: "+vessel.get("imo")+")",
										vessel,
										callback() {
											seg.selection.selectFeatureAndCenter(vessel);
										}
									}));

									ol.Observable.unByKey(positionUpdateListener);
								}
							});
					}

					loaderModal.setContent("LRIT request completed.");
					loaderModal.setClass("success");
					seg.modal.closeTimeout(loaderModal.title);

					const lritPastRequests = seg.preferences.session.get("LRIT") || [],
						request = {
							params,
							timestamp: moment()
						};

					lritPastRequests.push(request);
					seg.preferences.session.set("LRIT", lritPastRequests);

					const table = seg.ui.ttt.tables.openTables.find(t => t.label === "LRIT" ? t : false);

					if (table)
						table.data = lritPastRequests;
					else seg.ui.ttt.tables.open({
						label: "LRIT",
						customToolbar: {
							template: "<span class=\"text-size-sm\">Number of requests: <b>{{grid.filteredResults.length}}</b></span>"
						},
						data: lritPastRequests,
						itemAs: "history",
						fields: {
							requestDate: {
								label: "Date",
								template: "<span position-timestamp=\"history.timestamp\"></span>"
							},
							requestType: {
								label: "Access Type",
								template: "{{history.params.accessType}}"
							},
							startDate: {
								label: "Start Date",
								template: `
									<span ng-if="!history.params.startDateTime">N/A</span>
									<span ng-if="history.params.startDateTime" position-timestamp="history.params.startDateTime"></span>`
							},
							endDate: {
								label: "End Date",
								template: `
									<span ng-if="!history.params.stopDateTime">N/A</span>
									<span ng-if="history.params.stopDateTime" position-timestamp="history.params.stopDateTime"></span>`
							},
							imo: {
								label: "IMO Number",
								template: "{{history.params.imoNumber}}"
							},
							periodicRate: {
								label: "Periodic Rate",
								template: "{{history.params.periodicRate}}"
							}
						}
					});

					return res;

				}, e => {
					loaderModal.setContent("Request Failed");
					seg.modal.closeTimeout(loaderModal.title);
					seg.log.error("ERROR_IRIT_SUBMIT_REQUEST", e.error + ": " + e.result);
				});
			};

			return config;

		})()
	}
});
