/**
@namespace extensibility.modules.vessels
@description
# Vessels

## Available options
- **symbolizers** - Symbolizers available for the operation. It is an array that can contain any of the following strings:
	- "IALA"
	- "EFCA"
	- "Data Source"
	- "Target Age"
	- "Vessel Type"
	- "Voyage Information"
	- "EUNAVFOR"
	- "Gear Type"
	- "RFMO Status"
- **filters** - Filters available for the operation. It is an array that can contain any of the following strings:
	- "Voyage information"
	- "Position Source"
	- "Vessel Flag"
	- "Vessel Type (PSC Types)"
	- "Vessel Type (LRIT)"
	- "Navigation Status (AIS)"
	- "Risk Status"
	- "User Risk Status"
	- "Fishing vessels"
	- "Risk Levels"
	- "Special Categories"
	- "ICCAT"
	- "NAFO"
	- "NEAFC"
	- "EU Licence"
	- "Gear Type"
	- "Flag"
	- "FAO Types"
	- "PWR Class"
	- "GT Class"
	- "LOA Class"

The following options are all booleans. Omitting an option is the same as having a **false** value:
- **advanced_search**
	- **show_ssn_information**
- **ssn_enrichment** - SSN enrichment functionality and information
- **lrit** - LRIT requests
- **vessel_models** - Vessel outline at certain scale by default
- **fisheries_link** - Command&Info link to fisheries
- **ga_plan** - GA Plan upload/download
- **thetis** - THETIS information
- **track_import** - Import App
- **acq - Area Central Query config

## Global configurations and constants

#### Positions (`extensibility/src/modules/vessels/config.js`)
- **REFRESH_TIME** - Positions refresh time in millisseconds
- **SCALE** - Map scale at which positions transition to clusters

#### Clusters (`extensibility/src/modules/vessels/cluster/config.js`)
- **COLOR_SCHEMES** - Available color schemes for the clusters
- **REFRESH_TIME** - Clusters refresh time in milliseconds
- **CELL_RADIUS** - Cluster cell radius in pixels
*/

const {buildSymbolizerMenu} = require("./positions/symbolizers/index.js"),
	// {buildTrackQueryPanel} = require("./tracks/query.js"),
	{buildPositionsLayer} = require("./positions/layer.js"),
	{
		getTracks,
		initPositions,
		getLabel,
		getNavigationalStatus,
		getDimensions,
		openQueryPanel,
		toggleFollow,
		getLastKnownPosition
	} = require("./api.js"),
	{mapProperties} = require("./positions/properties.js");

module.exports = (config, permissions) => {
	// Check permissions to see ssn enrichment
	if(!permissions.vessels.ssn_enrichment.available)
		config.ssn_enrichment = false;

	// Check permissions to see ssn enrichment show more button
	if(permissions.vessels.ssn_enrichment.showmore)
		config.ssn_enrichment_showmore = true;

	// Check permissions to see lrit in query
	if (!permissions.lrit.available)
		config.lrit = false;

	// Check permissions to download ga_plan
	if (!permissions.vessels.ga_plan)
		config.ga_plan = false;

	return {
		name: "Vessels",
		layers: [
			{
				id: "vessels",
				name: "Vessels",
				layers: [
					require("./tracks/layer.js"),
					buildPositionsLayer(config)
				],
				visible: false
			}
		],
		preferences: {
			"show.deadReckoning": false
		},
		init() {
			// // Cluster config
			// require("./cluster").init();

			initPositions();

			const positionsLayer = seg.layer.get("positions");

			// Adding Show option on top-right-menu with Dead Reckoning
			// seg.ui.preferences.addSectionToTab("Show", {
			// 	alias: "vesselShow",
			// 	template: `
			// 		<div class="row" style="margin-top: 8px"><strong class="text-size-sm">Vessels</strong></div>
			// 		<div class="content">
			// 			<div class="row">
			// 				<toggle ng-model="show.vesselModel" ng-model-options="{getterSetter: true}"></toggle>
			// 				<label class="text-size-sm flex-1">Vessel Models</label>
			// 			</div>
			// 			<div class="row">
			// 				<toggle ng-model="vesselShow.deadReckoning" ng-model-options="{getterSetter: true}"></toggle>
			// 				<label class="text-size-sm flex-1">Dead Reckoning</label>
			// 			</div>
			// 		</div>
			// 	`,
			// 	bindings: {
			// 		deadReckoning(val) {
			// 			if (typeof val === "undefined")
			// 				return seg.preferences.workspace.get("show.deadReckoning");

			// 			seg.preferences.workspace.set("show.deadReckoning", val);
			// 			positionsLayer.getSource().changed();
			// 		},
			// 		vesselModel(val) {
			// 			if(typeof val === "undefined")
			// 				return seg.preferences.workspace.get("show.vesselModel");

			// 			seg.preferences.workspace.set.setWorkspacePreference("show.vesselModel", val);
			// 			positionsLayer.getSource().changed();
			// 		}
			// 	}
			// });
			// 
			// 
			// // Configuration to create or not the combobox for maxPositionCount threshold
			// // please refer to extensibility/src/config.json
			// // if(seg.CONFIG.getValue("vessels.positions.THRESHOLD_COMBO_ACTIVE"))
			// // 	seg.ui.preferences.addSectionToTab("Cluster", {
			// // 		alias: "maxPositionsCount",
			// // 		template: `
			// // 			<div class="row">
			// // 				<label class="text-size-sm flex-1">Max number of vessels before clustering (the greater the value, the slower SEG will be)</label>
			// // 			</div>
			// // 			<div class="row">
			// // 				<combo ng-model="maxPositionsCount.value" options="option for option in maxPositionsCount.options" ng-model-options="{getterSetter: true}"></combo>
			// // 			</div>`,
			// // 		bindings: {
			// // 			options: seg.CONFIG.getValue("vessels.positions.THRESHOLD_OPTIONS"),
			// // 			value(val) {
			// // 				if (typeof val === "undefined")
			// // 					return seg.preferences.workspace.get("show.maxPositionsCount");

			// // 				seg.preferences.workspace.set("show.maxPositionsCount", val);
			// // 				positionsLayer.getSource().changed();
			// // 			}
			// // 		}
			// // 	});

			// // seg.preferences.workspace.set("show.maxPositionsCount", seg.CONFIG.getValue("vessels.positions.THRESHOLD_DEFAULT"));
			
			//symbolizers menu
			// const positionsSymbolizers = buildSymbolizerMenu(config.symbolizers);
			// if (!config.ssn_enrichment)	{
			// 	const index = positionsSymbolizers.options.findIndex(opt => opt.alias === "Voyage Information");
			// 	if(index > -1)
			// 		positionsSymbolizers.options.splice(index, 1);
			// }

			// const positionsFilters = seg.ui.layerMenu.fromFilters(positionsLayer).map(filterMenuItem => {
			// 	if (filterMenuItem.alias === "User Risk Status")
			// 		filterMenuItem.info = {
			// 			title: "User Risk Status",
			// 			templateUrl: resolvePath("./positions/templates/user_risk_status.tooltip.html")
			// 		};

			// 	if (
			// 		(filterMenuItem.alias === "Gear Type") ||
			// 		(filterMenuItem.alias === "Flag") ||
			// 		(filterMenuItem.alias === "FAO Types") ||
			// 		(filterMenuItem.alias === "ICCAT")
			// 	)
			// 		filterMenuItem = seg.ui.layerMenu.buildMultiLevelMenu(filterMenuItem, 0);

			// 	return filterMenuItem;
			// });

			// const positionsMenu = seg.ui.layerMenu.fromLayer("Positions", positionsLayer);
			// positionsMenu.options = positionsMenu.options.concat(positionsFilters);

			// // Create Tracks Menu Items
			// const tracksLayer = seg.layer.get("tracks"),
			// 	tracksMenu = seg.ui.layerMenu.fromLayer("Tracks", tracksLayer),
			// 	tracksFilters = seg.ui.layerMenu.fromFilters(tracksLayer);

			// Object.assign(tracksMenu, {
			// 	options: tracksFilters[0].options,
			// 	info: {
			// 		title: "Tracks",
			// 		templateUrl: resolvePath("./tracks/templates/symbologies.html"),
			// 		bindings: {
			// 			sections: require("./tracks/symbologies.js")
			// 		}
			// 	}
			// });

			// tracksMenu.configurableOptions.push("width", "color");
			// // track width configuration
			// seg.ui.layerMenu.registerConfig("width", {
			// 	template: "<div class=\"flexbox flex-align-center\"><label class=\"text-size-sm font-weight-400\">Width</label><slider min=\"1\" max=\"20\" class=\"flex-1\" ng-model=\"value\" round=\"2\" ng-change=\"setWidth()\"></slider><input ng-model=\"value\" class=\"seg text-size-sm\"></div>",
			// 	bindings: {
			// 		value: seg.preferences.map.get("vessels.tracks.width", this.value),
			// 		setWidth() {
			// 			seg.preferences.map.set("vessels.tracks.width", this.value);
			// 			seg.layer.get("tracks").refresh();
			// 		}
			// 	}
			// });

			// // track color configuration
			// seg.ui.layerMenu.registerConfig("color", {
			// 	template: "<div class=\"flexbox flex-align-center\"><label class=\"text-size-sm font-weight-400\">Color</label><colorpicker ng-model=\"value\" ng-change=\"setColor()\"></colorpicker></div>",
			// 	bindings: {
			// 		value: seg.preferences.map.get("vessels.tracks.color", this.value) ? seg.preferences.map.get("vessels.tracks.color", this.value) : "#000",
			// 		setColor() {
			// 			seg.preferences.map.set("vessels.tracks.color", this.value);
			// 			seg.layer.get("tracks").refresh();
			// 		}
			// 	}
			// });

			// // Vessels Menu Creation
			// const vesselsMenu = seg.ui.layerMenu.fromLayer("VESSELS", seg.layer.get("vessels"));
			// vesselsMenu.options = [positionsSymbolizers, positionsMenu, tracksMenu];
			// seg.ui.layerMenu.register(vesselsMenu);

			// // Area Central Query
			// seg.ui.queryPanel.add(buildTrackQueryPanel(config));
			// seg.ui.queryPanel.ACQ.register(require("./tracks/acq.js")(config.acq));

			// // SSN Enrichment
			// if (config.ssn_enrichment)
			// 	seg.ui.queryPanel.add(require("./isp/index.js"));

			// // Advanced Search
			// seg.ui.searchAdvanced.addPanel(require("./positions/search.js")(config.advanced_search));

			// Shortcuts
			seg.shortcuts.add("tracks", []);
			seg.shortcuts.add("positions", [
				{
					label: "Get Track",
					callback(features) {
						if (features) {
							const trackOptions = {
								from: moment().subtract(1,"d"),
								to: moment(),
								vessels: features
							};

							return getTracks(trackOptions).then(trackTimeline => seg.timeline.set({
								delimiters: [
									trackOptions.from,
									trackOptions.to
								],
								groups: [trackTimeline]
							}), e => 
								console.log(e)
							);//seg.log.error("ERROR_VESSELS_GET_TRACKS_THEN", e + ": " + e));
						}
					},
					ellipsisCallback: function () {
						openQueryPanel();
					},
					filter(selectedSources) {
						return selectedSources.every(source => source.name === "Vessels");
					}
				},
				{
					label: "See in TTT",
					callback(retrievedVessels) {
						let vesselTable = seg.ui.ttt.tables.get("Vessels");

						if(!vesselTable)
							vesselTable = {
								label: "Vessels",
								customToolbar: {
									template: "<span class=\"text-size-sm\">Number of vessels: <b>{{grid.filteredRows.length}}</b></span>"
								},
								gridAPI: {
									getNavigationalStatus,
									getDimensions
								},
								data: [],
								itemAs: "vessel",
								fields: mapProperties({
									override: {
										mmsi: {visible: true},
										imo: {visible: true},
										ir: {visible: true},
										shipName: {visible: true},
										callSign: {visible: true},
										speed: {visible: true},
										lat: {visible: true},
										lon: {visible: true},
										flagState: {visible: true},
										ns: {visible: true},
										thetisDescription: {visible: true}
									}
								})
							};

						seg.ui.ttt.tables.open(vesselTable);
						seg.ui.ttt.tables.addDataToTable(vesselTable, retrievedVessels);
					},
					filter(selectedSources) {
						return selectedSources.every(source => source.name === "Vessels");
					}
				}
			]);

			// LRIT Requests
			// if (config.lrit){
			// 	seg.countries.getCountriesRequest("MEMBER_STATE").then(() => {
			// 		seg.ui.queryPanel.add(require("./lrit")(permissions));
			// 	}, e => seg.log.error("ERROR_VESSELS_GET_COUNTRIES_REQUEST_THEN", e.error + ": " + e.result));
			// }

			//register position features as TOIs
			// seg.favourites.registerTOItype({
			// 	source: positionsLayer.getSource(),
			// 	label: getLabel
			// });

			// Label preferences on top-right-menu
			// require("./positions/label_preferences.config.js").init();

			// if(config.track_import)
				// seg.ui.apps.add(require("../apps/import")(require("./tracks/import")(vesselsMenu)));

			// Avoid selecting AOI instead of vessel or tracks, during an ACQ, for instance.
			seg.map.registerPreferentialLayers("positions");
			seg.map.registerPreferentialLayers("tracks");

			// const followedVesselId = seg.preferences.workspace.get("following.featureId");
			// if(followedVesselId){
			// 	seg.modal.openModal("followVessel", "Retrieving followed vessel from previous session...", {class: "info"});
			// 	getLastKnownPosition({id: followedVesselId}).then(followedVessel => {
			// 		toggleFollow(followedVessel);
			// 	});
			// }

			// positionsMenu.options.forEach(({bindings}) => {
			// 	bindings.filter.afterSetActive && bindings.filter.afterSetActive();
			// });
		}
	};
};
