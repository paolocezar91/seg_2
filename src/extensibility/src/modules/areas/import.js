let importedAreasMenu;

// Sometimes imported features maycome as multypoligon. We need to split them into their own features.
const splitMultiPolygon = (features, key) => {
	const multiPolygonFeatures = [];

	features.forEach((feature) => {
		const geom = feature.geometry;
		const props = feature.properties;
		if (geom.type === "MultiPolygon"){
			for (let i = 0; i < geom.coordinates.length; i++){
				multiPolygonFeatures.push({
					id: geom.coordinates.length > 1 ? feature.id + "_" + (i + 1) : feature.id,
					type: "Feature",
					properties: Object.assign({}, props, {
						[key]: geom.coordinates.length > 1 ? props[key] + "_" + (i + 1) : props[key]
					}),
					geometry: {
						coordinates: geom.coordinates[i],
						type: "Polygon"
					}
				});
			}
		} else {
			multiPolygonFeatures.push(feature);
		}
	});

	return multiPolygonFeatures;
};

module.exports = (areasMenuItem) => {
	return {
		label: "Area",
		featureAs: "area",
		sourceOptions:["File"],
		defaultValue: {
			properties: {},
			geometry: {
				coordinates: [0, 0]
			}
		},
		toModel: function(){
			return;
		},
		importFromService: async file => {
			const params = {
				// minX: 8,
				// maxX: 9,
				// minY: 49,
				// maxY: 50,
			};

			return seg.request.post("v1/import/geojson", Object.assign({params}, {form: {file}, headers: {"Content-Type": undefined} }))
				.then(({result}) => {
					const key = "VME_ID";
					result.features = splitMultiPolygon(result.features, key);

					const features = (new ol.format.GeoJSON()).readFeatures(result, {
							featureProjection: seg.map.getProjection()
						}), // formatting features
						importedAreasLayer = seg.layer.get("imported_areas") || seg.layer.create({
							id: "imported_areas",
							name: "IMPORTED AREAS",
							visible: true,
							type: "Vector",
							source: {
								type: "Vector",
								projection: "EPSG:4326"
							},
							style: require("./import/style.js"),
							filters: [{
								name: "IMPORTED AREAS",
								options: []
							}]
						}), // creating or getting the imported areas layer
						importedAreasLayerSource = importedAreasLayer.getSource();

					features.forEach(f => f.getGeometry().set("type", "polygon"));
					importedAreasLayerSource.addFeatures(features); // add features to layer
					seg.selection.selectFeatureAndCenter(features); // centering and selecting feature(s)

					// Creates filters config based on the polygons that were imported
					const importedFilters = importedAreasLayerSource.getFeatures().reduce((acc, ft) => Object.assign(acc, {
						[ft.get(key) || ft.getId()]: ((area) => area.getId() === ft.getId())
					}), {});

					seg.layer.filter.setFilterOptions(importedAreasLayer.get("filters")[0], importedFilters);

					if (importedAreasMenu)
						areasMenuItem.options.pop();

					importedAreasMenu = seg.ui.layerMenu.fromFilters(importedAreasLayer)[0];

					importedAreasMenu.options = importedAreasMenu.options.sort((a, b) => {
						if (a.alias > b.alias) return 1;
						if (a.alias < b.alias) return -1;
						return 0;
					});

					areasMenuItem.options.push(importedAreasMenu);

					if (!seg.layer.get("areas").findLayerById("imported_areas"))
						seg.layer.get("areas").addLayer(importedAreasLayer); // adding imported areas layer to area layers

					seg.ui.cleanables.add("Clear Imported Areas", () => { // adding cleanable for imported areas
						const importedAreasFeatures = importedAreasLayerSource.getFeatures();
						importedAreasFeatures.forEach(feature => importedAreasLayerSource.removeFeature(feature));
						areasMenuItem.options.pop();
						importedAreasMenu = undefined;
					});

					return true;
				}, e => seg.log.error("ERROR_AREAS_IMPORT_FROM_SERVICES", e.error + ": " + e.result));
		}
	};
};