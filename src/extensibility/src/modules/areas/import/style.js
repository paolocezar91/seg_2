module.exports = feature => {
	const isEditing = feature.get("isEditing");
	let	strokeColorStyle = typeof feature.get("strokeColorStyle") !== "undefined" ? feature.get("strokeColorStyle") : "#3399CC",
		strokeWidthStyle = typeof feature.get("strokeWidthStyle") !== "undefined" ? feature.get("strokeWidthStyle") : 1.25,
		strokeOpacityStyle = typeof feature.get("strokeOpacityStyle") !== "undefined" ? feature.get("strokeOpacityStyle") : 1;

	if(feature.get("selected") && !isEditing){
		strokeColorStyle = "rgb(255, 255, 0)";
		strokeWidthStyle = 2.5;
		strokeOpacityStyle = 1;
	}
	else if(seg.favourites.isFavourite(feature))
		strokeColorStyle = seg.favourites.getGroupFromFeature(feature).color || "rgb(201, 151, 38)";

	strokeColorStyle = ol.color.asArray(strokeColorStyle);
	strokeColorStyle = strokeColorStyle.slice();
	strokeColorStyle[3] = strokeOpacityStyle;

	const hexColor = typeof feature.get("fillColorStyle") !== "undefined" ? feature.get("fillColorStyle") : "rgba(255,255,255, 0.4)";
	let fillColorStyle = ol.color.asArray(hexColor);
	fillColorStyle = fillColorStyle.slice();
	fillColorStyle[3] = typeof feature.get("fillOpacityStyle") !== "undefined" ? feature.get("fillOpacityStyle") : 0.4;

	return new ol.style.Style({
		fill: fillColorStyle[3] === 0 ? null : new ol.style.Fill({
			color: fillColorStyle
		}),
		stroke: strokeColorStyle[3] === 0 ? null : new ol.style.Stroke({
			color: strokeColorStyle,
			width: strokeWidthStyle
		})
	});
};
