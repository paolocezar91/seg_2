const initCGDAreas = async (params, createHoles) => {
		if (!params)
			return;

		let requests = [], cgdAreasFeatures = [];
		const modal = seg.modal.openModal("Loading areas", "Loading areas...", {class: "info"}),
			cgdAreaLayer = seg.layer.get("cgd_areas");

		// passing params object with key and array of values to be queried we can get all areas. Example:
		// This will create a request for each value of the array with the key "layer_name"
		// params: {
		// 	layer_name: [
		// 		"Exclusion Area 2",
		// 		"Exclusion Area 3",
		// 		"Exclusion Area 4",
		// 		"Exclusion Area 5",
		// 		"Exclusion Area 6",
		// 		"Exclusion Area 7",
		// 		"Exclusion Area 8",
		// 		"Exclusion Area 9",
		// 		"Exchange Area 10",
		// 		"Exchange Area 11"
		// 	]
		// }

		Object.entries(params).forEach(([key, values]) => {
			requests = requests.concat(createAreaRequest(key, values).map(request => request.then(res => {
				if (!res.result || !(res.result.features && res.result.features.length))
					return;

				const paramsAreaFeature = (new ol.format.GeoJSON()).readFeatures(res.result, {
					featureProjection: seg.map.getProjection()
				});

				if (createHoles && createHoles.find(i => i === key))
					paramsAreaFeature[0].set("params", key);

				cgdAreasFeatures = cgdAreasFeatures.concat(paramsAreaFeature);
			}, e => seg.log.error("ERROR_AREAS_CREATE_AREA_REQUEST", e.error + ": " + e.result))));
		});

		seg.promise.all(requests).then(() => {
			// For certain CGD areas we need to create wholes in their intersection (IVTMIS Exclusion and Exchange Areas)
			if (createHoles)
				createHoles.forEach(key => {
					seg.geometry.createHolesInGeometry(cgdAreasFeatures.filter(feature => feature.get("params") === key));
				});

			cgdAreaLayer.getSource().addFeatures(cgdAreasFeatures);
			modal.setContent("Areas loaded successfully.");
			modal.setClass("success");
			seg.modal.closeTimeout(modal.title);
		}, (e) => {
			modal.setContent("Failed to load cgdAreas.");
			modal.setClass("error");

			seg.modal.closeTimeout(modal.title);
			seg.log.error("ERROR_AREAS_LOAD_CGD_AREAS", e.error + ": " + e.result);
		});
	},
	createAreaRequest = (key, values) => {
		return values.map(value => seg.request.get("v1/eo/wfs/cgdAreas", {
			params: {
				[key]: value
			}
		}));
	},
	initStations = () => {
		return seg.request.get("v1/stations").then(res => {
			const baseStationsSource = seg.layer.get("base_stations").getSource(),
				stations = res.result.features.map(station => new ol.Feature(Object.assign(station.properties, {
					geometry: new ol.geom.Point(ol.proj.fromLonLat(station.geometry.coordinates, seg.map.getProjection()))
				})));

			baseStationsSource.addFeatures(stations);

			seg.ui.ttt.tables.register({
				customToolbar: {
					template: "<span class=\"text-size-sm\">Number of AIS Coastal stations: <b>{{grid.filteredResults.length}}</b></span>"
				},
				label: " AIS Coastal stations",
				data: stations,
				itemAs: "station",
				fields: {
					bsid: {
						label: "AIS Aerial Location",
						template: "{{station.get('aisAerialLocation')||N/A}}",
						visible: true
					},
					latitude: {
						label: "Latitude",
						template: "<span coordinate=\"[station.get('latitude'), 'lat']\"></span>",
						visible: true
					},
					longitude: {
						label: "Longitude",
						template: "<span coordinate=\"[station.get('longitude'), 'lon']\"></span>",
						visible: true
					},
					gridRef: {
						label: "Grid Reference",
						template: "{{station.get('gridRef') || \"N/A\"}}",
						visible: true
					},
					heightAboveGroundLevel: {
						label: "Height above Ground Level",
						template: "{{station.get('heightAboveGroundLevel')||N/A}}",
						visible: true
					},
					directionOfAe: {
						label: "Direction of Ae",
						template: "{{station.get('directionOfAe')||N/A}}",
						visible: true
					},
					mmsi: {
						label: "MMSI",
						template: "{{station.get('mmsi')||N/A}}",
						visible: true
					}
				},
				onSelectionChange(changedRows, selectedRows) {
					seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
				}
			});
		}, e => seg.log.error("ERROR_AREAS_INIT_STATIONS", e.error + ": " + e.result));
	},
	//counter is the menu level, so it can be limited not to render all levels
	createMenuItem = (areaLayer) => {
		const areaHasSupportedLayers = hasSupportedLayers(areaLayer);
		let menuItem;

		// if supported layers, create WMSLayer item
		if (areaHasSupportedLayers){
			menuItem = seg.ui.layerMenu.fromWMSLayer(areaLayer);
			let legends;
			const visibleLegends = [];

			const setVisibleLegends = () => {
				if (areaHasSupportedLayers)
					legends = buildLegendFromWMSLayer(areaLayer);
				else
					legends = areaLayer.getLayers().reduce((results, layer) => {
						if (isImageLayer(layer) && isVisible(layer))
							results.push(...buildLegendFromWMSLayer(layer));
						return results;
					}, []);
				visibleLegends.push(...legends);
			};

			setVisibleLegends();

			areaLayer.ol_.on("change", () => {
				//need to always return the same object
				visibleLegends.splice(0);
				setVisibleLegends();
			});

			menuItem.info = {
				title: areaLayer.get("name"),
				disabled() {
					if (areaHasSupportedLayers)
						return !isVisible(areaLayer);
					return !areaLayer.getLayers().some(layer => isImageLayer(layer) && isVisible(layer));
				},
				bindings: {
					legends: visibleLegends
				},
				template: `
					<div class="flexbox flex-direction-vertical areas-legend flex-wrap">
						<div ng-repeat="legend in legends track by legend.alias" class="flexbox">
							<img ng-src="{{legend.url}}">
							<span>{{legend.alias}}</span>
						</div>
					</div>
					<style>
					.areas-legend > div {
						min-width: 200px;
						margin-right: 10px;
						margin-bottom: 10px;
					}

					.areas-legend > div > span {
						margin-left: 10px;
					}
					</style>
				`
			};
		} else //else create single layer
			menuItem = seg.ui.layerMenu.fromLayer(areaLayer.get("name"), areaLayer);

		if (areaLayer.getLayers) //recursive call for child layers
			menuItem.options = areaLayer.getLayers().map(createMenuItem);

		return menuItem;
	},
	buildLegendFromWMSLayer = (wmsLayer) => {
		const url = wmsLayer.getSource().getUrl();
		return [].concat(wmsLayer.getSource().get("supportedLayers")).reduce((legends, supportedLayer) => {
			if (supportedLayer.enabled)
				return legends.concat({
					alias: supportedLayer.alias,
					url: url + "?SERVICE=WMS&LAYER=" + supportedLayer.name + "&FORMAT=image/png&REQUEST=GetLegendGraphic"
				});
			return legends;
		}, []);
	},
	mergeFilterAndWMSOptions = (toMerge) => {
		return toMerge.reduce((options, [wms, filter]) => {
			// wms and filter menu items are siblings, but we need to merge the wms options into the filter options
			if (filter){
				// We use this hook to link parent filter menu item to "wms_layer type" menu item and toggle the visibility of its layer accordingly.
				filter.bindings.filter.afterSetActive = () => {
					filter.options.forEach(opt => {
						if(opt.type === "wms_layer"){
							seg.preferences.map.set("layers." + opt.layer.get("id") + ".visible", filter.bindings.filter.getActive());
							opt.layer.ol_.setVisible(filter.bindings.filter.getActive());
						}
					});
					// wms.options.find(opt => opt.name === option.alias).setActive(option.bindings.filter.getActive());
				};
				filter.options = [].concat(filter.options, wms.options);
				options.push(filter);
			} else {
				// some cases don't require merging, if there are no colliding filter siblings
				options.push(wms);
			}
			return options;
		}, []);
	},
	hasSupportedLayers = (layer) => layer.getSource && layer.getSource().get("supportedLayers"),
	isImageLayer = (layer) => layer.getSource && layer.getSource().getUrl && layer.getSource().getUrl(),
	isVisible = (layer) => layer.getVisible() && layer.getSource().getParams().LAYERS && layer.getSource().getParams().LAYERS.length;

module.exports = {
	initStations,
	initCGDAreas,
	createMenuItem,
	mergeFilterAndWMSOptions
};