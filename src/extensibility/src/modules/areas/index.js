/**
@namespace extensibility.modules.areas
@description
# Areas

## Available options
- **ms_specific** - Include MS Specific areas. It is an array that can contain any of the following strings:
	- "malta_layers"
	- "italy_layers"
	- "netherlands_layers"
	- "portugal_layers"
	- "ireland_layers"
	- "sweden_layers"
	- "uk_layers"
	- "eu_layers"
- **fisheries** - Include fisheries areas. It is an array that can contain any of the following strings:
	- "general_fishery"
	- "global_fao_areas"
	- "data_filter_area"
	- "rmfo_specific_areas"
	- "NEAFC"
	- "north_sea_ww"
	- "ww_pelagic"
	- "baltic_sea"
	- "NAFO"
	- "tuna_farms"
- **eunavfor** - true/false - Include *EUNAVFOR* areas
- **area_import** - App for importing areas from shapefiles

Omitting any of the keys above causes the respective layer to not be included. For example, if you don"t want the operation to have *fisheries areas*, pass an options object to the areas module without a **fisheries** property. If you pass an empty array, an **empty** Fisheries layer group will be added.

## Global configurations and constants
None

## Adding new areas layers
Create a new {@tutorial layer_anatomy Layer configuration file} and require it in the Areas module **layers** property.
*/

let config,
	areasLayer,
	presetCGDMenu = [],
	CSVCGDdMenu = [],
	cgdMenu;

const {initStations, initCGDAreas, createMenuItem, mergeFilterAndWMSOptions} = require("./api"),
	{buildCSVFilters, setFiltersPreferences, removeParentFilterNames, getPresetFilters} = require("./cgd/filters");

const lazyLoader = async () => {
	if (!areasLayer.getVisible())
		return;

	if (!areasLayer.get("loading")) {
		areasLayer.set("loading", true);

		// REGULAR COUNTRIES AREAS and CSV AREAS area loaded using the same endpoint
		if (config.cgd && config.cgd.features) {
			initCGDAreas(config.cgd.features.params, config.cgd.features.createHoles);
		}

		if (seg.layer.get("base_stations"))
			initStations();
	}
};

module.exports = _config => {
	config = _config;

	return {
		name: "Areas",
		layers: [{
			id: "areas",
			order: 5,
			name: "AREAS",
			layers: config.layers || [],
			visible: false
		}],
		async init() {
			areasLayer = seg.layer.get("areas");
			const areasMenuItem = createMenuItem(areasLayer);

			if (config.cgd) {
				const cgdLayer = seg.layer.create(require("./cgd/layer")(config.cgd));

				// REGULAR COUNTRIES AREAS
				if(config.cgd.filters.preset_filters){
					seg.layer.filter.addFilters(cgdLayer, getPresetFilters(config.cgd.filters));
					presetCGDMenu = seg.ui.layerMenu.buildMultiLevelMenu(seg.ui.layerMenu.fromFilters(cgdLayer)[0]).options;

					const denmark = presetCGDMenu.find(filter => filter.alias === "AREAS Denmark");
					if(denmark){
						denmark.options[0].options[1].alias = denmark.options[0].options[1].alias.replace("-",".");
						denmark.options[0].options[1].bindings.filter.alias = denmark.options[0].options[1].bindings.filter.alias.replace("-",".");
					}
				}

				if (config.cgd.filters.csv_files) { // EFCA CSV NIGHTMARE
					const cgdFilters = await buildCSVFilters(config.cgd.filters);
					seg.layer.filter.addFilters(cgdLayer, cgdFilters);
					CSVCGDdMenu = seg.ui.layerMenu.fromFilters(cgdLayer);
					CSVCGDdMenu = CSVCGDdMenu[0];
					// SPECIFIC CASE FOR EFCA FAO/ICES AREA FILTERS
					if (CSVCGDdMenu.options.some(f => f.alias.includes("FAO/ICES"))) {
						const faoFilters = Object.assign({}, CSVCGDdMenu);
						faoFilters.options = CSVCGDdMenu.options.filter(f => f.alias.includes("FAO/ICES"));

						const otherFilters = Object.assign({}, CSVCGDdMenu);
						otherFilters.options = CSVCGDdMenu.options.filter(f => !f.alias.includes("FAO/ICES"));

						CSVCGDdMenu = [
							...seg.ui.layerMenu.buildMultiLevelMenu(faoFilters, 1).options,
							...seg.ui.layerMenu.buildMultiLevelMenu(otherFilters).options
						];
					} else
						CSVCGDdMenu = seg.ui.layerMenu.buildMultiLevelMenu(CSVCGDdMenu).options;
				}

				cgdMenu = []
					.concat(presetCGDMenu)
					.concat(CSVCGDdMenu)
					.map(opt => setFiltersPreferences(opt))
					.map(opt => removeParentFilterNames(opt, ["AREAS"]));

				if(config.mergeFilterAndWMSOptions){
					const toMerge = areasMenuItem.options.reduce((ntuplets, areasMenuItemOption) => {
						const foundCGDMenuOption = cgdMenu.find(cgdMenuOption => {
							return areasMenuItemOption.alias.includes(cgdMenuOption.alias);
						});

						if (foundCGDMenuOption)
							ntuplets.push([areasMenuItemOption, foundCGDMenuOption]);
						else
							ntuplets.push([areasMenuItemOption]);

						return ntuplets;
					}, []);

					cgdMenu = mergeFilterAndWMSOptions(toMerge);
					areasMenuItem.options = [].concat(cgdMenu);
				} else {
					areasMenuItem.options.push(...[].concat(cgdMenu));
				}

				areasLayer.addLayers(cgdLayer);
			}

			// Importing areas from shapefiles
			if(config.area_import)
				seg.ui.apps.add(require("../apps/import")(require("./import")(areasMenuItem)));

			//order by alphabetic order
			areasMenuItem.options = areasMenuItem.options.sort((a, b) => {
				if (a.alias > b.alias) return 1;
				if (a.alias < b.alias) return -1;
				return 0;
			});

			seg.ui.layerMenu.register(areasMenuItem);

			areasLayer.ol_.on("change:visible", lazyLoader);
			lazyLoader();
		}
	};
};