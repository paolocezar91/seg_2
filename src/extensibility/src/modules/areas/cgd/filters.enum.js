const Albania = {
	name: "Albania",
	fn: area => area.get("CBR_ALPHA2CODE") === "AL",
	options:[{
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"AL_BASELINE": area => area.get("DESCRIPTION") === "AL_BASELINE"
		}
	}]
};

// const Algeria = {
// 	name: "Algeria",
// 	fn: area => area.get("CBR_ALPHA2CODE") === "DZ",
// 	options:[{
// 		name: "Oilspills Alert Areas",
// 		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
// 		options: {
// 			"AL_BASELINE": area => area.get("DESCRIPTION") === "DZ_BASELINE"
// 		}
// 	}]
// };

const Azerbeijan = {
	name: "Azerbeijan",
	fn: area => area.get("CBR_ALPHA2CODE") === "AZ",
	options:[{
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"AL_BASELINE": area => area.get("DESCRIPTION") === "AZ_BASELINE"
		}
	}]
};

const Belgium = {
	name: "Belgium",
	fn: area => area.get("CBR_ALPHA2CODE") === "BE",
	options:[{
		name: "EO Coverage",
		fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
		options: {
			"Belgium": area => area.get("AUTHORITY") === "Belgium"
		}
	}, {
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"BE_BASELINE": area => area.get("DESCRIPTION") === "BE_BASELINE",
			"BE_QZJR_NL": area => area.get("DESCRIPTION") === "BE_QZJR_NL",
			"BE_QZJR_FR": area => area.get("DESCRIPTION") === "BE_QZJR_FR",
			"BE_QZJR_UK": area => area.get("DESCRIPTION") === "BE_QZJR_UK"
		}
	}]
};

const BonnAgreement = {
	name: "Bonn Agreement",
	fn: area => area.get("CBR_ALPHA2CODE") === "XA",
	options:[{
		name: "EO Coverage",
		fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
		options: {
			"Tour DHorizon Support": area => area.get("AUTHORITY") === "Tour DHorizon Support"
		}
	}, {
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"BA_BASELINE": area => area.get("DESCRIPTION") === "BA_BASELINE"
		}
	}]
};

const Bulgaria = {
	name: "Bulgaria",
	fn: area => area.get("CBR_ALPHA2CODE") === "BG",
	options:[{
		name: "EO Coverage",
		fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
		options: {
			"Bulgaria": area => area.get("AUTHORITY") === "Bulgaria"
		}
	}, {
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"BG_BASELINE": area => area.get("DESCRIPTION") === "BG_BASELINE"
		}
	}]
};

const Croatia = {
	name: "Croatia",
	fn: area => area.get("CBR_ALPHA2CODE") === "HR",
	options:[{
		name: "EO Coverage",
		fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
		options: {
			"Croatia": area => area.get("AUTHORITY") === "Croatia"
		}
	}, {
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"HR_BASELINE": area => area.get("DESCRIPTION") === "HR_BASELINE"
		}
	}]
};

const Cyprus = {
	name: "Cyprus",
	fn: area => area.get("CBR_ALPHA2CODE") === "CY",
	options:[{
		name: "EO Coverage",
		fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
		options: {
			"Cyprus": area => area.get("AUTHORITY") === "Cyprus"
		}
	}, {
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"CY_BASELINE": area => area.get("DESCRIPTION") === "CY_BASELINE"
		}
	}]
};

const Denmark = {
	name: "Denmark",
	fn: area => area.get("CBR_ALPHA2CODE") === "DK",
	options:[{
		name: "EO Coverage",
		fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
		options: {
			"Greenland Middle East": area => area.get("DESCRIPTION") === "Greenland Middle East",
			"Greenland South East": area => area.get("DESCRIPTION") === "Greenland South East",
			"Greenland South West": area => area.get("DESCRIPTION") === "Greenland South West",
			"Denmark_northsea": area => area.get("DESCRIPTION") === "Denmark_northsea",
			"HELCOM 1": area => area.get("DESCRIPTION") === "HELCOM 1"
		}
	}, {
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"DK_BASELINE": area => area.get("DESCRIPTION") === "DK_BASELINE",
			"DK_GREENLAND": area => area.get("DESCRIPTION") === "DK_GREENLAND"
		}
	}]
};

const Estonia = {
	name: "Estonia",
	fn: area => area.get("CBR_ALPHA2CODE") === "EE",
	options:[{
		name: "EO Coverage",
		fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
		options: {
			"HELCOM-A3-GOF": area => area.get("AUTHORITY") === "HELCOM-A3-GOF",
			"HELCOM2": area => area.get("AUTHORITY") === "HELCOM2"
		}
	}, {
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"EE_BASELINE": area => area.get("DESCRIPTION") === "EE_BASELINE"
		}
	}]
};

const Finland = {
	name: "Finland",
	fn: area => area.get("CBR_ALPHA2CODE") === "FI",
	options:[{
		name: "EO Coverage",
		fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
		options: {
			"HELCOM-A3-GOF": area => area.get("AUTHORITY") === "HELCOM-A3-GOF",
			"HELCOM-A3-BoB": area => area.get("AUTHORITY") === "HELCOM-A3-BoB"
		}
	}, {
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"FI_BASELINE": area => area.get("DESCRIPTION") === "FI_BASELINE"
		}
	}]
};

const France = {
	name: "France",
	fn: area => area.get("CBR_ALPHA2CODE") === "FR",
	options:[{
		name: "EO Coverage",
		fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
		options: {
			"France Bassas da India": area => area.get("AUTHORITY") === "France Bassas da India",
			"France EPZ": area => area.get("AUTHORITY") === "France EPZ",
			"France Guyane": area => area.get("AUTHORITY") === "France Guyane",
			"France Juan de Nova": area => area.get("AUTHORITY") === "France Juan de Nova",
			"France La Reunion": area => area.get("AUTHORITY") === "France La Reunion",
			"France Martinique Guadeloupe": area => area.get("AUTHORITY") === "France Martinique Guadeloupe",
			"France Mayotte": area => area.get("AUTHORITY") === "France Mayotte",
			"France Saint-Martin Saint-Barthelemy": area => area.get("AUTHORITY") === "France Saint-Martin Saint-Barthelemy",
			"France Tromelin": area => area.get("AUTHORITY") === "France Tromelin",
			"France, Atlantic": area => area.get("AUTHORITY") === "France, Atlantic",
			"France, Channel and North Sea": area => area.get("AUTHORITY") === "France, Channel and North Sea"
		}
	}, {
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"FR_ATLANTIC": area => area.get("DESCRIPTION") === "FR_ATLANTIC",
			"FR_CARAIBES": area => area.get("DESCRIPTION") === "FR_CARAIBES",
			"FR_CHANNEL": area => area.get("DESCRIPTION") === "FR_CHANNEL",
			"FR_MEDITERRANEAN": area => area.get("DESCRIPTION") === "FR_MEDITERRANEAN",
			"FR_GUYANE": area => area.get("DESCRIPTION") === "FR_GUYANE",
			"FR_MOZAMBIQUE": area => area.get("DESCRIPTION") === "FR_MOZAMBIQUE",
			"FR_LAREUNION": area => area.get("DESCRIPTION") === "FR_LAREUNION"
		}
	}]
};

const Germany = {
	name: "Germany",
	fn: area => area.get("CBR_ALPHA2CODE") === "DE",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"Germany_NS_1": area => area.get("AUTHORITY") === "Germany_NS_1",
				"HELCOM 1": area => area.get("AUTHORITY") === "HELCOM 1"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"DE_Alert_Zone": area => area.get("DESCRIPTION") === "DE_ALERT_ZONE",
				"DE_BS_AWZ": area => area.get("DESCRIPTION") === "DE_BS_AWZ",
				"DE_NS_AWZ": area => area.get("DESCRIPTION") === "DE_NS_AWZ"
			}
		}
	]
};
const Georgia = {
	name: "Georgia",
	fn: area => area.get("CBR_ALPHA2CODE") === "GE",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"Georgia": area => area.get("AUTHORITY") === "Georgia"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"GE_BASELINE": area => area.get("DESCRIPTION") === "GE_BASELINE"
			}
		}
	]
};
const Greece = {
	name: "Greece",

	fn: area => area.get("CBR_ALPHA2CODE") === "GR",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"Aegean_sea_1": area => area.get("AUTHORITY") === "Aegean_sea_1",
				"Aegean_sea_2": area => area.get("AUTHORITY") === "Aegean_sea_2",
				"Aegean_sea_3": area => area.get("AUTHORITY") === "Aegean_sea_3",
				"Ionian_sea_1": area => area.get("AUTHORITY") === "Ionian_sea_1"
			}
		},
		{
			name: "Oilspills Alert Areas",

			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"GR_BASELINE": area => area.get("DESCRIPTION") === "GR_BASELINE"
			}
		}
	]
};
const Iceland = {
	name: "Iceland",

	fn: area => area.get("CBR_ALPHA2CODE") === "IS",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"Iceland 1": area => area.get("AUTHORITY") === "Iceland 1",
				"Iceland 2": area => area.get("AUTHORITY") === "Iceland 2",
				"Iceland 3": area => area.get("AUTHORITY") === "Iceland 3",
				"Iceland 4": area => area.get("AUTHORITY") === "Iceland 4",
				"Iceland 5": area => area.get("AUTHORITY") === "Iceland 5",
				"Iceland 6": area => area.get("AUTHORITY") === "Iceland 6"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"IS_BASELINE": area => area.get("DESCRIPTION") === "IS_BASELINE",
				"IS_ALERT_ZONE": area => area.get("DESCRIPTION") === "IS_ALERT.ZONE"
			}
		}
	]
};
const Ireland = {
	name: "Ireland",

	fn: area => area.get("CBR_ALPHA2CODE") === "IE",
	options:[
		{
			name: "Oilspills Alert Areas",

			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"IE_BASELINE": area => area.get("DESCRIPTION") === "IE_BASELINE"
			}
		}
	]
};
// const Israel = {
// 	name: "Israel",
// 	fn: area => area.get("CBR_ALPHA2CODE") === "IL",
// 	options:[
// 		{
// 			name: "Oilspills Alert Areas",
// 			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
// 			options: {
// 				"IL_BASELINE": area => area.get("DESCRIPTION") === "IL_BASELINE"
// 			}
// 		}
// 	]
// };
const Italy = {
	name: "Italy",

	fn: area => area.get("CBR_ALPHA2CODE") === "IT",
	options:[{
		name: "EO Coverage",
		fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
		options: {
			"Italy_1": area => area.get("AUTHORITY") === "Italy_1",
			"Italy_2": area => area.get("AUTHORITY") === "Italy_2",
			"Italy_3": area => area.get("AUTHORITY") === "Italy_3",
			"Italy_4": area => area.get("AUTHORITY") === "Italy_4"
			// "Slovenia": area => area.get("AUTHORITY") === "Slovenia"
		}
	},
	{
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"IT_BASELINE": area => area.get("DESCRIPTION") === "IT_BASELINE"
		}
	}
	]
};
const Jordan = {
	name: "Jordan",

	fn: area => area.get("CBR_ALPHA2CODE") === "JO",
	options:[
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"JO_BASELINE": area => area.get("DESCRIPTION") === "JO_BASELINE"
			}
		}
	]
};
const Latvia = {
	name: "Latvia",

	fn: area => area.get("CBR_ALPHA2CODE") === "LV",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"HELCOM-A3-GOF": area => area.get("AUTHORITY") === "HELCOM-A3-GOF",
				"HELCOM2": area => area.get("AUTHORITY") === "HELCOM2"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"LV_BASELINE": area => area.get("DESCRIPTION") === "LV_BASELINE"
			}
		}
	]
};
const Lithuania = {
	name: "Lithuania",

	fn: area => area.get("CBR_ALPHA2CODE") === "LT",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"HELCOM2": area => area.get("AUTHORITY") === "HELCOM2"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"LT_BASELINE": area => area.get("DESCRIPTION") === "LT_BASELINE"
			}
		}
	]
};
const Malta = {
	name: "Malta",

	fn: area => area.get("CBR_ALPHA2CODE") === "MT",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"Malta": area => area.get("AUTHORITY") === "Malta"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"MT_BASELINE": area => area.get("DESCRIPTION") === "MT_BASELINE"
			}
		}
	]
};
const Montenegro = {
	name: "Montenegro",

	fn: area => area.get("CBR_ALPHA2CODE") === "ME",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"Red Zone": area => area.get("AUTHORITY") === "Red Zone"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"ME_REDZONE": area => area.get("DESCRIPTION") === "ME.REDZONE",
				"ME_YELLOWZONE1": area => area.get("DESCRIPTION") === "ME.YELLOWZONE1",
				"ME_YELLOWZONE2": area => area.get("DESCRIPTION") === "ME.YELLOWZONE2",
				"ME_GREENZONE1": area => area.get("DESCRIPTION") === "ME.GREENZONE1",
				"ME_GREENZONE2": area => area.get("DESCRIPTION") === "ME.GREENZONE2"
			}
		}
	]
};
const Morocco = {
	name: "Morocco",
	fn: area => area.get("CBR_ALPHA2CODE") === "MA",
	options:[
		{
			name: "Oilspills Alert Areas",

			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"MA_WEST": area => area.get("DESCRIPTION") === "MA.WEST",
				"MA_EAST": area => area.get("DESCRIPTION") === "MA.EAST"
			}
		}
	]
};
const Netherlands = {
	name: "Netherlands",
	fn: area => area.get("CBR_ALPHA2CODE") === "NL",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"Netherlands": area => area.get("AUTHORITY") === "Netherlands"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"NL_BASELINE": area => area.get("DESCRIPTION") === "NL_BASELINE"
			}
		}
	]
};

const Norway = {
	name: "Norway",
	fn: area => area.get("CBR_ALPHA2CODE") === "NO",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"Norway": area => area.get("AUTHORITY") === "Norway"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"NO_BASELINE": area => area.get("DESCRIPTION") === "NO_BASELINE"
			}
		}
	]
};

const Poland = {
	name: "Poland",
	fn: area => area.get("CBR_ALPHA2CODE") === "PL",
	options:[
		{
			name: "EO Coverage",

			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"HELCOM 1": area => area.get("AUTHORITY") === "HELCOM 1",
				"HELCOM2": area => area.get("AUTHORITY") === "HELCOM2"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"PL_BASELINE": area => area.get("DESCRIPTION") === "PL_BASELINE"
			}
		}
	]
};

const Portugal = {
	name: "Portugal",
	fn: area => area.get("CBR_ALPHA2CODE") === "PT",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"Portugal": area => area.get("AUTHORITY") === "Portugal",
				"Portugal_Azores": area => area.get("AUTHORITY") === "Portugal_Azores",
				"Portugal_Madeira": area => area.get("AUTHORITY") === "Portugal_Madeira"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"PT_MADEIRA": area => area.get("DESCRIPTION") === "PT_MADEIRA",
				"PT_CONTINENTAL": area => area.get("DESCRIPTION") === "PT_CONTINENTAL",
				"PT_AZOREN": area => area.get("DESCRIPTION") === "PT_AZOREN"
			}
		}
	]
};

const Romania = {
	name: "Romania",
	fn: area => area.get("CBR_ALPHA2CODE") === "RO",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"Romania": area => area.get("AUTHORITY") === "Romania"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"RO_BASELINE": area => area.get("DESCRIPTION") === "RO_BASELINE"
			}
		}
	]
};

const Spain = {
	name: "Spain",
	fn: area => area.get("CBR_ALPHA2CODE") === "ES",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"Spain_ATL_subarea_1": area => area.get("AUTHORITY") === "Spain_ATL_subarea_1",
				"Spain_ATL_subarea_2": area => area.get("AUTHORITY") === "Spain_ATL_subarea_2",
				"Spain_CAN_subarea_1": area => area.get("AUTHORITY") === "Spain_CAN_subarea_1",
				"Spain_CAN_subarea_2": area => area.get("AUTHORITY") === "Spain_CAN_subarea_2",
				"Spain_CAN_subarea_3": area => area.get("AUTHORITY") === "Spain_CAN_subarea_3",
				"Spain_MED_subarea_1": area => area.get("AUTHORITY") === "Spain_MED_subarea_1",
				"Spain_MED_subarea_2": area => area.get("AUTHORITY") === "Spain_MED_subarea_2",
				"Spain_MED_subarea_3": area => area.get("AUTHORITY") === "Spain_MED_subarea_3"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"ES_CANARY": area => area.get("DESCRIPTION") === "ES_CANARY",
				"ES_ATLANTIC": area => area.get("DESCRIPTION") === "ES_ATLANTIC",
				"ES_MEDITERRANEAN": area => area.get("DESCRIPTION") === "ES_MEDITERRANEAN"
			}
		}
	]
};

const Slovenia = {
	name: "Slovenia",
	fn: area => area.get("CBR_ALPHA2CODE") === "SI",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"Slovenia": area => area.get("AUTHORITY") === "Slovenia"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"SI_BASELINE": area => area.get("DESCRIPTION") === "SI_BASELINE"
			}
		}
	]
};

const Sweden = {
	name: "Sweden",
	fn: area => area.get("CBR_ALPHA2CODE") === "SE",
	options:[
		{
			name: "EO Coverage",
			fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
			options: {
				"HELCOM-A3-BoB": area => area.get("AUTHORITY") === "HELCOM-A3-BoB",
				"HELCOM-A3-GOF": area => area.get("AUTHORITY") === "HELCOM-A3-GOF",
				"HELCOM 1": area => area.get("AUTHORITY") === "HELCOM 1",
				"HELCOM2": area => area.get("AUTHORITY") === "HELCOM2"
			}
		},
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"SE_BASELINE": area => area.get("DESCRIPTION") === "SE_BASELINE"
			}
		}
	]
};

const Tunisia = {
	name: "Tunisia",

	fn: area => area.get("CBR_ALPHA2CODE") === "TN",
	options:[
		{
			name: "Oilspills Alert Areas",
			fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
			options: {
				"TN_BASELINE": area => area.get("DESCRIPTION") === "TN_BASELINE"
			}
		}
	]
};

const Turkey = {
	name: "Turkey",
	fn: area => area.get("CBR_ALPHA2CODE") === "TR",
	options:[{
		name: "EO Coverage",
		fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
		options: {
			"Turkey": area => area.get("AUTHORITY") === "Turkey",
			"Turkey 1": area => area.get("AUTHORITY") === "Turkey 1",
			"Turkey 2": area => area.get("AUTHORITY") === "Turkey 2",
			"Turkey 3": area => area.get("AUTHORITY") === "Turkey 3",
			"Turkey 4": area => area.get("AUTHORITY") === "Turkey 4",
			"Turkey 5": area => area.get("AUTHORITY") === "Turkey 5",
			"Turkey 6": area => area.get("AUTHORITY") === "Turkey 6",
			"Turkey - Old Moninfo Area ": area => area.get("AUTHORITY") === "Turkey - Old Moninfo Area "
		}
	},
	{
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"TR_BASELINE": area => area.get("DESCRIPTION") === "TR_BASELINE"
		}
	}]
};

const UK = {
	name: "United Kingdom",
	fn: area => area.get("CBR_ALPHA2CODE") === "GB",
	options:[{
		name: "EO Coverage",
		fn: area => area.get("AREA_CATEGORY") === "EO_COVERAGE_REQUIR",
		options: {
			"UK_Hotspot_1": area => area.get("AUTHORITY") === "UK_Hotspot_1",
			"UK_Hotspot_2": area => area.get("AUTHORITY") === "UK_Hotspot_2",
			"UK_Hotspot_3": area => area.get("AUTHORITY") === "UK_Hotspot_3",
			"UK_Hotspot_4": area => area.get("AUTHORITY") === "UK_Hotspot_4",
			"UK_Hotspot_5": area => area.get("AUTHORITY") === "UK_Hotspot_5",
			"UK_Hotspot_6": area => area.get("AUTHORITY") === "UK_Hotspot_6"
		}
	},
	{
		name: "Oilspills Alert Areas",
		fn: area => area.get("AREA_CATEGORY") === "EO_SPILL_ALERTING_A",
		options: {
			"UK_BASELINE": area => area.get("DESCRIPTION") === "UK_BASELINE",
			"UK_GIBRALTAR": area => area.get("DESCRIPTION") === "UK_GIBRALTAR"
		}
	}]
};

const OSPAR = {
	name: "OSPAR Exchange Area",
	fn: (area) => {
		const areaName = area.get("NAME");
		switch (areaName){
			case "Exclusion Area 2":
			case "Exclusion Area 3":
			case "Exclusion Area 4":
			case "Exclusion Area 5":
			case "Exclusion Area 6":
			case "Exclusion Area 7":
			case "Exclusion Area 8":
			case "Exclusion Area 9":
			case "Exchange Area 10":
			case "Exchange Area 11":
				return true;
			default:
				return false;
		}
	}
};

module.exports = {
	OSPAR,
	Albania,
	// Algeria,
	Azerbeijan,
	Belgium,
	BonnAgreement,
	Bulgaria,
	Croatia,
	Cyprus,
	Denmark,
	Estonia,
	Finland,
	France,
	Germany,
	Georgia,
	Greece,
	Iceland,
	Ireland,
	// Israel,
	Italy,
	Jordan,
	Latvia,
	Lithuania,
	Malta,
	Montenegro,
	Morocco,
	Netherlands,
	Norway,
	Poland,
	Portugal,
	Romania,
	Slovenia,
	Spain,
	Sweden,
	Tunisia,
	Turkey,
	UK
};