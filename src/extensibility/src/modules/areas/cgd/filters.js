const buildFilters = (config, parentConfig, _filter) => {
	let filter = Object.assign({}, _filter);
	if (config.fn){
		if (parentConfig) {
			config.name = parentConfig.name + " " + config.name;
			filter[config.name.replace(/\./g,"_")] = area => parentConfig.fn?(config.fn(area) && parentConfig.fn(area)):config.fn(area);
		} else
			filter[config.name.replace(/\./g,"_")] = area => config.fn(area);
	}

	if (config.options) {
		if (Array.isArray(config.options))
			config.options.forEach(option => {
				filter = buildFilters(option, config, filter);
			});
		else for (const key in config.options) {
			if (config.options.hasOwnProperty(key)) {
				const properKey = key.replace(/\./g,"_");
				filter[config.name.replace(/\./g,"_") + " " + properKey] = (area) => config.options[properKey](area) && config.fn(area);
			}
		}
	}
	return filter;
};

const getPresetFilters = ({preset_filters, only}) => {
	const filters_enum = require("./filters.enum"),
		filters = Object.assign({}, preset_filters, {
			options: preset_filters.options
				.map(filter => Object.assign(filters_enum[filter], {
					options: (() => {
						let opts = filters_enum[filter].options;
						// Enum filters have  "EO Coverage" and "Oilspills Alert Areas" filters,
						// but EMSA asked to show only the second, so we filter with the "only" value
						if(only)
							opts = opts.filter(option => only.some(name => option.name === name));
						return opts;
					})()
				}))
		});
	filters.options = buildFilters(filters, null);
	return [filters];
};


// CSV AREAS FUNCTIONS
const MENU_LEVEL_KEY = ["MENU_LEVEL0_KEY", "MENU_LEVEL1_KEY", "MENU_LEVEL2_KEY", "MENU_LEVEL3_KEY"],
	MENU_LEVEL_LABEL = ["MENU_LEVEL0_LABEL", "MENU_LEVEL1_LABEL", "MENU_LEVEL2_LABEL", "MENU_LEVEL3_LABEL"];

const removeParentFilterNames = (item, parents) => {
	parents.forEach(parentName => {
		if (~item.bindings.filter.alias.indexOf(parentName))
			item.bindings.filter.alias = item.bindings.filter.alias.replace(parentName, "").trim();
		if (~item.alias.indexOf(parentName))
			item.alias = item.alias.replace(parentName, "").trim();
	});

	if (item.options.length){
		parents.push(item.bindings.filter.alias.trim());
		item.options = item.options.map(option => removeParentFilterNames(option, parents));
	}

	return item;
};

const setFiltersPreferences = (opt) => {
	const active = seg.preferences.map.get("layers.cgd_areas.filters.AREAS.options." + opt.alias);
	opt.bindings.filter.setActive(active);
	if (opt.options.length)
		opt.options = opt.options.map(optOptions => setFiltersPreferences(optOptions));
	return opt;
};

// new function to transform csvs into area filters: let there be light
const buildCSVFilter = (csv) => {
	// splits the csv into its headers and data. Then creates and object with the headers as key and data as
	const arr = csv.split("\n"),
		areasObject = [],
		headers = arr[0].split(";");

	for(let i = 1; i < arr.length; i++) {
		const data = arr[i].split(";"), obj = {};
		for(let j = 0; j < data.length; j++) {
			obj[headers[j].trim()] = data[j].trim().replace(/['"]+/g, "");
		}
		areasObject.push(obj);
	}

	let filterLevel = {}, prevLayer = {}, parent;
	const csvFilter = {options: []};

	// Esses 4 loops varrem o areasObject verticalmente e horizontalmente para construir os níveis do menu, enquanto evita colisões dos nomes em cada nivel de menu
	for (let column = 0; column < 4; column++) {
		for(let row = 0; row < areasObject.length; row++) {
			const rowObject = areasObject[row];
			if(rowObject[MENU_LEVEL_KEY[column]] !== prevLayer["layerid"] && !isEmpty(rowObject[MENU_LEVEL_KEY[column]])) {
				switch(column) {
					case 0:
						parent = csvFilter;
						if(parent){
							// gera a layer com os parametros corretos
							filterLevel = buildFilterLevel(rowObject, column);
							parent.options.push(filterLevel);
							prevLayer = filterLevel;
						}
						break;
					case 1:
						parent = csvFilter.options.find(l => l.layerid === rowObject[MENU_LEVEL_KEY[column-1]]);
						if(parent){
							filterLevel = buildFilterLevel(rowObject, column);
							parent.options.push(filterLevel);
							prevLayer = filterLevel;
						}
						break;
					case 2:
						for(let i = 0; i < csvFilter.options.length; i++){
							parent = csvFilter.options[i].options.find(l => l.layerid === rowObject[MENU_LEVEL_KEY[column-1]]);
							if(parent){
								filterLevel = buildFilterLevel(rowObject, column);
								parent.options.push(filterLevel);
								prevLayer = filterLevel;
							}
						}
						break;
					case 3:
						for(let i = 0; i < csvFilter.options.length; i++){
							for(let j = 0; j < csvFilter.options[i].options.length; j++){
								parent = csvFilter.options[i].options[j].options.find(l => l.layerid === rowObject[MENU_LEVEL_KEY[column-1]]);
								if(parent){
									filterLevel = buildFilterLevel(rowObject, column);
									parent.options.push(filterLevel);
									prevLayer = filterLevel;
								}
							}
						}
						break;
				}
			}
		}
	}

	return csvFilter.options;
};

const buildFilterLevel = (rowObject, column) => {
	const layer = {
		fn: area => area.get("NAME").includes(rowObject[MENU_LEVEL_KEY[column]]),
		layerid: rowObject[MENU_LEVEL_KEY[column]],
		name: rowObject[MENU_LEVEL_LABEL[column]],
		level: column,
		options: []
	};

	// Check if it is the last column, either column = 3 or if there is no key on the next column
	if (column === 3 || (column !== 3 && isEmpty(rowObject[MENU_LEVEL_KEY[column+1]]))) {
		layer.fn = (area) => {
			let area_name = area.get("NAME"),
				split_area_with_parts = area.get("NAME").split("_part_"); // GETTING AREAS WITH _part_ string, to toggle araes with multiple parts

			if (split_area_with_parts.length === 1)
				split_area_with_parts = area.get("NAME").split("_PART");

			if (split_area_with_parts.length > 1) // if there is a split between _part_ it means that there are more than one area with that name
				area_name = split_area_with_parts[0]; // replace area_name so that it can filter every part

			const split = area_name.split(rowObject[MENU_LEVEL_KEY[column]]);

			return (split.length > 1) && (split[0].length === 0) && (split[1].length === 0);
		};
	}

	return layer;
};

const buildCSVFilters = async ({csv_files}) => {
	const csvRequests = csv_files.map(name => {
		return seg.request.get("areas/csvs/" + name, {})
			.then(csvResponse => csvResponse, e => seg.log.error("ERROR_CGD_BUILD_CSV_FILTERS", e.error + ": " + e.result));
	});

	return seg.promise.all(csvRequests).then(csvResponses => {
		const csvFilters = Object.assign({}, {
			name: "AREAS",
			options: csvResponses.reduce((options, csvResponse) => {
				return options.concat(buildCSVFilter(csvResponse));
			}, [])
		});
		csvFilters.options = buildFilters(csvFilters, null);
		return [csvFilters];
	});
};


const isEmpty = function(string) {
	return (string.length === 0 || !string.trim());
};

module.exports = {
	getPresetFilters,
	buildFilters,
	buildCSVFilters,
	removeParentFilterNames,
	setFiltersPreferences
};
