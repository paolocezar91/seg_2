const styleCache = {};

module.exports = ((area) => {
	const styles = [],
		selected = area.get("selected"),
		text = area.get("DESCRIPTION");

	let strokeColor = "#00f",
		strokeWidth = 1.25;

	if(selected){
		strokeColor = "#ff0";
		strokeWidth = 2.5;
	}

	const cacheKey = JSON.stringify({strokeColor, strokeWidth, text});

	if(!styleCache[cacheKey])
		styleCache[cacheKey] = new ol.style.Style({
			fill: new ol.style.Fill({
				color: "rgba(0,0,8,.1)"
			}),
			stroke: new ol.style.Stroke({
				color: strokeColor,
				width: strokeWidth
			}),
			text: new ol.style.Text({
				font: "12px sans-serif",
				text,
				fill: new ol.style.Fill({
					color: "#FFF"
				}),
				backgroundFill: new ol.style.Fill({
					color: "#000"
				})
			})
		});

	styles.push(styleCache[cacheKey]);

	return styles;
});