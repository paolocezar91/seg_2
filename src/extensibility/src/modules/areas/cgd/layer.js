const BATCH_SIZE = seg.CONFIG.getValue("BATCH_SIZE");


module.exports = config => ({
	id: "cgd_areas",
	name: "CGD AREAS",
	type: "Vector",
	source: {
		type: "Vector"
	},
	search: {
		keys: [
			"DESCRIPTION"
		],
		label: "DESCRIPTION",
		smartSearch: (smartSearchResults) => {
			const cgdAreasSource = seg.layer.get("cgd_areas").getSource();
			cgdAreasSource.getFeatures().forEach((cgdArea) => cgdArea.unset("isSmartSearchResult"));

			// Transformar attr vindos da smartsearch em capital case
			if (smartSearchResults["CGD_KEY"] && smartSearchResults["CGD_KEY"].features && smartSearchResults["CGD_KEY"].features.length) {
				smartSearchResults["CGD_KEY"].features = smartSearchResults["CGD_KEY"].features
					.map(feature => Object.assign(feature, {
						properties: {
							AREA_CATEGORY: feature.properties.areaCategory,
							AUTHORITY: feature.properties.authority,
							CBR_ALPHA2CODE: feature.properties.cbrAlpha2Code,
							DESCRIPTION: feature.properties.description,
							GEOMETRY_TYPE :feature.properties.geometryType,
							INSERTION_DATE: feature.properties.insertionDate,
							MODIFICATION_DATE :feature.properties.modificationDate,
							MODIFIED_BY :feature.properties.modifiedBy,
							NAME: feature.properties.name,
							OWNER: feature.properties.owner,
							TYPE_SPECIFIC_ATTR: feature.properties.typeSpecificAttr
						}
					}));

				const cgd_areas = mergeDuplicates((new ol.format.GeoJSON()).readFeatures(smartSearchResults["CGD_KEY"], {
					dataProjection: "EPSG:4326",
					featureProjection: seg.map.getProjection()
				}));
				// .sort((a, b) => {if (a.get("ID") > b.get("ID")) {return 1;} if (a.get("ID") < b.get("ID")) {return -1;} return 0;});

				cgdAreasSource.addFeatures(cgd_areas);

				cgd_areas.forEach((cgd_area) => {
					cgd_area.set("isSmartSearchResult", true);
				});

				return cgd_areas;
			}
			return [];
		},
		cleanSmartSearch: (results) => {
			results.forEach(result => seg.layer.get("cgd_areas").getSource().removeFeature(result.feature));
		}
	},
	style: config.style || require("./style.js"),
	commandInfo: (config.commandInfo) || undefined,
	filters: []
});

const mergeDuplicates = areas => {
	const chunks = [],
		cgdAreasSource = seg.layer.get("cgd_areas").getSource();

	for(let i=0; i<areas.length; i+=BATCH_SIZE)
		chunks.push(areas.slice(i, i+BATCH_SIZE));

	const mergedChunks = chunks.map(areas => mergeAreas(cgdAreasSource, areas));

	return [].concat(...mergedChunks);
};

const mergeAreas = (cgdAreasSource, areas) => areas.map(area => {
	// const existing = cgdAreasSource.getFeatureById(newFeature.getId());
	const existing = cgdAreasSource.getFeatures().find(found => found.get("NAME") === area.get("NAME"));
	return existing ? existing : area;
});