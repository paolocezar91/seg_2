/**@namespace extensibility
@description
These are the defined modules on extensibility for SEG.

- {@link extensibility.modules.vessels vessels}
- {@link extensibility.modules.ports ports}
- {@link extensibility.modules.orgInfraLogis orgInfraLogis}
- {@link extensibility.modules.eo eo}
- {@link extensibility.modules.sarSurpic sarSurpic}
- {@link extensibility.modules.metOcean metOcean}
- {@link extensibility.modules.tdm tdm}
- {@link extensibility.modules.areas areas}
- {@link extensibility.modules.location location}
- {@link extensibility.modules.alerts alerts}
- {@link extensibility.modules.apps apps}
- {@link extensibility.modules.events events}

SEG sends user information that is parsed on {@link extensibility.operations operations} and {@link extensibility.permissions permissions},
and injects on the modules to define the functionalities available in the SEG core interface.
*/

/**
@typedef extensibility.modules
@prop {string} name - name of the module
@prop {object} layers - {@link ol.layer.Layer layers} that are added to this module. They may be Image, Vector or mixed layers.
@prop {object} [preferences] - specific preferences for each module
@prop {callback} init - The init callback is called to setup any specific configuration that come from {@link extensibility.permission.permissionsOptions permissions} or {@link extensibility.operations.Config operations}

@description
The extensibility modules are an extension of Open Layers functionalities, with custom functions and attributes to serve the SEG interface.
*/
const MODULES = {
	"aoi": require("./aoi"),
	"vessels": require("./vessels"),
	"ports": require("./ports"),
	// "orgInfraLogis": require("./org_infra_logis"),
	"eo": require("./eo"),
	// "sarSurpic": require("./sar_surpic"),
	// "metOcean": require("./met_ocean"),
	// "tdm": require("./tdm"),
	// "areas": require("./areas"),
	// "location": require("./location"),
	// "alerts": require("./alerts"),
	// "apps": require("./apps"),
	// "events": require("./events")
};

const bindModuleToPermissions = (module, permissions) => (config = {}) => module(config, permissions);

module.exports = permissionsOptions => Object.entries(MODULES).reduce((modules, [key, module]) => Object.assign(modules, {
	[key]: bindModuleToPermissions(module, permissionsOptions)
}), {});