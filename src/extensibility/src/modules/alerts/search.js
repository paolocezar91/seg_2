const {mergeDuplicatesAndAddToSource, alertTTTFields} = require("./api.js");

/**
 * Registering attributes and functions for alert ACQ
 */

module.exports = () => ({
	name: "Alerts",
	template: resolvePath("./templates/search.html"),
	services: {
		search: {
			acquisitionStart: moment().subtract(1, "d"),
			acquisitionEnd: moment()
		}
	},
	submit() {
		const loaderModal = seg.modal.openModal("Alerts Advanced Search", "Retrieving Alerts...", {class: "info"});
		const url = seg.CONFIG.getValue("alerts.NEW_ALERTS")?seg.CONFIG.getValue("alerts.ALERTS_ENDPOINT"):seg.CONFIG.getValue("alerts.ALERTS_OLD_ENDPOINT");

		return seg.request.get(url, {
			params: {
				startTime: this.services.search.acquisitionStart.format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
				endTime: this.services.search.acquisitionEnd.format("YYYY-MM-DD[T]HH:mm:ss[Z]")
			}
		}).then(res => {
			//if no reports were retrieved, return
			if (!res.result.length) {
				loaderModal.setContent("Retrieved 0 alerts reports.");
				loaderModal.setClass("no-result");
				seg.modal.closeTimeout(loaderModal.title);
				return;
			}

			const features = mergeDuplicatesAndAddToSource(res.result);

			seg.ui.queryPanel.ACQ.addFeatures(features);
			loaderModal.setContent("Retrieved " + res.result.length + " alert reports");
			loaderModal.setClass("success");

			let alertsTable = seg.ui.ttt.tables.openTables.find(t => t.label === "Advanced Search alerts");

			if (!alertsTable)
				alertsTable = seg.ui.ttt.tables.open({
					label: "Adv. Search alerts",
					data: features,
					itemAs: "alert",
					fields: alertTTTFields,
					sortKey: "startTime",
					sortKeyOrder: "asc",
					onSelectionChange(changedRows, selectedRows) {
						seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
					}
				});
			else
				alertsTable.data = features;

			seg.modal.closeTimeout(loaderModal.title);
		}, e => {
			loaderModal.setContent("Advanced Search Failed");
			seg.modal.closeTimeout(loaderModal.title);
			seg.log.error("ERROR_ALERTS_SEARCH_SUBMIT", e.error + ": " + e.result);
		});
	},
	reset() {
		this.services.search.acquisitionStart = moment().subtract(1,"d");
		this.services.search.acquisitionEnd = moment();
	},
	save: (searchName) => {
		seg.search.getSavedSearches().then(res => {
			const savedSearchesSmart = res[1],
				savedSearches = res[0];

			savedSearches.push({
				name: searchName,
				config: this.services,
				src: "Alerts",
				advanced: true
			});

			seg.search.setSavedSearches(savedSearches.concat(savedSearchesSmart));
		}, e => seg.log.error("ERROR_ALERTS_SEARCH_SAVE", e.error + ": " + e.result));
	},
	load: (search) => {
		//open the advanced search panel
		this.services = search.config;
	}
});
