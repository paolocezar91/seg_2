module.exports = {
	id: "alerts",
	name: "Alerts",
	type: "Vector",
	order: 4,
	source: {
		type: "Vector"
	},
	timerange: {
		timestampProperty: "startTime",
		exceptionCheck: (feature) => (
			(feature.get("group_name") && feature.get("favourite_name")) ||
			feature.get("isACQResult")
		)
	},
	search: {
		keys: [
			"vesselName", "vesselMmsi", "id", "abmName"
		],
		label: "vesselMmsi"
	},
	commandInfo: {
		featureAs: "alert",
		bindings: {
			AlertCommandInfo: require("./command_info.scope.js")
		},
		//template url for this C&I
		templateUrl: resolvePath("./templates/command_info.html")
	},
	tooltip: {
		featureAs: "alert",
		bindings: {
			AlertToolTip: require("./command_info.scope.js")
		},
		templateUrl: resolvePath("./templates/tooltip.html")
	},
	style: require("./style.js"),
	visible: false
};
