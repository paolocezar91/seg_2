const mapProperties = require("./properties.js"),
	{alertTTTFields, parseCoordinates} = require("./api.js"),
	{getLastKnownPosition, mergeDuplicates} = require("../vessels/api.js");

/**
 * Init that is called when C&I is opened. We look for the vessel image associated with the vessel's alert, and also creates the AOI associated with the alert.
 * @param  {ol.Feature} alert Feature of an alert
 */
const init = (alert) => {
	getVesselImage(alert);
	createAOI(alert);
};

/**
 * Creates an AOI based on coordinates contained in the alert feature.
 * @param  {ol.Feature} alert Feature of an alert
 */
const createAOI = (alert) => {
	let aoi;
	const modal = seg.modal.openModal("ABM Alert AOI", "Creating Alert AOI..."),
		surveillanceInstance = alert.get("surveillanceInstance");

	aoi = seg.layer.get("aoi").getSource().getFeatures().find(aoi => {
		if(aoi.get("surveillanceInstance"))
			return aoi.get("surveillanceInstance").id === surveillanceInstance.id;
	});

	if(!aoi)
		aoi = alert.get("associatedAoi");

	if(!aoi){
		if ((
			surveillanceInstance.aoiParams &&
			surveillanceInstance.aoiParams.aoiAreas
		) || (
			surveillanceInstance.saParams &&
			surveillanceInstance.saParams.processedArea
		)) {
			const geometry  = new ol.geom.Polygon([[0,0]]);
			let coordinates;

			if(surveillanceInstance.saParams.processedArea)
				coordinates = surveillanceInstance.saParams.processedArea.split(" ").map(pair => ol.proj.transform(pair.split(",").map(val => Number(val)), "EPSG:4326", seg.map.getProjection()));
			else
				coordinates = parseCoordinates(surveillanceInstance.aoiParams.aoiAreas);

			if (coordinates.length === 2) {
				const [start, end] = coordinates;
				coordinates = [start, [start[0], end[1]], end, [end[0], start[1]], start];
				geometry.set("type", "rectangle");
			} else
				geometry.set("type", "polygon");

			geometry.setCoordinates([coordinates]);
			aoi = new ol.Feature({geometry, favourite_name: surveillanceInstance.name, aoi_alert: true, surveillanceInstance});

			seg.favourites.addFeaturesToGroup("Session", aoi, surveillanceInstance.name);

			alert.set("associatedAoi", aoi);

			modal.setContent("AOI associated with alert created");
		} else {
			modal.setContent("No AOI associated with this Alert");
		}
	} else {
		seg.favourites.addFeaturesToGroup("Session", aoi, surveillanceInstance.name);
	}
	seg.modal.closeTimeout(modal.title);
};

/**
 * Takes the surveillance form data contained in the alert feature and opens the ABM alert console
 * @param  {ol.Feature} alert Feature of an alert
 */
const openABM = (alert) => {
	seg.ui.apps.close("Report");
	seg.timeout(() => {
		seg.ui.apps.get("Report").bindings.setFoiType("ABM admin");
		seg.ui.apps.open("Report");

		const service = seg.ui.apps.get("Report").bindings.scope;

		seg.timeout(() => {
			service.getReportScope().openReport(alert.get("surveillanceInstance"));
			service.getReportScope().toggleEditing(false);
			service.logs.selected = null;

			service.logOpen = false;
			service.reportOpen = true;
		}, 250);
	}, 250);
};

/**
 * Look up the service for ship image based on a primary or secondary key of a vessel (imo or mmsi)
 * @param  {ol.Feature} alert Feature of an alert
 */
const getVesselImage = async (alert) => {
	const mmsi = alert.get("vessel").mmsi,
		imo = alert.get("vessel").imo;

	if (!alert.get("photos") && (mmsi || imo)) {
		alert.set("photos", []);

		let data;

		if(imo)
			data = await seg.request.getDataURL("v1/ship/particulars/image?imo=" + imo);

		if (!data && mmsi)
			data = await seg.request.getDataURL("v1/ship/particulars/image?mmsi=" + mmsi);

		if (data !== "data:" && !data.endsWith("base64,"))
			alert.get("photos").push(data);

		seg.request.get("v1/file/fileMetadata", {
			params: {
				path: "/vessels/images/" + alert.getId()
			}
		}).then(res => {
			res.result.features.forEach(feature => {
				seg.request.getDataURL("v1/file/download", {
					params: {
						path: feature.properties.path
					}
				}).then(data => {
					if (!alert.get("photos"))
						alert.set("photos", []);
					if (data !== "data:")
						alert.get("photos").push(data);
				}, e => seg.log.error("ERROR_ALERTS_GET_VESSEL_IMAGE_DOWNLOAD", e.error + ": " + e.result));
			});
		}, e => seg.log.error("ERROR_ALERTS_GET_VESSEL_IMAGE", e.error + ": " + e.result));
	}
};

/**
 * Based on vessel data, returns a label for it.
 * @param  {ol.Feature} vessel Vessel data, usually name, primary and secondary keys
 * @return {string}        Returns a given label of a vessel
 */
const getLabelForVessel = vessel => {
	if(vessel.name)
		return "Name: " + vessel.name;
	if(vessel.imo)
		return "IMO: " + vessel.imo;
	if(vessel.mmsi)
		return "MMSI: " + vessel.mmsi;
	if(vessel.cs)
		return "Call sign: " + vessel.cs;
};

/**
 * Helper function for getLabelForVessel
 * @param  {ol.Feature} alert Feature of an alert
 * @return {string}        Returns a given label of a vessel
 */
const getLabel = alert => {
	const vessel = alert.get("vessel");
	return vessel?getLabelForVessel(vessel):"";
};

/**
 * Function to open the ACQ console with some predefined parameters, like the center, and radius of the area of interest.
 * @param  {ol.Feature} alert Feature of an alert
 */
const openQueryPanel = alert => {
	const center = ol.extent.getCenter(alert.getGeometry().getExtent()),
		distanceUnits = seg.preferences.workspace.get("distanceUnits");
	let radius = 50/*nm*/,
		around = radius;

	switch(distanceUnits) {
		case "km": around *= 1.852; break;
		case "m": around *= 1852; break;
	}

	seg.ACQ.open("bboxBuilder", {
		corner1: ol.proj.toLonLat(center, seg.map.getProjection()),
		around
	});

	//NM to meters
	radius *= 1852;
	//meters to projection unit
	radius /= seg.map.getProjection().getMetersPerUnit();

	seg.map.centerOn(new ol.Feature({
		geometry: new ol.geom.Circle(center, radius)
	}));
};

/**
 * Similar to openQueryPanel function, but instead of opening ACQ console, this function just call ACQ services without allowing the user to change it's parameters
 * @param  {ol.Feature} alert Feature of an alert
 */
const areaQuery = alert => {
	const center = new ol.geom.Point(ol.extent.getCenter(alert.getGeometry().getExtent())),
		around = (25/*nm*/ * 1852/*m*/) / seg.map.getProjection().getMetersPerUnit(),
		geometry = ol.geom.Polygon.fromExtent(ol.extent.buffer(center.getExtent(), around)),
		alertTime = moment(alert.get("startTime"));

	seg.favourites.addFeaturesToGroup("Session", new ol.Feature({geometry}), alert.getId()+"_ACQ");

	seg.ACQ([ol.proj.transformExtent(geometry.getExtent(), seg.map.getProjection(), "EPSG:4326")], alertTime.clone().subtract(3, "h"), alertTime.clone().add(3, "h"));

	seg.map.centerOn(geometry);
};

const getSnoozeTime = () => seg.preferences.workspace.get("Alert.areaQuery.snoozedDefaultHours") || 24;

const snoozedAlert = alert => seg.popup.openPopup({
	title: "Snooze Alert",
	template: `
		Confirm alert snooze?
		<div class="flexbox" style="margin-top:15px">
			<button title="Yes" class="seg positive rounded md" ng-click="snooze()" style="margin-right:5px;">Yes</button>
			<button title="No"class="seg positive rounded md" ng-click="close()">No</button>
		</div>`,
	bindings: {
		close() {
			seg.popup.close("Snooze Alert");
		},
		async snooze() {
			const url = seg.CONFIG.getValue("alerts.NEW_ALERTS")?seg.CONFIG.getValue("alerts.ALERTS_ENDPOINT"):seg.CONFIG.getValue("alerts.ALERTS_OLD_ENDPOINT"),
				modal = seg.modal.openModal("SNOOZE_ALERT", "Requesting alert snooze  for 24 hours...");

			seg.request.put(url, {
				params: {
					id: alert.getId(),
					snoozedHours: (seg.preferences.workspace.get("Alert.areaQuery.snoozedDefaultHours") || 24)
				}
			}).then(() => {
				seg.selection.deselect(alert);
				seg.layer.get("alerts").getSource().removeFeature(alert);
				seg.popup.close("Snooze Alert");
				modal.setContent("Snooze for alert was successful");
				seg.modal.closeTimeout(modal.title);
			}, e => {
				modal.setContent("Snooze for alert was not successful:" + e.result);
				seg.modal.closeTimeout(modal.title);
				seg.popup.close("Snooze Alert");
			});


		}
	}
});

/**
 * Calls services to get the vessel data associated with the alert, creates a feature from this data, selects and open it's C&I
 * @param  {ol.Feature} alert Feature of an alert
 */
const selectVessel = async alert => {
	const alertVessel = alert.get("vessel");
	let vessel;
	// let vessels = seg.layer.get("positions").getSource().getFeatures();

	if (alertVessel) {
		vessel = seg.layer.get("positions")
			.getSource()
			.getFeatures()
			.find(vessel => (alertVessel.imo && vessel.get("mmsi") === alertVessel.mmsi) ||
				(alertVessel.imo && vessel.get("imo") === alertVessel.imo) ||
				(alertVessel.shipName && vessel.get("shipName") === alertVessel.shipName) ||
				(alertVessel.id && vessel.getId() === alertVessel.id)
			);

		if(vessel)
			seg.selection.selectFeatureAndCenter(vessel);
		else if(alertVessel.id) {
			// let currentPosition = await getLastKnownPosition(alertVessel.id);
			let currentPosition = await getLastKnownPosition({
				id: alertVessel.id,
				vesselId: alertVessel.id,
				csdId: alertVessel.csdId,
				imo: alertVessel.imo,
				mmsi: alertVessel.mmsi
			});

			if(Array.isArray(currentPosition) && currentPosition.length)
				currentPosition = currentPosition[0];

			if (currentPosition) {
				currentPosition.setId(alertVessel.id); // getLastKnownPosition returns wrong id, needs to reinforce with the one coming from alert
				vessel = await mergeDuplicates([currentPosition]);
				seg.layer.get("positions").getSource().addFeatures(vessel);
				seg.selection.selectFeatureAndCenter(vessel);
			} else {
				const modal = seg.modal.openModal("Vessel not found", "Not possible to select vessel", {class: "no-result"});
				seg.modal.closeTimeout(modal.title);
			}
		} else {
			const modal = seg.modal.openModal("Vessel not found", "Not possible to select vessel", {class: "no-result"});
			seg.modal.closeTimeout(modal.title);
		}
	}
};

/**
 * Properties of alerts to be shown in C&I
 * @type {SEG.properties}
 */
const displayPreferences_ = mapProperties({override: false});
//we only want to save the "visible" property, the rest is always the same
const toggleDisplayPreference = key => seg.preferences.workspace.set("Alert.commandInfo.displayOptions."+key, !displayPreferences_[key].visible);

/**
 * Properties of alerts to be shown in tooltip
 * @type {SEG.properties}
 */
const displayTooltipPreferences_ = mapProperties({
	override: {
		id: false,
		startTime: false,
		endTime: false,
		latitude: { visible: false },
		longitude: { visible: false },
		abmName: { visible: false },
		vesselImo: { visible: false },
		vesselName: false,
		vesselMmsi: false,
		vesselType: false
	}
});

/**
 * Calls service to download the report associated with the alert.
 * @param  {ol.Feature} alert Feature of an alert
 */
const downloadAlertReport = alert => {
	const modal = seg.modal.openModal("Alert Report", "Downloading Alert Report...", {class: "info"});

	seg.request.download("v1/eo/alert_report/" + alert.getId(), {}, alert.getId() + "_alert_report.pdf").then(() => {
		modal.setContent("Download successful");
		seg.modal.closeTimeout(modal.title);
	}, e => seg.log.error("ERROR_ALERTS_DOWNLOAD_ALERT_REPORT", e.error + ": " + e.result));
};

/**
 * Get the report url associated with the alert
 * @param  {ol.Feature} alert Feature of an alert
 * @return {string}       URL string
 */
const getReportUrl = alert => {
	return "v1/abm/am/alerts/report?alertId=" + alert.getId() + "&user=" + seg.user.getCurrentUser().username + "&project=" + seg.operation.project;
};


module.exports = {
	init,
	getLabel,
	openQueryPanel,
	areaQuery,
	getSnoozeTime,
	openABM,
	snoozedAlert,
	getReportUrl,
	selectVessel,
	get displayPreferences() {
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("Alert.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach(key => {
			if (displayPreferences_[key])
				displayPreferences_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return displayPreferences_;
	},
	toggleDisplayPreference,
	get tooltipDisplayPreferences() {
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("Alert.tooltip.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (displayTooltipPreferences_[key])
				displayTooltipPreferences_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return displayTooltipPreferences_;
	},
	downloadAlertReport,
	alertTTTFields
};
