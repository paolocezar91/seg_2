/**
@namespace extensibility.modules.alerts
@description
# Alerts

## Available options
None

## Global configurations and constants
None
*/
const {createMenu, abmNamesColors, alertTTTFields, updateAlertsSource} = require("./api");

module.exports = (/*config = {}*/) => {
	return {
		name: "Alerts",
		layers: [
			require("./layer.js")
		],
		init() {
			const alertsLayer = seg.layer.get("alerts"),
				alertsSource = alertsLayer.getSource(),
				//Alert Menu Preferences Creation
				alertsMenuItem = createMenu();

			//register Alerts in ACQ
			seg.ui.queryPanel.ACQ.register(require("./acq.js"));

			//register Alerts in Advanced Search
			seg.ui.searchAdvanced.addPanel(require("./search.js")());

			let startTime = moment().utc().subtract(1, "d"),
				endTime = moment().utc(),
				alertsTable;

			const fetchAlerts = (notify) => {
				const url = seg.CONFIG.getValue("alerts.NEW_ALERTS")?seg.CONFIG.getValue("alerts.ALERTS_ENDPOINT"):seg.CONFIG.getValue("alerts.ALERTS_OLD_ENDPOINT");

				return seg.request.get(url, {
					params: {
						startTime: startTime.format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
						endTime: endTime.format("YYYY-MM-DD[T]HH:mm:ss[Z]")
					}
				}).then(({result}) => {

					startTime = endTime.clone().subtract(1, "d"),
					endTime = moment().utc();

					if (!result)
						return [];

					updateAlertsSource(result, notify);

					//info has to be updated if new alertTypes come
					alertsMenuItem.info = {
						title: "Alerts Symbology",
						templateUrl: resolvePath("./templates/legend.html"),
						bindings: {
							getCurrentSymbolizer: () => seg.preferences.map.get("alerts.symbolizer"),
							abmNamesColors
						}
					};

					if (seg.ui.ttt.tables.get("Active Alerts"))
						seg.ui.ttt.tables.addDataToTable(alertsTable, alertsSource.getFeatures());

					seg.timeout(() => fetchAlerts(true), seg.CONFIG.getValue("alerts.REFRESH_TIME"));
				}, e => seg.log.error("ERROR_ALERTS_FETCH_ALERTS", e.status + ": " + e.message));
			};

			if (!seg.preferences.map.get("alerts.symbolizer"))
				seg.preferences.map.set("alerts.symbolizer","Target Age Color");

			seg.shortcuts.add("alerts", [
				{
					label: "See in TTT",
					callback(retrievedAlerts) {
						alertsTable = seg.ui.ttt.tables.get("Selected Alerts");

						if(!alertsTable)
							alertsTable = {
								label: "Selected Alerts",
								customToolbar: {
									template: "<span class=\"text-size-sm\">Number of alerts: <b>{{grid.filteredResults.length}}</b></span>"
								},
								data: [],
								itemAs: "alert",
								fields: alertTTTFields,
								sortKey: "startTime",
								sortKeyOrder: "asc",
								paginate: true
							};

						seg.ui.ttt.tables.addDataToTable(alertsTable, retrievedAlerts);
						seg.ui.ttt.tables.open(alertsTable);
					}
				}
			]);

			seg.ui.ttt.tables.register({
				customToolbar: {
					template: "<span class=\"text-size-sm\">Number of Alerts: <b>{{grid.filteredResults.length}}</b></span>"
				},
				label: "Active alerts",
				data: alertsSource.getFeatures(),
				itemAs: "alert",
				fields: alertTTTFields,
				sortKey: "startTime",
				sortKeyOrder: "asc",
				paginate: {
					itemsPerPage: 100
				},
				onSelectionChange(changedRows, selectedRows) {
					seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
				},
				onOpen: () => seg.ui.ttt.tables.addDataToTable(seg.ui.ttt.tables.get("Active alerts"), alertsSource.getFeatures())
			});

			fetchAlerts();
		}
	};
};
