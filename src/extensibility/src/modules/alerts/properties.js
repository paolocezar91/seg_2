/**
 * @namespace extensibility.modules.alerts.props
 * @description Properties for alerts
 *
 * @typedef {extensibility.modules.alerts.props.alertProperties}
 * @property {Object} id id for the alert feature
 * @property {Object} startTime startTime for the alert feature
 * @property {Object} endTime endTime for the alert feature
 * @property {Object} latitude latitude for the alert feature
 * @property {Object} longitude longitude for the alert feature
 * @property {Object} abmName abmName for the alert feature
 * @property {Object} vesselImo vesselImo for the vessel associated to the alert feature
 * @property {Object} vesselName vesselName for the vessel associated to the alert feature
 * @property {Object} vesselMmsi vesselMmsi for the vessel associated to the alert feature
 * @property {Object} vesselFlag vesselFlag for the vessel associated to the alert feature
 *
 */

const defaultProperties = {
	id: {
		label: "ID",
		template: "{{::alert.getId()}}",
		visible: true,
		order: 0
	},
	vesselName: {
		label: "Vessel name",
		template: "{{::alert.get('vesselName') || 'N/A'}}",
		visible: true,
		order: 1
	},
	vesselImo: {
		label: "Vessel IMO",
		template: "{{::alert.get('vesselImo') || 'N/A'}}",
		visible: true,
		order: 2
	},
	vesselMmsi: {
		label: "Vessel MMSI",
		template: "{{::alert.get('vesselMmsi') || 'N/A'}}",
		visible: true,
		order: 3
	},
	vesselFlag: {
		label: "Vessel Flag",
		template: "{{::(alert.get('vesselFlag') | country) || 'N/A'}}",
		visible: true,
		order: 4
	},
	vesselType: {
		label: "Vessel Type",
		template: "{{::(alert.get('vesselType') || 'N/A')}}",
		visible: true,
		order: 5
	},
	abmName: {
		label: "ABM Name",
		template: "{{::alert.get('abmName') || 'N/A'}}",
		visible: true,
		order: 6
	},
	startTime: {
		label: "Start time",
		template: "<span position-timestamp=\"::alert.get('startTime')\"></span>",
		visible: true,
		order: 7
	},
	endTime: {
		label: "End time",
		template: "<span position-timestamp=\"::alert.get('endTime')\"></span>",
		visible: true,
		order: 8
	},
	latitude: {
		label: "Latitude",
		template: "<span coordinate=\"::[alert.get('lat'), 'lat']\"></span>",
		visible: true,
		order: 9
	},
	longitude: {
		label: "Longitude",
		template: "<span coordinate=\"::[alert.get('lon'), 'lon']\"></span>",
		visible: true,
		order: 10
	}
};

/**
 * @function
 * @memberof extensibility.modules.alerts.properties
 * @param {Object} options Options that may override default properties of {@link extensibility.modules.alerts.props alertProperties}
 * @returns {extensibility.modules.alerts.properties.alertProperties} Properties that are mapped to be shown on C&I, TTT and tooltip in SEG.
 */
const mapProperties = (options) => seg.utils.mapItemProperties(defaultProperties, options);

module.exports = mapProperties;