const abmNamesColors = {},
	mapProperties = require("./properties.js");
/**
 * @namespace extensibility.modules.alerts.api
 * @description API for the Alert module
 *
 */

/**
 * Creates the menu for alerts with symbology. We have Target Age symbology (based on timestamp), and ABM Name, that color is set randomly.
 * @return {ol.Feature[]} - Returns the menu item
 */
const createMenu = () => {
	const alertsLayer = seg.layer.get("alerts"),
		alertsSource = alertsLayer.getSource(),
		alertsMenuItem = Object.assign(seg.ui.layerMenu.fromLayer("ALERTS", alertsLayer), {
			hideSelectAll: true
		}),
		//target age symbolizer
		targetAge = Object.assign(seg.ui.layerMenu.create("Target Age"), {
			type: "symbology",
			isChecked() {
				return seg.preferences.map.get("alerts.symbolizer") !== "ABM Name";
			},
			toggle() {
				if (seg.preferences.map.get("alerts.symbolizer") === "ABM Name")
					seg.preferences.map.set("alerts.symbolizer", "Target Age Color");

				alertsSource.changed();
			},
			options: ["Target Age Color", "Target Age Opacity"].map(option => Object.assign(seg.ui.layerMenu.create(option), {
				isChecked() {
					return seg.preferences.map.get("alerts.symbolizer") === option;
				},
				toggle() {
					if (seg.preferences.map.get("alerts.symbolizer") !== option)
						seg.preferences.map.set("alerts.symbolizer", option);

					alertsSource.changed();
				}
			})),
			hideSelectAll: true
		}),
		//abm name symbolizer
		abmName = Object.assign(seg.ui.layerMenu.create("ABM Name"), {
			type: "symbology",
			isChecked() {
				return seg.preferences.map.get("alerts.symbolizer") === "ABM Name";
			},
			toggle() {
				if (seg.preferences.map.get("alerts.symbolizer") !== "ABM Name")
					seg.preferences.map.set("alerts.symbolizer", "ABM Name");
				else seg.preferences.map.set("alerts.symbolizer", "Target Age Color");

				alertsSource.changed();
			}
		});
	//isPartial State is not needed on the menu
	delete alertsMenuItem.isPartial;

	alertsMenuItem.options.push(targetAge, abmName);

	seg.ui.layerMenu.register(alertsMenuItem);

	return alertsMenuItem;
};

/**
 * Function used to merge duplicate alert that may come from the service. We also normalize alert data because we may receive data from and older service or a newer service, that have different attributes
 * @param  {string[]} features - JSON array of alerts]
 * @return {ol.Feature[]} - Array of OpenLayers features
 */
const mergeDuplicatesAndAddToSource = features => {
	const alertSource = seg.layer.get("alerts").getSource(),
		featuresArray = features.map(alert => {
			const existingFeature = alertSource.getFeatureById(alert.alertId || alert.id);

			if (!existingFeature) {
				// OR operators used to contemplate old and new alert services
				const feature = new ol.Feature({
					startTime: alert.startTime || alert.startTime,
					endTime: alert.endTime || alert.endTime,
					abmName: alert.surveillanceInstance.name || alert.surveillanceInstance.name,
					lat: alert.lat || alert.latitude,
					lon: alert.lon || alert.longitude,
					vessel: alert.vessel || alert.identities[0],
					vesselType: alert.vessel ?  alert.vessel.shipType : null ||  alert.vessel ?  alert.vessel.thetisTypeCode : null || alert.identities[0] ?  alert.identities[0].shipType : null || alert.identities[0] ?  alert.identities[0].thetisTypeCode : null,
					vesselImo: alert.vessel ?  alert.vessel.imo : null || alert.identities[0] ?  alert.identities[0].imo : null,
					vesselName: alert.vessel ? alert.vessel.shipName : null || alert.identities[0] ? alert.identities[0].shipName : null,
					vesselMmsi: alert.vessel ? alert.vessel.mmsi : null || alert.identities[0] ? alert.identities[0].mmsi : null,
					vesselFlag: alert.vessel ? alert.vessel.fs : null || alert.identities[0] ? alert.identities[0].countryCode : null,
					surveillanceInstance: alert.surveillanceInstance,
					visible: true,
					geometry: new ol.geom.Point([Number(alert.lon || alert.longitude), Number(alert.lat || alert.latitude)])
				});

				if (typeof abmNamesColors[alert.surveillanceInstance.name] !== "undefined")
					feature.set("color", abmNamesColors[alert.surveillanceInstance.name]);
				else {
					abmNamesColors[alert.surveillanceInstance.name] = "#" + Math.floor(Math.random()*16777215).toString(16);
					feature.set("color", abmNamesColors[alert.surveillanceInstance.name]);
				}

				feature.setId(alert.id || alert.alertId);
				feature.setGeometry(feature.getGeometry().transform("EPSG:4326", seg.map.getProjection()));
				alertSource.addFeature(feature);

				return feature;
			}

			return existingFeature;
		});

	const alertsTable = seg.ui.ttt.tables.get("Active alerts");

	if (alertsTable)
		seg.ui.ttt.tables.addDataToTable(alertsTable, seg.layer.get("alerts").getSource().getFeatures());

	return featuresArray;
};

/**
 * Function called on alert fetching. May or may not call freshAlert to display that a new alert has been returned by service.
 * @param  {string[]} newAlerts String array of alerts
 * @param  {boolean} notify Flag to know if should call fresh alert function to show on top right menu
 */
const updateAlertsSource = (newAlerts, notify) => {
	const alertsLayer = seg.layer.get("alerts"),
		alertsSource = alertsLayer.getSource();

	if(notify) {
		newAlerts.forEach(alert => {
			alert.id = alert.id || alert.alertId;

			if(alertsSource.getFeatureById(alert.id) || !alert.id)
				return;

			let freshAlertLabel = "";
			const vessel = alert.vessel || alert.identities[0];

			if (vessel) {
				if(vessel.name)
					freshAlertLabel += "Name: " + vessel.name;
				else if(vessel.imo)
					freshAlertLabel += "IMO: " + vessel.imo;
				else if(vessel.mmsi)
					freshAlertLabel += "MMSI: " + vessel.mmsi;
				else if(vessel.cs)
					freshAlertLabel += "Call sign: " + vessel.cs;
				else freshAlertLabel = alert.id;
			}

			mergeDuplicatesAndAddToSource([alert]);

			const feature = alertsSource.getFeatureById(alert.id);

			seg.ui.alerts.addFreshAlert({
				label: freshAlertLabel,
				feature,
				callback() {
					alertsLayer.setVisible(true);
					seg.selection.selectFeatureAndCenter(feature);
				}
			});
		});
	}

	mergeDuplicatesAndAddToSource(newAlerts);
};

/**
 * Helper functions that parse coordinate format from ABM alerts
 * @param  {string} coords String representation of coordinates from ABM alerts
 * @return {number[]} Return coordinates array
 */
const parseCoordinates = (coords) => {
	coords = coords.replace(/[[\]RP]/g, "")
		.split(" ")
		.map(coord => coord.split(","));

	return coords.map(coord => ol.proj.transform(coord.map(n => Number(n)), "EPSG:4326", seg.map.getProjection()));
};

/**
 * Default properties to show on alerts TTT
 * @type {SEG.properties}
 */
const alertTTTFields = mapProperties({
	override: {
		id: false,
		startTime: false,
		endTime: false,
		latitude: {visible: false, order: 3},
		longitude: {visible: false, order: 4},
		abmName: false,
		vesselImo: false,
		vesselName: false,
		vesselMmsi: false,
		vesselFlag: false,
		vesselType: false
	}
});

module.exports = {
	abmNamesColors,
	alertTTTFields,
	mergeDuplicatesAndAddToSource,
	parseCoordinates,
	createMenu,
	updateAlertsSource
};