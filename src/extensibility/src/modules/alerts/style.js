const styleCache = {},
	selectedStyleCache = {},
	favouriteStyleCache = {};

const alertOpacity = (alert, baseStyle) => {
	if (baseStyle === "Target Age Opacity") {
		const diff = moment().diff(alert.get("startTime"), "m");
		if (diff >= 170)
			return 0.33;

		if (diff >= 60)
			return 0.66;

		if (diff > 10)
			return 0.88;
	}

	return 1;
};

const alertColor = (alert, baseStyle) => {
	if (baseStyle === "Target Age Color") {
		const diff =  moment().diff(alert.get("startTime"), "m");
		if ( diff >= 170)
			return "#F00";

		if (diff >= 60)
			return "#FFA500";

		if (diff > 10)
			return "#FF0";

		return "#58a54a";
	}

	return alert.get("color");
};

const selectedStyle = alert => {
	const selected = {
		radius: 36 / 2,
		stroke: alert.get("selected") ? new ol.style.Stroke({
			color: "#006ebc",
			width: 2
		}) : "",
		fill: alert.get("isSearchResult") ? new ol.style.Fill({
			color: "#58A64A"
		}) : "",
		opacity: alert.get("isSearchResult") ? 0.5 : 1
	};

	const cacheKey = JSON.stringify({
		selected: alert.get("selected"),
		isSearchResult: alert.get("isSearchResult"),
		radius: selected.radius,
		fill: selected.fill
	});

	if (!selectedStyleCache[cacheKey])
		selectedStyleCache[cacheKey] = new ol.style.Style({
			image: new ol.style.Circle(selected)
		});

	return selectedStyleCache[cacheKey];
};


module.exports = alert => {
	if(alert.get("isACQResult") && !alert.get("visible"))
		return;

	const height = 20,
		styles = [];

	const baseStyle = seg.preferences.map.get("alerts.symbolizer") || "ABM Name",
		fill = alertColor(alert, baseStyle),
		opacity = alertOpacity(alert, baseStyle);

	const cacheKey = JSON.stringify({
		fill,
		height,
		opacity
	});

	if (alert.get("selected") || alert.get("isSearchResult"))
		styles.push(selectedStyle(alert));

	if (!styleCache[cacheKey])
		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG({
				svg: require("./alert.svg"),
				fill,
				stroke: {
					color: "rgba(255,255,255,0.1)",
					width: 3
				},
				height: 20,
				opacity
			})
		});

	styles.push(styleCache[cacheKey]);

	if (seg.favourites.isFavourite(alert)) {
		const radius = 36 / 2,
			color = seg.favourites.getGroupFromFeature(alert).color || "rgb(201, 151, 38)";

		if (!favouriteStyleCache[radius, color])
			favouriteStyleCache[radius, color] = new ol.style.Style({
				image: new ol.style.Circle({
					radius,
					stroke: new ol.style.Stroke({
						color,
						width: 2
					})
				})
			});

		styles.push(favouriteStyleCache[radius, color]);
	}

	return styles;
};
