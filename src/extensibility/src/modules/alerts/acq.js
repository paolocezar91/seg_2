/**
 * Registering attributes and functions for alert ACQ
 */

const { mergeDuplicatesAndAddToSource, alertTTTFields } = require("./api.js");

module.exports = {
	name: "Alerts",
	template: resolvePath("./templates/acq.html"),
	services: {
		alert: {}
	},
	isAreaValid(selectedAreas){
		// an area limit is imposed if vessel tracks are seleted in any
		if (!Array.isArray(selectedAreas) || !selectedAreas.length){
			this.services.alert.areaStatus = {
				invalid: true,
				msg: "Select an area."
			};
			return false;
		} else {
			delete this.services.alert.areaStatus;
			return true;
		}
	},
	submit(bboxes, from, to) {
		if (this.services.alert.enabled) {
			let params, url;

			// EMSA has an older and a newer alert endpoint. Here we have a condition to change between each one, if necessary.
			if(seg.CONFIG.getValue("alerts.NEW_ALERTS")) {
				// New endpoint uses wkt polygons as parameter for bbox search
				url = seg.CONFIG.getValue("alerts.ALERTS_ENDPOINT");
				params = {
					project: "project",
					startTime: moment(from).format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
					endTime: moment(to).format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
					/**
					 * As a prefix we have the string POLYGON and between the coordinates we have double parenthesis. We take each of the vertix of the bbox. Latitude and Longitude is separated by a space, and each of the coordinates is split by a comma.
					 * Example:
					 * POLYGON((-15.510182417929173 44.76885836453538,-15.510182417929173 41.05286160285216,-22.75184165686369 41.05286160285216,-22.75184165686369 44.76885836453538,-15.510182417929173 44.76885836453538))
					 */
					wkt: "POLYGON((" + [
						ol.extent.getTopRight(bboxes[0]).join(" "),
						ol.extent.getBottomRight(bboxes[0]).join(" "),
						ol.extent.getBottomLeft(bboxes[0]).join(" "),
						ol.extent.getTopLeft(bboxes[0]).join(" "),
						ol.extent.getTopRight(bboxes[0]).join(" ")
					].join(",") + "))"
				};
			} else {
				url = seg.CONFIG.getValue("alerts.ALERTS_OLD_ENDPOINT");
				params = {
					project: "project",
					startTime: moment(from).format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
					endTime: moment(to).format("YYYY-MM-DD[T]HH:mm:ss[Z]"),
					lower_corner: ol.extent.getBottomRight(bboxes[0]).join(","),
					upper_corner: ol.extent.getTopLeft(bboxes[0]).join(",")
				};
			}

			const alertRequest = seg.request.get(url, {params}),
				loaderModal = seg.modal.openLoader("ACQ", "Retrieving Alerts...", {class: "info"});

			loaderModal.setPartialProgress("ACQ", 0);
			loaderModal.setDismissCallback(() => alertRequest.cancel());

			return alertRequest.then(res => {
				// set modal progress
				loaderModal.setPartialProgress("ACQ", 100);

				//if no reports were retrieved, return
				if (!res.result.length) {
					loaderModal.setContent("0 alert reports retrieved");
					loaderModal.setClass("no-result");
					seg.modal.closeTimeout(loaderModal.title);
					return;
				}

				const features = mergeDuplicatesAndAddToSource(res.result);

				seg.ui.queryPanel.ACQ.addFeatures(features);

				loaderModal.setContent("Retrieved " + res.result.length + " alert reports.");
				loaderModal.setClass("success");

				// Sending results to TTT
				const alertTable = seg.ui.ttt.tables.open({
					label: "ACQ alerts",
					customToolbar: {
						template: "<span class=\"text-size-sm\">Number of alerts: <b>{{grid.filteredResults.length}}</b></span>"
					},
					data: features,
					itemAs: "alert",
					fields: alertTTTFields,
					paginate: true,
					onSelectionChange(changedRows, selectedRows) {
						seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
					},
					paginate: {
						itemsPerPage: 100
					}
				});

				// Creating the ACQ cleanable
				seg.ui.queryPanel.ACQ.onClear(() => seg.ui.ttt.tables.close(alertTable));

				seg.modal.closeTimeout(loaderModal.title);
				// ACQ should return object for timeline component, which get tracks does
				let previouslySelectedFeature;

				// ACQ submit return is always sent to the timeline component.
				return {
					cluster: true,
					marker: {
						template: `
						<div class="default-marker" ng-click="selectAndCenter(items)">
							<img src="extensibility/modules/alerts/timeline_icon.png">
							<div style="background-color: #00c18e;" id="cluster-badge" ng-if="items.length > 1">{{items.length}}</div>
						</div>`,
						bindings: {
							selectAndCenter(items) {
								seg.selection.selectFeatureAndCenter(items[0].alert);
							}
						}
					},
					// TOOLTIP TO BE REMOVED
					tooltip: {
						template: `
						<ul class="default-tooltip">
							<li ng-repeat="item in items">
								<label ng-click="selectAndCenter(item.alert)">Alert: {{item.alert.getId()}}</label>
							</li>
						</ul>`,
						bindings: {
							selectAndCenter: seg.selection.selectFeatureAndCenter
						}
					},
					items: features.map(alert => {
						return {
							timestamp: moment(alert.get("startTime")),
							alert
						};
					}),
					onTimeChange(timestamp, before, after) {
						if(seg.ui.ttt.openTab === "timeline"){
							if(after.length)
								after.forEach(({alert}) => alert.set("visible", false));

							if(previouslySelectedFeature) {
								seg.selection.deselect(previouslySelectedFeature);
								previouslySelectedFeature = null;
							}

							if(before.length) {
								const currentSelectedFeature = before[before.length - 1].alert;

								if(currentSelectedFeature !== previouslySelectedFeature) {
									seg.selection.select(currentSelectedFeature, true);

									currentSelectedFeature.set("visible", true);

									previouslySelectedFeature = currentSelectedFeature;
								}
							}
						} else {
							after.forEach(({alert}) => alert.set("visible", true));
							before.forEach(({alert}) => alert.set("visible", true));
						}
					}
				};
			}, e => {
				loaderModal.setContent("No alerts were found");
				seg.modal.closeTimeout(loaderModal.title);
				seg.log.error("ERROR_ALERTS_SUBMIT", e.error + ": " + e.result);
			} , progress => loaderModal.setPartialProgress("ACQ", progress.loaded/progress.total*100));
		}
	}
};
