const lazyLoader = () => {
	seg.favourites.buildSessionMenu();
};

module.exports = (config) => {
	return {
		name: "AOI",
		layers: [
			require("./layer.js")
		],
		init() {
			const aoiLayer = seg.layer.get("aoi");
			aoiLayer.ol_.set("order", -1);

			// if(config.show_menu){
			// 	const aoiMenuItem = seg.ui.layerMenu.fromLayer("AOI", aoiLayer);
			// 	// 	fromFilters = seg.ui.layerMenu.fromFilters(aoiLayer);

			// 	// aoiMenuItem.options = aoiMenuItem.options.concat(fromFilters[0].options);
			// 	// aoiMenuItem.options[0].bindings.filter.setActive(true);
			// 	aoiMenuItem.toggle(true);
			// 	seg.ui.layerMenu.register(aoiMenuItem);
			// } else {
			// 	seg.layer.get("aoi").ol_.setVisible(true);
			// }

			// aoiLayer.ol_.on("change:visible", () => lazyLoader());
			// lazyLoader();
		}
	};
};