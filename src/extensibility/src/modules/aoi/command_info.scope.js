let linePolygonCoordinates, center, radius, centerShow, radiusShow, pattern, strokeWidthStyle, strokeOpacityStyle, fillOpacityStyle, strokeColorStyle, fillColorStyle, favourite_name;
const coordinatesShow = false;

const init = (aoi) => {
		if(aoi.getGeometry().get("type") !== "circle")
			createLinePolygonCoordinates(aoi);
		if(aoi.getGeometry().get("type") === "circle"){
			createCircle(aoi);
			createLinePolygonCoordinates(aoi);
		}
	},
	createLinePolygonCoordinates = (aoi) => {
		if(aoi.getGeometry().getType() === "LineString")
			linePolygonCoordinates = aoi
				.clone()
				.getGeometry()
				.getCoordinates()
				.map(coord => ({latLon: ol.proj.transform(coord, seg.map.getProjection(), "EPSG:4326")}));
		else
			linePolygonCoordinates = aoi
				.clone()
				.getGeometry()
				.getCoordinates()[0]
				.map(coord => ({latLon: ol.proj.transform(coord, seg.map.getProjection(), "EPSG:4326")}));
		getStyle(aoi);
	},
	redraw = (aoi) => {
		const props = {
			type: "Feature",
			properties: {},
			geometry: {
				type: aoi.getGeometry().getType(),
				coordinates: (aoi.getGeometry().getType() === "LineString") ? linePolygonCoordinates.map(coord => coord.latLon) : [linePolygonCoordinates.map(coord => coord.latLon)]
			}
		};
		const newFeature = (new ol.format.GeoJSON()).readFeature(props, {dataProjection: "EPSG:4326", featureProjection: seg.map.getProjection()});
		newFeature.getGeometry().set("type", (aoi.getGeometry().getType() === "LineString") ? "linePolygon" : "rectangle");
		aoi.setGeometry(newFeature.getGeometry());
	},
	removeRow = (row) => {
		linePolygonCoordinates.splice(linePolygonCoordinates.indexOf(row), 1);
	},
	addRow = () => {
		const lastCoord = Object.assign({}, linePolygonCoordinates[linePolygonCoordinates.length - 1] || {latLon: [0, 0]});
		linePolygonCoordinates.push(lastCoord);
	},
	createCircle = (aoi) => {
		const end = aoi.getGeometry().get("end");
		center = aoi.getGeometry().get("center") ? aoi.getGeometry().get("center") : ol.extent.getCenter(aoi.getGeometry().getExtent());
		centerShow = ol.proj.transform(center, seg.map.getProjection(), "EPSG:4326");
		radius = aoi.getGeometry().get("radius") ? aoi.getGeometry().get("radius") : Math.sqrt(seg.utils.squaredDistance(center, end));
		radiusShow = aoi.getGeometry().get("radiusShow") ? aoi.getGeometry().get("radiusShow") : formatLineString(aoi, center, end);
		pattern = radius/radiusShow;
		getStyle(aoi);
	},
	redrawCircle = (aoi) => {
		const end = aoi.getGeometry().getCoordinates()[0][0],
			geometry = ol.geom.Polygon.circular(
				ol.proj.toLonLat(center, seg.map.getProjection()),
				radius
			),
			angle = Math.atan((end[1] - center[1]) / (end[0] - center[0]));

		geometry.set("end", end);
		geometry.set("type", "circle");
		geometry.set("center", center);
		geometry.set("radius", radius);
		geometry.set("radiusShow", radiusShow);

		ol.geom.Polygon.makeRegular(geometry, center, radius, angle);
		aoi.setGeometry(geometry);
	},
	formatLineString = (aoi, c1, c2) => {
		let length = 0;
		const sourceProj = seg.map.getView().getProjection();

		c1 = ol.proj.transform(c1, sourceProj, "EPSG:3395"),
		c2 = ol.proj.transform(c1, sourceProj, "EPSG:3395"),

		length += ol.sphere.getDistance(c1, c2);

		return (Math.round(length * 100) / 100); // meters
	},
	setStrokeColorStyle = (aoi) => {
		aoi.set("strokeColorStyle", strokeColorStyle);
		aoi.set("isEditing", true);
		seg.layer.get("aoi").refresh();
	},
	setStrokeWidthStyle = (aoi) => {
		aoi.set("strokeWidthStyle", strokeWidthStyle);
		aoi.set("isEditing", true);
		seg.layer.get("aoi").refresh();
	},
	setStrokeOpacityStyle = (aoi) => {
		aoi.set("strokeOpacityStyle", strokeOpacityStyle);
		aoi.set("isEditing", true);
		seg.layer.get("aoi").refresh();
	},
	setFillOpacityStyle = (aoi) => {
		aoi.set("fillOpacityStyle", fillOpacityStyle);
		aoi.set("isEditing", true);
		seg.layer.get("aoi").refresh();
	},
	setFillColorStyle = (aoi) => {
		aoi.set("fillColorStyle", fillColorStyle);
		aoi.set("isEditing", true);
		seg.layer.get("aoi").refresh();
	},
	getStyle = (aoi) => {
		strokeWidthStyle = aoi.get("strokeWidthStyle") ? aoi.get("strokeWidthStyle") : 1.25;
		strokeColorStyle = aoi.get("strokeColorStyle") ? aoi.get("strokeColorStyle") : "#3399CC";
		strokeOpacityStyle = aoi.get("strokeOpacityStyle") ? aoi.get("strokeOpacityStyle") : 1;
		fillOpacityStyle = aoi.get("fillOpacityStyle") ? aoi.get("fillOpacityStyle") : 0.4;
		fillColorStyle = aoi.get("fillColorStyle") ? aoi.get("fillColorStyle") : "rgba(255,255,255,0.4)";
		favourite_name = aoi.get("favourite_name") ? aoi.get("favourite_name") : "";
		aoi.set("done", true);
	},
	setFavouriteName = (aoi) => {
		aoi.set("favourite_name", favourite_name);
		let favourite = (seg.favourites.getGroupFromFeature(aoi) || []).find(favourite => favourite.feature === aoi);

		if (!favourite)
			favourite = seg.favourites.getGroup("Session").find(favourite => favourite.feature === aoi);

		favourite.name = favourite_name;
		seg.favourites.saveFavourites();
	},
	downloadAOI = (aoi) => {
		const exportFormat = seg.export.exportFormats.find(({name}) => name === "SHP"),
			fields = {
				strokeWidthStyle: {},
				strokeColorStyle: {},
				strokeOpacityStyle: {},
				fillOpacityStyle: {},
				fillColorStyle: {},
				favourite_name: {}
			},
			data = [aoi],
			filename = aoi.get("favourite_name"),
			extension = "zip",
			fullName = `${exportFormat.name}_${filename}.${extension}`,
			view = {
				columns: Object.keys(fields).reduce((fields, key) => Object.assign(fields, {
					[key]: {
						label: key
					}
				}), {}),
				rows: data.map(feature => Object.keys(fields).reduce((values, key) => Object.assign(values, {
					[key]: feature.get(key)
				}), {}))
			},
			modal = seg.modal.openLoader("Export", `Exporting ${fullName}`);

		seg.export.doExport(exportFormat, data, view).then((res) => {
			//create download function to register on download manager
			const download = () => {
				const link = document.createElement("a");

				document.body.appendChild(link);

				link.href = window.URL.createObjectURL(res);
				link.download = fullName;

				link.click();

				window.URL.revokeObjectURL(link.href);
				link.remove();
			};

			seg.downloads.add({
				repeatDownload: download,
				date: moment(),
				filename,
				extension,
				size: res.size/1000
			});

			download();

			modal.setContent("Export successful");
			modal.updateProgress();
			seg.modal.closeTimeout(modal.title);
		}, e => seg.log.error("ERROR_OILSPILLS_DOWNLOAD_OILSPILL", e.error + ": " + e.result));
	},
	openFavourites = () => seg.favourites.open(),
	openACQ = () => seg.ui.queryPanel.openACQ(),
	openABM = (aoi) => {
		seg.ui.apps.close("Report");
		seg.timeout(() => {
			seg.ui.apps.get("Report").bindings.setFoiType("ABM admin");
			seg.ui.apps.open("Report");

			const service = seg.ui.apps.get("Report").bindings.report;

			seg.timeout(() => {
				service.getReportType().openFeedback(aoi.get("aoi_alert"));
				service.getReportType().toggleEditing(false);
				service.logs.selected = null;

				service.logOpen = false;
				service.reportOpen = true;
			}, 250);
		}, 250);
	};


module.exports = {
	get linePolygonCoordinates(){
		return linePolygonCoordinates;
	},
	init,
	redraw,
	removeRow,
	addRow,
	redrawCircle,
	setStrokeWidthStyle,
	setStrokeOpacityStyle,
	setFillOpacityStyle,
	setStrokeColorStyle,
	setFillColorStyle,
	setFavouriteName,
	openFavourites,
	openACQ,
	openABM,
	downloadAOI,
	coordinatesShow,
	get center(){return centerShow;},
	set center(value){center = ol.proj.transform(value, "EPSG:4326", seg.map.getProjection()); centerShow = value;},
	get radius(){return radiusShow;},
	set radius(value){radiusShow = value; radius = radiusShow*pattern;},
	get strokeWidthStyle(){return strokeWidthStyle;},
	set strokeWidthStyle(value){strokeWidthStyle = value;},
	get strokeOpacityStyle(){return strokeOpacityStyle;},
	set strokeOpacityStyle(value){strokeOpacityStyle = value;},
	get fillColorStyle(){return fillColorStyle;},
	set fillColorStyle(value){fillColorStyle = value;},
	get fillOpacityStyle(){return fillOpacityStyle;},
	set fillOpacityStyle(value){fillOpacityStyle = value;},
	get strokeColorStyle(){return strokeColorStyle;},
	set strokeColorStyle(value){strokeColorStyle = value;},
	get favourite_name(){return favourite_name;},
	set favourite_name(value){favourite_name = value;}
};