const BATCH_SIZE = 50,
	MAX_TILES = 1,
	normalizePortsResults = (ports) => {
		return ports.reduce((ports, port) => {
			let newport = {};

			// Some ports come without position, so for now gotta check it
			// TODO: ir buscar posição em outro lugar
			if(!port.locationPosition){
				return ports;
			}
			// CONVERT TO FEATURE IF NOT FEATURE
			if (port.type !== "Feature"){
				Object.assign(port, {
					latitude: port.locationPosition.latitude,
					longitude: port.locationPosition.longitude,
					name: port.locationNameWithDiacritics,
					functionsUNECE: port.functionsUNECE && port.functionsUNECE.map(func => func.function),
					alternativeLocationNames: port.alternativeLocationNames && Object.keys(port.alternativeLocationNames).map(key => port.alternativeLocationNames[key].locName).join("; "),
					loCode: port.digit5Locode.loCode
				});

				Object.assign(newport, {
					type: "Feature",
					geometry: {
						type: "Point",
						coordinates:[
							port.locationPosition.longitude,
							port.locationPosition.latitude
						]
					},
					properties: port,
					id: port.digit5Locode.loCode
				});
			} else {
				Object.assign(port.properties, {
					longitude:port.geometry.coordinates[0],
					latitude:port.geometry.coordinates[1],
					locode: port.properties.DETAILS.PORT_ID || port.properties.DETAILS.STAT_PORT,
					name: port.properties.DETAILS.NAME_ASCII,
					//XXX mapping from flag states obj - should be done dynamically in search function
					country: require("../../common/flag_states.json")[port.properties.DETAILS.COUNTRY_CODE],
					functionsUNECE: port.functionsUNECE && port.functionsUNECE.map(func => func.function),
					alternativeLocationNames: port.alternativeLocationNames && Object.keys(port.alternativeLocationNames).map(key => port.alternativeLocationNames[key].locName).join("; ")
				});

				port.id = port.properties.DETAILS.PORT_ID;

				newport = port;
			}

			return ports.concat(newport);
		}, []);
	},
	mergeDuplicates = ports => {
		const chunks = [],
			portsSource = seg.layer.get("ports").getSource();

		for(let i=0; i<ports.length; i+=BATCH_SIZE)
			chunks.push(ports.slice(i, i+BATCH_SIZE));

		const mergedChunks = chunks.map(ports => mergePorts(portsSource, ports));

		return [].concat(...mergedChunks);
	},
	mergePorts = (portsSource, ports) => ports.map(port => {
		//The new ports service brings no ID so we need to relate ports by LOCODE
		const existing = portsSource.getFeatureById(port.get("loCode"));
		return existing ? existing : port;
	}),
	portFunctions = {
		"Function not known, to be specified": "0",
		"Port": "1",
		"Rail terminal": "2",
		"Road terminal": "3",
		"Airport": "4",
		"Postal exchange office": "5",
		"Multimodal functions, ICDs etc": "6",
		"Fixed transport functions (e.g. oil platform)": "7",
		"Border crossing": "B"
	};


module.exports = {
	normalizePortsResults,
	mergeDuplicates,
	mergePorts,
	MAX_TILES,
	portFunctions
};