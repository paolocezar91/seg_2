const {getLastKnownPosition, mergeDuplicates} = require("../vessels/api"),
	mapProperties = require("./properties"),
	portFacilitiesMapProperties = require("./port_facilities/properties"),
	{portFunctions} = require("./api"),
	portTableSelection = async (changedRows, selectedRows) => {
		if (!selectedRows || !selectedRows[0])
			return;

		const modal = seg.modal.openModal("Last position", "Searching last known position of vessel...");

		const selectedVessel = selectedRows[0].item,
			latestPosition = await  await getLastKnownPosition({
				id: selectedVessel.VesselIdentification.id,
				csdId: selectedVessel.VesselIdentification.CSDID,
				imo: selectedVessel.VesselIdentification.IMONumber,
				mmsi: selectedVessel.VesselIdentification.MMSINumber
			});

		if(latestPosition) {
			const vessels = (await mergeDuplicates([latestPosition]))[0];
			seg.layer.get("positions").getSource().addFeature(vessels);
			vessels.set("visible", true);
			seg.selection.selectFeatureAndCenter(vessels);
			seg.modal.closeTimeout(modal.title);
		}
	};

module.exports = {
	displayPreferences_: mapProperties({override: false}),
	displayTooltipPreferences_: mapProperties({
		override: {
			locode: {order: 0},
			locationName: {order: 1},
			countryCode: {order: 2}
		}
	}),
	get displayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("port.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayPreferences_[key])
				self.displayPreferences_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayPreferences_;
	},
	openFavourites: seg.favourites.open,
	getLabel: (port) => {
		return port.get("DETAILS")?port.get("DETAILS").PORT_NAME:port.get("locationNameWithDiacritics");
	},
	get displayPreferencesForDisplay() {
		const preferences = this.displayPreferences;
		const result = [];

		angular.forEach(preferences, (value) => {
			if (value.visible) {
				result.push(value);
			}
		});

		return result;
	},
	toggleDisplayPreference(key) {
		//we only want to save the "selected" property, the rest is always the same
		seg.preferences.workspace.set("port.commandInfo.displayOptions." + key, !this.displayPreferences[key].visible);
	},
	get tooltipDisplayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("port.tooltip.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayTooltipPreferences_[key]) {
				self.displayTooltipPreferences_[key].visible = savedPreferences[key];
			}
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayTooltipPreferences_;
	},
	getDetails: (port) => {
		seg.request.get("v1/cld/locodes", {
			params: {
				locode: port.get("loCode")
			}
		}).then(({result}) => {
			port.setProperties(Object.assign(port.getProperties(), result[0]));
		}, e => seg.log.error("ERROR_PORTS_GET_DETAILS", e.error + ": " + e.result));
	},
	listVesselsInPort: async function(port) {
		const modal = seg.modal.openModal("Get vessels in Port", "Retrieving list of vessels in port " + (port.get("DETAILS")?port.get("DETAILS").PORT_NAME:port.get("locationNameWithDiacritics")) + "...");

		const tableFields = {
			imo: {
				label: "IMO",
				template: "<span>{{vessel.VesselIdentification.IMONumber}}</span>",
				visible: true
			},
			mmsi: {
				label: "MMSI",
				template: "<span>{{vessel.VesselIdentification.MMSINumber}}</span>",
				visible: true
			},
			name: {
				label: "Name",
				template: "<span>{{vessel.VesselIdentification.ShipName}}</span>",
				visible: true
			},
			flag: {
				label: "Flag",
				template: "<span>{{vessel.VesselIdentification.Flag}}</span>",
				visible: true
			},
			ata: {
				label: "ATA",
				template: `<span>
					<span>
						<span ng-if="vessel.ArrivalNotificationDetails.ATAPortOfCall" position-timestamp="vessel.ArrivalNotificationDetails.ATAPortOfCall"></span>
						<span ng-if="!vessel.ArrivalNotificationDetails.ATAPortOfCall">N/A</span>
					</span>
				</span>`,
				visible: true
			},
			etd: {
				label: "ETD",
				template: `<span>
					<span>
						<span ng-if="vessel.VoyageInformation.ETDFromPortOfCall" position-timestamp="vessel.VoyageInformation.ETDFromPortOfCall"></span>
						<span ng-if="!vessel.VoyageInformation.ETDFromPortOfCall">N/A</span>
					</span>
				</span>`,
				visible: true
			},
			positionInPort: {
				label: "Position in port",
				template: "<span>{{vessel.VoyageInformation.PositionInPortOfCall}}</span>",
				visible: true
			}
		};

		seg.request.get("v1/ship/ship_call/", {
			params: {
				details: "CurrentShipCallsAtEUPort",
				port_of_call: port.get("loCode")
			}
		}).then(res => {
			const {status, message} = res;

			if (!(status === "success" || status === "OK")) {
				seg.log.error("ERROR_PORT_LIST_VESSELS_IN_PORT", res.status + ": " + res.message);
				modal.setContent("<span class=\"retrieved-no-result\"><b>" + status + ": See console for details</b></span>");
			} else {
				let {result} = res;

				if (result.hasOwnProperty("Body"))
					result = result.Body;

				if (!result)
					return;

				if(result.QueryResults.PortPlusNotificationList.length){
					modal.setContent("<span class=\"retrieved-success\"><b>" + result.QueryResults.PortPlusNotificationList.length + "</b></span> results found.");

					seg.ui.ttt.tables.open({
						label: (port.get("DETAILS")?port.get("DETAILS").PORT_NAME:port.get("locationNameWithDiacritics"))+" Vessels in port",
						customToolbar: {
							template: "<span class=\"text-size-sm\">Number of vessels: <b>{{grid.filteredRows.length}}</b></span>"
						},
						data: result.QueryResults.PortPlusNotificationList,
						itemAs: "vessel",
						fields: tableFields,
						onSelectionChange: portTableSelection
					});
				}
			}

			seg.modal.closeTimeout(modal.title);
		}, e => {
			seg.modal.closeTimeout(modal.title);
			seg.log.error("ERROR_PORT_LIST_VESSELS_IN_PORT", e.error + ": " + e.result);
		});
	},
	listVesselsExpected: async function(port) {
		const modal = seg.modal.openModal("Get Expected arrivals", "Retrieving list of expected vessel arrivals in port " + (port.get("DETAILS")? port.get("DETAILS").PORT_NAME:port.get("locationNameWithDiacritics"))+ "...");

		const tableFields = {
			imo: {
				label: "IMO",
				template: "<span>{{vessel.VesselIdentification.IMONumber}}</span>",
				visible: true
			},
			mmsi: {
				label: "MMSI",
				template: "<span>{{vessel.VesselIdentification.MMSINumber}}</span>",
				visible: true
			},
			name: {
				label: "Name",
				template: "<span>{{vessel.VesselIdentification.ShipName}}</span>",
				visible: true
			},
			flag: {
				label: "Flag",
				template: "<span>{{vessel.VesselIdentification.Flag}}</span>",
				visible: true
			},
			eta: {
				label: "ETA",
				template: `<span>
					<span>
						<span ng-if="vessel.VoyageInformation.ETAToPortOfCall" position-timestamp="vessel.VoyageInformation.ETAToPortOfCall"></span>
						<span ng-if="!vessel.VoyageInformation.ETAToPortOfCall">N/A</span>
					</span>
				</span>`,
				visible: true
			}
		};

		seg.request.get("v1/ship/ship_call/", {
			params: {
				details: "ExpectedShipCallsAtEUPort",
				port_of_call: port.get("loCode")
			}
		}).then(res => {
			const {status, message} = res;

			if (!(status === "success" || status === "OK")) {
				modal.setContent("<span class=\"retrieved-no-result\"><b>" + status + ": See console for details</b></span>");
				seg.log.error("ERROR_PORT_LIST_VESSELS_EXPECTED", status + ": " + message);
			} else {
				let {result} = res;

				if (result.hasOwnProperty("Body"))
					result = result.Body;

				if (!result)
					return;

				if(result.QueryResults.PortPlusNotificationList.length){
					modal.setContent("<span class=\"retrieved-success\"><b>" + result.QueryResults.PortPlusNotificationList.length + "</b></span> results found.");

					seg.ui.ttt.tables.open({
						label: (port.get("DETAILS")?port.get("DETAILS").PORT_NAME:port.get("locationNameWithDiacritics"))+" Expected Arrivals",
						customToolbar: {
							template: "<span class=\"text-size-sm\">Number of vessels: <b>{{grid.filteredRows.length}}</b></span>"
						},
						data: result.QueryResults.PortPlusNotificationList,
						itemAs: "vessel",
						fields: tableFields,
						onSelectionChange: portTableSelection
					});
				}
			}

			seg.modal.closeTimeout(modal.title);
		}, e => {
			seg.modal.closeTimeout(modal.title);
			seg.log.error("ERROR_PORT_LIST_VESSELS_EXPECTED", e.error + ": " + e.result);
		});
	},
	listVesselsDepartures: async function(port) {
		const modal = seg.modal.openModal("Get recent departures", "Retrieving list of vessel departures in port " + (port.get("DETAILS")?port.get("DETAILS").PORT_NAME:port.get("locationNameWithDiacritics")) + "...");

		const tableFields = {
			imo: {
				label: "IMO",
				template: "<span>{{vessel.VesselIdentification.IMONumber}}</span>",
				visible: true
			},
			mmsi: {
				label: "MMSI",
				template: "<span>{{vessel.VesselIdentification.MMSINumber}}</span>",
				visible: true
			},
			name: {
				label: "Name",
				template: "<span>{{vessel.VesselIdentification.ShipName}}</span>",
				visible: true
			},
			flag: {
				label: "Flag",
				template: "<span>{{vessel.VesselIdentification.Flag}}</span>",
				visible: true
			},
			ata: {
				label: "ATA",
				template: `<span>
					<span>
						<span ng-if="vessel.ArrivalNotificationDetails.ATAPortOfCall" position-timestamp="vessel.ArrivalNotificationDetails.ATAPortOfCall"></span>
						<span ng-if="!vessel.ArrivalNotificationDetails.ATAPortOfCall">N/A</span>
					</span>
				</span>`,
				visible: true
			},
			atd: {
				label: "ATD",
				template: `<span>
					<span>
						<span ng-if="vessel.DepartureNotificationDetails.ATDPortOfCall" position-timestamp="vessel.DepartureNotificationDetails.ATDPortOfCall"></span>
						<span ng-if="!vessel.DepartureNotificationDetails.ATDPortOfCall">N/A</span>
					</span>
				</span>`,
				visible: true
			}
		};

		seg.request.get("v1/ship/ship_call/", {
			params: {
				details: "CompletedShipCallsAtEUPort",
				port_of_call: port.get("loCode")
			}
		}).then(res => {
			const {status, message} = res;

			if (!(status === "success" || status === "OK")) {
				modal.setContent("<span class=\"retrieved-no-result\"><b>" + status + ": See console for details</b></span>");
				seg.log.error("ERROR_PORTS_LIST_VESSELS_DEPARTURES", status + ": " + message);
			} else {
				let {result} = res;

				if (result.hasOwnProperty("Body"))
					result = result.Body;

				if (!result)
					return;

				if (result.QueryResults.PortPlusNotificationList.length) {
					modal.setContent("<span class=\"retrieved-success\"><b>" + result.QueryResults.PortPlusNotificationList.length + "</b></span> results found.");

					seg.ui.ttt.tables.open({
						label: (port.get("DETAILS")?port.get("DETAILS").PORT_NAME:port.get("locationNameWithDiacritics"))+" Recent departures",
						customToolbar: {
							template: "<span class=\"text-size-sm\">Number of vessels: <b>{{grid.filteredRows.length}}</b></span>"
						},
						data: result.QueryResults.PortPlusNotificationList,
						itemAs: "vessel",
						fields: tableFields,
						onSelectionChange: portTableSelection
					});
				}
			}


			seg.modal.closeTimeout(modal.title);
		}, e => {
			seg.modal.closeTimeout(modal.title);
			seg.log.error("ERROR_PORTS_LIST_VESSELS_DEPARTURES", e.error + ": " + e.result);
		});
	},
	listPortFacilities: (port) => {
		const portFacilities = Array.isArray(port.get("portFacility")) ? port.get("portFacility") : [port.get("portFacility")],
			portFacilitySource = seg.layer.get("port_facilities").getSource(),
			portFacilityData = {
				crs: {
					properties: {name: "EPSG:4326"},
					type: "name"
				},
				type: "FeatureCollection",
				features: portFacilities.map(portFacility => {
					return {
						type: "Feature",
						geometry: {
							type: "Point",
							coordinates:[
								port.get("locationPosition").longitude + 0.01, // shifting position?
								port.get("locationPosition").latitude + 0.01 // shifting position?
							]
						},
						properties: {
							description: portFacility.portFacilityDetails.description,
							loCode: portFacility.portFacilityDetails.loCode,
							name: portFacility.portFacilityDetails.name,
							lastUpdatedOn: portFacility.lastUpdateDetails.lastUpdatedOn,
							longitude: port.get("locationPosition").longitude,
							latitude: port.get("locationPosition").latitude
						}
					};
				})
			},
			portFacilityFeatures = new ol.format.GeoJSON()
				.readFeatures(portFacilityData, {
					dataProjection: "EPSG:4326",
					featureProjection: seg.map.getProjection()
				});

		portFacilitySource.addFeatures(portFacilityFeatures);

		seg.ui.ttt.tables.open({
			label: (port.get("DETAILS")?port.get("DETAILS").PORT_NAME:port.get("locationNameWithDiacritics"))+" port facilities",
			data: portFacilityFeatures,
			itemAs: "portFacility",
			fields: portFacilitiesMapProperties(false),
			onSelectionChange: (changedRows, selectedRows) => {
				seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
			}
		});

		seg.ui.cleanables.add("Clear Port Facilities", () => { // adding cleanable for imported areas
			portFacilitySource.clear();
		});
	},
	getPortFunctions: (port) => {
		return port.get("functionsUNECE") &&
			port.get("functionsUNECE")
				.map(id => {
					return Object.entries(portFunctions).find(([key, value]) => key && value.toString() === id.toString())[0];
				}).join("; ");
	}
};