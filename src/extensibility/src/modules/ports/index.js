/**
@namespace extensibility.modules.ports
@description
# Ports

## Available options
None

## Global configurations and constants
None
*/
let portsLayer;

const mapProperties = require("./properties.js"),
	{
		normalizePortsResults,
		mergeDuplicates,
		MAX_TILES
	} = require("./api");

const lazyLoader = async () => {
	if (!portsLayer.getVisible())
		return;

	if (!portsLayer.get("loading")) {
		portsLayer.set("loading", true);

		const modal = seg.modal.openModal("Loading Ports and Offshore Installations", "Loading Ports...");
		const requestExtents = seg.utils.parseExtents(seg.map.getViewportExtent(), MAX_TILES);

		seg.request.get("v1/ship/ports",{
			params: {
				minX:requestExtents[0][0],
				minY:requestExtents[0][1],
				maxX:requestExtents[0][2],
				maxY:requestExtents[0][3]
			}
		}).then(({result}) => {
			const obj = {
				"features": normalizePortsResults(result),
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:4326"
					}
				},
				"type": "FeatureCollection"
			};

			const ports = new ol.format.GeoJSON().readFeatures(obj, {
				dataProjection: "EPSG:4326",
				featureProjection: seg.map.getProjection()
			});

			portsLayer.getSource().addFeatures(mergeDuplicates(ports));

			portsLayer.set("loading", false);

			seg.shortcuts.add("ports", [
				{
					label: "See in TTT",
					callback: function (retrievedPorts) {
						let portsTable = seg.ui.ttt.tables.get("Selected Ports");
						if(!portsTable)
							portsTable = {
								label: "Selected Ports",
								customToolbar: {
									template: "<span class=\"text-size-sm\">Number of ports: <b>{{grid.filteredRows.length}}</b></span>"
								},
								data: [],
								itemAs: "port",
								fields: mapProperties({override: false})
							};

						seg.ui.ttt.tables.addDataToTable(portsTable, retrievedPorts);
						seg.ui.ttt.tables.open(portsTable);
					}
				}
			]);
		}, e => seg.log.error("ERROR_PORTS_LAZY_LOADER", e.error + ": " + e.result)).then(() => {
			seg.timeout(() => portsLayer.getSource().changed(), 100);
			modal.setContent("Ports <span class=\"retrieved-success\"><b>loaded</b></span>.");
			seg.modal.closeTimeout(modal.title);
		}, e => seg.log.error("ERROR_PORTS_LAZY_LOADER_THEN", e.error + ": " + e.result));
	}

};

module.exports = (/*config*/) => {
	return {
		name: "Ports",
		layers: [
			require("./layer.js"),
			require("./port_facilities/layer.js")
		],
		init() {
			portsLayer = seg.layer.get("ports");
			portsLayer.ol_.on("change:visible", lazyLoader);

			const portsMenuItem = seg.ui.layerMenu.fromLayer("PORTS AND OFFSHORE INSTALLATIONS", portsLayer);
			portsMenuItem.options = seg.ui.layerMenu.fromFilters(portsLayer);

			seg.ui.layerMenu.register(portsMenuItem);
			seg.map.onViewportChangeEnd(lazyLoader);
		}
	};
};
