const defaultProperties = {
	portName: {
		label: "Name",
		template: "{{::port.get('DETAILS')?port.get('DETAILS').PORT_NAME:port.get('locationNameWithoutDiacritics')}}",
		visible: true,
		order: 0
	},
	locationName:{
		label: "Location Name",
		template: "{{port.get('locationNameWithDiacritics') || port.get('DETAILS').NAME_ASCII || 'N/A'}}",//XXX this is not OK
		visible: true,
		order: 1
	},
	alternativeName:{
		label: "Alternative Name(s)",
		template: "{{(port.get('alternativeLocationNames') && port.get('alternativeLocationNames')) || port.get('DETAILS').NAME_ASCII || 'N/A'}}",//XXX this is not OK
		visible: true,
		order: 2
	},
	countryCode: {
		label: "Country",
		template: "{{::port.get('DETAILS')?port.get('DETAILS').COUNTRY_CODE:port.get('countryCode') | country}}",
		visible: true,
		order: 4
	},
	locode: {
		label: "LOCODE",
		template: "{{::port.get('DETAILS').PORT_ID || port.get('loCode') || 'N/A'}}",
		visible: true,
		order: 5
	},
	type: {
		label: "Type",
		template: "{{::port.get('type')===1?'UNECE':'SSN Specific'}}",
		visible: true,
		order: 6
	},
	function: {
		label: "Functions",
		template: "{{portCommandInfo.getPortFunctions(port)}}",
		visible: true,
		order: 7
	},
	latitude: {
		label: "Latitude",
		template: "<span [coordinate]=\"::[port.get('latitude'), 'lat']\"></span>",
		visible: true,
		order: 12
	},
	longitude: {
		label: "Longitude",
		template: "<span [coordinate]=\"::[port.get('longitude'), 'lon']\"></span>",
		visible: true,
		order: 13
	},
	inSECA: {
		label: `In SECA <button style="display: inline; margin-left:2px;" title="Sulphur Emission Control Area" class="seg primary round xs inline-block">
			<i icon="info"></i>
		</button>`,
		template: "{{::port.get('inSeca') || 'N/A'}}",
		visible: true,
		order: 9
	},
	EUBorderInspectionPost: {
		label: `EU Border Inspection Post <button style="display: inline; margin-left:2px;" title="Whether the location is an EU Border Inspection Post (BIP)" class="seg primary round xs inline-block">
			<i icon="info"></i>
		</button>`,
		template: "{{port.get('locationAttributes')?port.get('locationAttributes').bip:'N/A'}}",
		visible: true,
		order: 10
	},
	transhipmentPort: {
		label: `Transhipment Port <button style="display: inline; margin-left:2px;" title="Whether the location is in the list of designated EU ports where transhipment and/or landing operations of fishery products are authorised according to EC Regulation 1005/2008" class="seg primary round xs inline-block">
			<i icon="info"></i>
		</button>`,
		template: "{{port.get('locationAttributes')?port.get('locationAttributes').transhipmentPort:'N/A'}}",
		visible: true,
		order: 11
	},
	lastUpdatedOn: {
		label: "Last Updated On",
		template: "<span position-timestamp=\"::port.get('portFacility').lastUpdateDetails.lastUpdatedOn\"></span>",
		visible: true,
		order: 6
	}
	// createdBy:{
	// 	label: "Created By",
	// 	template: "{{port.get('createdBy') || 'N/A'}}",
	// 	visible: true,
	// 	order: 11
	// },
	// createdOn:{
	// 	label: "Created On",
	// 	template: "{{port.get('createdOn') || 'N/A'}}",
	// 	visible: true,
	// 	order: 12
	// }
};

const mapProperties = (options) => seg.utils.mapItemProperties(defaultProperties, options);

module.exports = mapProperties;
