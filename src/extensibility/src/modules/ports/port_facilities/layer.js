const mapProperties = require("./properties");

module.exports = {
	id: "port_facilities",
	name: "Port Facilities",
	type: "Vector",
	renderMode: "image",
	style: require("./style.js"),
	source: {
		type: "Vector",
		projection: "EPSG:4326"
	},
	tooltip: {
		multiTooltip: true,
		featureAs: "portFacility",
		bindings: {
			portFacilityTooltip: {
				tooltipDisplayPreferences: mapProperties({
					override: {
						name: false,
						description: false
					}
				})
			}
		},
		templateUrl: "extensibility/modules/ports/port_facilities/templates/tooltip.html"
	},
	search: {
		name: "Port Facilities",
		keys: ["name"],
		label: "name"
	}
};