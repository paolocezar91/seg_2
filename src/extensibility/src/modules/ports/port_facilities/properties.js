const defaultProperties = {
	name: {
		label: "Port Facility Name",
		template: "{{::portFacility.get('name') || 'N/A'}}",
		visible: true,
		order: 0
	},
	description: {
		label: "Description",
		template: "{{::portFacility.get('description') || 'N/A'}}",
		visible: true,
		order: 1
	},
	longitude: {
		label: "Longitude",
		template: "<span coordinate=\"::[portFacility.get('longitude'), 'lon']\"></span>",
		visible: true,
		order: 3
	},
	latitude: {
		label: "Latitude",
		template: "<span coordinate=\"::[portFacility.get('latitude'), 'lat']\"></span>",
		visible: true,
		order: 2
	}
};

const mapProperties = (options) => seg.utils.mapItemProperties(defaultProperties, options);

module.exports = mapProperties;