const {
	normalizePortsResults,
	mergeDuplicates,
	portFunctions
} = require("./api.js");

module.exports = {
	id: "ports",
	name: "Ports and Offshore Instalation",
	type: "Vector",
	renderMode: "image",
	style: require("./style.js"),
	source: {
		type: "Vector",
		projection: "EPSG:4326"
	},
	commandInfo: {
		featureAs: "port",
		bindings: {
			portCommandInfo: require("./command_info.scope.js")
		},
		//template url for this C&I
		templateUrl: "extensibility/modules/ports/templates/command_info.html"
	},
	tooltip: {
		featureAs: "port",
		bindings: {
			portTooltip: require("./command_info.scope.js")
		},
		templateUrl: "extensibility/modules/ports/templates/tooltip.html"
	},
	search: {
		name: "Ports and Offshore Instalation",
		keys: ["name", "countryName", "loCode", "locationNameWithoutDiacritics", "alternativeLocationNames"],
		label: "name",
		smartSearch: (smartSearchResults) => {
			const results = smartSearchResults.PORT_KEY;

			if (results && results.features && results.features.length) {
				const portsSource = seg.layer.get("ports").getSource();
				portsSource.getFeatures().forEach((port) => port.unset("isSmartSearchResult"));
				results.features = normalizePortsResults(results.features);

				const ports = mergeDuplicates((new ol.format.GeoJSON()).readFeatures(results, {
					dataProjection: "EPSG:4326",
					featureProjection: seg.map.getProjection()
				}));

				ports.forEach((vessel) => vessel.set("isSmartSearchResult", true));

				portsSource.addFeatures(ports);

				return ports;
			}

			return [];
		}
	},
	filters: [
		{
			name: "Function",
			inclusive: true,
			options: (() => {
				const filters = Object.entries(portFunctions).reduce((object, [key, val]) => {
					object[key] = (port) => {
						return port.get("functionsUNECE") && !!~(port.get("functionsUNECE").findIndex(value => value === val));
					};
					return object;
				}, {});

				filters["N/A"] = (port) => {
					return !port.get("functionsUNECE");
				};

				return filters;
			})()
		},
		{
			name: "Type",
			inclusive: true,
			options: {
				"UNECE": (port) => {
					return port.get("type") === 1;
				},
				"SSN Specific": (port) => {
					return port.get("type") === 0;
				}
			}
		},
		{
			name: "EU Border Inspections (BIP)",
			inclusive: true,
			options: {
				"Yes": (port) => {
					return port.get("locationAttributes") && port.get("locationAttributes").bip === "Y";
				},
				"No": (port) => {
					return port.get("locationAttributes") && port.get("locationAttributes").bip === "N";
				}
			}
		},
		{
			name: "Transhipment of fishery products",
			inclusive: true,
			options: {
				"Yes": (port) => {
					return port.get("locationAttributes") && port.get("locationAttributes").transhipmentPort === "Y";
				},
				"No": (port) => {
					return port.get("locationAttributes") && port.get("locationAttributes").transhipmentPort === "N";
				}
			}
		}

	],
	visible: false
};
