const styleCache = {},
	selectedStyleCache = {},
	favouriteStyleCache = {};

module.exports = port => {
	let height = Math.round(4400000 / seg.map.getScale()) * 12;
	const styles = [],
		type = port.get("type");

	//size adjustements to scale
	if (height > 15)
		height = 20;

	if (height < 1)
		height = 10;

	const cacheKey = JSON.stringify({type, height});

	if(!styleCache[cacheKey])
		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG({
				svg: require("./port.svg"),/*
				fill: "rgba(0,0,255,1)",
				stroke: {
					color: "rgba(255,255,255,1)",
					width: 1
				},*/
				height: height
			})
		});
	styles.push(styleCache[cacheKey]);

	if (port.get("selected")) {
		const radius = 40 / 2;
		if (!selectedStyleCache[radius])
			selectedStyleCache[radius] = new ol.style.Style({
				image: new ol.style.Circle({
					radius,
					stroke: new ol.style.Stroke({
						color: "#006ebc",
						width: 2
					})
				})
			});

		styles.push(selectedStyleCache[radius]);
	}

	if (seg.favourites.isFavourite(port)) {
		const radius = 40 / 2,
			color = seg.favourites.getGroupFromFeature(port).color || "rgb(201, 151, 38)";

		if (!favouriteStyleCache[radius,color])
			favouriteStyleCache[radius,color] = new ol.style.Style({
				image: new ol.style.Circle({
					radius,
					stroke: new ol.style.Stroke({
						color,
						width: 2
					})
				})
			});

		styles.push(favouriteStyleCache[radius,color]);
	}

	return styles;
};
