const styleCache = {};

module.exports = station => {
	let height = Math.round(4400000 / seg.map.getScale()) * 12;

	if (height > 15)
		height = 20;
	if (height < 1)
		height = 10;

	const fill = station.get("colorFeatures") || (station.get("typeName") === "TIDE_GAUGE_GPS" ? "#FF00FF" : null),
		cacheKey = JSON.stringify({
			height,
			fill
		});

	if (!styleCache[cacheKey])
		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG({
				svg: require("./emodnet-position.svg"),
				fill,
				stroke: {
					color: station.get("colorFeatures"),
					width: 1
				},
				height: height
			})
		});

	return styleCache[cacheKey];
};
