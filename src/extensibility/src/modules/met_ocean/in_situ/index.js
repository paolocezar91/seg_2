const {wfsLayersParams, wfsLayersConfig} = require("./api.js");

module.exports = {
	init: () => {
		return wfsLayersParams.map(wfsLayersConfig)
			.concat({
				id: "drift_buoy",
				name: "Drift Buoy",
				layers: [{type: "DRIFTBUOY", id: "driftbuoy_features", label: "Positions", code: "DB"}].map(wfsLayersConfig)
			}, {
				id: "hf_radar",
				name: "HF Radar",
				layers: [{type: "RADAR", id: "radar_features", label: "Positions", code: "RD"}].map(wfsLayersConfig)
			});
	}
};
