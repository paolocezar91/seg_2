//XXX Added option of bbox.
const wfsLayersParams = [
		{id: "argo_features", type: "PROFILER", label: "Argo/Profiler", code: "AP", bbox :true },
		{id: "ferrybox_features", type: "FERRYBOX", label: "Ferrybox", code: "FB", bbox :true },
		{id: "glider_features", type: "GLIDER", label: "Glider", code: "GL", bbox :true },
		{id: "mooring_features", type: "MOORING", label: "Mooring", code: "MO", bbox :true },
		{id: "bathy_messages_features", type: "BATHY_MESSAGES_GTS", label: "Bathy Messages GTS", code:"BA"},
		// {id: "ctd_profiles_features", type: "CTD_PROFILES", label: "CTD Profiles", code:"CT"},
		{id: "marine_mammal_features", type: "MARINE_MAMMAL", label: "Marine Mammal", code:"MM"},
		{id: "mini_logger_features", type: "MINI_LOGGER", label: "Mini Logger", code:"ML"},
		{id: "river_station_features", type: "RIVER_STATION", label: "River Station", code:"RF"},
		{id: "tesac_messages_gts_features", type: "TESAC_MESSAGES_GTS", label: "TESAC Messages", code:"TE"},
		{id: "tide_gauge_features", type: "TIDE_GAUGE", label: "Tide Gauge", code:"TG"},
		{id: "xbt_xctd_profiles_features", type: "XBT_XCTD_PROFILES", label: "XBT XCTD Profiles", code:"XB"},
		{id: "tide_gauge_gps_features", type: "TIDE_GAUGE_GPS", label: "Tide Gauge GPS", code:"Tide_Gauge_GPS", bbox :true},
		{id: "Profiling Mooring", type: "PROFILING_MOORING", label: "Profiling Mooring", code:"MOPR"}
	],
	wfsLayersConfig = (config) => {
		return {
			id: config.id,
			name: config.label,
			type: "Vector",
			source: {
				type: "Vector",
				name: config.label,
				async loader(extent) {
					seg.layer.get(config.id).getSource().addFeatures(await loadFeatures(extent, config.type, config.bbox));
				}
			},
			visible: false,
			style: require("./style.js"),
			projection: "EPSG:4326",
			commandInfo: {
				featureAs: "insitu",
				bindings: {
					inSituCommandInfo: require("./command_info.scope.js")
				},
				//template url for this C&I
				templateUrl: "extensibility/modules/met_ocean/in_situ/templates/command_info.html"
			},
			tooltip: {
				featureAs: "insitu",
				bindings: {
					insituTooltip: require("./command_info.scope.js")
				},
				templateUrl: "extensibility/modules/met_ocean/in_situ/templates/tooltip.html"
			}
		};
	},
	loadFeatures = async (extent, typeName, bbox) => {
		//XXX Added option of bbox.
		const params = {
			request: "GetFeature",
			service: "wfs",
			version: "1.0.0",
			typeName
		};

		if (bbox)
			params.bbox = ol.proj.transformExtent(extent, seg.map.getProjection(), "EPSG:4326").join(",");

		const res = await seg.request.get("v1/extension/wfs?", {
			params
		});

		if(typeName === "TIDE_GAUGE_GPS")
			res.result.features.forEach(feature => {
				feature.id = Math.random();
			});

		const stations = new ol.format.GeoJSON().readFeatures(res.result, {
			featureProjection: seg.map.getProjection()
		});

		stations.forEach((station, i) => {
			const coordinates = res.result.features[i].geometry.coordinates;

			station.set("latitude", coordinates[0]);
			station.set("longitude", coordinates[1]);
			station.set("typeName", typeName);

			return station;
		});

		return stations;
	},
	createInSituMenuItem = (inSituLayer) => {
		return Object.assign(seg.ui.layerMenu.fromLayer("In Situ", inSituLayer), {
			options: wfsLayersParams.map(type => seg.ui.layerMenu.fromLayer(type.label, seg.layer.get(type.id))),
			info: {
				title: "In-situ",
				templateUrl: "extensibility/modules/met_ocean/in_situ/templates/menu_legend.html"
			}
		});
	};


module.exports = {
	wfsLayersParams,
	wfsLayersConfig,
	loadFeatures,
	createInSituMenuItem
};