const mapProperties = require("./properties"),
	properties = mapProperties({
		override: {
			latitude: false,
			longitude: false,
			value: false,
			date: false,
			depth: false,
			code: false,
			routeType: false,
			qc: false,
			platformInfoLink: {
				visible: true
			}
		}
	});

module.exports = {
	displayPreferences_: properties,
	displayTooltipPreferences_: properties,
	get displayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("drift_buoy.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayPreferences_[key])
				self.displayPreferences_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayPreferences_;
	},
	openFavourites: seg.favourites.open,
	get displayPreferencesForDisplay() {
		const preferences = this.displayPreferences;
		const result = [];

		angular.forEach(preferences, (value) => {
			if (value.visible) {
				result.push(value);
			}
		});

		return result;
	},
	toggleDisplayPreference(key) {
		//we only want to save the "selected" property, the rest is always the same
		seg.preferences.workspace.set("drift_buoy.commandInfo.displayOptions." + key, !this.displayPreferences[key].visible);
	},
	get tooltipDisplayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("drift_buoy.tooltip.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayTooltipPreferences_[key]) {
				self.displayTooltipPreferences_[key].visible = savedPreferences[key];
			}
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayTooltipPreferences_;
	}
};
