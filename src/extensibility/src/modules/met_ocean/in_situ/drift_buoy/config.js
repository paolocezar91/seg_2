const TEMPERATURE_GRADIENT = ["-3","-2.37","-1.74","-1.11","-0.48","0.15","0.78","1.41","2.04","2.67","3.3","3.93","4.56","5.19","5.82","6.45","7.08","7.71","8.34","8.97","9.6","10.23","10.86","11.49","12.12","12.75","13.38","14.01","14.64","15.27","15.9","16.53","17.16","17.79","18.42","19.05","19.68","20.31","20.94","21.57","22.2","22.83","23.46","24.09","24.72","25.35","25.98","26.61","27.24","27.87","28.5","29.13","29.76","30.39","31.02","31.65","32.28","32.91","33.54","34.17","34.8","35.43","36.06"];
const configLayers = [{
	"id": "emodnet:route_db_temp_7d",
	"endpoint": "v1/extension/wms/driftBuoy",
	"name": "Drift Buoy (7 days)",
	"layer": "emodnet:route_db_temp_7d",
	"title": "Drift Buoy (7 days)",
	"info": `<div style="position:relative; height: 100%;">
		<span style="top: 0; position: absolute; font-weight:bold;">${TEMPERATURE_GRADIENT[TEMPERATURE_GRADIENT.length - 1]}º</span>
		<span style="bottom: 0; position: absolute; font-weight:bold;">${TEMPERATURE_GRADIENT[0]}º</span>
	</div>`,
	"env": ["var:value"]
		.concat(Object.entries(TEMPERATURE_GRADIENT).map(([key, value]) => ("v" + key + ":" + value))).join(";")
},
{
	"id": "emodnet:route_db_temp_60d",
	"endpoint": "v1/extension/wms/driftBuoy",
	"name": "Drift Buoy (60 days)",
	"layer": "emodnet:route_db_temp_60d",
	"title": "Drift Buoy (60 days)",
	"info": `<div style="position:relative; height: 100%;">
		<span style="top: 0; position: absolute; font-weight:bold;">${TEMPERATURE_GRADIENT[TEMPERATURE_GRADIENT.length - 1]}º</span>
		<span style="bottom: 0; position: absolute; font-weight:bold;">${TEMPERATURE_GRADIENT[0]}º</span>
	</div>`,
	"env": ["var:value"]
		.concat(Object.entries(TEMPERATURE_GRADIENT).map(([key, value]) => ("v" + key + ":" + value))).join(";")
}];

module.exports = {
	TEMPERATURE_GRADIENT,
	configLayers
};