const mapProperties = require("./properties"),
	tracksTooltipDisplayPreferences_ = mapProperties(false);

const {
	findSelectedLayer,
	getLegend,
	extractEndpoints
} = require("../../api.js");

const {
	TEMPERATURE_GRADIENT,
	configLayers
} = require("./config");

let driftBuoyTracksLayer, preventClear;

const createDriftBuoyWMSConfig = async () => {
	const capabilities = {};

	const config = [{
		"name": "Tracks",
		"id": "drift_buoy_wms",
		"layers": configLayers
	}];

	const requests = extractEndpoints(config).map(endpoint => {
		return seg.layer.getCapabilities(endpoint, "1.1.0")
			.then(capability => capabilities[endpoint] = capability,
				e => seg.log.error("ERROR_MET_OCEAN_GET_CAPABILITIES", "Failed to get capabilities for endpoint " + endpoint, {
					error: e,
					endpoint
				})
			);
	});

	const driftBuoyTracksLayerConfig = {
		id: "drift_buoy_tracks",
		name: "Drift Buoy Tracks (Features)",
		type: "Vector",
		renderMode: "image",
		style: require("./style.js"),
		source: {
			name: "Drift Buoy Tracks",
			type: "Vector",
			projection: "EPSG:4326"
		},
		tooltip: {
			featureAs: "trackPosition",
			bindings: {
				get tracksTooltipDisplayPreferences() {
					/*
					extend default preferences with saved ones
					so we always get latest value from saved preferences
					*/
					const savedPreferences = seg.preferences.workspace.get("drift_buoy_tracks.tooltip.displayOptions") || {};

					Object.keys(savedPreferences).forEach((key) => {
						if (tracksTooltipDisplayPreferences_[key])
							tracksTooltipDisplayPreferences_[key].selected = savedPreferences[key];
					});
					/*
					we have to always return the same object because of angular"s dirty checking
					(it needs to add a $$hashKey property to the watched object)
					*/
					return tracksTooltipDisplayPreferences_;
				}
			},
			templateUrl: resolvePath("./templates/tooltip.html")
		},
		commandInfo: {
		//alias to access the selected Vessel
			featureAs: "trackPosition",
			bindings: {
				driftBuoyCommandInfo: require("./command_info.scope.js")
			},
			//template url for this C&I
			templateUrl: resolvePath("./templates/command_info.html")
		},
		filters:[/*{
			name: "",
			options: {
				"T-AIS": function(track) {
					return track.get("source") === "T-AIS";
				}
			}
		}*/]
	};

	return await seg.promise.all(requests).then(() => {
		return [driftBuoyTracksLayerConfig, ...createConfigFromLayers("drift_buoy_wms", config, capabilities)];
	}, e => seg.log.error("ERROR_CREATE_DRIFT_BUOY_WMS_CONFIG", e.error + ": " + e.result));

};

const getFeatureInfo = (layer, {layers, position, image_size, extent}) => {
	return seg.request.get(layer.getSource().getUrls()[0], {
		params: {
			service: "WMS",
			request: "GetFeatureInfo",
			layers,
			styles: "rainbow.Line.pal",
			format: "image/png",
			transparent: true,
			maxZoom: 20,
			elevation: "0/0",
			cql_filter: "INCLUDE",
			env: ["var:value"].concat(Object.entries(TEMPERATURE_GRADIENT).map(([key, value]) => ("v" + key + ":" + value))).join(";"),
			info_format: "application/json",
			routeNoCache: moment().format("YYYY-MM-DD"),
			version: "1.1.1",
			width: image_size[0],
			height: image_size[1],
			srs:  "EPSG:900913",
			bbox: extent.join(","),
			query_layers: layers,
			X: position[0],
			Y: position[1],
			feature_count: 50
		}
	});
};

const getFeatureTracks = (driftBuoyFeature, {layers}) => {
	return seg.request.get("v1/extension/wfs/driftBuoy", {
		params: {
			service: "WFS",
			version: "1.0.0",
			request: "GetFeature",
			outputFormat: "application/json",
			SrsName: "EPSG:4326",
			CQL_FILTER: "platformid=" + driftBuoyFeature.get("platformid") + " AND qc=1",
			typeName: layers
		}
	}).then(featureTracks => {
		if (featureTracks)
			return featureTracks;
	}, e => seg.log.error("ERROR_DRIFT_BUOY_GET_FEATURE_TRACKS", e.error + ": " + e.result));
};

const buoyGetFeatureInfoEvent = () => {
	driftBuoyTracksLayer = seg.layer.get("drift_buoy_tracks");

	const map = seg.map;

	map.ol_.on("click", (e) => {
		const position = e.pixel,
			image_size = map.ol_.getSize(),
			extent = ol.proj.transformExtent(map.getView().calculateExtent(image_size), map.getProjection(), "EPSG:900913");

		preventClear = false;

		map.ol_.forEachLayerAtPixel(position, async (layer) => {
			const id = layer.get("id");

			if(
				id === "emodnet:route_db_temp_7d" ||
				id === "emodnet:route_db_temp_60d"
			) {
				preventClear = true;

				const modal = seg.modal.openModal("Drift Buoy GetFeatureInfo", "Requesting Drift Buoy tracks data...", {class: "info"}),
					layers = layer.getSource().getParams().LAYERS,
					request = getFeatureInfo(layer, {layers, position, image_size, extent});

				modal.setDismissCallback(() => {
					request.cancel();
				});

				request.then((results) => {
					if (results.features.length) {
						const driftBuoyFeatures = (new ol.format.GeoJSON()).readFeatures(results, {
							dataProjection: "EPSG:4326",
							featureProjection: seg.map.getProjection()
						});

						getFeatureTracks(driftBuoyFeatures[0], {layers}).then(tracksMetaData => {
							const driftBuoyTracks = (new ol.format.GeoJSON()).readFeatures(tracksMetaData, {
								dataProjection: "EPSG:4326",
								featureProjection: seg.map.getProjection()
							});

							driftBuoyTracks.forEach(track => {
								const position = ol.proj.transform(track.getGeometry().getCoordinates()[0], seg.map.getProjection(), "EPSG:4326");
								track.set("hidden", false);
								track.set("driftBuoyFeature", driftBuoyFeatures);
								track.set("latitude", position[1]);
								track.set("longitude", position[0]);
								track.set("platformInfoLink", "http://www.emodnet-physics.eu/map/spi.aspx?id=" + track.get("platformid"));
							});

							driftBuoyTracksLayer.getSource().clear();
							driftBuoyTracksLayer.getSource().addFeatures(driftBuoyTracks);
							seg.selection.select(driftBuoyTracks[0]);
							modal.setContent("Drift Buoy tracks data retrieved.");
						}, e => seg.log.error("ERROR_DRIFT_BUOY_GET_FEATURE_INFO_EVENT_GET_FEATURE_TRACKS", e.error + ": " + e.result));
					} else {
						modal.setContent("No Drift Buoy tracks retrieved.");
					}
					seg.modal.closeTimeout(modal.title);
				}, e => seg.log.error("ERROR_DRIFT_BUOY_GET_FEATURE_INFO_EVENT", e.error + ": " + e.result));
			} else {
				if(!preventClear)
					driftBuoyTracksLayer.getSource().clear();
			}
		});
	});
};

const createConfigFromLayers = (group, config, capabilities) => {
	return config.map(layer => {
		let _config = {
			id: layer.id,
			name: layer.name
		};

		if(layer.layers)
			_config.layers = createConfigFromLayers(group, layer.layers, capabilities);
		else {
			Object.assign(_config, {
				type: "Tile",
				visible: !!seg.preferences.map.get("layers." + _config.id + ".visible"),
				source: {
					type: "TileWMS",
					url: layer.endpoint,
					params: {
						LAYERS: layer.layer,
						STYLES: "rainbow.Line.pal",
						routeNoCache: moment().format("YYYY-MM-DD"),
						env: layer.env
					}
				},
				info: layer.info,
				link: layer.more_info
			});

			_config = enhanceConfigWithCapabilities(group, _config, capabilities[layer.endpoint]);
		}

		return _config;
	//remove undefined layers
	}).filter(layer => layer);
};

const createDriftBuoyMenuOptions = (menuItem, layer) => {
	if(layer.getLayers)
		Object.assign(menuItem, {
			options: layer.getLayers().filter(subLayer => subLayer.get("id") !== "drift_buoy_tracks").map(subLayer => {
				const subLayerMenuItem = seg.ui.layerMenu.fromLayer(subLayer.get("name"), subLayer);
				return Object.assign(createDriftBuoyMenuOptions(subLayerMenuItem, subLayer), {
					template: "<label class=\"drift-buoy-menu-item\"><span class=\"flex-1\">{{layer.get('name')}}</span></label>"
				});
			})
		});

	return menuItem;
};

const createDriftBuoyMenuItem = driftBuoyLayer => {
	return Object.assign(seg.ui.layerMenu.fromLayer(driftBuoyLayer.get("name"), driftBuoyLayer), {
		info: {
			title: {
				template: "{{getLayerName()}}",
				bindings: {
					getLayerName() {
						return findSelectedLayer(driftBuoyLayer.getLayers()).get("name");
					}
				}
			},
			bindings: {
				isValidLayer() {
					return !!findSelectedLayer(driftBuoyLayer.getLayers()).getSource;
				},
				getSelectedLayer() {
					return findSelectedLayer(driftBuoyLayer.getLayers());
				},
				getLegend(layer) {
					return getLegend(layer, {width: 25, height: 170});
				},
				style(val) {
					const selectedSource = findSelectedLayer(driftBuoyLayer.getLayers()).getSource();

					if(typeof val === "undefined")
						return selectedSource.getParams().STYLES;

					selectedSource.updateParams({
						STYLES: val
					});
				}
			},
			disabled() {
				return !findSelectedLayer(driftBuoyLayer.getLayers());
			},
			template: `<div ng-if="isValidLayer()" id="drift-buoy-legend">
				<div class="flexbox">
					<div>
						<combo id="style-picker" ng-if="getSelectedLayer().get('styles').length > 1" ng-model="style" ng-model-options="{getterSetter: true}" options="style for style in getSelectedLayer().get('styles')"></combo>
						<img ng-src="{{getLegend(getSelectedLayer())}}">
					</div>
					<div id="info" ng-if="getSelectedLayer().get('info')">
						<include template="{{getSelectedLayer().get('info')}}"</include>
						<p ng-if="getSelectedLayer().get('link')">
							<a ng-href="{{getSelectedLayer().get('link')}}" target="_blank">More info</a>
						</p>
					</div>
				</div>
			</div>
			<div ng-if="!isValidLayer()">Please select a Remote Sensing Layer</div>
			<style>
				#drift-buoy-legend #style-picker {
					margin-bottom: 10px;
				}
				#drift-buoy-legend #info {
					margin-left: 10px;
					max-width: 200px;
				}
			</style>`
		}
	});
};

const enhanceConfigWithCapabilities = (group, layer, capabilities) => {
	if (!capabilities){
		seg.log.error("metocean", "No capabilities defined for " + layer.name, {
			layer,
			capabilities
		});
		return;
	}

	const layerCapabilities = seg.layer.findLayerInCapabilities(layer, capabilities);

	if (!layerCapabilities)
		throw seg.log.error("metocean", "could not find layer " + layer.name + " in capabilities", {
			layer,
			capabilities
		});

	layer.source.projections = ["EPSG:900913"];
	layer.source.params.VERSION = capabilities.version;

	Object.assign(layer, {
		legendCache: {},
		legendRequests: {}
	});

	return layer;
};

module.exports = {
	createDriftBuoyMenuItem,
	createDriftBuoyMenuOptions,
	createDriftBuoyWMSConfig,
	buoyGetFeatureInfoEvent
};