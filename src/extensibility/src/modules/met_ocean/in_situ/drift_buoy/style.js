module.exports = trackPosition => {
	if (!trackPosition.get("hidden")) {
		const color = [255,255,255],
			width = 15;

		return new ol.style.Style({
			stroke: new ol.style.Stroke({
				color,
				width
			})
		});
	} else
		return;
};