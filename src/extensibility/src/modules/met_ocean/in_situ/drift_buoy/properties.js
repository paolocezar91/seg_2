const defaultProperties = {
	latitude: {
		label: "Latitude",
		template: "<span coordinate=\"[trackPosition.get('latitude'), 'lat']\"></span>",
		visible: true,
		order: 0
	},
	longitude: {
		label: "Longitude",
		template: "<span coordinate=\"[trackPosition.get('longitude'), 'lon']\"></span>",
		visible: true,
		order: 1
	},
	value: {
		label: "Value",
		template: "{{::trackPosition.get('value')}}º",
		visible: true,
		order: 2
	},
	date: {
		label: "Date",
		template: "<span position-timestamp=\"trackPosition.get('date')\"></span>",
		visible: true,
		order: 3
	},
	depth: {
		label: "Depth",
		template: "{{trackPosition.get('depth')}}",
		visible: true,
		order: 4
	},
	code: {
		label: "Code",
		template: "{{::trackPosition.get('code')}}",
		visible: true,
		order: 5
	},
	routeType: {
		label: "Route type",
		template: "{{::trackPosition.get('routetype')}}",
		visible: true,
		order: 6
	},
	qc: {
		label: "QC",
		template: "{{::trackPosition.get('qc')}}",
		visible: true,
		order: 7
	},
	platformInfoLink: {
		label: "Platform Info Link:",
		template: "<a href=\"{{trackPosition.get('platformInfoLink')}}\" target='blank_' style='display:flex'><i icon='emodnet-link'></i>EMODnet<i icon='arrow-link'></i></a>",
		visible: false,
		order: 8
	}
};

const mapProperties = (options) => seg.utils.mapItemProperties(defaultProperties, options);

module.exports = mapProperties;