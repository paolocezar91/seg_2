module.exports = {
	displayPreferences_: {
		fId: {
			label: "fID",
			template: "{{insitu.get('fid')}}",
			visible: false
		},
		platformId: {
			label: "Platform ID",
			template: "{{insitu.get('platformFid')}}",
			visible: false
		},
		platformCode: {
			label: "Platform Code",
			template: "{{insitu.get('platformCode')}}",
			visible: true
		},
		platformType:{
			label: "Platform Type",
			template: "{{inSituCommandInfo.getPlatformType(insitu)}}",
			visible: true
		},
		platformInfoLink: {
			label: "Link to Data",
			template: "<a href=\"{{insitu.get('platformInfoLink')}}\" target='blank_' style='display:flex'><i icon='emodnet-link'></i>EMODnet<i icon='arrow-link'></i></a>",
			visible: true
		},
		seaRegionCode: {
			label: "Sea Region Code",
			template: "{{insitu.get('seaRegionCode')}}",
			visible: true
		},
		seaRegionDescr: {
			label: "Sea Region Descr",
			template: "{{insitu.get('seaRegionDescr')}}",
			visible: true
		},
		parametersGroupCode: {
			label: "Group Code",
			template: "<emodnet-parameter code=\"insitu.get('parametersGroupDescr')\"></emodnet-parameter>",
			visible: true
		},
		parametersCodeDescr: {
			label: "Parameters Code Descr",
			template: "{{insitu.get('parametersCodeDescr')}}",
			visible: false
		},
		lastDataMeasured: {
			label: "Last Data Measured",
			template: "<span position-timestamp=\"insitu.get('lastDataMeasured')\"></span>",
			visible: true
		},
		country: {
			label: "Country",
			template: "{{insitu.get('country')}}",
			visible: true
		},
		dataOwner: {
			label: "Data Owner",
			template: "{{insitu.get('dataOwner')}}",
			visible: true
		},
		dataType: {
			label: "Data Type",
			template: "{{insitu.get('dataTypeDescr')}}",
			visible: true
		},
		dataProvider: {
			label: "Data Provider",
			template: "{{insitu.get('dataProvider')}}",
			visible: true
		},
		latitude: {
			label: "Latitude",
			template: "<span coordinate='[insitu.get(\"latitude\"), \"lat\"]'></span>",
			visible: true
		},
		longitude: {
			label: "Longitude",
			template: "<span coordinate='[insitu.get(\"longitude\"), \"lon\"]'></span>",
			visible: true
		}
	},
	displayTooltipPreferences_: {
		fId: {
			label: "fID:",
			template: "{{insitu.get('fid')}}",
			visible: true
		},
		seaRegionCode: {
			label: "Sea Region Code:",
			template: "{{insitu.get('seaRegionCode')}}",
			visible: false
		},
		seaRegionDescr: {
			label: "Sea Region Descr:",
			template: "{{insitu.get('platformFid')}}",
			visible: false
		},
		parametersGroupCode: {
			label: "Group Code:",
			template: "<emodnet-parameter code=\"insitu.get('parametersGroupDescr')\"></emodnet-parameter>",
			visible: true
		},
		parametersCodeDescr: {
			label: "Parameters Code Desc:r",
			template: "{{insitu.get('parametersCodeDescr')}}",
			visible: false
		},
		lastDataMeasured: {
			label: "Last measurement:",
			template: "<span timestamp=\"insitu.get('lastDataMeasured')\"></span>",
			visible: true
		},
		country: {
			label: "Country:",
			template: "{{insitu.get('country')}}",
			visible: false
		},
		dataOwner: {
			label: "Data Owner:",
			template: "{{insitu.get('dataOwner')}}",
			visible: false
		},
		dataProvider: {
			label: "Data Provider:",
			template: "{{insitu.get('dataProvider')}}",
			visible: false
		},
		platformInfoLink: {
			label: "Platform Info Link:",
			template: "<a href=\"{{insitu.get('platformInfoLink')}}\" target='blank_'>Click Here</a>",
			visible: false
		}
	},
	get displayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest template from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("inSitu.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {

			if (self.displayPreferences_[key])
				self.displayPreferences_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayPreferences_;
	},
	getLabel: function(insitu) {
		return insitu.get("platformCode")+" "+this.getPlatformType(insitu);
	},
	getPlatformType: function(insitu) {
		switch(insitu.get("platformTypeCode")) {
			case "AP": return "Argo/Profiling";
			case "BA": return "Bathy Messages GTS";
			case "CT": return "CTD Profiles";
			case "DB": return "Drift Buoy";
			case "FB": return "Ferry Box";
			case "GL": return "Glider";
			case "ML": return "Mini Logger";
			case "MM": return "Marine Mammal";
			case "RD": return "Radar";
			case "RF": return "River Station";
			case "MO": return "Mooring";
			case "MOPR": return "Profiling Mooring";
			case "PF": return "Profiler";
			case "Tide_Gauge_GPS": return "Tide Gauge GPS";
			case "TE": return "TESAC Messages";
			case "TG": return "Tide Gauge";
			case "XB": return "XBT XCTD Profiles";
		}
	},
	get displayPreferencesForDisplay() {
		const preferences = this.displayPreferences;
		const result = [];

		angular.forEach(preferences, (template) => {
			if (template.visible) {
				result.push(template);
			}
		});

		return result;
	},
	toggleDisplayPreference(key) {
		//we only want to save the 'visible' property, the rest is always the same
		seg.preferences.workspace.set("inSitu.commandInfo.displayOptions." + key, !this.displayPreferences[key].visible);
	},
	get tooltipDisplayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest template from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("inSitu.tooltip.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayTooltipPreferences_[key]) {
				self.displayTooltipPreferences_[key].visible = savedPreferences[key];
			}
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayTooltipPreferences_;
	}
};
