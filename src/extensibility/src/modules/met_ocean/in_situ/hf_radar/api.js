const DEFAULT_RATIO = 1;

const {
	findSelectedLayer,
	getLegend,
	enhanceConfigWithCapabilities,
	extractEndpoints,
	WMSTimeToPeriods,
	getMomentsInPeriods
} = require("../../api.js");

const {configLayers} = require("./config");

const createHFRadarWMSConfig = async () => {
	const capabilities = {};

	const config = [{
		"name": "Areas (Last 60 days)",
		"id": "hf_radar_wms",
		"layers": configLayers
	}];

	config[0].layers = (await seg.promise.all(config[0].layers.map(getMetadata))).filter(layer => layer);

	const requests = extractEndpoints(config).map(endpoint => {
		return seg.layer.getCapabilities(endpoint)
			.then(capability => capabilities[endpoint] = capability,
				e => seg.log.error("ERROR_HF_RADAR_CREATE_HF_RADAR_WMS_CONFIG_GET_CAPABILITIES", "Failed to get capabilities for endpoint " + endpoint, {
					error: e,
					endpoint
				})
			);
	});

	return await seg.promise.all(requests).then(() => {
		return createConfigFromLayers("hf_radar_wms", config, capabilities);
	}, e => seg.log.error("ERROR_HF_RADAR_CREATE_HF_RADAR", e.error + ": " + e.result));
};

const getMetadata = (layer) => {
	return seg.request.get(layer.endpoint, {
		params: {
			service: "WMS",
			version: "1.3.0",
			request: "GetMetadata",
			item: "layerDetails",
			layerName: layer.layer
		},
		timeout: 10 * 1000
	}).then(result => {
		if (typeof result === "object")
			return Object.assign(layer, {
				colorscalerange: result.scaleRange,
				supportedStyles: result.supportedStyles,
				units: result.units,
				additionalParameters: {
					routeNoCache: moment().format("YYYY-MM-DD"),
					colorscalerange: result.scaleRange
				},
				bbox: result.bbox.map(value => Number(value)), // parse to long
				customLoadFunction: (layer) => async (img, url) => {
					const params = seg.utils.getParams(url),
						extent = ol.proj.transformExtent(seg.map.getViewportExtent(), seg.map.getProjection(), "EPSG:4326"),
						transparent = !(
							ol.extent.containsCoordinate(extent, [layer.bbox[0], layer.bbox[1]]) ||
							ol.extent.containsCoordinate(extent, [layer.bbox[2], layer.bbox[3]])
						);

					img.getImage().src = transparent ? seg.source.transparent1x1Image() : (await seg.request.getDataURL(seg.utils.addParamsToURL(url, params)));
				}
			});
	}, e => {
		seg.log.error("ERROR_HF_RADAR_" + layer.name.toUpperCase() + "_GET_METADATA", e.error + ": " + e.result);
		return;
	});
};

const createConfigFromLayers = (group, config, capabilities) => {
	return config.map(layer => {
		let _config = {
			id: layer.id,
			name: layer.name
		};

		if(layer.layers)
			_config.layers = createConfigFromLayers(group, layer.layers, capabilities);
		else {
			Object.assign(_config, {
				type: "Image",
				visible: !!seg.preferences.map.get("layers." + _config.id + ".visible"),
				source: {
					type: "ImageWMS",
					url: layer.endpoint,
					params: {
						LAYERS: layer.layer
					},
					// Check if viewport contains the bbox defined on metadata, if it does, then download wms image, if it does not, then load transparent images
					customLoadFunction: layer.customLoadFunction(layer)
				},
				info: layer.info,
				link: layer.more_info,
				styleDefault: "vector/rainbow",
				projectionsDefault: ["EPSG:3857"],
				calendar: !!(layer.Calendar || layer.calendar)
			});

			if(layer.aditionalParameters)
				Object.assign(_config.source.params, layer.aditionalParameters.split("&").reduce((params, str) => {
					const [key, val] = str.split("=");
					params[key] = val;
					return params;
				}, {}));

			_config = enhanceConfigWithCapabilities(group, _config, capabilities[layer.endpoint]);
		}

		return _config;
	//remove undefined layers
	}).filter(layer => layer);
};

const createHFRadarMenuItem = hfRadarLayer => {
	return Object.assign(seg.ui.layerMenu.fromLayer(hfRadarLayer.get("name"), hfRadarLayer), {
		info: {
			title: {
				template: "{{getLayerName()}}",
				bindings: {
					getLayerName() {
						return findSelectedLayer(hfRadarLayer.getLayers()).get("name");
					}
				}
			},
			bindings: {
				isValidLayer() {
					return !!findSelectedLayer(hfRadarLayer.getLayers()).getSource;
				},
				getSelectedLayer() {
					return findSelectedLayer(hfRadarLayer.getLayers());
				},
				getLegend(layer) {
					return getLegend(layer, {width: 25, height: 170});
				},
				style(val) {
					const selectedSource = findSelectedLayer(hfRadarLayer.getLayers()).getSource();

					if(typeof val === "undefined")
						return selectedSource.getParams().STYLES;

					selectedSource.updateParams({
						STYLES: val
					});
				}
			},
			disabled() {
				return !findSelectedLayer(hfRadarLayer.getLayers());
			},
			template: `<div ng-if="isValidLayer()" id="hf-radar-legend">
				<div class="flexbox">
					<div>
						<combo id="style-picker" ng-if="getSelectedLayer().get('styles').length > 1" ng-model="style" ng-model-options="{getterSetter: true}" options="style for style in getSelectedLayer().get('styles')"></combo>
						<img ng-src="{{getLegend(getSelectedLayer())}}">
					</div>
					<div id="info" ng-if="getSelectedLayer().get('info')">
						<include template="{{getSelectedLayer().get('info')}}"</include>
						<p ng-if="getSelectedLayer().get('link')">
							<a ng-href="{{getSelectedLayer().get('link')}}" target="_blank">More info</a>
						</p>
					</div>
				</div>
			</div>
			<div ng-if="!isValidLayer()">Please select a Remote HF Radar Layer</div>
			<style>
				#hf-radar-legend #style-picker {
					margin-bottom: 10px;
				}
				#hf-radar-legend #info {
					margin-left: 10px;
					max-width: 200px;
				}
			</style>`
		}
	});
};

const openTimeline = layer => {
	const source = layer.getSource(),
		periods = WMSTimeToPeriods(layer.get("availablePeriods")),
		imageLoadFunction = source.getImageLoadFunction(),
		resizeListener = seg.map.onViewportSizeChange(refreshFrames);

	let currentTimestamp,
		requestTimeout,
		frameCache = {};

	const cacheFrames = timestamps => timestamps.forEach(timestamp => frameCache[timestamp] = seg.request.getDataURL(buildRequestUrl(timestamp)));

	const cacheDates = datesToCache => {
		Object.keys(frameCache).forEach(timestamp => {
			if(datesToCache.some(dateToCache => dateToCache.isSame(timestamp)))
				return;

			if(frameCache[timestamp]) {
				frameCache[timestamp].cancel();

				delete frameCache[timestamp];
			}
		});

		cacheFrames(datesToCache.filter(timestamp => !frameCache[timestamp.toISOString()]).map(timestamp => timestamp.toISOString()));
	};

	//adapted from openlayers ImageWMS source code
	const buildRequestUrl = timestamp => {
		const source = layer.getSource(),
			url = source.getUrl(),
			_params = source.getParams(),
			projection = source.getProjection(),
			v13 = $o.compareVersions(_params.VERSION || ol.DEFAULT_WMS_VERSION, "1.3") >= 0,
			size = seg.map.getSize(),
			_extent = ol.proj.transformExtent(seg.map.getViewportExtent(), seg.map.getProjection(), projection),
			center = ol.extent.getCenter(_extent),
			halfWidth = ol.extent.getWidth(_extent) * DEFAULT_RATIO / 2,
			halfHeight = ol.extent.getHeight(_extent) * DEFAULT_RATIO / 2,
			axisOrientation = projection.getAxisOrientation();

		let extent = [
			center[0] - halfWidth,
			center[1] - halfHeight,
			center[0] + halfWidth,
			center[1] + halfHeight
		];

		if (v13 && axisOrientation.substr(0, 2) === "ne")
			extent = [extent[1], extent[0], extent[3], extent[2]];

		const params = Object.assign({
			SERVICE: "WMS",
			REQUEST: "GetMap",
			FORMAT: "image/png",
			TRANSPARENT: true,
			WIDTH: Math.ceil(DEFAULT_RATIO * size[0]),
			HEIGHT: Math.ceil(DEFAULT_RATIO * size[1]),
			BBOX: extent.join(","),
			TIME: timestamp
		}, _params);

		params[v13?"CRS":"SRS"] = projection.getCode();

		return seg.utils.addParamsToURL(url, params);
	};

	const refreshFrames = () => {
		cacheFrames(Object.keys(frameCache));
		source.changed();
	};

	seg.timeline.set({
		speed: 1200,
		delimiters: [
			moment(),
			moment().add(10, "d")
		],
		onPlay() {
			seg.map.lockInteractions(true);

			source.setImageLoadFunction(async (image) => {
				if(currentTimestamp && frameCache[currentTimestamp.toISOString()])
					image.getImage().src = await frameCache[currentTimestamp.toISOString()];
			});
		},
		onStop() {
			frameCache = {};
			seg.map.lockInteractions(false);
			source.setImageLoadFunction(imageLoadFunction);
		},
		groups: [{
			marker: {
				template: `
				<div class="default-marker">
					<img src="extensibility/modules/met_ocean/forecast/timeline-icon.png">
				</div>`
			},
			items(range) {
				return getMomentsInPeriods(periods, range).map(timestamp => {
					return { timestamp };
				});
			},
			onTimeChange(timestamp, before, after) {
				if(before.length) {
					const previousTimestamp = currentTimestamp;
					currentTimestamp = before[before.length - 1].timestamp;
					if(currentTimestamp.isSame(previousTimestamp))
						return;

					source.updateParams({
						TIME: currentTimestamp.toISOString()
					});

					if(seg.timeline.isPlaying()) {
						if(requestTimeout)
							seg.timeout.cancel(requestTimeout);

						requestTimeout = seg.timeout(() => {
							cacheDates([
								currentTimestamp,
								...after.slice(0, 2).map(futureDate => futureDate.timestamp)
							]);

							requestTimeout = null;
						}, 50);
					}
				}
			}
		}]
	});

	seg.ui.cleanables.add("HF Radar", () => {
		seg.timeline.clear();
		resizeListener();
	});
};

const createHFRadarMenuOptions = (menuItem, layer) => {
	if(layer.getLayers)
		Object.assign(menuItem, {
			options: layer.getLayers().map(subLayer => {
				let subLayerMenuItem = seg.ui.layerMenu.fromLayer(subLayer.get("name"), subLayer);

				if (subLayer.get("id") !== "radar_features") {
					subLayerMenuItem = Object.assign(createHFRadarMenuOptions(subLayerMenuItem, subLayer), {
						template: `
							<label class="hfradar-menu-item">
								<span class="flex-1">{{layer.get("name")}}</span>
								<span id="timestamp" ng-if-start="layer.getSource" timestamp="layer.getSource().getParams().TIME"></span>
								<button ng-if-end class="seg round primary" ng-click="openTimeline()">
									<i icon="timeline" class="icon-size-sm"></i>
								</button>
							</label>
						`
					});

					if(subLayer.getSource)
						subLayerMenuItem.bindings.openTimeline = () => openTimeline(subLayer);
				}

				return subLayerMenuItem;
			})
		});

	return menuItem;
};


module.exports = {
	createHFRadarMenuItem,
	createHFRadarMenuOptions,
	createHFRadarWMSConfig
};