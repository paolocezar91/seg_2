const configLayers = [
	{
		id: "BASQUElast60days",
		name: "Basque",
		endpoint: "v1/extension/wms/BASQUE",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/BASQUElast60days/Basque_Last_60_Days_Basque_HFRadar
		layer: "surface_sea_water_velocity",
		title: "Basque (last 60 days)",
		calendar: true
	},
	{
		id: "CALYPSOlast60days",
		name: "Calypso",
		endpoint: "v1/extension/wms/CALYPSO",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/CALYPSOlast60days/Calypso_Last_60_Days_Calypso_HFRadar
		layer: "surface_sea_water_velocity",
		title: "Calypso (last 60 days)",
		calendar: true
	},
	{
		id: "COSYNAlast60days",
		name: "Cosyna",
		endpoint: "v1/extension/wms/COSYNA",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/COSYNAlast60days/Cosyna_Last_60_Days_Cosyna_HFRadar
		layer: "surface_sea_water_velocity",
		title: "Cosyna (last 60 days)",
		calendar: true
	},
	{
		id: "GALICIAlast60days",
		name: "Galicia",
		endpoint: "v1/extension/wms/GALICIA",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/GALICIAlast60days/Galicia_Last_60_Days_Galicia_HFRadar
		layer: "sea_water_velocity",
		title: "Galicia (last 60 days)",
		calendar: true
	},
	// {
	// 	id: "IZORlast60days",
	// 	name: "IZOR (last 60 days)",
	// 	endpoint: "v1/extension/wms/IZOR",
	// 	// thredds.emodnet-physics.eu/thredds/wms/fmrc/IZORlast60days/IZOR_Last_60_Days_IZOR_HFRadar
	// 	layer: "sea_water_velocity",
	// 	title: "IZOR (last 60 days)",
	// 	calendar: true
	// },
	{
		id: "RITMARElast60days",
		name: "Ritmare",
		endpoint: "v1/extension/wms/RITMARE",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/RITMARElast60days/Ritmare_Last_60_Days_Ritmare_HFRadar
		layer: "surface_sea_water_velocity",
		title: "Ritmare (last 60 days)",
		calendar: true
	},
	{
		id: "SHOMlast60days",
		name: "SHOM",
		endpoint: "v1/extension/wms/SHOM",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/SHOMlast60days/Shom_Last_60_Days_Shom_HFRadar
		layer: "sea_water_velocity",
		title: "SHOM (last 60 days)",
		calendar: true
	},
	{
		id: "SOCIBlast60days",
		name: "Socib",
		endpoint: "v1/extension/wms/SOCIB",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/SOCIBlast60days/Socib_Last_60_Days_Socib_HFRadar
		layer: "sea_water_velocity",
		title: "Socib (last 60 days)",
		calendar: true
	},
	{
		id: "PUERTOS_DELTAEBROlast60days",
		name: "Puerto Deltaebro",
		endpoint: "v1/extension/wms/PUERTOS_DELTAEBRO",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/PUERTOS_DELTAEBROlast60days/DELTAEBRO_Last_60_Days_Deltaebro_HFRadar
		layer: "sea_water_velocity",
		title: "Puerto Deltaebro (last 60 days)",
		calendar: true
	},
	{
		id: "PUERTOS_GIBRALTARlast60days",
		name: "Puerto Gibraltar",
		endpoint: "v1/extension/wms/PUERTOS_GIBRALTAR",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/PUERTOS_GIBRALTARlast60days/GIBRALTAR_Last_60_Days_Gibaltar_HFRadar
		layer: "sea_water_velocity",
		title: "Puerto Gibraltar (last 60 days)",
		calendar: true
	},
	{
		id: "PUERTOS_HUELVAlast60days",
		name: "Puerto Huelva",
		endpoint: "v1/extension/wms/PUERTOS_HUELVA",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/PUERTOS_HUELVAlast60days/HUELVA_Last_60_Days_Huelva_HFRadar
		layer: "sea_water_velocity",
		title: "Puerto Huelva (last 60 days)",
		calendar: true
	},
	{
		id: "US_WEST_COASTlast60days",
		name: "US West Coast",
		endpoint: "v1/extension/wms/US_WEST_COAST",
		// hfrnet-tds.ucsd.edu/thredds/wms/HFR/USWC/6km/hourly/RTV/HFRADAR_US_West_Coast_6km_Resolution_Hourly_RTV_best.ncd
		layer: "surface_sea_water_velocity",
		title: "US West Coast (last 60 days)",
		calendar: true
	},
	{
		id: "US_EST_COASTlast60days",
		name: "US East Coast",
		endpoint: "v1/extension/wms/US_EST_COAST",
		// hfrnet-tds.ucsd.edu/thredds/wms/HFR/USEGC/6km/hourly/RTV/HFRADAR_US_East_and_Gulf_Coast_6km_Resolution_Hourly_RTV_best.ncd
		layer: "surface_sea_water_velocity",
		title: "US East Coast (last 60 days)",
		calendar: true
	},
	{
		id: "US_HAWAII_STATElast60days",
		name: "US Hawaii State",
		endpoint: "v1/extension/wms/US_HAWAII_STATE",
		// hfrnet-tds.ucsd.edu/thredds/wms/HFR/USHI/6km/hourly/RTV/HFRADAR_US_Hawaii_6km_Resolution_Hourly_RTV_best.ncd
		layer: "surface_sea_water_velocity",
		title: "US Hawaii State (last 60 days)",
		calendar: true
	},
	{
		id: "PUERTO_RICO_AND_THE_US_VIRGIN_ISLANDSlast60days",
		name: "Puerto Rico and US Virgin Islands",
		endpoint: "v1/extension/wms/PUERTO_RICO_AND_THE_US_VIRGIN_ISLANDS",
		// hfrnet-tds.ucsd.edu/thredds/wms/HFR/PRVI/6km/hourly/RTV/HFRADAR_Puerto_Rico_and_the_US_Virgin_Islands_6km_Resolution_Hourly_RTV_best.ncd
		layer: "surface_sea_water_velocity",
		title: "Puerto Rico and US Virgin Islands (last 60 days)",
		calendar: true
	},
	{
		id: "COFlast60days",
		name: "COF",
		endpoint: "v1/extension/wms/COF",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/COFlast60days/Coffs_Harbour_site_in_northern_New_South_Wales_(COF)_Last_60_Days_COF_HFRadar
		layer: "sea_water_velocity",
		title: "COF (last 60 days)",
		calendar: true
	},
	{
		id: "ROTlast60days",
		name: "ROT",
		endpoint: "v1/extension/wms/ROT",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/ROTlast60days/Rottnest_Shelf_site_in_Western_Australia_(ROT)_Last_60_Days_ROT_HFRadar
		layer: "sea_water_velocity",
		title: "ROT (last 60 days)",
		calendar: true
	},
	{
		id: "SAGlast60days",
		name: "SAG",
		endpoint: "v1/extension/wms/SAG",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/SAGlast60days/South_Australia_Gulfs_site_in_central_South_Australia_(SAG)_Last_60_Days_SAG_HFRadar
		layer: "sea_water_velocity",
		title: "SAG (last 60 days)",
		calendar: true
	},
	{
		id: "TURQlast60days",
		name: "TURQ",
		endpoint: "v1/extension/wms/TURQ",
		// thredds.emodnet-physics.eu/thredds/wms/fmrc/TURQlast60days/Turquoise_Coast_site_in_Western_Australia_(TURQ)_Last_60_Days_TURQ_HFRadar
		layer: "sea_water_velocity",
		title: "TURQ (last 60 days)",
		calendar: true
	}
];

module.exports = {
	configLayers
};