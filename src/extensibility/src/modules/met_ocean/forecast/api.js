
//XXX seg.CONFIG is undefined at this state - should de removed from here
const {
	findSelectedLayer,
	getLegend,
	WMSTimeToPeriods,
	getMomentsInPeriods
} = require("../api.js");


const openTimeline = layer => {
	const source = layer.getSource(),
		periods = WMSTimeToPeriods(layer.get("availablePeriods")),
		imageLoadFunction = source.getImageLoadFunction(),
		resizeListener = seg.map.onViewportSizeChange(refreshFrames);

	let currentTimestamp,
		requestTimeout,
		frameCache = {};

	const cacheFrames = timestamps => timestamps.forEach(timestamp => frameCache[timestamp] = seg.request.getDataURL(buildRequestUrl(timestamp)));

	const cacheDates = datesToCache => {
		Object.keys(frameCache).forEach(timestamp => {
			if(datesToCache.some(dateToCache => dateToCache.isSame(timestamp)))
				return;

			if(frameCache[timestamp]) {
				frameCache[timestamp].cancel();

				delete frameCache[timestamp];
			}
		});

		cacheFrames(datesToCache.filter(timestamp => !frameCache[timestamp.toISOString()]).map(timestamp => timestamp.toISOString()));
	};

	//adapted from openlayers ImageWMS source code
	const buildRequestUrl = timestamp => {
		const DEFAULT_RATIO = seg.CONFIG.getValue("metocean.forecast.DEFAULT_RATIO");

		const source = layer.getSource(),
			url = source.getUrl(),
			_params = source.getParams(),
			projection = source.getProjection(),
			v13 = $o.compareVersions(_params.VERSION || ol.DEFAULT_WMS_VERSION, "1.3") >= 0,
			size = seg.map.getSize(),
			_extent = ol.proj.transformExtent(seg.map.getViewportExtent(), seg.map.getProjection(), projection),
			center = ol.extent.getCenter(_extent),
			halfWidth = ol.extent.getWidth(_extent) * DEFAULT_RATIO / 2,
			halfHeight = ol.extent.getHeight(_extent) * DEFAULT_RATIO / 2,
			axisOrientation = projection.getAxisOrientation();

		let extent = [
			center[0] - halfWidth,
			center[1] - halfHeight,
			center[0] + halfWidth,
			center[1] + halfHeight
		];

		if (v13 && axisOrientation.substr(0, 2) === "ne")
			extent = [extent[1], extent[0], extent[3], extent[2]];

		const params = Object.assign({
			SERVICE: "WMS",
			REQUEST: "GetMap",
			FORMAT: "image/png",
			TRANSPARENT: true,
			WIDTH: Math.ceil(DEFAULT_RATIO * size[0]),
			HEIGHT: Math.ceil(DEFAULT_RATIO * size[1]),
			BBOX: extent.join(","),
			TIME: timestamp
		}, _params);

		params[v13?"CRS":"SRS"] = projection.getCode();

		return seg.utils.addParamsToURL(url, params);
	};

	const refreshFrames = () => {
		cacheFrames(Object.keys(frameCache));
		source.changed();
	};

	seg.timeline.set({
		speed: seg.CONFIG.getValue("metocean.forecast.TIMELINE_SPEED"),
		delimiters: [
			moment(),
			moment().add(10, "d")
		],
		onPlay() {
			seg.map.lockInteractions(true);

			source.setImageLoadFunction(async (image) => {
				if(currentTimestamp && frameCache[currentTimestamp.toISOString()])
					image.getImage().src = await frameCache[currentTimestamp.toISOString()];
			});
		},
		onStop() {
			frameCache = {};
			seg.map.lockInteractions(false);
			source.setImageLoadFunction(imageLoadFunction);
		},
		groups: [{
			marker: {
				template: `
				<div class="default-marker">
					<img src="extensibility/modules/met_ocean/forecast/timeline-icon.png">
				</div>`
			},
			items(range) {
				return getMomentsInPeriods(periods, range).map(timestamp => {
					return { timestamp };
				});
			},
			onTimeChange(timestamp, before, after) {
				if(before.length) {
					const previousTimestamp = currentTimestamp;
					currentTimestamp = before[before.length - 1].timestamp;
					if(currentTimestamp.isSame(previousTimestamp))
						return;

					source.updateParams({
						TIME: currentTimestamp.toISOString()
					});

					if(seg.timeline.isPlaying()) {
						if(requestTimeout)
							seg.timeout.cancel(requestTimeout);

						requestTimeout = seg.timeout(() => {
							cacheDates([
								currentTimestamp,
								...after.slice(0, 2).map(futureDate => futureDate.timestamp)
							]);

							requestTimeout = null;
						}, 50);
					}
				}
			}
		}]
	});

	seg.ui.cleanables.add("Forecast", () => {
		seg.timeline.clear();
		resizeListener();
	});
};

const createForecastMenuOptions = (menuItem, layer) => {
	if(layer.getLayers)
		Object.assign(menuItem, {
			isPartial() {
				return (layer.getVisible() && !layer.getLayers().every(subLayer => subLayer.getVisible())) ? true : undefined;
			},
			options: layer.getLayers().map(subLayer => {
				let subLayerMenuItem = seg.ui.layerMenu.fromLayer(subLayer.get("name"), subLayer);
				subLayerMenuItem = Object.assign(createForecastMenuOptions(subLayerMenuItem, subLayer), {
					template: `<label class="forecast-menu-item">
						<span class="flex-1">{{layer.get("name")}}</span>
						<span id="timestamp" ng-if-start="layer.getSource" timestamp="layer.getSource().getParams().TIME"></span>
						<button ng-if-end class="seg round primary" ng-click="openTimeline()">
							<i icon="timeline" class="icon-size-sm"></i>
						</button>
					</label>`,
					toggle(value) {
						if(typeof value === "undefined")
							value = !subLayer.ol_.getVisible();

						if(!value)
							return;

						layer.getLayers().forEach(childLayer => {
							const value = childLayer === subLayer;
							seg.preferences.map.set("layers." + childLayer.get("id") + ".visible", value);
							childLayer.ol_.setVisible(value);
						});
					}
				});

				if(subLayer.getSource)
					subLayerMenuItem.bindings.openTimeline = () => openTimeline(subLayer);

				return subLayerMenuItem;
			}),
			hideSelectAll: true
		});

	return menuItem;
};

const createForecastMenuItem = forecastLayer => {
	return Object.assign(seg.ui.layerMenu.fromLayer(forecastLayer.get("name"), forecastLayer), {
		isChecked() {
			return (forecastLayer.ol_.getVisible() || seg.preferences.map.get("layers." + forecastLayer.get("id") + ".visible")); /*&&
				this.options && this.options.length && this.options.filter(option => option.isChecked() || option.isPartial());*/
		},
		info: {
			title: {
				template: "{{getLayerName()}}",
				bindings: {
					getLayerName() {
						return findSelectedLayer(forecastLayer.getLayers()).get("name");
					}
				}
			},
			bindings: {
				isValidLayer() {
					return !!findSelectedLayer(forecastLayer.getLayers()).getSource;
				},
				getSelectedLayer() {
					return findSelectedLayer(forecastLayer.getLayers());
				},
				getLegend(layer) {
					return getLegend(layer);
				},
				style(val) {
					const selectedSource = findSelectedLayer(forecastLayer.getLayers()).getSource();

					if(typeof val === "undefined")
						return selectedSource.getParams().STYLES;

					selectedSource.updateParams({
						STYLES: val
					});
				}
			},
			disabled() {
				return !findSelectedLayer(forecastLayer.getLayers());
			},
			template: `<div ng-if="isValidLayer()" id="forecast-legend">
				<div class="flexbox">
					<div>
						<combo id="style-picker" ng-if="getSelectedLayer().get('styles').length > 1" ng-model="style" ng-model-options="{getterSetter: true}" options="style for style in getSelectedLayer().get('styles')"></combo>
						<img ng-src="{{getLegend(getSelectedLayer())}}">
					</div>
					<div id="info" ng-if="getSelectedLayer().get('info')">
						<p>{{getSelectedLayer().get('info')}}<p>
						<p ng-if="getSelectedLayer().get('link')">
							<a ng-href="{{getSelectedLayer().get('link')}}" target="_blank">More info</a>
						</p>
					</div>
				</div>
			</div>
			<div ng-if="!isValidLayer()">Please select a Forecast Layer</div>
			<style>
				#forecast-legend #style-picker {
					margin-bottom: 10px;
				}
				#forecast-legend #info {
					margin-left: 10px;
					max-width: 200px;
				}
			</style>`
		}
	});
};

module.exports = { createForecastMenuItem, createForecastMenuOptions };
