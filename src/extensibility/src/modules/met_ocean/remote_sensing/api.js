const {
	findSelectedLayer,
	getLegend,
	getMinDateInPeriods,
	getMaxDateInPeriods,
	getMomentsInPeriods,
	WMSTimeToPeriods
} = require("../api.js");

const createRemoteSensingMenuOptions = (menuItem, layer) => {
	if(layer.getLayers)
		Object.assign(menuItem, {
			// isPartial() {
			// 	return (layer.getVisible() && !layer.getLayers().every(subLayer => subLayer.getVisible())) ? true : undefined;
			// },
			options: layer.getLayers().map(subLayer => {
				let subLayerMenuItem = seg.ui.layerMenu.fromLayer(subLayer.get("name"), subLayer);
				subLayerMenuItem = Object.assign(createRemoteSensingMenuOptions(subLayerMenuItem, subLayer), {
					template: `<label class="remote-sensing-menu-item">
						<span class="flex-1">{{layer.get("name")}}</span>
						<span id="timestamp" ng-if="!layer.get('calendar') && time()" timestamp="time()"></span>
						<date ng-if="layer.get('calendar')" ng-model="time" min="minDate" max="maxDate" ng-model-options="{getterSetter: true}" dates-in-range="getMomentsInRange(range)"></date>
					</label>`
				});

				if(subLayer.getSource) {
					subLayerMenuItem.bindings.time = val => {
						const source = subLayer.getSource();

						if(typeof val === "undefined") {
							if(!source.get("time"))
								source.set("time", moment(source.getParams().TIME));

							return source.get("time");
						}

						source.set("time", val);
						source.updateParams({
							TIME: val.toISOString()
						});
					};

					if(subLayer.get("calendar")) {
						const availablePeriods = WMSTimeToPeriods(subLayer.get("availablePeriods")),
							minDate = getMinDateInPeriods(availablePeriods),
							maxDate = getMaxDateInPeriods(availablePeriods);

						Object.assign(subLayerMenuItem.bindings, {
							minDate,
							maxDate,
							getMomentsInRange(range) {
								return getMomentsInPeriods(availablePeriods, range);
							}
						});
					}
				}

				return subLayerMenuItem;
			})
		});

	return menuItem;
};

const createRemoteSensingMenuItem = remoteSensingLayer => {
	return Object.assign(seg.ui.layerMenu.fromLayer(remoteSensingLayer.get("name"), remoteSensingLayer), {
		info: {
			title: {
				template: "{{getLayerName()}}",
				bindings: {
					getLayerName() {
						return findSelectedLayer(remoteSensingLayer.getLayers()).get("name");
					}
				}
			},
			bindings: {
				isValidLayer() {
					return !!findSelectedLayer(remoteSensingLayer.getLayers()).getSource;
				},
				getSelectedLayer() {
					return findSelectedLayer(remoteSensingLayer.getLayers());
				},
				getLegend(layer) {
					return getLegend(layer);
				},
				style(val) {
					const selectedSource = findSelectedLayer(remoteSensingLayer.getLayers()).getSource();

					if(typeof val === "undefined")
						return selectedSource.getParams().STYLES;

					selectedSource.updateParams({
						STYLES: val
					});
				}
			},
			disabled() {
				return !findSelectedLayer(remoteSensingLayer.getLayers());
			},
			template: `
			<div ng-if="isValidLayer()" id="remote-sensing-legend">
				<div class="flexbox">
					<div>
						<combo id="style-picker" ng-if="getSelectedLayer().get('styles').length > 1" ng-model="style" ng-model-options="{getterSetter: true}" options="style for style in getSelectedLayer().get('styles')"></combo>
						<img ng-src="{{getLegend(getSelectedLayer())}}">
					</div>
					<div id="info" ng-if="getSelectedLayer().get('info')">
						<p>{{getSelectedLayer().get("info")}}<p>
						<p ng-if="getSelectedLayer().get('link')">
							<a ng-href="{{getSelectedLayer().get('link')}}" target="_blank">More info</a>
						</p>
					</div>
				</div>
			</div>
			<div ng-if="!isValidLayer()">Please select a Remote Sensing Layer</div>
			<style>
				#remote-sensing-legend #style-picker {
					margin-bottom: 10px;
				}
				#remote-sensing-legend #info {
					margin-left: 10px;
					max-width: 200px;
				}
			</style>`
		}
	});
};

module.exports = { createRemoteSensingMenuItem, createRemoteSensingMenuOptions };
