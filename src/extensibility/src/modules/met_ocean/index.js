/**
@namespace extensibility.modules.metocean
@description
# MetOcean

## Available options
The following options are all booleans. Omitting an option is the same as having a **false** value:
- **remote_sensing** - Remote Sensing layers
- **forecast** - Forecast Model layers
- **in_situ** - In Situ stations layer

## Global configurations and constants
None

## Adding new MetOcean layers
Remote Sensing and Forecast layers are added in the SEG database.
*/
let config,
	metOceanLayer,
	remoteSensingLayer, remoteSensingMenu, inSituLayer, inSituMenu, buoyMenu, hfRadarMenu,
	forecastLayer, forecastMenu, buoyLayer, hfRadarLayer;

const { extractEndpoints, createConfigFromLayers } = require("./api.js"),
	{ createForecastMenuItem, createForecastMenuOptions } = require("./forecast/api.js"),
	{ createRemoteSensingMenuItem, createRemoteSensingMenuOptions } = require("./remote_sensing/api.js"),
	{ createDriftBuoyMenuItem, createDriftBuoyMenuOptions, createDriftBuoyWMSConfig, buoyGetFeatureInfoEvent } = require("./in_situ/drift_buoy/api.js"),
	{ createHFRadarMenuItem, createHFRadarMenuOptions, createHFRadarWMSConfig } = require("./in_situ/hf_radar/api.js"),
	{ createInSituMenuItem } = require("./in_situ/api.js"),
	groups = [], layers = [];


const lazyLoaderInSitu = async () => {
	if (!metOceanLayer.getVisible())
		return;

	if(config.in_situ && (!inSituLayer.get("loading") && inSituLayer.getVisible())){
		const modal = seg.modal.openModal("Loading metOcean inSitu", "Loading metOcean In Situ capabilities...", {class: "info"});

		buoyMenu.isLoading = true;
		hfRadarMenu.isLoading = true;

		inSituLayer.set("loading", true);

		const driftBuoyConfig = await createDriftBuoyWMSConfig();
		buoyLayer.addLayers(driftBuoyConfig.map(seg.layer.create));

		const hfRadarConfig = await createHFRadarWMSConfig();
		hfRadarLayer.addLayers(hfRadarConfig.map(seg.layer.create));

		delete buoyMenu.isLoading;
		createDriftBuoyMenuOptions(buoyMenu, buoyLayer);
		buoyGetFeatureInfoEvent();

		delete hfRadarMenu.isLoading;
		createHFRadarMenuOptions(hfRadarMenu, hfRadarLayer);
		// hfRadarMenu.options = [hfRadarMenu.options[0], ...hfRadarMenu.options[1].options];

		seg.modal.closeTimeout(modal.title);
	}
};

const lazyLoader = async () => {
	// metOceanLayer.addLayers(require("./wind"));

	if (!metOceanLayer.getVisible())
		return;

	if (!metOceanLayer.get("loading")) {
		metOceanLayer.set("loading", true);

		if(config.in_situ && (!inSituLayer.get("loading") && inSituLayer.getVisible()))
			lazyLoaderInSitu();

		const modal = seg.modal.openModal("Loading metOcean", "Loading metOcean capabilities...", {class: "info"});

		remoteSensingMenu.isLoading = true;
		forecastMenu.isLoading = true;

		// get all groups, sends request to get all layers parameters for remote_sensing and model_forecast
		const moduleRequests = groups.map(group => seg.request.get("v1/extension/wms/group", {
			params: { group }
		}).then(({result}) => {
			//get capabilities for this group
			const capabilities = {},
				requests = extractEndpoints(result).map(endpoint => {
					if(endpoint === "Dioxide") //XXX harcoded
						endpoint += "?token=public";

					return seg.layer.getCapabilities("v1/extension/wms/" + endpoint)
						.then(capability => capabilities[endpoint] = capability,
							e => seg.log.error("ERROR_MET_OCEAN_LAZY_LOADER_GET_CAPABILITIES", "Failed to get capabilities for endpoint " + endpoint, {
								error: e,
								endpoint
							})
						);
				});

			return seg.promise.all(requests).then(() => {
				const configFromLayers = createConfigFromLayers(group, result, capabilities);
				seg.layer.get(group).addLayers(configFromLayers.map(seg.layer.create));
			}, e => seg.log.error("ERROR_MET_OCEAN_LAZY_LOADER_CREATE_CONFIG", e.error + ": " + e.result));
		}, e => seg.log.error("ERROR_MET_OCEAN_LAZY_LOADER_GROUPS_MAP", e.error + ": " + e.result)));

		return seg.promise.all(moduleRequests).then(() => {
			//remote sensing
			if(config.remote_sensing) {
				delete remoteSensingMenu.isLoading;
				createRemoteSensingMenuOptions(remoteSensingMenu, remoteSensingLayer);
			}

			//forecast
			if(config.forecast) {
				delete forecastMenu.isLoading;
				createForecastMenuOptions(forecastMenu, forecastLayer);
			}

			seg.modal.closeTimeout(modal.title);
		}, e => seg.log.error("ERROR_MET_OCEAN_LAZY_LOADER", e.error + ": " + e.result));
	}
};

module.exports = _config => {
	config = _config;

	if(config.remote_sensing) {
		groups.push("remote_sensing");
		layers.push({
			id: "remote_sensing",
			name: "Remote Sensing",
			layers: []
		});
	}

	if(config.forecast) {
		groups.push("model_forecast");
		layers.push({
			id: "model_forecast",
			name: "Forecast Model",
			layers: []
		});
	}

	if(config.in_situ)
		layers.push({
			id: "in_situ",
			name: "In Situ",
			layers: require("./in_situ/index").init()
		});

	return {
		name: "MetOcean",
		layers: [{
			id: "metocean",
			order: 6,
			name: "MetOcean",
			layers,
			visible: false
		}],
		init() {
			metOceanLayer = seg.layer.get("metocean");

			// registering metOceanMenu
			const metoceanMenuItem = seg.ui.layerMenu.fromLayer("METOCEAN", metOceanLayer);
			metoceanMenuItem.toggle(false);
			seg.ui.layerMenu.register(metoceanMenuItem);

			// adding menu options if they are required on config
			// remote sensing
			if(config.remote_sensing) {
				remoteSensingLayer = seg.layer.get("remote_sensing");
				remoteSensingLayer.transform(seg.map.getProjection());
				remoteSensingMenu = createRemoteSensingMenuItem(remoteSensingLayer);
				metoceanMenuItem.options.push(remoteSensingMenu);
			}
			// forecast
			if(config.forecast) {
				forecastLayer = seg.layer.get("model_forecast");
				forecastLayer.transform(seg.map.getProjection());
				forecastMenu = createForecastMenuItem(forecastLayer);
				metoceanMenuItem.options.push(forecastMenu);
			}

			// in situ
			if(config.in_situ){
				buoyLayer = seg.layer.get("drift_buoy");
				buoyLayer.transform(seg.map.getProjection());
				buoyMenu = createDriftBuoyMenuItem(buoyLayer);

				hfRadarLayer = seg.layer.get("hf_radar");
				hfRadarLayer.transform(seg.map.getProjection());
				hfRadarMenu = createHFRadarMenuItem(hfRadarLayer);

				inSituMenu = createInSituMenuItem(seg.layer.get("in_situ"));
				inSituMenu.options = inSituMenu.options.concat(...[hfRadarMenu, buoyMenu]).sort((a, b) => a.alias > b.alias ? 1 : a.alias < b.alias ? -1 : 0);
				metoceanMenuItem.options.push(inSituMenu);

				inSituLayer = inSituMenu.bindings.layer;
				inSituLayer.ol_.on("change:visible", lazyLoaderInSitu);
				lazyLoaderInSitu();
			}

			metOceanLayer.ol_.on("change:visible", lazyLoader);
			lazyLoader();
		}
	};
};
