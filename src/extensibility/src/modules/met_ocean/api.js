const findSelectedLayer = layers => {
	for (let i = 0; i < layers.length; i++)
		if (layers[i].getVisible() && layers[i].get("type") !== "Vector") {
			if (layers[i].getLayers) {
				const foundInGroup = findSelectedLayer(layers[i].getLayers());

				if (foundInGroup)
					return foundInGroup;
			}

			return layers[i];
		}
};

const getLegend = (layer, options = {}) => {
	const source = layer.getSource(),
		{LAYERS, STYLES, COLORSCALERANGE} = source.getParams(),
		legendCache = layer.get("legendCache"),
		requesting = layer.get("legendRequests");

	if (!legendCache[STYLES] && !requesting[STYLES]) {
		requesting[STYLES] = true;

		const params = {
			REQUEST: "GetLegendGraphic",
			LAYER: LAYERS,
			FORMAT: "image/jpeg"
		};

		if(COLORSCALERANGE)
			params.COLORSCALERANGE = COLORSCALERANGE;

		if (STYLES.indexOf("/") !== -1)
			params.PALETTE = STYLES.split("/")[1];
		else
			params.STYLE = STYLES;

		if(options.width && options.height){
			params.width = options.width;
			params.height = options.height;
		}

		const url = (source.getUrl && source.getUrl()) || (source.getUrls && source.getUrls())[0];

		seg.request.getDataURL(seg.utils.addParamsToURL(url, params)).then(res => {
			legendCache[STYLES] = res;
			delete requesting[STYLES];
		}, e => seg.log.error("ERROR_MET_OCEAN_GET_DATA_URL", e.error + ": " + e.result));
	}

	return legendCache[STYLES];
};

const WMSTimeToPeriods = WMSTimeString => {
	const wmsTimeArray = WMSTimeString.split(","),
		result = [],
		limit = moment().subtract(6, "M");
	let values,
		i = wmsTimeArray.length - 1;

	do {
		values = wmsTimeArray[i].split("/");
		if (values.length === 1) {
			const date = moment(values[0], moment.ISO_8601);
			result.push(date);
			if (date < limit)
				i=-1;
		} else
			result.push({
				start: moment(values[0], moment.ISO_8601),
				end: moment(values[1], moment.ISO_8601),
				interval: moment.duration(values[2])
			});
		i--;
	} while( i>0);


	return result.reverse();
};

const getMaxDateInPeriods = periods => periods.reduce((max, period) => {
	if (moment.isMoment(period)) {
		if (!max || period > max)
			max = period;
	} else if (!max || period.end > max)
		max = period.end;

	return max;
}, null);

const getMinDateInPeriods = periods => periods.reduce((min, period) => {
	if (moment.isMoment(period)) {
		if (!min || period < min)
			min = period;
	} else if (!min || period.start < min)
		min = period.start;

	return min;
}, null);

const getMomentsInPeriods = (periods, range) => {
	const moments = [];

	periods.forEach(period => {
		if (moment.isMoment(period) && period.isBetween(range.start, range.end, null, "[]"))
			moments.push(period);
		//check if period overlaps with range
		else if (period.end >= range.start && period.start <= range.end) {
			//difference between period start and range start
			const delta = period.start - range.start,
				//offset in interval amounts
				offset = Math.ceil(delta / period.interval),
				startDate = moment(Math.max(period.start, period.start - offset * period.interval));

			while (startDate <= period.end) {
				moments.push(startDate.clone());
				startDate.add(period.interval);
			}
		}
	});

	return moments;
};

const isMomentInPeriods = (date, periods) => periods.some(period => {
	if (moment.isMoment(period))
		return date.isSame(period);

	if (date < period.start || date > period.end)
		return false;

	for (let current = period.start.clone(); current <= period.end; current.add(period.interval))
		if (date.isSame(current))
			return true;

	return false;
});

const enhanceConfigWithCapabilities = (group, layer, capabilities) => {
	if (!capabilities){
		seg.log.error("metocean", "No capabilities defined for " + layer.name, {
			layer,
			capabilities
		});
		return;
	}

	const layerCapabilities = seg.layer.findLayerInCapabilities(layer, capabilities);

	if (!layerCapabilities){
		seg.log.error("metocean", "could not find layer " + layer.name + " in capabilities", {
			layer,
			capabilities
		});
		return;
	}

	if (layerCapabilities.Style)
		layer.styles = layerCapabilities.Style.map(style => style.Name);
	layer.source.params.STYLES = layer.styleDefault || (layer.styles && layer.styles[0]);

	if (capabilities.version)
		layer.source.params.VERSION = capabilities.version;

	layer.source.projections = layer.projectionsDefault;
	if (capabilities.Capability.Layer.CRS)
		layer.source.projections = capabilities.Capability.Layer.CRS;

	if (layerCapabilities.Dimension) {
		const timeDimensionInfo = layerCapabilities.Dimension.find(dimension => dimension.name === "time");

		if (!timeDimensionInfo){
			seg.log.error("metocean", "no time info for " + layer.name, {
				layer,
				layerCapabilities
			});
			return;
		}

		layer.source.params.TIME = timeDimensionInfo.default;

		if (layer.calendar || group === "model_forecast" || group === "hf_radar_wms")
			layer.availablePeriods = timeDimensionInfo.values;
	}

	Object.assign(layer, {
		legendCache: {},
		legendRequests: {}
	});

	return layer;
};

const extractEndpoints = (layers) => {
	const endpoints = [];

	layers.forEach(layer => {
		if(layer.endpoint && !~endpoints.indexOf(layer.endpoint))
			endpoints.push(layer.endpoint);
		else if(layer.layers) {
			const subEndpoints = extractEndpoints(layer.layers);

			subEndpoints.forEach(endpoint => {
				if(!~endpoints.indexOf(endpoint))
					endpoints.push(endpoint);
			});
		}
	});

	return endpoints;
};

const createConfigFromLayers = (group, layers, capabilities) => {
	return layers.map(layer => {
		let _config = {
			id: group + "_" + layer.id,
			name: layer.name
		};

		//has to be true/false - undefined will default to true
		_config.visible = !!seg.preferences.map.get("layers." + _config.id + ".visible");

		if(layer.layers)
			_config.layers = createConfigFromLayers(group, layer.layers, capabilities);
		else {
			let endpoint = layer.endpoint;

			//XXX hardcoded
			if(endpoint === "Dioxide")
				endpoint += "?token=public";

			Object.assign(_config, {
				type: "Image",
				source: {
					type: "ImageWMS",
					url: "v1/extension/wms/" + endpoint,
					params: {
						LAYERS: layer.layer
					},
					customLoadFunction: layer.customLoadFunction && layer.customLoadFunction(layer)
				},
				info: layer.info,
				link: layer.more_info,
				calendar: !!(layer.Calendar || layer.calendar)
			});

			if(layer.additionalParameters)
				Object.assign(_config.source.params, layer.additionalParameters.split("&").reduce((params, str) => {
					const [key, val] = str.split("=");
					params[key] = val;
					return params;
				}, {}));

			_config = enhanceConfigWithCapabilities(group, _config, capabilities[endpoint]);
		}

		return _config;
	//remove undefined layers
	}).filter(layer => layer);
};

module.exports = {
	findSelectedLayer,
	getLegend,
	WMSTimeToPeriods,
	getMomentsInPeriods,
	getMaxDateInPeriods,
	getMinDateInPeriods,
	isMomentInPeriods,
	extractEndpoints,
	createConfigFromLayers,
	enhanceConfigWithCapabilities
};
