let wind;
const WindGL = require("./wind-gl");


function frame() {
	if (wind.windData) {
		wind.draw();
	}
	requestAnimationFrame(frame);
}

const windLayer = seg.layer.create({
	id: "wind",
	name: "Wind",
	type: "Image",
	source: {
		type: "ImageWMS",
		url: "extensibility/src/modules/met_ocean/wind/2016112000.png",
		customLoadFunction: async (img/*, url*/) => {
			img.getImage().src = seg.source.transparent1x1Image();

			let canvas = document.getElementById("windgl");
			if (canvas)
				canvas.remove();

			const originalCanvas = document.getElementsByTagName("canvas")[0];
			canvas = document.createElement("canvas");
			Object.assign(canvas, {
				width: window.innerWidth,
				height: window.innerHeight,
				id: "windgl",
				style: "position: fixed;"
			});
			originalCanvas.parentNode.prepend(canvas);

			wind = new WindGL(canvas.getContext("webgl", {antialiasing: false}));
			Object.assign(wind, {
				numParticles: 589824,
				fadeOpacity: 0.8,
				speedFactor: 1,
				dropRate: 0.1,
				dropRateBump: 0.2
			});

			const windData = await seg.request.get("extensibility/src/modules/met_ocean/wind/2016112000.json");

			const image = new Image();
			image.src = "extensibility/src/modules/met_ocean/wind/2016112000.png"; /*transparent ? seg.source.transparent1x1Image() : (await seg.request.getDataURL(seg.utils.addParamsToURL(url, params)));*/
			windData.image = image;
			image.onload = () => {
				wind.setWind(windData);
				wind.resize();
				frame();
			};
		}
	},
	visible: true
});

module.exports = windLayer;