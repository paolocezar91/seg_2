const featureTypes = [],
	maxFeaturesError = false,
	distinctDataByKey = () => {
		// Checks uploaded data to see distinct data by a given key (for vessels is mmsi, i.e.)
		if (importBindings.uploadedData)
			return importBindings.uploadedData
				.filter((obj, index, arr) => {
					const mapObj = arr.map(mapObj => String(mapObj[importBindings.selectedFeatureType.maxFeaturesKey]));
					return mapObj.indexOf(String(obj[importBindings.selectedFeatureType.maxFeaturesKey])) === index;
				});
		return [];

	},
	disableUploadButton = () => {
		return importBindings.selectedSource === "File" &&
			!importBindings.selectedFile;
	},
	disableImportButton = () => {
		importBindings.maxFeaturesError = importBindings.selectedFeatureType.additionalOptions.bindings.withExternal.enabled &&
			distinctDataByKey().length > importBindings.selectedFeatureType.maxFeatures;

		if (importBindings.maxFeaturesError)
			importBindings.errorMessage = "Importing is limited to a max of " + importBindings.selectedFeatureType.maxFeatures + " distinct " + importBindings.selectedFeatureType.featureAs;

		if (!importBindings.maxFeaturesError &&
			importBindings.selectedFeatureType &&
			importBindings.selectedFeatureType.additionalOptions &&
			importBindings.selectedFeatureType.additionalOptions.bindings &&
			importBindings.selectedFeatureType.additionalOptions.bindings.canSubmit
		) {
			const canSubmit = importBindings.selectedFeatureType.additionalOptions.bindings.canSubmit();
			if (canSubmit === "ok") {
				return false;
			} else {
				importBindings.errorMessage = canSubmit;
			}
		}

		return true;
	},
	close = () => {
		seg.ui.apps.close("Import");
		delete importBindings.selectedFile;
	},
	selectFile = () => {
		const input = document.createElement("input");
		input.setAttribute("style", "display: hidden");
		input.type = "file";
		document.body.appendChild(input);
		input.click();
		input.addEventListener("change", e => {
			seg.timeout(async () => {
				return importBindings.selectedFile = e.target.files[0];
			});
		});
	},
	updateSelectedSource = () => importBindings.selectedSource = importBindings.selectedFeatureType.sourceOptions[0],
	uploadFile = async () => {
		if(!importBindings.selectedFeatureType)
			return;

		let data, modal;

		switch(importBindings.selectedSource){
			case "File":
				modal = seg.modal.openModal("Importing data", "Importing data from file...", {class: "info"});

				if (importBindings.selectedFeatureType.importFromService) { // importing from service
					try{
						data = (await importBindings.selectedFeatureType.importFromService(importBindings.selectedFile));
						modal.setContent(importBindings.selectedFeatureType.label + "(s) imported successfully.");
						seg.modal.closeTimeout(modal.title);
					} catch (e) {
						modal.setContent("Error importing from service for " + importBindings.selectedFeatureType.label + "(s)");
						seg.modal.closeTimeout(modal.title);
					}
				}

				if (!importBindings.selectedFeatureType.importFromService) {  // import from csv file
					try {
						data = (await seg.import(importBindings.selectedFile, {point: true})).features;
						modal.setContent(importBindings.selectedFeatureType.label + "(s) imported successfully.");
						seg.modal.closeTimeout(modal.title);
					} catch(e) {
						modal.setContent("Error importing for " + importBindings.selectedFeatureType.label + ": " + e.message);
						seg.modal.closeTimeout(modal.title);
					}
				}

				break;
			case "Manual entry":
				data = [importBindings.selectedFeatureType.defaultValue];
				break;
		}

		if (!importBindings.selectedFeatureType.importFromService){
			if (data) {
				importBindings.uploadedData = importBindings.selectedFeatureType.toModel(data);
				importBindings.importedFields = Object.assign(importBindings.uploadedData.reduce((fields, item) => Object.assign(
					fields,
					Object.keys(item).filter(key => !~Object.keys(fields).indexOf(key)).reduce((_fields, key) => Object.assign(
						_fields, { [key]: {label: key, property: key, visible: key !== "seg_type" } }
					), {})
				), {}), importBindings.selectedFeatureType.additionalFields || {});
			}
		}

	},
	//call selected feature type"s importFn with additionalOptions {key: option.scope}
	importFn = () => {
		const bindings = importBindings.selectedFeatureType.additionalOptions?importBindings.selectedFeatureType.additionalOptions.bindings:{};
		return importBindings.selectedFeatureType
			.import(importBindings.uploadedData, bindings)
			.then(imported => imported);
	},
	addRow = () => {
		importBindings.uploadedData.push(...importBindings.selectedFeatureType.toModel([importBindings.selectedFeatureType.defaultValue]));
	},
	removeRow = (row) => {
		importBindings.uploadedData = importBindings.uploadedData.filter((item) => row.item !== item);
	},
	checkFeatureType = featureType => {
		const {label, featureAs, toModel, defaultValue} = featureType;

		if(typeof label !== "string")
			throw seg.log.error("seg.import", "Feature type must have a string label property", featureType);

		if(~featureTypes.findIndex(type => type.label === label))
			throw seg.log.error("seg.import", `Duplicate import type ${label}`, featureType);

		if(typeof featureAs !== "string")
			throw seg.log.error("seg.import", "Feature type must have a string featureAs property", featureType);

		if(typeof toModel !== "function")
			throw seg.log.error("seg.import", "Feature type must have a function toModel property", featureType);

		if(typeof defaultValue !== "object")
			throw seg.log.error("seg.import", "Feature type must have a defaultValue", featureType);

		featureTypes.push(featureType);

		return true;
	},
	importBindings = {
		close,
		selectFile,
		uploadFile,
		importFn,
		addRow,
		updateSelectedSource,
		disableUploadButton,
		disableImportButton,
		maxFeaturesError,
		removeRow
	};

module.exports = (...featureTypeOptions) => featureTypeOptions.every(checkFeatureType) && ({
	name: "Import",
	label: "Import",
	icon: "upload",
	bindings: {
		import: importBindings
	},
	onOpen() {
		//reset bindings
		Object.assign(importBindings, {
			selectedSource: featureTypes[0].sourceOptions[0],
			selectedFeatureType: featureTypes[0],
			featureTypeOptions: featureTypes,
			uploadedData: null,
			importedFields: null,
			selectedFile: null
		});
	},
	templateUrl: resolvePath("./template.html")
});