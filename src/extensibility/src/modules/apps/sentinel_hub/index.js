const SCALE = 500000, layersJson = require("./layers.json");

module.exports = (() => {
	//External Layer Group
	const eoExternalLayer = seg.layer.create({
		id: "eo_external",
		layers: [],
		name:"EO External"
	});

	seg.map.addLayer(eoExternalLayer);

	const layers = seg.map.ol_.getLayers();

	//get index of eo layer, so external_eo gets added after (on top of) it
	for(let i=0; i<layers.getLength(); i++)
		if(layers.item(i).get("id") === "eo_data_products") {
			layers.insertAt(i + 1, layers.remove(eoExternalLayer.ol_));
			break;
		}

	const scope = {
		selectedLayer: null,
		availableLayers: layersJson["Sentinel-2"], //null
		selectedSatellite: require("./satellites.json")[0], //null
		availableSatellites: require("./satellites.json"),
		selectedArea: [],
		close() {
			seg.ui.apps.close("Sentinel-Hub Data");
		},
		maxCloudCoverage: 0.2,
		timestamp: moment(),
		cloud: false,
		disableMaxCloudCoverage() {
			if(seg.map.getScale() <= SCALE){
				scope.maxCloudCoverage = 1;
				return true;
			}
			return false;
		},
		formatMaxCloudCoverage() {
			return scope.maxCloudCoverage < 0 ? 0 : Math.trunc(scope.maxCloudCoverage*100);
		},
		submit() {
			const eoExternal = seg.layer.get("eo_external"),
				loader = seg.modal.openModal("Sentinel", "Layer "+scope.selectedLayer.name+" requested.", {class: "info"});

			const wmsLayer = seg.layer.create({
				id: "Sentinel_" + scope.selectedSatellite.name + "_"+ scope.selectedLayer.name + "_" + moment().toISOString(),
				name: scope.selectedSatellite.name + " " + scope.selectedLayer.name,
				type: "Image",
				extent: scope.selectedArea[0].getGeometry().getExtent(),
				visible: true,
				source: {
					type: "ImageWMS",
					url: scope.selectedSatellite.url,
					params: {
						TIME: scope.timestamp.format("Y-M-D"),
						LAYERS: scope.selectedLayer.id,
						VERSION: "1.3.0",
						CLOUDCORRECTION: (scope.cloud) ? "replace" : "none",
						showLogo: false,
						MAXCC: scope.formatMaxCloudCoverage()
					},
					projections: [
						"EPSG:3857",
						"EPSG:4326"
					]
					// customLoadFunction: async (img, url) => {
					// 	const params = seg.utils.getParams(url),
					// 		transparent = ;
					// 	img.getImage().src = transparent ? seg.source.transparent1x1Image() : (await seg.request.getDataURL(seg.utils.addParamsToURL(url, params)));
					// }
				},
				selectable: {
					featureAs: "layer",
					title: scope.selectedSatellite.name + " " + scope.selectedLayer.name,
					fields: [
						{
							label:"Date",
							template: "<span position-timestamp=\"layer.get('timestamp')\"></span>"
						}, {
							label:"Latitude",
							template: "<span coordinate=\"[layer.get('coordinate')[0],'lat']\"></span>"
						}, {
							label:"Longitude",
							template: "<span coordinate=\"[layer.get('coordinate')[1],'lon']\"></span>"
						}, {
							label:"Max available resolution",
							template: `
								<span ng-if="layer.get('b01') || layer.get('b09') || layer.get('b10')">60m</span>
								<span ng-if="!(layer.get('b01') || layer.get('b09') || layer.get('b10')) && (layer.get('b05') || layer.get('b06') || layer.get('b08a') || layer.get('b11') || layer.get('b12'))">20m</span>
								<span ng-if="!(layer.get('b01') || layer.get('b09') || layer.get('b10') || layer.get('b05') || layer.get('b06') || layer.get('b08a') || layer.get('b11') || layer.get('b12')) && (layer.get('b02') || layer.get('b03') || layer.get('b04') || layer.get('b08'))">10m</span>
							`
						}, {
							label:"Cloud Cover Percentage",
							template: "{{layer.get('cloudCoverPercentage')}}"
						}
					],
					additionalParams: {
						VERSION: "1.3.0",
						CLOUDCORRECTION: (scope.cloud) ? "replace" : "none",
						MAXCC: scope.formatMaxCloudCoverage(),
						TIME: scope.timestamp.format("Y-M-D")
					}
				}
			});

			wmsLayer.setVisible(true);

			wmsLayer.getSource().on("imageloadend", ()  => {
				loader.setContent("Layer " + scope.selectedLayer.name + " loaded successfully.");
				seg.modal.closeTimeout(loader.title);
				seg.favourites.removeAOI(scope.selectedArea[0]);
			});

			wmsLayer.getSource().on("imageloaderror", () => {
				loader.setContent("Error on retrieving Layer " + scope.selectedLayer.name);
				seg.modal.closeTimeout(loader.title);
			});

			eoExternal.addLayer(wmsLayer);

			if (eoExternal.getLayers().length)
				seg.ui.cleanables.add("Clear Sentinel Layers", () => {
					seg.layer.get("eo_external").ol_.setLayers(new ol.Collection());
					seg.layer.get("eo_external").layers = [];
				});
			else seg.ui.cleanables.remove("Clear Sentinel Layers");
		},
		isSubmitDisabled() {
			return (scope.selectedLayer!==null && scope.selectedArea.length!==0) ? "" : "disabled";
		},
		init() {
			//scope.selectedArea = []
		},
		showLayerInformation() {
			seg.popup.openPopup({
				title: scope.selectedSatellite.name + ": " + scope.selectedLayer.name,
				template: "<div scrollable=\"overflow-y\" style=\"width:200px; height: 100px;\">{{selectedLayer.info}}</div>",
				bindings: {
					selectedLayer : scope.selectedLayer
				}
			});
		},
		updateLayers() {
			scope.availableLayers = layersJson[scope.selectedSatellite.id];
		}
	};

	return scope;
})();