/**
@namespace extensibility.modules.feedback
@description
# Apps

## Available options
- **fishery_events** - true/false - Include fisheries feedback form
- **insert_oilspill** - true/false - Include insert oilspills feedback form
- **oil_spills_feedback** - true/false - Include oilspills feedback form
- **sentinel** - true/false - Include sentinel app

## Global configurations and constants
None
*/
const reportAppService = require("./report/service");

module.exports = (config, permissions) => {
	return {
		name: "Apps",
		hasAlerts: config.abm_alert_insert && permissions.abm_alerts.canView,
		init() {
			if (config.fishery_events){
				seg.ui.apps.add({
					name: "Fishery Events",
					label: "Fishery Events",
					icon: "fishery",
					bindings: {
						init(){
							seg.ui.apps.get("Report").bindings.close();
							seg.ui.apps.get("Report").bindings.setFoiType("Fishery Events");
							seg.timeout(() => seg.ui.apps.open("Report"), 500);
						}
					},
					template: "<span ng-init='init()'></span>"
				});
				reportAppService.register("Fishery Events", require("./report/fishery_events/config.js"));
			}

			if(seg.CONFIG.getValue("alerts.NEW_ALERTS") && config.abm_alert_insert && permissions.abm_alerts.canView){
				// Here we fake a little. We create a placeholder app that when it inits, it starts up the report app with the correct foiType.
				seg.ui.apps.add({
					name: "ABM admin",
					label: "ABM admin",
					icon: "feedback",
					bindings: {
						init(){
							seg.ui.apps.get("Report").bindings.close();
							seg.ui.apps.get("Report").bindings.setFoiType("ABM admin");
							seg.timeout(() => seg.ui.apps.open("Report"), 500);
						}
					},
					template: "<span ng-init='init()'></span>"
				});
				reportAppService.register("ABM admin", require("./report/abm_alerts/config.js")(permissions.abm_alerts));
			}

			if(config.insert_oilspill)
				reportAppService.register("Insert Oilspill", require("./report/oilspills_insert/config.js"));

			if(config.oil_spills_feedback)
				reportAppService.register("Oilspills", require("./report/oilspills/config.js")(permissions.oilspills.feedback));

			//register sentinel app
			if (config.sentinel)
				seg.ui.apps.add({
					name: "Sentinel-Hub Data",
					label: "Sentinel-Hub Data",
					icon: "copernicus",
					bindings: {
						scope: require("./sentinel_hub")
					},
					templateUrl: resolvePath("./sentinel_hub/template.html")
				});

			if(reportAppService.getReportScopes().length)
				seg.ui.apps.add({
					name: "Report",
					label: "Report",
					icon: "feedback",
					hidden: true,
					bindings: reportAppService,
					templateUrl: resolvePath("./report/template.html")
				});
		}
	};
};
