/**
 * See more instructions in the end
 */
const reportTypes = {},
	scope = {
		// reportType is the key to change the selected app
		reportType: "",
		feature: null,
		reportOpen: true,
		disableReportTab: true,
		logOpen: false,
		init() {
			if(!scope.reportType || scope.reportType === "")
				for (const type in reportTypes) {
					if (reportTypes[type].getSelectedFeature) {
						scope.feature = scope.getReportScope(type).getSelectedFeature();

						if (scope.feature) {
							scope.reportType = type;
							break;
						}
					}
				}

			scope.getReportScope().init();
			scope.reportOpen = false;
			scope.logs.logFields = scope.getReportScope().displayFields;
			scope.logs.featureDisplayProperties = scope.getReportScope().displayFields_;
		},
		// Returns the functions from the registered app
		getReportScope: (type) => reportTypes[type || scope.reportType],
		getReportTypes: () => reportTypes,
		hasLog: () => scope.getReportScope().hasLog,
		reportIsOpen: () => scope.reportOpen,
		setFoiType(type){
			scope.reportType = type;
		},
		close() {
			seg.ui.apps.close("Report");

			if(scope.getReportScope() && scope.getReportScope().onClose)
				scope.getReportScope().onClose();

			delete scope.reportType;
			delete scope.feature;
		},
		openLogs() {
			scope.logOpen = true;
			scope.reportOpen = false;
			scope.disableReportTab = true;
			scope.getReportScope().update();
		},
		logs: {
			data: [],
			selected: null,
			featureDisplayProperties: {},
			logFields: {},
			functions: {
				// some basic functinos that have purpose
				new() {
					scope.disableReportTab = false;
					scope.getReportScope().scope.clear();

					scope.logOpen = false;
					scope.reportOpen = true;
				},
				see() {
					scope.disableReportTab = false;
					scope.getReportScope().scope.clear();
					scope.getReportScope().openReport(scope.logs.selected);
					scope.getReportScope().toggleEditing(false);

					scope.logs.selected = null;

					scope.logOpen = false;
					scope.reportOpen = true;
				},
				remove() {
					const modal = seg.modal.openModal("Report", "Deleting Report...", {class: "info"}),
						type = (scope.reportType === "fishery events") ? "fisheries" : "Oilspill";

					if(scope.getReportScope().removeReport)
						scope.getReportScope().removeReport(scope.logs.selected).then(() => {
							scope.getReportScope().update();
							modal.setContent("Delete successful");
							seg.modal.closeTimeout(modal.title);
							scope.logs.selected = null;
						}, e => {
							scope.getReportScope().update();
							modal.setContent("Delete Failed");
							seg.modal.closeTimeout(modal.title);
							seg.log.error("ERROR_REPORT_REPORT_scope.REMOVE", e.error + ": " + e.result);
						});
					else if(!scope.logs.functions.removeDisabled()){
						seg.request.delete("v1/feedback/"+type+"/"+ scope.logs.selected.id).then(() => {
							scope.getReportScope().update();
							modal.setContent("Delete successful");
							seg.modal.closeTimeout(modal.title);
							scope.logs.selected = null;
						}, e => {
							scope.getReportScope().update();
							modal.setContent("Delete Failed");
							seg.modal.closeTimeout(modal.title);
							seg.log.error("ERROR_REPORT_REPORT_scope.REMOVE", e.error + ": " + e.result);
						});
					} else {
						modal.setContent("Cannot remove this report");
					}
				},
				openInTTT() {
					if (scope.logs.data.length) {
						const data = scope.logs.selected ? [scope.logs.selected] : scope.logs.data;
						seg.ui.ttt.tables.close("Report");
						seg.ui.ttt.tables.close("All Reports");
						seg.ui.ttt.tables.open({
							label: (data.length === 1) ? "Report" : "All Reports",
							data: data,
							itemAs: "feedback",
							fields: scope.getReportScope().displayFieldsTTT,
							onSelectionChange: scope.getReportScope().onSelectionChange
						});
					}
				},
				openInTTTDisabled() {
					if ((scope.logs.data && !scope.logs.data.length) || (scope.logs.data === null) || (typeof scope.logs.data === "undefined"))
						return "disabled";
				},
				removeDisabled() {
					if (scope.logs.selected && scope.getReportScope().canRemoveReport)
						return !scope.getReportScope().canRemoveReport();
					else
						return scope.logs.functions.disabled();
				},
				update() {
					scope.getReportScope().update().then(res => {
						scope.logs.data = res;
					}, e => seg.log.error("ERROR_REPORT_REPORT_scope.UPDATE", e.error + ": " + e.result));
				},
				filter() {},
				setAsSelected(selected) {
					scope.logs.selected = selected.length ? selected[0].item : null;
				},
				disabled() {
					if ((typeof scope.logs.selected === "undefined") || (scope.logs.selected === null) || ((scope.logs.data === []) || (scope.logs.data === null)))
						return "disabled";
				},
				clear() {
					scope.logs.data = [];
					scope.logs.selected = null;
				}
			}
		},
		// Useful functions
		getDistanceUnits() {
			return seg.preferences.workspace.get("distanceUnits");
		},
		// Useful functions
		getAreaUnits()  {
			return seg.preferences.workspace.get("areaUnits");
		}
	};

/**
 * App service
 * This scope.provides a report template for an app.
 * Acronyms:
 * - FOI: Feature of Interest
 *
 * We export the reportscope.and other functions
 *
 * Functions that the registered types can have:
 * - isValid() - to check if the form is valid.
 * - shouldBeEditable() - to check if the form is isBeingEdited. Some apps can't be edited or viewed after submitted.
 * - isBeingEdited() - to check if app is in edit mode, either from a previous created report or in a new one.
 * - update() - some apps require to have their lists updated.
 * - getSelectedFeature() - most apps have a feature associated to their report, this returns that feature.
 * - hasLog() - most apps have a list of logs that should be shown before the report form.
 * - init() - execute a function when that particular app is initialized.
 * - onClose() - event that is triggered before the window is closed.
 * - openReport(data) - function that is triggered when starting to file a new report, or to edit a previous sent form.
 * - toggleEditing() - we can control the editable status of a form.
 * - canRemoveReport() - check if user has authorization and other conditions to disable the removal button
 * - removeReport(report) - remove report from table and usually sends a request to remove from database.
 * - displayFieldsTTT - object with the fields that will appear when detailed data is requested by the user.
 * - onSelectionChange - event when change selection on the app table
 */

module.exports = {
	scope,
	register(name, config) {
		reportTypes[name] = config;
	},
	isValid() {
		const isValid = scope.getReportScope().isValid,
			shouldBeEditable = scope.getReportScope().shouldBeEditable,
			isBeingEdited = scope.getReportScope().scope.isBeingEdited;

		if (shouldBeEditable && shouldBeEditable()) {
			return (isValid && isValid()) && (isBeingEdited && isBeingEdited());
		} else {
			return (isValid && isValid());
		}
	},
	shouldBeEditable() {
		const shouldBeEditable = scope.getReportScope().shouldBeEditable;
		return !shouldBeEditable || shouldBeEditable();
	},
	getReportScopes() {
		return Object.keys(reportTypes);
	},
	setLogs(data) {
		scope.logs.logFields = scope.getReportScope().displayFields;
		delete scope.logs.data;
		scope.logs.data = data;
	},
	setFoiType(type){
		scope.setFoiType(type);
	},
	close(){
		scope.close();
	},
	// Helper functions used in some apps for units
	getDistanceUnits() {
		return seg.preferences.workspace.get("distanceUnits");
	},
	getAreaUnits()  {
		return seg.preferences.workspace.get("areaUnits");
	},
	getSpeedUnits() {
		return seg.preferences.workspace.get("speedUnits");
	},
	getTimeUnits() {
		return seg.preferences.workspace.get("timeUnits");
	}
};