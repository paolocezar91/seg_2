const scope = {},
	{getLabel} = require("../../../eo/SAR/oil_spills/api.js"),
	reportService = require("../service.js");

scope.edit = true;
scope.onSiteObservation = {
	functions: {
		clear() {
			scope.onSiteObservation.active = false;
			scope.onSiteObservation.reasonsNoObservation = null;
			scope.onSiteObservation.compiler = "";
			scope.onSiteObservation.emsa_comment = "";

			scope.onSiteObservation.position.value = "";
			scope.onSiteObservation.position.eoImageId = "";
			scope.onSiteObservation.position.coordinates = [0, 0];
			scope.onSiteObservation.position.areas = [];

			scope.onSiteObservation.observer.active = false;
			scope.onSiteObservation.observer.country = {value: seg.user.getCurrentUser().country};
			scope.onSiteObservation.observer.observer = "";
			scope.onSiteObservation.observer.organization = "";
			scope.onSiteObservation.observer.comments = "";
			scope.onSiteObservation.datetime = moment();
			scope.onSiteObservation.method = null;

			scope.onSiteObservation.type = null;
			scope.onSiteObservation.subTypeOptions = [];
			scope.onSiteObservation.subtype = null;

			scope.onSiteObservation.additionalInformation = "";
			scope.onSiteObservation.priority = false;

			scope.onSiteObservation.parameters.active = false;
			scope.onSiteObservation.parameters.vwidth = "";
			scope.onSiteObservation.parameters.vthick = "";
			scope.onSiteObservation.parameters.vvolume = "";
			scope.onSiteObservation.parameters.vlength = "";
			scope.onSiteObservation.parameters.varea = "";
			scope.onSiteObservation.parameters.vthickmethod = "";
			scope.onSiteObservation.parameters.vbaoac = "";

			const oilspill = seg.selection.getSelected("oil_spills").length ? seg.selection.getSelected("oil_spills")[0] : null;
			if (oilspill)
				scope.onSiteObservation.position.coordinates = ol.proj.transform(ol.extent.getCenter(oilspill.getGeometry().getExtent()), seg.map.getProjection(), "EPSG:4326");
		},
		isValid() {
			const oso = scope.onSiteObservation;

			if(oso.active){
				return true;
			} else {
				if (oso.reasonsNoObservation !== null)
					return true;
			}
			return false;
		},
		resetReasonsNoObservation() {
			scope.onSiteObservation.reasonsNoObservation = null;
		},
		updateSubTypeOptions(subtype = "") {
			switch (scope.onSiteObservation.type) {
				case "Other substance confirmed":
					scope.onSiteObservation.subTypeOptions = ["Chemical oil", "Vegetable oil", "Fish oil", "Sewage", "Garbage", "Unknown substance"];
					break;
				case "Natural phenomena observed":
					scope.onSiteObservation.subTypeOptions = ["Algae", "Current front", "Pollen", "Sandbank", "Seaweed", "Upwelling Area", "Windless Area", "Unknown phenomena"];
					break;
				default:
					scope.onSiteObservation.subTypeOptions = [];
					break;
			}
			scope.onSiteObservation.subtype = subtype;
		},
		disable() {
			return ((scope.onSiteObservation.type === "") || (scope.onSiteObservation.type !== "Other substance confirmed" && scope.onSiteObservation.type !== "Natural phenomena observed")) ? true : null;
		},
		canRaisePriority(){
			// to raise priority of a feedback:
			return scope.edit && //feedback must be in edit mode
				scope.canRaisePriority && // canRaisePriority must be true (check permissions)
				scope.id && // feedback must have an id (that means it is not a new feedback)
				!scope.onSiteObservation.priority; // current priority must be low
		},
		raisePriority(){
			if (!scope.onSiteObservation.priority && scope.id) { // if priority is true, raise it's priority
				const feature = reportService.scope.feature,
					modal = seg.modal.openModal("Report oilspill priority", "Rising priority of feedback " + scope.id +"...", {class: "info"});

				seg.request.get("v1/feedback/raisePriority/OilSpill/" + feature.get("eventId"), {
					params: {
						featureId: scope.id
					}
				}).then(() => {
					modal.setContent("Priority has been risen.");
					scope.onSiteObservation.priority = true;
					seg.modal.closeTimeout(modal.title);
				}, e => seg.log.error("ERROR_REPORT_OILSPILLS_RAISE_PRIORITY", e.error + ": " + e.result));
			}
		}
	},
	active: false,
	reasonsNoObservation: null,
	reasonsNoObservationOptions: [
		"Weather conditions",
		"No assets available",
		"Considered as a lookalike",
		"Not operationally relevant"
	],
	compiler: seg.user.getCurrentUser().lastname,
	datetime: moment(),
	observer: {
		active: false,
		country: {value: ""},
		observer: null,
		organization: null
	},
	priority: false,
	method: null,
	position: {
		value: "",
		options: ["Coordinates", "Image", "AOI"],
		coordinates: [0, 0],
		eoImageId: "",
		areas: []
	},
	type: null,
	typeOptions: [
		{"id":1, "type":"Mineral oil confirmed"},
		{"id":2, "type":"Other substance confirmed"},
		{"id":3, "type":"Natural phenomena observed"},
		{"id":4, "type":"Unknown feature observed"},
		{"id":5, "type":"Nothing observed"}
	],
	subtype: null,
	subTypeOptions: [],
	parameters: {
		active: false,
		vwidth: null,
		vthick: null,
		vvolume: null,
		vlength: null,
		varea: null,
		vthickmethod: null,
		vbaoac: null
	},
	methodOptions: ["Fixed wing aircraft", "Helicopter", "Patrol vessel", "Merchant vessel", "RPAS", "In-situ platform", "Other"]
};

scope.possibleSources = {
	active: false,
	reportedSources: [],
	newSources: [],
	methodOptions: [
		{value: "On-site Observation", label: "On-site Observation"},
		{value: "Automatic analysis", label: "Automatic analysis"},
		{value: "Ship Inspection", label: "Ship Inspection"},
		{value: "Human Analysis", label: "Human Analysis"},
		{value: "", label: "N/A"}
	],
	possibleSourcesType: [
		"Vessel",
		"Offshore installation",
		"Wreck",
		"Pipeline",
		"Natural seepage",
		"Windmill",
		"Fish farm",
		"Accident",
		"Incident",
		"Other"
	],
	functions: {
		fetchVessel(index) {
			const selectedVessel = seg.selection.getSelected("positions");
			if (selectedVessel.length === 1){
				scope.possibleSources.newSources[index].name = selectedVessel[0].get("shipName");
				scope.possibleSources.newSources[index].imo = selectedVessel[0].get("imo");
				scope.possibleSources.newSources[index].mmsi = selectedVessel[0].get("mmsi");
			}
		},
		anyVessel(src) {
			return scope.possibleSources[src].some((source) => source.type === "Vessel");
		},
		newSource() {
			scope.possibleSources.newSources.push({
				detected: null,
				identified: null,
				type: null,
				userReported: true
			});
		},
		removeSource(source) {
			scope.possibleSources.newSources.splice(scope.possibleSources.newSources.indexOf(source), 1);
		},
		clear() {
			scope.possibleSources.reportedSources = [];
			scope.possibleSources.newSources = [];
			scope.possibleSources.active = false;
		}
	}
};

scope.linkOtherSpills = {
	active: false,
	functions: {
		associate() {
			if (scope.linkOtherSpills.functions.validateSpill())
				scope.linkOtherSpills.spills.push({
					source: scope.linkOtherSpills.spillId,
					data: (scope.linkOtherSpills.parameters.on) ? scope.linkOtherSpills.parameters.generic : ""
				});
		},
		fetchSelectedSpillId() {
			const selectedOilSpills = seg.selection.getSelected("oil_spills");
			if (selectedOilSpills.length === 1)
				scope.linkOtherSpills.spillId = getLabel(selectedOilSpills[0]);
		},
		removeSpill(spill) {
			scope.linkOtherSpills.spills.splice(scope.linkOtherSpills.spills.indexOf(spill), 1);
		},
		validateSpill() {
			const self = scope.linkOtherSpills;
			if (self.spillId !== null)
				return true;

			return false;
		},
		isValid() {
			return true;
		},
		clear() {
			scope.linkOtherSpills.spillId = "";
			scope.linkOtherSpills.parameters = {
				on: false,
				generic: ""
			};
			scope.linkOtherSpills.spills = [];
		}
	},
	spillId: null,
	parameters: {
		on: false,
		generic: null
	},
	spills: []
};

scope.attach = {
	attachments: [],
	attach(file) {
		const feature = reportService.scope.feature;

		scope.attach.attachments.push({
			file: file,
			fileName: file.name,
			path: "/report/" + "S_" + feature.get("eventId") + "/" + file.name
		});
	},
	functions: {
		clear() {
			scope.attach.attachments = [];
		},
		removeAttachment(attachment) {
			// If it has a fileId it is not a new attachment, so in order to remove we must make a delete query to WS
			if (attachment.fileId) {
				seg.request.delete("v1/feedback/attachment", {
					params: {
						fileId: attachment.fileId
					}
				}).then((res) => {
					if (res.status === "success") {
						const index = scope.attach.attachments.indexOf(attachment);
						scope.attach.attachments.splice(index, 1);
					}
				}, e => seg.log.error("ERROR_REPORT_OILSPILLS_REMOVE_ATTACHMENT", e.error + ": " + e.result));
			} else {
				const index = scope.attach.attachments.indexOf(attachment);
				scope.attach.attachments.splice(index, 1);
			}
		},
		downloadAttachment(attachment) {
			if (!attachment.file) {
				const modal = seg.modal.openModal("File Download", "Downloading " + attachment.fileName, {class: "info"});

				const link = document.createElement("a");
				document.body.appendChild(link);
				link.setAttribute("type", "hidden");
				link.href = "v1/feedback/attachment?fileId=" + attachment.fileId;
				link.download = attachment.fileName;
				link.click();

				seg.modal.closeTimeout(modal.title);
			}
		}
	}
};

scope.submit = () => {
	const feature = reportService.scope.feature;
	const modal = seg.modal.openModal("Report oilspill", !scope.id ? "Inserting oilspill report..." : "Updating oilspill report...", {class: "info"});

	//build structure to send
	const toSubmit = {
		onSiteObservation: !scope.onSiteObservation.active ? {
			observer: {
				country: {
					value: seg.user.getCurrentUser().country
				}
			},
			reasonsNoObservation: scope.onSiteObservation.reasonsNoObservation,
			compiler: scope.onSiteObservation.compiler
		} : {
			evtId: scope.onSiteObservation.eventId,
			position: {
				value: scope.onSiteObservation.position.value,
				areas: (new ol.format.GeoJSON()).writeFeatures(scope.onSiteObservation.position.areas, {
					dataProjection: "EPSG:4326",
					featureProjection: seg.map.getProjection()
				}),
				coordinates: scope.onSiteObservation.position.coordinates,
				eoImageId: scope.onSiteObservation.position.eoImageId
			},
			observer: {
				active: scope.onSiteObservation.observer.active,
				country: {
					value: scope.onSiteObservation.observer.country.value
				},
				organization: scope.onSiteObservation.observer.organization,
				observer: scope.onSiteObservation.observer.observer
			},
			priority: scope.onSiteObservation.priority,
			method: scope.onSiteObservation.method,
			datetime: scope.onSiteObservation.datetime.toISOString(),
			type: scope.onSiteObservation.type,
			subtype: scope.onSiteObservation.subtype,
			parameters: !scope.onSiteObservation.parameters.active ? {} : {
				vwidth: seg.utils.convertToMeters(scope.onSiteObservation.parameters.vwidth, seg.preferences.workspace.get("distanceUnits")).toString(),
				vthick: scope.onSiteObservation.parameters.vthick,
				vvolume: scope.onSiteObservation.parameters.vvolume,
				vlength: seg.utils.convertToMeters(scope.onSiteObservation.parameters.vlength, seg.preferences.workspace.get("distanceUnits")).toString(),
				varea: seg.utils.convertToSquareMeters(scope.onSiteObservation.parameters.varea, seg.preferences.workspace.get("areaUnits")).toString(),
				vthickmethod: scope.onSiteObservation.parameters.vthickmethod,
				vbaoac: scope.onSiteObservation.parameters.vbaoac
			},
			additionalInformation: scope.onSiteObservation.additionalInformation
		},
		possibleSources: !scope.possibleSources.active ? {} : {
			reportedSources: scope.possibleSources.reportedSources,
			newSources: scope.possibleSources.newSources
		},
		linkOtherSpills: !scope.linkOtherSpills.active ? {} : {
			spills: scope.linkOtherSpills.spills
			// source: string
			// data: string
		},
		attach: {
			attachments: scope.attach.attachments
		}
	};


	const requests = [];

	requests.push(seg.request.put(!scope.id ?
		"v1/feedback/Oilspill/" + feature.get("eventId") :
		"v1/feedback/Oilspill/" + feature.get("eventId") + "?featureId=" + scope.id, {
		data: {
			feedback: toSubmit
		}
	}));

	return seg.promise.all(requests).then(([res]) => {
		const feedbackId = scope.id || res.result[res.result.length -1].feedbackId;

		modal.setContent(!scope.id ? "Report inserted successfully" : "Report updated successfully");
		modal.setClass("success");
		seg.modal.closeTimeout(modal.title);

		const attachRequests = [];

		scope.attach.attachments.forEach(attachment => {
			if (attachment.file){
				attachRequests.push(seg.request.upload({
					url: "v1/feedback/attachment?feedbackId=" + feedbackId,
					file: attachment.file,
					pathToSave: attachment.path
				}).then(res => ({res, attachment}), res => ({res, attachment})),
				e => seg.log.error("ERROR_REPORT_OILSPILLS_SUBMIT_FEEDBACK", e.error + ": " + e.result));

			}
		});

		seg.promise.all(attachRequests).then((res) => {
			res.forEach(({res, attachment}) => {
				let modalFile;
				if(res.status !== "error" || res.error)
					modalFile = seg.modal.openModal(attachment.fileName, attachment.fileName + " was uploaded successfully.", {class: "success"});
				else
					modalFile = seg.modal.openModal(attachment.fileName, + attachment.fileName + " failed to upload.", {class: "success"});
				seg.modal.closeTimeout(modalFile.title);
			});
		}, e => seg.log.error("ERROR_REPORT_OILSPILLS_SUBMIT_UPLOAD", e.error + ": " + e.result));
	}, e => {
		modal.setContent(!scope.id ? "Error inserting oilspill report": "Error updating oilspill report");
		seg.modal.closeTimeout(modal.title);
		seg.log.error("ERROR_REPORT_OILSPILLS_SUBMIT_ALL", e.error + ": " + e.result);
	});
};

scope.clear = () => {
	scope.onSiteObservation.functions.clear();
	scope.possibleSources.functions.clear();
	scope.attach.functions.clear();

	scope.onSiteObservation.compiler = seg.user.getCurrentUser().lastname;
	scope.onSiteObservation.priority = false;

	scope.onSiteObservation.active = false;
	scope.possibleSources.active = false;
	scope.linkOtherSpills.active = false;
	scope.edit = true;
	scope.owner = seg.user.getCurrentUser().username;
	delete scope.id;
};

scope.isBeingEdited = () => scope.edit;
// scope.isEMSAUser = () => seg.user.isEMSAUser();
scope.isValid = () => scope.onSiteObservation.functions.isValid();
scope.isOwnedByUser = (username) => scope.owner === username;

module.exports = scope;
