module.exports = {
	displayPreferences_: {
		id: {
			label: "OilSpill ID",
			value: "{{scope.getReportScope().getLabel(scope.feature)}}",
			selected: true
		},
		serviceId: {
			label: "Service ID",
			value: "{{scope.feature.get('serviceID')}}",
			selected: true
		},
		origin: {
			label: "Origin",
			value: "{{scope.feature.get('origin') === 'SYNTHETIC' ? 'EXPECTED' : (scope.feature.get('origin'))}}",
			selected: true
		},
		timeStamp: {
			label: "Acquisition Start",
			value: "<span ng-if=\"scope.feature.get('timeStamp')\" position-timestamp=\"scope.feature.get('timeStamp')\"></span>",
			selected: true
		},
		class: {
			label: "Class",
			value: "{{scope.feature.get('class')}}",
			selected: false
		},
		area: {
			label: "Area ({{scope.getAreaUnits()}})",
			value: "{{(scope.feature.get('area') | area:\"m2\" | number) || 'N/A'}}",
			selected: true
		},
		length: {
			label: "Length ({{scope.getDistanceUnits()}})",
			value: "{{(scope.feature.get('length') | distance:\"m\" | number) || 'N/A'}}",
			selected: true
		},
		width: {
			label: "Width ({{scope.getDistanceUnits()}})",
			value: "{{(scope.feature.get('width') | distance:\"m\" | number) || 'N/A'}}",
			selected: true
		},
		centerPosition: {
			label: "Center Position",
			value: `
				<span coordinate="[scope.feature.get('center').coordinates[1], 'lat']"></span>
				<span coordinate="[scope.feature.get('center').coordinates[0], 'lon']"></span>
			`,
			selected: true
		},
		distanceFromCoast: {
			label: "Distance from coast ({{scope.getDistanceUnits()}})",
			value: "{{oilSpill.get('distanceFromCoast') | distance:\"m\" | number}}",
			selected: false
		},
		possibleSourcesDetected: {
			label: "Possible source detected",
			value: "{{scope.feature.get('possibleSourcess') ? \"True\" : \"False\"}}",
			selected: false
		}
	},
	displayFieldsTTT: {
		id: {
			label: "id",
			template: "{{feedback.id}}",
			visible: true
		},
		oso_priority: {
			label: "Priority",
			template: "{{(feedback.content.onSiteObservation.priority) ?\"HIGH\":\"LOW\"}}",
			visible: true
		},
		timestamp: {
			label: "Obs. Date/Time",
			template: "<span position-timestamp=\"feedback.content.onSiteObservation.datetime\"></span>",
			visible: true
		},
		oso_obsmethod: {
			label: "Method",
			template: "{{feedback.content.onSiteObservation.method}}",
			visible: true
		},
		oso_lat: {
			label: "Latitude",
			template: "<span coordinate=\"[feedback.content.onSiteObservation.position.coordinates[1],'lat']\"></span>",
			visible: true
		},
		oso_long: {
			label: "Longitude",
			template: "<span coordinate=\"[feedback.content.onSiteObservation.position.coordinates[0],'lon']\"></span>",
			visible: true
		},
		observer_country: {
			label: "Observer Country",
			template: "{{feedback.content.onSiteObservation.observer.country.value}}",
			visible: true
		},
		observer_organization: {
			label: "ObserverOrganization",
			template: "{{feedback.content.onSiteObservation.observer.organization}}",
			visible: true
		},
		observer_observer: {
			label: "Observer",
			template: "{{feedback.content.onSiteObservation.observer.observer}}",
			visible: true
		},
		oso_type: {
			label: "Type",
			template: "{{feedback.content.onSiteObservation.type || \"N/A\"}}",
			visible: true
		},
		oso_subtype: {
			label: "Subtype",
			template: "{{feedback.content.onSiteObservation.subtype || \"N/A\"}}",
			visible: true
		},
		oso_parameters: {
			label: "Parameters",
			template: "{{(feedback.content.onSiteObservation.parameters)?feedback.content.onSiteObservation.parameters.vthick+\"; \"+feedback.content.onSiteObservation.parameters.vwidth+\"; \"+feedback.content.onSiteObservation.parameters.vvolume+\"; \"+feedback.content.onSiteObservation.parameters.vlength+\"; \"+feedback.content.onSiteObservation.parameters.varea+\"; \"+feedback.content.onSiteObservation.parameters.vthickmethod+\"; \"+feedback.content.onSiteObservation.parameters.vbaoac:\"NO\"}}",
			visible: true
		},
		oso_additionalInformation: {
			label: "Additional Information",
			template: "{{feedback.content.onSiteObservation.additionalInformation}}",
			visible: true
		},
		oso_reasonsNoObservation: {
			label: "Reasons No Observation",
			template: "{{feedback.content.onSiteObservation.reasonsNoObservation}}",
			visible: true
		},
		possibleSources: {
			label: "Possible Sources",
			template: "{{feedback.content.possibleSources.reportedSources.length ? \"TRUE\" : \"FALSE\"}}",
			visible: true
		},
		// linkOtherSpills: {
		// 	label: "Link Other Spills",
		// 	template: "{{feedback.content.linkOtherSpills.spills.length ? \"VIEW DETAILS\" : \"N/A\"}}",
		// 	visible: true
		// },
		attachments: {
			label: "Attach",
			template: "{{feedback.content.attach.attachments.length ? feedback.content.attach.attachments.length : \"NO\"}}",
			visible: true
		},
		submited: {
			label: "Submited at",
			template: "<span position-timestamp=\"feedback.timestamp\"></span>",
			visible: true
		}
	},
	displayFields: {
		id: {
			label: "id",
			template: "{{feedback.id}}",
			visible: true
		},
		oilspill_id: {
			label: "OilSpill ID",
			template: "{{feedback.oilspill_id}}",
			visible: true
		},
		type: {
			label: "Report type",
			template: "{{feedback.content.onSiteObservation.type || 'N/A'}}",
			visible: true
		},
		subtype: {
			label: "Subtype",
			template: "{{feedback.content.onSiteObservation.subtype || 'N/A'}}",
			visible: true
		},
		country: {
			label: "Country",
			template: "{{(feedback.content.onSiteObservation.observer.country.value) || 'N/A'}}",
			visible: true
		},
		method: {
			label: "Method",
			template: "{{feedback.content.onSiteObservation.method || 'N/A'}}",
			visible: true
		},
		observation_datetime: {
			label: "Observation date/time",
			template: `
				<span ng-if="feedback.content.onSiteObservation.datetime"><span position-timestamp="feedback.content.onSiteObservation.datetime"></span></span>
				<span ng-if="!feedback.content.onSiteObservation.datetime">N/A</span>
			`,
			visible: true
		},
		submission_datetime: {
			label: "Submission date/time",
			template: `
				<span ng-if="feedback.timestamp" position-timestamp="feedback.timestamp"></span>
				<span ng-if="!feedback.timestamp">N/A</span>
			`,
			visible: true
		},
		priority: {
			label: "Priority",
			template: "{{(feedback.content.onSiteObservation.priority)?\"YES\":\"NO\"}}",
			visible: true
		},
		reasonsNoObservation: {
			label: "Reasons No Observation",
			template: "{{feedback.content.onSiteObservation.reasonsNoObservation || 'N/A'}}",
			visible: true
		}
	}
};