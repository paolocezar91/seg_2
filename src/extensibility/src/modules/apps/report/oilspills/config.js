const {displayPreferences_, displayFieldsTTT, displayFields} = require("./properties.js"),
	{getLabel} = require("../../../eo/SAR/oil_spills/api.js"),
	reportService = require("../service.js"),
	config = {
		class: "feedback-form",
		logTitle: "List of Feedbacks",
		reportTitle: "Feedback",
		exportTitle: "",
		templateUrl: "extensibility/modules/apps/report/oilspills/template.html",
		scope: require("./scope.js"),
		displayPreferences_,
		displayFieldsTTT,
		displayFields,
		originField: "Service Detected",
		init() {
			config.scope.clear();
			const oilspill = reportService.scope.feature;

			config.exportTitle = getLabel(oilspill);

			if (oilspill) {
				config.scope.onSiteObservation.position.coordinates = ol.proj.transform(ol.extent.getCenter(oilspill.getGeometry().getExtent()), seg.map.getProjection(), "EPSG:4326");
				config.scope.onSiteObservation.evtId = oilspill.get("eventId");
			}

			// Values for type options should come from this service, but it still returning an empty array.
			seg.request.get("v1/feedback/types/feedback")
				.then(({result}) => {
					if(result.length)
						config.scope.onSiteObservation.typeOptions = result;
				}, e => seg.log.error("ERROR_REPORT_OILSPILLS_INIT", e.error + ": " + e.result));
		},
		toggleEditing(value) {
			if (config.shouldBeEditable()) {
				if (value === false)
					config.scope.edit = false;
				else
					config.scope.edit = value || !config.scope.edit;
			}
		},
		disableEditButton() {
			// Checking if user is EMSA User
			return !((config.scope.canEditOwn && config.scope.isOwnedByUser(seg.user.getCurrentUser().username)) || config.scope.canEditAll);
		},
		openReport(feedback) {
			if (!feedback)
				return;

			if (feedback.content.onSiteObservation){
				if (!feedback.content.onSiteObservation.reasonsNoObservation ||
					!feedback.content.onSiteObservation.compiler ||
					(feedback.content.onSiteObservation.observer && !feedback.content.onSiteObservation.observer.comments)
				)
					config.scope.onSiteObservation.active = true;

				Object.assign(config.scope.onSiteObservation, feedback.content.onSiteObservation);

				if(feedback.content.onSiteObservation.parameters && Object.values(feedback.content.onSiteObservation.parameters).some(value => value !== "")){
					config.scope.onSiteObservation.parameters.active = true;
					// value is converted to meters when sent to service, so we need to convert back to whatever unit the user is using
					if (feedback.content.onSiteObservation.parameters.vwidth !== "")
						config.scope.onSiteObservation.parameters.vwidth = Number(feedback.content.onSiteObservation.parameters.vwidth);
					if (feedback.content.onSiteObservation.parameters.vlength !== "")
						config.scope.onSiteObservation.parameters.vlength = Number(feedback.content.onSiteObservation.parameters.vlength);
					if (feedback.content.onSiteObservation.parameters.varea !== "")
						config.scope.onSiteObservation.parameters.varea = Number(feedback.content.onSiteObservation.parameters.varea);
				}

				config.scope.onSiteObservation.datetime = moment(feedback.content.onSiteObservation.datetime);

				if(feedback.content.onSiteObservation.observer && Object.values(feedback.content.onSiteObservation.observer).some(value => value !== ""))
					config.scope.onSiteObservation.observer.active = true;
			}

			if (feedback.content.possibleSources && feedback.content.possibleSources.reportedSources.length){
				config.scope.possibleSources.active = true;
				Object.assign(config.scope.possibleSources, feedback.content.possibleSources);
			}

			if (feedback.content.linkOtherSpills && feedback.content.linkOtherSpills.spills.length) {
				config.scope.linkOtherSpills.active = true;
				Object.assign(config.scope.linkOtherSpills, feedback.content.linkOtherSpills);
			}

			const oilspill = reportService.scope.feature || seg.layer.get("oil_spills").getSource().getFeatureById(feedback.feature.id);

			if (
				!config.scope.onSiteObservation.position.coordinates || (
					config.scope.onSiteObservation.position.coordinates[0] === 0 &&
					config.scope.onSiteObservation.position.coordinates[1] === 0
				)
			)
				config.scope.onSiteObservation.position.coordinates = ol.extent.getCenter(oilspill.getGeometry().getExtent());

			if (!config.scope.onSiteObservation.position.value)
				config.scope.onSiteObservation.position.value = "";

			if (!config.scope.onSiteObservation.position.eoImageId)
				config.scope.onSiteObservation.position.eoImageId = "";

			if (!config.scope.onSiteObservation.position.areas)
				config.scope.onSiteObservation.position.areas = [];

			config.scope.attach.attachments = feedback.content.attach.attachments;

			config.scope.id = feedback.id;

			if(config.scope.id)
				config.scope.edit = false;

			config.scope.owner = feedback.owner;

			config.scope.onSiteObservation.functions.updateSubTypeOptions(feedback.content.onSiteObservation.subtype);
		},
		canRemoveReport(){
			return config.scope.canDeleteAll;
		},
		update() {
			if (reportService.scope.feature === null)
				return;

			const feature = reportService.scope.feature;

			return seg.request.get("v1/feedback/Oilspill/" + feature.get("eventId")).then(({result}) => {
				const resultingFeedbacks = result.reduce((lists, feedback) => {
					if (feedback.jsonFeedback && Object.keys(feedback.jsonFeedback).length)
						lists.push({
							id: feedback.feedbackId, //
							oilspill_id: getLabel(feature),
							content: feedback.jsonFeedback, //
							timestamp: moment(feedback.timeStamp), //
							owner: feedback.username,
							feature
						});

					return lists;
				}, []);

				reportService.setLogs(resultingFeedbacks);
			}, e => seg.log.error("ERROR_REPORT_OILSPILLS_UPDATE", e.error + ": " + e.result));
		},
		getLabel,
		getSelectedFeature() {
			const feature = seg.selection.getSelected("oil_spills");
			return (feature && feature[0]) || null;
		},
		isValid() {
			return !!reportService.scope.feature && (
				(
					config.scope.canEditOwn &&
					config.scope.isOwnedByUser(seg.user.getCurrentUser().username)
				) || config.scope.canEditAll
			) && config.scope.isValid();
		},
		shouldBeEditable() {
			// Oilspills feedback must have a service id
			// User must be either the owner of the oilspill and canEditOwn must be true
			// or canEditAll must be true
			// check permissions/index.js to see how canEditOwn & canEditAll can be toggled
			return config.scope.id && (
				config.scope.canEditOwn && config.scope.isOwnedByUser(seg.user.getCurrentUser().username) ||
				config.scope.canEditAll
			);
		},
		submit() {
			config.scope.submit();
		},
		hasLog: true
	};

module.exports = ({canRaisePriority, canEditAll, canDeleteAll, canEditOwn}) => {
	// canRaisePriority is defined by user permissions
	Object.assign(config.scope, {
		canRaisePriority, canEditAll, canDeleteAll, canEditOwn
	});
	return config;
};
