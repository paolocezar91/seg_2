const reportService = require("../service.js");

const config = {
	class: "feedback-form",
	reportTitle: "Feedback",
	templateUrl: "extensibility/modules/apps/report/oilspills_insert/template.html",
	scope: require("./scope.js"),
	displayPreferences_: {
		id: {
			label: "Service ID",
			value: "{{scope.feature.get('serviceID')}}",
			selected: true
		},
		operation: {
			label: "Operation(s)",
			value: "{{scope.feature.get('operations')}}",
			selected: true
		}
	},
	originField: "User Reported",
	init() {
		config.scope.clear();
		const feature = reportService.scope.feature;
		if (feature && feature.get("seg_type") === "eo_sar_imagery"){
			config.scope.observation.geometry = ol.proj.transform(ol.extent.getCenter(feature.getGeometry().getExtent()), seg.map.getProjection(), "EPSG:4326");
			config.scope.observation.operations = feature.get("operations");
			config.scope.properties.eventId = feature.get("doi");
			config.scope.properties.name = feature.get("doi");
			config.scope.properties.serviceId = feature.get("serviceID").toString();
		}
	},
	source: seg.layer.get("oil_spills").getSource(),
	openReport(feedback) {
		if (!feedback)
			return;
	},
	update() {},
	getSelectedFeature() {
		const feature = seg.selection.getSelected("eo_sar_imagery");
		if (feature[0])
			return feature[0];
		return null;
	},
	isValid() {
		return !!reportService.scope.feature &&
			config.scope.isValid();
	},
	shouldBeEditable() {
		return false;
	},
	submit() {
		config.scope.submit();
	},
	hasLog: false
};

module.exports = config;
