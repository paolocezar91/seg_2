const scope = {};

scope.properties = {
	origin: "EXPECTED", //
	dataSource: "", //
	tstamp: moment(), //
	serviceId: "", //
	name: "", //
	eventId: "" //
};

scope.observation = {
	geometry: "",
	area: "",
	length: "",
	width: "",
	thick: "",
	thickmethod: "",
	volume: "",
	baoac: "",
	organization: "",
	observer: "",
	observationMethod : null,
	methodOptions: ["Fixed wing aircraft", "Helicopter", "Patrol vessel", "Merchant vessel", "Other"],
	type: null,
	typeOptions: ["Mineral oil confirmed", "Other substance confirmed"],
	subtype: null,
	subTypeOptions: [],
	additionalInformation: "",
	operations: null,
	functions: {
		updateSubTypeOptions: () => {
			switch (scope.observation.type) {
				case "Other substance confirmed":
					scope.observation.subTypeOptions = ["Chemical oil", "Vegetable oil", "Fish oil", "Sewage", "Garbage", "Unknown substance"];
					break;
				default:
					scope.observation.subTypeOptions = [];
					break;
			}
			scope.observation.subtype = "";
		}
	}
};

scope.isValid = () => {
	if (
		scope.observation.observationMethod && (
			scope.observation.type === "Mineral oil confirmed" || (
				scope.observation.type === "Other substance confirmed" && scope.observation.subtype
			)
		)
	)
		return true;
};

scope.submit = () => {
	// Reverse is necessary since EODC accept only LAT/LON and not LON/LAT
	// Also first and last geometry points must be the same, so we send them duplicated
	const extensionGeometry = [].concat(...scope.observation.geometry).reverse().join(" ");

	//build structure to send
	const modal = seg.modal.openModal("Report oilspill", "Inserting oilspill report...", {class: "info"}),
		oilspill = {
			origin: scope.properties.origin,
			observer: scope.observation.observer,
			locationClassifications: [{country: scope.observation.country}],
			organization: scope.observation.organization,
			tstamp: scope.properties.tstamp.toISOString().split(".")[0]+"Z",
			serviceId: Number(scope.properties.serviceId),
			name: scope.properties.name,
			eventId: scope.properties.eventId,
			extensions: [{
				serviceId: scope.properties.serviceId,
				geometry: extensionGeometry + " " + extensionGeometry,
				width: seg.utils.convertToMeters(scope.observation.width, seg.preferences.workspace.get("distanceUnits")).toString(),
				length: seg.utils.convertToMeters(scope.observation.length, seg.preferences.workspace.get("distanceUnits")).toString(),
				area: seg.utils.convertToSquareMeters(scope.observation.area, seg.preferences.workspace.get("areaUnits")).toString()
			}],
			center: extensionGeometry,
			width: seg.utils.convertToMeters(scope.observation.width, seg.preferences.workspace.get("distanceUnits")).toString(),
			length: seg.utils.convertToMeters(scope.observation.length, seg.preferences.workspace.get("distanceUnits")).toString(),
			area: seg.utils.convertToSquareMeters(scope.observation.area, seg.preferences.workspace.get("areaUnits")).toString(),
			thick: scope.observation.thick,
			thickmethod: scope.observation.thickmethod,
			volume: scope.observation.volume,
			baoac: scope.observation.baoac,
			observationMethod : scope.observation.observationMethod,
			type: scope.observation.type,
			subtype: scope.observation.subtype,
			additionalInformation: scope.observation.additionalInformation,
			operations: {
				operationName: scope.observation.operations
			}
		};

	return seg.request.put("v1/eo/oilspill", {
		data: {
			oilspill
		}
	}).then(() => {
		modal.setContent("Oilspill inserted successfully");
		seg.modal.closeTimeout(modal.title);
	}, e => {
		modal.setContent("Error inserting oilspill");
		seg.modal.closeTimeout(modal.title);
		seg.log.error("ERROR_REPORT_OILSPILLS_INSERT_SUBMIT", e.error + ": " + e.result);
	});
};

scope.clear = () => {
	scope.properties = Object.assign(scope.properties, {
		origin: "EXPECTED", //
		tstamp: moment(), //
		serviceId: "", //
		name: "", //
		eventId: "" //
	});

	scope.observation = Object.assign(scope.observation, {
		geometry: "",
		area: "",
		length: "",
		width: "",
		thick: "",
		thickmethod: "",
		volume: "",
		baoac: "",
		organization: "",
		observer: "",
		observationMethod: null,
		type: null,
		subtype: null,
		subTypeOptions: [],
		additionalInformation: "",
		operations: null
	});
};

module.exports = scope;
