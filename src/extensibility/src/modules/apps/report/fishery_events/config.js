const reportService = require("../service.js");

const config = {
	templateUrl: "extensibility/modules/apps/report/fishery_events/template.html",
	scope: require("./scope.js"),
	class: "fishery-form",
	originField:"User Reported",
	displayPreferences_: {
		id: {
			label: "Fishery Event Identifier",
			value: "{{report.feature.get('id')}}",
			selected: true
		},
		name: {
			label: "Name",
			value: "{{report.feature.get('name')}}",
			selected: true
		},
		description: {
			label: "Description",
			value: "{{report.feature.get('description')}}",
			selected: true
		},
		latitude: {
			label: "Latitude",
			value: "<span><span coordinate=\"[report.feature.get('latitude'), 'lat']\"></span>",
			selected: true
		},
		longitude: {
			label: "Longitude",
			value: "<span><span coordinate=\"[report.feature.get('longitude'), 'lon']\"></span>",
			selected: true
		}
	},
	displayFieldsTTT: {
		id: {
			label: "id",
			template: "{{feedback.id}}",
			selected: true
		},
		timestamp: {
			label: "Obs. Time",
			template: "<span position-timestamp=\"feedback.content.observationSource.datetime\"></span>",
			selected: true
		},
		obstype: {
			label: "Obs. Src. Type",
			template: "{{feedback.content.observationSource.type.value}}",
			selected: true
		},
		obsmethod: {
			label: "Obs. Src. Method",
			template: "{{feedback.content.observationSource.methodValue}}",
			selected: true
		},
		obsposition: {
			label: "Obs. Src. Position",
			template: "{{feedback.content.observationSource.position.value}}",
			selected: true
		},
		obscoordinate: {
			label: "Obs. Src. Coordinate",
			template: "<span coordinate=\"[feedback.content.observationSource.position.coordinates[0],'lon']\"></span><span coordinate=\"[feedback.content.observationSource.position.coordinates[1],'lat']\"></span>",
			selected: true
		},
		observer: {
			label: "Observer",
			template: "{{(feedback.content.observationSource.observer.active)?\"YES\":\"NO\"}}",
			selected: true
		},
		confimed: {
			label: "Confir",
			template: "{{(feedback.content.observationSource.confirmed)?\"YES\":\"NO\"}}",
			selected: true
		},
		type: {
			label: "Obs. Res. Type",
			template: "{{feedback.content.observationSource.type.value}}",
			selected: true
		},
		subtype: {
			label: "Obs. Res. Subtype",
			template: "{{feedback.content.observationSource.methodValue}}",
			selected: true
		},
		parameters: {
			label: "Obs. Res. Parameters",
			template: "{{(feedback.content.observationSource.parameters.active)?feedback.content.observationSource.parameters.vthick+\"; \"+feedback.content.observationSource.parameters.vwidth+\"; \"+feedback.content.observationSource.parameters.vvolume+\"; \"+feedback.content.observationSource.parameters.vlength+\"; \"+feedback.content.observationSource.parameters.varea+\"; \"+feedback.content.observationSource.parameters.vthickmethod+\"; \"+feedback.content.observationSource.parameters.vbaoac:\"NO\"}}",
			selected: true
		},
		attachments: {
			label: "Attach",
			template: "{{feedback.hasAttachments}}",
			selected: true
		},
		submited: {
			label: "Submited",
			template: "<span position-timestamp=\"feedback.timestamp\"></span>",
			selected: true
		}
	},
	displayFields: {
		id: {
			label: "id",
			template: "{{feedback.id}}",
			selected: true
		},
		timestamp: {
			label: "Obs. Time",
			template: "<span position-timestamp=\"feedback.content.observationSource.datetime\"></span>",
			selected: true
		},
		confimed: {
			label: "Confir",
			template: "{{(feedback.content.observationSource.confirmed)?\"YES\":\"NO\"}}",
			selected: true
		},
		type: {
			label: "Type",
			template: "{{feedback.content.observationSource.type.value}}",
			selected: true
		},
		subtype: {
			label: "Subtype",
			template: "{{feedback.content.observationSource.methodValue}}",
			selected: true
		},
		attachments: {
			label: "Attach",
			template: "{{feedback.hasAttachments}}",
			selected: true
		},
		submited: {
			label: "Submited",
			template: "<span position-timestamp=\"feedback.timestamp\"></span>",
			selected: true
		}
	},
	init() {
		config.scope.clear();
		const feature = reportService.scope.feature;
		if (feature)
			config.scope.observationSource.position.coordinates = ol.proj.toLonLat(feature.getGeometry().getCoordinates());
	},
	openReport(feedback) {
		if (!feedback)
			return;

		config.scope.observationSource.position.coordinates = feedback.content.observationSource.position.coordinates;
		config.scope.observationSource.type.value =  feedback.content.observationSource.type.value;
		config.scope.observationSource.functions.updateMeth();
		config.scope.observationSource.methodValue =  feedback.content.observationSource.methodValue;

		config.scope.observationSource.observer.active = feedback.content.observationSource.observer.active.valueOf();
		config.scope.observationSource.observer.country.value = feedback.content.observationSource.observer.country.value.valueOf();
		config.scope.observationSource.observer.observer = feedback.content.observationSource.observer.observer ? feedback.content.observationSource.observer.observer.valueOf() : "";
		config.scope.observationSource.observer.comments = feedback.content.observationSource.observer.comments ? feedback.content.observationSource.observer.comments.valueOf() : "";
		config.scope.observationSource.datetime = moment(feedback.content.observationSource.datetime);

		config.scope.attach.attachments = angular.extend([], feedback.content.attach.attachments);

	},
	update() {
		config.scope.clear();
		if (reportService.scope.feature === null)
			return;

		const feature = reportService.scope.feature,
			importRequest = seg.request.get("v1/feedback/fisheries/" + feature.getId());

		importRequest.then(result => {
			/*XXX if we remove last feedback the list doesn"t get updated
			if (typeof result.result === "undefined") {
				// service.logs.functions.clear();
				return;
			}
			*/
			const feedbacks = result.result,
				resultingFeedbacks = [];

			if(Array.isArray(feedbacks)){
				// service.logs.data = [];

				feedbacks.forEach(feedback => {
					resultingFeedbacks.push({
						id: feedback.feedbackId,
						content: feedback.jsonFeedback.feedback,
						timestamp: moment(feedback.timeStamp, "DD-MM-YYYY hh:mm:ss"),
						feature: {
							id: feedback.objId,
							type: feedback.type
						},
						getObsSrcLat: feedback.jsonFeedback.feedback.observationSource.position.coordinates[1].valueOf(),
						getObsSrcLon: feedback.jsonFeedback.feedback.observationSource.position.coordinates[0].valueOf(),
						hasAttachments: (function() {
							if (feedback.jsonFeedback.feedback.attach.attachments.length !== 0)
								return "YES";
							return "NO";
						})()
					});
				});
			}

			reportService.setLogs(resultingFeedbacks);
		}, e => seg.log.error("ERROR_REPORT_FISHERY_EVENTS_UPDATE", e.error + ": " + e.result));
	},
	getSelectedFeature() {
		const feature = seg.selection.getSelected("events");
		if (feature[0]){
			return feature[0];
		}
		return null;
	},
	submit() {
		config.scope.submit();
	},
	hasLog: true
};

module.exports = config;
