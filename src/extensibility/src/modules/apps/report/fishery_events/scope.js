const FLAG_STATES = require("../../../../common/flag_states_obj.json");

const scope = {};

scope.logs = {
	data: [],
	selected: null,
	functions: {
		edit() {
			scope.openReport(scope.logs.selected);
			scope.reportOpen = false;
			scope.reportOpen = true;
		},
		remove() {
			const modal = seg.modal.openModal("Report", "Deleting Report...", {class: "info"});

			seg.request.delete("v1/feedback/fishery/" + scope.logs.selected.id).then(() => {
				scope.import();
				modal.setContent("Delete successful");
				seg.modal.closeTimeout(modal.title);
				scope.logs.selected = null;
			}, e => seg.log.error("ERROR_REPORT_FISHERY_EVENTS_REMOVE", e.error + ": " + e.result));
		},
		openInTTT() {
			if (scope.logs.data.length > 0) {
				const data = scope.logs.selected ? [scope.logs.selected] : scope.logs.data;
				seg.ui.ttt.tables.open({
					label: (data.length === 1) ? "Report" : "All Reports",
					data: data,
					itemAs: "feedback",
					fields: scope.logs.displayFieldsTTT
				});
			}
		},
		update() {
			scope.import();
		},
		filter() {},
		setAsSelected(selected) {
			if (!selected.length)
				scope.logs.selected = null;
			else
				scope.logs.selected = selected[0].item;
		},
		disabled() {
			if ((typeof scope.logs.selected === undefined) || (scope.logs.selected === null)) return "disabled";
		},
		clear() {
			scope.logs.data = [];
			scope.logs.selected = null;
		}
	},
	displayFields: {
		id: {
			label: "id",
			template: "{{feedback.id}}",
			selected: true
		},
		timestamp: {
			label: "Obs. Time",
			template: "<span position-timestamp=\"feedback.content.observationSource.datetime\"></span>",
			selected: true
		},
		attachments: {
			label: "Attach",
			template: "{{feedback.hasAttachments}}",
			selected: true
		},
		submited: {
			label: "Submited",
			template: "<span position-timestamp=\"feedback.timestamp\"></span>",
			selected: true
		}
	},
	displayFieldsTTT: {
		id: {
			label: "id",
			template: "{{feedback.id}}",
			selected: true
		},
		timestamp: {
			label: "Obs. Time",
			template: "<span position-timestamp=\"feedback.content.observationSource.datetime\"></span>",
			selected: true
		},
		obstype: {
			label: "Obs. Src. Type",
			template: "{{feedback.content.observationSource.type.value}}",
			selected: true
		},
		obsmethod: {
			label: "Obs. Src. Method",
			template: "{{feedback.content.observationSource.methodValue}}",
			selected: true
		},
		obsposition: {
			label: "Obs. Src. Position",
			template: "{{feedback.content.observationSource.position.value}}",
			selected: true
		},
		obscoordinate: {
			label: "Obs. Src. Coordinate",
			template: "<span coordinate=\"[feedback.content.observationSource.position.coordinates[0],'lon']\"></span><span coordinate=\"[feedback.content.observationSource.position.coordinates[1],'lat']\"></span>",
			selected: true
		},
		observer: {
			label: "Observer",
			template: "{{(feedback.content.observationSource.observer.active)?\"YES\":\"NO\"}}",
			selected: true
		},
		attachments: {
			label: "Attach",
			template: "{{feedback.hasAttachments}}",
			selected: true
		},
		submited: {
			label: "Submited",
			template: "<span position-timestamp=\"feedback.timestamp\"></span>",
			selected: true
		}
	}
};
scope.observationSource = {
	functions: {
		updateMeth() {
			switch (scope.observationSource.type.value) {
				case "Sightings":
					scope.observationSource.method = ["Aircraft", "Helicopter", "RPAS", "Patrol Vessel", "Merchant Vessel", "SAT, Sar image", "SAT, Optical image", "VMS", "AIS", "LRIT", "Other Sources"];
					break;
				case "Inspection with infrigements":
					scope.observationSource.method = ["At Port", "At Sea"];
					break;
				case "Inspection with no infrigements":
					scope.observationSource.method = ["At Port", "At Sea"];
					break;
			}
			scope.observationSource.methodValue = "";
		},
		fetchEOImageID() {
			const selectedFootprints = seg.selection.getSelected("eo_sar_imagery");
			if (selectedFootprints.length === 1) {
				scope.observationSource.position.eoImageId = selectedFootprints[0].getId();
				scope.observationSource.position.coordinates = [selectedFootprints[0].get("lon"), selectedFootprints[0].get("lat")];
			}
		},
		isValid() {
			const self = scope.observationSource;
			if (self.type.value !== "" && self.methodValue !== "" && ((self.position.value === "Coordinates" && self.position.coordinates !== undefined) || (self.position.value === "Image" && self.position.eoImageId !== "") || (self.position.value === "AOI" && self.position.areas.length !== 0)) || (self.observer.active && (self.observer.country.value !== "" && self.observer.observer === null))) return true;
			return false;
		},
		clear() {
			scope.observationSource.type.value = "";
			scope.observationSource.methodValue = "";
			scope.observationSource.position.value = "";
			scope.observationSource.position.eoImageId = "";
			scope.observationSource.position.coordinates = [0, 0];
			scope.observationSource.position.areas = [];
			scope.observationSource.observer.active = false;
			scope.observationSource.observer.country.value = "";
			scope.observationSource.observer.observer = "";
			scope.observationSource.observer.comments = "";
			scope.observationSource.datetime = moment().toISOString();
		}
	},
	type: {
		value: "",
		options: ["Sightings", "Inspection with infrigements", "Inspection with no infrigements"]
	},
	methodValue: "",
	method: [],
	position: {
		value: "",
		options: ["Coordinates", "Image", "AOI"],
		coordinates: [0, 0],
		eoImageId: "",
		areas: []
	},
	observer: {
		active: false,
		country: {
			value: "",
			options: FLAG_STATES
		},
		observer: null,
		comments: null
	},
	datetime: moment().toISOString()
};

scope.attach = {
	attachments: [],
	attach(file) {
		const feature = seg.selection.getSelected("oil_spills").length ? seg.selection.getSelected("oil_spills")[0] : null;

		scope.attach.attachments.push({
			file: file,
			name: file.name,
			path: "/report/" + "S_" + feature.getId() + "/" + file.name
		});
	},
	functions: {
		clear() {
			scope.attach.attachments = [];
		},
		removeAttachment(attachment) {
			const index = scope.attach.attachments.indexOf(attachment);
			scope.attach.attachments.splice(index, 1);
		},
		downloadAttachment(attachment) {
			if (!attachment.file) {
				const modal = seg.modal.openModal("File Download", "Downloading " + attachment.name + "...", {class: "info"});
				seg.request.download("v1/file/download", {
					params: {
						path: attachment.path
					}
				}, attachment.name).then(() => {
					modal.setContent("Download successful");
					seg.modal.closeTimeout(modal.title);
				}, e => seg.log.error("ERROR_REPORT_FISHERY_EVENTS_DOWNLOAD_ATTACHMENT", e.error + ": " + e.result));
			}
		}
	}
};

scope.submit = async() => {
	//Wrong
	let feature = seg.selection.getSelected("events").length ? seg.selection.getSelected("events")[0] : null;

	const requests = [];

	if (!feature){
		feature = await createNewFisheryEvent(scope.new.name, scope.new.description, scope.new.coordinates[1], scope.new.coordinates[0]);
		seg.layer.get("events").getSource().addFeature(feature);
	}

	const modal = seg.modal.openModal("Feedback", "Submitting...", {class: "info"});
	//build structure to send
	const toSubmit = {
		observationSource: {
			type: {
				value: scope.observationSource.type.value
			},
			methodValue: scope.observationSource.methodValue,
			position: {
				value: scope.observationSource.position.value,
				areas: (new ol.format.GeoJSON()).writeFeatures(scope.observationSource.position.areas, {
					dataProjection: "EPSG:4326",
					featureProjection: seg.map.getProjection()
				}),
				coordinates: scope.observationSource.position.coordinates,
				eoImageId: scope.observationSource.position.eoImageId
			},
			observer: {
				active: scope.observationSource.observer.active,
				country: {
					value: scope.observationSource.observer.country.value
				},
				observer: scope.observationSource.observer.observer,
				comments: scope.observationSource.observer.comments
			},
			datetime: scope.observationSource.datetime
		},
		attach: {
			attachments: scope.attach.attachments
		}
	};

	requests.push(seg.request.put("v1/feedback/fisheries/" + feature.getId(), {
		data: {
			feedback: toSubmit
		}
	}));

	scope.attach.attachments.forEach(attachment => {
		if (attachment.file) {
			requests.push(seg.request.upload({
				file: attachment.file,
				pathToSave: attachment.path
			}));
		}
	});

	seg.promise.all(requests).then(() => {
		modal.setContent("Feedback Successfully Submitted");
		seg.modal.closeTimeout(modal.title);
		scope.import();
	}, () => {
		modal.setContent("Feedback Submission Failed");
		seg.modal.closeTimeout(modal.title);
	}, e => seg.log.error("ERROR_REPORT_FISHERY_EVENTS_SUBMIT", e.error + ": " + e.result));
};

scope.import = () => {
	scope.clear();

	let feature = seg.selection.getSelected("events").length ? seg.selection.getSelected("events")[0] : null;

	if (feature)
		feature = (feature.get("type") === "fisheries")? feature: null;
	else return;

	if (feature === null) return;

	seg.request.get("v1/feedback/fisheries/" + feature.getId()).then(result => {
		if (typeof result.result === "undefined") {
			scope.logs.functions.clear();
			return;
		}

		const feedbacks = result.result;

		scope.logs.data = feedbacks.map(feedback => {
			return {
				id: feedback.feedbackId,
				content: feedback.jsonFeedback.feedback,
				timestamp: moment(feedback.timeStamp, "DD-MM-YYYY hh:mm:ss"),
				feature: {
					id: feedback.objId,
					type: feedback.type
				},
				getObsSrcLat: feedback.jsonFeedback.feedback.observationSource.position.coordinates[1].valueOf(),
				getObsSrcLon: feedback.jsonFeedback.feedback.observationSource.position.coordinates[0].valueOf(),
				hasAttachments: feedback.jsonFeedback.feedback.attach.attachments.length !== 0?"YES":"NO"
			};
		});

		if (scope.logs.data.length > 0) {
			scope.reportOpen = true;
			scope.logOpen = undefined;
		}
	}, e => seg.log.error("ERROR_REPORT_FISHERY_EVENTS_IMPORT", e.error + ": " + e.result));
};

scope.clear = () => {
	scope.new.name = null;
	scope.new.description = null;
	scope.new.coordinates = seg.map.getCenter();
	scope.observationSource.functions.clear();
	scope.attach.functions.clear();
};

scope.new = {
	name: null,
	description: null,
	coordinates: seg.map.getCenter()
};

const createNewFisheryEvent = (name, description, latitude, longitude) => seg.request.put("v1/events?type=fisheries&lat="+latitude+"&lon="+longitude+"&name="+name+"&description="+description).then(res => {
	const feature = new ol.Feature({
		id: res.result.id,
		name: res.result.name,
		description: res.result.description,
		geometry:new ol.geom.Point([res.result.lon,res.result.lat]).transform("EPSG:4326",seg.map.getProjection()),
		latitude: res.result.lat,
		longitude: res.result.lon
	});

	feature.setId(res.result.id);

	return feature;
}, e => seg.log.error("ERROR_REPORT_FISHERY_EVENTS_CREATE_NEW_FISHERY_EVENT", e.error + ": " + e.result));

module.exports = scope;
