const FLAG_STATES = require("../../../../common/flag_states.json"),
	{PSC_SHIP_TYPES} = require("../../../vessels/api.js"),
	PROCESSED_AREA_BUFFER = 2;

const scope = {};
scope.edit = true;

scope.submit = async () => {
	let data;
	const modal = seg.modal.openModal("ABM Alert", scope.properties.id ? "Updating ABM surveillance..." : "Inserting new ABM surveillance...", {class: "info"});

	try {
		// On Vessel list mode, we need to query imdate for the id of each vessel based on type and value selected by the user
		//build structure to send
		data = {
			name: scope.properties.name,
			distributor: scope.properties.emailNotification ? "WupAndEmailAlertDistributor" : "WupAlertDistributor",
			description: scope.properties.description,
			startTime: scope.properties.start.toISOString().split(".")[0]+"Z",
			alertPolicy: scope.properties.alertPolicy.value,
			status: "pending"
		};

		if(scope.properties.id)
			data.id = scope.properties.id;

		if(scope.properties.stopActive)
			data.stopTime = scope.properties.stop.toISOString().split(".")[0]+"Z";

		if(scope.properties.distributionList.active && scope.properties.distributionList.value)
			data.listId = scope.properties.distributionList.value;
		else
			delete data.listId;

		if(scope.properties.activateSurveillance)
			data.status = "running";

		if(scope.vessels.active){
			const voiParams = {};

			if(scope.vessels.vesselList.partialResults.length){
				// Checking inNotIn selector to determine negative or positive query
				voiParams[scope.vessels.vesselList.inNotIn === "IN" ? "EMSAId" : "-EMSAId"] = scope.vessels.vesselList.partialResults.reduce((ids, vessel) => {
					if (vessel.selected)
						ids.push(vessel.id);

					return ids;
				}, []);
			}

			if(scope.vessels.mode === "Filter") {
				// Checking inNotIn selector to determine negative or positive query
				voiParams[scope.vessels.filter.flagsInNotIn === "IN" ? "FlagState" : "-FlagState"] = scope.vessels.filter.FlagState;
				voiParams[scope.vessels.filter.shipTypesInNotIn === "IN" ? "ShipType" : "-ShipType"] = scope.vessels.filter.ShipType;
				voiParams["STRICT_MODE"] = scope.vessels.filter.exactMatch;
			}

			data.voiParams = JSON.stringify(voiParams);
		}

		if(scope.aois.selectedAreas){
			data.aoiAreas = "";
			if(scope.aois.selectedAreas.some(selectedArea => selectedArea.feature))
				scope.aois.selectedAreas = scope.aois.selectedAreas.map(selectedArea => selectedArea.feature);

			const selectedArea =scopeaois.selectedAreas[0];
			
			if(selectedArea.getGeometry().get("type") === "rectangle") {
				const bbox = seg.geometry.rectangleToExtent(scope.aois.selectedAreas)[0];
				data.aoiAreas += ("R[" + bbox.minx + "," + bbox.maxy + " " + bbox.maxx + "," + bbox.miny + "]");
			} else if(selectedArea.getGeometry().get("type") === "polygon" || selectedArea.getGeometry().get("type") === "circle") {
				data.aoiAreas += "P[";
				const vertices = selectedArea.getGeometry().getCoordinates()[0].map(coord => ol.proj.transform(coord, seg.map.getProjection(), "EPSG:4326"));


				data.aoiAreas += vertices[0][0] + "," + vertices[0][1];
				for (let j=1; j < vertices.length; ++j) {
					data.aoiAreas += " " + vertices[j][0] + "," + vertices[j][1];
				}
				data.aoiAreas += "]";
			}
		}

		if(scope.template.templateFields.every(({value}) => value && value !== "")){
			data.templateId = scope.template.value;
			scope.template.templateFields.forEach(({value, name, unit}, index) => {
				const pName = "pName" + (index + 1), pValue = "pValue" + (index + 1);
				data[pName] = name;

				if(value instanceof moment)
					if(unit === "ISO8601 format (HH:mm:ss.SSS)")
						value = value.format("HH:mm:ss");
					else
						value = value.toISOString().split(".")[0]+"Z";

				// For a few templates we have an attribute called processedArea, that should behave like AOI
				if(name === "processedArea" || name === "pastProcessedArea"){
					let vertices;
					if(value.some(selectedArea => selectedArea.feature))
						value = value.map(selectedArea => selectedArea.feature);

					const selectedArea = value[0];

					if(selectedArea.getGeometry().get("type") === "rectangle") {
						const extent = seg.geometry.rectangleToExtent(selectedArea)[0];

						// processedArea should have this format:
						// "-9.109566828039751,38.78316172516389 -9.109566828039751,38.78827568150879 -9.01809288405113,38.78827568150879 -9.01809288405113,38.78316172516389"
						vertices = ol.geom.Polygon.fromExtent([extent.minx, extent.maxy, extent.maxx, extent.miny]).getCoordinates()[0];
						vertices.splice(-1, 1);

						// and  aoiAreas should have this:
						// R[-9.109566828039751,38.78316172516389 -9.109566828039751,38.78827568150879]
						const bbox = seg.geometry.rectangleToExtent(selectedArea, {add_x: PROCESSED_AREA_BUFFER, add_y: PROCESSED_AREA_BUFFER})[0];
						data.aoiAreas += ("R[" + bbox.minx + "," + bbox.maxy + " " + bbox.maxx + "," + bbox.miny + "]");
					} else if(selectedArea.getGeometry().get("type") === "polygon" || selectedArea.getGeometry().get("type") === "circle") {
						vertices = selectedArea.getGeometry().getCoordinates()[0].map(coord => ol.proj.transform(coord, seg.map.getProjection(), "EPSG:4326"));

						// if area query is a polygon, we should use
						// P[-9.109566828039751,38.78316172516389 -9.109566828039751,38.78827568150879]
						data.aoiAreas += "P[";
						data.aoiAreas += vertices[0][0] + "," + vertices[0][1];
						for (let j = 1; j < vertices.length; ++j) {
							data.aoiAreas += " " + vertices[j][0] + "," + vertices[j][1];
						}
						data.aoiAreas += "]";
					}

					data[pValue] = "";
					for (let j = 0; j < vertices.length; ++j) {
						data[pValue] += vertices[j][0] + "," + vertices[j][1] + " ";
					}

					data[pValue] = data[pValue].trim();
				} else {
					data[pValue] = value;
				}
			});
		}
	} catch(e){
		modal.setContent("Error " + (scope.properties.id ? "inserting" : "updating") + " ABM surveillance. " + e);
		modal.setClass("error");
		seg.modal.closeTimeout(modal.title);
	}

	return seg.request.post(scope.properties.id ? "v1/abm/surveillance/instance?op=update" : "v1/abm/surveillance/instance?op=create", {
		data
	}).then(() => {
		modal.setContent(scope.properties.id ? "ABM surveillance updated successfully" : "ABM surveillance created successfully");
		seg.modal.closeTimeout(modal.title);
		return;
	}, e => {
		modal.setContent("Error " + (scope.properties.id ? "inserting" : "updating") + " ABM surveillance. " + e);
		modal.setClass("error");
		seg.modal.closeTimeout(modal.title);
		seg.log.error("ERROR_ABM_ALERTS_SUBMIT", e.error + ": " + e.result);
		return Promise.reject();
	});
};

scope.isBeingEdited = () => scope.edit;

scope.isNoAOISelected = () => {
	return scope.template.area_to_area_model.length === 0;
};

scope.speedUnits = () => seg.preferences.workspace.get("speedUnits");

scope.distanceUnits = () => seg.preferences.workspace.get("distanceUnits");

scope.timeUnits = () => seg.preferences.workspace.get("timeUnits");

scope.clear = () => {
	scope.edit = true;
	scope.properties = Object.assign({}, scope.properties, {
		id: null,
		name: null,
		description: null,
		start: moment(),
		stop: moment().clone().add("1", "week"),
		startActive: false,
		stopActive: false,
		emailNotification: null,
		activateSurveillance: null,
		alertPolicy: {
			alertPolicyOptions: [
				{label: "At start", value: "one_shot_at_start"},
				{label: "At end", value: "one_shot_at_end"},
				{label: "At start and end", value: "at_start_at_end"},
				{label: "At each occurrence", value: "at_each_occurrence"}
			],
			value: "one_shot_at_start"
		}
	});

	scope.properties.distributionList = Object.assign({}, scope.properties.distributionList, {
		value: null,
		users: [],
		active: false,
		functions: {
			changeDistributionList: () => {
				const found = scope.properties.distributionList.distributionListOptions.find(item => item.id === scope.properties.distributionList.value);
				if(found)
					scope.properties.distributionList.users = found.recipients.join("\n");
			}
		}
	});

	if(!scope.properties.distributionList.distributionListOptions)
		scope.properties.distributionList.distributionListOptions = [];

	// LUIS
	// AOIs may be drawn by user but also can be CGD areas that come from scope
	// However it's not decided yet which CGD areas will be shown and how.
	// once WHICH is defined, HOW is up to you ;)

	if(!scope.aois || !scope.aois.selectedAreas || !scope.aois.selectedAreas.length)
		scope.aois = Object.assign({}, scope.aois, {
			selectedAreas: [],
			openInSelectionMode: {
				selectionMode: "browse",
				defaultValues: null
			}
		});

	scope.vessels = {
		functions: {
			removeRow(row){
				scope.vessels.vesselList.queryAttributes = scope.vessels.vesselList.queryAttributes.filter(query => query !== row);
			},
			addRow(){
				scope.vessels.vesselList.queryAttributes.push({type: "mmsi", value: ""});
			},
			changeCallbackVesselType: filter => {
				scope.vessels.vesselList.shipTypesOptions = scope.vessels.vesselList._shipTypesOptions.filter(type => type.name.toLowerCase().includes(filter.toLowerCase()));
			},
			changeCallbackFlagState: filter => {
				scope.vessels.vesselList.flagsOptions = scope.vessels.vesselList._flagsOptions.filter(flag => flag.name.toLowerCase().includes(filter.toLowerCase()));
			},
			searchVessels(){
				// TODO: create cache of these searches
				const modal = seg.modal.openModal("ABM Alert Search Vessel", "Searching vessels...", {class: "info"}),
					vesselList = [],
					vesselListRequests = scope.vessels.vesselList.queryAttributes.map(query => seg.request.get("v1/ship/imdateID", {
						params: {
							[query.type]: query.value
						}
					}));

				seg.promise.all(vesselListRequests).then(results => {
					results.forEach(res => res.result.forEach(vessel => {
						vesselList.push(vessel);
					}));

					modal.setContent(vesselList.length + " vessels found.");
					seg.modal.closeTimeout(modal.title);

					scope.vessels.vesselList.partialResults = vesselList;
				});
			},
			vesselGetLabel(vessel){
				let label = "";
				if(vessel.identity)
					vessel = vessel.identity;

				if(vessel.shipName)
					label += vessel.shipName + " ";
				if (vessel.mmsi)
					label += "(MMSI: " + vessel.mmsi + ")";
				if (vessel.imo && !vessel.mmsi)
					label += "(IMO: " + vessel.imo + ")";
				if(vessel.id && !vessel.imo && !vessel.mmsi)
					label += "(EMSA ID: " + vessel.id + ")";

				return label;
			}
		},
		active: false,
		mode: "Vessel list",
		modeOptions: ["Vessel list", "Filter"],
		inNotInOptions: ["IN", "NOT IN"],
		vesselList: {
			loading: false,
			flagsOptions: Object.entries(FLAG_STATES).map(([value, name]) => ({name: "(" + value + ") " + name, value})),
			shipTypesOptions: Object.entries(PSC_SHIP_TYPES).map(([name, value]) => ({name, value})),
			_flagsOptions: Object.entries(FLAG_STATES).map(([value, name]) => ({name: "(" + value + ") " + name, value})),
			_shipTypesOptions: Object.entries(PSC_SHIP_TYPES).map(([name, value]) => ({name, value})),
			inNotIn: "IN",
			queryAttributeOptions: [
				{label: "MMSI", value: "mmsi"},
				{label: "IMO", value: "imo"},
				{label: "IR", value: "ir"},
				{label: "Vessel Name", value: "shipName"},
				{label: "Call Sign", value: "callSign"},
				{label: "Flag", value: "flagState"},
				{label: "Vessel Type", value: "shipType"}
			],
			queryAttributes: [{type: "mmsi", value: null}],
			partialResults : []
		},
		filter: {
			exactMatch: false,
			flagsInNotIn: "IN",
			flagsOptions: Object.entries(FLAG_STATES).map(([value, name]) => ({name: "(" + value + ") " + name, value, selected: false})),
			FlagState: [],
			shipTypesInNotIn: "IN",
			shipTypesOptions: Object.entries(PSC_SHIP_TYPES).map(([name, value]) => ({name, value, selected: false})),
			ShipType: []
		}
	};

	scope.template = Object.assign({}, scope.template, {
		area_to_area_model: [],
		functions: {
			changeType(){
				const found = scope.template.templateOptions.find(opt => opt.id === scope.template.value);
				if(found){
					// Templates that has Area in their names must show AOI section because they have their own AOI section
					scope.aois.show = !found.name.includes("Area");
					scope.template.templateFields = scope.template.templateContent[found.name] || [];
					scope.template.name = found.name;
				}
			},
			addSelectedAreaToValue(templateFields, index) {
				if(!templateFields[index].value || templateFields[index].value && !templateFields[index].value.length)
					templateFields[index].value = seg.selection.getSelected("aoi");
				else
					templateFields[index].value = null;
			},
			goToAOI(AOIorFavourite){
				if(AOIorFavourite[0].feature)
					AOIorFavourite = AOIorFavourite[0].feature;
				seg.selection.selectFeatureAndCenter(AOIorFavourite);
			}
		}
	});

	if(!scope.template.templateOptions)
		scope.template.templateOptions = [];

	if(!scope.template.templateFields)
		scope.template.templateFields = [];

	if(!scope.template.templateContent)
		scope.template.templateContent = {};

	scope.template.functions.changeType();
};

scope.isValid = () => {
	return scope.properties.name && scope.properties.name !== "" && // Alert must have a name
		scope.properties.start < scope.properties.stop && // start time must be before than stop time
		scope.template.value && // A template must be selected
		scope.properties.alertPolicy.value && // An alert policy must be selected
		( // regarding AOI
			scope.aois.selectedAreas.length === 1 || // Must have only 1 area selected
			scope.template.templateFields.find(field => { // Unless it has a processedArea or pastProcessedArea, which area also AOIs fields, and they are validated below
				return field.name === "processedArea" || field.name === "pastProcessedArea";
			})
		) &&
		( // regarding specific templates
			scope.template.value === 40000 || // template value 40000 does not need templateFields
			scope.template.value === 220000 || // template value 220000 does not need templateFields
			scope.template.templateFields.every(field => {
				// if it is not one of the cases that has no templateFields, all of them must have a vlue
				if(field.value){
					if(!Array.isArray(field.value)) return true;
					else if(Array.isArray(field.value) && field.value.length > 0) return true;
					else return false;
				} else
					return false;
			})
		);
};

module.exports = scope;
