let timeoutUpdate = null;
const REFRESH_TIME = 30; // 30 seconds
const reportService = require("../service.js");
const config = {
	title: "ABM management",
	class: "abm-form",
	logTitle: "ABMs",
	paginateGrid: true,
	reportTitle: "Add ABM",
	templateUrl: "extensibility/modules/apps/report/abm_alerts/template.html",
	scope: require("./scope.js"),
	displayFields: {
		name: {
			label: "Name",
			template: "{{feedback.name}}",
			visible: true
		},
		type: {
			label: "Type",
			template: "{{feedback.templateName}}",
			visible: true
		},
		status: {
			label: "Status",
			template: "{{feedback.status}}",
			visible: true
		},
		userRef: {
			label: "ABM Owner",
			template: "{{feedback.userRef}}",
			visible: false
		}
	},
	disableOpenInTTT: true,
	customLogFunctions: [
		{
			title: "Clone",
			icon: "new-window",
			do: () => {
				reportService.disableReportTab = false;
				config.scope.clear();
				config.openReport(reportService.scope.logs.selected, true);
				config.toggleEditing(true);

				reportService.scope.logs.selected = null;

				reportService.scope.logOpen = false;
				reportService.scope.reportOpen = true;
			},
			disabled: () => {
				if ((typeof reportService.scope.logs.selected === "undefined") || (reportService.scope.logs.selected === null) || ((reportService.scope.logs.data === []) || (reportService.scope.logs.data === null)))
					return "disabled";
			}
		},
		{
			title: "Start",
			icon: "play2",
			do: () => {
				const modal = seg.modal.openModal("ABM_surveillance", "Starting ABM surveillance...", {class: "info"}),
					previousStatus = reportService.scope.logs.selected.status;

				reportService.scope.logs.selected.status = "running";

				seg.request.post("v1/abm/surveillance/instance?op=start", {
					data: {
						id: reportService.scope.logs.selected.id
					}
				}).then(() => {
					modal.setContent("ABM surveillance started successfully");
					seg.modal.closeTimeout(modal.title);
					config.update();
				}, e => {
					modal.setContent("Failed to start ABM surveillance.");
					seg.modal.closeTimeout(modal.title);
					seg.log.error("ERROR_ABM_STOP_SURVEILLANCE", e.error + ": " + e.result);
					reportService.scope.logs.selected.status = previousStatus;
				});
			},
			disabled: () => {
				if (
					typeof reportService.scope.logs.selected === "undefined" ||
					reportService.scope.logs.selected === null ||
					reportService.scope.logs.data === [] ||
					reportService.scope.logs.data === null ||
					reportService.scope.logs.selected.status === "running"
				)
					return "disabled";
			},
			style: "height: 15px;"
		},
		{
			title: "Stop",
			icon: "stop",
			do: () => {
				const modal = seg.modal.openModal("ABM_surveillance", "Stopping ABM surveillance...", {class: "info"}),
					previousStatus = reportService.scope.logs.selected.status;

				reportService.scope.logs.selected.status = "stopped";

				seg.request.post("v1/abm/surveillance/instance?op=stop", {
					data: {
						id: reportService.scope.logs.selected.id
					}
				}).then(() => {
					modal.setContent("ABM surveillance stopped successfully");
					seg.modal.closeTimeout(modal.title);
					config.update();
				}, e => {
					modal.setContent("Failed to stop ABM surveillance.");
					seg.modal.closeTimeout(modal.title);
					seg.log.error("ERROR_ABM_STOP_SURVEILLANCE", e.error + ": " + e.result);
					reportService.scope.logs.selected.status = previousStatus;
				});
			},
			disabled: () => {
				if (
					typeof reportService.scope.logs.selected === "undefined" ||
					reportService.scope.logs.selected === null ||
					reportService.scope.logs.data === [] ||
					reportService.scope.logs.data === null ||
					reportService.scope.logs.selected.status === "stopped" ||
					reportService.scope.logs.selected.status === "terminated" ||
					reportService.scope.logs.selected.status === "blocked"
				)
					return "disabled";
			},
			style: "height: 15px;"
		},
		{
			title: "Refresh",
			icon: "refresh",
			do: () => {
				config.update();
			},
			disabled: () => null,
			style: "height: 18px;"
		}
	],
	originField: "",
	init() {
		config.scope.clear();
		config.scope.template.templateOptions = [];
		config.scope.template.templateFields = [];
		config.scope.template.templateContent = {};

		// LUIS
		// This request is to populate data for the distribution list
		// isOperator parameter is still a mistery but probably will have to do with some role from the user
		// Probably nothing to do here, but...
		seg.request.get("v1/abm/dlm/search"/*, {params: {isOperator: true}}*/).then(res => {
			if(res.result && res.result[0]){
				config.scope.properties.distributionList.distributionListOptions = res.result;
				config.scope.properties.distributionList.value = res.result[0].id;
				config.scope.properties.distributionList.users = res.result[0].recipients.join("\n");
			}
		}, e => seg.log.error("ERROR_REPORT_ABM_ALERT_DLM", e.error + ": " + e.result));

		const updateTemplates = (result) => {
			result = result.sort((a, b) => {if (a.name > b.name) {return 1;} if (a.name < b.name) {return -1;} return 0;});

			// Mapping template options and params
			config.scope.template.templateOptions = result
				.map(({id, name, description, saParamsDescription}) => ({id, name, description, saParamsDescription}));

			config.scope.template.templateOptions.forEach(({name, saParamsDescription}) => {
				if(saParamsDescription && typeof saParamsDescription[0] !== "string")
					config.scope.template.templateContent[name] = saParamsDescription.map(param => {
						param.labelValue = param.value;
						param.labelUnits = "";
						param.changeValue = () => {};
						param.unitChanged = () => {};

						// Marteling of default values
						switch(name){
							case "Anchorage":
								if(param.name === "immobilityDistanceThresholdInMeters")
									param.value = 200;
								if(param.name === "minAnomalyDurationInSeconds")
									param.value = 1400;
								break;
							case "AnchorageOutsidePort":
								if(param.name === "immobilityDistanceThresholdInMeters")
									param.value = 300;
								if(param.name === "minAnomalyDurationInSeconds")
									param.value = 1400;
								break;
							case "AtPortAtSea": break;
							case "AtSeaEncounter":
								//Max dist in meters
								if(param.name === "maxDistanceThresholdInMeters")
									param.value = 500;
								if(param.name === "maxSpeedThresholdInMetersPerSecond")
									param.value = 20;
								if(param.name === "minDurationInSeconds")
									param.value = 600;
								break;
							case "ChangeInETA": break;
							case "ChangeOfPort": break;
							case "DesignatedPortOfArrival": break;
							case "DesignatedPortOfDeparture": break;
							case "DistanceToArea": break;
							case "DistanceToShore": break;
							case "Drifting":
								if(param.name === "minimumDriftingDuration")
									param.value = 600;
								if(param.name === "maximumSpeed")
									param.value = 3;
								if(param.name === "minimumHeadingDeviation")
									param.value = 15;
								break;
							case "FromAreaToArea": break;
							case "HeadingOffShore": break;
							case "HeadingToShore": break;
							case "InArea": break;
							case "NotReporting":
								if(param.name === "reportingPeriodInSeconds")
									param.value = 360;
								if(param.name === "observationPeriodInSeconds")
									param.value = 900;
								break;
							case "OutArea": break;
							case "OverReporting": break;
							case "RouteDeviation": break;
							case "SpeedAnomalyOverPeriod": break;
							case "SpeedAnomalyOverPeriodOutsidePort": break;
							case "SpoofingPositionInError":
								if(param.name === "maximumSpeedInMetersPerSecond")
									param.value = 30;
								if(param.name === "maximumTimeBetweenTwoPositionsInSeconds")
									param.value = 1800;
								break;
							case "SuddenChangeOfHeading":
								if(param.name === "maxDifferenceThresholdInDegrees")
									param.value = 15;
								if(param.name === "maxTimeIntervalInSeconds")
									param.value = 1800;
								break;
							case "SuddenChangeOfSpeed":
								if(param.name === "maxDifferenceThresholdInMetersPerSecond")
									param.value = 3;
								if(param.name === "percentageThreshold")
									param.value = 60;
								if(param.name === "maxTimeIntervalInSeconds")
									param.value = 1800;
								break;
							case "SuddenChangeOfSpeedOutsidePort":
								if(param.name === "maxDifferenceThresholdInMetersPerSecond")
									param.value = 3;
								if(param.name === "percentageThreshold")
									param.value = 60;
								if(param.name === "maxTimeIntervalInSeconds")
									param.value = 1800;
								break;
							case "TimeAndPeriodOfDay":
								if(param.name === "startPeriod")
									param.value = moment();
								if(param.name === "endPeriod")
									param.value = moment().add(12, "hour");
								break;
							case "TrafficSeparationScheme": break;
							case "UnderReporting": break;
						}

						// adding unit template to match user config
						switch(param.unit){
							case "meters":
								param.labelUnits = seg.preferences.workspace.get("distanceUnits");
								param.label += " ({{getDistanceUnits()}})";
								config.distanceChange(param);
								break;
							case "seconds":
								param.labelUnits = seg.preferences.workspace.get("timeUnits");
								param.label += " ({{getTimeUnits()}})";
								config.timeChange(param);
								break;
							case "m/s":
							case "meter per second":
							case "meters per second":
							case "meters per seconds":
								param.labelUnits = seg.preferences.workspace.get("speedUnits");
								param.label += " ({{getSpeedUnits()}})";
								config.speedChange(param);
								break;
							case "percent":
								param.label += " (%)";
								break;
							case "degree":
							case "degrees":
								param.label += " (°)";
								break;
						}

						return param;
					});
				else
					config.scope.template.templateContent[name] = [];
			});

			config.scope.template.functions.changeType();
		};

		seg.request.get("v1/abm/surveillance/template", {params: {op: "find"}}).then(res => updateTemplates(res.result), e => {
			seg.log.error("ERROR_REPORT_ABM_ALERT_TEMPLATES", e.error + ": " + e.result);
			// Fallback to templates json
			seg.request.get("extensibility/modules/apps/report/abm_alerts/templates_fallback.json").then(res => updateTemplates(res.result));
		});
	},
	source: seg.layer.get("alerts").getSource(),
	toggleEditing(val) {
		config.scope.edit = val || !config.scope.edit;
	},
	disableEditButton() {
		return false;
	},
	openReport(alert, clone = false) {
		if (!alert)
			return;

		const aoiSource = seg.layer.get("aoi").getSource();
		aoiSource.getFeatures().forEach(aoi => {
			if(aoi.get("aoi_alert"))
				aoiSource.removeFeature(aoi);
		});

		config.scope.properties = Object.assign(config.scope.properties, {
			name: !clone ? alert.name : alert.name + "_cloned",
			description: alert.description,
			start: (alert.startTime || alert.actualStartTime) ? moment(alert.startTime || alert.actualStartTime) : moment(),
			stop: (alert.stopTime || alert.actualStopTime) ? moment(alert.stopTime || alert.actualStopTime) : moment().add("1", "week"),
			startActive: (alert.startTime || alert.actualStartTime) ? true : false,
			stopActive: (alert.stopTime || alert.actualStopTime) ? true : false,
			emailNotification: alert.distributor === "WupAndEmailAlertDistributor",
			activateSurveillance: null,
			userRef: alert.userRef
		});

		if(!clone)
			config.scope.properties.id = alert.id;

		config.scope.properties.alertPolicy.value = alert.alertPolicy;
		config.scope.properties.distributionList.value = Number(alert.distributionId);
		config.scope.properties.distributionList.active = !!alert.distributionId;

		const foundDistributionList = config.scope.properties.distributionList.distributionListOptions.find(item => item.id === config.scope.properties.distributionList.value);
		if(foundDistributionList)
			config.scope.properties.distributionList.users = foundDistributionList.recipients.join("\n");

		// HERE ARE SOME SURVEILLANCE SHEANANIGANS!!!
		// every abm surveillance has one or more geometries
		// usually is stored in aoiAreas
		if (alert.aoiParams &&
			alert.aoiParams.aoiAreas &&
			(
				!alert.saParams || (
					alert.saParams &&
					(!alert.saParams.processedArea) &&
					(!alert.saParams.pastProcessedArea)
				)
			)
		) {
			// aoiArea is stored like this
			// R[-9.109566828039751,38.78316172516389 -9.109566828039751,38.78827568150879]
			// P[-9.109566828039751,38.78316172516389 -9.109566828039751,38.78827568150879]
			// we parse the coordinates to use them to create a aoi Feature.
			const geometry = new ol.geom.Polygon([[0,0]]);
			let coordinates = config.parseCoordinates(alert.aoiParams.aoiAreas);

			if (coordinates.length === 2){
				const [start, end] = coordinates;
				coordinates = [start, [start[0], end[1]], end, [end[0], start[1]], start];
				geometry.set("type", "rectangle");
			} else {
				if(coordinates[0] !== coordinates[coordinates.length -1])
					coordinates.push(coordinates[0]);
				geometry.set("type", "polygon");
			}

			geometry.setCoordinates([coordinates]);
			const aoi = new ol.Feature({geometry, favourite_name: alert.name, aoi_alert: alert});
			seg.favourites.addFeaturesToGroup("Session", aoi, alert.name);
			seg.selection.selectFeatureAndCenter(aoi);

			config.scope.aois.selectedAreas = [aoi];
			config.scope.aois.active = true;
		} else {
			// if processedArea or pastProcessedArea are present in saParams
			// then we create area with these coordinates
			// they usually are stored with each vertix of the polygon
			// "-9.109566828039751,38.78316172516389 -9.109566828039751,38.78827568150879 -9.01809288405113,38.78827568150879 -9.01809288405113,38.78316172516389"
			config.scope.aois.selectedAreas = [];

			if(alert.saParams.processedArea){
				config.scope.aois.selectedAreas = config.scope.aois.selectedAreas.concat(config.processedAreaToFeature(alert.saParams.processedArea, alert));
			}

			if(alert.saParams.pastProcessedArea){
				config.scope.aois.selectedAreas = config.scope.aois.selectedAreas.concat(config.processedAreaToFeature(alert.saParams.pastProcessedArea, alert));
			}

			config.scope.aois.active = true;
		}

		if(alert.voiParams){
			if (typeof alert.voiParams === "string")
				alert.voiParams = JSON.parse(alert.voiParams);

			config.scope.vessels.active = true;

			if(alert.voiParams.hasOwnProperty("EMSAId") && alert.voiParams["EMSAId"].length){
				const vesselList = [];

				config.scope.vessels.mode = "Vessel list";

				// TODO: change to new scope that can query lots of vessel instead of lots of queries of one vessel
				seg.promise.all(alert.voiParams["EMSAId"].map(vesselId => {
					return seg.request.get("v1/ship/getCurrentVesselPosition", {
						params: {
							vesselId
						}
					});
				})).then(results => {
					results.forEach(res => res.result && res.result.features.forEach(feature => {
						if(feature)
							vesselList.push(Object.assign(feature.properties, {selected: true}));
					}));

					if(vesselList.length)
						config.scope.vessels.vesselList.partialResults = vesselList;
					else {
						config.scope.vessels.vesselList.partialResults = alert.voiParams["EMSAId"].map(EMSAid => ({EMSAid, selected: true}));
						config.scope.vessels.vesselList.EMSAidNotFound = true;
					}
				});
			} else {
				config.scope.vessels.mode = "Filter";

				if(alert.voiParams.hasOwnProperty("-FlagState")){
					config.scope.vessels.filter["FlagState"] = alert.voiParams["-FlagState"];
					config.scope.vessels.filter["flagsInNotIn"] = "NOT IN";
				} else {
					config.scope.vessels.filter["FlagState"] = alert.voiParams["FlagState"];
					config.scope.vessels.filter["flagsInNotIn"] = "IN";
				}

				if(alert.voiParams.hasOwnProperty("-ShipType")){
					config.scope.vessels.filter["ShipType"] = alert.voiParams["-ShipType"];
					config.scope.vessels.filter["shipTypesInNotIn"] = "NOT IN";
				} else {
					config.scope.vessels.filter["ShipType"] = alert.voiParams["ShipType"];
					config.scope.vessels.filter["shipTypesInNotIn"] = "IN";
				}

				if(alert.voiParams.STRICT_MODE)
					config.scope.vessels.filter.exactMatch = true;
			}
		}

		// Fill templateId
		config.scope.template.value = Number(alert.templateId);

		config.scope.template.functions.changeType();

		// Finds template options
		const foundTemplateOptions = config.scope.template.templateOptions.find(opt => opt.id === config.scope.template.value);
		if(foundTemplateOptions && alert.saParams){
			config.scope.template.templateFields = config.scope.template.templateContent[foundTemplateOptions.name];
			Object.entries(alert.saParams).forEach(([param, value]) => {
				const field = config.scope.template.templateFields.find(field => field.name === param);
				if(field){
					if((field.type === "double" || field.type === "int" || field.type === "long") && field.unit !== "ISO8601 format (HH:mm:ss.SSS)")
						field.value = Number(value);
					else if(field.name === "processedArea" || field.name === "pastProcessedArea")
						field.value = config.scope.aois.selectedAreas || [];
					else if(field.unit === "ISO8601 format (HH:mm:ss.SSS)")
						field.value = moment().hour(Number(value.split(":")[0])).minute(Number(value.split(":")[1])).second(Number(value.split(":")[2]));
					else
						field.value = value;
				} else {
					const modal = seg.modal.openModal("ABM_surveillance_open", "Error loading surveillance template." , {class: "error"});
					seg.modal.closeTimeout(modal.title);
					return;
				}
			});
		}
	},
	processedAreaToFeature(coordinatesString, alert) {
		const geometry = new ol.geom.Polygon([[0,0]]),
			coordinates = coordinatesString.split(" ")
				.map(coord => coord.split(","))
				.map(coord => ol.proj.transform(coord.map(n => Number(n)), "EPSG:4326", seg.map.getProjection()));

		if(coordinates.length === 4){
			coordinates.push(coordinates[0]);
			geometry.set("type", "rectangle");
		} else {
			geometry.set("type", "polygon");
		}

		geometry.setCoordinates([coordinates]);
		const aoi = new ol.Feature({geometry, favourite_name: alert.name, aoi_alert: alert});
		seg.selection.selectFeatureAndCenter(aoi);
		return seg.favourites.addFeaturesToGroup("Session", aoi, alert.name);
	},
	update() {
		const modal = seg.modal.openModal("ABM_surveillance_update", "Updating ABM surveillance data...", {class: "info"});
		const request = seg.request.post("v1/abm/surveillance/instance?op=find", {data: {}}).then(({result}) => {
			modal.setContent("ABM Surveillance list updated.");
			seg.modal.closeTimeout(modal.title);
			reportService.setLogs(result);
		}, e => {
			modal.setContent("Error getting ABM surveillance.");
			seg.modal.closeTimeout(modal.title);
			seg.log.error("ERROR_REPORT_ABM_ALERT_UPDATE", e.error + ": " + e.result);
		});

		if(timeoutUpdate)
			seg.timeout.cancel(timeoutUpdate);

		timeoutUpdate = seg.timeout(() => {
			config.update();
		}, REFRESH_TIME * 1000);

		return request;
	},
	onClose() {
		if(timeoutUpdate)
			seg.timeout.cancel(timeoutUpdate);
	},
	removeReport(alert) {
		return seg.request.post("v1/abm/surveillance/instance?op=delete", {data: {id: alert.id}}).then(() => {
			config.update();
		});
	},
	// getSelectedFeature() {
	// 	return;
	// },
	isValid() {
		return config.scope.edit && config.scope.isValid();
	},
	shouldBeEditable() {
		return (config.scope.properties.id ? true : false) && (
			// If user can edit any surveillance
			config.scope.allSurveillance ||
			// If user can edit own surveillance, and surveillance is theirs
			config.scope.ownSurveillance && config.scope.properties.userRef === seg.user.getCurrentUser().userid
		);
	},
	submit() {
		config.scope.submit().then(() => reportService.scope.openLogs());
	},
	hasLog: true,
	parseCoordinates(coords) {
		coords = coords.replace(/[[\]RP]/g, "")
			.split(" ")
			.map(coord => coord.split(","));

		return coords.map(coord => ol.proj.transform(coord.map(n => Number(n)), "EPSG:4326", seg.map.getProjection()));
	},
	distanceChange(param) {
		if(seg.preferences.workspace.get("distanceUnits") === "nm")
			param.labelValue = ((param.value / 1852) * 100) / 100;
		else if(seg.preferences.workspace.get("distanceUnits") === "km")
			param.labelValue = ((param.value / 1000) * 100) / 100;
		else
			param.labelValue = param.value;

		param.changeValue = () => {
			if(seg.preferences.workspace.get("distanceUnits") === "nm")
				param.value = param.labelValue * 1852;
			else if(seg.preferences.workspace.get("distanceUnits") === "km")
				param.value = param.labelValue * 1000;
			else
				param.value = param.labelValue;
		};

		param.unitChanged = () => {
			if(seg.preferences.workspace.get("distanceUnits") === "nm"){
				if(param.labelUnits === "m")
					param.labelValue = ((param.labelValue / 1852) * 100) / 100;
				else
					param.labelValue = ((param.labelValue / 1.852) * 100) / 100;
			} else if(seg.preferences.workspace.get("distanceUnits") === "km"){
				if(param.labelUnits === "m")
					param.labelValue = ((param.labelValue / 1000) * 100) / 100;
				else
					param.labelValue = ((param.labelValue * 1.852) * 100) / 100;
			} else {
				if(param.labelUnits === "nm")
					param.labelValue = ((param.labelValue * 1852) * 100) / 100;
				else
					param.labelValue = ((param.labelValue * 1000) * 100) / 100;
			}

			param.changeValue();
			param.labelUnits = seg.preferences.workspace.get("distanceUnits");
		};
	},
	speedChange(param){
		if(seg.preferences.workspace.get("speedUnits") === "knots")
			param.labelValue = ((1.94384449 * param.value) * 100) / 100;
		else
			param.labelValue = param.value;

		param.changeValue = () => {
			if(seg.preferences.workspace.get("speedUnits") === "knots")
				param.value = param.labelValue / 1.94384449;
			else
				param.value = param.labelValue;
		};
		param.unitChanged = () => {
			if(seg.preferences.workspace.get("speedUnits") === "knots")
				param.labelValue = ((param.labelValue * 1.94384449) * 100) / 100;
			else
				param.labelValue = ((param.labelValue / 1.94384449) * 100) / 100;
			param.labelUnits = seg.preferences.workspace.get("speedUnits");
		};
	},
	timeChange(param){
		if(seg.preferences.workspace.get("timeUnits") === "min"){
			param.labelValue = ((param.value / 60) * 100) / 100;
		} else if(seg.preferences.workspace.get("timeUnits") === "h"){
			param.labelValue = ((param.value / 3600) * 100) / 100;
		} else
			param.labelValue = param.value;

		param.changeValue = () => {
			if(seg.preferences.workspace.get("timeUnits") === "min")
				param.value = param.labelValue * 60;
			else if(seg.preferences.workspace.get("timeUnits") === "h")
				param.value = param.labelValue * 3600;
			else
				param.value = param.labelValue;
		};
		param.unitChanged = () => {
			if(seg.preferences.workspace.get("timeUnits") === "min"){
				if(param.labelUnits === "sec")
					param.labelValue = ((param.labelValue / 60) * 100) / 100;
				else
					param.labelValue = ((param.labelValue * 60) * 100) / 100;
			} else if(seg.preferences.workspace.get("timeUnits") === "h"){
				if(param.labelUnits === "sec")
					param.labelValue = ((param.labelValue / 3600) * 100) / 100;
				else
					param.labelValue = ((param.labelValue / 60) * 100) / 100;
			} else {
				if(param.labelUnits === "min")
					param.labelValue = ((param.labelValue * 60) * 100) / 100;
				else
					param.labelValue = ((param.labelValue * 3600) * 100) / 100;
			}
			param.changeValue();
			param.labelUnits = seg.preferences.workspace.get("timeUnits");
		};
	}
};

module.exports = ({allSurveillance,allDistributionList,ownSurveillance,ownDistributionList}) => {
	Object.assign(config.scope, {
		allSurveillance,
		allDistributionList,
		ownSurveillance,
		ownDistributionList
	});
	config.displayFields.userRef.visible = allSurveillance;
	return config;
};
