let tdmLayer, tdmMenuItem;

module.exports = () => {
	return {
		name: "TDM",
		layers: [{
			id: "tdm",
			name: "TRAFFIC DENSITY MAPS",
			layers: [],
			visible: true,
			order: 2
		}],
		init() {
			if(seg.CONFIG.getValue("tdm.CREATE_APP_AND_LAYER")){
				const request = {
					areas: seg.request.get("v1/tdms/area/"),
					period: seg.request.get("v1/tdms/period/"),
					shiptype: seg.request.get("v1/tdms/shiptype/")
				};

				seg.promise.all(request).then(({areas, period, shiptype}) => {
					tdmLayer = seg.layer.get("tdm");

					tdmMenuItem = seg.ui.layerMenu.fromLayer("TRAFFIC DENSITY MAPS", tdmLayer);
					tdmMenuItem.toggle(false);

					seg.ui.layerMenu.register(tdmMenuItem);
					// Hammered data for sorting and extent data. This should come from service.
					const extents = {
							"north_sea_north_atlantic": [-42.544058999725294, 44.87655161972163, 41.96961195758122, 87.52954493098727],
							"atlantic": [-60.711768059864994, 19.78985184576753, 23.801902897441526, 62.44284515703316],
							"baltic_sea": [-1.4725464206329626, 48.012335891452295, 40.78428905802029, 69.3388325470851],
							"black_sea": [23.606456615980253, 37.66069160688603, 44.734874355306886, 48.32393993470244],
							"mediterranean_sea": [-5.920358720707144, 25.912630392463722, 36.336476757946116, 47.239127048096535],
							"all_europe": [-75.41252525673498, 13.767428866322808, 93.61481665787804, 70.01564665050606]
						},
						areasOrder = {"north_sea_north_atlantic": 0, "atlantic": 1, "baltic_sea": 2, "black_sea": 3, "mediterranean_sea": 4, "all_europe": 5},
						shiptypeOrder = {"all_traffic": 0, "cargo": 1, "tanker": 2, "fishing": 3, "passenger": 4, "all_other": 5};

					if(areas) {
						areas.forEach(area => {
							area.extent = extents[area.id];
							area.order = areasOrder[area.id];
						});
						areas.sort((a, b) => (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0));
					}

					if(shiptype){
						shiptype.forEach(st => {
							st.order = shiptypeOrder[st.id];
						});
						shiptype.sort((a, b) => (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0));
					}

					seg.ui.apps.add({
						name: "TDM",
						label: "Add a Traffic Density Map",
						icon: "traffic-map",
						bindings: {
							scope: require("./app")({areas, period, shiptype})
						},
						templateUrl: resolvePath("./templates/app.html")
					});
				}, e => {
					seg.log.error("ERROR_TDMS_INIT", e.error + ": " + e.result, e);
					const modal = seg.modal.openModal("ERROR_TDMS_INIT", "Error in Density Maps services: " + e.error + " - " + e.result, "error");
					seg.modal.closeTimeout(modal.title);
				});
			}
		}
	};
};

