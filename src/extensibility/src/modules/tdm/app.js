let tdmLayer, tdmTable;

const tdmLayerCache = {},
	fields = [
		{
			template: "<img src=\"v1/tdms/tdm/legend/{{tdmLayer.get('selectedDensityMap').id}}\" style=\"float: right;\">"
		},
		{
			label: "Creation Date Time",
			template: "<span position-timestamp=\"::tdmLayer.get('creationdt')\"></span>"
		},
		{
			label: "Area",
			template: "{{ tdmLayer.get('selectedArea') }}"
		},
		{
			label: "Period",
			template: "{{ tdmLayer.get('selectedPeriod') }}"
		},
		{
			label: "Start date",
			template: "<span position-timestamp=\"::tdmLayer.get('startdt')\"></span>"
		},
		{
			label: "End date",
			template: "<span position-timestamp=\"::tdmLayer.get('enddt')\"></span>"
		},
		{
			label: "Number of Ships Monitored",
			template: `
				<ul style="margin-left: 20px;">
					<li ng-repeat="(key, value) in tdmLayer.get('numberShipsMonitored')" style="text-transform: capitalize;">
						<strong>{{ key }}</strong>: {{ value }}
					</li>
				</ul>
			`
		}
	];

module.exports = (({areas, period, shiptype}) => {
	const scope = {
		init() {
			tdmLayer = seg.layer.get("tdm");
			scope.reset();
		},
		info() {
			seg.popup.openPopup({
				title: "Traffic Density Maps Info",
				templateUrl: "extensibility/modules/tdm/templates/app_info.html",
				bindings: {
					scope: {
						fields,
						label: getLabel(scope),
						tdmLayer: tdmLayerCache[scope.id]
					}
				}
			});
		},
		close() {
			seg.ui.apps.close("TDM");
		},
		reset(fullReset) {
			if(fullReset){
				tdmLayer.getLayers().forEach(layer => seg.ui.cleanables.call("TDM: " + getLabel(layer.getProperties())));
				seg.ui.ttt.tables.close(tdmTable);
			}

			Object.assign(scope, {
				id: false,
				availableAreas: areas.map(({name, code, id, extent}) => ({name, code, id, extent})),
				availableShips: shiptype.map(({name, type, id}) => ({name, type, id})),
				availablePeriods: period.map(({name, type, id}) => ({name, type, id})),
				availableDensityMaps: null,
				selectedPeriod: null,
				selectedArea: null,
				selectedShip: null,
				selectedDensityMap: null,
				label: null,
				legendUrl: null
			});
		},
		isGetAvailableDisabled() {
			return !(scope.selectedArea && scope.selectedPeriod && scope.selectedShip);
		},
		getAvailable(){
			if(scope.selectedPeriod && scope.selectedArea && scope.selectedShip){
				scope.selectedDensityMap = null;
				scope.availableDensityMaps = [];
				// After selecting area, period and shiptype, we query to get the according id
				seg.request.get("v1/tdms/tdm", {
					params: {
						area: scope.selectedArea,
						period: scope.selectedPeriod,
						shiptype: scope.selectedShip
					}
				}).then((result = []) => {
					// and then we map the date combobox with these dates and ids
					scope.availableDensityMaps = result.map(({id, startdt, enddt}) => ({
						id,
						range: moment(startdt).format("YYYY-MM (MMMM)"),
						label: moment(startdt).format("YYYY-MM"),
						startdt: moment(startdt),
						enddt: moment(enddt)
					})).sort((a,b) => {
						if(a.range > b.range) return -1;
						if(a.range < b.range) return 1;
						if(a.range === b.range) return 0;
					});
				});
			}
		},
		isSubmitDisabled(){
			return !scope.selectedDensityMap;
		},
		async submit() {
			// upon submit we create a wms layer
			scope.id = [scope.selectedArea, scope.selectedShip, scope.selectedPeriod, scope.selectedDensityMap.range].join("-");

			const modal = seg.modal.openModal(scope.id, "Loading " + getLabel(scope) + " Traffic Density Map...", {class: "info"});
			if(!tdmLayerCache[scope.id]){
				const metadata = (await seg.request.get("v1/tdms/tdm/metadata/" + scope.selectedDensityMap.id)).metadata;

				tdmLayerCache[scope.id] = seg.layer.create({
					id: scope.id,
					name: "Traffic Density Maps Info",
					visible: true,
					type: "Image",
					source: {
						type: "ImageWMS",
						url: "v1/tdms/tdm/wms/" + scope.selectedDensityMap.id,
						params: {
							VERSION: "1.1.1"
						},
						projections: ["EPSG:3857", "EPSG:4326"],
						customLoadFunction: async (img, url) => {
							let newImg;
							try {
								const params = seg.utils.getParams(url);
								newImg = await seg.request.getDataURL(seg.utils.addParamsToURL(url, params));

								if(newImg){
									img.getImage().src = newImg;
									modal.setContent((getLabel(scope) + " Traffic Density Map was loaded."));
									modal.setClass("success");
								} else {
									img.getImage().src = seg.source.transparent1x1Image();
									modal.setContent(("Error loading " + getLabel(scope) + " Traffic Density Map."));
									modal.setClass("error");
								}

								seg.modal.closeTimeout(modal.title);
							} catch (e) {
								img.getImage().src = seg.source.transparent1x1Image();
								modal.setContent("Error loading " + getLabel(scope) + " Traffic Density Map.");
								seg.modal.closeTimeout(modal.title);
							}
						}
					},
					selectable: {
						layerAs: "tdmLayer",
						fields
					}
				});

				tdmLayerCache[scope.id].setProperties({
					scope: {id: scope.id},
					selectedPeriod: scope.selectedPeriod.replace("_", " ").toUpperCase(),
					selectedArea: scope.selectedArea.replace("_", " ").toUpperCase(),
					selectedShip: scope.selectedShip.replace("_", " ").toUpperCase(),
					selectedRange: scope.selectedDensityMap.range,
					selectedDensityMap: scope.selectedDensityMap,
					startdt: scope.selectedDensityMap.startdt,
					creationdt: moment(metadata.CreationDateTime),
					numberShipsMonitored: metadata.Number_ShipsMonitored,
					enddt: scope.selectedDensityMap.enddt,
					_extent: scope.availableAreas.find(area => scope.selectedArea === area.code).extent || [0,1,0,1]
				});

				tdmLayer.addLayer(tdmLayerCache[scope.id], [tdmLayer]);
				tdmLayer.setVisible(true);
			}

			// If table does not exist, create it
			if(!tdmTable){
				tdmTable = seg.ui.ttt.tables.open({
					label: "Traffic Density Maps",
					itemAs: "tdmLayer",
					data: [],
					fields: {
						area: {
							label: "Area",
							template: "{{ tdmLayer.get('selectedArea') }}",
							visible: true,
							order: 0
						},
						shiptype: {
							label: "Ship Type",
							template: "{{ tdmLayer.get('selectedShip') }}",
							visible: true,
							order: 1
						},
						period: {
							label: "Period",
							template: "{{ tdmLayer.get('selectedPeriod') }}",
							visible: true,
							order: 2
						},
						dates: {
							label: "Dates",
							template: "{{ tdmLayer.get('selectedRange') }}",
							visible: true,
							order: 3
						}
					},
					onSelectionChange(changedRows, selectedRows) {
						if(selectedRows.length === 1){
							const selectedLayer = selectedRows[0].item;
							tdmLayer.getLayers().forEach(layer => layer.setVisible(false));
							tdmLayerCache[selectedLayer.get("id")].setVisible(true);
						}
					}

				});
			} else {
				// Open it otherwise
				seg.ui.ttt.tables.open(tdmTable);
			}

			// Set all layers as not visible
			tdmLayer.getLayers().forEach(layer => layer.setVisible(false));
			// Set the created or existing layer as visible
			tdmLayerCache[scope.id].setVisible(true);
			// Set the view around the map extent
			seg.map.getView().fit(ol.proj.transformExtent(tdmLayerCache[scope.id].get("_extent"), "EPSG:4326", seg.map.getProjection()));
			// Adding the visible data to the table
			seg.ui.ttt.tables.addDataToTable(tdmTable, tdmLayer.getLayers().filter(layer => layer.getVisible()));
			// Creating cleanable to remove layer
			seg.ui.cleanables.add("TDM: " + getLabel(scope), (_scope) => {
				tdmTable.data = tdmTable.data.filter(d => d !== seg.layer.get(_scope.id));
				tdmLayerCache[_scope.id].setVisible(false);
			}, Object.assign({}, {id: tdmLayerCache[scope.id].get("id")}));
		},
		setMonth(){
			const monthArr = getMonthArray(scope.selectedYearMonthly);
			scope.availableMonths = monthArr.months;
			scope.selectedMonth = scope.selectedMonth > monthArr.lastMonth ? monthArr.lastMonth : scope.selectedMonth;
		}
	};

	return scope;
});

const getCurrentYear = () => {
	return new Date().getFullYear();
};

const getCurrentMonth = () => {
	return new Date().getMonth() + 1;
};

const getMonthArray = (year) => {
	const months = [],
		lastMonth = year === getCurrentYear() ? getCurrentMonth() : 12;

	for(let i = lastMonth; i > 0; i--)
		months.unshift(i);

	return {
		months,
		lastMonth
	};
};

const getLabel = (scope) => {
	return [
		scope.selectedDensityMap && scope.selectedDensityMap.label,
		scope.selectedArea,
		scope.selectedShip,
		scope.selectedPeriod
	].join(" / ")
		.replace(/_/g, " ")
		.toUpperCase();
};