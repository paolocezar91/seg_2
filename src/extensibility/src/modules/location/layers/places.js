module.exports = {
	id: "places",
	name: "Places",
	type: "Vector",
	source: {
		type: "Vector"
	},
	search: {
		keys: [
			"place_name", "country_name", "code"
		],
		label: "place_name",
		smartSearch: (smartSearchResults) => {
			const results = smartSearchResults["PLACE_KEY"],
				placesSource = seg.layer.get("places").getSource();

			placesSource.getFeatures().forEach((vessel) => vessel.unset("isSmartSearchResult"));

			if (results && results.features && results.features.length) {
				const places = mergeDuplicates((new ol.format.GeoJSON()).readFeatures(results, {
					dataProjection: "EPSG:4326",
					featureProjection: seg.map.getProjection()
				}));

				placesSource.addFeatures(places);

				places.forEach((place) => {
					place.set("isSmartSearchResult", true);
					return place;
				});

				return places;
			}
			return [];
		}
	},
	style() {
		//stop rendering until further notice
		return;
	}
};

const mergeDuplicates = places => {
	const chunks = [],
		placesSource = seg.layer.get("places").getSource(),
		BATCH_SIZE = seg.CONFIG.getValue("BATCH_SIZE");

	for(let i=0; i<places.length; i+=BATCH_SIZE)
		chunks.push(places.slice(i, i+BATCH_SIZE));

	const mergedChunks = chunks.map(places => mergePlaces(placesSource, places));

	return [].concat(...mergedChunks);
};

const mergePlaces = (placesSource, places) => places.map(place => {
	const existing = placesSource.getFeatureById(place.getId());
	return existing ? existing : place;
});
