module.exports = {
	id: "country",
	name: "Country",
	type: "Vector",
	source: {
		type: "Vector"
	},
	search: {
		keys: [
			"code", "name"
		],
		label: "name"
	},
	style() {
		//stop rendering until further notice
		return;
	}
};
