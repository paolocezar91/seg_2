/**
@namespace extensibility.modules.location
@description
# Location

## Available options
None

## Global configurations and constants
None
*/
module.exports = (/*config*/) => {
	return {
		name: "Location",
		layers: [{
			id: "location",
			name: "Location",
			layers: [
				require("./layers/countries.js"),
				require("./layers/places.js")
			]
		}],
		init() {
			//seg.ui.searchAdvanced.addPanel(require("./search"));
			const countrySource = seg.layer.get("country").getSource(),
				countryList = require("../../common/flag_states_obj.json");///XXX COUNTRY

			countryList.forEach(country => {
				if(country.latitude && country.longitude)
					countrySource.addFeature(new ol.Feature({
						name: country.name,
						geometry: new ol.geom.Point(ol.proj.fromLonLat([country.longitude, country.latitude], seg.map.getProjection())),
						code: country.code
					}));
			});
		}
	};
};
