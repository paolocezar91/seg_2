module.exports = {
	name: "Location",
	template: resolvePath("./templates/search.html"),
	services: (function () {
		const countryList = require("../../common/flag_states.json"),
			countries = [];

		Object.keys(countryList).forEach((key) => {
			const country = {
				name: countryList[key],
				value: key,
				selected: false
			};
			countries.push(country);

		});

		const config = {
			search: {
				//countryList: countryList,
				countries: countries,
				selectedCountries: []
			}
		};
		return config;
	})(),
	submit: function () {
		const params = {locode: this.services.search.locode},
			loaderModal = seg.modal.openModal("LOCODE Advanced Search", "Retrieving Locations...", {class: "info"}),
			//	const boxCoordinate = options.bboxes[0];
			LocodeRequest = seg.request.get("v1/cld/locodes", {
				params: params
			});

		loaderModal.setDismissCallback(LocodeRequest.cancel);

		return LocodeRequest.then((res) => {
			const locations = res.result;

			//if no reports were retrieved, return
			if (!locations.length) {
				loaderModal.setContent("Retrieved " + locations.length + " Locations.");
				loaderModal.setClass("no-result");
				seg.modal.closeTimeout(loaderModal.title);
				return;
			}

			loaderModal.setContent("Retrieved " + locations.length + " Locations.");
			loaderModal.setContent("success");

			let locationsTable = seg.ui.ttt.tables.openTables.find(t => t.label === "Advanced Search Locations");

			if (!locationsTable)
				locationsTable = seg.ui.ttt.tables.open({
					label: "Advanced Search Locations",
					data: locations,
					itemAs: "location",
					fields: {
						countryCode: {
							label: "Country Code",
							template: "{{location.countryCode}}"
						},
						createdBy: {
							label: "Created By",
							template: "{{location.createdBy}}"
						},
						createdOn: {
							label: "Created On",
							template: "<span position-timestamp=\"location.createdOn\"></span>"
						},
						loCode: {
							label: "LOCODE",
							template: "{{location.loCode}}"
						},
						locationDefinedAs: {
							label: "Location As",
							template: "{{location.locationDefinedAs}}"
						},
						locationNameUNECE: {
							label: "Location Name UNECE",
							template: "{{location.locationNameUNECE}}"
						}
					}
				});
			else
				locationsTable.data = locations;

			seg.modal.closeTimeout(loaderModal.title);
		}, e => seg.log.error("ERROR_LOCODE_ADVANCED_SEARCH_SUBMIT", e.error + ": " + e.result));
	},
	reset: function () {
		this.services.search.locode = null;
		this.services.search.locationName = null;
		this.services.search.countryCode = null;
		this.services.search.countryName = null;
	}
};
