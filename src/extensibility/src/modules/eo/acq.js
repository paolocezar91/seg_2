const
	eoFootprintACQ = require("./SAR/footprints/acq"),
	oilSpillsACQ = require("./SAR/oil_spills/acq"),
	vdsACQ = require("./VDS/acq"),
	ACQ_TIME_LIMIT = 6; //months

const buildServices = ({eo_acq, oil_spills, vds}) => {
	const services = {};

	if(eo_acq)
		services["Footprints"] = {enabled: false};

	if(oil_spills)
		services["Oil Spills"] = {enabled: false};

	if(vds)
		services["Correlated Positions"] = {enabled: false},
		services["Uncorrelated Positions"] = {enabled: false};

	const _config = {
		EOProducts: {
			enabled: false,
			modes: [
				"All",
				"Select..."
			],
			mode: "All",
			services,
			resetEOProducts: () => {
				_config.EOProducts.mode = "All";
				if(eo_acq)
					services["Footprints"] = {enabled: false};

				if(oil_spills)
					services["Oil Spills"] = {enabled: false};

				if(vds)
					services["Correlated Positions"] = {enabled: false},
					services["Uncorrelated Positions"] = {enabled: false};
			}
		}
	};
	return _config;
};

module.exports = ({eo_acq, oil_spills, vds, footprints_table_fields, oil_spills_table_fields}) => ({
	name: "EO",
	template: resolvePath("./templates/acq.html"),
	services: buildServices({eo_acq, oil_spills, vds}),
	isTimeValid(start, end) {
		this.services.EOProducts.timeStatus = {
			invalid: !(end.diff(start, "M", true) <= ACQ_TIME_LIMIT),
			msg: "Time range should be less than " + ACQ_TIME_LIMIT + " months."
		};

		return !this.services.EOProducts.timeStatus.invalid;
	},
	isSelectionValid() {
		this.services.EOProducts.selectionStatus = {
			invalid: this.services.EOProducts.mode === "Select..." && //Either no service is selected no footprints in ACQ and we"re in Select... mode
				(Object.keys(this.services.EOProducts.services).every(key => !this.services.EOProducts.services[key].enabled)),
			msg: "Select at least one EO product."
		};

		return !this.services.EOProducts.selectionStatus.invalid;
	},
	submit(bboxes, from, to, force) {
		const opts = this.services.EOProducts;

		if(!opts.enabled && !force)
			return;

		const services = opts.services,
			eo = eo_acq && ((force && force.Footprints) || services.Footprints.enabled || opts.mode === "All"),
			spills = oil_spills && ((force && force["Oil Spills"]) || services["Oil Spills"].enabled || opts.mode === "All"),
			uncorrelated = vds && (!force && (services["Uncorrelated Positions"].enabled || opts.mode === "All")),
			correlated = vds && (!force && (services["Correlated Positions"].enabled || opts.mode === "All")),
			requests = [];

		if(eo)
			requests.push(eoFootprintACQ(bboxes, from, to, footprints_table_fields));
		if(spills)
			requests.push(oilSpillsACQ(bboxes, from, to, oil_spills_table_fields));
		if(uncorrelated || correlated)
			requests.push(vdsACQ(bboxes, from, to, uncorrelated, correlated));

		return seg.promise.all(requests).then(results => results, () => seg.map.disableFilter("ACQ_FILTER"));
	}
});