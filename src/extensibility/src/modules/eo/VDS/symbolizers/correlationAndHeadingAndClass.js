const styleCache = {},
	CORRELATED_STYLE = new ol.style.Style({
		text: new ol.style.Text({
			font: "10px sans-serif",
			text: "●",
			fill: new ol.style.Fill({
				color: "#000"
			}),
			textBaseline: "middle"
		})
	}),
	getSize = hasHeading => {
		const max = hasHeading?40:30,
			scale = hasHeading?5400000:4400000;

		return Math.min(max, Math.max(40, Math.round(scale / seg.map.getScale()) * 12));
	},
	getLengthClass = (lengthClass,sar) =>  {
		const lenghtClassReference = {
				"SAR": [
					[100,999,"#f00"],
					[30,100,"#ffa500"],
					[0,30,"#ff0"]
				],
				"Optical": [
					[100,999,"#f00"],
					[50,100,"#ffa500"],
					[20,50,"#ff0"],
					[15,20,"#00f"],
					[10,15,"#f10000"],
					[0,10,"#f0f"]
				]
			},
			isInRange = (value,min,max) => !isNaN(value) && (value > min) && (value <= max),
			result = lenghtClassReference[sar ? "SAR" : "Optical"].filter((interval) => isInRange(lengthClass, interval[0], interval[1]))[0];
		return result ? result[2] : ( sar ? "#0f0" : "#cbcbcb" );
	},
	buildCorrelationAndHeadingAndClass = vessel => {
		const heading = vessel.get("CORRELATED_POSITION_HEADING") || vessel.get("ESTIMATED_COG"),
			hasHeading = typeof heading !== "undefined",
			rotation = hasHeading ? heading * Math.PI / 180 : false,
			fill = getLengthClass( vessel.get("ESTIMATED_LENGTH"), vessel.get("sar")? true : false),
			height = getSize(hasHeading);

		return {
			svg: hasHeading ? require("../../../vessels/positions/symbolizers/IALA/iala.svg") : require("../../../vessels/positions/symbolizers/IALA/iala-nohdg.svg"),
			fill,
			stroke: {
				color: "#000",
				width: 1
			},
			height,
			rotation
		};
	},
	style = vessel => {
		const baseSymbolizer = buildCorrelationAndHeadingAndClass(vessel),
			{fill, rotation, height} = baseSymbolizer,
			cacheKey = JSON.stringify({fill, rotation, height}),
			styles = [];

		if(!styleCache[cacheKey]){
			baseSymbolizer.fill = fill;

			styleCache[cacheKey] = new ol.style.Style({
				image: new seg.style.SVG(baseSymbolizer)
			});
		}

		styles.push(styleCache[cacheKey]);

		if (vessel.get("correlated"))
			styles.push(CORRELATED_STYLE);

		return styles;
	};

module.exports = {
	name: "Correlation And Heading And Class",
	style,
	CORRELATED_STYLE
};