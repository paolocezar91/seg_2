let DEFAULT_SYMBOLIZER;
const symbolizers = {
		"Correlation And Heading": require("./correlationAndHeading.js"),
		"Correlation And Heading And Class": require("./correlationAndHeadingAndClass.js"),
		"Correlation And Heading And Type": require("./correlationAndHeadingAndType.js")
	},
	getSelected = () => symbolizers[seg.preferences.map.get("eo.correlated.symbolizer")] || DEFAULT_SYMBOLIZER,
	select = symbolizer => seg.preferences.map.set("eo.correlated.symbolizer", symbolizer.name),
	isSelected = symbolizer => symbolizer.name === getSelected().name,
	buildSymbolizerMenu = symbolizerNames => {
		const detectedVesselsSource = seg.layer.get("detected_vessels").getSource();

		//CHECK HERE FOR SYMBOLIZER NAME
		DEFAULT_SYMBOLIZER = symbolizers[symbolizerNames[0]];

		return Object.assign(seg.ui.layerMenu.create("Symbology"), {
			options: symbolizerNames.map(symbolizerName => {
				const symbolizer = symbolizers[symbolizerName],
					menuItem = seg.ui.layerMenu.create(symbolizer.name);

				if(Array.isArray(symbolizer))
					return Object.assign(menuItem, {
						isChecked() {
							return symbolizer.some(isSelected);
						},
						toggle() {
							if(!symbolizer.some(isSelected)) {
								select(symbolizer[0]);
								detectedVesselsSource.changed();
							}
						},
						hideSelectAll: true,
						options: symbolizer.map(symbolizer => Object.assign(seg.ui.layerMenu.create(symbolizer.name), {
							isChecked() {
								return isSelected(symbolizer);
							},
							toggle() {
								select(symbolizer);
								detectedVesselsSource.changed();
							}
						}))
					});


				return Object.assign(menuItem, {
					isChecked() {
						return isSelected(symbolizer);
					},
					toggle() {
						select(symbolizer);
						detectedVesselsSource.changed();
					}
				});
			}),
			hideSelectAll: true,
			type: "symbology"
		});
	};

module.exports = {
	buildSymbolizerMenu,
	getSelected,
	optical: {
		name: "Optical VDS Symbology",
		selectedSymbology: () => {
			return getSelected().name;
		},
		sar: false
	},
	sar: {
		name: "SAR VDS Symbology",
		selectedSymbology: () => {
			return getSelected().name;
		},
		sar: true
	}
};


