const styleCache = [],
	{ CORRELATED_STYLE } = require("./correlationAndHeadingAndClass"),
	getSize = hasHeading => {
		const max = hasHeading?40:30,
			scale = hasHeading?5400000:4400000;

		return Math.min(max, Math.max(40, Math.round(scale / seg.map.getScale()) * 12));
	},
	//isInRange = (value,min,max) => !isNaN(value) && (value > min) && (value <= max),
	getColor = type => {
		// Passenger vessels: BLUE
		if (type === 370 || type === 371 || type === 383 || (type >= 60 && type <= 69))
			return "#0000FF";
		// Cargo vessels: GREEN
		if (type === 355 || type === 360 || type === 361 || type === 384 || (type >= 70 && type <= 79))
			return "#00FF00";
		// Tanker vessels: RED
		if (type === 310 || type === 311 || type === 313 || type === 314 || type === 330 || (type >= 80 && type <= 89))
			return "#FF0000";
		// Tug vessel : LIGHT BLUE
		if (type === 385 || type === 52)
			return "#00ffff";
		// Pleasure vessels : PURPLE
		if (type === 319 || type === 37)
			return "#990099";
		// Fishing vessels: ORANGE
		if (type === 315 || type === 30)
			return "#FF5721";

		return "#808080";
	},
	buildCorrelationAndHeadingAndClass = vessel => {
		const heading = vessel.get("CORRELATED_POSITION_HEADING") || vessel.get("ESTIMATED_COG"),
			hasHeading = typeof heading !== "undefined",
			rotation = hasHeading ? heading * Math.PI / 180 : false,
			fill = getColor(Number(vessel.get("CORRELATED_IDENTITY_SHIP_TYPE"))),
			height = getSize(hasHeading);

		return {
			svg: hasHeading ? require("../../../vessels/positions/symbolizers/IALA/iala.svg") : require("../../../vessels/positions/symbolizers/IALA/iala-nohdg.svg"),
			fill,
			stroke: {
				color: "#000",
				width: 1
			},
			height,
			rotation
		};
	},
	style = vessel => {
		const baseSymbolizer = buildCorrelationAndHeadingAndClass(vessel),
			{fill, rotation, height} = baseSymbolizer,
			cacheKey = JSON.stringify({fill, rotation, height}),
			styles = [];

		if(!styleCache[cacheKey]){
			baseSymbolizer.fill = fill;
			styleCache[cacheKey] = new ol.style.Style({
				image: new seg.style.SVG(baseSymbolizer)
			});
		}

		styles.push(styleCache[cacheKey]);

		if (vessel.get("correlated"))
			styles.push(CORRELATED_STYLE);

		return styles;
	};

module.exports = {
	name: "Correlation And Heading And Type",
	style
};


