const styleCache = {},
	getSize = hasHeading => {
		const max = hasHeading?40:30,
			scale = hasHeading?5400000:4400000;

		return Math.min(max, Math.max(40, Math.round(scale / seg.map.getScale()) * 12));
	},
	style = vessel => {
		const baseSymbolizer = buildCorrelationAndHeadingStyle(vessel),
			{fill, rotation, height} = baseSymbolizer,
			styles = [],
			cacheKey = JSON.stringify({fill, rotation, height});

		if(!styleCache[cacheKey]) {
			baseSymbolizer.fill = fill;

			styleCache[cacheKey] = new ol.style.Style({
				image: new seg.style.SVG(baseSymbolizer)
			});
		}

		styles.push(styleCache[cacheKey]);
		return styles;
	},
	buildCorrelationAndHeadingStyle = vessel => {
		const heading = vessel.get("CORRELATED_POSITION_HEADING") || vessel.get("ESTIMATED_COG"),
			hasHeading = typeof heading !== "undefined",
			rotation = hasHeading ? heading * Math.PI / 180 : false,
			fill = vessel.get("uncorrelated") ? "#AC0700" : "#FC7F7B",
			height = getSize(hasHeading);

		return {
			svg: hasHeading ? require("../../../vessels/positions/symbolizers/IALA/iala.svg") : require("../../../vessels/positions/symbolizers/IALA/iala-nohdg.svg"),
			fill,
			stroke: {
				color: "#000",
				width: 1
			},
			height,
			rotation
		};
	};

module.exports = {
	name: "Correlation And Heading",
	style
};