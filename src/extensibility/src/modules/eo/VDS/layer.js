module.exports = config => {
	const detectedVesselsBindings = require("./command_info.scope.js")(config);
	return {
		id: "detected_vessels",
		name: "Detected Vessels",
		type: "Vector",
		source: {
			name: "Detected Vessels",
			type: "Vector"
		},
		commandInfo: {
			featureAs: "detectedVessel",
			bindings: {
				detectedVesselsCommandInfo: detectedVesselsBindings
			},
			//template url for this C&Iz
			templateUrl: resolvePath("./templates/command_info.html")
		},
		tooltip: {
			featureAs: "detectedVessel",
			bindings: {
				detectedVesselsTooltip: detectedVesselsBindings
			},
			templateUrl: resolvePath("./templates/tooltip.html")
		},
		style: require("./style.js"),
		filters: ["SAR","optical"].map(type => ({
			name: type + "Correlation",
			options: {
				"Correlated": function (feature) {
					return (!!feature.get("sar") === (type === "SAR")) && !feature.get("uncorrelated") ;
				},
				"Uncorrelated": function (feature) {
					return (!!feature.get("sar") === (type === "SAR")) && feature.get("uncorrelated");
				},
				"All": function(feature){
					return (feature.get("sar") === (type === "SAR"));
				},
				"AllUpperLevel": function(feature){
					return (feature.get("sar") === (type === "SAR"));
				}
			}
		}))
	};
};