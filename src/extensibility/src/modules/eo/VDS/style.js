const selectedStylesCache = [],
	{ getSelected } = require("./symbolizers/index.js"),
	getSize = hasHeading => {
		const max = hasHeading?40:30,
			scale = hasHeading?5400000:4400000;

		return Math.min(max, Math.max(40, Math.round(scale / seg.map.getScale()) * 12));
	},
	style = (vessel) =>{
		const styles = [];

		if (vessel.get("selected")){
			const heading = vessel.get("CORRELATED_POSITION_HEADING") || vessel.get("ESTIMATED_COG"),
				height = getSize(heading !== "undefined") / 1.7,
				selectedCacheKey = JSON.stringify({ height });

			if (!selectedStylesCache[selectedCacheKey])
				selectedStylesCache[selectedCacheKey] = new ol.style.Style({
					image: new ol.style.Circle({
						radius: height,
						stroke: new ol.style.Stroke({
							color: "yellow",
							width: 2
						})
					})
				});

			styles.push(selectedStylesCache[selectedCacheKey]);
		}

		styles.push(...getSelected().style(vessel));

		return styles;
	};

module.exports = style;