module.exports = (sar) => {
	const symbologies = {
		"Correlation And Heading": [
			{
				title: "Correlation",
				symbologies: [
					{
						class: "correlated",
						label: "Correlated",
						style: "fill: #FC7F7B;",
						icon: "vessel-iala"
					},
					{
						class: "uncorrelated",
						label: "Uncorrelated",
						style: "fill: #AC0700;",
						icon: "vessel-iala"
					}
				]
			},
			{
				title: "Heading",
				symbologies: [
					{
						class: "heading",
						label: "Heading available",
						style: "fill: transparent; stroke: black; stroke-width: 2px;",
						icon: "vessel-iala"
					},
					{
						class: "heading",
						label: "No heading available",
						style: "fill: transparent; stroke: black; stroke-width: 2px;",
						icon: "vessel-iala-nohdg"
					}
				]
			}
		],
		"Correlation And Heading And Class": [
			{
				title: "Correlation",
				symbologies: [
					{
						class: "correlated",
						label: "Correlated",
						style: "fill: transparent; stroke: black; stroke-width: 2px;",
						icon: "vessel-iala"
					},
					{
						class: "uncorrelated",
						label: "Not Correlated",
						style: "fill: transparent; stroke: black; stroke-width: 2px;",
						icon: "vessel-iala"
					}
				]
			},
			{
				title: "Heading",
				symbologies: [
					{
						class: "heading",
						label: "Heading available",
						style: "fill: transparent; stroke: black; stroke-width: 2px;",
						icon: "vessel-iala"
					},
					{
						class: "heading",
						label: "No heading available",
						style: "fill: transparent; stroke: black; stroke-width: 2px;",
						icon: "vessel-iala-nohdg"
					}
				]
			}
		],
		"Correlation And Heading And Type": [
			{
				title: "Correlation",
				symbologies: [
					{
						class: "correlated",
						label: "Correlated",
						style: "fill: transparent; stroke: black; stroke-width: 2px;",
						icon: "vessel-iala"
					},
					{
						class: "uncorrelated",
						label: "Not Correlated",
						style: "fill: transparent; stroke: black; stroke-width: 2px;",
						icon: "vessel-iala"
					}
				]
			},
			{
				title: "Type",
				symbologies: [
					{
						class: "passenger_vessels",
						label: "Passenger vessels",
						style: "fill: #0000FF;",
						icon: "vessel-iala"
					},
					{
						class: "cargo_vessels",
						label: "Cargo vessels",
						style: "fill: #00FF00;",
						icon: "vessel-iala-nohdg"
					},
					{
						class: "tanker_vessels",
						label: "Tanker vessels",
						style: "fill: #FF0000;",
						icon: "vessel-iala"
					},
					{
						class: "tug_vessel",
						label: "Tug vessel",
						style: "fill: #00ffff;",
						icon: "vessel-iala"
					},
					{
						class: "pleasure_vessels",
						label: "Pleasure vessels",
						style: "fill: #990099;",
						icon: "vessel-iala"
					},
					{
						class: "fishing_vessels",
						label: "Fishing vessels",
						style: "fill: #FF5721;",
						icon: "vessel-iala"
					},
					{
						class: "unknown",
						label: "Unknown",
						style: "fill: #808080;",
						icon: "vessel-iala"
					}
				]
			},
			{
				title: "Heading",
				symbologies: [
					{
						class: "heading",
						label: "Heading available",
						style: "fill: transparent; stroke: black; stroke-width: 2px;",
						icon: "vessel-iala"
					},
					{
						class: "heading",
						label: "No heading available",
						style: "fill: transparent; stroke: black; stroke-width: 2px;",
						icon: "vessel-iala-nohdg"
					}
				]
			}
		]
	};

	const temp = symbologies["Correlation And Heading And Class"][1];
	if(sar)
		symbologies["Correlation And Heading And Class"][1] = (
			{
				title: "Vessel Estimated Length",
				symbologies: [
					{
						class: "sar-100",
						label: "length > 100m",
						style: "fill: #f00;",
						icon: "vessel-iala"
					},
					{
						class: "sar-30-100",
						label: "30m < length < 100m",
						style: "fill: #ffa500;",
						icon: "vessel-iala"
					},
					{
						class: "sar-0-30",
						label: "length < 30m",
						style: "fill: #ff0;",
						icon: "vessel-iala"
					},
					{
						class: "sar-unknown",
						label: "length unknown",
						style: "fill: #0f0;",
						icon: "vessel-iala"
					}
				]
			}
		);
	else
		symbologies["Correlation And Heading And Class"][1] = (
			{
				title: "Vessel Estimated Length",
				symbologies: [
					{
						class: "optical-100-999",
						label: "100m < length < 999m",
						style: "fill: #f00;",
						icon: "vessel-iala"
					},
					{
						class: "optical-50-100",
						label: "50m < length < 100m",
						style: "fill: #ffa500;",
						icon: "vessel-iala"
					},
					{
						class: "optical-20-50",
						label: "20m < length < 50m",
						style: "fill: #ff0;",
						icon: "vessel-iala"
					},
					{
						class: "optical-15-20",
						label: "15m < length < 20m",
						style: "fill: #00f;",
						icon: "vessel-iala"
					},
					{
						class: "optical-10-15",
						label: "10m < length < 15m",
						style: "fill: #f10000;",
						icon: "vessel-iala"
					},
					{
						class: "optical-0-10",
						label: "length < 10m",
						style: "fill: #f0f;",
						icon: "vessel-iala"
					}
				]
			}
		);
	symbologies["Correlation And Heading And Class"][2] = temp;

	return symbologies;
};