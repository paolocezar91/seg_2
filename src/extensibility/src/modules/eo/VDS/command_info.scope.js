const {
	getLabel,
	DEFAULT_CORRELATED,
	DEFAULT_UNCORRELATED,
	displayVesselInfo,
	displayVDSPositionInfo,
	displayTooltipPreferences,
	getServiceId
} = require("./api.js");

const {getLastKnownPosition, mergeDuplicates} = require("../../vessels/api.js");

module.exports = config => ({
	// Init function called on command and info or tooltip opening.
	// Tries to get clip image from a given VDS
	init(detectedVessel, tooltip = false) {
		this.detectedVesselPicture = detectedVessel.get("thumbnail") || this.detectedVesselDefaultPicture;

		if(!tooltip && detectedVessel.get("shipThumbnail") && getServiceId(detectedVessel.get("CSN_ID")) && !detectedVessel.get("thumbnail"))
			seg.request.getDataURL("v1/eo/clip_image", {
				params: {
					fileName: detectedVessel.get("shipThumbnail"),
					serviceId: getServiceId(detectedVessel.get("CSN_ID"))
				}
			}).then(image => {
				if (image){
					this.detectedVesselPicture = image;
					detectedVessel.set("thumbnail", image);
				} else {
					seg.log.error("ERROR_VDS_INIT_GET_IMAGE", image.status + ": " + image.message);
					seg.log.error("Failed to get VDS image", getLabel(detectedVessel));
				}
			}, e => seg.log.error("ERROR_VDS_INIT_GET_IMAGE", e.error + ": " + e.result));
	},
	// Returns total dimension of a given VDS. See properties file.
	sumCorrelatedIdentityLength(detectedVessel) {
		return (
			Number(detectedVessel.get("CORRELATED_IDENTITY_LENGTH_A")) +
			Number(detectedVessel.get("CORRELATED_IDENTITY_LENGTH_B")) +
			Number(detectedVessel.get("CORRELATED_IDENTITY_LENGTH_C")) +
			Number(detectedVessel.get("CORRELATED_IDENTITY_LENGTH_D"))
		) || "N/A";
	},
	// default VDS C&I picture
	detectedVesselDefaultPicture: "assets/images/novessel.png",
	detectedVesselPicture: "assets/images/novessel.png",
	// default properties to be shown in C&I
	displayPreferences_: config.vds_correlated_fields || DEFAULT_CORRELATED,
	get displayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("DetectedVessel.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayPreferences_[key]) {
				self.displayPreferences_[key].visible = savedPreferences[key];
			}
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayPreferences_;
	},
	displayVesselInfo_: displayVesselInfo,
	get displayVesselInfo() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("DetectedVessel.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayVesselInfo_[key])
				self.displayVesselInfo_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayVesselInfo_;
	},
	displayVDSPositionInfo_: displayVDSPositionInfo,
	get displayVDSPositionInfo() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("DetectedVessel.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayVDSPositionInfo_[key]) {
				self.displayVDSPositionInfo_[key].visible = savedPreferences[key];
			}
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayVDSPositionInfo_;
	},
	displayUncorrelatedPreferences_: config.vds_uncorrelated_fields || DEFAULT_UNCORRELATED,
	get displayUncorrelatedPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("DetectedVessel.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayUncorrelatedPreferences_[key]) {
				self.displayUncorrelatedPreferences_[key].visible = savedPreferences[key];
			}
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayUncorrelatedPreferences_;
	},
	toggleDisplayPreference(key, preferences_) {
		//we only want to save the "visible" property, the rest is always the same
		seg.preferences.workspace.set("DetectedVessel.commandInfo.displayOptions." + key, !this[preferences_][key].visible);
	},
	//private property
	displayTooltipPreferences_: displayTooltipPreferences,
	get tooltipDisplayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("DetectedVessel.tooltip.displayOptions") || {};
		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayTooltipPreferences_[key]) {
				self.displayTooltipPreferences_[key].visible = savedPreferences[key];
			}
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayTooltipPreferences_;
	},
	getLastKnownPosition: (detectedVessel) => {
		if(!detectedVessel.get("vessel_ref")) {
			const vessel_ref = seg.layer.get("positions").getSource().getFeatures().find(vessel => {
				return vessel.get("imo") && detectedVessel.get("CORRELATED_IDENTITY_IMO") && vessel.get("imo") === detectedVessel.get("CORRELATED_IDENTITY_IMO") ||
					vessel.get("mmsi") && detectedVessel.get("CORRELATED_IDENTITY_MMSI") && vessel.get("mmsi") === detectedVessel.get("CORRELATED_IDENTITY_MMSI") ||
					vessel.getId() && detectedVessel.get("CORRELATED_IDENTITY_IMDATE_ID") && vessel.getId() === detectedVessel.get("CORRELATED_IDENTITY_IMDATE_ID");
			});

			if (vessel_ref){
				detectedVessel.set("vessel_ref", vessel_ref);
				seg.selection.selectFeatureAndCenter(vessel_ref);
			} else {
				const modal = seg.modal.openModal("Last position", "Searching last known position of VDS...", {class: "info"});

				getLastKnownPosition({
					imo: detectedVessel.get("CORRELATED_IDENTITY_IMO"),
					mmsi: detectedVessel.get("CORRELATED_IDENTITY_MMSI"),
					id: detectedVessel.get("CORRELATED_IDENTITY_IMDATE_ID")
				}).then((currentPosition) => {
					if(currentPosition.length)
						currentPosition = currentPosition[0];
					// If any position is found, merge it with duplicates, select and center it
					if(currentPosition) {
						mergeDuplicates([currentPosition]).then((mergedVessels) => {
							currentPosition = mergedVessels[0];
							detectedVessel.set("vessel_ref", currentPosition);
							seg.layer.get("positions").getSource().addFeature(currentPosition);
							seg.selection.selectFeatureAndCenter(currentPosition);

							modal.setContent("Last position found");
							seg.modal.closeTimeout(modal.title);
						}, e => seg.log.error("ERROR_VDS_MERGE_DUPLICATES", e.error + ": " + e.result));
					} else {
						modal.setContent("Last position is unknown");
						seg.modal.closeTimeout(modal.title);
					}
				}, e => seg.log.error("ERROR_VDS_GET_LAST_KNOWN_POSITION", e.error + ": " + e.result));
			}
		} else {
			seg.selection.selectFeatureAndCenter(detectedVessel.get("vessel_ref"));
		}
	},
	getLabel,
	getServiceId
});