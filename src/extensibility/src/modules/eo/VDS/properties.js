const defaultProperties = {
	vds_label: {
		order: -1,
		label: "VDS ID",
		template: "{{::detectedVesselsCommandInfo.getLabel(detectedVessel) || \"N/A\"}}",
		visible: true
	},
	longitude: {
		order: 0,
		label: "Longitude (Center)",
		template: "<span coordinate=\"::[detectedVessel.get('lon'), 'lon']\"></span>",
		visible: true
	},
	latitude: {
		order: 1,
		label: "Latitude (Center)",
		template: "<span coordinate=\"::[detectedVessel.get('lat'), 'lat']\"></span>",
		visible: true
	},
	timestamp: {
		order: 2,
		label: "Timestamp",
		template: "<span position-timestamp=\"::detectedVessel.get('VDS_TIMESTAMP')\"></span>",
		visible: true
	},
	estimatedSpeed: {
		order: 3,
		label: "Estimated Speed (KNOTS)",
		template: "{{::detectedVessel.get('CORRELATED_POSITION_SPEED')?detectedVessel.get('CORRELATED_POSITION_SPEED'):\"N/A\"}}",
		visible: true
	},
	estimatedHeading: {
		order: 4,
		label: "Estimated Heading",
		template: "{{::detectedVessel.get('CORRELATED_POSITION_HEADING')?detectedVessel.get('CORRELATED_POSITION_HEADING'):\"N/A\"}}",
		visible: true
	},
	insertionTime: {
		order: 5,
		label: "Transmission Time",
		template: "<span position-timestamp=\"::detectedVessel.get('VDS_INSERTION_TIME')\"></span>",
		visible: true
	},
	mmsi: {
		order: 6,
		label: "MMSI",
		template: "{{detectedVessel.get('CORRELATED_IDENTITY_MMSI') || \"N/A\"}}",
		visible: true
	},
	imo: {
		order: 7,
		label: "IMO",
		template: "{{detectedVessel.get('CORRELATED_IDENTITY_IMO') || \"N/A\"}}",
		visible: true
	},
	shipName: {
		order: 8,
		label: "SHIP NAME",
		template: "{{detectedVessel.get('CORRELATED_IDENTITY_SHIP_NAME') || \"N/A\"}}",
		visible: true
	},
	callSign: {
		order: 9,
		label: "Call sign",
		template: "{{detectedVessel.get('CORRELATED_IDENTITY_CALL_SIGN')>=0?detectedVessel.get('CORRELATED_IDENTITY_CALL_SIGN'):\"N/A\"}}",
		visible: true
	},
	estimatedLength: {
		order: 10,
		label: "Estimated length (M)",
		template: "{{::detectedVessel.get('ESTIMATED_LENGTH')?detectedVessel.get('ESTIMATED_LENGTH'):\"N/A\"}}",
		visible: true
	},
	estimatedWidth: {
		order: 11,
		label: "Estimated width (M)",
		template: "{{::detectedVessel.get('ESTIMATED_WIDTH')?detectedVessel.get('ESTIMATED_WIDTH'):\"N/A\"}}",
		visible: true
	},
	confidence: {
		order: 12,
		label: "Confidence Level",
		template: "{{::detectedVessel.get('CONFIDENCE_LEVEL') ? detectedVessel.get('CONFIDENCE_LEVEL') : null || detectedVessel.get('confidenceLevel') ? detectedVessel.get('confidenceLevel') : 'N/A'  }}",
		visible: true
	},
	requestor: {
		order: 13,
		label: "Requestor",
		template: "{{::detectedVessel.get('VDS_REQUESTOR') ? detectedVessel.get('VDS_REQUESTOR') : \"N/A\"}}",
		visible: true
	},
	packageName: {
		order: 14,
		label: "Package Name",
		template: "{{::detectedVessel.get('PACKAGE_NAME') ? detectedVessel.get('PACKAGE_NAME') : \"N/A\"}}",
		visible: true
	},
	correlationResult: {
		order: 15,
		label: "Correlated",
		template: "{{::detectedVessel.get('CORRELATION_RESULT')==='CORRELATED_POSITION'?\"Yes\":\"No\"}}",
		visible: true
	},
	vesselId: {
		order: 16,
		label: "Vessel ID",
		template: "{{::detectedVessel.get('CORRELATED_IDENTITY_IMDATE_ID') || \"N/A\"}}",
		visible: true
	},
	serviceId: {
		order: 17,
		label: "Service ID",
		template: "{{::detectedVesselsCommandInfo.getServiceId(detectedVessel.get('CSN_ID'))}}",
		visible: true
	},
	imageTimestamp: {
		order: 18,
		label: "Image Timestamp",
		template: "<span position-timestamp=\"::detectedVessel.get('IMAGE_TIMESTAMP')\"></span>",
		visible: true
	},
	pk: {
		order: 19,
		label: "PK",
		template: "{{::detectedVessel.get('PK') ? detectedVessel.get('PK') : \"N/A\"}}",
		visible: true
	},
	CSNid: {
		order: 20,
		label: "CSN ID",
		template: "{{::detectedVessel.get('CSN_ID') ? detectedVessel.get('CSN_ID') : \"N/A\"}}",
		visible: true
	},
	centerPosition: {
		order: 21,
		label: "Center Position",
		template: `
			<div>
				<span coordinate="::[detectedVessel.get('lat'), 'lat']"></span>
				<span coordinate="::[detectedVessel.get('lon'), 'lon']"></span>
			</div>`,
		visible: true
	},
	speedClassification:{
		label: "Speed Classification",
		template: "{{::detectedVessel.get('speedClassification') ? detectedVessel.get('speedClassification') : \"N/A\"}}",
		visible: true
	},
	lengthClass:{
		label: "Length Class",
		template: "{{::detectedVessel.get('lengthClass') ? detectedVessel.get('lengthClass') : \"N/A\"}}",
		visible: true
	},
	// FML, I hate spelling error in parameters
	lengthError: {
		label: "Length Error",
		template: "{{::detectedVessel.get('legthError') ? detectedVessel.get('legthError') : \"N/A\"}}",
		visible: false
	},
	widthError: {
		label: "Width Error",
		template: "{{::detectedVessel.get('widthError') ? detectedVessel.get('widthError') : \"N/A\"}}",
		visible: false
	},
	positionAccuracyX:{
		label: "Pos Accuracy X",
		template: "{{::detectedVessel.get('positionAccuracyX') ? detectedVessel.get('positionAccuracyX') : \"N/A\"}}",
		visible: false
	},
	positionAccuracyY:{
		label: "Pos Accuracy Y",
		template: "{{::detectedVessel.get('positionAccuracyY') ? detectedVessel.get('positionAccuracyY') : \"N/A\"}}",
		visible: false
	},
	vesselType:{
		label: "Vessel Type",
		template: "{{::detectedVessel.get('vesselType') ? detectedVessel.get('vesselType') : \"N/A\"}}",
		visible: false
	},
	correlated: {
		order: 22,
		label: "Correlated",
		template: "{{::!detectedVessel.get('uncorrelated')?\"Yes\":\"No\"}}",
		visible: true
	},
	dimensions: {
		order: 23,
		label: "Dimensions",
		template: "{{::detectedVesselsCommandInfo.sumCorrelatedIdentityLength(detectedVessel) || \"N/A\"}}",
		visible: true
	}
};

const mapProperties = (options) => seg.utils.mapItemProperties(defaultProperties, options);

module.exports = mapProperties;