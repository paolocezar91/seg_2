const {getLabel, getDetectedVessels} = require("./api.js");

module.exports = (bboxes, from, to, uncorrelated, correlated, correlated_fields) => {
	seg.ui.cleanables.call("Clear VDS");
	seg.ui.cleanables.call("ACQ");

	return getDetectedVessels({
		uncorrelated,
		correlated,
		from,
		to,
		bbox: bboxes[0].join(","),
		acq: true,
		correlated_fields
	}).then(({owsData}) => {
		if(owsData.length)
			return {
				marker: {
					template: `
					<div class="default-marker" ng-click="selectAndCenter(items)">
						<img src="extensibility/modules/eo/VDS/timeline_icon.png">
						<div style="background-color: #ff0000" id="cluster-badge" ng-if="items.length > 1">{{items.length}}</div>
					</div>`,
					bindings: {
						selectAndCenter(items) {
							seg.selection.selectFeatureAndCenter(items[0].position);
						}
					}
				},
				// TOOLTIP TO BE REMOVED
				tooltip: {
					template: `
					<ul class="default-tooltip">
						<li ng-repeat="item in items">
							<label ng-click="selectAndCenter(item.position)">{{getLabel(item.position)}}</label>
						</li>
					</ul>
					`,
					bindings: {
						getLabel,
						selectAndCenter: seg.selection.selectFeatureAndCenter
					}
				},
				items: owsData.map(position => {
					return {
						timestamp: moment(position.get("VDS_TIMESTAMP")),
						position
					};
				})
			};
	});
};