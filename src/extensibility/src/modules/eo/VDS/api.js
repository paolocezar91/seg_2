let vdsTables = [], eoList = [];
const mapProperties = require("./properties"),
	{buildSymbolizerMenu} = require("./symbolizers/index.js"),
	DEFAULT_CORRELATED = mapProperties({
		override: {
			vds_label: {order: 0},
			imo: {order: 1},
			mmsi: {order: 2},
			callSign: {order: 3},
			shipName: {order: 4},
			correlationResult: {order: 5},
			confidenceLevel: {order: 6},
			centerPosition: {order: 7},
			vesselType: {order: 8},
			estimatedLength: {order: 9},
			estimatedWidth: {order: 10},
			estimatedHeading: {order: 11},
			estimatedSpeed: {order: 12},
			speedClassification: {order: 13},
			lengthClass: {order: 14},
			positionAccuracyX: {order: 15},
			positionAccuracyY: {order: 16},
			confidence: {order: 17},
			lengthError: {order: 18},
			widthError: {order: 19}
		}
	}),
	DEFAULT_UNCORRELATED = mapProperties({
		override: {
			longitude: {order: 0},
			latitude: {order: 1},
			timestamp: {order: 2},
			insertionTime: {order: 3},
			imageTimestamp: {order: 4},
			requestor: {order: 5},
			confidence: {order: 6},
			estimatedLength: {order: 7},
			pk: {order: 8},
			CSNid: {order: 9},
			speedClassification: {order: 10},
			confidenceLevel: {order: 11},
			lengthClass: {order: 12},
			positionAccuracyX: {order: 13},
			positionAccuracyY: {order: 14},
			lengthError: {order: 15},
			widthError: {order: 16},
			vesselType: {order: 17},
			packageName: { label: "Package ID", order: 18 }
		}
	}),
	displayVesselInfo = mapProperties({
		override: {
			mmsi:  false,
			imo: false,
			shipName: false,
			callSign: false,
			dimensions: false
		}
	}),
	displayVDSPositionInfo = mapProperties({
		override: {
			longitude: {
				template: "<span coordinate=\"[detectedVessel.get('CORRELATED_POSITION_LONGITUDE'), 'lon']\"></span>"
			},
			latitude: {
				template: "<span coordinate=\"[detectedVessel.get('CORRELATED_POSITION_LATITUDE'), 'lat']\"></span>"
			},
			timestamp: false
		}
	}),
	displayTooltipPreferences = mapProperties({
		override: {
			timestamp: {correlated: true, uncorrelated: true},
			mmsi: {correlated: true},
			imo: {correlated: true},
			shipName: {correlated: true},
			confidence: {uncorrelated: true},
			requestor: {uncorrelated: true},
			correlated: {correlated: true, uncorrelated: true}
		}
	});


const fetchVDS = (correlated, sar, params) => seg.request.get("v1/eo/wfs/ext/ows", {
	params: Object.assign({
		correlated_position: (correlated?"":"UN")+"CORRELATED_POSITION"
	}, params)
}).then(res => Object.assign(res.result, {
	features: res.result.features.map(feature => {
		const [lon, lat] = feature.geometry.coordinates;

		return Object.assign(feature, {
			properties: Object.assign(feature.properties, {
				lon,
				lat,
				sar,
				[(correlated?"":"un") + "correlated"]: true
			})
		});
	})
}), e => seg.log.error("ERROR_VDS_FETCH_VDS", e.error + ": " + e.result));

const getLabel = detectedVessel => {
	const CSN_ID = detectedVessel.get("CSN_ID").split("_");
	return "VDS_" + CSN_ID[0] + "_" + Number(CSN_ID[CSN_ID.length - 1]).pad(3);
};

const getDetectedVessels = config => {
	let vdsTableFields, vdsTable;
	const loaderModal = {},
		detectedVesselsSource = seg.layer.get("detected_vessels").getSource();

	if(config.correlated_fields){
		vdsTableFields = Object.assign({}, config.correlated_fields, {
			vds_label: {
				order: 0,
				label: "VDS ID",
				template: "{{getLabel(detectedVessel) || \"N/A\"}}",
				visible: true
			},
			serviceId: {
				order: 17,
				label: "Service ID",
				template: "{{getServiceId(detectedVessel.get('CSN_ID'))}}",
				visible: true
			}
		});
	} else {
		vdsTableFields = mapProperties({
			override: {
				timestamp: {order: 0},
				latitude: {order: 1},
				longitude: {order: 2},
				correlated: {order: 3, visible: true},
				mmsi: {order: 4},
				imo: {order: 5},
				shipName: {order: 6}
			}
		});
	}

	const params = {};

	if (config.eo){
		params.serviceId = config.eo.get("serviceID");
		eoList.push(config.eo);
	} else if (config.bbox)
		Object.assign(params, {
			bbox: config.bbox,
			startDateTime: config.from.toISOString(),
			endDateTime: config.to.toISOString()
		});

	const addVDSToFeatures = (results, title) => {
		if(!results)
			return [];

		const detectedVesselsFeatures = (new ol.format.GeoJSON()).readFeatures(results, {featureProjection: seg.map.getProjection()});
		detectedVesselsSource.addFeatures(detectedVesselsFeatures);

		if (!detectedVesselsFeatures.length) {
			loaderModal[title].setContent("No Vessels Detected");
			seg.modal.closeTimeout(loaderModal[title].title);
			seg.ui.ttt.tables.close("VDS Positions");
		} else {
			if(config.acq)
				seg.ui.queryPanel.ACQ.addFeatures(detectedVesselsFeatures);

			if (!vdsTable) {
				vdsTable = seg.ui.ttt.tables.open({
					customToolbar: {
						template: "<span class=\"text-size-sm\">Number of detected vessels: <b>{{grid.filteredResults.length}}</b></span>"
					},
					label: config.eo ? config.eo.get("serviceID") + " VDS Positions" : "ACQ VDS Positions",
					data: detectedVesselsFeatures,
					itemAs: "detectedVessel",
					sortKey: "vds_label",
					sortKeyOrder: "asc",
					gridAPI: {
						getTimestamp(detectedVessel) {
							return detectedVessel.get("CORRELATED_POSITION_TIMESTAMP") || detectedVessel.get("VDS_TIMESTAMP");
						},
						getLabel,
						getServiceId
					},
					fields: vdsTableFields,
					onSelectionChange(changedRows, selectedRows) {
						seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
					}
				});

				vdsTables.push(vdsTable);

				seg.ui.cleanables.add("Clear VDS", () => {
					vdsTables.forEach(seg.ui.ttt.tables.close);
					vdsTables = [];

					detectedVesselsSource.getFeatures().forEach(ft => detectedVesselsSource.removeFeature(ft));
					if (eoList.length){
						eoList.forEach(eo => eo.unset("detectedVessels"));
						eoList = [];
					}

					seg.selection.select([]);
				});

			} else {
				seg.ui.ttt.tables.addDataToTable(vdsTable, detectedVesselsFeatures);
			}
		}

		if (config.eo)
			config.eo.set("detectedVessels", [].concat(config.eo.get("detectedVessels"), ...detectedVesselsFeatures).filter(vds => vds));

		return detectedVesselsFeatures;
	};

	if (config.correlated)
		loaderModal.correlated = seg.modal.openModal("Correlated","Retrieving Correlated detected vessels...", {class: "info"});

	if (config.uncorrelated)
		loaderModal.uncorrelated = seg.modal.openModal("Uncorrelated","Retrieving Uncorrelated detected vessels...", {class: "info"});

	const VDSRequests = {
		correlated: null,
		uncorrelated: null,
		eodcData: null
	};

	if (config.correlated)
		VDSRequests.correlated = fetchVDS(true, config.sar, params);

	if (config.uncorrelated)
		VDSRequests.uncorrelated = fetchVDS(false, config.sar, params);

	if (config.eo){
		VDSRequests.eodcData = seg.request.get("v1/eo/wfs/vds", {
			params: {
				eoId: config.eo.get("doi")
			}
		});
	}

	return seg.promise.all(VDSRequests).then(({correlated, uncorrelated, eodcData}) => {
		return seg.promise.all({
			correlatedFeatures: addVDSToFeatures(correlated, "correlated"),
			uncorrelatedFeatures: addVDSToFeatures(uncorrelated, "uncorrelated")
		}).then(({correlatedFeatures, uncorrelatedFeatures}) => {
			const owsData = [].concat(correlatedFeatures, uncorrelatedFeatures);

			if (loaderModal.correlated){
				if(correlatedFeatures.length)
					loaderModal.correlated.setContent("Succesfully retrieved correlated VDS");
				else
					loaderModal.correlated.setContent("No correlated VDS found");

				seg.modal.closeTimeout(loaderModal.correlated.title);
			}

			if (loaderModal.uncorrelated){
				if(uncorrelatedFeatures.length)
					loaderModal.uncorrelated.setContent("Succesfully retrieved uncorrelated VDS");
				else
					loaderModal.uncorrelated.setContent("No uncorrelated VDS found");
				seg.modal.closeTimeout(loaderModal.uncorrelated.title);
			}

			return {
				owsData,
				eodcData: eodcData ? eodcData.result.features : null
			};
		});
	}, () => {
		loaderModal.correlated.setContent("Failure getting correlated VDS");
		seg.modal.closeTimeout(loaderModal.correlated.title);
		loaderModal.uncorrelated.setContent("Failure getting uncorrelated VDS");
		seg.modal.closeTimeout(loaderModal.uncorrelated.title);
	});
};

/**
 *  buildVDSMenu
 *	index is to get SAR(0) or Optical(1) in vdsfilters object
 *	vdsFilters[index].options[0]; is a filter for Correlated VDS
 *	vdsFilters[index].options[1]; is a filter for Uncorrelated VDS
 *	vdsFilters[index].options[2]; is a filter for All VDS
 *	so we build a menu with option 2 being the father, and 0 and 1 as children
 */

const buildVDSMenu = (info, index) => {
	const vdsFilters = seg.ui.layerMenu.fromFilters(seg.layer.get("detected_vessels"))[index],
		vdsSymbolizers = buildSymbolizerMenu(["Correlation And Heading", "Correlation And Heading And Class", "Correlation And Heading And Type"]),
		vdsMenuObject = vdsFilters.options[2],
		newVdsMenuObject = Object.assign({}, vdsMenuObject, {
			options: vdsMenuObject.options.concat(vdsSymbolizers, vdsFilters.options[0], vdsFilters.options[1]),
			alias: "VDS",
			template: "<label><span class=\"flex-1\">VDS</span></label>",
			info: info,
			isChecked() {
				//Since a menu element without checked option exists (simbolizers) this will filter it
				const filteredOptions = newVdsMenuObject.options.filter(option => option.isChecked);
				return newVdsMenuObject.bindings.filter.getActive() && filteredOptions.every(option => option.isChecked()) ? true : undefined;
			},
			isPartial() {
				//Since a menu element without checked option exists (simbolizers) this will filter it
				const filteredOptions = newVdsMenuObject.options.filter(option => option.isChecked);
				return newVdsMenuObject.bindings.filter.getActive() && filteredOptions.some(option => !option.isChecked()) && filteredOptions.some(option => option.isChecked()) ? true : undefined;
			}
		});

	return newVdsMenuObject;
};

const getServiceId = csnId => csnId.substr(0, csnId.indexOf("_"));

module.exports = {
	getLabel,
	getServiceId,
	getDetectedVessels,
	DEFAULT_CORRELATED,
	DEFAULT_UNCORRELATED,
	displayVesselInfo,
	displayVDSPositionInfo,
	displayTooltipPreferences,
	buildVDSMenu
};
