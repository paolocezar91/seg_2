/**
@namespace extensibility.modules.eo
@description
# Earth Observation

## Available options
The following options are all booleans. Omitting an option is the same as having a **false** value:
- **eo_acq** - EO Area Centric Query functionality
- **oil_spills** - Oil Spills layers and functionality
- **eo_show_footprints** - Open Recent EO products in TTT by default
- **eo_show_oilspills** - Open Recent Oil Spills in TTT by default
- **oil_spills_report_in_ssn** - SSN Report functionality
- **footprints_alert_report** - Alert report functionality
- **oil_spills_feedback** - Oil Spills feedback form (Requires {@link module:feedback Feedback module})
- **footprints_activity_detection** - Activity detection functionality
- **footprints_download_eo** - EO image downloading
- **sentinel** - External EO app
- **vds** - VDS functionality
- **oil_spills_slick_info** - Enable slick info on Oil Spill C&I

## Global configurations and constants
None
*/
let footprintsLayer,
	eoDataProductsLayer,
	eoMenu,
	config;

const OILSPILLS_DATE_RANGE = 3,
	{ DEFAULT_EO_TABLE_FIELDS, shouldHaveImage, buildFootprintsFilterMenu, imageLayerExists, showImageLayer, setPreferences, setPreferencesChildren, getCoordinates } = require("./SAR/footprints/api"),
	{ DEFAULT_OILSPILLS_TABLE_FIELDS, getLabel, getOperations } = require("./SAR/oil_spills/api"),
	{ buildFootprintsLayer, updateFootprintsLayer } = require("./SAR/footprints/layer"),
	{ buildOilSpillsLayer } = require("./SAR/oil_spills/layer"),
	{ buildFootprintsSearch	} = require("./SAR/footprints/search"),
	{ buildOilspillsSearch } = require("./SAR/oil_spills/search"),
	{ buildSymbolizerMenu, getSelected } = require("./SAR/oil_spills/symbolizers"),
	{ buildVDSMenu } = require("./VDS/api"),
	moduleResponse = {};


const lazyLoaderFilters = async () => {
	// if (!eoDataProductsLayer.get("loading_filters")) {
	// 	eoDataProductsLayer.set("loading_filters", true);

	// 	seg.layer.filter.setFilters(footprintsLayer, await updateFootprintsLayer(config));

	// 	if (config.join_optical_and_sar){
	// 		const sarOpticalMenuItem = seg.ui.layerMenu.fromFilters(footprintsLayer);
	// 		sarOpticalMenuItem.forEach((opt) => setPreferencesChildren(opt));
	// 		eoMenu.options = [].concat(...sarOpticalMenuItem);
	// 	} else {
	// 		const [sarMenuItem, opticalMenuItem] = buildFootprintsFilterMenu(footprintsLayer);
	// 		setPreferences(sarMenuItem);
	// 		setPreferences(opticalMenuItem);
	// 		eoMenu.options.push(sarMenuItem, opticalMenuItem);
	// 	}
	// }
};

const lazyLoaderOilspills = async (force = false) => {
	if(!force &&
		!eoDataProductsLayer.getVisible() &&
		!seg.preferences.workspace.get("show.oilSpills")
	)
		return;

	if (!eoDataProductsLayer.get("loading_oilspills")) {
		const modal = seg.modal.openModal("Loading Oilspills Data", "Loading Oilspills data...");
		const moduleRequests = {};

		if (config.oil_spills && !eoDataProductsLayer.get("loading_oilspills") && (force || eoDataProductsLayer.getVisible() || seg.preferences.workspace.get("show.oilSpills"))){
			eoDataProductsLayer.set("loading_oilspills", true);
			moduleRequests.oilspills = seg.request.get("v1/eo/wfs", {
				params: {
					begin_position: moment().subtract(OILSPILLS_DATE_RANGE,"days").startOf("day").toISOString(),
					end_position: moment().toISOString()
				}
			}).subscribe((oilspills) => {
				if (!oilspills.result.features.length){
					modal.setContent("<span class=\"retrieved-no-result\"><b>0</b></span> Oilspills returned.");
					seg.modal.closeTimeout(modal.title);
					return;
				}

				moduleResponse.oilspills(oilspills);
				modal.setContent("Oilspills data <span class=\"retrieved-success\"><b>loaded</b></span>.");
				seg.modal.closeTimeout(modal.title);
			}, (e) => {
				seg.log.error("ERROR_EO_LAZY_LOADER_OILSPILLS", e.error + ": " + e.result);
				modal.setContent("<span class=\"retrieved-error\">Error loading Oilspills: " + (e.message || e.result) + "</span>");
				seg.modal.closeTimeout(modal.title);
			});
		}
	}
};

const lazyLoaderEO = async (force = false) => {
	if(!force &&
		!eoDataProductsLayer.getVisible() &&
		!seg.preferences.workspace.get("show.EO")
	)
		return;

	// Requesting data from webservices and executing responses defined on init()
	if (!eoDataProductsLayer.get("loading_eo")) {
		const modal = seg.modal.openModal("Loading EO Data", "Loading EO Data products...");

		if (!eoDataProductsLayer.get("loading_eo") && (force || eoDataProductsLayer.getVisible() || seg.preferences.workspace.get("show.EO"))){
			eoDataProductsLayer.set("loading_eo", true);
			seg.request.get("v1/eo/catalogue").subscribe((footprints) => {
					if (!footprints.result.features.length){
						modal.setContent("<span class=\"retrieved-no-result\"><b>0</b></span> EOs returned.");
						seg.modal.closeTimeout(modal.title);
						return;
					}

					moduleResponse.footprints(footprints);
					modal.setContent("EO data <span class=\"retrieved-success\"><b>loaded</b></span>.");
					seg.modal.closeTimeout(modal.title);
				}, (e) => {
					seg.log.error("ERROR_EO_LAZY_LOADER_EO", e.error + ": " + e.result);
					modal.setContent("<span class=\"retrieved-error\">Error loading EOs: " + (e.message || e.result) + "</span>");
					seg.modal.closeTimeout(modal.title);
				});
		}


	}
};

module.exports = (_config, permissions) => {
	// setting up config and layers that should be added
	config = _config;
	config.vds_reprocessing = permissions.footprints.vds_reprocessing;
	const layers = [];

	// Layer that will house all sar products, like winds and swell layers, also oilspills
	const eoSarProductsLayer = {
		id: "eo_sar_products",
		name: "EO SAR Products",
		layers: [
			require("./SAR/other/winds_layer.js"),
			require("./SAR/other/swells_layer.js")
		]
	};		

	layers.push(buildFootprintsLayer(config), require("./SAR/other/images_layer.js"), eoSarProductsLayer);

	if (config.oil_spills)
		eoSarProductsLayer.layers.push(buildOilSpillsLayer(config));

	if (config.vds)
		layers.push(require("./VDS/layer.js")(config));

	if (config.footprints_activity_detection)
		layers.push(require("./SAR/activity/layer.js"));

	return {
		name: "Earth Observation Data & Products",
		layers: [{
			id: "eo_data_products",
			order: 7,
			name: "Earth Observation Data & Products",
			layers,
			visible: false
		}],
		preferences: {
			"show.oilSpills": config.eo_show_oilspills,
			"show.EO": config.eo_show_footprints
		},
		init() {
			// Check permissions to see oilspills in feedback
			if (!permissions.oilspills.feedback.available)
				config.oil_spills_feedback = false;

			eoDataProductsLayer = seg.layer.get("eo_data_products");
			eoDataProductsLayer.ol_.on("change:visible", () => {
				lazyLoaderFilters();
				lazyLoaderEO();
				if (config.oil_spills)
					lazyLoaderOilspills();
			});

			footprintsLayer = seg.layer.get("eo_sar_imagery");
			const footprintsSource = footprintsLayer.getSource();

			//FootPrints preference creation
			//add preferences options
			// seg.ui.preferences.addSectionToTab("Show", {
			// 	alias: "eoShow",
			// 	template: `
			// 		<div class="row" ng-if="eoShow.oilSpills">
			// 			<label class="text-size-sm flex-1">Recent detected oil spills</label>
			// 			<toggle ng-model="eoShow.oilSpill" ng-model-options="{getterSetter: true}"></toggle>
			// 		</div>
			// 		<div class="row">
			// 			<label class="text-size-sm flex-1">Recently added EO products</label>
			// 			<toggle ng-model="eoShow.EO" ng-model-options="{getterSetter: true}"></toggle>
			// 		</div>`,
			// 	bindings: {
			// 		oilSpill(val) {
			// 			if (typeof val === "undefined")
			// 				return seg.preferences.workspace.get("show.oilSpills");

			// 			if (!val)
			// 				seg.ui.ttt.tables.close("Oil Spills");
			// 			else {
			// 				lazyLoaderFilters();
			// 				lazyLoaderOilspills(true).then(() => seg.ui.ttt.tables.open("Oil Spills"));
			// 			}

			// 			seg.preferences.workspace.set("show.oilSpills", val);
			// 		},
			// 		EO(val) {
			// 			if (typeof val === "undefined")
			// 				return seg.preferences.workspace.get("show.EO");

			// 			if (!val)
			// 				seg.ui.ttt.tables.close("EO Acquisitions");
			// 			else {
			// 				lazyLoaderFilters();
			// 				lazyLoaderEO(true).then(() => seg.ui.ttt.tables.open("EO Acquisitions"));
			// 			}

			// 			seg.preferences.workspace.set("show.EO", val);
			// 		},
			// 		oilSpills: config.oil_spills
			// 	}
			// });

			// // Creating and registering EO menus
			// const eoSarProductsMenu = seg.ui.layerMenu.fromLayer("EO SAR Products", seg.layer.get("eo_sar_products"));
			// const eoOpticalProductsMenu = Object.assign({}, seg.ui.layerMenu.fromLayer("EO Optical Products", seg.layer.get("eo_sar_products")), {
			// 	alias: "EO Optical Products",
			// 	isChecked: () => (eoOpticalProductsMenu.bindings.active && !eoOpticalProductsMenu.isPartial()) ? true : undefined,
			// 	isPartial: () => (eoOpticalProductsMenu.bindings.active && eoOpticalProductsMenu.options.some((option) => !option.isChecked())) ? true : undefined,
			// 	toggle: (state) => {
			// 		eoOpticalProductsMenu.bindings.active = state === undefined ? !eoOpticalProductsMenu.bindings.active : state;
			// 		//since smoke and mirrors were used for this item
			// 		const VDSLayer = seg.layer.get("detected_vessels");

			// 		if (VDSLayer)
			// 			VDSLayer.get("filters")[1].options[3].active = state === undefined ? !VDSLayer.get("filters")[1].options[3].active : state;
			// 	},
			// 	bindings: {
			// 		alias: "EO Optical Products",
			// 		active: true
			// 	},
			// 	options: [],
			// 	template: "<label><span class=\"flex-1\">{{alias}}</span></label>"
			// });

			// // Menus for activity detection
			// if (config.footprints_activity_detection) {
			// 	const activityLayer = seg.layer.get("eo_detected_activity"),
			// 		activityFilters = seg.ui.layerMenu.fromFilters(activityLayer);

			// 	activityFilters[0].options[0].template = "<label><span class=\"flex-1\">Activity Detection</span></label>";
			// 	eoSarProductsMenu.options.push(activityFilters[0].options[0]);

			// 	activityFilters[0].options[1].template = "<label><span class=\"flex-1\">Activity Detection</span></label>";
			// 	eoOpticalProductsMenu.options.push(activityFilters[0].options[1]);
			// }

			// // VDS layers, menu and config
			// if (config.vds) {
			// 	const {sar, optical} = require("./VDS/symbolizers"),
			// 		symbologies = require("./VDS/symbologies"),
			// 		templateUrl = resolvePath("./VDS/templates/symbologies.html"),
			// 		infoSAR = Object.assign({
			// 			title: "SAR VDS Symbology",
			// 			bindings: {sections: Object.assign(sar, symbologies(true))},
			// 			templateUrl
			// 		}),
			// 		infoOptical = Object.assign({
			// 			title: "Optical VDS Symbology",
			// 			bindings: {sections: Object.assign(optical, symbologies(false))},
			// 			templateUrl
			// 		});

			// 	eoSarProductsMenu.options.push(buildVDSMenu(infoSAR, 0));
			// 	eoOpticalProductsMenu.options.push(buildVDSMenu(infoOptical, 1));

			// 	// set vds as a preferential layer to avoid problems when clicking on the same area as EO or other features
			// 	seg.map.registerPreferentialLayers("detected_vessels");
			// }

			// // creates menu from layer
			// eoMenu = seg.ui.layerMenu.fromLayer("EO DATA", eoDataProductsLayer);
			// if(config.footprints_menu_tooltip)
			// 	eoMenu.info = config.footprints_menu_tooltip;

			// if (!config.join_optical_and_sar) {
			// 	eoMenu.options.push(
			// 		eoSarProductsMenu,
			// 		eoOpticalProductsMenu
			// 	);
			// }
			// seg.ui.layerMenu.register(eoMenu);

			// // requiring label preferences for EOs
			// require("./SAR/footprints/label").register();

			// //register ACQ
			// if(config.eo_acq || config.vds || config.oil_spills)
			// 	seg.ui.queryPanel.ACQ.register(require("./acq.js")(config));

			// // register advanced search
			// seg.ui.searchAdvanced.addPanel(buildFootprintsSearch(config));

			// seg.ui.ttt.gallery.add({
			// 	label: "EO Acquisitions",
			// 	persistent: true,
			// 	scope: {
			// 		select(eo) {
			// 			seg.selection.selectFeatureAndCenter(eo);

			// 			if (imageLayerExists(eo))
			// 				showImageLayer(eo);
			// 		}
			// 	},
			// 	itemAs: "EO",
			// 	items: [],
			// 	onOpen: () => {
			// 		const requests = [],
			// 			galleryModal = seg.modal.openModal("galleryModal", "Loading EO images in Gallery...");

			// 		lazyLoaderEO(true).then(() => {
			// 			seg.ui.ttt.gallery.get("EO Acquisitions")[0].items = footprintsSource.getFeatures();

			// 			const features = footprintsSource.getFeatures();

			// 			features.forEach(feature => {
			// 				if (!feature.get("thumbnail") && feature.get("status") && shouldHaveImage(feature)) {
			// 					const transformedExtent = ol.proj.transformExtent(feature.getGeometry().getExtent(), seg.map.getProjection(), "EPSG:4326");

			// 					requests.push(seg.request.getDataURL("v1/eo/wms/image", {
			// 						params: {
			// 							FORMAT: "image/png",
			// 							BBOX: transformedExtent.join(","),
			// 							Request: "GetMap",
			// 							WIDTH: 300,
			// 							HEIGHT: 300,
			// 							TRANSPARENT: true,
			// 							LAYERS: feature.get("doi"),
			// 							SERVICE: "WMS",
			// 							VERSION: "1.1.0",
			// 							SRS: "EPSG:4326",
			// 							STYLES: ""
			// 						},
			// 						headers: {
			// 							"Content-Type": "image/png"
			// 						}
			// 					}).then(thumbnail => feature.set("thumbnail", thumbnail), () => {}), e => seg.log.error("ERROR_EO_ON_OPEN_THUMBNAIL", e.error + ": " + e.result));
			// 				}
			// 			});

			// 			seg.promise.all(requests).then(() => {
			// 				galleryModal.setContent("EO images loaded in Gallery.");
			// 				seg.modal.closeTimeout(galleryModal.title);
			// 			});
			// 		});
			// 	},
			// 	templateUrl: resolvePath("./templates/gallery_item.html")
			// });

			const EOTTTFields = config.footprints_table_fields || DEFAULT_EO_TABLE_FIELDS,
				eoTable = seg.ui.ttt.tables.register({
					customToolbar: {
						template: "<span class=\"text-size-sm\">Number of EO Acquisitions: <b>{{grid.filteredResults.length}}</b></span>"
					},
					label: "EO Acquisitions",
					data: [],
					itemAs: "EO",
					gridAPI: {
						getDistanceUnits:() => seg.preferences.workspace.get("distanceUnits"),
						getCoordinates
					},
					paginate: true,
					fields: EOTTTFields,
					onSelectionChange(changedRows, selectedRows) {
						seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
					},
					onOpen: () => {
						lazyLoaderFilters();
						lazyLoaderEO(true);
					}
				});

			// Creates response for EO catalogue request that may or not be done on lazyLoader
			moduleResponse.footprints = ({result}) => {
				if (!result)
					return [];

				result.crs.properties.name = result.crs.properties.name.toUpperCase();

				const footprints = (new ol.format.GeoJSON()).readFeatures(result, {
					featureProjection: seg.map.getProjection()
				});

				footprints.forEach(footprint => {
					const preferenceKey = "layers."+ footprintsLayer.get("id") + ".filters." + (footprint.get("sar") ? "SAR" : "Optical") + " Imagery.enabled";

					const filename = footprint.get("filename"),
						operations = footprint.get("operations");

					footprint.setProperties({
						operations: operations ? operations.split(",").filter((value, index, self) => self.indexOf(value) === index).join(", ") : "",
						totalOilSpills: footprint.get("oilSpills") || 0,
						visible: config.join_optical_and_sar || seg.preferences.map.get(preferenceKey),
						filename: filename ? filename.replace(/.*filename=/, "") : undefined,
						oilSpills: undefined
					});
				});

				footprintsSource.addFeatures(footprints);
				seg.ui.ttt.tables.addDataToTable(eoTable, footprints);

				if (seg.preferences.workspace.get("show.EO"))
					seg.ui.ttt.tables.open("EO Acquisitions");

				seg.shortcuts.add("eo_sar_imagery", [{
					label: "See in TTT",
					callback(retrievedEO) {
						let eoSelectedTable = seg.ui.ttt.tables.get("Selected EO Acquisitions");

						if(!eoSelectedTable)
							eoSelectedTable = {
								label: "Selected EO Acquisitions",
								customToolbar: {
									template: "<span class=\"text-size-sm\">Number of EO Acquisitions: <b>{{grid.filteredResults.length}}</b></span>"
								},
								data: [],
								itemAs: "EO",
								fields: EOTTTFields
							};

						seg.ui.ttt.tables.addDataToTable(eoSelectedTable, retrievedEO);
						seg.ui.ttt.tables.open(eoSelectedTable);
					}
				}]);
			};

			// Oil spills config
			// if (config.oil_spills) {
			// 	const oilspillsLayer = seg.layer.get("oil_spills"),
			// 		oilspillsMenu = seg.ui.layerMenu.fromLayer("Oil Spills", oilspillsLayer),
			// 		oilspillsOptions = seg.ui.layerMenu.fromFilters(oilspillsLayer)/*.reduce((acc,elem) =>[...acc,...elem.options],[])*/;

			// 	// oilspills menu
			// 	Object.assign(oilspillsMenu, {
			// 		options: [...oilspillsMenu.options, ...oilspillsOptions],
			// 		info: {
			// 			title: "Oil Spills Symbology",
			// 			templateUrl: resolvePath("./SAR/oil_spills/templates/menu_tooltip.html"),
			// 			bindings: {
			// 				name: "Oil Spills Symbology",
			// 				selectedSymbology: function() {
			// 					return getSelected().name;
			// 				}
			// 			}
			// 		}
			// 	});

			// 	if(config.oil_spills_symbolizers)
			// 		oilspillsMenu.options.unshift(buildSymbolizerMenu(config.oil_spills_symbolizers));

			// 	eoSarProductsMenu.options.push(oilspillsMenu);
			// 	seg.ui.searchAdvanced.addPanel(buildOilspillsSearch(config));

			// 	// TTT registering
			// 	const oilspillsTTTFields = config.oil_spills_table_fields || DEFAULT_OILSPILLS_TABLE_FIELDS,
			// 		oilSpillsTable = seg.ui.ttt.tables.register({
			// 			customToolbar: {
			// 				template: "<span class=\"text-size-sm\">Number of Oilspills: <b>{{grid.filteredResults.length}}</b></span>"
			// 			},
			// 			gridAPI: {
			// 				getAreaUnits: () => seg.preferences.workspace.get("areaUnits"),
			// 				getDistanceUnits: () => seg.preferences.workspace.get("distanceUnits"),
			// 				getLabel: (oilspill) => getLabel(oilspill),
			// 				getOperations
			// 			},
			// 			label: "Oil Spills",
			// 			data: [],
			// 			itemAs: "oilSpill",
			// 			fields: oilspillsTTTFields,
			// 			onSelectionChange(changedRows, selectedRows) {
			// 				seg.selection.selectFeatureAndCenter(selectedRows.map(row => {
			// 					row.item.set("visible", true);
			// 					return row.item;
			// 				}), undefined, seg.map.getView().getResolution());
			// 			},
			// 			onDoubleClick(changedRows, selectedRows) {
			// 				seg.selection.selectFeatureAndCenter(selectedRows.map(row => {
			// 					row.item.set("visible", true);
			// 					return row.item;
			// 				}), undefined, seg.map.getView().getResolution());
			// 			},
			// 			onOpen: () => {
			// 				lazyLoaderFilters();
			// 				lazyLoaderOilspills(true);
			// 			}
			// 		});

			// 	// creating oilspills module response
			// 	moduleResponse.oilspills = ({result}) => {
			// 		if (!result)
			// 			return [];

			// 		const oilSpills = (new ol.format.GeoJSON()).readFeatures(result, {featureProjection: seg.map.getProjection()});

			// 		oilSpills.forEach(oilSpill => {
			// 			const timestamp = oilSpill.get("timeStamp");

			// 			if (timestamp && timestamp.year)
			// 				oilSpill.set("timeStamp", moment({
			// 					year: timestamp.year,
			// 					month: timestamp.monthValue,
			// 					day: timestamp.dayOfMonth,
			// 					hour: timestamp.hour,
			// 					minute: timestamp.minute,
			// 					second: timestamp.second
			// 				}).toISOString());

			// 			oilSpill.setProperties({"id": oilSpill.getId(), "numberOfSlicks": oilSpill.getGeometry().getPolygons().length, "label": getLabel(oilSpill)});

			// 			if (oilSpill.getGeometry().getCoordinates().every(coord => coord === 0))
			// 				oilSpill.getGeometry().setCoordinates(oilSpill.get("center").split(" "));
			// 		});

			// 		const oilspillsSource = oilspillsLayer.getSource();
			// 		oilspillsSource.addFeatures(oilSpills);

			// 		seg.shortcuts.add("oil_spills", [{
			// 			label: "See in TTT",
			// 			callback: function (retrievedOilspills) {
			// 				let oilSpillsSelectedTable = seg.ui.ttt.tables.get("Selected Oilspills");

			// 				if(!oilSpillsSelectedTable)
			// 					oilSpillsSelectedTable = {
			// 						label: "Selected Oilspills",
			// 						customToolbar: {
			// 							template: "<span class=\"text-size-sm\">Number of Oilspills: <b>{{grid.filteredResults.length}}</b></span>"
			// 						},
			// 						data: [],
			// 						itemAs: "oilSpill",
			// 						fields: oilspillsTTTFields
			// 					};

			// 				seg.ui.ttt.tables.addDataToTable(oilSpillsSelectedTable, retrievedOilspills);
			// 				seg.ui.ttt.tables.open(oilSpillsSelectedTable);
			// 			}
			// 		}]);

			// 		seg.ui.ttt.tables.addDataToTable(oilSpillsTable, oilSpills);

			// 		if (seg.preferences.workspace.get("show.oilSpills"))
			// 			seg.ui.ttt.tables.open(oilSpillsTable);
			// 	};

			// 	seg.map.registerPreferentialLayers("oil_spills");
			// 	lazyLoaderOilspills();
			// }

			lazyLoaderFilters();
			lazyLoaderEO();
		}
	};
};
