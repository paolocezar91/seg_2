const styleCache = {};

const getActivityIconSize = () => Math.min(25, Math.max(5, Math.round(5500000/seg.map.getScale()) * 12));

const selectedStyle = new ol.style.Style({
	fill: new ol.style.Fill({
		color: "rgba(255,0,0,0.5)"
	}),
	stroke: new ol.style.Stroke({
		color: "#f00",
		width: 1
	})
});

module.exports = feature => {
	if(feature.get("selected"))
		return selectedStyle;

	const height = getActivityIconSize();

	if(!styleCache[height])
		styleCache[height] = {
			image: new seg.style.SVG({
				svg: require("./activity.svg"),
				height,
				fill: "#F00",
				stroke: {
					color: "white",
					width: 1
				}
			})
		};

	return new ol.style.Style(Object.assign({}, styleCache[height], {geometry: new ol.geom.Point(ol.extent.getCenter(feature.getGeometry().getExtent()))}));
};
