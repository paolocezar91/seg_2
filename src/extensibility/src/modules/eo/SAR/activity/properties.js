const defaultProperties = {
	serviceId: {
		label: "Service ID",
		template: "{{detectedActivity.get('serviceId') ? detectedActivity.get('serviceId') : 'N/A'}}",
		visible: true,
		order: 0
	},
	id: {
		label: "Activity identifier",
		template: "{{detectedActivityCommandInfo.getLabel(detectedActivity)}}",
		visible: true,
		order: 1
	},
	latitude: {
		label: "Latitude (center)",
		template: "<span coordinate='[detectedActivity.get(\"lat\"), \"lat\"]'></span>",
		visible: true,
		order: 2
	},
	longitude: {
		label: "Longitude (center)",
		template: "<span coordinate='[detectedActivity.get(\"lon\"), \"lon\"]'></span>",
		visible: true,
		order: 3
	},
	timeStamp: {
		label: "Acquisition start",
		template: "<span position-timestamp='detectedActivity.get(\"timeStamp\")'></span>",
		visible: true,
		order: 4
	},
	activityType: {
		label: "Activity Type",
		template: "{{detectedActivity.get('activityType') ? detectedActivity.get('activityType') : 'N/A'}}",
		visible: true,
		order: 5
	},
	activitySubType: {
		label: "Activity Subyype",
		template: "{{detectedActivity.get('activitySubType') ? detectedActivity.get('activitySubType') : 'N/A'}}",
		visible: true,
		order: 6
	},
	activityDescription: {
		label: "Description",
		template: "{{detectedActivity.get('activityDescription') ? detectedActivity.get('activityDescription') : 'N/A'}}",
		visible: true,
		order: 7
	},
	activityConfidenceLevel: {
		label: "Confidence Level",
		template: "{{detectedActivity.get('activityConfidenceLevel') ? detectedActivity.get('activityConfidenceLevel') : 'N/A'}}",
		visible: true,
		order: 8
	},
	positionAccuracyVectorX: {
		label: "Position Accuracy X",
		template: "{{detectedActivity.get('positionAccuracyVectorX') ? detectedActivity.get('positionAccuracyVectorX') : 'N/A'}}",
		visible: true,
		order: 9
	},
	positionAccuracyVectorY: {
		label: "Position Accuracy Y",
		template: "{{detectedActivity.get('positionAccuracyVectorY') ? detectedActivity.get('positionAccuracyVectorY') : 'N/A'}}",
		visible: true,
		order: 10
	}
};

const mapProperties = (options) => seg.utils.mapItemProperties(defaultProperties, options);

module.exports = mapProperties;