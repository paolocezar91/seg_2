const mapProperties = require("./properties");

module.exports = {
	init(activity) {
		this.activityPicture = activity.get("picture") || "assets/images/noOilSpill.png";

		if (activity.get("activityClipImage") && activity.get("serviceId") && !activity.get("picture"))
			seg.request.getDataURL("v1/eo/clip_image", {
				params: {
					fileName: activity.get("activityClipImage"),
					serviceId: activity.get("serviceId")
				},
				headers: {
					"Content-Type": "image/tiff"
				}
			}).then(res => {
				if (res){
					this.activityPicture = res;
					activity.set("picture", res);
				} else{
					seg.log.error("INFO_ACTIVITY_NO_OILSPILL_PHOTO", res.status + ": " + res.message);
					activity.set("picture", "assets/images/noOilSpill.png");
				}
			}, e => {
				activity.set("picture", "assets/images/noOilSpill.png");
				seg.log.error("INFO_ACTIVITY_NO_OILSPILL_PHOTO", e.error + ": " + e.result);
			});
	},
	centerMap: function (detectedActivity) {
		seg.selection.selectFeatureAndCenter(detectedActivity);
	},
	alertReportDownload: function(feature){
		const modal = seg.modal.openModal("Alert Report", "Downloading Alert Report...", {class: "info"});

		seg.request.download("v1/eo/alert_report/" + feature.getId(), {}, feature.getId() + "_alert_report.pdf").then(() => {
			modal.setContent("Download successful");
			seg.modal.closeTimeout(modal.title);
		}, e => {
			modal.setContent("Download Failed");
			seg.modal.closeTimeout(modal.title);
			seg.log.error("ERROR_ACTIVITY_ALERT_REPORT_DOWNLOAD", e.error + ": " + e.result);
		});
	},
	displayPreferences_: mapProperties(false),
	get displayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("activity.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayPreferences_[key])
				self.displayPreferences_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayPreferences_;
	},
	get activityInfo(){
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("activity.tooltip.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			self.displayActivityInfo_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return self.displayActivityInfo_;
	},
	displayActivityInfo_: mapProperties({
		override: {
			timeStamp: false,
			activityType: false
		}
	}),
	getVesselTracks(feature) {
		seg.selection.select(feature);
		seg.ACQ.open("bboxBuilder", {
			corner1: [feature.get("boundedBy")[0], feature.get("boundedBy")[1]],
			corner2: [feature.get("boundedBy")[2], feature.get("boundedBy")[3]],
			from: moment(feature.get("timeStamp")).subtract(1, "day"),
			to: moment(feature.get("timeStamp"))
		});
	},
	getLabel(detectedActivity){
		const number = detectedActivity.get("activityid").split("_");
		return "ACT_"+ detectedActivity.get("serviceId") + "_" + number[number.length-1];
	}
};
