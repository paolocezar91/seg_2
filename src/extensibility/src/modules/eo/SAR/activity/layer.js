module.exports = {
	id: "eo_detected_activity",
	name: "EO Detected Activity",
	type: "Vector",
	source: {
		name: "Detected Activity",
		type: "Vector"
	},
	style: require("./style.js"),
	commandInfo: {
		featureAs: "detectedActivity",
		bindings: {
			detectedActivityCommandInfo: require("./command_info.scope.js")
		},
		//template url for this C&I
		templateUrl: resolvePath("./templates/command_info.html")
	},
	tooltip: {
		featureAs: "detectedActivity",
		bindings: {
			detectedActivityTooltip: require("./command_info.scope.js")
		},
		templateUrl: resolvePath("./templates/tooltip.html")
	},
	filters: [{
		name: "sar",
		options: {
			"sar": activity => activity.get("sar"),
			"optical": activity => !activity.get("sar")
		}
	}]
};
