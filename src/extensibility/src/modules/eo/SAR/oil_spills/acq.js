const { DEFAULT_OILSPILLS_TABLE_FIELDS, getLabel, getOperations } = require("./api.js");

module.exports = (bboxes, from, to, oil_spills_table_fields) => {
	const bbox = bboxes[0],
		oilSpillsRequest = seg.request.get("v1/eo/wfs", {
			params: angular.extend({
				begin_position: moment(from).toISOString(),
				end_position: moment(to).toISOString(),
				lower_corner: ol.extent.getBottomLeft(bbox).join(","),
				upper_corner: ol.extent.getTopRight(bbox).join(",")
			})
		}),
		loaderModal = seg.modal.openLoader("ACQ Oilspills", "Retrieving Oil spills...", {class: "info"});

	loaderModal.setPartialProgress("ACQ", 0);
	loaderModal.setDismissCallback(oilSpillsRequest.cancel);

	return oilSpillsRequest.then(res => {
		// set modal progress
		loaderModal.setPartialProgress("ACQ", 100);

		const oilSpills = (new ol.format.GeoJSON()).readFeatures(res.result, {
			featureProjection: seg.map.getProjection()
		});

		//if no reports were retrieved, return
		if (!oilSpills.length) {
			loaderModal.setContent("Retrieved " + oilSpills.length + " oil spills reports.");
			loaderModal.setClass("no-result");
			seg.modal.closeTimeout(loaderModal.title);
			return;
		}

		const oilSpillSource = seg.layer.get("oil_spills").getSource(),
			features = [];

		oilSpills.forEach((oilspill) => {
			const existFeature = oilSpillSource.getFeatureById(oilspill.getId());
			if (!existFeature) {
				const timestamp = oilspill.get("timeStamp");

				if (timestamp && timestamp.year) {
					const year = timestamp.year,
						month = timestamp.monthValue < 10 ? "0" + timestamp.monthValue : timestamp.monthValue,
						day = timestamp.dayOfMonth < 10 ? "0" + timestamp.dayOfMonth : timestamp.dayOfMonth,
						hour = timestamp.hour < 10 ? "0" + timestamp.hour : timestamp.hour,
						minute = timestamp.minute < 10 ? "0" + timestamp.minute : timestamp.minute,
						second = timestamp.second < 10 ? "0" + timestamp.second : timestamp.second;

					oilspill.set("timeStamp", year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + second + "Z");
				}

				oilspill.set("old_oilspill", true); // used to filter the features that will be removed when acq cleanable is activated
				oilspill.set("id", oilspill.getId());
				oilSpillSource.addFeature(oilspill);
				seg.ui.queryPanel.ACQ.addFeature(oilspill);
				features.push(oilspill);
			} else {
				existFeature.set("existing_oilspill", true); // used to filter the features that will be removed when acq cleanable is activated
				seg.ui.queryPanel.ACQ.addFeature(existFeature);
				features.push(existFeature);
			}
		});
		features.forEach((feature) => {
			feature.set("visible", true);
		});
		loaderModal.setContent("Retrieved " + oilSpills.length + " oil spills reports.");
		loaderModal.setClass("success");

		//Oil spills table
		// let acqOilSpillTableSelectionListener;

		const oilspillTable = seg.ui.ttt.tables.open({
			label: "ACQ Oil Spills",
			data: features,
			itemAs: "oilSpill",
			gridAPI:{
				getLabel,
				getOperations,
				getAreaUnits:() => seg.preferences.workspace.get("areaUnits"),
				getDistanceUnits:() => seg.preferences.workspace.get("distanceUnits")
			},
			fields: oil_spills_table_fields || DEFAULT_OILSPILLS_TABLE_FIELDS,
			onSelectionChange(changedRows, selectedRows) {
				seg.selection.select(selectedRows.map(row => {
					row.item.set("visible", true);
					return row.item;
				}));
			},
			onDoubleClick(changedRows, selectedRows) {
				seg.selection.selectFeatureAndCenter(selectedRows.map(row => {
					row.item.set("visible", true);
					return row.item;
				}));
			}
		});

		seg.ui.queryPanel.ACQ.onClear(() => {
			seg.ui.ttt.tables.close(oilspillTable);
			oilSpillSource.getFeatures().filter(feature => feature.get("old_oilspill")).forEach(feature => oilSpillSource.removeFeature(feature));
			oilSpillSource.getFeatures().filter(feature => feature.get("existing_oilspill")).forEach(feature => feature.set("visible", false));

		});

		seg.modal.closeTimeout(loaderModal.title);

		// ACQ should return object for timeline component
		let previouslySelectedFeature;
		return {
			cluster: true,
			marker: {
				template: `
				<div class="default-marker" ng-click="selectAndCenter(items)">
					<img src="extensibility/modules/eo/SAR/oil_spills/timeline_icon.png">
					<div style="background-color: #ff006d;" id="cluster-badge" ng-if="items.length > 1">{{items.length}}</div>
				</div>`,
				bindings: {
					selectAndCenter(items) {
						seg.selection.selectFeatureAndCenter(items[0].oilSpill);
					}
				}
			},
			// TOOLTIP TO BE REMOVED
			tooltip: {
				template: `
				<ul class="default-tooltip">
					<li ng-repeat="item in items">
						<label ng-click="selectAndCenter(item.oilSpill)">{{item.oilSpill.get('serviceID')}}</label>
					</li>
				</ul>`,
				bindings: {
					selectAndCenter: seg.selection.selectFeatureAndCenter
				}
			},
			items: features.map(oilSpill => ({
				timestamp: moment(oilSpill.get("timeStamp")), oilSpill
			})),
			onTimeChange(timestamp, before, after) {
				if(seg.ui.ttt.openTab === "timeline"){
					if(after.length)
						after.forEach(({oilSpill}) => oilSpill.set("visible", false));

					if(previouslySelectedFeature) {
						seg.selection.deselect(previouslySelectedFeature);
						previouslySelectedFeature = null;
					}

					if(before.length) {
						before.forEach(({oilSpill}) => oilSpill.set("visible", true));

						const currentSelectedFeature = before[before.length - 1].oilSpill;

						if(currentSelectedFeature !== previouslySelectedFeature) {
							seg.selection.select(currentSelectedFeature, true);
							previouslySelectedFeature = currentSelectedFeature;
						}
					}
				} else {
					after.forEach(({oilSpill}) => oilSpill.set("visible", true));
					before.forEach(({oilSpill}) => oilSpill.set("visible", true));
				}
			}
		};
	}, e => {
		loaderModal.setContent("Request Failed");
		seg.modal.closeTimeout(loaderModal.title);
		seg.log.error("ERROR_OIL_SPILLS_OIL_SPILLS_REQUEST", e.error + ": " + e.result);
	}, progress => loaderModal.setPartialProgress("ACQ", progress.loaded / progress.total * 100));
};
