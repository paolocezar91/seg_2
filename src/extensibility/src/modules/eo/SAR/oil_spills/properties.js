const defaultProperties = {
	id: {
		label: "Oilspill ID",
		template: "{{::oilSpillsCommandInfo.getLabel(oilSpill) || \"N/A\"}}",
		visible: true,
		order: 0
	},
	serviceId: {
		label: "Service ID",
		template: "{{::oilSpill.get('serviceID') || \"N/A\"}}",
		visible: true,
		order: 1
	},
	origin: {
		label: "Origin",
		template: "{{::oilSpill.get('origin') === 'SYNTHETIC' ? 'EXPECTED' : (oilSpill.get('origin') || \"N/A\")}}",
		visible: true,
		order: 2
	},
	timeStamp: {
		label: "Acquisition Start",
		template: `
			<span position-timestamp="::oilSpill.get('timeStamp')"></span>
		`,
		visible: true,
		order: 3
	},
	acquisitionStop: {
		label: "Acquisition Stop",
		template: "N/A",
		visible: false,
		order: 3
	},
	reliabilityClass: {
		label :"Reliability Class",
		template: "{{::oilSpill.get('reliabilityClass') || \"N/A\"}}",
		visible: true,
		order: 4
	},
	area: {
		label: "Area ({{oilSpillsCommandInfo.getAreaUnits()}})",
		template: "{{(oilSpill.get('area') | area:\"m2\" | number) || \"N/A\"}}",
		visible: true,
		order: 5
	},
	length: {
		label: "Length ({{oilSpillsCommandInfo.getDistanceUnits()}})",
		template: "{{oilSpill.get('length')>=0 ? (oilSpill.get('length') | distance:\"m\" | number) : \"N/A\" }}",
		visible: true,
		order: 6
	},
	width: {
		label: "Width ({{oilSpillsCommandInfo.getDistanceUnits()}})",
		template: "{{oilSpill.get('width')>=0 ? (oilSpill.get('width') | distance:\"m\" | number) : \"N/A\"}}",
		visible: true,
		order: 7
	},
	distanceFromCoast: {
		label: "Distance from coast ({{oilSpillsCommandInfo.getDistanceUnits()}})",
		template: "{{(oilSpill.get('distanceFromCoast') | distance:\"m\" | number) || \"N/A\"}}",
		visible: true,
		order: 8
	},
	numberOfSlicks: {
		label: "Number of slicks",
		template:"{{::oilSpill.getGeometry().getPolygons().length || \"N/A\"}}",
		visible: true,
		order: 9
	},
	possibleSourceDetected: {
		label: "Possible source detected",
		// template: "{{::oilSpill.get('possibleSources') ? \"YES\" : \"NO\"}}",
		template: "N/A", // value will remain N/A until further notice
		visible: true,
		order: 10
	},
	possibleSourceIdentified: {
		label: "Possible source identified",
		// template: "{{::oilSpill.get('possibleSources') ? \"YES\" : \"NO\"}}",
		template: "N/A", // value will remain N/A until further notice
		visible: true,
		order: 10
	},
	sar_windDirection: {
		label: "SAR Wind Direction (°)",
		template: "{{::oilSpill.get('sarwind_windDirection') || 'N/A'}}",
		visible: true,
		order: 11
	},
	sar_windIntensity: {
		label: "SAR Wind Speed (m/s)",
		template: "{{::oilSpill.get('sarwind_windIntensity') || 'N/A'}}",
		visible: true,
		order: 12
	},
	warningIssued: {
		label:"Warning Issued",
		template: "{{::oilSpill.get('oilSpillWarning')? \"YES\": \"NO\"}}",
		visible: true,
		order: 13
	},
	satellite: {
		label: "Satellite",
		template: "N/A",
		visible: false,
		order: 13
	},
	centerPosition: {
		label: "Center Position",
		template: `
			<span coordinate="::[oilSpill.get('center').coordinates[1], 'lat']"></span>
			<span coordinate="::[oilSpill.get('center').coordinates[0], 'lon']"></span>
		`,
		visible: false,
		order: 14
	},
	latitude: {
		label: "Lat (Center)",
		template: `
			<span coordinate="::[oilSpill.get('center').coordinates[1], 'lat']"></span>
		`,
		visible: true,
		order: 14
	},
	longitude: {
		label: "Long (Center)",
		template: `
			<span coordinate="::[oilSpill.get('center').coordinates[0], 'lon']"></span>
		`,
		visible: true,
		order: 15
	},
	met_windIntensity: {
		label: "Met Wind Speed (m/s)",
		template: "{{::oilSpill.get('meteoWind_windIntensity') || \"N/A\"}}",
		visible: true,
		order: 16
	},
	met_windDirection: {
		label: "Met Wind Direction (°)",
		template: "{{::oilSpill.get('meteoWind_windDirection') || \"N/A\"}}",
		visible: true,
		order: 17
	},
	operation: {
		label: "EO Operation",
		template: "{{::oilSpillsCommandInfo.getOperations(oilSpill) || \"N/A\"}}",
		visible: true,
		order: 18
	},
	feedback: {
		label: "Feedback",
		template: "{{::oilSpill.get('feedbacks').length?\"YES\":\"NO\"}}",
		visible: true,
		order: 19
	},
	affectedAlertAreas: {
		label: "Affected alert areas",
		template: "{{::oilSpill.get('affectedAlertAreas').length ? oilSpill.get('affectedAlertAreas').join(', ') : \"N/A\"}}",
		visible: true,
		order: 20
	},
	feedbackAvailabilty: {
		label: "Feedback availability",
		template: "{{::oilSpill.get('feedbackAvailability')?\"YES\":\"NO\"}}",
		visible: true,
		order: 22
	},
	oilSpillWarning: {
		label: "Oil spill warning",
		template: "{{::oilSpill.get('oilSpillWarning')?\"YES\":\"NO\"}}",
		visible: true,
		order: 23
	},
	sensorResolution: {
		label: "Sensor Resolution",
		template: "{{ oilSpill.get('sensorResolution') ? oilSpill.get('sensorResolution') : 'Click to fill' }}",
		visible: true,
		order: 24
	},
	alertLevel:{
		label: "Alert Level",
		template: "{{::oilSpill.get('alertLevel')?oilSpill.get('alertLevel'):'N/A'}}",
		visible: true,
		order: 25
	}
};

const mapProperties = (options) => seg.utils.mapItemProperties(defaultProperties, options);

module.exports = mapProperties;