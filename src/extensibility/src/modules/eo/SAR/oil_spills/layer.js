const {runDriftModel, showSlickInfo, getOperations} = require("./api.js"),
	{oilspill_filters} = require("./filters.js");

const buildOilSpillsLayer = config => {
	const oilSpillsBindings = Object.assign({}, require("./command_info.scope")(config), {
		getDistanceUnits: () =>  seg.preferences.workspace.get("distanceUnits"),
		getAreaUnits: () => seg.preferences.workspace.get("areaUnits")
	});

	return {
		id: "oil_spills",
		name: "Oil Spills",
		type: "Vector",
		source: {
			type: "Vector"
		},
		style: require("./style.js"),
		commandInfo: {
			//alias to access the selected Oil Spills
			featureAs: "oilSpill",
			bindings: {
				oilSpillsCommandInfo: Object.assign({}, oilSpillsBindings, {
					runDriftModel,
					getOperations,
					reportInSSN: config.oil_spills_report_in_ssn,
					downloadAlertReport: config.footprints_alert_report,
					showSlickInfo: config.oil_spills_slick_info && showSlickInfo,
					feedback: () => !!~require("../../../apps/report/service").getReportScopes().findIndex(type => type === "Insert Oilspill")
				})
			},
			//template url for this C&I
			templateUrl: resolvePath("./templates/command_info.html")
		},
		tooltip: {
			featureAs: "oilSpill",
			bindings: {
				oilSpillsTooltip: Object.assign({}, oilSpillsBindings)
			},
			templateUrl: resolvePath("./templates/tooltip.html")
		},
		search: {
			keys: [
				"serviceID"
			],
			label: "label"
		},
		filters: oilspill_filters
	};
};

module.exports = {
	buildOilSpillsLayer
};