const {
	DEFAULT_OILSPILLS_DISPLAY_PREFERENCES,
	DEFAULT_TOOLTIP_OILSPILLS_DISPLAY_PREFERENCES,
	getLabel
} = require("./api");

module.exports = config => ({
	oilSpillDefaultPicture: "assets/images/noOilSpill.png",
	init(oilSpill) {
		// We need to get endPosition time for the EO associated with the Oilspill to use in ACQ functions
		seg.request.get("v1/eo/catalogue/search", {
			params: {
				serviceID: oilSpill.get("serviceID")
			}
		}).then(res => {
			oilSpill.set("endPosition", res.result.features[0].properties.endPosition);
			oilSpill.set("sensorResolution", res.result.features[0].properties.sensorResolution);
		});

		this.oilSpillPicture = oilSpill.get("picture") || this.oilSpillDefaultPicture;
		if(oilSpill.get("dataReference") && oilSpill.get("serviceID") && !oilSpill.get("picture"))
			seg.request.getDataURL("v1/eo/clip_image", {
				params: {
					fileName: oilSpill.get("dataReference"),
					serviceId: oilSpill.get("serviceID")
				}
			}).then(image => {
				if (image){
					this.oilSpillPicture = image;
					oilSpill.set("picture", image);
				} else {
					seg.log.error("ERROR_OIL_SPILLS_INIT_GET_OS_IMAGE", "There's no image for this oilspill");
				}
			}, e => seg.log.error("ERROR_OIL_SPILLS_INIT_GET_OS_IMAGE", e.error + ": " + e.result));
	},
	centerMap(oilSpill) {
		seg.selection.selectFeatureAndCenter(oilSpill);
	},
	getVesselTracks(oilSpill) {
		let around;
		const center = new ol.geom.Point(ol.extent.getCenter(oilSpill.getGeometry().getExtent())),
			beginPosition = moment(oilSpill.get("timeStamp")),
			endPosition = moment(oilSpill.get("endPosition")),
			ratio = Math.cosh(center.getCoordinates()[1]/6378137),
			radius = 37.8 + (Math.max(oilSpill.get("length"), oilSpill.get("width")) / (2 * 1852)),
			distanceUnits = seg.preferences.workspace.get("distanceUnits");

		around = seg.utils.convertToMeters(radius * ratio, seg.preferences.workspace.get("distanceUnits"));
		switch(distanceUnits) {
			case "km": around *= 1.852; break;
			case "m": around *= 1852; break;
		}

		const geometry = ol.geom.Polygon.fromExtent(ol.extent.buffer(center.getExtent(), around)),
			aoi = new ol.Feature({geometry});

		geometry.set("radius", radius);
		aoi.set("isACQResult", true);

		seg.favourites.addFeaturesToGroup("Session", aoi, getLabel(oilSpill) + "_ACQ");
		seg.ACQ(
			[ol.proj.transformExtent(geometry.getExtent(), seg.map.getProjection(), "EPSG:4326")],
			beginPosition.clone().subtract(12, "h"),
			endPosition.clone(),
			{
				"EO": {
					"Footprints": true,
					"Oil Spills": true
				}
			}
		);

		seg.map.centerOn(geometry);
	},
	displayPreferences_: config.oil_spills_fields || DEFAULT_OILSPILLS_DISPLAY_PREFERENCES,
	get displayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("OilSpill.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayPreferences_[key])
				self.displayPreferences_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayPreferences_;
	},
	toggleDisplayPreference(key) {
		//we only want to save the "visible" property, the rest is always the same
		this.displayPreferences[key].visible = !this.displayPreferences[key].visible;

		seg.preferences.workspace.set("OilSpill.commandInfo.displayOptions." + key, this.displayPreferences[key].visible);
	},
	//private property
	displayTooltipPreferences_: config.oil_spills_tooltip_fields || DEFAULT_TOOLTIP_OILSPILLS_DISPLAY_PREFERENCES,
	get tooltipDisplayPreferences()	{
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/

		const savedPreferences = seg.preferences.workspace.get("OilSpill.tooltip.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			self.displayTooltipPreferences_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayTooltipPreferences_;
	},
	showPossibleSources: function(oilSpill) {
		seg.ui.ttt.tables.close("Possible Sources");
		seg.ui.ttt.tables.open({
			label: "Possible Sources",
			data: oilSpill.get("possibleSources"),
			itemAs: "PS",
			fields: {
				id: {
					label: "ID",
					template: "{{PS.id}}",
					visible: true
				},
				identifier: {
					label: "Identifier",
					template: "{{PS.identifier}}",
					visible: true
				},
				vesselConnected: {
					label: "Vessel Connected",
					template: "{{PS.vesselConnected}}",
					visible: true
				}
			}
		});
	},
	openACQ(oilSpill) {
		const center = ol.extent.getCenter(oilSpill.getGeometry().getExtent()),
			distanceUnits = seg.preferences.workspace.get("distanceUnits"),
			beginPosition = moment(oilSpill.get("timeStamp")).subtract(12, "h"),
			endPosition = moment(oilSpill.get("endPosition"));

		// WE HAD THE CALCULATION FOR 12NM, BUT THEY ASKED TO CHANGE TO 70KM, SINCE 37.8NM = 70KM WE USE NOW 37.8
		let radius = 37.8 + (Math.max(oilSpill.get("length"), oilSpill.get("width")) / (2 * 1852))/*nm*/,
			around = radius;

		switch(distanceUnits) {
			case "km": around *= 1.852; break;
			case "m": around *= 1852; break;
		}

		seg.ACQ.open("bboxBuilder", {
			center: ol.proj.toLonLat(center, seg.map.getProjection()),
			around,
			from: beginPosition,
			to: endPosition
		});

		//NM to meters
		radius *= 1852;
		//meters to projection unit
		radius /= seg.map.getProjection().getMetersPerUnit();

		seg.map.centerOn(new ol.Feature({
			geometry: new ol.geom.Circle(center, radius + 4000)
		}));
	},
	alertReportDownload: async feature => {
		const modal = seg.modal.openModal("Alert Report", "Downloading Alert Report...", {class: "info"});

		seg.request.download("v1/eo/alert_report/zip", {
			params: {
				oilSpillId: feature.get("eventId")
			}
		}, feature.getId() + "_alert_report.zip").then(() => {
			modal.setContent("Download succesful");
			modal.setClass("success");
			seg.modal.closeTimeout(modal.title);
		}, e => {
			modal.setContent("Download failed");
			modal.setClass("error");
			seg.modal.closeTimeout(modal.title);
			seg.log.error("oil spills", "alert report download failed ("+ feature.getId() +")", e);
		});
	},
	downloadOilspill: (os) => {
		const exportFormat = seg.export.exportFormats.find(({name}) => name === "SHP"),
			fields = config.oil_spills_tooltip_fields || DEFAULT_TOOLTIP_OILSPILLS_DISPLAY_PREFERENCES,
			data = [os],
			filename = getLabel(os),
			extension = "zip",
			fullName = `${exportFormat.name}_${filename}.${extension}`,
			view = {
				columns: Object.keys(fields).reduce((fields, key) => Object.assign(fields, {
					[key]: {
						label: key
					}
				}), {}),
				rows: data.map(feature => Object.keys(fields).reduce((values, key) => Object.assign(values, {
					[key]: feature.get(key)
				}), {}))
			},
			modal = seg.modal.openLoader("Export", `Exporting ${fullName}`, {class: "info"});

		seg.export.doExport(exportFormat, data, view).then((res) => {
			//create download function to register on download manager
			const download = () => {
				const link = document.createElement("a");

				document.body.appendChild(link);

				link.href = window.URL.createObjectURL(res);
				link.download = fullName;

				link.click();

				window.URL.revokeObjectURL(link.href);
				link.remove();
			};

			seg.downloads.add({
				repeatDownload: download,
				date: moment(),
				filename,
				extension,
				size: res.size/1000
			});

			download();

			modal.setContent("Export successful");
			modal.updateProgress();
			seg.modal.closeTimeout(modal.title);
		}, e => seg.log.error("ERROR_OILSPILLS_DOWNLOAD_OILSPILL", e.error + ": " + e.result));
	},
	openReport: () => {
		seg.ui.apps.get("Report").bindings.close();
		seg.timeout(() => seg.ui.apps.open("Report"), 500);
	},
	getCenter: (os) => ol.proj.toLonLat(ol.extent.getCenter(os.getGeometry().getExtent()), seg.map.getProjection()),
	openOriginPopup: () => {
		seg.popup.openPopup({
			title: "Origin",
			templateUrl: resolvePath("./templates/origin_popup.html")
		});
	},
	getLabel
});