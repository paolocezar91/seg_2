const
	{DEFAULT_OILSPILLS_TABLE_FIELDS, mergeDuplicates, getLabel, getOperations} = require("./api"),
	{generateOptions} = require("../footprints/api.js"),
	OPERATIONS = require("../footprints/constants/operations.json"),
	DEFAULT_SEARCH_FIELDS = {
		acquisitionStart: moment().subtract(3,"d"),
		acquisitionStop: moment(),
		showTime: false,
		serviceId: null,
		oilspillId: null,
		featureDOI: null,
		classOptions: [{label: "A", value: "A"},{label: "B", value: "B"}, {label: "All", value: null}],
		selectedClass: null,
		feedback: null,
		operationsOptions: generateOptions(OPERATIONS, true),
		operation: null,
		alert_area: null
	};



const buildOilspillsSearch = config => {
	return {
		name: "Oilspills",
		template: resolvePath("./templates/search.html"),
		services: (() => {
			const _config = {
				// Script for requesting sattelites and operations to CSN service
				// <div ng-init="init()"> on template
				// async init () {
				// 	const operations = (await seg.csn.getCSN("operations")) || [];
				// 	_config.search.operationOptions = generateOptions(operations
				// 		.filter((obj, pos, arr) => arr.map(mapObj => mapObj).indexOf(obj) === pos)
				// 		.sort(), true);
				// },
				search: config.oil_spills_search_fields || DEFAULT_SEARCH_FIELDS,
				operation: seg.operation.project,
				isOilspillIDValid: (oilspillId) => {
					return oilspillId && !!oilspillId.match(/OS_[0-9]+_[0-9]+/);
				}
			};

			return _config;
		})(),
		canSubmit: function() {
			const {showTime, selectedArea, serviceId, featureDOI, oilspillId} = this.services.search,
				correctOSid = this.services.isOilspillIDValid(oilspillId);

			if (showTime || (selectedArea && selectedArea.length) || (serviceId && serviceId !== "") || (oilspillId && oilspillId !== "" && correctOSid) || (featureDOI && featureDOI !== ""))
				return true;
			return false;
		},
		submit: function () {
			seg.ui.cleanables.call("Adv. Search Oilspills");
			// create Request
			const searchService = this.services.search,
				loaderModal = seg.modal.openModal("Oilspills Advanced Search", "Retrieving Oil Spills...", {class: "info"});
			let params = {};

			if (searchService.showTime){
				params = Object.assign({
					begin_position: moment.isMoment(searchService.acquisitionStart) ? searchService.acquisitionStart.toISOString() : moment().subtract(3,"d"),
					end_position: moment.isMoment(searchService.acquisitionStop) ? searchService.acquisitionStop.toISOString() : moment().toISOString()
				}, params);
			}

			if(!searchService.oilspillId && (searchService.selectedArea && searchService.selectedArea.length)) {
				const areaExtent = ol.proj.transformExtent(searchService.selectedArea[0].getGeometry().getExtent(), seg.map.getProjection(), "EPSG:4326");
				params = Object.assign({
					lower_corner: ol.extent.getBottomLeft(areaExtent).join(","),
					upper_corner: ol.extent.getTopRight(areaExtent).join(",")
				}, params);
			}

			//XXX should move this to operation config
			if (seg.operation.project === "CLEANSEANET")
				params = Object.assign({
					oilSpillWarning: searchService.warningIssued,
					origin: searchService.origin
					//operations: searchService.eoOperation
				}, params);

			params = Object.assign({
				serviceID: searchService.serviceId ? searchService.serviceId : null,
				featureDOI: searchService.featureDOI || null,
				class_a:(searchService.selectedClass === "A")? true: null,
				class_b: (searchService.selectedClass === "B")? true: null,
				with_feedback:searchService.feedback,
				operation: searchService.operation,
				alert_area: searchService.alert_area
				// psd: searchService.psd
			}, params);

			if(searchService.oilspillId && searchService.oilspillId.match(/OS_[0-9]+_[0-9]+/))
				params.serviceID = searchService.oilspillId.replace(/(OS_|_\d+)/g, "");

			seg.request.get("v1/eo/wfs", {
				params: seg.utils.removeUndefinedAttributes(params)
			}).then(res => {
				let oilspills = (new ol.format.GeoJSON()).readFeatures(res.result, {
					featureProjection: seg.map.getProjection()
				});

				if(searchService.oilspillId && searchService.oilspillId.match(/OS_[0-9]+_[0-9]+/))
					oilspills = oilspills.filter(os => getLabel(os) === searchService.oilspillId);

				if (!oilspills.length) {
					loaderModal.setContent("Retrieved " + oilspills.length + " Oilspills reports.");
					loaderModal.setClass("no-result");
					seg.modal.closeTimeout(loaderModal.title);
					return;
				}

				loaderModal.setContent("Retrieved " + oilspills.length + " Oilspills reports.");
				loaderModal.setClass("success");

				oilspills.forEach(oilspill => {
					oilspill.set("isSearchResult", true);
					oilspill.set("numberOfSlicks", oilspill.getGeometry().getPolygons().length);
				});

				const mergeDuplicateResults = mergeDuplicates(oilspills);
				const oilspillsSource = seg.layer.get("oil_spills").getSource();
				oilspillsSource.addFeatures(mergeDuplicateResults);

				seg.modal.closeTimeout(loaderModal.title);

				let oilspillsTable = seg.ui.ttt.tables.openTables.find( t => t.label === "Oil Spills Adv. Search");

				if (!oilspillsTable)
					oilspillsTable = seg.ui.ttt.tables.open({
						label: "Oil Spills Adv. Search",
						data: oilspills,
						itemAs: "oilSpill",
						gridAPI: {
							getLabel,
							getOperations,
							getAreaUnits:() => seg.preferences.workspace.get("areaUnits"),
							getDistanceUnits:() => seg.preferences.workspace.get("distanceUnits")
						},
						customToolbar: {
							template: "<span class=\"text-size-sm\">Number of Oilspills: <b>{{grid.filteredResults.length}}</b></span>"
						},
						fields: config.oil_spills_table_fields || DEFAULT_OILSPILLS_TABLE_FIELDS,
						onSelectionChange(changedRows, selectedRows) {
							const items = selectedRows.map(({item}) => {
								item.set("visible", true);
								return item;
							});
							seg.map.getView().fit(items);
							seg.selection.selectFeatureAndCenter(items, undefined, seg.map.getView().getResolution());
						}
					});
				else
					oilspillsTable.data = oilspills;

				seg.ui.cleanables.add("Adv. Search Oilspills", () => {
					seg.ui.ttt.tables.close("Oil Spills Adv. Search");
					seg.ui.cleanables.remove("Adv. Search Oilspills");
					oilspillsSource.getFeatures()
						.filter(feature => feature.get("isSearchResult"))
						.forEach(feature => {
							oilspillsSource.removeFeature(feature);
						});
				});
			}, e => seg.log.error("ERROR_OIL_SPILLS_SEARCH_SUBMIT", e.error + ": " + e.result));
		},
		reset: function () {
			for (const prop in this.services.search) {
				if(!prop.includes("Options"))
					if (this.services.search[prop] && typeof this.services.search[prop] === "object") {
						if(Array.isArray(this.services.search[prop]))
							this.services.search[prop] = [];
						else
							this.services.search[prop].value = null;
					} else {
						this.services.search[prop] = null;
					}
			}

			seg.selection.select([]);

			this.services.search.acquisitionStart = moment();
			this.services.search.acquisitionStop = moment();
		},
		save: function (searchName) {
			seg.search.getSavedSearches().then(res => {
				const savedSearchesSmart = res[1], savedSearches = res[0];
				savedSearches.push({
					name: searchName,
					config: this.services,
					src: "Oilspills",
					advanced: true
				});

				seg.search.setSavedSearches(savedSearches.concat(savedSearchesSmart));
			}, e => seg.log.error("ERROR_OIL_SPILLS_SEARCH_SAVE", e.error + ": " + e.result));
		}
	};
};

module.exports = {
	buildOilspillsSearch,
	DEFAULT_SEARCH_FIELDS
};
