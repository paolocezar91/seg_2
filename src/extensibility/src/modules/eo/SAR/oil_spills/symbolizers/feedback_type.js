const style = oilSpill => {
	switch(oilSpill.get("feedback_type")) {
		case "CSN Detection no feedback": return [0,175,13]; // rgb(0,175,13)
		case "Mineral oil confirmed": return [255,0,0]; // rgb(255,0,0)
		case "Other substance confirmed": return [255,0,255]; // rgb(255,0,255)
		case "Unknown feature observed": return [167,96,7]; // rgb(167,96,7)
		case "Natural phenomena observed": return [0,255,255]; // rgb(0,255,255)
		case "Nothing observed": return [255,255,0]; // rgb(255,255,0)
	}
};

module.exports = {
	name: "Feedback Type",
	style
};