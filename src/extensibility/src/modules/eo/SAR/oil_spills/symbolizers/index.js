const symbolizers = {
		"Class Type": require("./class"),
		"Alert Level": require("./alert_level"),
		"Feedback Type": require("./feedback_type")
		// "Origin": require("./origin")
	},
	getSelected = () => symbolizers[seg.preferences.map.get("oilSpills.symbolizer")] || symbolizers["Class Type"],
	select = symbolizer => seg.preferences.map.set("oilSpills.symbolizer", symbolizer.name),
	isSelected = symbolizer => symbolizer.name === getSelected().name,
	buildSymbolizerMenu = symbolizerNames => {
		const oilSpillsSource = seg.layer.get("oil_spills").getSource();

		return Object.assign(seg.ui.layerMenu.create("Symbology"), {
			options: symbolizerNames.map(symbolizerName => {
				const symbolizer = symbolizers[symbolizerName],
					menuItem = seg.ui.layerMenu.create(symbolizerName);

				if(Array.isArray(symbolizer))
					return Object.assign(menuItem, {
						isChecked() {
							return symbolizer.some(isSelected);
						},
						toggle() {
							if(!symbolizer.some(isSelected)) {
								select(symbolizer[0]);
								oilSpillsSource.changed();
							}
						},
						hideSelectAll: true,
						options: symbolizer.map(symbolizer => Object.assign(seg.ui.layerMenu.create(symbolizer.name), {
							isChecked() {
								return isSelected(symbolizer);
							},
							toggle() {
								select(symbolizer);
								oilSpillsSource.changed();
							}
						}))
					});

				return Object.assign(menuItem, {
					isChecked() {
						return isSelected(symbolizer);
					},
					toggle() {
						select(symbolizer);
						oilSpillsSource.changed();
					}
				});
			}),
			type: "symbology",
			hideSelectAll: true
		});
	};

module.exports = {
	buildSymbolizerMenu,
	getSelected
};
