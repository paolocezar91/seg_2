const style = oilSpill => {
	switch(oilSpill.get("alertLevel")) {
		case "Green": return [0,255,0];
		case "Yellow": return [255,255,0];
		case "Red": return [255,0,0];
	}
};

module.exports = {
	name: "Alert Level",
	style
};