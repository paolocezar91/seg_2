const style = oilSpill => {
	switch(oilSpill.get("reliabilityClass")) {
		case "A": return [255,0,0];
		case "B": return [0,255,0];
	}
};

module.exports = {
	name: "Class Type",
	style
};