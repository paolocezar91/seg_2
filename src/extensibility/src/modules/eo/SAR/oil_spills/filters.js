const oilspill_filters = [
	{
		name: "Class",
		options: {
			"Class A": oilspill => oilspill.get("reliabilityClass") === "A",
			"Class B": oilspill => oilspill.get("reliabilityClass") === "B",
			"N/A": oilspill => !oilspill.get("reliabilityClass")
		}
	},
	{
		name: "Feedback",
		options: {
			"Has feedback": oilspill => oilspill.get("feedbacks") && oilspill.get("feedbacks").length || oilspill.get("feedbackAvailability"),
			"No feedback": oilspill => !oilspill.get("feedbacks") || !oilspill.get("feedbacks").length || !oilspill.get("feedbackAvailability")
		}
	},
	{
		name: "Alert level",
		options: {
			"Green": oilspill => oilspill.get("alertLevel").match(/Green/i),
			"Yellow": oilspill => oilspill.get("alertLevel").match(/Yellow/i),
			"Red": oilspill => oilspill.get("alertLevel").match(/Red/i),
			"N/A": oilspill => !oilspill.get("alertLevel")
		}
	},
	{
		name: "Feedback Type",
		options: {
			"Mineral oil confirmed": (oilspill) => oilspill.get("feedback_type") === "Mineral oil confirmed",
			"Other substance confirmed": (oilspill) => oilspill.get("feedback_type") === "Other substance confirmed",
			"Unknown feature observed": (oilspill) => oilspill.get("feedback_type") === "Unknown feature observed",
			"Natural phenomena observed": (oilspill) => oilspill.get("feedback_type") === "Natural phenomena observed",
			"Nothing observed": (oilspill) => oilspill.get("feedback_type") === "Nothing observed",
			"N/A": (oilspill) => !oilspill.get("feedback_type") || oilspill.get("feedback_type") === ""
		}
	}
];

module.exports = {
	oilspill_filters
};