module.exports = {
	"Class Type": [
		{
			title: "Class Type",
			symbologies: [
				{
					class: "A",
					label: "Class A",
					style: "background-color: rgba(255,0,0, 1.0);"
				},
				{
					class: "B",
					label: "Class B",
					style: "background-color: rgba(0,255,0, 1.0);"
				}
			]
		}
	],
	"Feedback Type": [
		{
			title: "Feedback Type",
			symbologies: [
				{
					class: "CSN-Detection-no-feedback",
					label: "CSN Detection no feedback",
					style: "background-color: rgba(0, 175, 13, 1.0);"
				},
				{
					class: "Nothing-observed",
					label: "Nothing observed",
					style: "background-color: rgba(255,255,0, 1.0);"
				},
				{
					class: "Natural-phenomena-observed",
					label: "Natural phenomena observed",
					style: "background-color: rgba(0, 255, 255, 1.0);"
				},
				{
					class: "Unknown-feature-observed",
					label: "Unknown feature observed",
					style: "background-color: rgba(167,96,7, 1.0);"
				},
				{
					class: "Other-substance-confirmed",
					label: "Other substance confirmed",
					style: "background-color: rgba(255,0,255, 1.0);"
				},
				{
					class: "Mineral-oil-confirmed",
					label: "Mineral oil confirmed",
					style: "background-color: rgba(255,0,0, 1.0);"
				}
			]
		}
	],
	"Alert Level": [
		{
			title: "Alert Level",
			symbologies: [
				{
					class: "Green",
					label: "Green",
					style: "background-color: rgba(0,255,0, 1.0);"
				},
				{
					class: "Yellow",
					label: "Yellow",
					style: "background-color: rgba(255,255,0, 1.0);"
				},
				{
					class: "Red",
					label: "Red",
					style: "background-color: rgba(255,0,0, 1.0);"
				}
			]
		}
	]
};