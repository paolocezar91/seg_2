const {getSelected} = require("./symbolizers"),
	DEFAULT_ICON = require("./oilspill.svg"),
	FEEDBACK_ICON = require("./oilspill_feedback.svg"),
	searchResultStyleCache = {},
	selectedStyleCache = {},
	iconStyleCache = {},
	geometryStyleCache = {},
	getOilSpillInSearchResultSize = () => Math.min(25, Math.max(5, Math.round(5500000 / seg.map.getScale()) * 12)),
	getOilSpillSelectedSize = () => Math.min(30, Math.max(10, Math.round(6600000 / seg.map.getScale()) * 12)),
	getOilSpillSize = () => Math.min(25, Math.max(5, Math.round(5500000 / seg.map.getScale()) * 12)),
	isVisible = oilSpill => {
		if (typeof oilSpill.get("filteredInTTT") !== "undefined") {
			return oilSpill.get("filteredInTTT");
		} else if (!oilSpill.get("visible")) return false;
		// else if(!oilSpill.get("isSearchResult")) return false;
		return true;
	};

module.exports = oilSpill => {
	if (!isVisible(oilSpill))
		return;

	const selected = oilSpill.get("selected"),
		fillColor = getSelected().style(oilSpill);

	if(seg.map.getScale() > 2000000 || oilSpill.get("origin") === "SYNTHETIC") {
		const styles = [],
			centerPointGeometry = new ol.geom.Point(ol.extent.getCenter(oilSpill.getGeometry().getExtent())),
			iconFillColor = fillColor || [0,0,0],
			iconImage = oilSpill.get("feedbacks") && oilSpill.get("feedbacks").length ? FEEDBACK_ICON : DEFAULT_ICON,
			hasFeedback = oilSpill.get("feedbacks") && oilSpill.get("feedbacks").length ? true: false;

		if (oilSpill.get("isSearchResult")) {
			const radius = getOilSpillInSearchResultSize() / 2;

			if(!searchResultStyleCache[radius])
				searchResultStyleCache[radius] = new ol.style.Circle({
					radius,
					fill: new ol.style.Fill({
						color: "#58A64A"
					}),
					opacity: .5
				});

			styles.push(new ol.style.Style({
				geometry: centerPointGeometry,
				image: searchResultStyleCache[radius]
			}));
		}

		if(selected) {
			const radius = getOilSpillSelectedSize() / 2;

			if(!selectedStyleCache[radius])
				selectedStyleCache[radius] = new ol.style.Circle({
					radius,
					stroke: new ol.style.Stroke({
						color: "#006ebc",
						width: 2
					})
				});


			styles.push(new ol.style.Style({
				geometry: centerPointGeometry,
				image: selectedStyleCache[radius]
			}));
		}

		const height = getOilSpillSize(),
			cacheKey = JSON.stringify({height, selected, iconFillColor, hasFeedback});

		if(!iconStyleCache[cacheKey])
			iconStyleCache[cacheKey] = new seg.style.SVG({
				svg: iconImage,
				fill: `rgb(${iconFillColor.join(",")})`,
				height,
				stroke: {
					color: selected?"yellow":"rgba(255,255,255,0.1)",
					width: 1
				},
				opacity: 1
			});


		styles.push(new ol.style.Style({
			geometry: centerPointGeometry,
			image: iconStyleCache[cacheKey]
		}));

		return styles;
	}

	const geometryFillColor = fillColor || [255,255,255],
		geometryStyleCacheKey = JSON.stringify({selected, geometryFillColor});

	if(!geometryStyleCache[geometryStyleCacheKey])
		geometryStyleCache[geometryStyleCacheKey] = new ol.style.Style({
			fill: new ol.style.Fill({
				color: `rgba(${geometryFillColor.join(",")},.1)`
			}),
			stroke: new ol.style.Stroke({
				color: oilSpill.get("selected") ? "yellow" : "blue",
				width: 1
			})
		});

	return geometryStyleCache[geometryStyleCacheKey];
};
