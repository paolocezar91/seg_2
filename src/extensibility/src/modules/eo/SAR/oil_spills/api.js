const mapProperties = require("./properties.js"),
	DEFAULT_OILSPILLS_TABLE_FIELDS = mapProperties({
		override: {
			// id: {template: "{{getLabel(oilSpill)}}"},
			// serviceId: false,
			// timeStamp: false,
			// origin: false,
			// reliabilityClass: false,
			// numberOfSlicks: {template: "{{::oilSpill.get('numberOfSlicks')}}"},
			// area: {label: "Area ({{getAreaUnits()}})"},
			// length: {label: "Length ({{getDistanceUnits()}})"},
			// width: {label:  "Width ({{getDistanceUnits()}})"},
			// distanceFromCoast: {label: "Distance from coast ({{getDistanceUnits()}})"},
			// possibleSourceDetected: false,
			// affectedAlertAreas: false,
			// sar_windDirection: false,
			// sar_windIntensity: false,
			// warningIssued: false,
			// feedback: false,
			// affectedAlertAreas: false,
			// oilSpillWarning: false

			id: {
				template: "{{getLabel(oilSpill)}}",
				order: 0
			},
			serviceId: {order: 1},
			timeStamp: {order: 2},
			latitude: {order: 3},
			longitude: {order: 4},
			area: {order: 5, label: "Area ({{getAreaUnits()}})"},
			length: {order: 6, label: "Length ({{getDistanceUnits()}})"},
			width: {order: 7, label: "Width ({{getDistanceUnits()}})"},
			distanceFromCoast: {order: 8, label: "Distance from coast ({{getDistanceUnits()}})"},
			numberOfSlicks: {order: 9},
			origin: {order: 10},
			reliabilityClass: {label: "Class", order: 11},
			feedback: {order: 12},
			possibleSourceDetected: {order: 13},
			possibleSourceIdentified: {order: 14},
			warningIssued: {order: 15},
			sar_windIntensity: {order: 16, visible: false},
			sar_windDirection: {order: 17, visible: false},
			met_windIntensity: {order: 18, visible: false},
			met_windDirection: {order: 19, visible: false},
			operation: {
				template: "{{getOperations(oilSpill) || \"N/A\"}}",
				order: 20
			},
			affectedAlertAreas: {order: 21},
			sensorResolution:{order: 22},
			alertLevel:{order:23}
		}
	}),
	DEFAULT_OILSPILLS_DISPLAY_PREFERENCES = mapProperties({
		override: {
			// id: false,
			// serviceId: false,
			// timeStamp: false,
			// origin: {
			// 	template: `<span>
			// 		<span>{{::oilSpill.get('origin') || "N/A"}}</span>
			// 		<button style="margin-left: 2px; display: inline;" ng-if="oilSpill.get('origin')" ng-click="oilSpillsCommandInfo.openOriginPopup()" title="Info" class="seg primary round xs inline-block"><i icon="info"></i></button>
			// 	</span>`
			// },
			// reliabilityClass: false,
			// numberOfSlicks: false,
			// area: false,
			// length: false,
			// width: false,
			// distanceFromCoast: false,
			// possibleSourceDetected: false,
			// possibleSourceIdentified: false,
			// affectedAlertAreas: false,
			// sar_windDirection: false,
			// sar_windIntensity: false,
			// warningIssued: false,
			// feedback: false,
			// affectedAlertAreas: false,
			// oilSpillWarning: false


			id: false,
			serviceId: {order: 1},
			timeStamp: {order: 2},
			latitude: {order: 3},
			longitude: {order: 4},
			area: {order: 5},
			length: {order: 6},
			width: {order: 7},
			distanceFromCoast: {order: 8},
			numberOfSlicks: {order: 9},
			origin: {
				template: `<span>
					<span>{{::oilSpill.get('origin') === 'SYNTHETIC' ? 'EXPECTED' : (oilSpill.get('origin') || "N/A")}}</span>
					<button style="margin-left: 2px; display: inline;" ng-if="oilSpill.get('origin')" ng-click="oilSpillsCommandInfo.openOriginPopup()" title="Info" class="seg primary round xs inline-block"><i icon="info"></i></button>
				</span>`,
				order: 10
			},
			reliabilityClass: {label: "Class", order: 11},
			feedback: {order: 12},
			possibleSourceDetected: {order: 13},
			possibleSourceIdentified: {order: 14},
			warningIssued: {order: 15},
			sar_windIntensity: {order: 16},
			sar_windDirection: {order: 17},
			met_windIntensity: {order: 18},
			met_windDirection: {order: 19},
			operation: {order: 20},
			affectedAlertAreas: {order: 21},
			sensorResolution:{order: 22},
			alertLevel:{order:23}
		}
	}),
	DEFAULT_TOOLTIP_OILSPILLS_DISPLAY_PREFERENCES = mapProperties({
		override: {
			timeStamp: false,
			serviceId: false,
			area: {label: "Area ({{oilSpillsTooltip.getAreaUnits()}})"},
			length: {label: "Length ({{oilSpillsTooltip.getDistanceUnits()}})"},
			width: {label:  "Width ({{oilSpillsTooltip.getDistanceUnits()}})"},
			distanceFromCoast: {label: "Distance from coast ({{oilSpillsTooltip.getDistanceUnits()}})"},
			spillIdentifier: {visible: false},
			origin: {visible: false},
			reliabilityClass: {visible: false},
			numberOfSlicks: {visible: false},
			possibleSourceDetected: {visible: false},
			possibleSourceIdentified: {visible: false},
			sar_windDirection: {visible: false},
			sar_windIntensity: {visible: false},
			sensorResolution:{visible: false},
			alertLevel:{visible:false}
		}
	});

const getLabel = oilspill => {
	if (oilspill.get("eventId")) {
		const division = oilspill.get("eventId").split("_");
		return "OS_" + oilspill.get("serviceID") + "_" + division[division.length - 1];
	}
	return;
};

const runDriftModel = oilSpill => {
	const loaderModal = seg.modal.openModal("Oil Spill", "Running drift model...", {class: "info"}),
		request = seg.request.get("v1/eo/wfs/wfsDrift", {
			params: {
				oilspill_id: oilSpill.get("eventId")
			}
		});

	loaderModal.setDismissCallback(request.cancel);

	request.then(({result}) => {
		let latestForecast;

		result.forEach(forecast => {
			if (!latestForecast || forecast.runId > latestForecast.runId)
				latestForecast = forecast;
		});

		if (!latestForecast.centreTrajectory.features.length) {
			loaderModal.setContent("No drift model data.");
			loaderModal.setClass("no-result");
			seg.modal.closeTimeout(loaderModal.title);
			return;
		}

		loaderModal.setContent("Drift model complete");

		const originalGeometry = oilSpill.getGeometry().clone();
		latestForecast = (new ol.format.GeoJSON()).readFeatures(latestForecast.centreTrajectory, {
			featureProjection: seg.map.getProjection()
		});

		seg.map.centerOn(latestForecast);

		seg.ui.cleanables.add("Drift Model", () => {
			oilSpill.setGeometry(originalGeometry);
			seg.timeline.clear();
		});

		seg.modal.closeTimeout(loaderModal.title);

		seg.timeline.set({
			groups: [{
				marker: {
					template: `
					<div class="default-marker" ng-click="selectAndCenter(items)">
						<img src="extensibility/modules/eo/SAR/oil_spills/timeline_icon.png">
						<div style="background-color: #ff006d;" id="cluster-badge" ng-if="items.length > 1">{{items.length}}</div>
					</div>`,
					bindings: {
						selectAndCenter(items) {
							seg.selection.selectFeatureAndCenter(items[0].forecast);
						}
					}
				},
				items: latestForecast.map(forecast => {
					return {
						timestamp: moment(forecast.get("timeStamp")),
						forecast
					};
				}),
				onTimeChange(timestamp, before, after) {
					const currentDrift = (before.length ? before[before.length - 1] : after[0]).forecast,
						oldCenter = ol.extent.getCenter(oilSpill.getGeometry().getExtent()),
						newCenter = currentDrift.getGeometry().getCoordinates(),
						difference = [newCenter[0] - oldCenter[0], newCenter[1] - oldCenter[1]];

					oilSpill.getGeometry().translate(difference[0], difference[1]);
				}
			}]
		});
	}, error => {
		loaderModal.setContent("<b class=\"warning\">Error:</b> " + error.data.message);
		seg.modal.closeTimeout(loaderModal.title);
		seg.log.error("ERROR_OIL_SPILLS_RUN_DRIFT_MODEL", error.status + ": " + error.message);
	});
};

const showSlickInfo = oilSpill => seg.ui.ttt.tables.open({
	label: getLabel(oilSpill) + " Slicks",
	data: oilSpill.getGeometry().getPolygons().map(polygon => {
		const polygonInMeters = polygon.clone().transform(seg.map.getProjection(), "EPSG:3395"),
			extentInMeters = polygonInMeters.getExtent();

		return {
			polygon,
			area: polygon.getArea(),
			length: ol.extent.getHeight(extentInMeters),
			width: ol.extent.getWidth(extentInMeters),
			center: ol.proj.toLonLat(ol.extent.getCenter(extentInMeters), "EPSG:3395")
		};
	}),
	itemAs: "slick",
	gridAPI: {
		getAreaUnits: () => seg.preferences.workspace.get("areaUnits"),
		getDistanceUnits: () => seg.preferences.workspace.get("distanceUnits")
	},
	fields: {
		area: {
			label: "Area ({{getAreaUnits()}})",
			template: "{{slick.area | distance:\"m\" | number:3}}",
			visible: true
		},
		length: {
			label: "Length ({{getDistanceUnits()}})",
			template: "{{slick.length | distance:\"m\" | number}}",
			visible: true
		},
		width: {
			label: "Width ({{getDistanceUnits()}})",
			template: "{{slick.width | distance:\"m\" | number}}",
			visible: true
		},
		lat: {
			label: "Latitude (center)",
			template: "<span coordinate=\"[slick.center[1], 'lat']\"></span>",
			visible: true
		},
		lon: {
			label: "Longitude (center)",
			template: "<span coordinate=\"[slick.center[0], 'lon']\"></span>",
			visible: true
		}
	}
	// onSelectionChange: (_, selectedRows) => {
	// 	seg.selection.selectFeatureAndCenter(selectedRows[0].item.polygon);
	// }
});

const mergeDuplicates = (oilspills) => {
	return oilspills.map(oilspill => {

		const id = oilspill.getId(),
			existing = seg.layer.get("oil_spills").getSource().getFeatureById(id);

		if (existing) {
			if (oilspill.get("timeStamp") > existing.get("timeStamp"))
				existing.setProperties(Object.assign(existing.getProperties(), oilspill.getProperties()));
			return existing;
		}

		return oilspill;
	});
};

const getOperations = (os) => {
	return os.get("operations").map(op => op.operationName).join("; ").replace(",", ";");
};

module.exports = {
	DEFAULT_OILSPILLS_TABLE_FIELDS,
	DEFAULT_OILSPILLS_DISPLAY_PREFERENCES,
	DEFAULT_TOOLTIP_OILSPILLS_DISPLAY_PREFERENCES,
	mergeDuplicates,
	getLabel,
	getOperations,
	runDriftModel,
	showSlickInfo
};
