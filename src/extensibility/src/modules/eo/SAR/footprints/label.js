const properties = {
	"Platform": {
		"alias": "Sensor",
		"template": "{{eo.get('platform')}}",
		"visible": true
	},
	"endPosition": {
		"alias": "End Position",
		"template": "<span position-timestamp=\"eo.get('endPosition')\"></span>",
		"visible": true
	}
};
const getTemplateForProperty = key => properties[key].template;
const getLabelAlias = key => properties[key].alias;
const getLabelProperties= () =>{
	const keys = [];
	for (const key in properties){
		keys.push(key);
	}
	return keys;
};
const register = () => {
	seg.ui.preferences.addSectionToTab("Labels", {
		alias: "labels",
		templateUrl: resolvePath("./templates/label_preferences.html"),
		bindings: {
			scales: [
				546875,
				273437,
				136718,
				68359,
				34179,
				17089,
				8544,
				4272,
				2136,
				1068,
				534
			],
			showingEOLabels() {
				return seg.preferences.workspace.get("EO.label.visible");
			},
			toggleEOLabels() {
				seg.preferences.workspace.set("EO.label.visible", !this.showingEOLabels());
				if (!this.EOLabelProperties.filter(prop => this.isPropertySelected(prop)).length)
					this.toggleLabelProperty("Platform");
				seg.layer.get("eo_sar_imagery").refresh();
			},
			EOLabelProperties: getLabelProperties(),
			getLabelAlias: getLabelAlias,
			get activationScale() {
				return seg.preferences.workspace.get("EO.label.scale");
			},
			set activationScale(value){
				this.setActivationScale(value);
			},
			isPropertySelected(property) {
				return seg.preferences.workspace.get("EO.label.properties."+property);
			},
			toggleLabelProperty(property) {
				seg.preferences.workspace.set("EO.label.properties."+property, !this.isPropertySelected(property));
				if (!this.EOLabelProperties.filter(prop => this.isPropertySelected(prop)).length)
					this.toggleLabelProperty("Platform");
				seg.layer.get("eo_sar_imagery").refresh();
			},
			setActivationScale(value) {
				seg.preferences.workspace.set("EO.label.scale", value);
				seg.layer.get("eo_sar_imagery").refresh();
			},
			getCurrentScale() {
				return seg.map.getScale();
			}
		}
	});
};

module.exports =  {
	getTemplateForProperty,
	getLabelProperties,
	getLabelAlias,
	register
};