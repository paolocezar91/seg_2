module.exports = {
	sar: [{
		symbologies: [
			{
				class: "green",
				label: "Delivered"
			},
			{
				class: "black",
				label: "Tasked"
			},
			{
				class: "purple",
				label: "Archived"
			},
			{
				class: "red",
				label: "Cancelled"
			},
			{
				class: "red",
				label: "Error"
			},
			{
				class: "red",
				label: "Anomaly"
			},
			{
				class: "orange",
				label: "Pending"
			}
		]
	}],
	optical: [{
		symbologies: [
			{
				class: "green",
				label: "Delivered"
			},
			{
				class: "black",
				label: "Tasked"
			},
			{
				class: "purple",
				label: "Archived"
			},
			{
				class: "red",
				label: "Cancelled"
			},
			{
				class: "red",
				label: "Error"
			},
			{
				class: "red",
				label: "Anomaly"
			},
			{
				class: "orange",
				label: "Pending"
			}
		]
	}]
};