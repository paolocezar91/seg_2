const {getTemplateForProperty, getLabelAlias} = require("./label"),
	{DEFAULT_FILTERS} = require("./filters");

const buildFootprintsLayer = config => ({
	id: "eo_sar_imagery",
	name: "SAR Imagery",
	type: "Vector",
	renderMode: "image",
	label: {
		featureAs: "eo",
		bindings: {
			getVisibleProperties: function () {
				return seg.preferences.workspace.get("EO.label.properties");
			},
			showLabel: function () {
				if (seg.map.isFilterEnabled("SELECT_FILTER") && !this.eo.get("wasSelected") || this.eo.get("isVisibleInTTT") === false)
					return false;

				const properties = this.getVisibleProperties();

				for (const property in properties) {
					if (properties[property]) {
						return true;
					}
				}

				return false;
			},
			getTemplateForProperty,
			getLabelAlias
		},
		templateUrl: resolvePath("./templates/label.html"),
		scalePreference: "EO.label.scale",
		visiblePreference: "EO.label.visible"
	},
	style: require("./style"),
	source: {
		name: "EO Footprints",
		type: "Vector",
		projection: "EPSG:4326"
	},
	tooltip: {
		featureAs: "feature",
		templateUrl: config.footprints_tooltip || resolvePath("./templates/tooltip.html")
	},
	commandInfo: {
		//alias to access the selected EO Feature
		featureAs: "EO",
		bindings: {
			EOCommandInfo: Object.assign({}, require("./command_info.scope")(config), {
				canFeedback: (eo) => {
					return !!~require("../../../apps/report/service").getReportScopes().findIndex(type => type === "Oilspills") && eo.get("sar") && eo.get("status") === "Delivered" ? false : "disabled";
				},
				vds_reprocessing: config.vds_reprocessing,
				canDownloadEO: config.footprints_download_eo,
				canDownloadAlertReport: config.footprints_alert_report,
				canActivityDetection: config.footprints_activity_detection,
				oilSpills: config.oil_spills,
				vds: config.vds,
				getDistanceUnits: () => seg.preferences.workspace.get("distanceUnits")
			})
		},
		onClose: () => {
			seg.popup.close("Re-correlation process");
		},
		//template url for this C&I
		templateUrl: resolvePath("./templates/command_info.html")
	},
	//duplicate filters for SAR and Optical
	filters: (() => {
		const footprintsFilters = config.footprints_filters || DEFAULT_FILTERS;
		return config.join_optical_and_sar ?
			[].concat(...footprintsFilters) :
			[].concat(...footprintsFilters
				.map(filter => ["SAR", "Optical"]
					.map(type => {
						let filterObj = Object.assign({}, filter);

						if (filterObj[type])
							filterObj = filterObj[type];

						return {
							name: type+":"+filterObj.name,
							options: Object.entries(filterObj.options).reduce(
								(options, [key, filterFn]) => Object.assign(options, {
									[key]: EO => (EO.get("sar") === (type === "SAR")) && filterFn(EO)
								}), {})
						};
					})
				)
			);
	})(),
	search: {
		name : "EO image",
		keys: ["sensorType", "operationalMode", "serviceID", "platform", "sensorMode"],
		label: "serviceID"
	}
});

const updateFootprintsLayer = async (config) => {
	const footprintsFilters = config.footprints_filters || DEFAULT_FILTERS;
	return config.join_optical_and_sar ?
		[].concat(...footprintsFilters) :
		[].concat(...footprintsFilters
			.map(filter => ["SAR", "Optical"]
				.map(type => {
					let filterObj = Object.assign({}, filter);

					if (filterObj[type])
						filterObj = filterObj[type];

					return {
						name: type+":"+filterObj.name,
						options: Object.entries(filterObj.options).reduce(
							(options, [key, filterFn]) => Object.assign(options, {
								[key]: EO => (EO.get("sar") === (type === "SAR")) && filterFn(EO)
							}), {})
					};
				})
			)
		);
};

module.exports = {
	buildFootprintsLayer,
	updateFootprintsLayer
};

