const styleCache = {},
	{getNormalizedStatus} = require("./api.js"),

	getEOStyleInfo = footprint => ({
		status: getNormalizedStatus(footprint),
		selected: footprint.get("selected"),
		isSar: footprint.get("sar"),
		visible: footprint.get("visible")
	}),
	createEOStyle = (color, selected, dashed, backgroundColor) => new ol.style.Style({
		fill: new ol.style.Fill({
			color: selected?backgroundColor:"rgba(255,255,255,.3)"
		}),
		stroke: new ol.style.Stroke({
			color,
			width: selected?3:1,
			lineDash: dashed&&[10, 10]
		})
	}),
	isVisible = footprint => {
		if (typeof footprint.get("filteredInTTT") !== "undefined") {
			return footprint.get("filteredInTTT");
		} else if (!footprint.get("visible")) return false;
		return true;
	};

module.exports = footprint => {
	const {status, selected, isSar} = getEOStyleInfo(footprint),
		cacheKey = JSON.stringify({status, selected, isSar});

	if (!isVisible(footprint))
		return;

	if (!styleCache[cacheKey]) {
		let color, backgroundColor;

		switch (status) {
			case "PENDING":
				color = "rgb(255,102,0)";
				backgroundColor = "rgba(255,102,0,0.4)";
				break;
			case "TASKED":
				color = "rgb(0,0,0)";
				backgroundColor = "rgba(0,0,0,0.4)";
				break;
			case "DELIVERED":
				color = "rgb(0, 255, 0)";
				backgroundColor = "rgba(0,255,0,0.4)";
				break;
			case "ARCHIVED":
				color = "rgb(102, 0, 153)";
				backgroundColor = "rgba(102, 0, 153, 0.4)";
				break;
			default:
				color = "rgb(255,0, 0)";
				backgroundColor = "rgba(255,0,0,0.4)";
				break;
		}

		styleCache[cacheKey] = createEOStyle(color, selected, !footprint.get("sar"), backgroundColor);
	}

	return styleCache[cacheKey];
};
