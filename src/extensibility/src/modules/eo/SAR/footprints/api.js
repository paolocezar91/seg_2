const SATELLITES = require("./constants/satellites.json"),
	SENSOR_MODES = require("./constants/sensor_modes.json"),
	mapProperties = require("./properties.js");

const generateOptions = (options, withAll) => [...options.map(option => ({
	label: option,
	value: option
})), withAll ? {
	label: "All",
	value: null
} : undefined].filter(options => options);

const EO_STATUS = {
	CANCELLED: 	/cancelled/i,
	DELIVERED: 	/delivered|acquired/i,
	ARCHIVED: 	/archived/i,
	TASKED: 	/tasked/i,
	ANOMALY: 	/anomaly/i,
	ERROR: 		/error/i,
	PENDING:	/pending/i
};

const getNormalizedStatus = eo => {
	for (const key in EO_STATUS)
		if (EO_STATUS[key].test(eo.get("status")))
			return key;

	return "OTHER";
};

const EULA_MAP = {
	cosmo: "cosmoskymed",
	deimos: "deimos",
	pleiades: "pleiades",
	radarsat: "radarsat",
	sentinel: "sentinel",
	spot: "spot",
	terrasar: "terrasar",
	worldview: "worldview_geoeye",
	geoeye: "worldview_geoeye"
};

const getEULA = EO => {
	const satellite = EO.get("platform"),
		EULAkey = Object.keys(EULA_MAP).find(key => satellite.match(new RegExp(key, "i")));

	if(EULAkey)
		return EULA_MAP[EULAkey];
};

const downloadEOImage = EO => {
	const modal = seg.modal.openModal("Downloading EO Image", "Downloading EO Image "  + EO.get("doi") + ".zip", {class: "info"});
	seg.request.download("v1/eo/wcs", {params: {coverage: EO.get("doi")}}, EO.get("doi")+".zip").then(() => seg.modal.closeTimeout(modal.title));
};
const shouldHaveImage = eo => eo.get("doi") && /DELIVERED|ARCHIVED/.test(getNormalizedStatus(eo));

const mergeDuplicates = footprints => {
	return footprints.map(footprint => {

		const id = footprint.getId(),
			existing = seg.layer.get("eo_sar_imagery").getSource().getFeatureById(id);


		if (existing) {
			if (footprint.get("ts") > existing.get("ts"))
				existing.setProperties(Object.assign(existing.getProperties(), footprint.getProperties()));
			return existing;
		}

		footprint.set("totalOilSpills", footprint.get("oilSpills") || 0);
		footprint.unset("oilSpills");

		return footprint;
	});
};

const getOilspills = eo => {
	const oilSpillSource = seg.layer.get("oil_spills").getSource();

	return seg.request.get("v1/eo/wfs", {
		params: {
			serviceID: eo.get("serviceID"),
			//XXX
			begin_position: "2001-01-01T00:00:00Z",
			//XXX
			end_position: "2099-12-31T23:59:59Z"
		}
	}).then(res => {
		let oilSpills = (new ol.format.GeoJSON()).readFeatures(res.result, {
			featureProjection: seg.map.getProjection()
		});

		oilSpills = oilSpills.map(oilSpill => {
			//merge duplicates
			const existing = oilSpillSource.getFeatureById(oilSpill.getId());

			if(existing)
				oilSpill = existing;
			else {
				oilSpill.set("id", oilSpill.getId());

				const timestamp = oilSpill.get("timeStamp");

				if (timestamp && timestamp.year)
					oilSpill.set("timeStamp", moment({
						year: timestamp.year,
						month: timestamp.monthValue,
						day: timestamp.dayOfMonth,
						hour: timestamp.hour,
						minute: timestamp.minute,
						second: timestamp.second
					}).toISOString());
			}

			oilSpill.set("numberOfSlicks", oilSpill.getGeometry().getPolygons().length);

			return oilSpill;
		});

		eo.set("oilSpills", oilSpills);
		eo.set("totalOilSpills", oilSpills.length);

		oilSpillSource.addFeatures(oilSpills);
	}, e => seg.log.error("ERROR_FOOTPRINTS_GET_OILSPILLS", e.error + ": " + e.result));
};
const check = type => EO => EO.get("sar") && typeof EO.get("has:"+type) === "undefined" && seg.request.get("v1/eo/wms/"+type, {
	params: {
		REQUEST: "GetMap",
		LAYERS: EO.get("doi"),
		// OPERATION: EO.get("operations"),
		FORMAT: "image/png",
		VERSION: "1.1.0",
		SERVICE: "WMS",
		WIDTH: 1,
		HEIGHT: 1,
		BBOX: "-0.1,-0.1,0.1,0.1",
		SRS: "EPSG:4326",
		STYLES: ""
	}
}).then(res => EO.set("has:"+type, !res.startsWith("<?xml")));

const checkWaveWind = EO => seg.promise.all([check("wave")(EO), check("wind")(EO)]);

const getVDSTotal = (EO) => {
	if (EO.get("totalVDS"))
		return;

	if (EO.get("status") !== "Delivered"){
		EO.set("totalVDS", "N/A");
		return;
	}

	return seg.request.get("v1/eo/wfs/vds/hits", {
		params:{
			serviceID: EO.get("serviceID")
		}
	}).then(({result}) => {
		EO.set("totalVDS", result >= 0 ? "" + result : "N/A");
	}, e => {
		EO.set("totalVDS", "Error getting VDS total");
		seg.log.error("ERROR_FOOTPRINTS_GET_VDS_TOTAL", e.error + ": " + e.result);
	});
};

const buildFootprintsFilterMenu = footprintsLayer => {
	const footprintFilters = footprintsLayer.get("filters"),
		eoSarImagerySource = seg.layer.get("eo_sar_imagery").getSource(),
		filtersByType = footprintFilters.reduce((_filtersByType, filter) => {
			const type = filter.name.startsWith("SAR:")?"SAR":"Optical";

			return Object.assign(_filtersByType, {
				[type]: [..._filtersByType[type], Object.assign(filter, {
					name: filter.name
				})]
			});
		}, {
			SAR: [],
			Optical: []
		}),
		mapFromFilter = filter => {
			const fromFilter = seg.ui.layerMenu.fromFilter(footprintsLayer.get("id"), filter);
			fromFilter.bindings.filter.alias = fromFilter.bindings.filter.alias.replace(/(?:SAR|Optical):/, "");
			// fromFilter.alias = fromFilter.bindings.filter.alias;
			return fromFilter;
		},
		sarFilterOptions = filtersByType["SAR"].map(mapFromFilter),
		opticalFilterOptions = filtersByType["Optical"].map(mapFromFilter),
		getImageryFeatures = sar => eoSarImagerySource.getFeatures().filter(f => f.get("sar") === sar),
		createImageryMenu = (options, sar) => {
			const preferenceKey = "layers."+ footprintsLayer.get("id") + ".filters." + (sar ? "SAR" : "Optical") + " Imagery.enabled",
				imageryMenu = {
					template: "<label><span class=\"flex-1\">"+(sar?"SAR":"Optical")+" Imagery</span></label>",
					options,
					alias: (sar ? "SAR" : "Optical") + " Imagery",
					isChecked: () => {
						if (
							seg.preferences.map.get(preferenceKey) &&
							imageryMenu.options.every(option => option.isChecked())
						)
							return true;
					},
					isPartial: () => {
						if (
							seg.preferences.map.get(preferenceKey) &&
							imageryMenu.options.some(option => option.isChecked()) && imageryMenu.options.some(option => !option.isChecked())
						)
							return true;
					},
					toggle: (state) => {
						const imageryFeatures = getImageryFeatures(sar),
							currentState = imageryFeatures.length ? imageryFeatures.every(f => f.get("visible")) : seg.preferences.map.get(preferenceKey);

						// imageryMenu.bindings.active = state === undefined ? !currentState : state;
						seg.preferences.map.set(preferenceKey, state === undefined ? !currentState : state);

						imageryFeatures.forEach(f => f.set("visible", state === undefined ? !currentState : state));
					},
					bindings: {
						alias: (sar?"SAR":"Optical") + " Imagery",
						active: true
					},
					info: {
						title: (sar?"SAR":"Optical") + " Imagery",
						templateUrl: "extensibility/modules/eo/SAR/footprints/templates/"+(sar?"sar":"optical")+"_imagery_symbologies.html",
						bindings: {
							sections: require("./symbologies.js")[(sar?"sar":"optical")]
						}
					},
					type: "filter"
				};

			return imageryMenu;
		};

	return [
		createImageryMenu(sarFilterOptions, true), //sar
		createImageryMenu(opticalFilterOptions, false) //optical
	];
};
const DEFAULT_EO_SEARCH_FIELDS = {
	acquisitionStart: moment().subtract(90,"d"),
	acquisitionStop: moment(),
	showTime: false,
	platform: "",
	oil_spill: null,
	vds: null,
	ms_feedback: null,
	satelliteOptical: null,
	satelliteOpticalOptions: generateOptions(SATELLITES.optical, true),
	satelliteSAR: null,
	satelliteSAROptions: generateOptions(SATELLITES.sar, true),
	sensor_mode: null,
	sensorModeOptions: generateOptions(SENSOR_MODES, true),
	generalOptions: [{label:"Yes", value: true},{label:"No", value: false}, {label: "All", value: null}],
	generalOptionsWithoutAll: [{label:"Yes", value: true},{label:"No", value: false}],
	sar: false,
	optical:false,
	serviceId: null,
	selectedArea: []
};
const DEFAULT_EO_TABLE_FIELDS = mapProperties({override: {
	id: false,
	status: false,
	operation: false,
	DTOid: false,
	satelliteSensor: false,
	acquisitionStart: false,
	acquisitionStop: false,
	totalOilSpills: false,
	totalVDS: false,
	wasRecorrelated: false,
	lastRecorrelation: false,
	totalActivities: false,
	sensorType: false,
	sensorMode: false,
	sensorResolution: false,
	affectedAlertAreas: false,
	pass: false,
	pinpointing: false,
	polarization: false,
	coverageCompliance: false,
	usableArea: false,
	feedback: false,
	polygon: {
		visible: false
	}
}});
const DEFAULT_EO_DISPLAY_PREFERENCES = mapProperties({
	override: {
		id: false,
		status: false,
		operation: false,
		DTOid: false,
		satelliteSensor: false,
		acquisitionStart: false,
		acquisitionStop: false,
		totalOilSpills: false,
		totalVDS: false,
		wasRecorrelated: false,
		lastRecorrelation: false,
		totalActivities: false,
		sensorType: false,
		sensorMode: false,
		sensorResolution: false,
		affectedAlertAreas: false,
		pass: false,
		pinpointing: false,
		polarization: false,
		coverageCompliance: false,
		usableArea: false,
		feedback: false,
		polygon: {
			template: `<span style="display: block; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
				<span ng-repeat="coord in EOCommandInfo.getCoordinates(EO)">
					<span coordinate="[coord[1], 'lat']"></span>
					<span coordinate="[coord[0], 'lon']"></span>;
				</span>
			</span>`,
			visible: true
		}
	}
});
const imageLayerExists = EO => EO.get("thumbnail");
const hasImageLayer = EO => EO.get("imageLayerVisible");
const showImageLayer = EO => {
	if(hasImageLayer(EO))
		return;

	let imageLayer = EO.get("imageLayer");

	if(!imageLayer) {
		imageLayer = seg.layer.create({
			// had to add date because of unique ids
			// id: EO.get("doi") + "_image_" + Date.now(),
			// type: "Tile",
			// extent: EO.getGeometry().getExtent(),
			// source: {
			// 	url: "v1/eo/wms/image",
			// 	type: "TileWMS",
			// 	projections: ["EPSG:4326"],
			// 	params: {
			// 		LAYERS: EO.get("doi"),
			// 		FORMAT: "image/png",
			// 		VERSION: "1.1.0"
			// 	},
			// 	tileGrid: new ol.tilegrid.TileGrid({
			// 		tileSize: [seg.CONFIG.getValue("eo.TILE_SIZE"), seg.CONFIG.getValue("eo.TILE_SIZE")],
			// 		extent: seg.map.getProjection().getExtent(),
			// 		resolutions: seg.map.getResolutionsFromExtent(EO.getGeometry().getExtent())
			// 	})
			// }
			id: EO.get("doi") + "_image_" + Date.now(),
			type: "Image",
			extent: EO.getGeometry().getExtent(),
			visible: true,
			source: {
				url: "v1/eo/wms/image",
				type: "ImageWMS",
				projections: [
					"EPSG:4326",
					"EPSG:3395"
				],
				params: {
					LAYERS: EO.get("doi"),
					FORMAT: "image/png",
					VERSION: "1.1.0"
				},
				customLoadFunction: async (imgWrapper, url) => {
					const params = seg.utils.getParams(url),
						doi = params.LAYERS,
						transformation = seg.preferences.session.get("EO." + doi + ".imagetransformation") || {brightness: 0, contrast: 0, alpha: 255},
						{brightness, contrast, alpha} = transformation,
						data64 = (await seg.request.getDataURL(seg.utils.addParamsToURL(url, params)));

					if(brightness === 0 && contrast === 0 &&  alpha === 255) {
						imgWrapper.getImage().src = data64;
					} else {
						const canvas = document.getElementById("canvas"),
							img = new Image(params.WIDTH, params.HEIGHT);
						// if there is a canvas, image has already been transformed so we have to take the new image and add transformations to it
						if (canvas && img){
							img.onload = async () => {
								canvas.width = params.WIDTH;
								canvas.height = params.HEIGHT;
								canvas.getContext("2d").drawImage(img, 0, 0);
								const modal = seg.modal.openModal(EO.get("serviceID") + "_imageloadend", "Transforming " + EO.get("serviceID") + "'s image..."),
									canvastmp = seg.image.applyImageTransformation(canvas, brightness, contrast, alpha);

								if (canvastmp){
									imgWrapper.getImage().src = await canvastmp.toDataURL();
									// e.image.setImage(img);
								}
								modal.setContent(EO.get("serviceID") + "'s transformation complete.");
								seg.modal.closeTimeout(modal.title);
							};
							img.src = data64;
						}
					}
				}
			}
		});

		//add filter listener to layer
		const filterListener = () => imageLayer.setVisible(!EO.get("hidden"));

		imageLayer.set("filterListener", filterListener);
		imageLayer.setVisible(true);

		EO.on("change:hidden", filterListener);

		EO.set("imageLayer", imageLayer);
	}

	seg.layer.get("eo_sar_images").addLayer(imageLayer);

	EO.set("imageLayerVisible", true);
};

const hideImageLayer = EO => {
	if(!hasImageLayer(EO))
		return;

	const imageLayer = EO.get("imageLayer");

	if(imageLayer) {
		//remove filter listener
		EO.un("change:hidden", imageLayer.get("filterListener"));
		imageLayer.unset("filterListener");

		EO.unset("imageLayerVisible");
		seg.layer.get("eo_sar_images").removeLayer(imageLayer);
	}
};

const setPreferences = (opt, level = 0) => {
	const active = seg.preferences.map.get("layers.eo_sar_imagery.filters." + opt.alias + ".enabled");

	if (opt.bindings.filter) {
		opt.bindings.filter.setActive(active);
	}

	if (opt.options.length && level < 1)
		opt.options.forEach(optOptions => setPreferences(optOptions, level + 1));

	if (opt.options.length && level === 1)
		opt.options.forEach(optOptions => setPreferencesChildren(optOptions, opt.alias));
};


const setPreferencesChildren = (opt, parentAlias = null) => {
	let active;
	if (parentAlias) {
		active = seg.preferences.map.get("layers.eo_sar_imagery.filters." + parentAlias + ".options." + opt.alias);
	} else {
		active = seg.preferences.map.get("layers.eo_sar_imagery.filters." + opt.alias + ".enabled");
	}

	if (opt.bindings.filter) {
		opt.bindings.filter.setActive(active);
	}
	if (opt.options.length)
		opt.options.forEach(optOptions => setPreferencesChildren(optOptions, opt.alias));
};

const cachedCoordinates = {};
const getCoordinates = (EO) => {
	if (cachedCoordinates[EO.getId()])
		return cachedCoordinates[EO.getId()];

	cachedCoordinates[EO.getId()] = EO.getGeometry().getCoordinates()[0]
		.map(coord => ol.proj.transform(coord, seg.map.getProjection(), "EPSG:4326"));

	return cachedCoordinates[EO.getId()];
};

module.exports = {
	DEFAULT_EO_TABLE_FIELDS,
	DEFAULT_EO_DISPLAY_PREFERENCES,
	DEFAULT_EO_SEARCH_FIELDS,
	getNormalizedStatus,
	shouldHaveImage,
	mergeDuplicates,
	getOilspills,
	checkWaveWind,
	getEULA,
	getVDSTotal,
	downloadEOImage,
	buildFootprintsFilterMenu,
	imageLayerExists,
	hasImageLayer,
	showImageLayer,
	hideImageLayer,
	generateOptions,
	setPreferences,
	setPreferencesChildren,
	getCoordinates
};
