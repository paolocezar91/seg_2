const {shouldHaveImage, showImageLayer, hideImageLayer, DEFAULT_EO_TABLE_FIELDS, getCoordinates} = require("./api.js");

module.exports = (bboxes, from, to, footprints_table_fields) => {
	const bbox = bboxes[0],
		EOFootprintRequest = seg.request.get("v1/eo/catalogue", {
			params: angular.extend({
				begin_position: moment(from).toISOString(),
				end_position: moment(to).toISOString(),
				lower_corner: ol.extent.getBottomLeft(bbox).join(","),
				upper_corner: ol.extent.getTopRight(bbox).join(",")
			})
		}),
		loaderModal = seg.modal.openLoader("ACQ EO", "Retrieving EO Footprints...", {class: "info"});

	loaderModal.setPartialProgress("ACQ", 0);
	loaderModal.setDismissCallback(EOFootprintRequest.cancel);

	return EOFootprintRequest.then(({result}) => {
		result.crs.properties.name = "EPSG:4326"; // result.crs.properties.name.toUpperCase();
		// set modal progress
		loaderModal.setPartialProgress("ACQ", 100);

		const footprints = (new ol.format.GeoJSON()).readFeatures(result, {
			featureProjection: seg.map.getProjection()
		});

		//if no reports were retrieved, return
		if(!footprints.length){
			loaderModal.setContent("Retrieved "+footprints.length+" EO Footprints reports");
			loaderModal.setClass("no-result");
			seg.modal.closeTimeout(loaderModal.title);
			return;
		} else {
			loaderModal.setContent("Retrieved "+footprints.length+" EO Footprints reports.");
			loaderModal.setClass("success");
		}

		const footprintsSource = seg.layer.get("eo_sar_imagery").getSource(),
			features = [];

		footprints.forEach(footprint => {
			footprint.set("visible", true);
			const existFeature = footprintsSource.getFeatureById(footprint.getId());
			if (!existFeature) {
				const operations = footprint.get("operations");

				footprint.setProperties({
					old_eo: true,
					isSearchResult: true,
					totalOilSpills: footprint.get("oilSpills") || 0,
					operations: operations ? operations.split(",").filter((value, index, self) => self.indexOf(value) === index).join(", ") : ""
				}); // used to filter the features that will be removed when acq cleanable is activated
				footprint.unset("oilSpills");

				footprintsSource.addFeature(footprint);
				seg.ui.queryPanel.ACQ.addFeature(footprint);
				features.push(footprint);
			} else {
				seg.ui.queryPanel.ACQ.addFeature(existFeature);
				features.push(existFeature);
			}
		});

		const footprintACQTable = seg.ui.ttt.tables.open({
			label: "ACQ EO Acquisitions",
			data: features,
			itemAs: "EO",
			gridAPI:{
				getDistanceUnits:() => seg.preferences.workspace.get("distanceUnits"),
				getCoordinates
			},
			customToolbar: {
				template: "<span class=\"text-size-sm\">Number of EO Services: <b>{{grid.filteredResults.length}}</b></span>"
			},
			fields: footprints_table_fields || DEFAULT_EO_TABLE_FIELDS,
			onSelectionChange(changedRows, selectedRows) {
				seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
			}
		});

		seg.ui.queryPanel.ACQ.onClear(() => {
			seg.selection.select([]);
			seg.ui.ttt.tables.close(footprintACQTable);
			footprintsSource.getFeatures()
				.filter(feature => feature.get("old_eo"))
				.forEach(feature => {
					const imageLayer = feature.get("imageLayer");
					// if EO has image layer these should be removed too.
					if(imageLayer)
						seg.layer.get("eo_sar_images").removeLayer(imageLayer);
					footprintsSource.removeFeature(feature);
				});
		});

		seg.modal.closeTimeout(loaderModal.title);

		// ACQ should return object for timeline component
		let previouslySelectedFeature;
		return {
			cluster: true,
			marker: {
				template: `
				<div class="default-marker" ng-click="selectAndCenter(items)">
					<img src="extensibility/modules/eo/SAR/footprints/timeline_icon.png">
					<div style="background-color: #e55a00;" id="cluster-badge" ng-if="items.length > 1">{{items.length}}</div>
				</div>`,
				bindings: {
					selectAndCenter(items) {
						seg.selection.selectFeatureAndCenter(items[0].footprint);
					}
				}
			},
			// TOOLTIP TO BE REMOVED
			tooltip: {
				template: `
				<ul class="default-tooltip">
					<li ng-repeat="item in items">
						<label ng-click="selectAndCenter(item.footprint)">{{item.footprint.get('serviceID')}}</label>
					</li>
				</ul>`,
				bindings: {
					selectAndCenter: seg.selection.selectFeatureAndCenter
				}
			},
			items: features.map(footprint => {
				return {
					timestamp: moment(footprint.get("beginPosition")),
					footprint
				};
			}),
			onTimeChange(timestamp, before, after) {
				if(after.length)
					after.forEach(({footprint}) => hideImageLayer(footprint));

				if(previouslySelectedFeature) {
					seg.selection.deselect(previouslySelectedFeature);
					previouslySelectedFeature = null;
				}

				if(before.length) {
					const currentSelectedFeature = before[before.length - 1].footprint;

					if(currentSelectedFeature !== previouslySelectedFeature) {
						seg.selection.select(currentSelectedFeature, true);

						if(shouldHaveImage(currentSelectedFeature))
							showImageLayer(currentSelectedFeature);

						previouslySelectedFeature = currentSelectedFeature;
					}
				}
			}
		};
	}, e => {
		loaderModal.setContent("Retrieved 0 EO Footprints reports");
		loaderModal.setClass("no-result");
		seg.modal.closeTimeout(loaderModal.title);
		seg.log.error("ERROR_FOOTPRINTS_EO_FOOTPRINT_REQUEST", e.error + ": " + e.result);
	}, progress => loaderModal.setPartialProgress("ACQ", progress.loaded/progress.total*100));
};
