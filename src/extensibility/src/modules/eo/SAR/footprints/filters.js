const {getNormalizedStatus} = require("./api"),
	STATUS = require("./constants/status.json");

// const SENSORS = require("./constants/sensor_modes.json");

const createEOFilter = (name, values, compareFn) => ({
	name,
	options: values.reduce((options, value) => Object.assign(options, {
		[value]: EO => compareFn(EO, value)
	}), {})
});
const STATUS_FILTER = createEOFilter("Status", STATUS, (EO, status) => getNormalizedStatus(EO) === status.toUpperCase());
// const SENSOR_FILTER = createEOFilter("Sensor", SENSORS, (EO, sensor) => EO.get("operationalMode") === sensor);
const operationMatchFn = (EO, operation) => EO.get("operations").match(new RegExp(operation, "i"));

module.exports = {
	DEFAULT_FILTERS: [STATUS_FILTER],
	STATUS_FILTER,
	createEOFilter,
	operationMatchFn
};
