const defaultProperties = {
	id: {
		label: "Service ID",
		template: "{{::EO.get('serviceID')}}",
		visible: true,
		order:0
	},
	status: {
		label: "Status",
		template: "{{::EO.get('status') || 'N/A'}}",
		visible: true,
		order: 1
	},
	operation: {
		label: "EO Operation",
		template: "{{::EO.get('operations').replace(',',';') || 'N/A'}}",
		visible: true,
		order: 2
	},
	DTOid: {
		label: "DTO ID",
		template: "{{::EO.get('dataTakeOpportunityID') || 'N/A'}}",
		visible: true,
		order: 3
	},
	satelliteSensor: {
		label: "Satellite",
		template: "{{::EO.get('platform')}}",
		visible: true,
		order:4
	},
	acquisitionStart: {
		label: "Acquisition Start",
		template: "<span position-timestamp=\"::EO.get('beginPosition')\"></span>",
		visible: true,
		order:5
	},
	acquisitionStop: {
		label: "Acquisition Stop",
		template: "<span position-timestamp=\"::EO.get('endPosition')\"></span>",
		visible: true,
		order:6
	},
	totalOilSpills: {
		label: "Total oil spills",
		template: "{{ (EO.get('oilSpills') && EO.get('oilSpills').length) || EO.get('totalOilSpills') || '0'}}",
		visible: true,
		order:7
	},
	totalVDS: {
		label: "Total VDS",
		template: "{{ (EO.get('detectedVessels') && EO.get('detectedVessels').length) || EO.get('totalVDS') || 'Click to fill'}}",
		visible: true,
		order: 8
	},
	wasRecorrelated: {
		label: "VDS Re-correlated",
		template: "{{ EO.get('recorrelationInfo') ? 'Yes' : 'No' }}",
		visible: true,
		order: 9
	},
	lastRecorrelation: {
		label: "Last Re-correlation Date",
		template: "{{ EO.get('recorrelationInfo') ? (EO.get('recorrelationInfo').dateResponse.toISOString().split('.')[0]+'Z') : 'N/A' }}",
		visible: true,
		order: 10
	},
	totalActivities: {
		label: "Total activities",
		template: "{{::EO.get('totalActivities') || '0'}}",
		visible: true,
		order: 11
	},
	sensorType: {
		label: "Sensor type",
		template: "{{::EO.get('sensorType') || 'N/A'}}",
		visible: true,
		order:12
	},
	sensorMode: {
		label: "Sensor mode",
		template: "{{::EO.get('sensorMode') !== 'null' ? EO.get('sensorMode') : 'N/A'}}",
		visible: true,
		order:13
	},
	sensorResolution: {
		label: "Sensor resolution (m)",
		template: "{{ (EO.get('sensorResolution') && EO.get('sensorResolution') | number: 1) || 'N/A'}}",
		visible: true,
		order:14
	},
	affectedAlertAreas: {
		label: "Affected areas",
		template: "{{::EO.get('affectedAlertAreas').length ? EO.get('affectedAlertAreas').join('; ') : 'N/A'}}",
		visible: true,
		order: 15
	},
	pass: {
		label: "Pass",
		template: "{{::EO.get('orbitDirection') || 'N/A'}}",
		visible: true,
		order:16
	},
	pinpointing: {
		label: "Pinpointing",
		template: "N/A",
		visible: true,
		order: 17
	},
	polarization: {
		label: "Polarization",
		// template: "{{::EO.get('sar') || 'N/A'}}",
		template: "N/A",
		visible: true,
		order: 18
	},
	coverageCompliance: {
		label: "Coverage",
		template: "{{::EO.get('coverageCompliance') || 'N/A'}}",
		visible: true,
		order: 19
	},
	usableArea: {
		label: "Usable Area %",
		template: "{{::EO.get('usableArea')  || 'N/A'}}",
		visible: true,
		order: 20
	},
	feedback: {
		label: "Feedback",
		template: "N/A",
		visible: true,
		order: 21
	},
	polygon: {
		label: "Polygon",
		template: "<span class=\"cut-150\"><span ng-repeat=\"coord in ::getCoordinates(EO)\"><span coordinate=\"::[coord[1], 'lat']\"></span> <span coordinate=\"::[coord[0], 'lon']\"></span>; </span></span>",
		visible: true,
		order: 22
	}
	// orbitNumber: {
	// 	label: "Orbit Number",
	// 	template: "{{::EO.get('orbitNumber') || 'N/A'}}",
	// 	visible: true,
	// 	order: 9
	// },
	// bandCombination: {
	// 	label: "Band Combination",
	// 	template: "{{::EO.get('productType') || 'N/A'}}",
	// 	visible: true,
	// 	order: 10
	// },
	// availableBands: {
	// 	label: "Available Bands",
	// 	template: "{{::EO.get('sar') || 'N/A'}}",
	// 	visible: true,
	// 	order: 13
	// },
	// filename: {
	// 	label: "Filename",
	// 	template: "{{::EO.get('filename')}}",
	// 	visible: true,
	// 	order: 21
	// },
	// satelliteOperator: {
	// 	label: "Satellite operator",
	// 	template: "{{::EO.get('serviceProvider1')}}",
	// 	visible: true,
	// 	order: 22
	// },
	// serviceProvider: {
	// 	label: "Service provider",
	// 	template: "{{::EO.get('serviceProvider2')}}",
	// 	visible: true,
	// 	order: 24
	// }
};

const mapProperties = (options) => seg.utils.mapItemProperties(defaultProperties, options);

module.exports = mapProperties;