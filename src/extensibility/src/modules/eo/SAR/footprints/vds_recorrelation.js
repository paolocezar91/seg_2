let popUpConfig;

const recorrelateVDS = (EO) => {
		let cancelAnimationFrame_ = 0;

		popUpConfig = {
			title: "Re-correlation process",
			template: `
				<div style="max-width: 350px;">
					<div ng-init="init(EO)">
						<div style="margin-bottom:10px;" ng-if="EO.get('recorrelationInfo').tc && EO.get('recorrelationInfo').tn && !isAfter(EO.get('recorrelationInfo').tn)">
							<p ng-if="!isBefore(EO.get('recorrelationInfo').tc)">
								<span class="text-color-12"><strong>Re-correlation available</strong></span><br><br>
							</p>
							<p ng-if="isBefore(EO.get('recorrelationInfo').tc)">
								<span class="text-color-warning"><strong>Re-correlation process is currently not available.</strong></span><br><br>
							</p>
							<p style="margin-bottom: 10px" ng-if="isBefore(EO.get('recorrelationInfo').tc)">
								Time before new re-correlation is available is: <br>
								<span><strong id="tc"></strong></span>
							</p>
						</div>
						<div style="margin-bottom:10px;" ng-if="EO.get('recorrelationInfo').tn">
							<p ng-if="!isAfter(EO.get('recorrelationInfo').tn)">
								Remaining time before re-correlation window is closed: <br>
								<span><strong id="tn"></strong></span>							
							</p>
							<p ng-if="isAfter(EO.get('recorrelationInfo').tn)" class="text-color-warning">
								<span><strong>Re-correlation not available</strong></span>
							</p>
						</div>
						<div style="margin-bottom:10px;" ng-if="EO.get('recorrelationInfo').tn && EO.get('recorrelationInfo').tc && !isAfter(EO.get('recorrelationInfo').tn)">
							Last re-correlation was at: <strong>{{ EO.get('recorrelationInfo').dateResponse.format("YYYY-MM-DD HH:mm:ss") }}</strong>.<br>
							STATUS: <strong>{{ EO.get('recorrelationInfo').pckStatus}}</strong><br>
							MESSAGE: <strong>{{ EO.get('recorrelationInfo').pckStatusMsg}}</strong>
						</div>
						<div ng-if="EO.get('recorrelationInfo') === null || !isRecorrelateVDSDisabled(EO)">
							Export the current results as these will be overwritten when running the re-correlation process. <br>
							Do you want to run a re-correlation?
						</div>
					</div>
				</div>
				<div class="flexbox" style="margin-top:15px" ng-if="EO.get('recorrelationInfo') === null || !isRecorrelateVDSDisabled(EO)">
					<button title="YES" class="seg positive rounded md" ng-click="recorrelate(EO)" style="margin-right:5px;">YES</button>
					<button title="NO" class="seg positive-reverse rounded md" ng-click="close()">NO</button>
				</div>`,
			bindings: {
				EO,
				tcCount: null,
				tnCount: null,
				init(EO) {
					getRecorrelationInfo(EO).then(() => {
						let recorrelationInfo = EO.get("recorrelationInfo");
						const countdownToString = (time) => time.clone().countdown(moment()/*, window.countdown.DAYS | window.countdown.HOURS | window.countdown.MINUTES*/);

						(function timerLoop() {
							recorrelationInfo = EO.get("recorrelationInfo");

							if(recorrelationInfo){
								popUpConfig.bindings.tcCount = countdownToString(recorrelationInfo.tc);
								popUpConfig.bindings.tnCount = countdownToString(recorrelationInfo.tn);

								if(popUpConfig.bindings.tcCount.value < 0 && document.getElementById("tc"))
									document.getElementById("tc").innerHTML = popUpConfig.bindings.tcCount.toString();

								if(popUpConfig.bindings.tnCount.value < 0 && document.getElementById("tn"))
									document.getElementById("tn").innerHTML = popUpConfig.bindings.tnCount.toString();
							}

							if(!cancelAnimationFrame_)
								requestAnimationFrame(timerLoop);
							else
								requestAnimationFrame(() => {
									// we need this to trigger $digest cycle and update the popup
									try{seg.$apply();}catch(e){e;}
								});
						})();
					});
				},
				close() {
					cancelAnimationFrame_ = true;
					seg.popup.close("Re-correlation process");
				},
				isRecorrelateVDSDisabled,
				isBefore(time){
					return moment().clone().isBefore(time);
				},
				isAfter(time){
					return moment().clone().isAfter(time);
				},
				recorrelate(EO) {
					const modal = seg.modal.openModal("RE_CORRELATION", "Requesting re-correlation...");
					seg.ui.cleanables.call("Clear VDS");

					seg.request.get("v1/vds/recorrelate", {
						params:{
							package: EO.get("doi")
						}
					}).then(({result}) => {
						seg.popup.close("Re-correlation process");
						modal.setContent("Re-correlation successful, however you will not be able to see new results until the \"Show VDS\" is available.");
						seg.modal.closeTimeout(modal.title, 7000);
						updateRecorrelationInfo(EO, result);
					}, e => {
						modal.setContent("Re-correlation unsuccessful: " + ((e.originalRes.pck && e.originalRes.pck.errorMessage) || e.error || e.message));
						seg.modal.closeTimeout(modal.title, 7000);
						updateRecorrelationInfo(EO, e.originalRes);
					});
				}
			}
		};
		const popup = seg.popup.openPopup(popUpConfig);
		popup.setDismissCallback(() => {
			cancelAnimationFrame_ = true;
		});
	},
	isRecorrelateVDSDisabled = (EO) => {
		const recorrelationInfo = EO.get("recorrelationInfo");

		if(recorrelationInfo){
			// If info is not available yet, consider it disabled
			if(recorrelationInfo.pckStatus === "IN_PROGRESS")
				return true;

			if(recorrelationInfo.pckStatus && (
				popUpConfig.bindings.isBefore(recorrelationInfo.tc) ||
				popUpConfig.bindings.isAfter(recorrelationInfo.tn)
			))
				return true;
		}

		// else, it is enabled
		return undefined;
	},
	getRecorrelationInfo = (EO) => {
		// We get info about the EO package
		return seg.request.get("v1/vds/recorrelate/info", {
			params:{
				package: EO.get("doi")
			}
		}).then(({result}) => {
			// and set the info to it
			updateRecorrelationInfo(EO, result);
		});
	},
	updateRecorrelationInfo = (EO, info) => {
		//  here we take the serverTimestamp and add the tc and tn to it.
		//  tc refers to the time to next recorrelation is available
		//  tn referes to the time recorrelation is no longer available
		let recorrelationInfo =  null;

		if(info.pck) {
			recorrelationInfo = {
				pckStatus: info.pck.pckStatus,
				pckStatusMsg: "",
				dateResponse: moment(info.pck.dateResponse),
				tc: moment(info.pck.dateResponse).clone().add(info.tc, "seconds"),
				tn: moment(info.pck.dateResponse).clone().add(info.tn, "seconds")
			};

			switch(recorrelationInfo.pckStatus){
				case "SUCCESS":
					recorrelationInfo.pckStatusMsg = "Last re-correlate process was successful. Please wait 1 minute for Show VDS button to be available again.";
					break;
				case "INTERNAL_FAILURE":
					recorrelationInfo.pckStatusMsg = "There was an internal failure in the re-correlate process";
					break;
				case "ERROR":
					recorrelationInfo.pckStatusMsg = "There was an error in the re-correlate process";
					break;
				case "WARNING":
					recorrelationInfo.pckStatusMsg = "There was a warning in the re-correlate process";
					break;
				case "IN_PROGRESS":
					recorrelationInfo.pckStatusMsg = "Re-correlation is in progress";
					break;
			}
		}
		EO.set("recorrelationInfo", recorrelationInfo);
	},
	isVDSButtonDisabled = (EO) => {
		const recorrelationInfo = EO.get("recorrelationInfo");

		if(!recorrelationInfo)
			return undefined;

		if(recorrelationInfo && !recorrelationInfo.dateResponse || recorrelationInfo.pckStatus !== "SUCCESS")
			return undefined;

		// check if: NOW is BEFORE the time of the response given by recorrlation service plus 1 minute
		return moment().isBefore(recorrelationInfo.dateResponse.clone().add(1, "minute")) ? true : undefined;
	};

module.exports = {
	recorrelateVDS, getRecorrelationInfo, isRecorrelateVDSDisabled, isVDSButtonDisabled
};