const {
		checkWaveWind,
		getOilspills,
		getEULA,
		getVDSTotal,
		downloadEOImage,
		getNormalizedStatus,
		imageLayerExists,
		showImageLayer,
		hasImageLayer,
		hideImageLayer,
		DEFAULT_EO_DISPLAY_PREFERENCES,
		cachedCoordinates = {}
	} = require("./api"),
	{ getDetectedVessels } = require("../../VDS/api"),
	{ DEFAULT_OILSPILLS_TABLE_FIELDS, getLabel, getOperations } = require("../oil_spills/api"),
	{ recorrelateVDS, getRecorrelationInfo, isRecorrelateVDSDisabled, isVDSButtonDisabled } = require("./vds_recorrelation"),
	init = EO => {
		seg.promise.all([
			getRecorrelationInfo(EO),
			getImage(EO),
			checkWaveWind(EO),
			getVDSTotal(EO)
		]);
	},
	getImage = EO => {
		if (EO.get("thumbnail"))
			return;

		const transformedExtent = ol.proj.transformExtent(EO.getGeometry().getExtent(), seg.map.getProjection(), "EPSG:4326");

		// if in (acquired or archived) status get the image, else set the default image
		if (EO.get("status") && require("./api.js").shouldHaveImage(EO))
			seg.request.getDataURL("v1/eo/wms/image", {
				params: {
					REQUEST: "GetMap",
					LAYERS: EO.get("doi"),
					// OPERATION: EO.get("operations"),
					FORMAT: "image/png",
					VERSION: "1.1.0",
					SERVICE: "WMS",
					WIDTH: 300,
					HEIGHT: 300,
					BBOX: transformedExtent.join(","),
					STYLES: "",
					SRS: "EPSG:4326"
				},
				headers: {
					"Content-Type": "image/png"
				}
			}).then(thumbnail => EO.set("thumbnail", thumbnail), e => seg.log.error("ERROR_OILSPILLS_INSERT_GET_IMAGE", e.error + ": " + e.result));
	},
	syncEOFilterOutWithProducts = (val, features) => features.forEach(os => os.set("wasSelected", val));

module.exports = config => ({
	init,
	imageLayerExists,
	showImageLayer,
	hideImageLayer,
	hasImageLayer,
	isVDSButtonDisabled,
	toggleImageLayer(EO) {
		if (hasImageLayer(EO))
			this.hideImageLayer(EO);
		else this.showImageLayer(EO);
	},
	isEODelivered: (EO) => /DELIVERED/.test(getNormalizedStatus(EO)),
	recorrelateVDS,
	getRecorrelationInfo,
	isRecorrelateVDSDisabled,
	hasDetectedVessels(EO) {
		return !(!EO.get("detectedVessels") || !EO.get("detectedVessels").length);
	},
	downloadActive(EO) {
		// if (!EO.get("serviceType"))
		// 	return false;

		// const availableFunctions = EO.get("serviceType").serviceElementsServiceProvider1.concat(",").concat(EO.get("serviceType").serviceElementsServiceProvider2).split(",");

		// const exists = (availableFunctions.indexOf("Data Downlink") !== -1) ? true : false;

		if (EO.get("doi") && EO.get("status").match(/delivered/i))
			return true;

		return false;
	},
	getImageToChange(EO) {
		const imageLayer = EO.get("imageLayer");

		if (imageLayer) {
			return imageLayer;
		} else
			return null;
	},
	alertReportAvailable: EO => {
		if (!EO.get("status").match(/delivered|archived|acquired/i) || !EO.get("sar"))
			return "disabled";
	},
	toggleSarWind: (EO) => {
		let windLayer = EO.get("windLayer");

		if (windLayer) {
			//remove filter listener
			EO.un("change:hidden", windLayer.get("filterListener"));
			windLayer.unset("filterListener");

			EO.unset("windLayer");
			seg.layer.get("eo_sar_wind").removeLayer(windLayer);
			return;
		}

		const loaderModal = seg.modal.openModal("SAR WIND", "Retrieving SAR WIND...", {class: "info"});

		windLayer = seg.layer.create({
			id: EO.get("doi") + "_sar_wind_" + Date.now(),
			type: "Tile",
			extent: EO.getGeometry().getExtent(),
			source: {
				url: "v1/eo/wms/wind",
				type: "TileWMS",
				projections: ["EPSG:4326"],
				params: {
					REQUEST: "GetMap",
					LAYERS: EO.get("doi"),
					FORMAT: "image/png",
					VERSION: "1.1.0",
					transparent: true,
					UPSEQUENCE: moment(new Date()).toISOString(),
					SRS: "EPSG:4326",
					STYLES: ""
				}
			}
		});

		//add filter listener to layer
		const filterListener = () => windLayer.setVisible(!EO.get("hidden"));

		windLayer.set("filterListener", filterListener);
		EO.on("change:hidden", filterListener);

		seg.layer.get("eo_sar_wind").addLayer(windLayer);
		EO.set("windLayer", windLayer);

		seg.timeout(() => {
			loaderModal.setContent("SAR Wind retrieved");
			seg.modal.closeTimeout(loaderModal.title);
		}, 1000);
	},
	toggleSarSwell: (EO) => {
		let swellLayer = EO.get("swellLayer");

		if (swellLayer) {
			//remove filter listener
			EO.un("change:hidden", swellLayer.get("filterListener"));
			swellLayer.unset("filterListener");

			EO.unset("swellLayer");
			seg.layer.get("eo_sar_swell").removeLayer(swellLayer);
			return;
		}

		const loaderModal = seg.modal.openModal("SAR SWELL", "Retrieving SAR SWELL...", {class: "info"});

		swellLayer = seg.layer.create({
			id: EO.get("doi") + "_sar_swell_" + Date.now(),
			type: "Tile",
			extent: EO.getGeometry().getExtent(),
			source: {
				url: "v1/eo/wms/wave",
				type: "TileWMS",
				projections: ["EPSG:4326"],
				params: {
					REQUEST: "GetMap",
					LAYERS: EO.get("doi"),
					FORMAT: "image/png",
					VERSION: "1.1.0",
					UPSEQUENCE: moment(new Date()).toISOString(),
					SRS: "EPSG:4326",
					STYLES: ""
				}
			}
		});

		//add filter listener to layer
		const filterListener = () => swellLayer.setVisible(!EO.get("hidden"));

		swellLayer.set("filterListener", filterListener);
		EO.on("change:hidden", filterListener);

		seg.layer.get("eo_sar_swell").addLayer(swellLayer);
		EO.set("swellLayer", swellLayer);

		seg.timeout(() => {
			loaderModal.setContent("SAR Swell retrieved.");
			seg.modal.closeTimeout(loaderModal.title);
		}, 1000);
	},
	showLegend: (title) => {
		seg.popup.openPopup({
			title: title + " legend",
			template: "<include template-url={{legendURL}}></include>",
			bindings: {
				legendURL:"extensibility/modules/eo/SAR/other/templates/swells_legend.html"
			}
		});
	},
	toggleVDS: async (eo) => {
		if (eo.get("detectedVessels")) {
			eo.get("detectedVessels").forEach(detected => {
				if (seg.source.findFeature(seg.layer.get("detected_vessels").ol_.getSource(), detected))
					seg.layer.get("detected_vessels").ol_.getSource().removeFeature(detected);
			});
			eo.unset("detectedVessels");
		} else{
			const {eodcData, owsData} = await getDetectedVessels({
				correlated: true,
				uncorrelated: true,
				eo,
				sar: eo.get("sar"),
				correlated_fields: config.vds_correlated_fields
			});

			eo.on("change:wasSelected", ({target}) => syncEOFilterOutWithProducts(target.get("wasSelected"), owsData));

			if (eodcData) {
				eodcData.forEach(eodc => {
					const found = seg.layer.get("detected_vessels").getSource().getFeatures().find(vds => eodc.properties.id === vds.get("CSN_ID"));

					if (found)
						found.set("shipThumbnail", eodc.properties.shipThumbnail);
				});
			}
		}
	},
	toggleActivity: (footprint) => {
		const detectedActivitySource = seg.layer.get("eo_detected_activity").getSource();

		const cleanActivityFromEO = () => {
			const detectedFeatures = footprint.get("detectedActivity") || [];

			detectedFeatures.forEach(detected => {
				if (seg.source.findFeature(detectedActivitySource, detected))
					detectedActivitySource.removeFeature(detected);
			});

			seg.ui.ttt.tables.close(footprint.get("detectedActivityTableRef_"));
			footprint.unset("detectedActivity");
			footprint.unset("detectedActivityTableRef_");

			let clearFunctions = seg.preferences.session.get("EOWithActivity") || [];

			for (let i = 0; i < clearFunctions.length - 1; i++) {
				if (clearFunctions[i].eoId === footprint.getId()) {
					clearFunctions = clearFunctions.splice(i, 1);
				}
			}

			if (clearFunctions.length > 0) {
				seg.preferences.session.set("EOWithActivity", clearFunctions);
			} else {
				seg.preferences.session.set("EOWithActivity", undefined);
				seg.ui.cleanables.remove("Clear Activity");
			}
		};

		const clearFunctions = seg.preferences.session.get("EOWithActivity") || [];

		if (footprint.get("detectedActivity")) {
			cleanActivityFromEO();
		} else {
			const loaderModal = seg.modal.openModal("EO Detected Activity", "Retrieving detected activity...", {class: "info"});

			seg.request.get("v1/eo/wfs/activity?", {
				params: {
					serviceId: footprint.get("serviceID")
				}
			}).then(({result}) => {
				const detectedActivites = (new ol.format.GeoJSON()).readFeatures(result, {
					featureProjection: seg.map.getProjection()
				});

				if (!detectedActivites.length)
					loaderModal.setContent("No activity found");
				else {
					footprint.on("change:wasSelected", ({target}) => syncEOFilterOutWithProducts(target.get("wasSelected"), detectedActivites));

					detectedActivites.forEach((detectedActivity) => {
						const center = ol.proj.transform(ol.extent.getCenter(detectedActivity.getGeometry().getExtent()), seg.map.getProjection(), "EPSG:4326");
						detectedActivity.setProperties({
							lon: center[0],
							lat: center[1],
							sar: footprint.get("sar"),
							serviceId: footprint.get("serviceID")
						});
					});
					detectedActivitySource.addFeatures(detectedActivites);
					//Add to ttt
					const table = seg.ui.ttt.tables.open({
						label: "EO Detected Activity",
						data: detectedActivites,
						gridAPI: {
							getLabel: require("../activity/command_info.scope").getLabel
						},
						itemAs: "detectedActivity",
						fields: require("../activity/properties")({
							override: {
								serviceId: false,
								id: {
									template: "{{getLabel(detectedActivity)}}"
								},
								timeStamp: false,
								activityDescription: false,
								activityType: false,
								activitySubType: false,
								latitude: false,
								longitude: false,
								activityConfidenceLevel: false,
								positionAccuracyVectorX: false,
								positionAccuracyVectorY: false
							}
						}),
						onSelectionChange(changedRows, selectedRows) {
							seg.selection.selectFeatureAndCenter(selectedRows.map(row => {
								row.item.set("visible", true);
								return row.item;
							}));
						}
					});

					footprint.setProperties({
						"detectedActivityTableRef_": table,
						"detectedActivity": detectedActivites
					});

					loaderModal.setContent("Found " + detectedActivites.length + " Activities");

					clearFunctions.push({
						eoId: footprint.getId(),
						execute: cleanActivityFromEO
					});

					const clearFunction = seg.ui.cleanables.get("Clear Activity");
					seg.preferences.session.set("EOWithActivity", clearFunctions);

					if (typeof clearFunction === "undefined") {
						seg.ui.cleanables.add("Clear Activity", () => {
							const functions = seg.preferences.session.get("EOWithActivity") || [];

							functions.forEach((clear) => {
								clear.execute();
							});
							//seg.ui.cleanables.remove("Clear Activity");
						});
					}
				}
				seg.modal.closeTimeout(loaderModal.title);
			},
			(e) => {
				seg.log.error("ERROR_OILSPILLS_INSERT_TOGGLE_ACTIVITY", e.error + ": " + e.result);
				loaderModal.setContent("Error retrieving Activity");
				seg.modal.closeTimeout(loaderModal.title);
			});
		}
	},
	openReport: () => {
		seg.ui.apps.get("Report").bindings.close();
		seg.timeout(() => seg.ui.apps.open("Report"), 500);
	},
	getLabelForShowOrHideOilSpills(feature) {
		const oilSpills = feature.get("oilSpills");
		return (Array.isArray(oilSpills) && !!oilSpills.length && oilSpills.every(os => os.get("visible"))) ? "Hide Potential OS" : "Show Potential OS";
	},
	async toggleOilSpills(eo) {
		let oilSpills = eo.get("oilSpills"),
			visible;


		if(!oilSpills) {
			const loader = seg.modal.openModal("EO " + eo.getId() + " Oilspills", "Retrieving detected oilpills...", {class: "info"});

			await getOilspills(eo);
			oilSpills = eo.get("oilSpills");

			loader.setContent(oilSpills.length + " Oilspills Retrieved");
			seg.modal.closeTimeout(loader.title);
		}
		else visible = oilSpills.every(os => os.get("visible"));

		oilSpills.forEach(os => os.set("visible", !visible));
		eo.on("change:wasSelected", ({target}) => syncEOFilterOutWithProducts(target.get("wasSelected"), oilSpills));

		if(!visible && oilSpills.length) {
			// let eoOilSpillTableSelectionListener;

			seg.ui.ttt.tables.open({
				label: "EO " + eo.get("serviceID") + " Oil Spills",
				data: oilSpills,
				itemAs: "oilSpill",
				gridAPI:{
					getLabel,
					getOperations,
					getDistanceUnits:() => seg.preferences.workspace.get("distanceUnits"),
					getAreaUnits: () => seg.preferences.workspace.get("areaUnits")
				},
				fields: config.oil_spills_table_fields || DEFAULT_OILSPILLS_TABLE_FIELDS,
				onSelectionChange(changedRows, selectedRows) {
					seg.selection.selectFeatureAndCenter(selectedRows.map(row => {
						row.item.set("visible", true);

						return row.item;
					}));
				}
			});

			seg.ui.cleanables.add("Clear Potential Oilspills", () => {
				oilSpills = eo.get("oilSpills");
				visible = oilSpills.every(os => os.get("visible"));
				oilSpills.forEach(os => os.set("visible", !visible));
			});
		}
	},
	//private property
	displayPreferences_: config.footprints_fields || DEFAULT_EO_DISPLAY_PREFERENCES,
	get displayPreferences() {
		const self = this;
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("EO.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach((key) => {
			if (self.displayPreferences_[key])
				self.displayPreferences_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayPreferences_;
	},
	toggleDisplayPreference(key) {
		//const self = this;
		//toggle the passed in preference
		this.displayPreferences[key].visible = !this.displayPreferences[key].visible;

		//we only want to save the "visible" property, the rest is always the same
		seg.preferences.workspace.set("EO.commandInfo.displayOptions." + key, this.displayPreferences[key].visible);
	},
	hasOilspills: (EO) => {
		if (!EO.get("oilpills") || (EO.get("oilpills") && !EO.get("oilpills").length))
			return "disabled";
	},
	download_EO(EO) {
		const EULA = getEULA(EO);
		if(EULA)
			seg.popup.openPopup({
				title: "Download EO",
				template: `
					<include template-url={{disclaimerURL}}></include>
					<a ng-href="{{EULA}}" target="_blank">EULA download</a>
					<div class="flexbox" style="margin-top:15px">
						<button title="Download" class="seg positive rounded md" ng-click="download()" style="margin-right:5px;">Yes</button>
						<button title="Cancel Download"class="seg positive rounded md" ng-click="close()">No</button>
					</div>`,
				bindings: {
					disclaimerURL: "extensibility/modules/eo/SAR/footprints/templates/EO_download_disclaimer.html",
					EULA: "extensibility/modules/eo/SAR/footprints/EULA/" + EULA + ".pdf",
					close() {
						seg.popup.close("Download EO");
					},
					download() {
						seg.popup.close("Download EO");
						downloadEOImage(EO);
					}
				}
			});
		else downloadEOImage(EO);
	},
	functionState(functionName, EO) {
		const status = EO.get("status").toUpperCase();

		if (typeof EO.get("serviceType") === "undefined")
			return "disabled";

		const availableFunctions = EO.get("serviceType").serviceElementsServiceProvider1.concat(",").concat(EO.get("serviceType").serviceElementsServiceProvider2).split(","),
			exists = !!~availableFunctions.indexOf(functionName);

		return ((status === "DELIVERED" || status === "ACQUIRED" || status === "ARCHIVED") && exists) || "disabled";
	},
	alertReportDownload: (feature) => {
		const modal = seg.modal.openModal("Alert Report", "Downloading Alert Report...", {class: "info"});

		seg.request.download("v1/eo/alert_report/zip", {
			params: {
				serviceId: feature.get("serviceID")
			}
		}, feature.getId() + "_alert_report.zip").then((res) => {
			if(res.tag === "ERROR_BACKEND_DOWNLOAD"){
				modal.setContent("Download Failed");
				seg.modal.closeTimeout(modal.title);
				seg.log.error("ERROR_FOOTPRINT_ALERT_REPORT_DOWNLOAD", res.tag + ": " + res.message);
			} else {
				modal.setContent("Download successful");
				seg.modal.closeTimeout(modal.title);
			}
		}, e => {
			modal.setContent("Download Failed");
			seg.modal.closeTimeout(modal.title);
			seg.log.error("ERROR_FOOTPRINT_ALERT_REPORT_DOWNLOAD", e.error + ": " + e.result);
		});
	},
	cropCoordinates(coordArr) {
		const distance = (a,b) => Math.sqrt((a.x - b.x)**2 + (a.y - b.y)**2),
			is_between = (a,b,c) => Math.abs((distance(a,c) + distance(c,b)) - distance(a,b)) < 0.001;

		for(let i = 0; i < (coordArr.length - 1); i++){
			for( let j = i + 1; j < coordArr.length; j++){
				if ( (coordArr[i][0] === coordArr[j][0]) && (coordArr[i][1] === coordArr[j][1]) ) {
					coordArr.splice(j, 1);
				}
			}
		}
		coordArr.forEach(a => {
			const ax = a[1],
				ay = a[0],
				aa = {x: ax, y: ay};

			coordArr.forEach(b => {
				const bx = b[1],
					by = b[0],
					ba = {x:bx, y:by};

				coordArr.forEach(c => {
					const cx = c[1],
						cy = c[0],
						ca = {x: cx, y: cy};

					if(!(ax === bx && ay === by) && !(ax === cx && ay === cy) && !(bx === cx && by === cy))
						if(is_between(aa, ba, ca))
							for(let i = 0; i < coordArr.length; i++)
								if (coordArr[i] === c)
									coordArr.splice(i, 1);
				});
			});
		});

		return coordArr;
	},
	getCoordinates(EO){
		if (!cachedCoordinates[EO.getId()])
			cachedCoordinates[EO.getId()] = this.cropCoordinates(EO.clone().getGeometry().getCoordinates()[0].map(coord => ol.proj.transform(coord, seg.map.getProjection(), "EPSG:4326")));

		return cachedCoordinates[EO.getId()];
	}
});