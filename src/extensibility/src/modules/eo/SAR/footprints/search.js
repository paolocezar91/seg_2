let footprintsSource;
const {
	/*mergeDuplicates,*/
	DEFAULT_EO_SEARCH_FIELDS,
	DEFAULT_EO_TABLE_FIELDS,
	getCoordinates
	// generateOptions
} = require("./api.js");


const buildFootprintsSearch = config => ({
	name: "EO",
	template: resolvePath("./templates/search.html"),
	services: (() => {
		const _config = {
			// Script for requesting sattelites and operations to CSN service
			// <div ng-init="init()"> on template
			// async init() {
			// 	const platforms = (await seg.csn.getCSN("platforms")) || [];
			// 	_config.search.satelliteSAROptions = generateOptions(platforms.sort(), true);

			// 	if (_config.search.operationOptions){
			// 		const operations = (await seg.csn.getCSN("operations")) || [];
			// 		_config.search.operationOptions = generateOptions(operations
			// 			.filter((obj, pos, arr) => arr.map(mapObj => mapObj).indexOf(obj) === pos)
			// 			.sort(), true
			// 		);
			// 	}
			// },
			search: config.footprints_search_fields || DEFAULT_EO_SEARCH_FIELDS,
			vds: config.vds,
			operation: seg.operation.project
			// Script for requesting sattelites and operations to CSN service
			// ng-change="updateSensorModeOptions()" on template
			// async updateSensorModeOptions() {
			// 	const sensorModes = (await seg.csn.getSensorMode(_config.search.satelliteSAR)) || [];
			// 	_config.search.sensorModeOptions = generateOptions(sensorModes
			// 		.reduce((acc, value) => acc.concat(value.sensorMode), [])
			// 		.filter((obj, pos, arr) => arr.map(mapObj => mapObj).indexOf(obj) === pos)
			// 		.sort(), true
			// 	);
			// }
		};

		_config.search.resetEOImagery = () => {
			_config.search.sar = false;
			_config.search.optical = false;
			_config.search.eoOperation = null;
			_config.search.status = null;
			_config.search.oil_spill = null;
			_config.search.ms_feedback = null;
			_config.search.vds = null;
			_config.search.satelliteSAR = null;
			_config.search.sensor_mode = null;
			_config.search.satelliteOptical = null;
		};

		_config.search.errors = false;

		return _config;
	})(),
	canSubmit(){
		const {selectedArea, showTime, acquisitionStart, acquisitionStop, serviceId} = this.services.search;
		const ADV_SEARCH_MONTHS = seg.CONFIG.getValue("eo.ADV_SEARCH_MONTHS");
		//check if > 6 months
		if(serviceId && serviceId !== ""){
			this.services.errors = false;
			return true;
		}

		if(showTime){
			if(acquisitionStop.diff(acquisitionStart, "M", true) > ADV_SEARCH_MONTHS){
				this.services.search.errors = {timerange: "Timerange is invalid, must be less than " + ADV_SEARCH_MONTHS + " months"};
				return false;
			}
		} else if (!selectedArea.length){
			this.services.search.errors = {area: "Please select an area."};
			return false;
		}
		this.services.search.errors = false;
		return true;
	},
	submit() {
		seg.ui.cleanables.call("Adv. Search Footprints");
		// create Request
		const requests = [],
			searchService = this.services.search,
			loaderModal = seg.modal.openModal("EO Advanced Search", "Retrieving EO Data...", {class: "info"});
		let params = {serviceID: searchService.serviceId};

		if (this.services.search.showTime){
			params = Object.assign({
				begin_position: searchService.acquisitionStart.toISOString(),
				end_position: searchService.acquisitionStop.toISOString()
			}, params);
		} else if(searchService.selectedArea[0]) {
			const areaExtent = ol.proj.transformExtent(searchService.selectedArea[0].getGeometry().getExtent(), seg.map.getProjection(), "EPSG:4326");
			params = Object.assign({
				lower_corner: ol.extent.getBottomLeft(areaExtent).join(","),
				upper_corner: ol.extent.getTopRight(areaExtent).join(",")
			}, params);
		}

		if ((searchService.EOImagery) && ((searchService.sar) || (searchService.optical))) {
			if (searchService.sar){
				if (this.services.operation === "OPR_CSN")
					params = Object.assign({
						operations: searchService.eoOperation ? searchService.eoOperation.toUpperCase() : null,
						serviceStatus: searchService.status
						//origin: searchService.origin
					}, params);

				requests.push(seg.request.get("v1/eo/catalogue/search", {
					params: Object.assign({
						platform: searchService.satelliteSAR, //satelllite
						sar: true,
						oil_spill: searchService.oil_spill,
						vds: searchService.vds,
						sensor_mode: searchService.sensor_mode, /////Sensor Mode
						ms_feedback: searchService.ms_feedback
					}, params)
				}));
			}

			if (searchService.optical)
				requests.push(seg.request.get("v1/eo/catalogue/search", {
					params: Object.assign({
						platform: searchService.satelliteOptical, //satelllite
						sar: false
					}, params)
				}));
		} else
			requests.push(seg.request.get("v1/eo/catalogue/search", {
				params
			}));

		loaderModal.setDismissCallback(() => {
			requests.forEach(req => req.cancel());
		});

		seg.promise.all(requests).then(res => {
			res.forEach(r => {
				r.result.crs = {
					"type": "name",
					"properties": {
						"name": "EPSG:4326"
					}
				};
				// r.result.crs.properties.name = r.result.crs.properties.name.toUpperCase();
			});

			footprintsSource = seg.layer.get("eo_sar_imagery").getSource();

			res = res.slice(1).reduce((acc, res) => res?Object.assign(acc, {
				features: [...acc.features, ...res.features]
			}):acc, res[0]).result;

			const footprints = (new ol.format.GeoJSON()).readFeatures(res, {
					featureProjection: seg.map.getProjection()
				}),
				features = [];

			if (!footprints.length) {
				loaderModal.setContent("Retrieved " + footprints.length + " EO Data reports.");
				loaderModal.setClass("no-result");
				seg.modal.closeTimeout(loaderModal.title);
				return;
			}

			for (let i = 0; i < footprints.length; i++) {
				const footprint = footprints[i];

				footprint.set("visible", true);
				const existFeature = footprintsSource.getFeatureById(footprint.getId());
				if (!existFeature) {
					const operations = footprint.get("operations");

					footprint.setProperties({
						old_eo: true,
						isSearchResult: true,
						totalOilSpills: footprint.get("oilSpills") || 0,
						operations: operations ? operations.split(",").filter((value, index, self) => self.indexOf(value) === index).join(", ") : ""
					}); // used to filter the features that will be removed when acq cleanable is activated
					footprint.unset("oilSpills");

					footprintsSource.addFeature(footprint);
					features.push(footprint);
				} else {
					existFeature.setProperties({
						isSearchResult: true,
						totalOilSpills: existFeature.get("oilSpills") || existFeature.get("totalOilSpills") || 0
					});
					existFeature.unset("oilSpills");

					features.push(existFeature);
				}
			}

			// const mergeDuplicateResults = mergeDuplicates(footprints);
			// footprintsSource.addFeatures(mergeDuplicateResults);

			loaderModal.setContent("Retrieved " + footprints.length + " EO Data reports.");
			loaderModal.setClass("success");
			seg.modal.closeTimeout(loaderModal.title);

			let footprintTable = seg.ui.ttt.tables.openTables.find(t => t.label === "Adv. EO Acquisitions");

			if (!footprintTable){
				seg.ui.ttt.tables.close(footprintTable);

				footprintTable = seg.ui.ttt.tables.open({
					label: "Adv. EO Acquisitions",
					data: features,
					itemAs: "EO",
					customToolbar: {
						template: "<span class=\"text-size-sm\">Number of EO Acquisitions: <b>{{grid.filteredResults.length}}</b></span>"
					},
					gridAPI:{
						getDistanceUnits:() => seg.preferences.workspace.get("distanceUnits"),
						getCoordinates
					},
					fields: config.footprints_table_fields || DEFAULT_EO_TABLE_FIELDS,
					onSelectionChange(changedRows, selectedRows) {
						seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
					}
				});
			} else seg.ui.ttt.tables.addDataToTable(footprintTable, features);

			seg.ui.cleanables.add("Adv. Search Footprints", () => {
				seg.ui.ttt.tables.close("Adv. EO Acquisitions");
				seg.ui.cleanables.remove("Adv. Search Footprints");
				footprintsSource.getFeatures()
					.filter(feature => feature.get("isSearchResult"))
					.filter(feature => feature.get("old_eo"))
					.forEach(feature => footprintsSource.removeFeature(feature));
			});
		}, e => {
			seg.log.error("ERROR_FOOTPRINTS_SUBMIT", e.error + ": " + e.result);
			loaderModal.setContent("Error retrieving EO data: " + e.result);
			seg.modal.closeTimeout(loaderModal.title);
		});
	},
	reset() {
		for (const prop in this.services.search) {
			if(!prop.includes("Options"))
				if (this.services.search[prop] && typeof this.services.search[prop] === "object") {
					if(Array.isArray(this.services.search[prop]))
						this.services.search[prop] = [];
					else
						this.services.search[prop].value = null;
				} else {
					this.services.search[prop] = null;
				}
		}
		seg.selection.select([]);

		this.services.search.acquisitionStart = moment().subtract(90,"d");
		this.services.search.acquisitionStop = moment();
	},
	save: function (searchName) {
		seg.search.getSavedSearches().then(res => {
			const savedSearchesSmart = res[1],
				savedSearches = res[0];
			savedSearches.push({
				name: searchName,
				config: this.services,
				src: "EO",
				advanced: true
			});

			seg.search.setSavedSearches(savedSearches.concat(savedSearchesSmart));
		}, e => seg.log.error("ERROR_FOOTPRINTS_SAVE", e.error + ": " + e.result));
	}
});

module.exports = {
	buildFootprintsSearch
};
