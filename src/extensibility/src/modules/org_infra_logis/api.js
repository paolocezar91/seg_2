const mapProperties = require("./stmid/properties");

const stmidTTTFields = mapProperties({
	override: {
		authorityId: { visible: true, order: 0},
		authorityName: { visible: true, order: 1},
		createdOn: { visible: false, order: 2},
		updatedOn: { visible: true, order: 3},
		locationCode: { visible: true, order: 4},
		// creatorUniqueId: { visible: true },
		// active: { visible: false },
		// deleted: { visible: false },
		// visibility: { visible: false },
		// entityType: { visible: false },
		// contactPersonFirstName: { visible: false },
		// contactPersonLastName: { visible: false },
		contactPersonPhone: { visible: false, order: 5},
		contactPersonEmail: { visible: false, order: 6},
		contactPersonFax: { visible: false, order: 7},
		depPhone: { visible: false, order: 8},
		depFax: { visible: false, order: 9},
		depEmail: { visible: false, order: 10},
		depPostalAddress: { visible: false, order: 11}
	}
});

const loadStmids = () => {
	const stmidLayer = seg.layer.get("stmid");

	if (!stmidLayer.get("loading")) {
		const modal = seg.modal.openModal("Loading Organisations", "Loading Organisations...", {class: "info"});

		return seg.request.get("v1/eo/stmid", {
			params: {
				service: "WFS",
				version: "2.0.0",
				request: "GetFeature",
				type_name: "ssn:COD_AUTHORITIES_WFS_VW",
				cql_filter: "ssn:ENTITY_TYPE='Public' AND ssn:ACTIVE=1 AND ssn:VISIBILITY='Published'"
			}
		}).then(res => {
			if (!res.result)
				return [];

			const stmidSource = stmidLayer.getSource();
			const features = (new ol.format.GeoJSON()).readFeatures(res.result, {
				featureProjection: seg.map.getProjection()
			});
			const collectionFeatures = [];

			for (let i = 0; i < features.length; i++) {
				const stmid = features[i];

				if (stmid.get("dutyList") && stmid.getGeometry() && stmid.getGeometry().getCoordinates() && stmid.getGeometry().getCoordinates().length) {
					stmidSource.addFeature(stmid);
					collectionFeatures.push(stmid);
				}
			}

			seg.ui.ttt.tables.register({
				customToolbar: {
					template: "<span class=\"text-size-sm\">Number of Maritime Authorities: <b>{{grid.filteredResults.length}}</b></span>"
				},
				label: "MARITIME AUTHORITIES",
				data: collectionFeatures,
				itemAs: "stmid",
				fields: stmidTTTFields,
				onSelectionChange(changedRows, selectedRows) {
					seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
				}
			});

			seg.shortcuts.add("stmid", [{
				label: "See in TTT",
				callback(retrievedSTMID) {
					let stmidTable = seg.ui.ttt.tables.get("Selected STMID");

					if(!stmidTable)
						stmidTable = {
							label: "Selected STMID",
							customToolbar: {
								template: "<span class=\"text-size-sm\">Number of STMID: <b>{{grid.filteredResults.length}}</b></span>"
							},
							data: [],
							itemAs: "stmid",
							fields: stmidTTTFields
						};

					seg.ui.ttt.tables.addDataToTable(stmidTable, retrievedSTMID);
					seg.ui.ttt.tables.open(stmidTable);
				}
			}]);

			modal.setContent("Organisations loaded.");
			modal.setClass("success");
			seg.modal.closeTimeout(modal.title);
		}, e => seg.log.error("ERROR_ORG_INFRA_LOGIS_lOAD_STMIDS", e.error + ": " + e.result));
	}
	return seg.promise.resolve();
};

module.exports = {
	loadStmids,
	stmidTTTFields
};