module.exports = {
	id: "stmid_duties",
	order: 7,
	name: "Duties",
	type: "Vector",
	renderMode: "image",
	style: require("./style.js"),
	source: {
		type: "Vector"
	},
	tooltip: {
		featureAs: "duty",
		bindings: {
			dutyToolTip: {}
		},
		templateUrl: resolvePath("./templates/tooltip.html")
	}
};