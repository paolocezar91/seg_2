const styleCache = {},
	selectedStyleCache = {},
	favouriteStyleCache = {};

module.exports = duty => {
	if(duty.get("dutyAOI")){
		const isEditing = duty.get("isEditing");
		let	strokeColorStyle = "#3399CC",
			strokeWidthStyle = 1.25,
			strokeOpacityStyle = 1;

		if(duty.get("selected") && !isEditing){
			strokeColorStyle = "rgb(255, 255, 0)";
			strokeWidthStyle = 2.5;
			strokeOpacityStyle = 1;
		}

		strokeColorStyle = ol.color.asArray(strokeColorStyle);
		strokeColorStyle = strokeColorStyle.slice();
		strokeColorStyle[3] = strokeOpacityStyle;

		const hexColor = typeof duty.get("fillColorStyle") !== "undefined" ? duty.get("fillColorStyle") : "rgba(255,255,255, 0.4)";
		let fillColorStyle = ol.color.asArray(hexColor);
		fillColorStyle = fillColorStyle.slice();
		fillColorStyle[3] = typeof duty.get("fillOpacityStyle") !== "undefined" ? duty.get("fillOpacityStyle") : 0.4;

		return new ol.style.Style({
			fill: fillColorStyle[3] === 0 ? null : new ol.style.Fill({
				color: fillColorStyle
			}),
			stroke: strokeColorStyle[3] === 0 ? null : new ol.style.Stroke({
				color: strokeColorStyle,
				width: strokeWidthStyle
			})
		});
	} else {
		const styles = [];
		let height = Math.round(4400000 / seg.map.getScale()) * 12;

		//size adjustements to scale
		if (height > 15)
			height = 20;

		if (height < 1)
			height = 15;

		if(!styleCache[height])
			styleCache[height] = new ol.style.Style({
				image: new seg.style.SVG({
					svg: require("./stmid-icon.svg"),
					fill: "#FF0000",
					stroke: {
						color: "rgba(255,255,255,1)",
						width: 1
					},
					height: height
				})
			});

		styles.push(styleCache[height]);

		if (duty.get("selected")) {
			const radius = 40 / 2;

			if (!selectedStyleCache[radius])
				selectedStyleCache[radius] = new ol.style.Style({
					image: new ol.style.Circle({
						radius,
						stroke: new ol.style.Stroke({
							color: "#006ebc",
							width: 2
						})
					})
				});

			styles.push(selectedStyleCache[radius]);
		}

		if (seg.favourites.isFavourite(duty)) {
			const radius = 36 / 2,
				color = seg.favourites.getGroupFromFeature(duty).color || "rgb(201, 151, 38)";

			if (!favouriteStyleCache[radius, color])
				favouriteStyleCache[radius, color] = new ol.style.Style({
					image: new ol.style.Circle({
						radius,
						stroke: new ol.style.Stroke({
							color,
							width: 2
						})
					})
				});

			styles.push(favouriteStyleCache[radius, color]);
		}

		return styles;
	}
};
