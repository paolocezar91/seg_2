const styleCache = {},
	selectedStyleCache = {},
	favouriteStyleCache = {};

module.exports = stmid => {
	const styles = [];
	let height = Math.round(4400000 / seg.map.getScale()) * 12;

	//size adjustements to scale
	if (height > 15)
		height = 20;

	if (height < 1)
		height = 15;

	if(!styleCache[height])
		styleCache[height] = new ol.style.Style({
			image: new seg.style.SVG({
				svg: require("./stmid-icon.svg"),
				fill: "rgba(0,0,255,1)",
				stroke: {
					color: "rgba(255,255,255,1)",
					width: 1
				},
				height: height
			})
		});

	styles.push(styleCache[height]);

	if (stmid.get("selected")) {
		const radius = 40 / 2;

		if (!selectedStyleCache[radius])
			selectedStyleCache[radius] = new ol.style.Style({
				image: new ol.style.Circle({
					radius,
					stroke: new ol.style.Stroke({
						color: "#006ebc",
						width: 2
					})
				})
			});

		styles.push(selectedStyleCache[radius]);
	}

	if (seg.favourites.isFavourite(stmid)) {
		const radius = 36 / 2,
			color = seg.favourites.getGroupFromFeature(stmid).color || "rgb(201, 151, 38)";

		if (!favouriteStyleCache[radius, color])
			favouriteStyleCache[radius, color] = new ol.style.Style({
				image: new ol.style.Circle({
					radius,
					stroke: new ol.style.Stroke({
						color,
						width: 2
					})
				})
			});

		styles.push(favouriteStyleCache[radius, color]);
	}

	return styles;
};
