const mapProperties = require("./properties");

module.exports = {
	init(stmid) {
		this.StmidPicture = this.StmidDefaultPicture = "assets/images/noSTMID.png";

		seg.request.get("v1/cod/detail", {
			params: {
				organisationId: stmid.get("authorityId")
			}
		}).then(({result}) => {
			let props = {};
			const org = result.organisationDetails[0];

			if(org && org.countryOrInstitution && org.databaseRecordInfo && org.organisationInformation){

				props = {
					organisationName: org.organisationName,
					countryCode: org.countryOrInstitution.code,
					locationCode: org.organisationLocationDefinedByLoCode,
					localLanguageName: org.localLanguageName,
					updatedOn: org.databaseRecordInfo.lastUpdatedOn.lastUpdatedOn,
					functionList: org.organisationFunction.join("; "),
					email: org.organisationInformation.contact.contactNumbers.email || "N/A",
					phone: org.organisationInformation.contact.contactNumbers.businessTelephone || "N/A",
					depPostalAddress: org.organisationInformation.contact.address.lineOne || "N/A"
				};

				stmid.setProperties(Object.assign(stmid.getProperties(), props));

				if(org.organisationDuties && org.organisationDuties.duties && org.organisationDuties.duties.length){
					const dutiesListDetails = org.organisationDuties.duties,
						dutiesList = stmid.get("dutiesList").map(duty => {
							const found = dutiesListDetails.find(dutyDetail => dutyDetail.code === duty.dutyCode);
							return Object.assign({}, duty, found);
						});

					stmid.set("dutiesList", dutiesList);
				}
			}

		});
	},
	centerMap(stmid) {
		seg.selection.selectFeatureAndCenter(stmid);
	},
	getLabelForStmid(stmid) {
		return stmid.get("authorityName");
	},
	listDuties(stmid) {
		const duties = stmid.get("dutiesList");

		if (duties)
			seg.ui.ttt.tables.open({
				label: stmid.get("authorityName"),
				data: duties,
				itemAs: "duty",
				fields: mapProperties({
					override: {
						code: false,
						name: false,
						description: false
					}
				}),
				onSelectionChange(changedRows, selectedRows) {
					const loaderModal = seg.modal.openModal("DUTY_AREA",  "Opening Duty Area", {class: "info"});
					const requestsCGD = [], requestsLocodes = [];
					if(selectedRows.length === 1){
						selectedRows.forEach(row => {
							const area = row.item.area;

							if(area){
								switch(area.type){
									case "CGD":
										requestsCGD.push(seg.request.get("v1/eo/wfs/cgdAreas", {
											params: {
												id: area.referenceKey
											}
										}));
										break;
									case "LOCODES":
										area.locodes.forEach(locode => {
											requestsLocodes.push(seg.request.get("v1/cld/locodes", {
												params: {
													locode
												}
											}));
										});
										break;
								}
							}
						});

						if(!requestsCGD.length && !requestsLocodes.length){
							loaderModal.setContent("<span class='text-color-warning'>No area associated with this duty found</span>");
							seg.modal.closeTimeout(loaderModal.title);
							return;
						}

						if(requestsCGD.length)
							seg.promise.all(requestsCGD).then(results => {
								results.forEach(({result}) => {
									const features = (new ol.format.GeoJSON()).readFeatures(result, {
											featureProjection: seg.map.getProjection()
										}),
										dutyLayer = seg.layer.get("stmid_duties");

									features.forEach(feature => {
										feature.setProperties(Object.assign(feature.getProperties(), {
											dutyAOI: true,
											dutyCode: selectedRows[0].item.dutyCode,
											dutyName : selectedRows[0].item.dutyName,
											authorityId: stmid.get("authorityId"),
											authorityName: stmid.get("authorityName")
										}));
										// seg.favourites.addFeaturesToGroup("Session", features, feature.get("NAME"));
									});

									dutyLayer.getSource().addFeatures(features);
									seg.selection.selectFeatureAndCenter(features);

									seg.ui.cleanables.add("Clear Duty Areas", () => {
										dutyLayer.getSource().getFeatures().filter(aoi => aoi.get("dutyAOI")).forEach(feature => dutyLayer.getSource().removeFeature(feature));
									});
								});

								loaderModal.setContent("Area associated with this duty found.");
								seg.modal.closeTimeout(loaderModal.title);
							});
						else if(requestsLocodes.length){
							seg.promise.all(requestsLocodes).then(results => {
								const dutyLayer = seg.layer.get("stmid_duties");
								const dutyFeatureData = {
										crs: {
											properties: {name: "EPSG:4326"},
											type: "name"
										},
										type: "FeatureCollection",
										features: results.map(({result}, index) => {
											return {
												id: stmid.get("authorityId") + "_" + index,
												type: "Feature",
												geometry: {
													type: "Point",
													coordinates: [result.locationPosition.longitude, result.locationPosition.latitude]
												},
												properties: {
													dutyCode: selectedRows[0].item.dutyCode,
													dutyName : selectedRows[0].item.dutyName,
													authorityId: stmid.get("authorityId"),
													authorityName: stmid.get("authorityName")
												}
											};
										})
									},
									dutyFeatures = new ol.format.GeoJSON()
										.readFeatures(dutyFeatureData, {
											dataProjection: "EPSG:4326",
											featureProjection: seg.map.getProjection()
										});

								dutyLayer.getSource().addFeatures(dutyFeatures);

								dutyLayer.setVisible(true);

								seg.ui.cleanables.add("Clear Duty Locodes", () => {
									seg.layer.get("stmid_duties").getSource().getFeatures().forEach(duty => seg.layer.get("stmid_duties").getSource().removeFeature(duty));
								});

								loaderModal.setContent("Locode associated with this duty found.");
								seg.modal.closeTimeout(loaderModal.title);
							});
						} else {
							loaderModal.setContent("<span class='text-color-warning'>Area associated with this duty not found.</span>");
							seg.modal.closeTimeout(loaderModal.title);
						}
					}
				}
			});
	},
	showAuthorityDutyModal(stmid) {
		/// XXX Should this be closed?
		const loaderModal = seg.modal.openModal(stmid.get("authorityName"),  "Authority Duty: " + stmid.get("authorityDuty"), {class: "info"});
		seg.modal.closeTimeout(loaderModal.title);
	},
	displayPreferences_: mapProperties({
		override: {
			organisationId: false,
			organisationName: false,
			localLanguageName: false,
			countryCode: false,
			latitude: false,
			longitude: false,
			locationCode: {label: "Location", order: 3},
			contactPersonPhone: false,
			depPhone: false,
			contactPersonEmail: false,
			depEmail: false,
			depPostalAddress: false,
			function: false,
			lastUpdatedOn: false
		}
	}),
	get displayPreferences() {
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("STMID.commandInfo.displayOptions") || {};

		Object.keys(savedPreferences).forEach(key => {
			if (this.displayPreferences_[key])
				this.displayPreferences_[key].visible = savedPreferences[key];
		});

		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayPreferences_;
	},
	get displayPreferencesForDisplay() {
		return Object.values(this.displayPreferences).filter(value => value.visible);
	},
	toggleDisplayPreference(key) {
		//we only want to save the "visible" property, the rest is always the same
		seg.preferences.workspace.set("STMID.commandInfo.displayOptions."+key, !this.displayPreferences[key].visible);
	},
	displayTooltipPreferences_: mapProperties({
		override: {
			organisationId: {order: 0},
			localLanguageName: {order: 1},
			function: {order: 2},
			locationCode: {label: "Location", order: 3},
			lastUpdatedOn: {order: 4}
		}
	}),
	get tooltipDisplayPreferences() {
		/*
		extend default preferences with saved ones
		so we always get latest value from saved preferences
		*/
		const savedPreferences = seg.preferences.workspace.get("STMID.tooltip.displayOptions") || {};

		Object.keys(savedPreferences).forEach(key => {
			if (this.displayTooltipPreferences_[key])
				this.displayTooltipPreferences_[key].visible = savedPreferences[key];
		});
		/*
		we have to always return the same object because of angular"s dirty checking
		(it needs to add a $$hashKey property to the watched object)
		*/
		return this.displayTooltipPreferences_;
	}
};