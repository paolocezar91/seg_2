const defaultProperties = {
	organisationId: {
		order: 0,
		label: "Organisation ID",
		template: "{{stmid.get('authorityId')}}",
		visible: true
	},
	organisationName: {
		order: 1,
		label: "Organisation Name",
		template: "{{stmid.get('authorityName')}}",
		visible: true
	},
	localLanguageName: {
		order: 2,
		label: "Local Language Name",
		template: "{{stmid.get('localLanguageName')}}",
		visible: true
	},
	countryCode: {
		order: 3,
		label: "Country",
		template: "{{stmid.get('countryCode') | country}}",
		visible: true
	},
	latitude: {
		order: 4,
		label: "Latitude",
		template: "<span coordinate=\"[(stmid.get('latitude')/600000), 'lat']\"></span>",
		visible: true
	},
	longitude: {
		order: 5,
		label: "Longitude",
		template: "<span coordinate=\"[(stmid.get('longitude')/600000), 'lon']\"></span>",
		visible: true
	},
	locationCode: {
		order: 6,
		label: "Location Code",
		template: "<span ng-if=\"stmid.get('locationCode')\" locode=\"stmid.get('locationCode')\"></span>",
		visible: true
	},
	phone: {
		order: 7,
		label: "Department Address",
		template: "{{stmid.get('depPhone')}}",
		visible: true
	},
	email: {
		order: 8,
		label: "Department Address",
		template: "<span class=\"email-label\">{{stmid.get('depEmail')}}</span>",
		visible: true
	},
	depPostalAddress: {
		order: 9,
		label: "Department Address",
		template: "{{stmid.get('depPostalAddress')}}",
		visible: true
	},
	function: {
		order: 10,
		label: "Function",
		template: "{{stmid.get('functionList')}}",
		visible: true
	},
	lastUpdatedOn: {
		order: 11,
		label: "Last Updated On",
		template: "<span position-timestamp=\"stmid.get('updatedOn')\"></span>",
		visible: true
	},
	depPhone: {
		order: 12,
		label: "Department Phone",
		template: "{{stmid.get('depPhone')}}",
		visible: false
	},
	depEmail: {
		order: 13,
		label: "Department Email",
		template: "<span class=\"email-label\">{{stmid.get('depEmail')}}</span>",
		visible: false
	},
	contactPersonPhone: {
		order: 14,
		label: "Person Phone",
		template: "{{stmid.get('contactPersonPhone')}}",
		visible: false
	},
	contactPersonEmail: {
		order: 15,
		label: "Person Email",
		template: "<span class=\"email-label\">{{stmid.get('contactPersonEmail')}}</span>",
		visible: false
	},
	// DUTY PROPERTIES
	code: {
		order: 14,
		label: "Code",
		template: "{{duty.dutyCode}}",
		visible: true
	},
	name: {
		order: 15,
		label: "Name",
		template: "{{duty.dutyName}}",
		visible: true
	},
	description: {
		order: 16,
		label: "Description",
		template: "{{duty.dutyDescription}}",
		visible: true
	}
	// organisationDuty: {
	// 	order: 17,
	// 	label: "Authority Duty",
	// 	template: "{{stmid.get('authorityDuty')}}",
	// 	visible: true
	// },
	// creatorUniqueId: {
	// 	order: 18,
	// 	label: "Creator Unique Id",
	// 	template: "{{stmid.get('creatorUniqueId')}}",
	// 	visible: true
	// },
	// active: {
	// 	order: 19,
	// 	label: "Active",
	// 	template: "{{stmid.get('active') == 1 ? \"Yes\" : \"No\"}}",
	// 	visible: true
	// },
	// deleted: {
	// 	order: 20,
	// 	label: "Deleted",
	// 	template: "{{stmid.get('deleted') == 1 ? \"Yes\" : \"No\"}}",
	// 	visible: true
	// },
	// visibility: {
	// 	order: 21,
	// 	label: "Visibility",
	// 	template: "{{stmid.get('visibility')}}",
	// 	visible: true
	// },
	// entityType: {
	// 	order: 22,
	// 	label: "Entity Type",
	// 	template: "{{stmid.get('entityType')}}",
	// 	visible: true
	// },
	// contactPersonFirstName: {
	// 	order: 23,
	// 	label: "Person First Name",
	// 	template: "{{stmid.get('contactPersonFirstName')}}",
	// 	visible: true
	// },
	// contactPersonLastName: {
	// 	order: 24,
	// 	label: "Person Last Name",
	// 	template: "{{stmid.get('contactPersonLastName')}}",
	// 	visible: true
	// },
	// contactPersonFax: {
	// 	order: 25,
	// 	label: "Person Fax",
	// 	template: "{{stmid.get('contactPersonFax')}}",
	// 	visible: true
	// },
	// depFax: {
	// 	order: 26,
	// 	label: "Department Fax",
	// 	template: "{{stmid.get('depFax')}}",
	// 	visible: true
	// },
};

const mapProperties = (options) => seg.utils.mapItemProperties(defaultProperties, options);

module.exports = mapProperties;
