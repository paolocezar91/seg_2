const {STMID_DUTYCODE_TYPES} = require("./api.js");

module.exports = {
	id: "stmid",
	order: 7,
	name: "Organisations",
	type: "Vector",
	renderMode: "image",
	style: require("./style.js"),
	source: {
		type: "Vector"
	},
	commandInfo: {
		//alias to access the selected Oil Spills
		featureAs: "stmid",
		bindings: {
			StmidCommandInfo: require("./command_info.scope.js")
		},
		//template url for this C&I
		templateUrl: resolvePath("./templates/command_info.html")
	},
	tooltip: {
		featureAs: "stmid",
		bindings: {
			StmidToolTip: require("./command_info.scope.js")
		},
		templateUrl: resolvePath("./templates/tooltip.html")
	},
	search: {
		keys: [
			"authorityName", "locationCode", "localLanguageName"
		],
		label: "authorityName"
	},
	filters: [
		{
			name: "Duty",
			inclusive: true,
			options: (() => {
				const filters = {};
				for (const key in STMID_DUTYCODE_TYPES)
					filters[key] = (stmid) => stmid.get("dutiesList").some((duty) => duty.dutyCode === STMID_DUTYCODE_TYPES[key]);
				return filters;
			})()
		}
	]
};