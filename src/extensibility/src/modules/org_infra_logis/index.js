/**
@namespace extensibility.modules.org_infra_logis
@description
# Organisations Infrastructures & Logistics

## Available options
- **stmid** - true/false - Maritime Authorities layer

## Global configurations and constants
None
*/
let config, orgInfraLogisLayer, stmidLayer, OILMenuItem, STMIDMenuItem;
const {loadStmids} = require("./api"),
	requests = [];

const lazyLoader = () => {
	if(!orgInfraLogisLayer.getVisible())
		return;

	if (config.stmid) {
		STMIDMenuItem.isLoading = true;

		const stmidRequest = loadStmids();

		stmidRequest.then(() => {
			if (!stmidLayer.get("loading")) {
				stmidLayer.set("loading", true);

				const stmidFilters = seg.ui.layerMenu.fromFilters(stmidLayer)[0].options;
				STMIDMenuItem.options = STMIDMenuItem.options.concat(stmidFilters);
			}
		}, e => seg.log.error("ERROR_ORG_INFRA_LOGIS_LAZY_LOADER_STMID_REQUEST", e.error + ": " + e.result));

		requests.push(stmidRequest);
	}

	return seg.promise.all(requests)
		.then(() => {
			delete STMIDMenuItem.isLoading;
		}, e => seg.log.error("ERROR_ORG_INFRA_LOGIS_LAZY_LOADER", e.error + ": " + e.result));
	// }
};

module.exports = _config => {
	config = _config;

	const layers = [];

	if (config.stmid){
		layers.push(require("./stmid/layer.js"));
		layers.push(require("./stmid/duties/layer.js"));
	}

	return {
		name: "Organisations, Infrastructure & Logistics",
		layers: [{
			id: "org_infra_logis",
			name: "Organisations, Infrastructure & Logistics",
			layers,
			visible: false
		}],
		init() {
			orgInfraLogisLayer = seg.layer.get("org_infra_logis");
			OILMenuItem = seg.ui.layerMenu.fromLayer("ORGANISATIONS", orgInfraLogisLayer);

			if (config.stmid) {
				stmidLayer = seg.layer.get("stmid");
				STMIDMenuItem = seg.ui.layerMenu.fromLayer("Maritime Authorities", stmidLayer);
				OILMenuItem.options.push(STMIDMenuItem);
			}

			seg.ui.layerMenu.register(OILMenuItem);
			orgInfraLogisLayer.ol_.on("change:visible", lazyLoader);
			lazyLoader();
		}
	};
};


