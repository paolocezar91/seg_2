module.exports = {
	name: "Organisations",
	template: resolvePath("./templates/search.html"),
	services: (() => {
		return {
			search: {}
		};
	})(),
	submit: () => {
		// create Request
		const params = {
			service: "WFS",
			type_name: "ssn:COD_AUTHORITIES_WFS_VW",
			request: "getFeatures"
			//	begin_position: this.services.search.aquisitionStart?this.services.search.aquisitionStart.toISOString(): moment().toISOString(),
			//	end_position: this.services.search.aquisitionStop?this.services.search.aquisitionStop.toISOString() :moment().toISOString(),
			//	status: this.services.search.status.value?this.services.search.status.value:"",
			//	platform: this.services.search.platform
		};

		const loaderModal = seg.modal.openLoader("Organisations Adv Search", "Retrieving STMID's...", {class: "info"});
		loaderModal.setPartialProgress("Organisations Adv. Search", 0);

		const STMIDRequest = seg.request.get("v1/eo/stmid?", {
			params: params
		});

		loaderModal.setDismissCallback(STMIDRequest.cancel);

		return STMIDRequest.then(({result}) => {
			// set modal progress
			loaderModal.setPartialProgress("Organisations Adv Search", 100);

			const stmids = (new ol.format.GeoJSON()).readFeatures(result, {
				featureProjection: seg.map.getProjection()
			});

			//if no reports were retrieved, return
			if (!stmids.length) {
				loaderModal.setContent("Retrieved " + stmids.length + " STMID's reports,");
				loaderModal.setClass("no-result");
				seg.modal.closeTimeout(loaderModal.title);
				return;
			}

			const stmidsSource = seg.layer.get("stmid").getSource();
			const features = stmids.map(stmid => {
				const existFeature = stmidsSource.getFeatureById(stmid.getId());

				if (!existFeature) {
					stmidsSource.addFeature(stmid);
					//seg.ui.queryPanel.ACQ.addFeature(stmid);
					features.push(stmid);
				} else {
					//seg.ui.queryPanel.ACQ.addFeature(existFeature);
					features.push(existFeature);
				}
			});

			loaderModal.setContent("Retrieved " + stmids.length + " STMID's reports reports.");
			loaderModal.setClass("success");

			let stmidsTable = seg.ui.ttt.tables.openTables.find(t => t.label === "Adv. STMID Acquisitions");
			if (!stmidsTable)
				stmidsTable = seg.ui.ttt.tables.open({
					label: "Adv. STMID Acquisitions",
					data: features,
					itemAs: "stmid",
					//acq: true, ??
					fields: {
						authorityId: {
							label: "ID",
							template: "{{stmid.get('authorityId')}}",
							visible: true
						},
						authorityName: {
							label: "Authority Name",
							template: "{{stmid.get('authorityName')}}",
							visible: true
						},
						authorityDuty: {
							label: "Authority Duty",
							template: "{{stmid.get('authorityDuty')}}",
							visible: true
						},
						createdOn: {
							label: "Created On",
							template: "<span position-timestamp=\"stmid.get('createdOn')\"></span>",
							visible: false
						},
						updatedOn: {
							label: "Updated On",
							template: "<span position-timestamp=\"stmid.get('updatedOn')\"></span>",
							visible: true
						},
						creatorUniqueId: {
							label: "Creator Unique template",
							template: "{{stmid.get('creatorUniqueId')}}",
							visible: true
						},
						locationCode: {
							label: "Location Code",
							value: "{{stmid.get('locationCode')}}",
							visible: true
						},
						active: {
							label: "Active",
							template: "{{stmid.get('active') == 1 ? \"Yes\" : \"No\"}}",
							visible: true
						},
						deleted: {
							label: "Deleted",
							template: "{{stmid.get('deleted') == 1 ? \"Yes\" : \"No\"}}",
							visible: true
						},
						visibility: {
							label: "Visibility",
							template: "{{stmid.get('visibility')}}",
							visible: true
						},
						entityType: {
							label: "Entity Type",
							template: "{{stmid.get('entityType')}}",
							visible: true
						},
						contactPersonFirstName: {
							label: "Person First Name",
							template: "{{stmid.get('contactPersonFirstName')}}",
							visible: true
						},
						contactPersonLastName: {
							label: "Person Last Name",
							template: "{{stmid.get('contactPersonLastName')}}",
							visible: true
						},
						contactPersonPhone: {
							label: "Person Phone",
							template: "{{stmid.get('contactPersonPhone')}}",
							visible: true
						},
						depPhone: {
							label: "Department Phone",
							template: "{{stmid.get('depPhone')}}",
							visible: true
						},
						contactPersonFax: {
							label: "Person Fax",
							template: "{{stmid.get('contactPersonFax')}}",
							visible: true
						},
						depFax: {
							label: "Department Fax",
							template: "{{stmid.get('depFax')}}",
							visible: true
						},
						contactPersonEmail: {
							label: "Person Email",
							template: "<span class=\"email-label\">{{stmid.get('contactPersonEmail')}}</span>",
							visible: true
						},
						depEmail: {
							label: "Department Email",
							template: "<span class=\"email-label\">{{stmid.get('depEmail')}}</span>",
							visible: true
						},
						depPostalAddress: {
							label: "Department Address",
							template: "{{stmid.get('depPostalAddress')}}",
							visible: true
						}
					},
					onSelectionChange(changedRows, selectedRows) {
						seg.selection.selectFeatureAndCenter(selectedRows.map(row => row.item));
					}
				});
			else
				stmidsTable.data = features;

			seg.modal.closeTimeout(loaderModal.title);

			//may need to be specific on its name.. to see
			//seg.ui.ttt.timeline.addFeatures("EO Footprints", features);
			// seg.ui.queryPanel.ACQ.onClear(() => {
			// 	seg.ui.ttt.tables.close(footprintACQTable);
			// 	seg.ui.ttt.timeline.remCategory("EO Footprints");
			// 	seg.layer.get("eo_footprints").getSource().clear();
			// });

		}, e => seg.log.error("ERROR_ORG_INFRA_LOGIS_STMID_REQUEST_SUBMIT", e.error + ": " + e.result), (progress) => {
			loaderModal.setPartialProgress("Organisations Adv Search", progress.loaded / progress.total * 100);
		});
	},
	reset: () => {
		seg.log("Organisations.search.reset", this.services.search);
	}
};
