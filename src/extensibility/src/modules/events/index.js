/**
@namespace extensibility.modules.events
@description
# Events

## Available options
None

## Global configurations and constants
None
*/
let onSelectionEvent;

module.exports = (/*config*/) => {
	return {
		name: "Events",
		layers: [
			require("./layer.js")
		],
		init() {
			onSelectionEvent = seg.selection.onSelectionChange(() => {
				if (seg.selection.getSelected("events").length === 1)
					seg.timeout(() => seg.ui.apps.open("Report"));
				onSelectionEvent();
			});

			const eventsLayer = seg.layer.get("events"),
				availableEvents = ["fisheries"], //XXX should come from config file
				requests = availableEvents.map(type => seg.request.get("v1/events?", {
					params: {
						type
					}
				}).then(res => {
					if (!res.result)
						return;

					const featuresArray = res.result.map(event => {
						const feature = new ol.Feature({
							id: event.id,
							type,
							name: event.name || "",
							description: event.description || "",
							visible: true,
							latitude: event.lat,
							longitude: event.lon,
							geometry: new ol.geom.Point([event.lon, event.lat]).transform("EPSG:4326", seg.map.getProjection())
						});

						feature.setId(event.id);
						return feature;
					});

					eventsLayer.getSource().addFeatures(featuresArray);
				}, e => seg.log.error("ERROR_EVENTS_AVAILABLE_EVENTS", e.error + ": " + e.result), () => {}));

			return seg.promise.all(requests).then(() => {
				const eventsLayer = seg.layer.get("events"),
					eventsMenuItem = seg.ui.layerMenu.fromLayer("EVENTS", eventsLayer),
					eventsFiltersMenuItems = seg.ui.layerMenu.fromFilters(eventsLayer);

				eventsMenuItem.options = eventsFiltersMenuItems[0].options;
				seg.ui.layerMenu.register(eventsMenuItem);
			}, e => seg.log.error("ERROR_EVENTS_INIT", e.error + ": " + e.result));
		}
	};
};
