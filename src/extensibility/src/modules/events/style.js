const styleCache = {},
	selectedStyleCache = {};

module.exports = event => {
	let height = Math.round(4400000 / seg.map.getScale()) * 12;
	const styles = [],
		type = event.get("type"),
		selected = event.get("selected"),
		cacheKey = JSON.stringify({type, height});

	//size adjustements to scale
	if (height > 15)
		height = 20;

	if (height < 1)
		height = 10;


	if(!styleCache[cacheKey])
		styleCache[cacheKey] = new ol.style.Style({
			image: new seg.style.SVG({
				svg: require("./fisheries.svg"),
				fill: "rgba(0,0,255,1)",
				stroke: {
					color: "rgba(255,255,255,1)",
					width: 1
				},
				height
			})
		});

	styles.push(styleCache[cacheKey]);

	if (selected) {
		const radius = Math.min(40, Math.max(1, Math.round(8800000 / seg.map.getScale()) * 12)) / 2;

		if (!selectedStyleCache[radius])
			selectedStyleCache[radius] = new ol.style.Style({
				image: new ol.style.Circle({
					radius,
					stroke: new ol.style.Stroke({
						color: "#006ebc",
						width: 2
					})
				})
			});

		styles.push(selectedStyleCache[radius]);
	}

	return styles;
};
