module.exports = {
	id: "events",
	name: "Events",
	type: "Vector",
	order: 3,
	source: {
		name: "Events",
		type: "Vector",
		projection: "EPSG:4326"
	},
	style: require("./style.js"),
	filters: [
		{
			name: "Types",
			options: {
				Fishery: function (event) {
					return event.get("type") === "fisheries";
				}
			}
		}
	]
};
