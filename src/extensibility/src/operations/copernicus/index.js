module.exports = ({aoi, vessels, orgInfraLogis, eo, metOcean, areas, location, alerts, apps}) => ({
	"modules": [
		aoi(),
		vessels({
			"symbolizers": [
				"IALA",
				"Data Source",
				"Vessel Type",
				"Voyage Information"
			],
			"filters": [
				"Voyage information",
				"Position Source",
				"Vessel Flag",
				"Vessel Type (PSC Types)",
				"Navigation Status (AIS)",
				"User Risk Status"
			],
			"advanced_search":{
				"show_ssn_information": true
			},
			"vessel_models": true,
			"ssn_enrichment": true,
			"lrit": true,
			"fisheries_link": true,
			"ga_plan": true,
			"thetis": true,
			"track_import": true
		}),
		orgInfraLogis({
			"stmid": true
		}),
		eo({
			"eo_acq": true,
			"oil_spills": true,
			"eo_show_footprints": true,
			"eo_show_oilspills": true,
			"oil_spills_report_in_ssn": true,
			"footprints_alert_report": true,
			"footprints_activity_detection": true,
			"footprints_download_eo": true,
			"vds": true
		}),
		metOcean({
			"remote_sensing": true,
			"forecast": true,
			"in_situ": true
		}),
		areas({
			layers: [
				...require("../../layers/ms_specific/config.js")({
					"malta_layers": {},
					"italy_layers": {},
					"netherlands_layers": {},
					"portugal_layers": {},
					"ireland_layers": {},
					"sweden_layers": {},
					"uk_layers": {},
					"eu_layers": {}
				})
			],
			"area_import": true
		}),
		location(),
		alerts(),
		apps({
			"sentinel": true,
			"abm_alert_insert": true
		})
	]
});