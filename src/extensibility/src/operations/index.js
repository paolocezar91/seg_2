require("babel-polyfill");
/**@namespace extensibility.operations
@description
Here are defined the avalaible operations used to configure the extensibility modules.

They are filtered comparing the user roles and their project value, and after that, the {@link extensibility.operations.Config config} is applied on the modules on {@link seg.operation}.
*/

/**
@typedef extensibility.operations.OperationsOptions
@prop {object} operation - [key] that represents operation object
@prop {string} operation.id - Operation id
@prop {extensibility.operations.Config} operation.config - Configuration instance
@prop {string} operation.alias - Alias that is shown on operations selection in SEG interface
@prop {string} operation.project - Definition used to filter user available functionalities
@prop {string} operation.v1 - Fallback used for v1 current_user_info, different name for the same operation
*/

/**
@typedef extensibility.operations.Config
@prop {extensibility.modules[]} modules - Instances of the extensibility modules with the according configuration set to the operation.
*/

const OPERATIONS = {
	"OPR_MSPILOT": {
		"id": "ivtmis",
		"config": require("./ivtmis"),
		"alias": "IVTMIS",
		"project": "OPR_MSPILOT",
		"v1": "IMDatE Service MSPILOT"
	},
	"OPR_COPERNICUS": {
		"id": "copernicus",
		"config": require("./copernicus"),
		"alias": "COPERNICUS",
		"project": "OPR_COPERNICUS",
		"v1": "IMDatE Service COPERNICUS"
	},
	"OPR_DEFAULT": {
		"id": "default",
		"config": require("./default"),
		"alias": "Default",
		"project": "OPR_DEFAULT",
		"v1": "IMDatE Service Default"
	},
	"OPR_EFCA_ATLANTIC": {
		"id": "efca_atlantic",
		"config": require("./efca_atlantic"),
		"alias": "EFCA Atlantic",
		"project": "OPR_EFCA_ATLANTIC",
		"v1": "IMDatE Service Atlantic"
	},
	"OPR_EFCA_MEDITERRANEAN": {
		"id": "efca_med",
		"config": require("./efca_med"),
		"alias": "EFCA Mediterranean",
		"project": "OPR_EFCA_MEDITERRANEAN",
		"v1": "IMDatE Service Mediterranean"
	},
	"OPR_FRONTEX": {
		"id": "frontex",
		"config": require("./frontex"),
		"alias": "FRONTEX",
		"project": "OPR_FRONTEX",
		"v1": "IMDatE Service FRONTEX"
	},
	"OPR_MAOC_N": {
		"id": "maoc-n",
		"config": require("./maoc-n"),
		"alias": "MAOC-N",
		"project": "OPR_MAOC_N",
		"v1": "IMDatE Service MAOC-N"
	},
	"OPR_EUNAVFOR": {
		"id": "EUNAVFOR",
		"config": require("./eunavfor"),
		"alias": "EUNAVFOR",
		"project": "OPR_EUNAVFOR",
		"v1": "IMDatE Service OPR_EUNAVFOR"
	},
	"OPR_CSN": {
		"id": "cleanseanet",
		"config": require("./cleanseanet"),
		"alias": "CLEANSEANET",
		"project": "OPR_CSN",
		"v1":"IMDatE Service CleanSeaNet"
	},
	"OPR_SAFEMED": {
		"id": "safemed",
		"config": require("./cleanseanet"),
		"alias": "ENI IMS",
		"project": "OPR_SAFEMED",
		"v1": "IMDatE Service SAFEMED"
	},
	"OPR_BCSEA": {
		"id": "bcsea",
		"config": require("./cleanseanet"),
		"alias": "ENI IMS",
		"project": "OPR_BCSEA",
		"v1": "IMDatE Service BCSEA"
	},
	"OPR_BCSEA_AIS": {
		"id": "bcsea_ais",
		"config": require("./bcsea_ais"),
		"alias": "ENI IMS",
		"project": "OPR_BCSEA_AIS",
		"v1": "IMDatE Service BCSEA AIS"
	},
	"OPR_EFCA_IMS": {
		"id": "efca_ims",
		"config": require("./efca_ims"),
		"alias": "EFCA IMS",
		"project": "OPR_EFCA_IMS",
		"v1": "IMDatE Service EFCA_IMS"
	},
	"OPR_EUROPOL": {
		"id": "europol",
		"config": require("./europol"),
		"alias": "EUROPOL",
		"project": "OPR_EUROPOL",
		"v1": "IMDatE Service EUROPOL"
	}
};

/** @constructs extensibility.operations.Operations
@param {roles} roles - roles defined on SEG database
@returns {object} Extensibility {@link extensibility.module modules} and {@link extensibility.operations.OperationsOptions operations} that the user has access, filtered by project
*/

module.exports = (roles, operations) => {
	const permissions = require("../permissions")(roles),
		modules = require("../modules")(permissions);

	return {
		modules,
		operations: Object.entries(OPERATIONS)
			.filter(([project, data]) => !!~roles.indexOf(data.v1) || (operations && !!~operations.indexOf(project)))
			.map((operation) => operation[1])
	};
};