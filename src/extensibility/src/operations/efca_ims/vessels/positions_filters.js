const filters = [
	{
		name: "Position Source",
		options: {
			"T-AIS": (vessel) => vessel.get("src") === "T-AIS",
			"SAT-AIS": (vessel) => vessel.get("src") === "Sat-AIS",
			"VMS": (vessel) => vessel.get("src") === "VMS",
			"LRIT": (vessel) => vessel.get("src") === "LRIT",
			"MRS": (vessel) => vessel.get("src") === "MRS",
			"VDS": (vessel) => vessel.get("src") === "VDS",
			"Sighting/inspection": (vessel) => {
				const src = vessel.get("src");
				return src === "Sighting" || src === "Inspections infrigement" || src === "Inspections no infrigement";
			},
			"Other/Uploaded": (vessel) => vessel.get("src") === "Other",
			"N/A": (vessel) => !vessel.get("src") || vessel.get("src") === "" || typeof vessel.get("src") === "undefined" //should be when vessel has no src property
		}
	}
];

module.exports = filters;