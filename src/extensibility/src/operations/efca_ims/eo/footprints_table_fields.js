const mapProperties = require("../../../modules/eo/SAR/footprints/properties");

module.exports = mapProperties({
	override: {
		id: {order: 0},
		acquisitionStop: {label: "Acquisition date", order: 1},
		status: {order: 2},
		sensorType: {order: 3},
		sensorResolution: {order: 4},
		satelliteSensor: {order: 5},
		operation: {order: 6},
		affectedAlertAreas: {order: 7},
		polarization: {order: 8},
		totalActivities: {order: 8},
		coverageCompliance: {order: 9, visible: false},
		usableArea: {order: 10 , visible: false},
		pinpointing: {order: 11, visible: false},
		feedback: {order: 12, visible: false},
		polygon: {order: 13, visible: false}
	}
});