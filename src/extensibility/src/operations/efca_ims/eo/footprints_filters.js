const {STATUS_FILTER, createEOFilter, operationMatchFn} = require("../../../modules/eo/SAR/footprints/filters");

const OPERATION_FILTER = createEOFilter("EO Operation", [
	"EFCA",
	"FRONTEX",
	"CleanSeaNet"
], operationMatchFn);

const TYPE_FILTER = {
	name: "Sensor Type",
	options: {
		RADAR: EO => {
			return EO.get("sensorType") === "RADAR";
		},
		OPTICAL: EO => {
			return EO.get("sensorType") === "OPTICAL";
		}
	}
};

module.exports = [
	Object.assign({}, STATUS_FILTER, {
		name: "Images Status"
	}),
	TYPE_FILTER,
	OPERATION_FILTER
];