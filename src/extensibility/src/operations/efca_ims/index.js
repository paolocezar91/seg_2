const areaLayers = [
	require("../../layers/maritime_boundaries/config.js"),
	require("../../layers/iccat_wms/config.js"),
	require("../../layers/nafo_wms/config.js"),
	require("../../layers/gfcm_wms/config.js"),
	require("../../layers/neafc_wms/config.js"),
	require("../../layers/fao_ices_wms/config.js")
];

module.exports = ({aoi, vessels, ports, eo, sarSurpic, metOcean, areas, location, alerts, apps}) => ({
	"base_layers": {
		include: [
			"nautical_chart_base",
			"nautical_chart_detailed"
		],
		exclude: [
			"cmap_base",
			"cmap_standard"
		]
	},
	default_base_layer: "nautical_chart_base",
	"modules": [
		aoi(),
		vessels({
			"symbolizers": [
				"Data Source",
				"Vessel Type",
				"Target Age",
				"Gear Type",
				"RFMO Status"
			],
			"filters": [
				"Flag",
				//"Vessel Type",
				"Vessel Type (PSC Types)",
				"NEAFC",
				"ICCAT",
				"NAFO",
				//"EU Fisheries",
				"User Risk Status",
				"Navigation Status (AIS)",
				"Position Source",
				"Gear Type",
				"FAO Types",
				"PWR Class",
				"GT Class",
				"LOA Class"
			],
			"filters_override": require("./vessels/positions_filters"),
			"advanced_search":{
				"show_ssn_information": false
			},
			"vessel_models": true,
			"ssn_enrichment": true,
			"lrit": true,
			"fisheries_link": true,
			"ga_plan": true,
			"thetis": true,
			"track_import": true,
			"acq": {
				limitArea: 16000, // nautical miles
				limitTime: 2 // days
			}
		}),
		ports(),
		eo({
			"eo_acq": true,
			"oil_spills": false,
			"join_optical_and_sar": true,
			"eo_show_footprints": true,
			"eo_show_oilspills": false,
			"oil_spills_report_in_ssn": false,
			"oil_spills_slick_info": false,
			"footprints_alert_report": true,
			"footprints_activity_detection": true,
			"footprints_filters": require("./eo/footprints_filters"),
			"footprints_table_fields": require("./eo/footprints_table_fields"),
			"footprints_menu_tooltip": require("./eo/footprints_menu_tooltip"),
			"footprints_download_eo": true,
			"sentinel": true,
			"vds": true
		}),
		sarSurpic(),
		metOcean({
			"remote_sensing": true,
			"forecast": true,
			"in_situ": true
		}),
		areas({
			"layers": areaLayers,
			"cgd": {
				filters: {
					csv_files: [
						"EFCA-IUU-CGD-GFCM.1.0.2.csv",
						"EFCA-IUU-CGD-FAO.1.0.3.csv",
						"EFCA-IUU-CGD-NAFO.1.0.2.csv",
						"EFCA-IUU-CGD-NEAFC.1.0.4.csv"
					]
				},
				features: {
					params: {
						type: ["FISHERIES"]
					}
				}
			},
			"area_import": true,
			"mergeFilterAndWMSOptions" : true
		}),
		location(),
		alerts(),
		apps({
			"sentinel": true,
			"abm_alert_insert": true
		})
	]
});