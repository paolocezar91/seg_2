module.exports = ({aoi, vessels, metOcean, areas, location, alerts, apps}) => ({
	"modules": [
		aoi(),
		vessels({
			"symbolizers": [
				"Data Source",
				"Target Age",
				"Vessel Type",
				"EUNAVFOR"
			],
			"filters": [
				"Position Source",
				"Vessel Flag",
				"Vessel Type (PSC Types)",
				"Navigation Status (AIS)",
				"User Risk Status",
				"Risk Levels",
				"Special Categories"
			],
			"advanced_search":{
				"show_ssn_information": true
			},
			"vessel_models": true,
			"ga_plan": true,
			"track_import": true
		}),
		metOcean({
			"remote_sensing": true,
			"forecast": true,
			"in_situ": true
		}),
		areas({
			"layers": require("../../layers/eunavfor_areas/config.js"),
			"area_import": true
		}),
		location(),
		alerts(),
		apps({
			"abm_alert_insert": true,
			"sentinel": true
		})
	]
});