module.exports = ({aoi, vessels, ports, orgInfraLogis, eo, sarSurpic, metOcean, areas, location, alerts, apps}) => ({
	"base_layers": {
		include: [
			"bathymetry_eu",
			"bathymetry_global"
		]
	},
	"modules": [
		aoi({"show_menu": true}),
		vessels({
			"symbolizers": [
				"IALA",
				"Data Source",
				"Vessel Type",
				"Target Age",
				"Voyage Information"
			],
			"filters": [
				"Voyage information",
				"Position Source",
				"Vessel Flag",
				"Vessel Type (PSC Types)",
				"Navigation Status (AIS)",
				"User Risk Status"
			],
			"advanced_search":{
				"show_ssn_information": true
			},
			"vessel_models": true,
			"ssn_enrichment": true,
			"lrit": true,
			"fisheries_link": true,
			"ga_plan": true,
			"thetis": true,
			"track_import": true
		}),
		ports(),
		orgInfraLogis({
			"stmid": true
		}),
		eo({
			"eo_acq": true,
			"oil_spills": true,
			"eo_show_footprints": true,
			"eo_show_oilspills": true,
			"oil_spills_report_in_ssn": true,
			"footprints_alert_report": true,
			"footprints_activity_detection": true,
			"footprints_download_eo": true,
			"sentinel": true,
			"vds": true
		}),
		sarSurpic(),
		metOcean({
			"remote_sensing": true,
			"forecast": true,
			"in_situ": true
		}),
		areas({
			layers: [
				...require("../../layers/ms_specific/config.js")({
					"malta_layers": {},
					"italy_layers": {},
					"netherlands_layers": {},
					"portugal_layers": {},
					"ireland_layers": {},
					"sweden_layers": {},
					"uk_layers": {},
					"eu_layers": {}
				})
			],
			"cgd": {
				filters: {
					preset_filters: {
						name: "AREAS OSPAR Exchange Area",
						options: ["OSPAR"]
					},
					csv_files: [
						"IVTMIS-CGD-EXCLUSION-1.0.3.csv"
					]
				},
				features: {
					params: {
						layer_name: [
							"Exclusion Area 2",
							"Exclusion Area 3",
							"Exclusion Area 4",
							"Exclusion Area 5",
							"Exclusion Area 6",
							"Exclusion Area 7",
							"Exclusion Area 8",
							"Exclusion Area 9",
							"Exchange Area 10",
							"Exchange Area 11"
						],
						id: [
							"GEO_R_REG_AREA.576", // EEZ MED
							"GEO_R_REG_AREA.577", // EEZ ATLANTIC BISCAY
							"GEO_R_REG_AREA.578", // EEZ ATLANTIC OFF FINISTERRE
							"GEO_R_REG_AREA.579" // EEZ LA MANCHE
						]
					},
					createHoles: ["layer_name"]
				},
				tooltip: false,
				commandInfo: {
					featureAs: "area",
					bindings: {
						areaCommandInfo: {
							show: (area) => {
								switch(area.get("NAME")){
									case "Exclusion Area 2":
									case "Exclusion Area 3":
									case "Exclusion Area 4":
									case "Exclusion Area 5":
									case "Exclusion Area 6":
									case "Exclusion Area 7":
									case "Exclusion Area 8":
									case "Exclusion Area 9":
									case "Exchange Area 10":
									case "Exchange Area 11":
										return true;
									default:
										return false;
								}
							}
						}
					},
					templateUrl: resolvePath("./areas/cgd/command_info.template.html")
				},
				style: require("./areas/cgd/style.js")
			},
			"area_import": true
		}),
		location(),
		alerts(),
		apps({
			"abm_alert_insert": true,
			"sentinel": true
		})
	]
});