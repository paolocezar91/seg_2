module.exports = ({aoi, vessels, metOcean, areas, location, alerts, apps}) => ({
	"modules": [
		aoi(),
		vessels({
			"symbolizers": [
				"IALA",
				"Data Source",
				"Vessel Type",
				"Voyage Information"
			],
			"filters": [
				"Voyage information",
				"Position Source",
				"Vessel Flag",
				"Vessel Type (PSC Types)",
				"Navigation Status (AIS)",
				"User Risk Status"
			],
			"advanced_search":{
				"show_ssn_information": true
			},
			"vessel_models": true,
			"ssn_enrichment": true,
			"lrit": true,
			"fisheries_link": true,
			"ga_plan": true,
			"track_import": true
		}),
		metOcean({
			"remote_sensing": true,
			"forecast": true,
			"in_situ": true
		}),
		areas({
			layers: [
				...require("../../layers/ms_specific/config.js")({
					"eu_layers": {}
				})
			],
			"area_import": true
		}),
		location(),
		alerts(),
		apps({
			"abm_alert_insert": true,
			"sentinel": true
		})
	]
});