module.exports = ({aoi, areas, vessels, /*orgInfraLogis,*/ eo, metOcean, location, alerts, apps}) => ({
	"modules": [
		aoi(),
		vessels({
			"symbolizers": [
				"IALA",
				"Data Source",
				"Vessel Type",
				"Voyage Information"
			],
			"filters": [
				"Voyage information",
				"Vessel Flag",
				"Vessel Type (PSC Types)",
				"User Risk Status"
			],
			"advanced_search":{
				"show_ssn_information": true
			},
			"vessel_models": true,
			"ssn_enrichment": true,
			"fisheries_link": true,
			"ga_plan": true,
			"track_import": true,
			"thetis": true
		}),
		// orgInfraLogis(),
		eo({
			"eo_show_footprints": true,
			"oil_spills_report_in_ssn": true,
			"footprints_activity_detection": true,
			"sentinel": true,
			"eo_acq": true,
			"vds": true
		}),
		metOcean({
			"remote_sensing": true,
			"forecast": true,
			"in_situ": true
		}),
		location(),
		alerts(),
		apps({
			"sentinel": true,
			"abm_alert_insert": true
		}),
		areas({
			"area_import": true
		})
	]
});