const styleCache = {};

module.exports = area => {
	const styles = [],
		name = area.get("NAME"),
		selected = area.get("selected");
	let fillColor, strokeColor = "#00f", strokeWidth = 1.25, text;

	switch (name){
		case "Exchange Area 10":
		case "Exchange Area 11":
			fillColor = "rgba(0,0,8,.1)";
			text = area.get("NAME");
			break;
		case "EEZ_France.shp_3":
		case "EEZ_France.shp_1":
		case "EEZ_France.shp_2":
		case "EEZ_France.shp_0":
			fillColor = "rgba(0,0,8,.1)";
			strokeColor = "#00f";
			text = area.get("DESCRIPTION");
			break;
		default:
			fillColor = "rgba(0,0,0,0.0)";
			text = area.get("NAME");
			break;
	}

	if(selected){
		strokeColor = "#ff0";
		strokeWidth = 2.5;
	}

	const cacheKey = JSON.stringify({fillColor, strokeColor, strokeWidth, name});

	if(!styleCache[cacheKey])
		styleCache[cacheKey] = new ol.style.Style({
			fill: new ol.style.Fill({
				color: fillColor
			}),
			stroke: new ol.style.Stroke({
				color: strokeColor,
				width: strokeWidth
			}),
			text: new ol.style.Text({
				font: "12px sans-serif",
				text,
				fill: new ol.style.Fill({
					color: "#FFF"
				}),
				stroke: new ol.style.Stroke({
					color: "#000",
					width: 1
				})
			})
		});

	styles.push(styleCache[cacheKey]);

	return styles;
};
