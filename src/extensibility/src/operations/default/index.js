module.exports = ({aoi, vessels, ports, orgInfraLogis, eo, sarSurpic, metOcean, areas, location, alerts, apps, events}) => ({
	"modules": [
		aoi(),
		vessels({
			"symbolizers": [
				"IALA",
				"EFCA",
				"Data Source",
				"Target Age",
				"Vessel Type",
				"Voyage Information"
			],
			"filters": [
				"Voyage information",
				"Position Source",
				"Vessel Flag",
				"Vessel Type (PSC Types)",
				"Navigation Status (AIS)",
				"User Risk Status",
				"Fishing vessels"
			],
			"advanced_search":{
				"show_ssn_information": true
			},
			"ssn_enrichment": true,
			"lrit": true,
			"vessel_models": true,
			"fisheries_link": true,
			"ga_plan": true,
			"thetis": true,
			"track_import": true
		}),
		ports(),
		orgInfraLogis({
			"stmid": true
		}),
		eo({
			"eo_acq": true,
			"oil_spills": true,
			"eo_show_footprints": true,
			"eo_show_oilspills": true,
			"oil_spills_report_in_ssn": true,
			"footprints_alert_report": true,
			"footprints_activity_detection": true,
			"footprints_download_eo": true,
			"sentinel": true,
			"vds": true
		}),
		sarSurpic(),
		metOcean({
			"remote_sensing": true,
			"forecast": true,
			"in_situ": true
		}),
		areas({
			layers: [
				...require("../../layers/ms_specific/config.js")({
					"malta_layers": {},
					"italy_layers": {},
					"netherlands_layers": {},
					"portugal_layers": {},
					"ireland_layers": {},
					"sweden_layers": {},
					"uk_layers": {},
					"eu_layers": {}
				}),
				require("../../layers/fisheries/config.js")([
					"general_fishery",
					"global_fao_areas",
					"data_filter_area",
					"rmfo_specific_areas",
					"NEAFC",
					"north_sea_ww",
					"ww_pelagic",
					"baltic_sea",
					"NAFO",
					"tuna_farms"
				]),
				...require("../../layers/eunavfor_areas/config.js"),
				require("../../layers/density_map/config.js")
			],
			"area_import": true
		}),
		location(),
		alerts(),
		apps({
			"abm_alert_insert": true,
			"fishery_events": true,
			"sentinel": true
		}),
		events()
	]
});