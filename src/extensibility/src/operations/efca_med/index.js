module.exports = ({aoi, vessels, eo, metOcean, areas, location, alerts, apps, events}) => ({
	"modules": [
		aoi(),
		vessels({
			"symbolizers": [
				"IALA",
				"EFCA",
				"Data Source",
				"Target Age",
				"Vessel Type",
				"Voyage Information"
			],
			"filters": [
				"Voyage information",
				"Position Source",
				"Vessel Flag",
				"Vessel Type (PSC Types)",
				"Navigation Status (AIS)",
				"User Risk Status",
				"Fishing vessels"
			],
			"advanced_search":{
				"show_ssn_information": false
			},
			"vessel_models": true,
			"ssn_enrichment": true,
			"fisheries_link": true,
			"track_import": true
		}),
		eo({
			"eo_show_footprints": true,
			"oil_spills_report_in_ssn": true,
			"footprints_activity_detection": true,
			"sentinel": true,
			"vds": true
		}),
		metOcean({
			"remote_sensing": true,
			"forecast": true,
			"in_situ": true
		}),
		areas({
			layers: [
				...require("../../layers/ms_specific/config.js")({
					"malta_layers": {},
					"italy_layers": {},
					"netherlands_layers": {},
					"portugal_layers": {},
					"ireland_layers": {},
					"sweden_layers": {},
					"uk_layers": {},
					"eu_layers": {}
				}),
				require("../../layers/fisheries/config.js")([
					"general_fishery",
					"global_fao_areas",
					"data_filter_area",
					"tuna_farms"
				])
			],
			"area_import": true
		}),
		location(),
		alerts(),
		apps({
			"abm_alert_insert": true,
			"fishery_events": true,
			"sentinel": true
		}),
		events()
	]
});