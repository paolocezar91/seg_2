const msSpecificLayers = require("../../layers/ms_specific/config.js")({
	"eu_layers": {}
});

const euLayers = msSpecificLayers.find(layer => layer.id === "eu_layers");

delete euLayers.source.supportedLayers;

Object.assign(euLayers, {
	name: "Indicative EEZs",
	source: Object.assign(euLayers.source, {
		params: {
			LAYERS: "EEZ_BOUNDARIES_V10"
		}
	})
});

module.exports = ({aoi, vessels, ports, orgInfraLogis, eo, sarSurpic, metOcean, areas, location, alerts, apps}) => ({
	"base_layers": {
		include: [
			"gebco",
			"demis"
		]
	},
	"modules": [
		aoi(),
		vessels({
			"symbolizers": [
				"IALA",
				"Data Source",
				"Vessel Type",
				"Target Age",
				"Voyage Information"
			],
			"filters": [
				"Voyage information",
				"Position Source",
				"Vessel Flag",
				"Vessel Type (PSC Types)",
				"Navigation Status (AIS)",
				"User Risk Status"
			],
			"advanced_search":{
				"show_ssn_information": true
			},
			"vessel_models": true,
			"ssn_enrichment": true,
			"lrit": true,
			"fisheries_link": true,
			"ga_plan": true,
			"thetis": true,
			"track_import": true
		}),
		ports(),
		orgInfraLogis({
			"stmid": true
		}),
		apps({
			"abm_alert_insert": true,
			"insert_oilspill": true,
			"oil_spills_feedback": true,
			"sentinel": true
		}),
		eo({
			"oil_spills": true,
			"eo_acq": true,
			"eo_show_oilspills": true,
			"eo_show_footprints": true,
			"oil_spills_slick_info": true,
			"oil_spills_symbolizers": ["Class Type", "Alert Level" , "Feedback Type"],
			"oil_spills_fields": require("./eo/oil_spills_fields"),
			"oil_spills_table_fields": require("./eo/oil_spills_table_fields"),
			"oil_spills_tooltip_fields": require("./eo/oil_spills_tooltip_fields"),
			"oil_spills_search_fields": require("./eo/oil_spills_search_fields"),
			"oil_spills_report_in_ssn": true,
			"footprints_alert_report": true,
			"footprints_activity_detection": true,
			"footprints_download_eo": true,
			"footprints_fields": require("./eo/footprints_fields"),
			"footprints_tooltip": resolvePath("./eo/footprints_tooltip.html"),
			"footprints_table_fields": require("./eo/footprints_table_fields"),
			"footprints_search_fields": require("./eo/footprints_search_fields"),
			"footprints_filters": require("./eo/footprints_filters"),
			"vds_correlated_fields": require("./eo/detected_vessels_fields"),
			"vds_uncorrelated_fields": require("./eo/detected_vessels_fields"),
			"sentinel": true,
			"vds": true
		}),
		sarSurpic(),
		metOcean({
			"remote_sensing": true,
			"forecast": true,
			"in_situ": true
		}),
		areas({
			"layers": msSpecificLayers,
			"cgd": {
				filters: {
					preset_filters: {
						name: "AREAS",
						options: ["Albania", "Azerbeijan", "Belgium", "BonnAgreement", "Bulgaria", "Croatia", "Cyprus", "Denmark", "Estonia", "Finland", "France", "Germany", "Georgia", "Greece", "Iceland", "Ireland", "Italy", "Jordan", "Latvia", "Lithuania", "Malta", "Montenegro", "Morocco", "Netherlands", "Norway", "Poland", "Portugal", "Romania", "Slovenia", "Spain", "Sweden", "Tunisia", "Turkey", "UK"]
					},
					only: ["Oilspills Alert Areas" /*, EO Coverage*/]
				},
				features: {
					params: {
						type: ["EO_SPILL_ALERTING_A" /*, EO_COVERAGE_REQUIR*/]
					}
				},
				tooltip: {
					fields: require("./areas/cgd/tooltip_fields")
				}
			},
			"area_import": true
		}),
		location(),
		alerts()
	]
});