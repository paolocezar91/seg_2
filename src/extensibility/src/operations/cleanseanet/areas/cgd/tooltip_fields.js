module.exports = {
	options: [
		{
			label: "Authority",
			template: "{{area.get('AUTHORITY')}}"
		},
		{
			label: "Name",
			template: "{{area.get('NAME')}}"
		},
		{
			label: "Modification date",
			template: "<span position-timestamp=\"area.get('MODIFICATION_DATE')\"></span>"
		}
	]
};