const mapProperties = require("../../../modules/eo/VDS/properties");
module.exports = mapProperties({
	override: {
		vds_label: {order: 0},
		serviceId: {order: 2},
		timestamp: {order: 3, label: "Acquisition time"},
		correlationResult: {order: 4},
		confidence: {order: 5},
		latitude: {order: 6},
		longitude: {order: 7},
		vesselType: {order: 8},
		estimatedLength: {order: 9},
		estimatedWidth: {order: 10},
		estimatedHeading: {order: 11},
		estimatedSpeed: {order: 12},
		speedClassification: {order: 13},
		lengthClass: {order: 14},
		accuracyError: {order: 15},
		lengthError: {order: 16, visible: true},
		widthError: {order: 17, visible: true},
		mmsi: {order: 19, visible: false},
		imo: {order: 20, visible: false},
		shipName: {order: 21, visible: false},
		positionAccuracyX: {order: 22},
		positionAccuracyY: {order: 23}
	}
});