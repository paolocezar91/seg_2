const
	STATUS = require("../../../modules/eo/SAR/footprints/constants/status.json"),
	EO_OPERATIONS = ["CleanSeaNet","SAFEMED","BCSEA","TRACECA","IMS_POLLUTION","PlatFormMon"],
	ORIGINS = require("../../../modules/eo/SAR/footprints/constants/origins.json"),
	SENSOR_TYPES = require("../../../modules/eo/SAR/footprints/constants/sensor_types.json");

const {generateOptions, DEFAULT_EO_SEARCH_FIELDS} = require("../../../modules/eo/SAR/footprints/api");

module.exports = Object.assign({}, DEFAULT_EO_SEARCH_FIELDS, {
	acquisitionStart: moment().subtract(90,"d"),
	acquisitionStop: moment(),
	status: null,
	statusOptions: generateOptions(STATUS, true),
	eoOperation: null,
	operationOptions: generateOptions(EO_OPERATIONS, true),
	origin: null,
	originOptions: generateOptions(ORIGINS, true),
	sensor: null,
	sensorOptions: generateOptions(SENSOR_TYPES, true)
});