const mapProperties = require("../../../modules/eo/SAR/footprints/properties");

module.exports = mapProperties({
	override: {
		id: false,
		status: false,
		operation: false,
		DTOid: false,
		satelliteSensor: false,
		acquisitionStart: false,
		acquisitionStop: false,
		totalOilSpills: false,
		totalVDS: false,
		wasRecorrelated: false,
		lastRecorrelation: false,
		totalActivities: false,
		sensorType: false,
		sensorMode: false,
		sensorResolution: false,
		affectedAlertAreas: false,
		pass: false,
		pinpointing: false,
		polarization: false,
		coverageCompliance: false,
		usableArea: false,
		feedback: false,
		polygon: {
			visible: false
		}
	}
});


