const mapProperties = require("../../../modules/eo/SAR/oil_spills/properties.js");

module.exports = mapProperties({
	override:{
		timeStamp: {order: 0 },
		serviceId: {order: 1, visible: true},
		area: {label: "Area ({{oilSpillsTooltip.getAreaUnits()}})", order: 2, visible: false},
		length: {label: "Length ({{oilSpillsTooltip.getDistanceUnits()}})", order: 3, visible: false},
		width: {label:  "Width ({{oilSpillsTooltip.getDistanceUnits()}})", order: 4 },
		distanceFromCoast: {label: "Distance from coast ({{oilSpillsTooltip.getDistanceUnits()}})", order: 5, visible: false},
		spillIdentifier: {order: 6, visible: false},
		origin: {order: 7, visible: false},
		reliabilityClass: {order: 8, visible: false},
		numberOfSlicks: {order: 9, visible: false},
		possibleSourceDetected: {order: 10, visible: false},
		possibleSourceIdentified: {order: 11, visible: false},
		sar_windDirection: {order: 12, visible: false},
		sar_windIntensity: {order: 13, visible: false}
	}
});
