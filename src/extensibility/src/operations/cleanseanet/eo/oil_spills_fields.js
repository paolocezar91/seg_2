const mapProperties = require("../../../modules/eo/SAR/oil_spills/properties.js");

module.exports = mapProperties({
	override:{
		id: false,
		serviceId: {order: 1},
		timeStamp: {order: 2},
		acquisitionStop: {order: 2},
		latitude: {order: 3},
		longitude: {order: 4},
		area: {order: 5},
		length: {order: 6},
		width: {order: 7},
		distanceFromCoast: {order: 8},
		numberOfSlicks: {order: 9},
		origin: {
			template: `<span>
				<span>{{::oilSpill.get('origin') === 'SYNTHETIC' ? 'EXPECTED' : (oilSpill.get('origin') || "N/A")}}</span>
				<button style="margin-left: 2px; display: inline;" ng-if="oilSpill.get('origin')" ng-click="oilSpillsCommandInfo.openOriginPopup()" title="Info" class="seg primary round xs inline-block"><i icon="info"></i></button>
			</span>`,
			order: 10
		},
		reliabilityClass: {label: "Class", order: 11},
		feedback: {order: 12},
		possibleSourceDetected: {order: 13},
		possibleSourceIdentified: {order: 14},
		warningIssued: {order: 15},
		satellite: {order: 15},
		sar_windIntensity: {order: 16},
		sar_windDirection: {order: 17},
		met_windIntensity: {order: 18},
		met_windDirection: {order: 19},
		operation: {order: 20},
		affectedAlertAreas: {order: 21},
		sensorResolution:{
			order: 22
		},
		alertLevel:{
			order:23
		}
	}
});
