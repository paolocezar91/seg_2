const mapProperties = require("../../../modules/eo/SAR/footprints/properties");

module.exports = mapProperties({
	override: {
		id: false,
		status: false,
		operation: false,
		DTOid: false,
		satelliteSensor: false,
		acquisitionStart: false,
		acquisitionStop: false,
		totalOilSpills: false,
		totalVDS: false,
		wasRecorrelated: false,
		lastRecorrelation: false,
		totalActivities: false,
		sensorType: false,
		sensorMode: false,
		sensorResolution: false,
		affectedAlertAreas: false,
		pass: false,
		pinpointing: false,
		polarization: false,
		coverageCompliance: false,
		usableArea: false,
		feedback: false,
		polygon: {
			template: `<span style="display: block; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
				<span ng-repeat="coord in EOCommandInfo.getCoordinates(EO)">
					<span coordinate="[coord[1], 'lat']"></span>
					<span coordinate="[coord[0], 'lon']"></span>;
				</span>
			</span>`,
			visible: true
		}
	}
});