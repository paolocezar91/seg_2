const {
	DEFAULT_SEARCH_FIELDS
} = require("../../../modules/eo/SAR/oil_spills/search");

module.exports = Object.assign({}, DEFAULT_SEARCH_FIELDS, {
	eoOperation: null,
	origin: null,
	originOptions: [{
		label:"Detected",
		value:"DETECTED"
	}, {
		label:"Expected",
		value:"EXPECTED"
	}, {
		label:"Predicted",
		value:"PREDICTED"
	}, {
		label:"All",
		value: null
	}],
	warningIssued: null,
	warningIssuedOptions: [{
		label:"Yes",
		value: true
	}, {
		label:"No",
		value: false
	}, {
		label:"All",
		value: null
	}]
	// Waiting for the correct parameter name
	// psd: null,
	// psdOptions: [{
	// 	label:"Yes",
	// 	value: true
	// }, {
	// 	label:"No",
	// 	value: false
	// }, {
	// 	label:"All",
	// 	value: null
	// }]
});