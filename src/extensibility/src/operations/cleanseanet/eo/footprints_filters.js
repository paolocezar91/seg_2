const SATELLITES = require("../../../modules/eo/SAR/footprints/constants/satellites.json");

const {STATUS_FILTER, createEOFilter, operationMatchFn} = require("../../../modules/eo/SAR/footprints/filters");

const createSatelliteFilter = satellites => createEOFilter("Satellite", satellites, (EO, satellite) => EO.get("platform") === satellite);

const SATELLITE_FILTERS = {
	"SAR": createSatelliteFilter(SATELLITES.sar),
	"Optical": createSatelliteFilter(SATELLITES.optical)
};

const OPERATION_FILTER = createEOFilter("EO Operation", [
	"CleanSeaNet",
	"SAFEMED",
	"BCSEA",
	"TRACECA",
	"IMS_POLLUTION",
	"PlatFormMon"
], operationMatchFn);

module.exports = [
	STATUS_FILTER,
	SATELLITE_FILTERS,
	OPERATION_FILTER
];

