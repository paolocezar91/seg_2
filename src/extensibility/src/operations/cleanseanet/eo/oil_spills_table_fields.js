const mapProperties = require("../../../modules/eo/SAR/oil_spills/properties.js");

module.exports = mapProperties({
	override:{
		id: {
			template: "{{getLabel(oilSpill)}}",
			order: 0
		},
		serviceId: {order: 1},
		timeStamp: {order: 2},
		acquisitionStop: {order: 2},
		latitude: {order: 3},
		longitude: {order: 4},
		area: {order: 5, label: "Area ({{getAreaUnits()}})"},
		length: {order: 6, label: "Length ({{getDistanceUnits()}})"},
		width: {order: 7, label: "Width ({{getDistanceUnits()}})"},
		distanceFromCoast: {order: 8, label: "Distance from coast ({{getDistanceUnits()}})"},
		numberOfSlicks: {order: 9},
		origin: {order: 10},
		reliabilityClass: {label: "Class", order: 11},
		feedback: {order: 12},
		possibleSourceDetected: {order: 13},
		possibleSourceIdentified: {order: 14},
		warningIssued: {order: 15},
		satellite: {order: 15},
		sar_windIntensity: {order: 16, visible: false},
		sar_windDirection: {order: 17, visible: false},
		met_windIntensity: {order: 18, visible: false},
		met_windDirection: {order: 19, visible: false},
		operation: {
			template: "{{getOperations(oilSpill)}}",
			order: 20
		},
		affectedAlertAreas: {order: 21},
		sensorResolution:{
			order: 22
		},
		alertLevel:{
			order:23
		}
	}
});
