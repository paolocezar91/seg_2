const fs = require("fs-extra"),
	browserify = require("browserify"),
	babelify = require("babelify"),
	through = require("through"),
	path = require("path"),
	uglifyify = require("uglifyify"),
	extensibilityRegex = new RegExp(`.+${(path.sep + "extensibility" + path.sep + "src").replace(/\\/g, "\\\\")}(.+)`, "g");

//extensibility
function resolvePath(file) {
	let buffer = "";

	if (!/\.js/.test(file))
		return through();

	return through(chunk => buffer += chunk.toString(), function() {
		const jst = buffer.toString().replace(/resolvePath\(.(.+).\)/g, (str, match) => "'"+path.normalize(path.dirname(file)+"/"+match).replace(extensibilityRegex, "extensibility$1").split("\\").join("\\\\")+"'");
		this.queue(jst);

		return this.queue(null);
	});
}

function svg(file) {
	let buffer = "";

	if (!/\.svg/.test(file))
		return through();

	return through(chunk => buffer += chunk.toString(), function() {
		const jst = buffer.toString(),
			compiled = `
				const parser = new DOMParser();
				module.exports = parser.parseFromString(${JSON.stringify(jst)}, 'image/svg+xml').querySelector('svg');
			`;

		this.queue(compiled);

		return this.queue(null);
	});
}

const b = browserify({
	entries: ["src/operations/index.js"],
	standalone: "extensibility",
	transform: [
		svg,
		resolvePath,
		babelify.configure({
			presets: ["latest"]
		}),
		uglifyify
	]
});

console.log("Bundling extensibility...");

function cleanDir(dir) {
	let files = fs.readdirSync(dir);

	files.forEach(file => {
		const resolvedPath = path.resolve(dir, file);

		const stat = fs.statSync(resolvedPath);

		if(stat.isDirectory())
			cleanDir(resolvedPath);
	});

	files = fs.readdirSync(dir);

	if(!files.length)
		return fs.removeSync(dir);
}

const dirs = ["layers", "modules", "operations"];

try {
	dirs.forEach(dir => fs.removeSync("build/" + dir));

	fs.copySync("src", "build", {
		filter(file) {
			return !file.endsWith(".js");
		}
	});

	dirs.forEach(dir => cleanDir("build/" + dir));

	const writeStream = fs.createWriteStream("build/extensibility.min.js");

	b.bundle()
		.on("error", e => {
			console.error(e.stack);
			writeStream.end();
			process.exit(1);
		})
		.pipe(writeStream)
		.on("close", () => {
			console.log("Bundle finished.");
			process.exit(0);
		});
} catch(e) {
	console.error(e);
	process.exit(1);
}